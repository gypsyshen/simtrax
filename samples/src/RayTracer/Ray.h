
#ifndef _Ray_H
#define _Ray_H

#include "trax.hpp"
#include "syVector.h"

// camera ray
class Ray {
public:
	syVector p, dir;
	syVector inv_dir;
	int sign[3];
	
public:
	Ray();
	Ray(const Ray &r);
	Ray(const syVector _p, const syVector _dir);

	void Set(const syVector _p, const syVector _dir);
    void Normalize();

	Ray& operator=(const Ray &_ray);	
};

#endif