// all trax programs must include trax.hpp
#include "trax.hpp"

#include "Image.h"
#include "Ray.h"
#include "syVector.h"
#include "sySphere.h"
#include "PointLight.h"
#include "Scene.h"
#include "HitRecord.h"
#include "Lambertian.h"
#include "PinholeCamera.h"
#include "syBox.h"
#include "syTriangle.h"

// "main" function is called "trax_main", returns void instead of int
void trax_main(){
	int xres = GetXRes();
	int yres = GetYRes();
	int start_fb = GetFrameBuffer();
	Image image(xres, yres, start_fb);
	
	// load scene
	Scene scene;
	scene.Load();

	// load sample numbers
	int numSamples = loadi(TRAX_NUM_SAMPLES);
	// load ray depth
	int rayDepth = loadi(TRAX_RAY_DEPTH);
	
	// load camera
	PinholeCamera camera( GetCamera(), xres, yres );
	
	// atomic increment allows multiple threads to get unique pixel assignments
	// generate camera ray
	for( int pix = atomicinc(0); pix < xres*yres; pix = atomicinc(0) ) {
		// i, j are the framebuffer pixel coordinates
		// [i = 0, j = 0] is bottom-left
		// [i = yres-1, j = xres-1] is the top-right
		int i = pix / xres;
		int j = pix % xres;
		
		Color result(0.f, 0.f, 0.f);
		if(numSamples == 1) {
			Ray ray = camera.GenerateRay(j, i);
			result = scene.GetColor(ray, rayDepth);
		} else {
			for(int s = 0; s < numSamples; ++s) {
				// generate a ray
				Ray ray = camera.GenerateSampleRay(j, i);
				result += scene.GetColor(ray, rayDepth);
			}
			result /= (float)numSamples;
		}
		
		// assign the color
		image.Set(i, j, result);
	}
}


