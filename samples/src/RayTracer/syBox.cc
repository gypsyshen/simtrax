#include "syBox.h"
#include "Utility.h"

syBox::syBox() { Set(); }
syBox::syBox(const syVector &_min, const syVector &_max) {	Set(_min, _max); }

void syBox::LoadFromMemory(const int &_addr) {
	bound[0].LoadFromMemory(_addr);
	bound[1].LoadFromMemory(_addr+3);
}

void syBox::Set(const syVector &_min, const syVector &_max) {
	bound[0] = _min;
	bound[1] = _max;
}

void syBox::Set() {
	bound[0].Zero();
	bound[1].Zero();
}

// reference: An Efficient and Robust Ray�CBox Intersection Algorithm, Amy Williams et al. 2004.
// valid intersection interval: [0 t]
bool syBox::IntersectsW( const Ray &ray, HitDist &t_box_near) const {

	float t_min, t_max, ty_min, ty_max, tz_min, tz_max;

	t_min = (bound[ray.sign[0]].x - ray.p.x) * ray.inv_dir.x;
	t_max = (bound[1-ray.sign[0]].x - ray.p.x) * ray.inv_dir.x;
	ty_min = (bound[ray.sign[1]].y - ray.p.y) * ray.inv_dir.y;
	ty_max = (bound[1-ray.sign[1]].y - ray.p.y) * ray.inv_dir.y;

	if( (t_min>ty_max) || (ty_min>t_max) ) return false;
	
	if(ty_min > t_min) t_min = ty_min;
	if(ty_max < t_max) t_max = ty_max;

	tz_min = (bound[ray.sign[2]].z - ray.p.z) * ray.inv_dir.z;
	tz_max = (bound[1-ray.sign[2]].z - ray.p.z) * ray.inv_dir.z;

	if( (t_min>tz_max) || (tz_min>t_max) ) return false;


	if(tz_min > t_min) t_min = tz_min;
	if(tz_max < t_max) t_max = tz_max;

	if(t_max < 0.f) return false;

	t_box_near.t = t_min;
//	t_box_far.t = t_max;
	return true;
}