#BEGIN Preamble
	REG	r0
	REG	r1
	REG	r2
	REG	r3
	REG	r4
	REG	r5
	REG	r6
	REG	r7
	REG	r8
	REG	r9
	REG	r10
	REG	r11
	REG	r12
	REG	r13
	REG	r14
	REG	r15
	REG	r16
	REG	r17
	REG	r18
	REG	r19
	REG	r20
	REG	r21
	REG	r22
	REG	r23
	REG	r24
	REG	r25
	REG	r26
	REG	r27
	REG	r28
	REG	r29
	REG	r30
	REG	r31
	LOADIMM	r0, 0
	ADDI	r1, r0, 8192
start:	brlid	r15, main
	NOP
	HALT
abort:	ADDI	r5, r0, -1
	PRINT	r5
	HALT
#END Preamble
#	.file	"rt.bc"
#	.text
#	.globl	_ZN5ColorC2Ei
#	.align	2
#	.type	_ZN5ColorC2Ei,@function
#	.ent	_ZN5ColorC2Ei           # @_ZN5ColorC2Ei
_ZN5ColorC2Ei:
#	.frame	r1,0,r15
#	.mask	0x0
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2Ei
$0tmp0:
#	.size	_ZN5ColorC2Ei, ($tmp0)-_ZN5ColorC2Ei

#	.globl	_ZN5Color14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN5Color14LoadFromMemoryERKi,@function
#	.ent	_ZN5Color14LoadFromMemoryERKi # @_ZN5Color14LoadFromMemoryERKi
_ZN5Color14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5Color14LoadFromMemoryERKi
$0tmp1:
#	.size	_ZN5Color14LoadFromMemoryERKi, ($tmp1)-_ZN5Color14LoadFromMemoryERKi

#	.globl	_ZN5ColorC2Ev
#	.align	2
#	.type	_ZN5ColorC2Ev,@function
#	.ent	_ZN5ColorC2Ev           # @_ZN5ColorC2Ev
_ZN5ColorC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	0
	SWI	r3,	r5,	4
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2Ev
$0tmp2:
#	.size	_ZN5ColorC2Ev, ($tmp2)-_ZN5ColorC2Ev

#	.globl	_ZN5ColorC2ERKS_
#	.align	2
#	.type	_ZN5ColorC2ERKS_,@function
#	.ent	_ZN5ColorC2ERKS_        # @_ZN5ColorC2ERKS_
_ZN5ColorC2ERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2ERKS_
$0tmp3:
#	.size	_ZN5ColorC2ERKS_, ($tmp3)-_ZN5ColorC2ERKS_

#	.globl	_ZN5ColorC2ERKfS1_S1_
#	.align	2
#	.type	_ZN5ColorC2ERKfS1_S1_,@function
#	.ent	_ZN5ColorC2ERKfS1_S1_   # @_ZN5ColorC2ERKfS1_S1_
_ZN5ColorC2ERKfS1_S1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r7,	0
	SWI	r3,	r5,	4
	LWI	r3,	r8,	0
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2ERKfS1_S1_
$0tmp4:
#	.size	_ZN5ColorC2ERKfS1_S1_, ($tmp4)-_ZN5ColorC2ERKfS1_S1_

#	.globl	_ZNK5ColorplERKS_
#	.align	2
#	.type	_ZNK5ColorplERKS_,@function
#	.ent	_ZNK5ColorplERKS_       # @_ZNK5ColorplERKS_
_ZNK5ColorplERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPADD	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPADD	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPADD	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColorplERKS_
$0tmp5:
#	.size	_ZNK5ColorplERKS_, ($tmp5)-_ZNK5ColorplERKS_

#	.globl	_ZNK5ColormiERKS_
#	.align	2
#	.type	_ZNK5ColormiERKS_,@function
#	.ent	_ZNK5ColormiERKS_       # @_ZNK5ColormiERKS_
_ZNK5ColormiERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPRSUB	r4,	r3,	r4
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPRSUB	r4,	r8,	r6
	SWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColormiERKS_
$0tmp6:
#	.size	_ZNK5ColormiERKS_, ($tmp6)-_ZNK5ColormiERKS_

#	.globl	_ZNK5ColormlERKS_
#	.align	2
#	.type	_ZNK5ColormlERKS_,@function
#	.ent	_ZNK5ColormlERKS_       # @_ZNK5ColormlERKS_
_ZNK5ColormlERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPMUL	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPMUL	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPMUL	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColormlERKS_
$0tmp7:
#	.size	_ZNK5ColormlERKS_, ($tmp7)-_ZNK5ColormlERKS_

#	.globl	_ZNK5ColordvERKS_
#	.align	2
#	.type	_ZNK5ColordvERKS_,@function
#	.ent	_ZNK5ColordvERKS_       # @_ZNK5ColordvERKS_
_ZNK5ColordvERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPDIV	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPDIV	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPDIV	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColordvERKS_
$0tmp8:
#	.size	_ZNK5ColordvERKS_, ($tmp8)-_ZNK5ColordvERKS_

#	.globl	_ZNK5ColorplEf
#	.align	2
#	.type	_ZNK5ColorplEf,@function
#	.ent	_ZNK5ColorplEf          # @_ZNK5ColorplEf
_ZNK5ColorplEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	FPADD	r4,	r3,	r7
	LWI	r3,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPADD	r4,	r6,	r7
	SWI	r4,	r5,	4
	FPADD	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColorplEf
$0tmp9:
#	.size	_ZNK5ColorplEf, ($tmp9)-_ZNK5ColorplEf

#	.globl	_ZNK5ColormiEf
#	.align	2
#	.type	_ZNK5ColormiEf,@function
#	.ent	_ZNK5ColormiEf          # @_ZNK5ColormiEf
_ZNK5ColormiEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	FPRSUB	r4,	r7,	r3
	LWI	r3,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPRSUB	r4,	r7,	r6
	SWI	r4,	r5,	4
	FPRSUB	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColormiEf
$0tmp10:
#	.size	_ZNK5ColormiEf, ($tmp10)-_ZNK5ColormiEf

#	.globl	_ZNK5ColormlEf
#	.align	2
#	.type	_ZNK5ColormlEf,@function
#	.ent	_ZNK5ColormlEf          # @_ZNK5ColormlEf
_ZNK5ColormlEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	FPMUL	r4,	r3,	r7
	LWI	r3,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPMUL	r4,	r6,	r7
	SWI	r4,	r5,	4
	FPMUL	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColormlEf
$0tmp11:
#	.size	_ZNK5ColormlEf, ($tmp11)-_ZNK5ColormlEf

#	.globl	_ZNK5ColordvEf
#	.align	2
#	.type	_ZNK5ColordvEf,@function
#	.ent	_ZNK5ColordvEf          # @_ZNK5ColordvEf
_ZNK5ColordvEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	FPDIV	r4,	r3,	r7
	LWI	r3,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPDIV	r4,	r6,	r7
	SWI	r4,	r5,	4
	FPDIV	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5ColordvEf
$0tmp12:
#	.size	_ZNK5ColordvEf, ($tmp12)-_ZNK5ColordvEf

#	.globl	_ZN5ColorpLERKS_
#	.align	2
#	.type	_ZN5ColorpLERKS_,@function
#	.ent	_ZN5ColorpLERKS_        # @_ZN5ColorpLERKS_
_ZN5ColorpLERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColorpLERKS_
$0tmp13:
#	.size	_ZN5ColorpLERKS_, ($tmp13)-_ZN5ColorpLERKS_

#	.globl	_ZN5ColormIERKS_
#	.align	2
#	.type	_ZN5ColormIERKS_,@function
#	.ent	_ZN5ColormIERKS_        # @_ZN5ColormIERKS_
_ZN5ColormIERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColormIERKS_
$0tmp14:
#	.size	_ZN5ColormIERKS_, ($tmp14)-_ZN5ColormIERKS_

#	.globl	_ZN5ColormLERKS_
#	.align	2
#	.type	_ZN5ColormLERKS_,@function
#	.ent	_ZN5ColormLERKS_        # @_ZN5ColormLERKS_
_ZN5ColormLERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColormLERKS_
$0tmp15:
#	.size	_ZN5ColormLERKS_, ($tmp15)-_ZN5ColormLERKS_

#	.globl	_ZN5ColordVERKS_
#	.align	2
#	.type	_ZN5ColordVERKS_,@function
#	.ent	_ZN5ColordVERKS_        # @_ZN5ColordVERKS_
_ZN5ColordVERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColordVERKS_
$0tmp16:
#	.size	_ZN5ColordVERKS_, ($tmp16)-_ZN5ColordVERKS_

#	.globl	_ZN5ColorpLEf
#	.align	2
#	.type	_ZN5ColorpLEf,@function
#	.ent	_ZN5ColorpLEf           # @_ZN5ColorpLEf
_ZN5ColorpLEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	FPADD	r3,	r3,	r6
	SWI	r3,	r5,	0
	LWI	r3,	r5,	4
	FPADD	r3,	r3,	r6
	SWI	r3,	r5,	4
	LWI	r3,	r5,	8
	FPADD	r3,	r3,	r6
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColorpLEf
$0tmp17:
#	.size	_ZN5ColorpLEf, ($tmp17)-_ZN5ColorpLEf

#	.globl	_ZN5ColormIEf
#	.align	2
#	.type	_ZN5ColormIEf,@function
#	.ent	_ZN5ColormIEf           # @_ZN5ColormIEf
_ZN5ColormIEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	FPRSUB	r3,	r6,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r5,	4
	FPRSUB	r3,	r6,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r5,	8
	FPRSUB	r3,	r6,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColormIEf
$0tmp18:
#	.size	_ZN5ColormIEf, ($tmp18)-_ZN5ColormIEf

#	.globl	_ZN5ColormLEf
#	.align	2
#	.type	_ZN5ColormLEf,@function
#	.ent	_ZN5ColormLEf           # @_ZN5ColormLEf
_ZN5ColormLEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	FPMUL	r3,	r3,	r6
	SWI	r3,	r5,	0
	LWI	r3,	r5,	4
	FPMUL	r3,	r3,	r6
	SWI	r3,	r5,	4
	LWI	r3,	r5,	8
	FPMUL	r3,	r3,	r6
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColormLEf
$0tmp19:
#	.size	_ZN5ColormLEf, ($tmp19)-_ZN5ColormLEf

#	.globl	_ZN5ColordVEf
#	.align	2
#	.type	_ZN5ColordVEf,@function
#	.ent	_ZN5ColordVEf           # @_ZN5ColordVEf
_ZN5ColordVEf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	FPDIV	r3,	r3,	r6
	SWI	r3,	r5,	0
	LWI	r3,	r5,	4
	FPDIV	r3,	r3,	r6
	SWI	r3,	r5,	4
	LWI	r3,	r5,	8
	FPDIV	r3,	r3,	r6
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColordVEf
$0tmp20:
#	.size	_ZN5ColordVEf, ($tmp20)-_ZN5ColordVEf

#	.globl	_ZNK5Color5ClampEv
#	.align	2
#	.type	_ZNK5Color5ClampEv,@function
#	.ent	_ZNK5Color5ClampEv      # @_ZNK5Color5ClampEv
_ZNK5Color5ClampEv:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	0
	SWI	r3,	r5,	4
	SWI	r3,	r5,	8
	LWI	r3,	r6,	0
	ORI	r4,	r0,	0
	FPLT	r8,	r3,	r4
	bneid	r8,	$0BB21_2
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB21_2:
	bneid	r7,	$0BB21_7
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r8,	r3,	r4
	bneid	r8,	$0BB21_5
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB21_5:
	bneid	r7,	$0BB21_7
	NOP
	ADD	r4,	r3,	r0
$0BB21_7:
	SWI	r4,	r5,	0
	LWI	r3,	r6,	4
	ORI	r4,	r0,	0
	FPLT	r8,	r3,	r4
	bneid	r8,	$0BB21_9
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB21_9:
	bneid	r7,	$0BB21_14
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r8,	r3,	r4
	bneid	r8,	$0BB21_12
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB21_12:
	bneid	r7,	$0BB21_14
	NOP
	ADD	r4,	r3,	r0
$0BB21_14:
	SWI	r4,	r5,	4
	LWI	r3,	r6,	8
	ORI	r4,	r0,	0
	FPLT	r7,	r3,	r4
	bneid	r7,	$0BB21_16
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB21_16:
	bneid	r6,	$0BB21_21
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r7,	r3,	r4
	bneid	r7,	$0BB21_19
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB21_19:
	bneid	r6,	$0BB21_21
	NOP
	ADD	r4,	r3,	r0
$0BB21_21:
	rtsd	r15,	8
	SWI	r4,	r5,	8
#	.end	_ZNK5Color5ClampEv
$0tmp21:
#	.size	_ZNK5Color5ClampEv, ($tmp21)-_ZNK5Color5ClampEv

#	.globl	_ZNK5Color1REv
#	.align	2
#	.type	_ZNK5Color1REv,@function
#	.ent	_ZNK5Color1REv          # @_ZNK5Color1REv
_ZNK5Color1REv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZNK5Color1REv
$0tmp22:
#	.size	_ZNK5Color1REv, ($tmp22)-_ZNK5Color1REv

#	.globl	_ZNK5Color1GEv
#	.align	2
#	.type	_ZNK5Color1GEv,@function
#	.ent	_ZNK5Color1GEv          # @_ZNK5Color1GEv
_ZNK5Color1GEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADDI	r3,	r5,	4
#	.end	_ZNK5Color1GEv
$0tmp23:
#	.size	_ZNK5Color1GEv, ($tmp23)-_ZNK5Color1GEv

#	.globl	_ZNK5Color1BEv
#	.align	2
#	.type	_ZNK5Color1BEv,@function
#	.ent	_ZNK5Color1BEv          # @_ZNK5Color1BEv
_ZNK5Color1BEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADDI	r3,	r5,	8
#	.end	_ZNK5Color1BEv
$0tmp24:
#	.size	_ZNK5Color1BEv, ($tmp24)-_ZNK5Color1BEv

#	.globl	_ZNK5Color5ClampERKS_S1_
#	.align	2
#	.type	_ZNK5Color5ClampERKS_S1_,@function
#	.ent	_ZNK5Color5ClampERKS_S1_ # @_ZNK5Color5ClampERKS_S1_
_ZNK5Color5ClampERKS_S1_:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	0
	SWI	r3,	r5,	4
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPLT	r10,	r4,	r3
	bneid	r10,	$0BB25_2
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB25_2:
	bneid	r9,	$0BB25_7
	NOP
	LWI	r3,	r8,	0
	FPGT	r10,	r4,	r3
	bneid	r10,	$0BB25_5
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB25_5:
	bneid	r9,	$0BB25_7
	NOP
	ADD	r3,	r4,	r0
$0BB25_7:
	SWI	r3,	r5,	0
	LWI	r3,	r7,	4
	LWI	r4,	r6,	4
	FPLT	r10,	r4,	r3
	bneid	r10,	$0BB25_9
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB25_9:
	bneid	r9,	$0BB25_14
	NOP
	LWI	r3,	r8,	4
	FPGT	r10,	r4,	r3
	bneid	r10,	$0BB25_12
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB25_12:
	bneid	r9,	$0BB25_14
	NOP
	ADD	r3,	r4,	r0
$0BB25_14:
	SWI	r3,	r5,	4
	LWI	r3,	r7,	8
	LWI	r4,	r6,	8
	FPLT	r7,	r4,	r3
	bneid	r7,	$0BB25_16
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB25_16:
	bneid	r6,	$0BB25_21
	NOP
	LWI	r3,	r8,	8
	FPGT	r7,	r4,	r3
	bneid	r7,	$0BB25_19
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB25_19:
	bneid	r6,	$0BB25_21
	NOP
	ADD	r3,	r4,	r0
$0BB25_21:
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK5Color5ClampERKS_S1_
$0tmp25:
#	.size	_ZNK5Color5ClampERKS_S1_, ($tmp25)-_ZNK5Color5ClampERKS_S1_

#	.globl	_ZN9HitRecordC2Ev
#	.align	2
#	.type	_ZN9HitRecordC2Ev,@function
#	.ent	_ZN9HitRecordC2Ev       # @_ZN9HitRecordC2Ev
_ZN9HitRecordC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1203982336
	SWI	r3,	r5,	0
	ADDI	r3,	r0,	-1
	SWI	r3,	r5,	4
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN9HitRecordC2Ev
$0tmp26:
#	.size	_ZN9HitRecordC2Ev, ($tmp26)-_ZN9HitRecordC2Ev

#	.globl	_ZN9HitRecord4InitEv
#	.align	2
#	.type	_ZN9HitRecord4InitEv,@function
#	.ent	_ZN9HitRecord4InitEv    # @_ZN9HitRecord4InitEv
_ZN9HitRecord4InitEv:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1203982336
	SWI	r3,	r5,	0
	ADDI	r3,	r0,	-1
	SWI	r3,	r5,	4
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN9HitRecord4InitEv
$0tmp27:
#	.size	_ZN9HitRecord4InitEv, ($tmp27)-_ZN9HitRecord4InitEv

#	.globl	_ZNK9HitRecord3HitEv
#	.align	2
#	.type	_ZNK9HitRecord3HitEv,@function
#	.ent	_ZNK9HitRecord3HitEv    # @_ZNK9HitRecord3HitEv
_ZNK9HitRecord3HitEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	ORI	r4,	r0,	1203982336
	FPNE	r4,	r3,	r4
	bneid	r4,	$0BB28_2
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB28_2:
	rtsd	r15,	8
	NOP
#	.end	_ZNK9HitRecord3HitEv
$0tmp28:
#	.size	_ZNK9HitRecord3HitEv, ($tmp28)-_ZNK9HitRecord3HitEv

#	.globl	_ZN9HitRecordaSERKS_
#	.align	2
#	.type	_ZN9HitRecordaSERKS_,@function
#	.ent	_ZN9HitRecordaSERKS_    # @_ZN9HitRecordaSERKS_
_ZN9HitRecordaSERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN9HitRecordaSERKS_
$0tmp29:
#	.size	_ZN9HitRecordaSERKS_, ($tmp29)-_ZN9HitRecordaSERKS_

#	.globl	_ZN7HitDist4InitEv
#	.align	2
#	.type	_ZN7HitDist4InitEv,@function
#	.ent	_ZN7HitDist4InitEv      # @_ZN7HitDist4InitEv
_ZN7HitDist4InitEv:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1203982336
	rtsd	r15,	8
	SWI	r3,	r5,	0
#	.end	_ZN7HitDist4InitEv
$0tmp30:
#	.size	_ZN7HitDist4InitEv, ($tmp30)-_ZN7HitDist4InitEv

#	.globl	_ZN7HitDistC2Ev
#	.align	2
#	.type	_ZN7HitDistC2Ev,@function
#	.ent	_ZN7HitDistC2Ev         # @_ZN7HitDistC2Ev
_ZN7HitDistC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1203982336
	rtsd	r15,	8
	SWI	r3,	r5,	0
#	.end	_ZN7HitDistC2Ev
$0tmp31:
#	.size	_ZN7HitDistC2Ev, ($tmp31)-_ZN7HitDistC2Ev

#	.globl	_ZN5ImageC2ERKiS1_S1_
#	.align	2
#	.type	_ZN5ImageC2ERKiS1_S1_,@function
#	.ent	_ZN5ImageC2ERKiS1_S1_   # @_ZN5ImageC2ERKiS1_S1_
_ZN5ImageC2ERKiS1_S1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r7,	0
	SWI	r3,	r5,	4
	LWI	r3,	r8,	0
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ImageC2ERKiS1_S1_
$0tmp32:
#	.size	_ZN5ImageC2ERKiS1_S1_, ($tmp32)-_ZN5ImageC2ERKiS1_S1_

#	.globl	_ZN5Image3SetERKiS1_RK5Color
#	.align	2
#	.type	_ZN5Image3SetERKiS1_RK5Color,@function
#	.ent	_ZN5Image3SetERKiS1_RK5Color # @_ZN5Image3SetERKiS1_RK5Color
_ZN5Image3SetERKiS1_RK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r4,	r8,	0
	ORI	r3,	r0,	0
	FPLT	r10,	r4,	r3
	bneid	r10,	$0BB33_2
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB33_2:
	bneid	r9,	$0BB33_7
	NOP
	ORI	r3,	r0,	1065353216
	FPGT	r10,	r4,	r3
	bneid	r10,	$0BB33_5
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB33_5:
	bneid	r9,	$0BB33_7
	NOP
	ADD	r3,	r4,	r0
$0BB33_7:
	LWI	r9,	r8,	4
	ORI	r4,	r0,	0
	FPLT	r11,	r9,	r4
	bneid	r11,	$0BB33_9
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB33_9:
	bneid	r10,	$0BB33_14
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r11,	r9,	r4
	bneid	r11,	$0BB33_12
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB33_12:
	bneid	r10,	$0BB33_14
	NOP
	ADD	r4,	r9,	r0
$0BB33_14:
	LWI	r9,	r8,	8
	ORI	r8,	r0,	0
	FPLT	r11,	r9,	r8
	bneid	r11,	$0BB33_16
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB33_16:
	bneid	r10,	$0BB33_21
	NOP
	ORI	r8,	r0,	1065353216
	FPGT	r11,	r9,	r8
	bneid	r11,	$0BB33_19
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB33_19:
	bneid	r10,	$0BB33_21
	NOP
	ADD	r8,	r9,	r0
$0BB33_21:
	LWI	r9,	r6,	0
	LWI	r10,	r5,	0
	MUL	r9,	r10,	r9
	LWI	r10,	r7,	0
	ADD	r9,	r9,	r10
	MULI	r9,	r9,	3
	LWI	r10,	r5,	8
	ADD	r9,	r9,	r10
	STORE	r9,	r3,	0
	LWI	r3,	r6,	0
	LWI	r9,	r5,	0
	MUL	r3,	r9,	r3
	LWI	r9,	r7,	0
	ADD	r3,	r3,	r9
	MULI	r3,	r3,	3
	LWI	r9,	r5,	8
	ADD	r3,	r3,	r9
	STORE	r3,	r4,	1
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	MUL	r3,	r4,	r3
	LWI	r4,	r7,	0
	ADD	r3,	r3,	r4
	MULI	r3,	r3,	3
	LWI	r4,	r5,	8
	ADD	r3,	r3,	r4
	STORE	r3,	r8,	2
	rtsd	r15,	8
	NOP
#	.end	_ZN5Image3SetERKiS1_RK5Color
$0tmp33:
#	.size	_ZN5Image3SetERKiS1_RK5Color, ($tmp33)-_ZN5Image3SetERKiS1_RK5Color

#	.globl	_ZN10Lambertian10SetDiffuseERK5Color
#	.align	2
#	.type	_ZN10Lambertian10SetDiffuseERK5Color,@function
#	.ent	_ZN10Lambertian10SetDiffuseERK5Color # @_ZN10Lambertian10SetDiffuseERK5Color
_ZN10Lambertian10SetDiffuseERK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	rtsd	r15,	8
	SWI	r3,	r5,	0
#	.end	_ZN10Lambertian10SetDiffuseERK5Color
$0tmp34:
#	.size	_ZN10Lambertian10SetDiffuseERK5Color, ($tmp34)-_ZN10Lambertian10SetDiffuseERK5Color

#	.globl	_ZNK10Lambertian7DiffuseEv
#	.align	2
#	.type	_ZNK10Lambertian7DiffuseEv,@function
#	.ent	_ZNK10Lambertian7DiffuseEv # @_ZNK10Lambertian7DiffuseEv
_ZNK10Lambertian7DiffuseEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZNK10Lambertian7DiffuseEv
$0tmp35:
#	.size	_ZNK10Lambertian7DiffuseEv, ($tmp35)-_ZNK10Lambertian7DiffuseEv

#	.globl	_ZN13PinholeCameraC2EiRKiS1_
#	.align	2
#	.type	_ZN13PinholeCameraC2EiRKiS1_,@function
#	.ent	_ZN13PinholeCameraC2EiRKiS1_ # @_ZN13PinholeCameraC2EiRKiS1_
_ZN13PinholeCameraC2EiRKiS1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	FPCONV	r4,	r3
	ORI	r3,	r0,	1065353216
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	64
	LWI	r4,	r8,	0
	FPCONV	r4,	r4
	FPDIV	r3,	r3,	r4
	SWI	r3,	r5,	68
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	SWI	r3,	r5,	8
	ADDI	r3,	r6,	9
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	ADDI	r3,	r6,	12
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	32
	ADDI	r3,	r6,	15
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	40
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	44
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	48
	ADDI	r3,	r6,	18
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	52
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	56
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	60
#	.end	_ZN13PinholeCameraC2EiRKiS1_
$0tmp36:
#	.size	_ZN13PinholeCameraC2EiRKiS1_, ($tmp36)-_ZN13PinholeCameraC2EiRKiS1_

#	.globl	_ZN13PinholeCamera3SetEiRKiS1_
#	.align	2
#	.type	_ZN13PinholeCamera3SetEiRKiS1_,@function
#	.ent	_ZN13PinholeCamera3SetEiRKiS1_ # @_ZN13PinholeCamera3SetEiRKiS1_
_ZN13PinholeCamera3SetEiRKiS1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	FPCONV	r4,	r3
	ORI	r3,	r0,	1065353216
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	64
	LWI	r4,	r8,	0
	FPCONV	r4,	r4
	FPDIV	r3,	r3,	r4
	SWI	r3,	r5,	68
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	SWI	r3,	r5,	8
	ADDI	r3,	r6,	9
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	ADDI	r3,	r6,	12
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	32
	ADDI	r3,	r6,	15
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	40
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	44
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	48
	ADDI	r3,	r6,	18
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	52
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	56
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	60
#	.end	_ZN13PinholeCamera3SetEiRKiS1_
$0tmp37:
#	.size	_ZN13PinholeCamera3SetEiRKiS1_, ($tmp37)-_ZN13PinholeCamera3SetEiRKiS1_

#	.globl	_ZN13PinholeCameraC2Ev
#	.align	2
#	.type	_ZN13PinholeCameraC2Ev,@function
#	.ent	_ZN13PinholeCameraC2Ev  # @_ZN13PinholeCameraC2Ev
_ZN13PinholeCameraC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN13PinholeCameraC2Ev
$0tmp38:
#	.size	_ZN13PinholeCameraC2Ev, ($tmp38)-_ZN13PinholeCameraC2Ev

#	.globl	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
#	.align	2
#	.type	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_,@function
#	.ent	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_ # @_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_:
#	.frame	r1,12,r15
#	.mask	0x300000
	ADDI	r1,	r1,	-12
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	LWI	r3,	r10,	0
	FPCONV	r4,	r3
	ORI	r3,	r0,	1065353216
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	64
	LWI	r4,	r1,	40
	LWI	r4,	r4,	0
	FPCONV	r4,	r4
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	68
	LWI	r4,	r6,	0
	SWI	r4,	r5,	0
	LWI	r4,	r6,	4
	SWI	r4,	r5,	4
	LWI	r4,	r6,	8
	SWI	r4,	r5,	8
	LWI	r4,	r7,	0
	SWI	r4,	r5,	12
	LWI	r4,	r7,	4
	SWI	r4,	r5,	16
	LWI	r4,	r7,	8
	SWI	r4,	r5,	20
	LWI	r4,	r9,	0
	SWI	r4,	r5,	36
	LWI	r4,	r8,	0
	SWI	r4,	r5,	24
	LWI	r6,	r8,	4
	SWI	r6,	r5,	28
	FPMUL	r4,	r4,	r4
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	LWI	r6,	r8,	8
	SWI	r6,	r5,	32
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	FPINVSQRT	r4,	r4
	FPDIV	r7,	r3,	r4
	LWI	r4,	r5,	24
	FPDIV	r4,	r4,	r7
	SWI	r4,	r5,	24
	LWI	r6,	r5,	28
	FPDIV	r6,	r6,	r7
	SWI	r6,	r5,	28
	LWI	r8,	r5,	32
	FPDIV	r9,	r8,	r7
	SWI	r9,	r5,	32
	LWI	r11,	r5,	20
	FPMUL	r7,	r4,	r11
	LWI	r8,	r5,	12
	FPMUL	r12,	r9,	r8
	FPRSUB	r7,	r7,	r12
	FPMUL	r8,	r6,	r8
	LWI	r20,	r5,	16
	FPMUL	r12,	r4,	r20
	FPRSUB	r8,	r8,	r12
	FPMUL	r12,	r8,	r6
	FPMUL	r21,	r7,	r9
	FPRSUB	r12,	r12,	r21
	FPMUL	r20,	r9,	r20
	FPMUL	r11,	r6,	r11
	FPRSUB	r11,	r20,	r11
	SWI	r11,	r5,	40
	SWI	r7,	r5,	44
	SWI	r8,	r5,	48
	SWI	r12,	r5,	52
	FPMUL	r9,	r11,	r9
	FPMUL	r12,	r8,	r4
	FPRSUB	r9,	r9,	r12
	SWI	r9,	r5,	56
	FPMUL	r4,	r7,	r4
	FPMUL	r6,	r11,	r6
	FPRSUB	r4,	r4,	r6
	SWI	r4,	r5,	60
	FPMUL	r4,	r7,	r7
	FPMUL	r6,	r11,	r11
	FPADD	r4,	r6,	r4
	FPMUL	r6,	r8,	r8
	FPADD	r4,	r4,	r6
	FPINVSQRT	r4,	r4
	FPDIV	r4,	r3,	r4
	LWI	r6,	r5,	40
	FPDIV	r6,	r6,	r4
	SWI	r6,	r5,	40
	LWI	r6,	r5,	44
	FPDIV	r6,	r6,	r4
	SWI	r6,	r5,	44
	LWI	r6,	r5,	48
	FPDIV	r4,	r6,	r4
	SWI	r4,	r5,	48
	LWI	r4,	r5,	56
	FPMUL	r4,	r4,	r4
	LWI	r6,	r5,	52
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r6,	r4
	LWI	r6,	r5,	60
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	FPINVSQRT	r8,	r4
	LWI	r4,	r5,	36
	LWI	r6,	r5,	40
	FPMUL	r11,	r6,	r4
	LWI	r6,	r10,	0
	LWI	r7,	r5,	68
	LWI	r9,	r5,	52
	LWI	r10,	r5,	56
	LWI	r12,	r5,	60
	SWI	r11,	r5,	40
	LWI	r11,	r5,	44
	FPMUL	r11,	r11,	r4
	SWI	r11,	r5,	44
	FPDIV	r11,	r3,	r8
	FPDIV	r3,	r12,	r11
	FPDIV	r8,	r10,	r11
	FPDIV	r9,	r9,	r11
	LWI	r10,	r5,	48
	FPMUL	r10,	r10,	r4
	SWI	r10,	r5,	48
	FPCONV	r6,	r6
	FPMUL	r6,	r6,	r7
	FPDIV	r4,	r4,	r6
	FPMUL	r6,	r9,	r4
	SWI	r6,	r5,	52
	FPMUL	r6,	r8,	r4
	SWI	r6,	r5,	56
	FPMUL	r3,	r3,	r4
	SWI	r3,	r5,	60
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
$0tmp39:
#	.size	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_, ($tmp39)-_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_

#	.globl	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
#	.align	2
#	.type	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_,@function
#	.ent	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_ # @_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_:
#	.frame	r1,12,r15
#	.mask	0x300000
	ADDI	r1,	r1,	-12
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	LWI	r3,	r10,	0
	FPCONV	r4,	r3
	ORI	r3,	r0,	1065353216
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	64
	LWI	r4,	r1,	40
	LWI	r4,	r4,	0
	FPCONV	r4,	r4
	FPDIV	r4,	r3,	r4
	SWI	r4,	r5,	68
	LWI	r4,	r6,	0
	SWI	r4,	r5,	0
	LWI	r4,	r6,	4
	SWI	r4,	r5,	4
	LWI	r4,	r6,	8
	SWI	r4,	r5,	8
	LWI	r4,	r7,	0
	SWI	r4,	r5,	12
	LWI	r4,	r7,	4
	SWI	r4,	r5,	16
	LWI	r4,	r7,	8
	SWI	r4,	r5,	20
	LWI	r4,	r9,	0
	SWI	r4,	r5,	36
	LWI	r4,	r8,	0
	SWI	r4,	r5,	24
	LWI	r6,	r8,	4
	SWI	r6,	r5,	28
	FPMUL	r4,	r4,	r4
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	LWI	r6,	r8,	8
	SWI	r6,	r5,	32
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	FPINVSQRT	r4,	r4
	FPDIV	r7,	r3,	r4
	LWI	r4,	r5,	24
	FPDIV	r4,	r4,	r7
	SWI	r4,	r5,	24
	LWI	r6,	r5,	28
	FPDIV	r6,	r6,	r7
	SWI	r6,	r5,	28
	LWI	r8,	r5,	32
	FPDIV	r9,	r8,	r7
	SWI	r9,	r5,	32
	LWI	r11,	r5,	20
	FPMUL	r7,	r4,	r11
	LWI	r8,	r5,	12
	FPMUL	r12,	r9,	r8
	FPRSUB	r7,	r7,	r12
	FPMUL	r8,	r6,	r8
	LWI	r20,	r5,	16
	FPMUL	r12,	r4,	r20
	FPRSUB	r8,	r8,	r12
	FPMUL	r12,	r8,	r6
	FPMUL	r21,	r7,	r9
	FPRSUB	r12,	r12,	r21
	FPMUL	r20,	r9,	r20
	FPMUL	r11,	r6,	r11
	FPRSUB	r11,	r20,	r11
	SWI	r11,	r5,	40
	SWI	r7,	r5,	44
	SWI	r8,	r5,	48
	SWI	r12,	r5,	52
	FPMUL	r9,	r11,	r9
	FPMUL	r12,	r8,	r4
	FPRSUB	r9,	r9,	r12
	SWI	r9,	r5,	56
	FPMUL	r4,	r7,	r4
	FPMUL	r6,	r11,	r6
	FPRSUB	r4,	r4,	r6
	SWI	r4,	r5,	60
	FPMUL	r4,	r7,	r7
	FPMUL	r6,	r11,	r11
	FPADD	r4,	r6,	r4
	FPMUL	r6,	r8,	r8
	FPADD	r4,	r4,	r6
	FPINVSQRT	r4,	r4
	FPDIV	r4,	r3,	r4
	LWI	r6,	r5,	40
	FPDIV	r6,	r6,	r4
	SWI	r6,	r5,	40
	LWI	r6,	r5,	44
	FPDIV	r6,	r6,	r4
	SWI	r6,	r5,	44
	LWI	r6,	r5,	48
	FPDIV	r4,	r6,	r4
	SWI	r4,	r5,	48
	LWI	r4,	r5,	56
	FPMUL	r4,	r4,	r4
	LWI	r6,	r5,	52
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r6,	r4
	LWI	r6,	r5,	60
	FPMUL	r6,	r6,	r6
	FPADD	r4,	r4,	r6
	FPINVSQRT	r8,	r4
	LWI	r4,	r5,	36
	LWI	r6,	r5,	40
	FPMUL	r11,	r6,	r4
	LWI	r6,	r10,	0
	LWI	r7,	r5,	68
	LWI	r9,	r5,	52
	LWI	r10,	r5,	56
	LWI	r12,	r5,	60
	SWI	r11,	r5,	40
	LWI	r11,	r5,	44
	FPMUL	r11,	r11,	r4
	SWI	r11,	r5,	44
	FPDIV	r11,	r3,	r8
	FPDIV	r3,	r12,	r11
	FPDIV	r8,	r10,	r11
	FPDIV	r9,	r9,	r11
	LWI	r10,	r5,	48
	FPMUL	r10,	r10,	r4
	SWI	r10,	r5,	48
	FPCONV	r6,	r6
	FPMUL	r6,	r6,	r7
	FPDIV	r4,	r4,	r6
	FPMUL	r6,	r9,	r4
	SWI	r6,	r5,	52
	FPMUL	r6,	r8,	r4
	SWI	r6,	r5,	56
	FPMUL	r3,	r3,	r4
	SWI	r3,	r5,	60
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
$0tmp40:
#	.size	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_, ($tmp40)-_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_

#	.globl	_ZN13PinholeCamera14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN13PinholeCamera14LoadFromMemoryERKi,@function
#	.ent	_ZN13PinholeCamera14LoadFromMemoryERKi # @_ZN13PinholeCamera14LoadFromMemoryERKi
_ZN13PinholeCamera14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	9
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	12
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	32
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	15
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	40
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	44
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	48
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	18
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	52
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	56
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	60
#	.end	_ZN13PinholeCamera14LoadFromMemoryERKi
$0tmp41:
#	.size	_ZN13PinholeCamera14LoadFromMemoryERKi, ($tmp41)-_ZN13PinholeCamera14LoadFromMemoryERKi

#	.globl	_ZNK13PinholeCamera11GenerateRayERKiS1_
#	.align	2
#	.type	_ZNK13PinholeCamera11GenerateRayERKiS1_,@function
#	.ent	_ZNK13PinholeCamera11GenerateRayERKiS1_ # @_ZNK13PinholeCamera11GenerateRayERKiS1_
_ZNK13PinholeCamera11GenerateRayERKiS1_:
#	.frame	r1,28,r15
#	.mask	0x7f00000
	ADDI	r1,	r1,	-28
	SWI	r20,	r1,	24
	SWI	r21,	r1,	20
	SWI	r22,	r1,	16
	SWI	r23,	r1,	12
	SWI	r24,	r1,	8
	SWI	r25,	r1,	4
	SWI	r26,	r1,	0
	LWI	r10,	r8,	0
	LWI	r21,	r7,	0
	LWI	r3,	r6,	32
	LWI	r4,	r6,	48
	LWI	r7,	r6,	60
	LWI	r8,	r6,	8
	LWI	r9,	r6,	56
	LWI	r11,	r6,	28
	LWI	r12,	r6,	44
	LWI	r20,	r6,	52
	LWI	r22,	r6,	68
	LWI	r23,	r6,	24
	LWI	r24,	r6,	40
	LWI	r25,	r6,	4
	LWI	r26,	r6,	64
	LWI	r6,	r6,	0
	SWI	r6,	r5,	0
	FPCONV	r6,	r21
	FPMUL	r6,	r6,	r26
	SWI	r25,	r5,	4
	FPADD	r6,	r6,	r6
	ORI	r21,	r0,	-1082130432
	FPADD	r6,	r6,	r21
	FPMUL	r24,	r24,	r6
	FPADD	r23,	r23,	r24
	FPCONV	r10,	r10
	FPMUL	r10,	r10,	r22
	FPADD	r10,	r10,	r10
	FPADD	r21,	r10,	r21
	FPMUL	r10,	r20,	r21
	FPADD	r10,	r23,	r10
	FPMUL	r12,	r12,	r6
	FPADD	r11,	r11,	r12
	FPMUL	r9,	r9,	r21
	FPADD	r9,	r11,	r9
	SWI	r8,	r5,	8
	FPMUL	r8,	r9,	r9
	FPMUL	r11,	r10,	r10
	FPADD	r8,	r11,	r8
	FPMUL	r7,	r7,	r21
	FPMUL	r4,	r4,	r6
	FPADD	r3,	r3,	r4
	FPADD	r3,	r3,	r7
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r8,	r4
	FPINVSQRT	r4,	r4
	ORI	r11,	r0,	1065353216
	FPDIV	r7,	r11,	r4
	FPDIV	r4,	r10,	r7
	FPDIV	r6,	r9,	r7
	FPDIV	r7,	r3,	r7
	FPDIV	r3,	r11,	r7
	FPDIV	r8,	r11,	r6
	FPDIV	r9,	r11,	r4
	ORI	r10,	r0,	0
	FPLT	r12,	r9,	r10
	FPLT	r22,	r8,	r10
	FPLT	r20,	r3,	r10
	ADDI	r21,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB42_2
	ADD	r10,	r11,	r0
	ADD	r10,	r21,	r0
$0BB42_2:
	bneid	r22,	$0BB42_4
	ADD	r20,	r11,	r0
	ADD	r20,	r21,	r0
$0BB42_4:
	bneid	r12,	$0BB42_6
	NOP
	ADD	r11,	r21,	r0
$0BB42_6:
	SWI	r4,	r5,	12
	SWI	r6,	r5,	16
	SWI	r7,	r5,	20
	SWI	r9,	r5,	24
	SWI	r8,	r5,	28
	SWI	r3,	r5,	32
	SWI	r11,	r5,	36
	SWI	r20,	r5,	40
	SWI	r10,	r5,	44
	LWI	r26,	r1,	0
	LWI	r25,	r1,	4
	LWI	r24,	r1,	8
	LWI	r23,	r1,	12
	LWI	r22,	r1,	16
	LWI	r21,	r1,	20
	LWI	r20,	r1,	24
	rtsd	r15,	8
	ADDI	r1,	r1,	28
#	.end	_ZNK13PinholeCamera11GenerateRayERKiS1_
$0tmp42:
#	.size	_ZNK13PinholeCamera11GenerateRayERKiS1_, ($tmp42)-_ZNK13PinholeCamera11GenerateRayERKiS1_

#	.globl	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
#	.align	2
#	.type	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_,@function
#	.ent	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_ # @_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
_ZNK13PinholeCamera17GenerateSampleRayERKiS1_:
#	.frame	r1,36,r15
#	.mask	0x1ff00000
	ADDI	r1,	r1,	-36
	SWI	r20,	r1,	32
	SWI	r21,	r1,	28
	SWI	r22,	r1,	24
	SWI	r23,	r1,	20
	SWI	r24,	r1,	16
	SWI	r25,	r1,	12
	SWI	r26,	r1,	8
	SWI	r27,	r1,	4
	SWI	r28,	r1,	0
	RAND	r23
	RAND	r21
	LWI	r11,	r8,	0
	LWI	r24,	r7,	0
	LWI	r3,	r6,	32
	LWI	r4,	r6,	48
	LWI	r7,	r6,	60
	LWI	r8,	r6,	8
	LWI	r9,	r6,	56
	LWI	r10,	r6,	28
	LWI	r12,	r6,	44
	LWI	r20,	r6,	52
	LWI	r22,	r6,	68
	LWI	r25,	r6,	24
	LWI	r26,	r6,	40
	LWI	r27,	r6,	4
	LWI	r28,	r6,	64
	LWI	r6,	r6,	0
	SWI	r6,	r5,	0
	ORI	r6,	r0,	-1090519040
	FPADD	r23,	r23,	r6
	FPCONV	r24,	r24
	FPADD	r23,	r24,	r23
	FPADD	r21,	r21,	r6
	FPMUL	r6,	r23,	r28
	SWI	r27,	r5,	4
	FPADD	r6,	r6,	r6
	ORI	r23,	r0,	-1082130432
	FPADD	r6,	r6,	r23
	FPMUL	r24,	r26,	r6
	FPADD	r24,	r25,	r24
	FPCONV	r11,	r11
	FPADD	r11,	r11,	r21
	FPMUL	r11,	r11,	r22
	FPADD	r11,	r11,	r11
	FPADD	r21,	r11,	r23
	FPMUL	r11,	r20,	r21
	FPADD	r11,	r24,	r11
	FPMUL	r12,	r12,	r6
	FPADD	r10,	r10,	r12
	FPMUL	r9,	r9,	r21
	FPADD	r9,	r10,	r9
	SWI	r8,	r5,	8
	FPMUL	r8,	r9,	r9
	FPMUL	r10,	r11,	r11
	FPADD	r8,	r10,	r8
	FPMUL	r7,	r7,	r21
	FPMUL	r4,	r4,	r6
	FPADD	r3,	r3,	r4
	FPADD	r3,	r3,	r7
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r8,	r4
	FPINVSQRT	r4,	r4
	ORI	r10,	r0,	1065353216
	FPDIV	r7,	r10,	r4
	FPDIV	r4,	r11,	r7
	FPDIV	r6,	r9,	r7
	FPDIV	r7,	r3,	r7
	FPDIV	r3,	r10,	r7
	FPDIV	r8,	r10,	r6
	FPDIV	r9,	r10,	r4
	ORI	r10,	r0,	0
	FPLT	r12,	r9,	r10
	FPLT	r22,	r8,	r10
	FPLT	r20,	r3,	r10
	ADDI	r21,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB43_2
	ADD	r10,	r11,	r0
	ADD	r10,	r21,	r0
$0BB43_2:
	bneid	r22,	$0BB43_4
	ADD	r20,	r11,	r0
	ADD	r20,	r21,	r0
$0BB43_4:
	bneid	r12,	$0BB43_6
	NOP
	ADD	r11,	r21,	r0
$0BB43_6:
	SWI	r4,	r5,	12
	SWI	r6,	r5,	16
	SWI	r7,	r5,	20
	SWI	r9,	r5,	24
	SWI	r8,	r5,	28
	SWI	r3,	r5,	32
	SWI	r11,	r5,	36
	SWI	r20,	r5,	40
	SWI	r10,	r5,	44
	LWI	r28,	r1,	0
	LWI	r27,	r1,	4
	LWI	r26,	r1,	8
	LWI	r25,	r1,	12
	LWI	r24,	r1,	16
	LWI	r23,	r1,	20
	LWI	r22,	r1,	24
	LWI	r21,	r1,	28
	LWI	r20,	r1,	32
	rtsd	r15,	8
	ADDI	r1,	r1,	36
#	.end	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
$0tmp43:
#	.size	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_, ($tmp43)-_ZNK13PinholeCamera17GenerateSampleRayERKiS1_

#	.globl	_ZN10PointLightC2EiRK5Color
#	.align	2
#	.type	_ZN10PointLightC2EiRK5Color,@function
#	.ent	_ZN10PointLightC2EiRK5Color # @_ZN10PointLightC2EiRK5Color
_ZN10PointLightC2EiRK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	12
	SWI	r3,	r5,	16
	SWI	r3,	r5,	20
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	SWI	r3,	r5,	8
	LWI	r3,	r7,	8
	SWI	r3,	r5,	20
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	0
	rtsd	r15,	8
	SWI	r3,	r5,	12
#	.end	_ZN10PointLightC2EiRK5Color
$0tmp44:
#	.size	_ZN10PointLightC2EiRK5Color, ($tmp44)-_ZN10PointLightC2EiRK5Color

#	.globl	_ZN10PointLight14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN10PointLight14LoadFromMemoryERKi,@function
#	.ent	_ZN10PointLight14LoadFromMemoryERKi # @_ZN10PointLight14LoadFromMemoryERKi
_ZN10PointLight14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN10PointLight14LoadFromMemoryERKi
$0tmp45:
#	.size	_ZN10PointLight14LoadFromMemoryERKi, ($tmp45)-_ZN10PointLight14LoadFromMemoryERKi

#	.globl	_ZN10PointLightC2Ev
#	.align	2
#	.type	_ZN10PointLightC2Ev,@function
#	.ent	_ZN10PointLightC2Ev     # @_ZN10PointLightC2Ev
_ZN10PointLightC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r0,	r5,	20
	SWI	r0,	r5,	16
	SWI	r0,	r5,	12
	SWI	r0,	r5,	8
	SWI	r0,	r5,	4
	rtsd	r15,	8
	SWI	r0,	r5,	0
#	.end	_ZN10PointLightC2Ev
$0tmp46:
#	.size	_ZN10PointLightC2Ev, ($tmp46)-_ZN10PointLightC2Ev

#	.globl	_ZN10PointLightC2ERK8syVectorRK5Color
#	.align	2
#	.type	_ZN10PointLightC2ERK8syVectorRK5Color,@function
#	.ent	_ZN10PointLightC2ERK8syVectorRK5Color # @_ZN10PointLightC2ERK8syVectorRK5Color
_ZN10PointLightC2ERK8syVectorRK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	12
	SWI	r3,	r5,	16
	SWI	r3,	r5,	20
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	8
	SWI	r3,	r5,	20
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	0
	rtsd	r15,	8
	SWI	r3,	r5,	12
#	.end	_ZN10PointLightC2ERK8syVectorRK5Color
$0tmp47:
#	.size	_ZN10PointLightC2ERK8syVectorRK5Color, ($tmp47)-_ZN10PointLightC2ERK8syVectorRK5Color

#	.globl	_ZN10PointLight12SetIntensityERK5Color
#	.align	2
#	.type	_ZN10PointLight12SetIntensityERK5Color,@function
#	.ent	_ZN10PointLight12SetIntensityERK5Color # @_ZN10PointLight12SetIntensityERK5Color
_ZN10PointLight12SetIntensityERK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	8
	SWI	r3,	r5,	20
	LWI	r3,	r6,	4
	SWI	r3,	r5,	16
	LWI	r3,	r6,	0
	rtsd	r15,	8
	SWI	r3,	r5,	12
#	.end	_ZN10PointLight12SetIntensityERK5Color
$0tmp48:
#	.size	_ZN10PointLight12SetIntensityERK5Color, ($tmp48)-_ZN10PointLight12SetIntensityERK5Color

#	.globl	_ZNK10PointLight8PositionEv
#	.align	2
#	.type	_ZNK10PointLight8PositionEv,@function
#	.ent	_ZNK10PointLight8PositionEv # @_ZNK10PointLight8PositionEv
_ZNK10PointLight8PositionEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZNK10PointLight8PositionEv
$0tmp49:
#	.size	_ZNK10PointLight8PositionEv, ($tmp49)-_ZNK10PointLight8PositionEv

#	.globl	_ZNK10PointLight9IntensityEv
#	.align	2
#	.type	_ZNK10PointLight9IntensityEv,@function
#	.ent	_ZNK10PointLight9IntensityEv # @_ZNK10PointLight9IntensityEv
_ZNK10PointLight9IntensityEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADDI	r3,	r5,	12
#	.end	_ZNK10PointLight9IntensityEv
$0tmp50:
#	.size	_ZNK10PointLight9IntensityEv, ($tmp50)-_ZNK10PointLight9IntensityEv

#	.globl	_ZNK10PointLight9DirectionERK8syVector
#	.align	2
#	.type	_ZNK10PointLight9DirectionERK8syVector,@function
#	.ent	_ZNK10PointLight9DirectionERK8syVector # @_ZNK10PointLight9DirectionERK8syVector
_ZNK10PointLight9DirectionERK8syVector:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPRSUB	r4,	r3,	r4
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPRSUB	r4,	r8,	r6
	SWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK10PointLight9DirectionERK8syVector
$0tmp51:
#	.size	_ZNK10PointLight9DirectionERK8syVector, ($tmp51)-_ZNK10PointLight9DirectionERK8syVector

#	.globl	_ZN3RayC2Ev
#	.align	2
#	.type	_ZN3RayC2Ev,@function
#	.ent	_ZN3RayC2Ev             # @_ZN3RayC2Ev
_ZN3RayC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN3RayC2Ev
$0tmp52:
#	.size	_ZN3RayC2Ev, ($tmp52)-_ZN3RayC2Ev

#	.globl	_ZN3RayC2E8syVectorS0_
#	.align	2
#	.type	_ZN3RayC2E8syVectorS0_,@function
#	.ent	_ZN3RayC2E8syVectorS0_  # @_ZN3RayC2E8syVectorS0_
_ZN3RayC2E8syVectorS0_:
#	.frame	r1,4,r15
#	.mask	0x100000
	ADDI	r1,	r1,	-4
	SWI	r20,	r1,	0
	LWI	r3,	r7,	8
	LWI	r4,	r7,	4
	LWI	r7,	r7,	0
	LWI	r8,	r6,	8
	LWI	r9,	r6,	4
	LWI	r6,	r6,	0
	SWI	r6,	r5,	0
	SWI	r9,	r5,	4
	SWI	r8,	r5,	8
	SWI	r7,	r5,	12
	SWI	r4,	r5,	16
	FPMUL	r4,	r4,	r4
	FPMUL	r6,	r7,	r7
	FPADD	r4,	r6,	r4
	SWI	r3,	r5,	20
	FPMUL	r3,	r3,	r3
	FPADD	r3,	r4,	r3
	FPINVSQRT	r3,	r3
	ORI	r9,	r0,	1065353216
	FPDIV	r3,	r9,	r3
	LWI	r4,	r5,	12
	FPDIV	r11,	r4,	r3
	SWI	r11,	r5,	12
	LWI	r4,	r5,	16
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	16
	LWI	r6,	r5,	20
	FPDIV	r6,	r6,	r3
	FPDIV	r3,	r9,	r4
	FPDIV	r4,	r9,	r6
	ADDI	r10,	r0,	0
	ADDI	r7,	r0,	1
	ORI	r12,	r0,	0
	FPLT	r20,	r4,	r12
	bneid	r20,	$0BB53_2
	ADD	r8,	r7,	r0
	ADD	r8,	r10,	r0
$0BB53_2:
	FPDIV	r11,	r9,	r11
	FPLT	r20,	r3,	r12
	bneid	r20,	$0BB53_4
	ADD	r9,	r7,	r0
	ADD	r9,	r10,	r0
$0BB53_4:
	FPLT	r12,	r11,	r12
	bneid	r12,	$0BB53_6
	NOP
	ADD	r7,	r10,	r0
$0BB53_6:
	SWI	r6,	r5,	20
	SWI	r11,	r5,	24
	SWI	r3,	r5,	28
	SWI	r4,	r5,	32
	SWI	r7,	r5,	36
	SWI	r9,	r5,	40
	SWI	r8,	r5,	44
	LWI	r20,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_ZN3RayC2E8syVectorS0_
$0tmp53:
#	.size	_ZN3RayC2E8syVectorS0_, ($tmp53)-_ZN3RayC2E8syVectorS0_

#	.globl	_ZN3Ray3SetE8syVectorS0_
#	.align	2
#	.type	_ZN3Ray3SetE8syVectorS0_,@function
#	.ent	_ZN3Ray3SetE8syVectorS0_ # @_ZN3Ray3SetE8syVectorS0_
_ZN3Ray3SetE8syVectorS0_:
#	.frame	r1,4,r15
#	.mask	0x100000
	ADDI	r1,	r1,	-4
	SWI	r20,	r1,	0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	SWI	r3,	r5,	12
	LWI	r4,	r7,	4
	SWI	r4,	r5,	16
	FPMUL	r3,	r3,	r3
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	LWI	r4,	r7,	8
	SWI	r4,	r5,	20
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r9,	r0,	1065353216
	FPDIV	r3,	r9,	r3
	LWI	r4,	r5,	12
	FPDIV	r11,	r4,	r3
	SWI	r11,	r5,	12
	LWI	r4,	r5,	16
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	16
	LWI	r6,	r5,	20
	FPDIV	r6,	r6,	r3
	FPDIV	r3,	r9,	r4
	FPDIV	r4,	r9,	r6
	ADDI	r10,	r0,	0
	ADDI	r7,	r0,	1
	ORI	r12,	r0,	0
	FPLT	r20,	r4,	r12
	bneid	r20,	$0BB54_2
	ADD	r8,	r7,	r0
	ADD	r8,	r10,	r0
$0BB54_2:
	FPDIV	r11,	r9,	r11
	FPLT	r20,	r3,	r12
	bneid	r20,	$0BB54_4
	ADD	r9,	r7,	r0
	ADD	r9,	r10,	r0
$0BB54_4:
	FPLT	r12,	r11,	r12
	bneid	r12,	$0BB54_6
	NOP
	ADD	r7,	r10,	r0
$0BB54_6:
	SWI	r6,	r5,	20
	SWI	r11,	r5,	24
	SWI	r3,	r5,	28
	SWI	r4,	r5,	32
	SWI	r7,	r5,	36
	SWI	r9,	r5,	40
	SWI	r8,	r5,	44
	LWI	r20,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_ZN3Ray3SetE8syVectorS0_
$0tmp54:
#	.size	_ZN3Ray3SetE8syVectorS0_, ($tmp54)-_ZN3Ray3SetE8syVectorS0_

#	.globl	_ZN3RayC2ERKS_
#	.align	2
#	.type	_ZN3RayC2ERKS_,@function
#	.ent	_ZN3RayC2ERKS_          # @_ZN3RayC2ERKS_
_ZN3RayC2ERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r6,	12
	SWI	r3,	r5,	12
	LWI	r3,	r6,	16
	SWI	r3,	r5,	16
	LWI	r3,	r6,	20
	SWI	r3,	r5,	20
	LWI	r3,	r6,	24
	SWI	r3,	r5,	24
	LWI	r3,	r6,	28
	SWI	r3,	r5,	28
	LWI	r3,	r6,	32
	SWI	r3,	r5,	32
	LWI	r3,	r6,	36
	SWI	r3,	r5,	36
	LWI	r3,	r6,	40
	SWI	r3,	r5,	40
	LWI	r3,	r6,	44
	rtsd	r15,	8
	SWI	r3,	r5,	44
#	.end	_ZN3RayC2ERKS_
$0tmp55:
#	.size	_ZN3RayC2ERKS_, ($tmp55)-_ZN3RayC2ERKS_

#	.globl	_ZN3RayaSERKS_
#	.align	2
#	.type	_ZN3RayaSERKS_,@function
#	.ent	_ZN3RayaSERKS_          # @_ZN3RayaSERKS_
_ZN3RayaSERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r6,	12
	SWI	r3,	r5,	12
	LWI	r3,	r6,	16
	SWI	r3,	r5,	16
	LWI	r3,	r6,	20
	SWI	r3,	r5,	20
	LWI	r3,	r6,	24
	SWI	r3,	r5,	24
	LWI	r3,	r6,	28
	SWI	r3,	r5,	28
	LWI	r3,	r6,	32
	SWI	r3,	r5,	32
	LWI	r3,	r6,	36
	SWI	r3,	r5,	36
	LWI	r3,	r6,	40
	SWI	r3,	r5,	40
	LWI	r3,	r6,	44
	SWI	r3,	r5,	44
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN3RayaSERKS_
$0tmp56:
#	.size	_ZN3RayaSERKS_, ($tmp56)-_ZN3RayaSERKS_

#	.globl	_ZN3Ray9NormalizeEv
#	.align	2
#	.type	_ZN3Ray9NormalizeEv,@function
#	.ent	_ZN3Ray9NormalizeEv     # @_ZN3Ray9NormalizeEv
_ZN3Ray9NormalizeEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	16
	FPMUL	r3,	r3,	r3
	LWI	r4,	r5,	12
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r5,	20
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	LWI	r4,	r5,	12
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	12
	LWI	r4,	r5,	16
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	16
	LWI	r4,	r5,	20
	FPDIV	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	20
#	.end	_ZN3Ray9NormalizeEv
$0tmp57:
#	.size	_ZN3Ray9NormalizeEv, ($tmp57)-_ZN3Ray9NormalizeEv

#	.globl	_Z9trax_mainv
#	.align	2
#	.type	_Z9trax_mainv,@function
#	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
#	.frame	r1,824,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-824
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	ADD	r8,	r0,	r0
	LOAD	r12,	r8,	1
	SWI	r12,	r1,	812
	FPCONV	r5,	r12
	ORI	r6,	r0,	1065353216
	LOAD	r3,	r8,	4
	FPCONV	r7,	r3
	LOAD	r11,	r8,	7
	SWI	r11,	r1,	808
	LOAD	r4,	r8,	12
	LOAD	r9,	r4,	0
	SWI	r9,	r1,	696
	LOAD	r9,	r4,	1
	SWI	r9,	r1,	700
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	704
	LOAD	r4,	r8,	28
	LOAD	r4,	r8,	29
	ADDI	r9,	r0,	17
	LOAD	r4,	r8,	9
	LOAD	r10,	r8,	8
	SWI	r10,	r1,	588
	LOAD	r20,	r9,	0
	SWI	r20,	r1,	712
	ADDI	r9,	r0,	16
	LOAD	r9,	r9,	0
	SWI	r9,	r1,	708
	LOAD	r8,	r8,	10
	LOAD	r9,	r8,	0
	SWI	r9,	r1,	736
	LOAD	r9,	r8,	1
	SWI	r9,	r1,	740
	LOAD	r9,	r8,	2
	SWI	r9,	r1,	744
	ADDI	r9,	r8,	9
	LOAD	r10,	r9,	0
	LOAD	r10,	r9,	1
	LOAD	r9,	r9,	2
	ADD	r9,	r11,	r0
	MUL	r11,	r3,	r12
	ADDI	r3,	r8,	12
	LOAD	r10,	r3,	0
	SWI	r10,	r1,	748
	LOAD	r10,	r3,	1
	SWI	r10,	r1,	752
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	756
	ADDI	r3,	r8,	15
	LOAD	r10,	r3,	0
	SWI	r10,	r1,	760
	LOAD	r10,	r3,	1
	SWI	r10,	r1,	764
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	768
	ADDI	r3,	r8,	18
	LOAD	r8,	r3,	0
	SWI	r8,	r1,	772
	LOAD	r8,	r3,	1
	SWI	r8,	r1,	776
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	780
	ATOMIC_INC	r3,	0
	CMP	r8,	r11,	r3
	bgeid	r8,	$0BB58_757
	NOP
	FPDIV	r5,	r6,	r5
	SWI	r5,	r1,	784
	FPDIV	r5,	r6,	r7
	SWI	r5,	r1,	788
	ADDI	r5,	r0,	1
	CMP	r6,	r5,	r20
	FPCONV	r7,	r20
	bneid	r6,	$0BB58_12
	SWI	r7,	r1,	800
	ADDI	r4,	r4,	4
	SWI	r4,	r1,	712
	ADD	r9,	r11,	r0
	SWI	r9,	r1,	804
$0BB58_3:
	LWI	r4,	r1,	812
	DIV	r5,	r4,	r3
	MUL	r4,	r5,	r4
	SWI	r4,	r1,	716
	RSUB	r3,	r4,	r3
	SWI	r3,	r1,	720
	FPCONV	r3,	r3
	LWI	r4,	r1,	784
	FPMUL	r3,	r3,	r4
	FPADD	r4,	r3,	r3
	ORI	r3,	r0,	-1082130432
	FPADD	r4,	r4,	r3
	LWI	r6,	r1,	760
	FPMUL	r6,	r6,	r4
	LWI	r7,	r1,	748
	FPADD	r7,	r7,	r6
	FPCONV	r5,	r5
	LWI	r6,	r1,	788
	FPMUL	r5,	r5,	r6
	FPADD	r5,	r5,	r5
	FPADD	r6,	r5,	r3
	LWI	r3,	r1,	772
	FPMUL	r3,	r3,	r6
	FPADD	r3,	r7,	r3
	LWI	r5,	r1,	764
	FPMUL	r5,	r5,	r4
	LWI	r7,	r1,	752
	FPADD	r5,	r7,	r5
	LWI	r7,	r1,	776
	FPMUL	r7,	r7,	r6
	FPADD	r5,	r5,	r7
	FPMUL	r7,	r5,	r5
	FPMUL	r8,	r3,	r3
	FPADD	r7,	r8,	r7
	LWI	r8,	r1,	780
	FPMUL	r6,	r8,	r6
	LWI	r8,	r1,	768
	FPMUL	r4,	r8,	r4
	LWI	r8,	r1,	756
	FPADD	r4,	r8,	r4
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r24,	r0,	1065353216
	FPDIV	r6,	r24,	r6
	FPDIV	r8,	r4,	r6
	FPDIV	r4,	r5,	r6
	FPDIV	r5,	r3,	r6
	ADD	r25,	r0,	r0
	ORI	r22,	r0,	0
	ADD	r21,	r22,	r0
	ADD	r23,	r22,	r0
	LWI	r3,	r1,	744
	SWI	r3,	r1,	560
	LWI	r3,	r1,	740
	SWI	r3,	r1,	564
	LWI	r3,	r1,	736
	SWI	r3,	r1,	568
	ADD	r27,	r24,	r0
	brid	$0BB58_4
	ADD	r26,	r24,	r0
$0BB58_397:
	FPMUL	r11,	r28,	r9
	FPMUL	r12,	r29,	r10
	FPRSUB	r12,	r11,	r12
	FPMUL	r9,	r30,	r9
	FPMUL	r11,	r29,	r20
	FPRSUB	r9,	r11,	r9
	FPMUL	r10,	r30,	r10
	FPMUL	r11,	r28,	r20
	FPRSUB	r20,	r10,	r11
	FPMUL	r10,	r28,	r20
	FPMUL	r11,	r29,	r9
	FPRSUB	r10,	r10,	r11
	FPMUL	r11,	r30,	r9
	FPMUL	r21,	r28,	r12
	FPRSUB	r11,	r11,	r21
	FPMUL	r11,	r11,	r6
	FPMUL	r21,	r20,	r7
	FPADD	r11,	r21,	r11
	FPMUL	r10,	r10,	r6
	FPMUL	r21,	r12,	r7
	FPADD	r10,	r21,	r10
	FPMUL	r21,	r30,	r8
	FPADD	r10,	r10,	r21
	FPMUL	r21,	r29,	r8
	FPADD	r11,	r11,	r21
	FPMUL	r12,	r29,	r12
	FPMUL	r20,	r30,	r20
	FPMUL	r3,	r3,	r24
	LWI	r21,	r1,	644
	FPMUL	r24,	r24,	r21
	FPMUL	r4,	r4,	r27
	LWI	r21,	r1,	640
	FPMUL	r27,	r27,	r21
	FPMUL	r5,	r5,	r26
	LWI	r21,	r1,	636
	FPMUL	r26,	r26,	r21
	FPADD	r22,	r22,	r3
	LWI	r21,	r1,	656
	FPADD	r21,	r21,	r4
	FPADD	r23,	r23,	r5
	FPMUL	r3,	r11,	r11
	FPMUL	r4,	r10,	r10
	FPADD	r3,	r4,	r3
	FPRSUB	r4,	r12,	r20
	FPMUL	r4,	r4,	r6
	FPMUL	r5,	r9,	r7
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r28,	r8
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r4,	r4
	FPADD	r3,	r3,	r5
	FPINVSQRT	r3,	r3
	ORI	r5,	r0,	1065353216
	FPDIV	r6,	r5,	r3
	FPDIV	r3,	r10,	r6
	FPDIV	r7,	r11,	r6
	FPMUL	r8,	r7,	r7
	FPMUL	r9,	r3,	r3
	FPADD	r8,	r9,	r8
	FPDIV	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPADD	r6,	r8,	r6
	FPINVSQRT	r6,	r6
	FPDIV	r5,	r5,	r6
	FPDIV	r8,	r4,	r5
	FPDIV	r4,	r7,	r5
	FPDIV	r5,	r3,	r5
$0BB58_4:
	SWI	r5,	r1,	576
	ORI	r3,	r0,	1065353216
	FPDIV	r7,	r3,	r8
	SWI	r7,	r1,	600
	FPDIV	r9,	r3,	r4
	SWI	r9,	r1,	604
	ADD	r6,	r4,	r0
	FPDIV	r3,	r3,	r5
	SWI	r3,	r1,	608
	ORI	r4,	r0,	0
	FPLT	r3,	r3,	r4
	FPLT	r5,	r9,	r4
	FPLT	r9,	r7,	r4
	ADDI	r4,	r0,	0
	ADDI	r7,	r0,	1
	SWI	r7,	r1,	616
	bneid	r9,	$0BB58_6
	SWI	r7,	r1,	620
	SWI	r4,	r1,	620
$0BB58_6:
	SWI	r23,	r1,	652
	SWI	r22,	r1,	648
	LWI	r7,	r1,	616
	bneid	r5,	$0BB58_8
	SWI	r7,	r1,	624
	SWI	r4,	r1,	624
$0BB58_8:
	bneid	r3,	$0BB58_10
	NOP
	SWI	r4,	r1,	616
$0BB58_10:
	LWI	r3,	r1,	708
	CMP	r3,	r3,	r25
	bgeid	r3,	$0BB58_11
	NOP
	SWI	r27,	r1,	668
	SWI	r26,	r1,	664
	SWI	r24,	r1,	660
	SWI	r21,	r1,	656
	ADDI	r25,	r25,	1
	SWI	r25,	r1,	672
	ADDI	r9,	r0,	1
	ORI	r3,	r0,	1203982336
	SWI	r3,	r1,	572
	ADDI	r4,	r0,	-1
	SWI	r4,	r1,	596
	LWI	r3,	r1,	620
	XORI	r3,	r3,	1
	SWI	r3,	r1,	628
	LWI	r3,	r1,	624
	XORI	r3,	r3,	1
	SWI	r3,	r1,	632
	LWI	r3,	r1,	616
	XORI	r3,	r3,	1
	SWI	r3,	r1,	636
	SWI	r0,	r1,	336
	SWI	r4,	r1,	592
	brid	$0BB58_41
	ADD	r26,	r6,	r0
$0BB58_146:
	beqid	r4,	$0BB58_147
	NOP
	FPGT	r11,	r5,	r21
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r11,	$0BB58_150
	ADD	r10,	r3,	r0
	ADD	r10,	r4,	r0
$0BB58_150:
	bneid	r10,	$0BB58_152
	NOP
	ADD	r5,	r21,	r0
$0BB58_152:
	FPGE	r6,	r12,	r5
	FPUN	r5,	r12,	r5
	BITOR	r5,	r5,	r6
	bneid	r5,	$0BB58_154
	NOP
	ADD	r3,	r4,	r0
$0BB58_154:
	bneid	r3,	$0BB58_156
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	SWI	r29,	r5,	-4
	LWI	r5,	r1,	580
	SW	r5,	r4,	r3
	brid	$0BB58_182
	ADDI	r9,	r9,	1
$0BB58_147:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	brid	$0BB58_182
	SWI	r29,	r3,	-4
$0BB58_156:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	LWI	r6,	r1,	580
	SWI	r6,	r5,	-4
	SW	r29,	r4,	r3
	brid	$0BB58_182
	ADDI	r9,	r9,	1
$0BB58_41:
	ADDI	r3,	r0,	-1
	ADD	r30,	r9,	r3
	bslli	r3,	r30,	2
	ADDI	r4,	r1,	336
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r4,	r3,	r4
	SWI	r0,	r1,	56
	SWI	r0,	r1,	52
	SWI	r0,	r1,	48
	LOAD	r3,	r4,	0
	SWI	r3,	r1,	48
	LOAD	r3,	r4,	1
	SWI	r3,	r1,	52
	LOAD	r3,	r4,	2
	SWI	r3,	r1,	56
	ADDI	r3,	r4,	3
	LOAD	r5,	r3,	0
	SWI	r5,	r1,	60
	LOAD	r5,	r3,	1
	SWI	r5,	r1,	64
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	68
	LWI	r3,	r1,	632
	MULI	r27,	r3,	12
	ADDI	r3,	r1,	48
	ADD	r5,	r3,	r27
	ADDI	r10,	r4,	7
	ADDI	r4,	r4,	6
	LWI	r6,	r1,	616
	MULI	r23,	r6,	12
	LOAD	r31,	r4,	0
	SWI	r31,	r1,	72
	LOAD	r4,	r10,	0
	SWI	r4,	r1,	580
	SWI	r4,	r1,	76
	LWI	r5,	r5,	4
	LWI	r6,	r1,	564
	FPRSUB	r5,	r6,	r5
	LWI	r4,	r1,	604
	FPMUL	r29,	r5,	r4
	LW	r5,	r3,	r23
	LWI	r6,	r1,	568
	FPRSUB	r5,	r6,	r5
	LWI	r4,	r1,	608
	FPMUL	r10,	r5,	r4
	ADDI	r5,	r0,	1
	FPGT	r6,	r10,	r29
	bneid	r6,	$0BB58_43
	NOP
	ADDI	r5,	r0,	0
$0BB58_43:
	bneid	r5,	$0BB58_44
	NOP
	LWI	r4,	r1,	624
	MULI	r21,	r4,	12
	ADD	r5,	r3,	r21
	LWI	r5,	r5,	4
	LWI	r6,	r1,	564
	FPRSUB	r5,	r6,	r5
	LWI	r4,	r1,	604
	FPMUL	r12,	r5,	r4
	LWI	r4,	r1,	636
	MULI	r5,	r4,	12
	LW	r3,	r3,	r5
	LWI	r6,	r1,	568
	FPRSUB	r3,	r6,	r3
	LWI	r4,	r1,	608
	FPMUL	r3,	r3,	r4
	FPGT	r6,	r12,	r3
	bneid	r6,	$0BB58_47
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_47:
	bneid	r11,	$0BB58_48
	NOP
	FPGT	r24,	r12,	r10
	ADDI	r20,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r24,	$0BB58_51
	ADD	r22,	r11,	r0
	ADD	r22,	r20,	r0
$0BB58_51:
	bneid	r22,	$0BB58_53
	NOP
	ADD	r12,	r10,	r0
$0BB58_53:
	LWI	r4,	r1,	628
	MULI	r25,	r4,	12
	ADDI	r28,	r1,	48
	ADD	r10,	r28,	r25
	FPLT	r6,	r29,	r3
	bneid	r6,	$0BB58_55
	ADD	r22,	r11,	r0
	ADD	r22,	r20,	r0
$0BB58_55:
	LWI	r6,	r10,	8
	LWI	r7,	r1,	560
	FPRSUB	r6,	r7,	r6
	LWI	r4,	r1,	600
	FPMUL	r10,	r6,	r4
	FPGT	r6,	r12,	r10
	bneid	r6,	$0BB58_57
	NOP
	ADD	r11,	r20,	r0
$0BB58_57:
	bneid	r22,	$0BB58_59
	NOP
	ADD	r29,	r3,	r0
$0BB58_59:
	bneid	r11,	$0BB58_60
	NOP
	LWI	r3,	r1,	620
	MULI	r22,	r3,	12
	ADD	r3,	r28,	r22
	LWI	r3,	r3,	8
	LWI	r6,	r1,	560
	FPRSUB	r3,	r6,	r3
	LWI	r4,	r1,	600
	FPMUL	r3,	r3,	r4
	FPGT	r6,	r3,	r29
	bneid	r6,	$0BB58_63
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_63:
	bneid	r11,	$0BB58_64
	NOP
	ADD	r4,	r26,	r0
	FPLT	r26,	r10,	r29
	ADDI	r20,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r26,	$0BB58_67
	ADD	r24,	r11,	r0
	ADD	r24,	r20,	r0
$0BB58_67:
	bneid	r24,	$0BB58_69
	ADD	r26,	r4,	r0
	ADD	r10,	r29,	r0
$0BB58_69:
	ORI	r6,	r0,	0
	FPLT	r6,	r10,	r6
	bneid	r6,	$0BB58_71
	NOP
	ADD	r11,	r20,	r0
$0BB58_71:
	bneid	r11,	$0BB58_72
	NOP
	FPGT	r24,	r3,	r12
	ADDI	r11,	r0,	0
	ADDI	r10,	r0,	1
	bneid	r24,	$0BB58_75
	ADD	r20,	r10,	r0
	ADD	r20,	r11,	r0
$0BB58_75:
	bneid	r20,	$0BB58_77
	NOP
	ADD	r3,	r12,	r0
$0BB58_77:
	LWI	r4,	r1,	572
	FPLT	r3,	r4,	r3
	bneid	r3,	$0BB58_79
	NOP
	ADD	r10,	r11,	r0
$0BB58_79:
	bneid	r10,	$0BB58_80
	NOP
	beqid	r31,	$0BB58_82
	NOP
	SWI	r30,	r1,	612
	ADD	r12,	r0,	r0
	SWI	r31,	r1,	584
	ADDI	r3,	r0,	-1
	CMP	r3,	r3,	r31
	bneid	r3,	$0BB58_157
	NOP
	LWI	r3,	r1,	580
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	88
	SWI	r0,	r1,	84
	SWI	r0,	r1,	80
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	80
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	84
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	88
	ADDI	r4,	r3,	3
	LOAD	r6,	r4,	0
	SWI	r6,	r1,	92
	LOAD	r6,	r4,	1
	SWI	r6,	r1,	96
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	100
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	104
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	108
	ADDI	r10,	r1,	80
	ADD	r3,	r10,	r27
	LWI	r3,	r3,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	604
	FPMUL	r30,	r3,	r4
	LW	r3,	r10,	r23
	LWI	r4,	r1,	568
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	608
	FPMUL	r3,	r3,	r4
	ADDI	r11,	r0,	1
	FPGT	r4,	r3,	r30
	bneid	r4,	$0BB58_86
	NOP
	ADDI	r11,	r0,	0
$0BB58_86:
	ADD	r4,	r0,	r0
	ORI	r12,	r0,	1203982336
	bneid	r11,	$0BB58_116
	NOP
	ADD	r6,	r10,	r21
	LW	r10,	r10,	r5
	LWI	r6,	r6,	4
	LWI	r7,	r1,	564
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	604
	FPMUL	r29,	r6,	r7
	LWI	r6,	r1,	568
	FPRSUB	r6,	r6,	r10
	LWI	r7,	r1,	608
	FPMUL	r10,	r6,	r7
	FPGT	r6,	r29,	r10
	bneid	r6,	$0BB58_89
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_89:
	bneid	r11,	$0BB58_116
	NOP
	FPGT	r20,	r29,	r3
	ADDI	r4,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB58_92
	ADD	r12,	r11,	r0
	ADD	r12,	r4,	r0
$0BB58_92:
	bneid	r12,	$0BB58_94
	NOP
	ADD	r29,	r3,	r0
$0BB58_94:
	ADDI	r20,	r1,	80
	ADD	r3,	r20,	r25
	FPLT	r6,	r30,	r10
	bneid	r6,	$0BB58_96
	ADD	r12,	r11,	r0
	ADD	r12,	r4,	r0
$0BB58_96:
	LWI	r3,	r3,	8
	LWI	r6,	r1,	560
	FPRSUB	r3,	r6,	r3
	LWI	r6,	r1,	600
	FPMUL	r3,	r3,	r6
	FPGT	r6,	r29,	r3
	bneid	r6,	$0BB58_98
	NOP
	ADD	r11,	r4,	r0
$0BB58_98:
	bneid	r12,	$0BB58_100
	NOP
	ADD	r30,	r10,	r0
$0BB58_100:
	ADD	r4,	r0,	r0
	ORI	r12,	r0,	1203982336
	bneid	r11,	$0BB58_116
	NOP
	ADD	r6,	r20,	r22
	LWI	r6,	r6,	8
	LWI	r7,	r1,	560
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	600
	FPMUL	r10,	r6,	r7
	FPGT	r6,	r10,	r30
	bneid	r6,	$0BB58_103
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_103:
	bneid	r11,	$0BB58_116
	NOP
	FPLT	r20,	r3,	r30
	ADDI	r4,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB58_106
	ADD	r12,	r11,	r0
	ADD	r12,	r4,	r0
$0BB58_106:
	bneid	r12,	$0BB58_108
	NOP
	ADD	r3,	r30,	r0
$0BB58_108:
	ORI	r6,	r0,	0
	FPLT	r3,	r3,	r6
	bneid	r3,	$0BB58_110
	NOP
	ADD	r11,	r4,	r0
$0BB58_110:
	ADD	r4,	r0,	r0
	ORI	r12,	r0,	1203982336
	bneid	r11,	$0BB58_116
	NOP
	FPGT	r6,	r10,	r29
	ADDI	r4,	r0,	1
	bneid	r6,	$0BB58_113
	ADD	r3,	r4,	r0
	ADDI	r3,	r0,	0
$0BB58_113:
	bneid	r3,	$0BB58_115
	NOP
	ADD	r10,	r29,	r0
$0BB58_115:
	ADD	r12,	r10,	r0
$0BB58_116:
	LWI	r3,	r1,	580
	ADDI	r29,	r3,	1
	bslli	r3,	r29,	3
	LWI	r6,	r1,	588
	ADD	r3,	r3,	r6
	SWI	r0,	r1,	120
	SWI	r0,	r1,	116
	SWI	r0,	r1,	112
	LOAD	r6,	r3,	0
	SWI	r6,	r1,	112
	LOAD	r6,	r3,	1
	SWI	r6,	r1,	116
	LOAD	r6,	r3,	2
	SWI	r6,	r1,	120
	ADDI	r10,	r3,	3
	LOAD	r6,	r10,	0
	SWI	r6,	r1,	124
	LOAD	r6,	r10,	1
	SWI	r6,	r1,	128
	LOAD	r6,	r10,	2
	SWI	r6,	r1,	132
	ADDI	r6,	r3,	6
	LOAD	r6,	r6,	0
	SWI	r6,	r1,	136
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	140
	ADDI	r3,	r1,	112
	ADD	r6,	r3,	r27
	LWI	r6,	r6,	4
	LWI	r7,	r1,	564
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	604
	FPMUL	r27,	r6,	r7
	LW	r6,	r3,	r23
	LWI	r7,	r1,	568
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	608
	FPMUL	r10,	r6,	r7
	ADDI	r11,	r0,	1
	FPGT	r6,	r10,	r27
	bneid	r6,	$0BB58_118
	NOP
	ADDI	r11,	r0,	0
$0BB58_118:
	bneid	r11,	$0BB58_143
	LWI	r24,	r1,	612
	ADD	r6,	r3,	r21
	LWI	r6,	r6,	4
	LWI	r7,	r1,	564
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	604
	FPMUL	r21,	r6,	r7
	LW	r3,	r3,	r5
	LWI	r5,	r1,	568
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	608
	FPMUL	r3,	r3,	r5
	FPGT	r6,	r21,	r3
	bneid	r6,	$0BB58_121
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_121:
	bneid	r5,	$0BB58_143
	NOP
	FPGT	r23,	r21,	r10
	ADDI	r11,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r23,	$0BB58_124
	ADD	r20,	r5,	r0
	ADD	r20,	r11,	r0
$0BB58_124:
	bneid	r20,	$0BB58_126
	NOP
	ADD	r21,	r10,	r0
$0BB58_126:
	ADDI	r20,	r1,	112
	ADD	r10,	r20,	r25
	FPLT	r6,	r27,	r3
	bneid	r6,	$0BB58_128
	ADD	r23,	r5,	r0
	ADD	r23,	r11,	r0
$0BB58_128:
	LWI	r6,	r10,	8
	LWI	r7,	r1,	560
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	600
	FPMUL	r10,	r6,	r7
	FPGT	r6,	r21,	r10
	bneid	r6,	$0BB58_130
	NOP
	ADD	r5,	r11,	r0
$0BB58_130:
	bneid	r23,	$0BB58_132
	NOP
	ADD	r27,	r3,	r0
$0BB58_132:
	bneid	r5,	$0BB58_143
	NOP
	ADD	r3,	r20,	r22
	LWI	r3,	r3,	8
	LWI	r5,	r1,	560
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	600
	FPMUL	r5,	r3,	r5
	FPGT	r6,	r5,	r27
	bneid	r6,	$0BB58_135
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_135:
	bneid	r3,	$0BB58_143
	NOP
	FPLT	r22,	r10,	r27
	ADDI	r11,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r22,	$0BB58_138
	ADD	r20,	r3,	r0
	ADD	r20,	r11,	r0
$0BB58_138:
	bneid	r20,	$0BB58_140
	NOP
	ADD	r10,	r27,	r0
$0BB58_140:
	ORI	r6,	r0,	0
	FPGE	r7,	r10,	r6
	FPUN	r6,	r10,	r6
	BITOR	r6,	r6,	r7
	bneid	r6,	$0BB58_142
	NOP
	ADD	r3,	r11,	r0
$0BB58_142:
	bneid	r3,	$0BB58_146
	NOP
$0BB58_143:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r4
	bneid	r3,	$0BB58_144
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	580
	brid	$0BB58_182
	SWI	r4,	r3,	-4
$0BB58_44:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_48:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_60:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_157:
	MULI	r3,	r12,	11
	LWI	r4,	r1,	580
	ADD	r5,	r3,	r4
	LOAD	r25,	r5,	0
	LOAD	r27,	r5,	1
	LOAD	r30,	r5,	2
	ADDI	r3,	r5,	3
	LOAD	r11,	r3,	0
	LOAD	r23,	r3,	1
	LOAD	r3,	r3,	2
	ADDI	r6,	r5,	6
	LOAD	r10,	r6,	0
	LOAD	r7,	r6,	1
	LOAD	r6,	r6,	2
	FPRSUB	r22,	r30,	r6
	FPRSUB	r29,	r27,	r7
	FPMUL	r6,	r8,	r29
	FPMUL	r7,	r26,	r22
	FPRSUB	r9,	r6,	r7
	FPRSUB	r10,	r25,	r10
	FPMUL	r6,	r8,	r10
	LWI	r28,	r1,	576
	FPMUL	r7,	r28,	r22
	FPRSUB	r21,	r7,	r6
	FPRSUB	r20,	r25,	r11
	FPRSUB	r11,	r27,	r23
	FPMUL	r6,	r21,	r11
	FPMUL	r7,	r9,	r20
	FPADD	r24,	r7,	r6
	FPMUL	r6,	r26,	r10
	ADD	r4,	r26,	r0
	FPMUL	r7,	r28,	r29
	FPRSUB	r23,	r6,	r7
	FPRSUB	r28,	r30,	r3
	FPMUL	r3,	r23,	r28
	FPADD	r26,	r24,	r3
	ORI	r3,	r0,	0
	FPGE	r6,	r26,	r3
	FPUN	r3,	r26,	r3
	BITOR	r3,	r3,	r6
	bneid	r3,	$0BB58_159
	ADDI	r24,	r0,	1
	ADDI	r24,	r0,	0
$0BB58_159:
	bneid	r24,	$0BB58_161
	ADD	r3,	r26,	r0
	FPNEG	r3,	r26
$0BB58_161:
	ORI	r6,	r0,	953267991
	FPLT	r6,	r3,	r6
	bneid	r6,	$0BB58_163
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_163:
	bneid	r3,	$0BB58_164
	NOP
	ADD	r31,	r8,	r0
	LWI	r3,	r1,	560
	FPRSUB	r30,	r30,	r3
	LWI	r3,	r1,	568
	FPRSUB	r25,	r25,	r3
	LWI	r3,	r1,	564
	FPRSUB	r27,	r27,	r3
	FPMUL	r3,	r27,	r20
	FPMUL	r24,	r25,	r11
	FPMUL	r20,	r30,	r20
	FPMUL	r6,	r25,	r28
	FPMUL	r7,	r30,	r11
	FPMUL	r8,	r27,	r28
	FPRSUB	r11,	r3,	r24
	FPRSUB	r28,	r6,	r20
	FPRSUB	r20,	r7,	r8
	FPMUL	r3,	r28,	r29
	FPMUL	r6,	r20,	r10
	FPADD	r3,	r6,	r3
	FPMUL	r6,	r11,	r22
	FPADD	r6,	r3,	r6
	ORI	r3,	r0,	1065353216
	FPDIV	r3,	r3,	r26
	FPMUL	r22,	r6,	r3
	ORI	r6,	r0,	0
	FPLT	r6,	r22,	r6
	bneid	r6,	$0BB58_167
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB58_167:
	bneid	r10,	$0BB58_168
	ADD	r26,	r4,	r0
	FPMUL	r6,	r21,	r27
	FPMUL	r7,	r9,	r25
	FPADD	r6,	r7,	r6
	FPMUL	r7,	r28,	r26
	LWI	r4,	r1,	576
	FPMUL	r8,	r20,	r4
	FPMUL	r9,	r23,	r30
	FPADD	r10,	r6,	r9
	FPADD	r6,	r8,	r7
	ADD	r8,	r31,	r0
	FPMUL	r7,	r11,	r8
	FPADD	r6,	r6,	r7
	FPMUL	r9,	r6,	r3
	FPMUL	r3,	r10,	r3
	FPADD	r6,	r3,	r9
	ORI	r7,	r0,	1065353216
	FPLE	r21,	r6,	r7
	ORI	r6,	r0,	0
	FPGE	r3,	r3,	r6
	FPGE	r20,	r9,	r6
	LWI	r4,	r1,	572
	FPGE	r6,	r22,	r4
	FPUN	r7,	r22,	r4
	BITOR	r23,	r7,	r6
	ADDI	r11,	r0,	0
	ADDI	r10,	r0,	1
	bneid	r21,	$0BB58_171
	ADD	r9,	r10,	r0
	ADD	r9,	r11,	r0
$0BB58_171:
	bneid	r23,	$0BB58_173
	ADD	r21,	r10,	r0
	ADD	r21,	r11,	r0
$0BB58_173:
	bneid	r20,	$0BB58_175
	ADD	r23,	r10,	r0
	ADD	r23,	r11,	r0
$0BB58_175:
	bneid	r3,	$0BB58_177
	NOP
	ADD	r10,	r11,	r0
$0BB58_177:
	bneid	r21,	$0BB58_180
	NOP
	BITAND	r3,	r10,	r23
	BITAND	r3,	r3,	r9
	ADDI	r6,	r0,	1
	CMP	r3,	r6,	r3
	bneid	r3,	$0BB58_180
	NOP
	ADDI	r3,	r5,	10
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	592
	SWI	r5,	r1,	596
	brid	$0BB58_180
	SWI	r22,	r1,	572
$0BB58_164:
	brid	$0BB58_180
	ADD	r26,	r4,	r0
$0BB58_168:
	ADD	r8,	r31,	r0
$0BB58_180:
	ADDI	r12,	r12,	1
	LWI	r3,	r1,	584
	CMP	r3,	r3,	r12
	bneid	r3,	$0BB58_157
	NOP
	brid	$0BB58_182
	LWI	r9,	r1,	612
$0BB58_64:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_72:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_80:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_82:
	brid	$0BB58_182
	ADD	r9,	r30,	r0
$0BB58_144:
	ADD	r9,	r24,	r0
$0BB58_182:
	bgtid	r9,	$0BB58_41
	NOP
	ORI	r3,	r0,	1203982336
	LWI	r4,	r1,	572
	FPNE	r4,	r4,	r3
	bneid	r4,	$0BB58_185
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_185:
	beqid	r3,	$0BB58_186
	NOP
	LWI	r7,	r1,	596
	LOAD	r3,	r7,	0
	LOAD	r4,	r7,	1
	LOAD	r9,	r7,	2
	ADDI	r6,	r7,	3
	LOAD	r5,	r6,	0
	LOAD	r10,	r6,	1
	LOAD	r20,	r6,	2
	ADDI	r6,	r7,	6
	LOAD	r11,	r6,	0
	LOAD	r21,	r6,	1
	LOAD	r6,	r6,	2
	FPRSUB	r12,	r6,	r9
	FPRSUB	r20,	r20,	r9
	FPRSUB	r9,	r10,	r4
	FPRSUB	r10,	r21,	r4
	FPMUL	r4,	r20,	r10
	FPMUL	r6,	r9,	r12
	FPRSUB	r4,	r4,	r6
	FPRSUB	r5,	r5,	r3
	FPRSUB	r11,	r11,	r3
	FPMUL	r3,	r20,	r11
	FPMUL	r6,	r5,	r12
	FPRSUB	r3,	r6,	r3
	FPMUL	r6,	r3,	r3
	FPMUL	r7,	r4,	r4
	FPADD	r12,	r7,	r6
	FPMUL	r6,	r9,	r11
	FPMUL	r5,	r5,	r10
	FPRSUB	r5,	r6,	r5
	FPMUL	r6,	r5,	r5
	FPADD	r6,	r12,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r9,	r7,	r6
	FPDIV	r3,	r3,	r9
	SWI	r3,	r1,	632
	FPMUL	r3,	r3,	r26
	FPDIV	r22,	r4,	r9
	LWI	r4,	r1,	576
	FPMUL	r4,	r22,	r4
	FPADD	r3,	r4,	r3
	FPDIV	r4,	r5,	r9
	SWI	r4,	r1,	628
	FPMUL	r4,	r4,	r8
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB58_211
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_211:
	bneid	r3,	$0BB58_213
	NOP
	LWI	r3,	r1,	628
	FPNEG	r3,	r3
	SWI	r3,	r1,	628
	LWI	r3,	r1,	632
	FPNEG	r3,	r3
	SWI	r3,	r1,	632
	FPNEG	r22,	r22
$0BB58_213:
	LWI	r5,	r1,	576
	LWI	r10,	r1,	572
	FPMUL	r3,	r26,	r10
	LWI	r4,	r1,	564
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r5,	r10
	LWI	r5,	r1,	568
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	696
	FPRSUB	r5,	r4,	r5
	LWI	r6,	r1,	700
	FPRSUB	r9,	r3,	r6
	FPMUL	r6,	r9,	r9
	FPMUL	r7,	r5,	r5
	FPADD	r7,	r7,	r6
	FPMUL	r6,	r8,	r10
	LWI	r8,	r1,	560
	FPADD	r6,	r8,	r6
	LWI	r8,	r1,	704
	FPRSUB	r10,	r6,	r8
	FPMUL	r8,	r10,	r10
	FPADD	r8,	r7,	r8
	LWI	r7,	r1,	592
	MULI	r7,	r7,	25
	LWI	r11,	r1,	712
	ADD	r11,	r11,	r7
	ORI	r7,	r0,	1028443341
	LOAD	r12,	r11,	0
	SWI	r12,	r1,	636
	LOAD	r12,	r11,	1
	SWI	r12,	r1,	640
	LOAD	r11,	r11,	2
	SWI	r11,	r1,	644
	FPINVSQRT	r11,	r8
	SWI	r11,	r1,	692
	FPINVSQRT	r11,	r8
	ORI	r8,	r0,	1065353216
	FPDIV	r11,	r8,	r11
	FPDIV	r12,	r5,	r11
	SWI	r12,	r1,	680
	FPDIV	r21,	r9,	r11
	SWI	r21,	r1,	684
	FPMUL	r5,	r21,	r21
	FPMUL	r9,	r12,	r12
	FPADD	r5,	r9,	r5
	FPDIV	r23,	r10,	r11
	SWI	r23,	r1,	688
	FPMUL	r9,	r23,	r23
	FPADD	r5,	r5,	r9
	FPINVSQRT	r5,	r5
	FPDIV	r10,	r8,	r5
	ORI	r9,	r0,	0
	FPDIV	r25,	r12,	r10
	FPDIV	r27,	r8,	r25
	SWI	r27,	r1,	620
	FPGE	r11,	r27,	r9
	FPUN	r20,	r27,	r9
	FPDIV	r5,	r21,	r10
	SWI	r5,	r1,	572
	FPDIV	r24,	r8,	r5
	FPGE	r5,	r24,	r9
	FPUN	r12,	r24,	r9
	BITOR	r21,	r12,	r5
	FPDIV	r5,	r23,	r10
	SWI	r5,	r1,	576
	FPDIV	r12,	r8,	r5
	SWI	r12,	r1,	584
	FPGE	r5,	r12,	r9
	FPUN	r8,	r12,	r9
	BITOR	r23,	r8,	r5
	FPLT	r8,	r27,	r9
	FPLT	r10,	r24,	r9
	FPLT	r12,	r12,	r9
	ADDI	r9,	r0,	0
	ADDI	r31,	r0,	1
	bneid	r23,	$0BB58_215
	SWI	r31,	r1,	596
	SWI	r9,	r1,	596
$0BB58_215:
	ADD	r5,	r22,	r0
	BITOR	r11,	r20,	r11
	bneid	r21,	$0BB58_217
	SWI	r31,	r1,	600
	SWI	r9,	r1,	600
$0BB58_217:
	bneid	r11,	$0BB58_219
	SWI	r31,	r1,	604
	SWI	r9,	r1,	604
$0BB58_219:
	bneid	r12,	$0BB58_221
	SWI	r31,	r1,	608
	SWI	r9,	r1,	608
$0BB58_221:
	SWI	r31,	r1,	612
	ADD	r22,	r24,	r0
	bneid	r10,	$0BB58_223
	SWI	r22,	r1,	624
	SWI	r9,	r1,	612
$0BB58_223:
	FPMUL	r10,	r5,	r7
	SWI	r5,	r1,	676
	LWI	r5,	r1,	632
	FPMUL	r11,	r5,	r7
	LWI	r5,	r1,	628
	FPMUL	r7,	r5,	r7
	bneid	r8,	$0BB58_225
	SWI	r31,	r1,	616
	SWI	r9,	r1,	616
$0BB58_225:
	FPADD	r5,	r6,	r7
	SWI	r5,	r1,	560
	FPADD	r3,	r3,	r11
	SWI	r3,	r1,	564
	FPADD	r3,	r4,	r10
	SWI	r3,	r1,	568
	ORI	r10,	r0,	1203982336
	brid	$0BB58_226
	SWI	r0,	r1,	336
$0BB58_355:
	beqid	r12,	$0BB58_356
	NOP
	FPGT	r11,	r6,	r3
	ADDI	r8,	r0,	0
	ADDI	r7,	r0,	1
	bneid	r11,	$0BB58_359
	ADD	r9,	r7,	r0
	ADD	r9,	r8,	r0
$0BB58_359:
	bneid	r9,	$0BB58_361
	NOP
	ADD	r6,	r3,	r0
$0BB58_361:
	FPGE	r3,	r23,	r6
	FPUN	r4,	r23,	r6
	BITOR	r3,	r4,	r3
	bneid	r3,	$0BB58_363
	NOP
	ADD	r7,	r8,	r0
$0BB58_363:
	bneid	r7,	$0BB58_365
	NOP
	bslli	r3,	r31,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	SWI	r30,	r5,	-4
	LWI	r5,	r1,	580
	SW	r5,	r4,	r3
	brid	$0BB58_366
	NOP
$0BB58_356:
	bslli	r3,	r31,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	brid	$0BB58_367
	SWI	r30,	r3,	-4
$0BB58_365:
	bslli	r3,	r31,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	LWI	r6,	r1,	580
	SWI	r6,	r5,	-4
	SW	r30,	r4,	r3
$0BB58_366:
	brid	$0BB58_367
	ADDI	r31,	r31,	1
$0BB58_226:
	ADDI	r3,	r0,	-1
	ADD	r30,	r31,	r3
	bslli	r3,	r30,	2
	ADDI	r4,	r1,	336
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	148
	SWI	r0,	r1,	144
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	144
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	148
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	152
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	156
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	160
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	164
	LWI	r4,	r1,	600
	MULI	r9,	r4,	12
	ADDI	r8,	r1,	144
	ADD	r6,	r8,	r9
	ADDI	r4,	r3,	7
	ADDI	r3,	r3,	6
	LWI	r5,	r1,	616
	MULI	r24,	r5,	12
	LOAD	r12,	r3,	0
	SWI	r12,	r1,	168
	LOAD	r3,	r4,	0
	SWI	r3,	r1,	580
	SWI	r3,	r1,	172
	LWI	r3,	r6,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	FPMUL	r21,	r3,	r22
	LW	r3,	r8,	r24
	LWI	r4,	r1,	568
	FPRSUB	r3,	r4,	r3
	FPMUL	r6,	r3,	r27
	ADDI	r3,	r0,	1
	FPGT	r4,	r6,	r21
	bneid	r4,	$0BB58_228
	NOP
	ADDI	r3,	r0,	0
$0BB58_228:
	bneid	r3,	$0BB58_229
	NOP
	LWI	r3,	r1,	612
	MULI	r3,	r3,	12
	ADD	r4,	r8,	r3
	LWI	r4,	r4,	4
	LWI	r5,	r1,	564
	FPRSUB	r4,	r5,	r4
	FPMUL	r29,	r4,	r22
	LWI	r4,	r1,	604
	MULI	r7,	r4,	12
	LW	r4,	r8,	r7
	LWI	r5,	r1,	568
	FPRSUB	r4,	r5,	r4
	FPMUL	r8,	r4,	r27
	FPGT	r4,	r29,	r8
	bneid	r4,	$0BB58_232
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_232:
	bneid	r11,	$0BB58_233
	NOP
	FPGT	r26,	r29,	r6
	ADDI	r23,	r0,	0
	ADDI	r20,	r0,	1
	bneid	r26,	$0BB58_236
	ADD	r11,	r20,	r0
	ADD	r11,	r23,	r0
$0BB58_236:
	bneid	r11,	$0BB58_238
	NOP
	ADD	r29,	r6,	r0
$0BB58_238:
	LWI	r4,	r1,	596
	MULI	r6,	r4,	12
	ADDI	r26,	r1,	144
	ADD	r11,	r26,	r6
	FPLT	r4,	r21,	r8
	bneid	r4,	$0BB58_240
	ADD	r28,	r20,	r0
	ADD	r28,	r23,	r0
$0BB58_240:
	LWI	r4,	r11,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r11,	r4,	r5
	FPGT	r4,	r29,	r11
	bneid	r4,	$0BB58_242
	NOP
	ADD	r20,	r23,	r0
$0BB58_242:
	bneid	r28,	$0BB58_244
	NOP
	ADD	r21,	r8,	r0
$0BB58_244:
	bneid	r20,	$0BB58_245
	NOP
	LWI	r4,	r1,	608
	MULI	r8,	r4,	12
	ADD	r4,	r26,	r8
	LWI	r4,	r4,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r23,	r4,	r5
	FPGT	r4,	r23,	r21
	bneid	r4,	$0BB58_248
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB58_248:
	bneid	r20,	$0BB58_249
	NOP
	ADD	r5,	r27,	r0
	FPLT	r28,	r11,	r21
	ADDI	r26,	r0,	0
	ADDI	r20,	r0,	1
	bneid	r28,	$0BB58_252
	ADD	r27,	r20,	r0
	ADD	r27,	r26,	r0
$0BB58_252:
	bneid	r27,	$0BB58_254
	NOP
	ADD	r11,	r21,	r0
$0BB58_254:
	ORI	r4,	r0,	0
	FPLT	r4,	r11,	r4
	bneid	r4,	$0BB58_256
	ADD	r27,	r5,	r0
	ADD	r20,	r26,	r0
$0BB58_256:
	bneid	r20,	$0BB58_257
	NOP
	FPGT	r26,	r23,	r29
	ADDI	r20,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r26,	$0BB58_260
	ADD	r21,	r11,	r0
	ADD	r21,	r20,	r0
$0BB58_260:
	bneid	r21,	$0BB58_262
	NOP
	ADD	r23,	r29,	r0
$0BB58_262:
	FPLT	r4,	r10,	r23
	bneid	r4,	$0BB58_264
	NOP
	ADD	r11,	r20,	r0
$0BB58_264:
	bneid	r11,	$0BB58_265
	NOP
	beqid	r12,	$0BB58_267
	NOP
	ADD	r29,	r0,	r0
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r12
	bneid	r4,	$0BB58_269
	NOP
	LWI	r4,	r1,	580
	bslli	r4,	r4,	3
	LWI	r5,	r1,	588
	ADD	r11,	r4,	r5
	SWI	r0,	r1,	180
	SWI	r0,	r1,	176
	LOAD	r4,	r11,	0
	SWI	r4,	r1,	176
	LOAD	r4,	r11,	1
	SWI	r4,	r1,	180
	LOAD	r4,	r11,	2
	SWI	r4,	r1,	184
	ADDI	r12,	r11,	3
	LOAD	r4,	r12,	0
	SWI	r4,	r1,	188
	LOAD	r4,	r12,	1
	SWI	r4,	r1,	192
	LOAD	r4,	r12,	2
	SWI	r4,	r1,	196
	ADDI	r4,	r11,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	200
	ADDI	r4,	r11,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	204
	ADDI	r20,	r1,	176
	ADD	r4,	r20,	r9
	LWI	r4,	r4,	4
	LWI	r5,	r1,	564
	FPRSUB	r4,	r5,	r4
	FPMUL	r26,	r4,	r22
	LW	r4,	r20,	r24
	LWI	r5,	r1,	568
	FPRSUB	r4,	r5,	r4
	FPMUL	r11,	r4,	r27
	ADDI	r21,	r0,	1
	FPGT	r4,	r11,	r26
	bneid	r4,	$0BB58_295
	NOP
	ADDI	r21,	r0,	0
$0BB58_295:
	ADD	r28,	r30,	r0
	ADD	r12,	r0,	r0
	ORI	r23,	r0,	1203982336
	bneid	r21,	$0BB58_325
	NOP
	ADD	r4,	r20,	r3
	LW	r20,	r20,	r7
	LWI	r4,	r4,	4
	LWI	r5,	r1,	564
	FPRSUB	r4,	r5,	r4
	FPMUL	r30,	r4,	r22
	LWI	r4,	r1,	568
	FPRSUB	r4,	r4,	r20
	FPMUL	r29,	r4,	r27
	FPGT	r4,	r30,	r29
	bneid	r4,	$0BB58_298
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB58_298:
	bneid	r20,	$0BB58_325
	NOP
	FPGT	r23,	r30,	r11
	ADDI	r12,	r0,	0
	ADDI	r20,	r0,	1
	bneid	r23,	$0BB58_301
	ADD	r21,	r20,	r0
	ADD	r21,	r12,	r0
$0BB58_301:
	bneid	r21,	$0BB58_303
	NOP
	ADD	r30,	r11,	r0
$0BB58_303:
	ADDI	r11,	r1,	176
	ADD	r21,	r11,	r6
	FPLT	r4,	r26,	r29
	bneid	r4,	$0BB58_305
	ADD	r23,	r20,	r0
	ADD	r23,	r12,	r0
$0BB58_305:
	LWI	r4,	r21,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r21,	r4,	r5
	FPGT	r4,	r30,	r21
	bneid	r4,	$0BB58_307
	NOP
	ADD	r20,	r12,	r0
$0BB58_307:
	bneid	r23,	$0BB58_309
	NOP
	ADD	r26,	r29,	r0
$0BB58_309:
	ADD	r12,	r0,	r0
	ORI	r23,	r0,	1203982336
	bneid	r20,	$0BB58_325
	NOP
	ADD	r4,	r11,	r8
	LWI	r4,	r4,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r29,	r4,	r5
	FPGT	r4,	r29,	r26
	bneid	r4,	$0BB58_312
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_312:
	bneid	r11,	$0BB58_325
	NOP
	FPLT	r23,	r21,	r26
	ADDI	r12,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r23,	$0BB58_315
	ADD	r20,	r11,	r0
	ADD	r20,	r12,	r0
$0BB58_315:
	bneid	r20,	$0BB58_317
	NOP
	ADD	r21,	r26,	r0
$0BB58_317:
	ORI	r4,	r0,	0
	FPLT	r4,	r21,	r4
	bneid	r4,	$0BB58_319
	NOP
	ADD	r11,	r12,	r0
$0BB58_319:
	ADD	r12,	r0,	r0
	ORI	r23,	r0,	1203982336
	bneid	r11,	$0BB58_325
	NOP
	FPGT	r4,	r29,	r30
	ADDI	r12,	r0,	1
	bneid	r4,	$0BB58_322
	ADD	r11,	r12,	r0
	ADDI	r11,	r0,	0
$0BB58_322:
	bneid	r11,	$0BB58_324
	NOP
	ADD	r29,	r30,	r0
$0BB58_324:
	ADD	r23,	r29,	r0
$0BB58_325:
	LWI	r4,	r1,	580
	ADDI	r30,	r4,	1
	bslli	r4,	r30,	3
	LWI	r5,	r1,	588
	ADD	r11,	r4,	r5
	SWI	r0,	r1,	212
	SWI	r0,	r1,	208
	LOAD	r4,	r11,	0
	SWI	r4,	r1,	208
	LOAD	r4,	r11,	1
	SWI	r4,	r1,	212
	LOAD	r4,	r11,	2
	SWI	r4,	r1,	216
	ADDI	r20,	r11,	3
	LOAD	r4,	r20,	0
	SWI	r4,	r1,	220
	LOAD	r4,	r20,	1
	SWI	r4,	r1,	224
	LOAD	r4,	r20,	2
	SWI	r4,	r1,	228
	ADDI	r4,	r11,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	232
	ADDI	r4,	r11,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	236
	ADDI	r20,	r1,	208
	ADD	r4,	r20,	r9
	LWI	r4,	r4,	4
	LWI	r5,	r1,	564
	FPRSUB	r4,	r5,	r4
	FPMUL	r9,	r4,	r22
	LW	r4,	r20,	r24
	LWI	r5,	r1,	568
	FPRSUB	r4,	r5,	r4
	FPMUL	r11,	r4,	r27
	ADDI	r21,	r0,	1
	FPGT	r4,	r11,	r9
	bneid	r4,	$0BB58_327
	NOP
	ADDI	r21,	r0,	0
$0BB58_327:
	bneid	r21,	$0BB58_352
	NOP
	ADD	r3,	r20,	r3
	LWI	r3,	r3,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	FPMUL	r3,	r3,	r22
	LW	r4,	r20,	r7
	LWI	r5,	r1,	568
	FPRSUB	r4,	r5,	r4
	FPMUL	r7,	r4,	r27
	FPGT	r4,	r3,	r7
	bneid	r4,	$0BB58_330
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB58_330:
	bneid	r20,	$0BB58_352
	NOP
	FPGT	r26,	r3,	r11
	ADDI	r21,	r0,	0
	ADDI	r20,	r0,	1
	bneid	r26,	$0BB58_333
	ADD	r24,	r20,	r0
	ADD	r24,	r21,	r0
$0BB58_333:
	bneid	r24,	$0BB58_335
	NOP
	ADD	r3,	r11,	r0
$0BB58_335:
	ADDI	r24,	r1,	208
	ADD	r11,	r24,	r6
	FPLT	r4,	r9,	r7
	bneid	r4,	$0BB58_337
	ADD	r6,	r20,	r0
	ADD	r6,	r21,	r0
$0BB58_337:
	LWI	r4,	r11,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r11,	r4,	r5
	FPGT	r4,	r3,	r11
	bneid	r4,	$0BB58_339
	NOP
	ADD	r20,	r21,	r0
$0BB58_339:
	bneid	r6,	$0BB58_341
	NOP
	ADD	r9,	r7,	r0
$0BB58_341:
	bneid	r20,	$0BB58_352
	NOP
	ADD	r4,	r24,	r8
	LWI	r4,	r4,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	584
	FPMUL	r6,	r4,	r5
	FPGT	r4,	r6,	r9
	bneid	r4,	$0BB58_344
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB58_344:
	bneid	r7,	$0BB58_352
	NOP
	FPLT	r21,	r11,	r9
	ADDI	r8,	r0,	0
	ADDI	r7,	r0,	1
	bneid	r21,	$0BB58_347
	ADD	r20,	r7,	r0
	ADD	r20,	r8,	r0
$0BB58_347:
	bneid	r20,	$0BB58_349
	NOP
	ADD	r11,	r9,	r0
$0BB58_349:
	ORI	r4,	r0,	0
	FPGE	r5,	r11,	r4
	FPUN	r4,	r11,	r4
	BITOR	r4,	r4,	r5
	bneid	r4,	$0BB58_351
	NOP
	ADD	r7,	r8,	r0
$0BB58_351:
	bneid	r7,	$0BB58_355
	NOP
$0BB58_352:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r12
	bneid	r3,	$0BB58_353
	NOP
	bslli	r3,	r31,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	580
	brid	$0BB58_367
	SWI	r4,	r3,	-4
$0BB58_229:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_233:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_245:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_249:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_257:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_265:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_267:
	brid	$0BB58_367
	ADD	r31,	r30,	r0
$0BB58_269:
	SWI	r30,	r1,	592
$0BB58_270:
	MULI	r3,	r29,	11
	LWI	r4,	r1,	580
	ADD	r6,	r3,	r4
	LOAD	r9,	r6,	0
	LOAD	r21,	r6,	1
	LOAD	r30,	r6,	2
	ADDI	r3,	r6,	6
	ADDI	r4,	r6,	3
	LOAD	r7,	r4,	0
	LOAD	r11,	r4,	1
	LOAD	r23,	r4,	2
	LOAD	r6,	r3,	0
	LOAD	r4,	r3,	1
	LOAD	r3,	r3,	2
	FPRSUB	r8,	r30,	r3
	FPRSUB	r24,	r21,	r4
	LWI	r5,	r1,	576
	FPMUL	r3,	r5,	r24
	LWI	r22,	r1,	572
	FPMUL	r4,	r22,	r8
	FPRSUB	r3,	r3,	r4
	FPRSUB	r31,	r9,	r6
	FPMUL	r4,	r5,	r31
	FPMUL	r5,	r25,	r8
	FPRSUB	r6,	r5,	r4
	FPRSUB	r20,	r9,	r7
	FPRSUB	r11,	r21,	r11
	FPMUL	r4,	r6,	r11
	FPMUL	r5,	r3,	r20
	FPADD	r27,	r5,	r4
	FPMUL	r4,	r22,	r31
	FPMUL	r5,	r25,	r24
	FPRSUB	r7,	r4,	r5
	FPRSUB	r26,	r30,	r23
	FPMUL	r4,	r7,	r26
	FPADD	r27,	r27,	r4
	ORI	r4,	r0,	0
	FPGE	r5,	r27,	r4
	FPUN	r4,	r27,	r4
	BITOR	r4,	r4,	r5
	bneid	r4,	$0BB58_272
	ADDI	r28,	r0,	1
	ADDI	r28,	r0,	0
$0BB58_272:
	bneid	r28,	$0BB58_274
	ADD	r23,	r27,	r0
	FPNEG	r23,	r27
$0BB58_274:
	ORI	r4,	r0,	953267991
	FPLT	r4,	r23,	r4
	bneid	r4,	$0BB58_276
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB58_276:
	bneid	r23,	$0BB58_291
	NOP
	LWI	r4,	r1,	560
	FPRSUB	r28,	r30,	r4
	LWI	r4,	r1,	568
	FPRSUB	r9,	r9,	r4
	LWI	r4,	r1,	564
	FPRSUB	r21,	r21,	r4
	FPMUL	r23,	r21,	r20
	FPMUL	r30,	r9,	r11
	FPMUL	r20,	r28,	r20
	FPMUL	r4,	r9,	r26
	FPMUL	r5,	r28,	r11
	FPMUL	r22,	r21,	r26
	FPRSUB	r11,	r23,	r30
	FPRSUB	r26,	r4,	r20
	FPRSUB	r20,	r5,	r22
	FPMUL	r4,	r26,	r24
	FPMUL	r5,	r20,	r31
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r11,	r8
	FPADD	r4,	r4,	r5
	ORI	r5,	r0,	1065353216
	FPDIV	r23,	r5,	r27
	FPMUL	r8,	r4,	r23
	ORI	r4,	r0,	0
	FPLT	r4,	r8,	r4
	bneid	r4,	$0BB58_279
	ADDI	r24,	r0,	1
	ADDI	r24,	r0,	0
$0BB58_279:
	bneid	r24,	$0BB58_291
	NOP
	FPMUL	r4,	r6,	r21
	FPMUL	r3,	r3,	r9
	FPADD	r3,	r3,	r4
	LWI	r4,	r1,	572
	FPMUL	r4,	r26,	r4
	FPMUL	r5,	r20,	r25
	FPMUL	r6,	r7,	r28
	FPADD	r6,	r3,	r6
	FPADD	r3,	r5,	r4
	LWI	r4,	r1,	576
	FPMUL	r4,	r11,	r4
	FPADD	r3,	r3,	r4
	FPMUL	r3,	r3,	r23
	FPMUL	r6,	r6,	r23
	FPADD	r4,	r6,	r3
	ORI	r5,	r0,	1065353216
	FPLE	r9,	r4,	r5
	ORI	r4,	r0,	0
	FPGE	r11,	r6,	r4
	FPGE	r20,	r3,	r4
	ADDI	r7,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r9,	$0BB58_282
	ADD	r6,	r3,	r0
	ADD	r6,	r7,	r0
$0BB58_282:
	bneid	r20,	$0BB58_284
	ADD	r9,	r3,	r0
	ADD	r9,	r7,	r0
$0BB58_284:
	bneid	r11,	$0BB58_286
	ADD	r20,	r3,	r0
	ADD	r20,	r7,	r0
$0BB58_286:
	FPLT	r4,	r8,	r10
	bneid	r4,	$0BB58_288
	NOP
	ADD	r3,	r7,	r0
$0BB58_288:
	BITAND	r4,	r20,	r9
	BITAND	r4,	r4,	r6
	BITAND	r3,	r3,	r4
	bneid	r3,	$0BB58_290
	NOP
	ADD	r8,	r10,	r0
$0BB58_290:
	ADD	r10,	r8,	r0
$0BB58_291:
	ADDI	r29,	r29,	1
	CMP	r3,	r12,	r29
	bneid	r3,	$0BB58_270
	NOP
	LWI	r31,	r1,	592
	LWI	r27,	r1,	620
	brid	$0BB58_367
	LWI	r22,	r1,	624
$0BB58_353:
	ADD	r31,	r28,	r0
$0BB58_367:
	bgtid	r31,	$0BB58_226
	NOP
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	692
	FPDIV	r3,	r3,	r4
	FPGT	r4,	r3,	r10
	FPUN	r3,	r3,	r10
	BITOR	r3,	r3,	r4
	ADDI	r6,	r0,	1
	LWI	r28,	r1,	628
	bneid	r3,	$0BB58_370
	LWI	r29,	r1,	632
	ADDI	r6,	r0,	0
$0BB58_370:
	ORI	r3,	r0,	0
	ADD	r4,	r3,	r0
	ADD	r5,	r3,	r0
	bneid	r6,	$0BB58_372
	LWI	r12,	r1,	676
	LWI	r3,	r1,	684
	FPMUL	r3,	r29,	r3
	LWI	r4,	r1,	680
	FPMUL	r4,	r12,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	688
	FPMUL	r4,	r28,	r4
	FPADD	r3,	r3,	r4
	ORI	r5,	r0,	0
	FPMAX	r3,	r3,	r5
	LWI	r4,	r1,	636
	FPMUL	r6,	r4,	r3
	LWI	r4,	r1,	640
	FPMUL	r4,	r4,	r3
	LWI	r7,	r1,	644
	FPMUL	r3,	r7,	r3
	FPADD	r3,	r3,	r5
	FPADD	r4,	r4,	r5
	FPADD	r5,	r6,	r5
$0BB58_372:
	RAND	r7
	RAND	r6
	FPADD	r6,	r6,	r6
	ORI	r8,	r0,	-1082130432
	FPADD	r6,	r6,	r8
	FPADD	r7,	r7,	r7
	FPADD	r7,	r7,	r8
	FPMUL	r9,	r7,	r7
	FPMUL	r10,	r6,	r6
	FPADD	r8,	r9,	r10
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r8,	r11
	bneid	r11,	$0BB58_374
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB58_374:
	bneid	r8,	$0BB58_372
	NOP
	ORI	r8,	r0,	0
	FPGE	r11,	r12,	r8
	FPUN	r8,	r12,	r8
	BITOR	r8,	r8,	r11
	bneid	r8,	$0BB58_377
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_377:
	ORI	r8,	r0,	1065353216
	FPRSUB	r9,	r9,	r8
	FPRSUB	r9,	r10,	r9
	FPINVSQRT	r10,	r9
	bneid	r11,	$0BB58_379
	ADD	r9,	r12,	r0
	FPNEG	r9,	r12
$0BB58_379:
	ADD	r30,	r12,	r0
	ORI	r11,	r0,	0
	FPGE	r12,	r29,	r11
	FPUN	r11,	r29,	r11
	BITOR	r12,	r11,	r12
	bneid	r12,	$0BB58_381
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_381:
	ADD	r12,	r29,	r0
	LWI	r24,	r1,	660
	LWI	r26,	r1,	664
	bneid	r11,	$0BB58_383
	LWI	r27,	r1,	668
	FPNEG	r12,	r29
$0BB58_383:
	ORI	r11,	r0,	0
	FPGE	r20,	r28,	r11
	FPUN	r11,	r28,	r11
	BITOR	r11,	r11,	r20
	ADDI	r20,	r0,	1
	bneid	r11,	$0BB58_385
	LWI	r22,	r1,	648
	ADDI	r20,	r0,	0
$0BB58_385:
	ADD	r11,	r28,	r0
	bneid	r20,	$0BB58_387
	LWI	r23,	r1,	652
	FPNEG	r11,	r28
$0BB58_387:
	FPGE	r20,	r9,	r12
	FPUN	r21,	r9,	r12
	BITOR	r20,	r21,	r20
	ADDI	r21,	r0,	1
	bneid	r20,	$0BB58_389
	LWI	r25,	r1,	672
	ADDI	r21,	r0,	0
$0BB58_389:
	FPDIV	r8,	r8,	r10
	ORI	r20,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r21,	$0BB58_393
	NOP
	FPLT	r9,	r9,	r11
	bneid	r9,	$0BB58_392
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB58_392:
	bneid	r21,	$0BB58_397
	ADD	r9,	r10,	r0
$0BB58_393:
	FPLT	r9,	r12,	r11
	bneid	r9,	$0BB58_395
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_395:
	ORI	r9,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r11,	$0BB58_397
	ADD	r20,	r10,	r0
	ORI	r9,	r0,	0
	ORI	r10,	r0,	1065353216
	brid	$0BB58_397
	ADD	r20,	r9,	r0
$0BB58_11:
	LWI	r10,	r1,	648
	brid	$0BB58_187
	LWI	r6,	r1,	652
$0BB58_186:
	ORI	r3,	r0,	1065151889
	LWI	r4,	r1,	660
	FPMUL	r3,	r4,	r3
	LWI	r10,	r1,	648
	FPADD	r10,	r10,	r3
	ORI	r3,	r0,	1060806590
	LWI	r4,	r1,	668
	FPMUL	r3,	r4,	r3
	LWI	r21,	r1,	656
	FPADD	r21,	r21,	r3
	ORI	r3,	r0,	1057988018
	LWI	r4,	r1,	664
	FPMUL	r3,	r4,	r3
	LWI	r6,	r1,	652
	FPADD	r6,	r6,	r3
$0BB58_187:
	ORI	r3,	r0,	0
	FPLT	r5,	r6,	r3
	ADDI	r4,	r0,	1
	bneid	r5,	$0BB58_189
	LWI	r8,	r1,	808
	ADDI	r4,	r0,	0
$0BB58_189:
	bneid	r4,	$0BB58_194
	NOP
	ORI	r3,	r0,	1065353216
	FPGT	r5,	r6,	r3
	bneid	r5,	$0BB58_192
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB58_192:
	bneid	r4,	$0BB58_194
	NOP
	ADD	r3,	r6,	r0
$0BB58_194:
	ORI	r4,	r0,	0
	FPLT	r6,	r21,	r4
	bneid	r6,	$0BB58_196
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_196:
	bneid	r5,	$0BB58_201
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r6,	r21,	r4
	bneid	r6,	$0BB58_199
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_199:
	bneid	r5,	$0BB58_201
	NOP
	ADD	r4,	r21,	r0
$0BB58_201:
	ORI	r5,	r0,	0
	FPLT	r7,	r10,	r5
	ADDI	r6,	r0,	1
	bneid	r7,	$0BB58_203
	LWI	r9,	r1,	804
	ADDI	r6,	r0,	0
$0BB58_203:
	bneid	r6,	$0BB58_208
	NOP
	ORI	r5,	r0,	1065353216
	FPGT	r7,	r10,	r5
	bneid	r7,	$0BB58_206
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB58_206:
	bneid	r6,	$0BB58_208
	NOP
	ADD	r5,	r10,	r0
$0BB58_208:
	LWI	r6,	r1,	716
	LWI	r7,	r1,	720
	ADD	r6,	r6,	r7
	MULI	r6,	r6,	3
	ADD	r6,	r6,	r8
	STORE	r6,	r3,	0
	STORE	r6,	r4,	1
	STORE	r6,	r5,	2
	ATOMIC_INC	r3,	0
	CMP	r4,	r9,	r3
	bltid	r4,	$0BB58_3
	NOP
	brid	$0BB58_757
	NOP
$0BB58_12:
	CMP	r5,	r5,	r20
	bltid	r5,	$0BB58_24
	NOP
	ADDI	r4,	r4,	4
	SWI	r4,	r1,	716
	ADD	r4,	r11,	r0
	SWI	r4,	r1,	804
$0BB58_14:
	LWI	r5,	r1,	812
	DIV	r4,	r5,	r3
	MUL	r5,	r4,	r5
	SWI	r5,	r1,	816
	RSUB	r3,	r5,	r3
	SWI	r3,	r1,	820
	ADD	r6,	r0,	r0
	ORI	r5,	r0,	0
	SWI	r5,	r1,	720
	FPCONV	r4,	r4
	SWI	r4,	r1,	792
	FPCONV	r3,	r3
	SWI	r3,	r1,	796
	SWI	r5,	r1,	724
	ADD	r4,	r5,	r0
$0BB58_15:
	SWI	r4,	r1,	732
	SWI	r6,	r1,	728
	ORI	r3,	r0,	-1090519040
	RAND	r4
	FPADD	r4,	r4,	r3
	LWI	r5,	r1,	796
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	784
	FPMUL	r4,	r4,	r5
	RAND	r5
	FPADD	r5,	r5,	r3
	FPADD	r4,	r4,	r4
	ORI	r3,	r0,	-1082130432
	FPADD	r4,	r4,	r3
	LWI	r6,	r1,	760
	FPMUL	r6,	r6,	r4
	LWI	r7,	r1,	748
	FPADD	r7,	r7,	r6
	LWI	r6,	r1,	792
	FPADD	r5,	r6,	r5
	LWI	r6,	r1,	788
	FPMUL	r5,	r5,	r6
	FPADD	r5,	r5,	r5
	FPADD	r6,	r5,	r3
	LWI	r3,	r1,	772
	FPMUL	r3,	r3,	r6
	FPADD	r3,	r7,	r3
	LWI	r5,	r1,	764
	FPMUL	r5,	r5,	r4
	LWI	r7,	r1,	752
	FPADD	r5,	r7,	r5
	LWI	r7,	r1,	776
	FPMUL	r7,	r7,	r6
	FPADD	r5,	r5,	r7
	FPMUL	r7,	r5,	r5
	FPMUL	r8,	r3,	r3
	FPADD	r7,	r8,	r7
	LWI	r8,	r1,	780
	FPMUL	r6,	r8,	r6
	LWI	r8,	r1,	768
	FPMUL	r4,	r8,	r4
	LWI	r8,	r1,	756
	FPADD	r4,	r8,	r4
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r25,	r0,	1065353216
	FPDIV	r6,	r25,	r6
	FPDIV	r10,	r4,	r6
	FPDIV	r29,	r5,	r6
	FPDIV	r4,	r3,	r6
	ADD	r26,	r0,	r0
	ORI	r22,	r0,	0
	ADD	r23,	r22,	r0
	ADD	r24,	r22,	r0
	LWI	r3,	r1,	744
	SWI	r3,	r1,	560
	LWI	r3,	r1,	740
	SWI	r3,	r1,	564
	LWI	r3,	r1,	736
	SWI	r3,	r1,	568
	SWI	r25,	r1,	660
	brid	$0BB58_16
	SWI	r25,	r1,	656
$0BB58_755:
	FPMUL	r5,	r27,	r10
	FPMUL	r20,	r28,	r11
	FPRSUB	r20,	r5,	r20
	FPMUL	r5,	r29,	r10
	FPMUL	r10,	r28,	r12
	FPRSUB	r5,	r10,	r5
	FPMUL	r10,	r29,	r11
	FPMUL	r11,	r27,	r12
	FPRSUB	r12,	r10,	r11
	FPMUL	r10,	r27,	r12
	FPMUL	r11,	r28,	r5
	FPRSUB	r10,	r10,	r11
	FPMUL	r11,	r29,	r5
	FPMUL	r21,	r27,	r20
	FPRSUB	r11,	r11,	r21
	FPMUL	r11,	r11,	r7
	FPMUL	r21,	r12,	r8
	FPADD	r11,	r21,	r11
	FPMUL	r10,	r10,	r7
	FPMUL	r21,	r20,	r8
	FPADD	r10,	r21,	r10
	FPMUL	r21,	r29,	r9
	FPADD	r10,	r10,	r21
	FPMUL	r21,	r28,	r9
	FPADD	r11,	r11,	r21
	FPMUL	r20,	r28,	r20
	FPMUL	r12,	r29,	r12
	FPMUL	r3,	r3,	r25
	LWI	r21,	r1,	636
	FPMUL	r25,	r25,	r21
	LWI	r21,	r1,	660
	FPMUL	r4,	r4,	r21
	LWI	r28,	r1,	632
	FPMUL	r21,	r21,	r28
	SWI	r21,	r1,	660
	LWI	r21,	r1,	656
	FPMUL	r6,	r6,	r21
	LWI	r28,	r1,	628
	FPMUL	r21,	r21,	r28
	SWI	r21,	r1,	656
	FPADD	r22,	r22,	r3
	FPADD	r23,	r23,	r4
	FPADD	r24,	r24,	r6
	FPMUL	r3,	r11,	r11
	FPMUL	r4,	r10,	r10
	FPADD	r3,	r4,	r3
	FPRSUB	r4,	r20,	r12
	FPMUL	r4,	r4,	r7
	FPMUL	r5,	r5,	r8
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r27,	r9
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r4,	r4
	FPADD	r3,	r3,	r5
	FPINVSQRT	r3,	r3
	ORI	r5,	r0,	1065353216
	FPDIV	r7,	r5,	r3
	FPDIV	r3,	r10,	r7
	FPDIV	r6,	r11,	r7
	FPMUL	r8,	r6,	r6
	FPMUL	r9,	r3,	r3
	FPADD	r8,	r9,	r8
	FPDIV	r4,	r4,	r7
	FPMUL	r7,	r4,	r4
	FPADD	r7,	r8,	r7
	FPINVSQRT	r7,	r7
	FPDIV	r5,	r5,	r7
	FPDIV	r10,	r4,	r5
	FPDIV	r29,	r6,	r5
	FPDIV	r4,	r3,	r5
$0BB58_16:
	SWI	r4,	r1,	576
	ORI	r3,	r0,	1065353216
	FPDIV	r7,	r3,	r10
	FPDIV	r5,	r3,	r29
	SWI	r5,	r1,	600
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	604
	ORI	r4,	r0,	0
	FPLT	r3,	r3,	r4
	FPLT	r5,	r5,	r4
	FPLT	r6,	r7,	r4
	ADDI	r4,	r0,	0
	ADDI	r8,	r0,	1
	SWI	r8,	r1,	616
	bneid	r6,	$0BB58_18
	SWI	r8,	r1,	620
	SWI	r4,	r1,	620
$0BB58_18:
	SWI	r24,	r1,	652
	SWI	r23,	r1,	648
	SWI	r22,	r1,	644
	LWI	r6,	r1,	616
	bneid	r5,	$0BB58_20
	SWI	r6,	r1,	624
	SWI	r4,	r1,	624
$0BB58_20:
	bneid	r3,	$0BB58_22
	NOP
	SWI	r4,	r1,	616
$0BB58_22:
	LWI	r3,	r1,	708
	CMP	r3,	r3,	r26
	bgeid	r3,	$0BB58_23
	NOP
	SWI	r25,	r1,	664
	ADDI	r26,	r26,	1
	SWI	r26,	r1,	668
	ADDI	r9,	r0,	1
	ORI	r3,	r0,	1203982336
	SWI	r3,	r1,	572
	ADDI	r4,	r0,	-1
	SWI	r4,	r1,	596
	LWI	r3,	r1,	620
	XORI	r3,	r3,	1
	SWI	r3,	r1,	632
	LWI	r3,	r1,	624
	XORI	r3,	r3,	1
	SWI	r3,	r1,	636
	LWI	r3,	r1,	616
	XORI	r3,	r3,	1
	SWI	r3,	r1,	640
	SWI	r0,	r1,	336
	SWI	r4,	r1,	592
	ADD	r25,	r7,	r0
	brid	$0BB58_421
	SWI	r25,	r1,	628
$0BB58_535:
	beqid	r3,	$0BB58_536
	NOP
	FPGT	r7,	r22,	r26
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r7,	$0BB58_539
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB58_539:
	bneid	r5,	$0BB58_541
	NOP
	ADD	r22,	r26,	r0
$0BB58_541:
	FPGE	r5,	r21,	r22
	FPUN	r7,	r21,	r22
	BITOR	r5,	r7,	r5
	bneid	r5,	$0BB58_543
	NOP
	ADD	r3,	r4,	r0
$0BB58_543:
	bneid	r3,	$0BB58_545
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	SWI	r6,	r5,	-4
	LWI	r5,	r1,	580
	SW	r5,	r4,	r3
	brid	$0BB58_563
	ADDI	r9,	r9,	1
$0BB58_536:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	brid	$0BB58_563
	SWI	r6,	r3,	-4
$0BB58_545:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	LWI	r7,	r1,	580
	SWI	r7,	r5,	-4
	SW	r6,	r4,	r3
	brid	$0BB58_563
	ADDI	r9,	r9,	1
$0BB58_421:
	ADDI	r3,	r0,	-1
	ADD	r8,	r9,	r3
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	336
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	248
	SWI	r0,	r1,	244
	SWI	r0,	r1,	240
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	240
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	244
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	248
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	252
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	256
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	260
	LWI	r4,	r1,	636
	MULI	r5,	r4,	12
	SWI	r5,	r1,	612
	ADDI	r4,	r1,	240
	ADD	r5,	r4,	r5
	ADDI	r6,	r3,	7
	ADDI	r3,	r3,	6
	LWI	r7,	r1,	616
	MULI	r28,	r7,	12
	LOAD	r31,	r3,	0
	SWI	r31,	r1,	264
	LOAD	r3,	r6,	0
	SWI	r3,	r1,	580
	SWI	r3,	r1,	268
	LWI	r5,	r5,	4
	LWI	r6,	r1,	564
	FPRSUB	r5,	r6,	r5
	LWI	r3,	r1,	600
	FPMUL	r21,	r5,	r3
	LW	r5,	r4,	r28
	LWI	r6,	r1,	568
	FPRSUB	r5,	r6,	r5
	LWI	r3,	r1,	604
	FPMUL	r5,	r5,	r3
	ADDI	r6,	r0,	1
	FPGT	r7,	r5,	r21
	bneid	r7,	$0BB58_423
	NOP
	ADDI	r6,	r0,	0
$0BB58_423:
	bneid	r6,	$0BB58_424
	NOP
	LWI	r3,	r1,	624
	MULI	r26,	r3,	12
	ADD	r6,	r4,	r26
	LWI	r6,	r6,	4
	LWI	r7,	r1,	564
	FPRSUB	r6,	r7,	r6
	LWI	r3,	r1,	600
	FPMUL	r6,	r6,	r3
	LWI	r3,	r1,	640
	MULI	r22,	r3,	12
	LW	r4,	r4,	r22
	LWI	r7,	r1,	568
	FPRSUB	r4,	r7,	r4
	LWI	r3,	r1,	604
	FPMUL	r4,	r4,	r3
	FPGT	r7,	r6,	r4
	bneid	r7,	$0BB58_427
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_427:
	bneid	r11,	$0BB58_428
	NOP
	FPGT	r7,	r6,	r5
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r7,	$0BB58_431
	ADD	r11,	r12,	r0
	ADD	r11,	r20,	r0
$0BB58_431:
	bneid	r11,	$0BB58_433
	NOP
	ADD	r6,	r5,	r0
$0BB58_433:
	LWI	r3,	r1,	632
	MULI	r30,	r3,	12
	ADDI	r23,	r1,	240
	ADD	r5,	r23,	r30
	FPLT	r7,	r21,	r4
	bneid	r7,	$0BB58_435
	ADD	r24,	r12,	r0
	ADD	r24,	r20,	r0
$0BB58_435:
	LWI	r5,	r5,	8
	LWI	r7,	r1,	560
	FPRSUB	r5,	r7,	r5
	FPMUL	r5,	r5,	r25
	FPGT	r7,	r6,	r5
	bneid	r7,	$0BB58_437
	NOP
	ADD	r12,	r20,	r0
$0BB58_437:
	bneid	r24,	$0BB58_439
	NOP
	ADD	r21,	r4,	r0
$0BB58_439:
	bneid	r12,	$0BB58_440
	NOP
	LWI	r3,	r1,	620
	MULI	r27,	r3,	12
	ADD	r4,	r23,	r27
	LWI	r4,	r4,	8
	LWI	r7,	r1,	560
	FPRSUB	r4,	r7,	r4
	FPMUL	r4,	r4,	r25
	FPGT	r7,	r4,	r21
	bneid	r7,	$0BB58_443
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_443:
	bneid	r11,	$0BB58_444
	NOP
	FPLT	r7,	r5,	r21
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r7,	$0BB58_447
	ADD	r11,	r12,	r0
	ADD	r11,	r20,	r0
$0BB58_447:
	bneid	r11,	$0BB58_449
	NOP
	ADD	r5,	r21,	r0
$0BB58_449:
	ORI	r7,	r0,	0
	FPLT	r5,	r5,	r7
	bneid	r5,	$0BB58_451
	NOP
	ADD	r12,	r20,	r0
$0BB58_451:
	bneid	r12,	$0BB58_452
	NOP
	FPGT	r7,	r4,	r6
	ADDI	r12,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r7,	$0BB58_455
	ADD	r11,	r5,	r0
	ADD	r11,	r12,	r0
$0BB58_455:
	bneid	r11,	$0BB58_457
	NOP
	ADD	r4,	r6,	r0
$0BB58_457:
	LWI	r3,	r1,	572
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB58_459
	NOP
	ADD	r5,	r12,	r0
$0BB58_459:
	ADD	r3,	r31,	r0
	bneid	r5,	$0BB58_460
	SWI	r3,	r1,	584
	beqid	r3,	$0BB58_462
	NOP
	ADD	r21,	r0,	r0
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r3
	bneid	r4,	$0BB58_464
	NOP
	LWI	r3,	r1,	580
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	280
	SWI	r0,	r1,	276
	SWI	r0,	r1,	272
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	272
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	276
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	280
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	284
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	288
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	292
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	296
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	300
	ADDI	r5,	r1,	272
	LWI	r31,	r1,	612
	ADD	r3,	r5,	r31
	LWI	r3,	r3,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	600
	FPMUL	r23,	r3,	r4
	LW	r3,	r5,	r28
	LWI	r4,	r1,	568
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	604
	FPMUL	r4,	r3,	r4
	ADDI	r6,	r0,	1
	FPGT	r3,	r4,	r23
	bneid	r3,	$0BB58_475
	NOP
	ADDI	r6,	r0,	0
$0BB58_475:
	SWI	r8,	r1,	608
	ADD	r3,	r0,	r0
	ORI	r21,	r0,	1203982336
	bneid	r6,	$0BB58_505
	NOP
	ADD	r6,	r5,	r26
	LW	r5,	r5,	r22
	LWI	r6,	r6,	4
	LWI	r7,	r1,	564
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	600
	FPMUL	r6,	r6,	r7
	LWI	r7,	r1,	568
	FPRSUB	r5,	r7,	r5
	LWI	r7,	r1,	604
	FPMUL	r24,	r5,	r7
	FPGT	r7,	r6,	r24
	bneid	r7,	$0BB58_478
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_478:
	bneid	r5,	$0BB58_505
	NOP
	FPGT	r7,	r6,	r4
	ADDI	r3,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r7,	$0BB58_481
	ADD	r11,	r5,	r0
	ADD	r11,	r3,	r0
$0BB58_481:
	bneid	r11,	$0BB58_483
	NOP
	ADD	r6,	r4,	r0
$0BB58_483:
	ADDI	r12,	r1,	272
	ADD	r4,	r12,	r30
	FPLT	r7,	r23,	r24
	bneid	r7,	$0BB58_485
	ADD	r20,	r5,	r0
	ADD	r20,	r3,	r0
$0BB58_485:
	LWI	r4,	r4,	8
	LWI	r7,	r1,	560
	FPRSUB	r4,	r7,	r4
	FPMUL	r4,	r4,	r25
	FPGT	r7,	r6,	r4
	bneid	r7,	$0BB58_487
	NOP
	ADD	r5,	r3,	r0
$0BB58_487:
	bneid	r20,	$0BB58_489
	NOP
	ADD	r23,	r24,	r0
$0BB58_489:
	ADD	r3,	r0,	r0
	ORI	r21,	r0,	1203982336
	bneid	r5,	$0BB58_505
	NOP
	ADD	r5,	r12,	r27
	LWI	r5,	r5,	8
	LWI	r7,	r1,	560
	FPRSUB	r5,	r7,	r5
	FPMUL	r24,	r5,	r25
	FPGT	r7,	r24,	r23
	bneid	r7,	$0BB58_492
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_492:
	bneid	r5,	$0BB58_505
	NOP
	FPLT	r7,	r4,	r23
	ADDI	r3,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r7,	$0BB58_495
	ADD	r11,	r5,	r0
	ADD	r11,	r3,	r0
$0BB58_495:
	bneid	r11,	$0BB58_497
	NOP
	ADD	r4,	r23,	r0
$0BB58_497:
	ORI	r7,	r0,	0
	FPLT	r4,	r4,	r7
	bneid	r4,	$0BB58_499
	NOP
	ADD	r5,	r3,	r0
$0BB58_499:
	ADD	r3,	r0,	r0
	ORI	r21,	r0,	1203982336
	bneid	r5,	$0BB58_505
	NOP
	FPGT	r5,	r24,	r6
	ADDI	r3,	r0,	1
	bneid	r5,	$0BB58_502
	ADD	r4,	r3,	r0
	ADDI	r4,	r0,	0
$0BB58_502:
	bneid	r4,	$0BB58_504
	NOP
	ADD	r24,	r6,	r0
$0BB58_504:
	ADD	r21,	r24,	r0
$0BB58_505:
	LWI	r4,	r1,	580
	ADDI	r6,	r4,	1
	bslli	r4,	r6,	3
	LWI	r5,	r1,	588
	ADD	r4,	r4,	r5
	SWI	r0,	r1,	312
	SWI	r0,	r1,	308
	SWI	r0,	r1,	304
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	304
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	308
	LOAD	r5,	r4,	2
	SWI	r5,	r1,	312
	ADDI	r5,	r4,	3
	LOAD	r7,	r5,	0
	SWI	r7,	r1,	316
	LOAD	r7,	r5,	1
	SWI	r7,	r1,	320
	LOAD	r5,	r5,	2
	SWI	r5,	r1,	324
	ADDI	r5,	r4,	6
	LOAD	r5,	r5,	0
	SWI	r5,	r1,	328
	ADDI	r4,	r4,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	332
	ADDI	r4,	r1,	304
	ADD	r5,	r4,	r31
	LWI	r5,	r5,	4
	LWI	r7,	r1,	564
	FPRSUB	r5,	r7,	r5
	LWI	r7,	r1,	600
	FPMUL	r23,	r5,	r7
	LW	r5,	r4,	r28
	LWI	r7,	r1,	568
	FPRSUB	r5,	r7,	r5
	LWI	r7,	r1,	604
	FPMUL	r5,	r5,	r7
	ADDI	r12,	r0,	1
	FPGT	r7,	r5,	r23
	bneid	r7,	$0BB58_507
	NOP
	ADDI	r12,	r0,	0
$0BB58_507:
	bneid	r12,	$0BB58_532
	NOP
	ADD	r7,	r4,	r26
	LWI	r7,	r7,	4
	LWI	r8,	r1,	564
	FPRSUB	r7,	r8,	r7
	LWI	r8,	r1,	600
	FPMUL	r26,	r7,	r8
	LW	r4,	r4,	r22
	LWI	r7,	r1,	568
	FPRSUB	r4,	r7,	r4
	LWI	r7,	r1,	604
	FPMUL	r4,	r4,	r7
	FPGT	r7,	r26,	r4
	bneid	r7,	$0BB58_510
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_510:
	bneid	r11,	$0BB58_532
	NOP
	FPGT	r7,	r26,	r5
	ADDI	r22,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r7,	$0BB58_513
	ADD	r11,	r12,	r0
	ADD	r11,	r22,	r0
$0BB58_513:
	bneid	r11,	$0BB58_515
	NOP
	ADD	r26,	r5,	r0
$0BB58_515:
	ADDI	r5,	r1,	304
	ADD	r20,	r5,	r30
	FPLT	r7,	r23,	r4
	bneid	r7,	$0BB58_517
	ADD	r24,	r12,	r0
	ADD	r24,	r22,	r0
$0BB58_517:
	LWI	r7,	r20,	8
	LWI	r8,	r1,	560
	FPRSUB	r7,	r8,	r7
	FPMUL	r20,	r7,	r25
	FPGT	r7,	r26,	r20
	bneid	r7,	$0BB58_519
	NOP
	ADD	r12,	r22,	r0
$0BB58_519:
	bneid	r24,	$0BB58_521
	NOP
	ADD	r23,	r4,	r0
$0BB58_521:
	bneid	r12,	$0BB58_532
	NOP
	ADD	r4,	r5,	r27
	LWI	r4,	r4,	8
	LWI	r5,	r1,	560
	FPRSUB	r4,	r5,	r4
	FPMUL	r22,	r4,	r25
	FPGT	r5,	r22,	r23
	bneid	r5,	$0BB58_524
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB58_524:
	bneid	r4,	$0BB58_532
	NOP
	FPLT	r7,	r20,	r23
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB58_527
	ADD	r11,	r4,	r0
	ADD	r11,	r5,	r0
$0BB58_527:
	bneid	r11,	$0BB58_529
	NOP
	ADD	r20,	r23,	r0
$0BB58_529:
	ORI	r7,	r0,	0
	FPGE	r8,	r20,	r7
	FPUN	r7,	r20,	r7
	BITOR	r7,	r7,	r8
	bneid	r7,	$0BB58_531
	NOP
	ADD	r4,	r5,	r0
$0BB58_531:
	bneid	r4,	$0BB58_535
	NOP
$0BB58_532:
	ADDI	r4,	r0,	1
	CMP	r3,	r4,	r3
	bneid	r3,	$0BB58_533
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	580
	brid	$0BB58_563
	SWI	r4,	r3,	-4
$0BB58_424:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_428:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_440:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_444:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_452:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_460:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_462:
	brid	$0BB58_563
	ADD	r9,	r8,	r0
$0BB58_464:
	SWI	r8,	r1,	608
$0BB58_465:
	MULI	r4,	r21,	11
	LWI	r3,	r1,	580
	ADD	r6,	r4,	r3
	LOAD	r27,	r6,	0
	LOAD	r28,	r6,	1
	LOAD	r31,	r6,	2
	ADDI	r4,	r6,	3
	LOAD	r5,	r4,	0
	LOAD	r12,	r4,	1
	LOAD	r4,	r4,	2
	ADDI	r7,	r6,	6
	LOAD	r20,	r7,	0
	LOAD	r8,	r7,	1
	LOAD	r7,	r7,	2
	FPRSUB	r23,	r31,	r7
	FPRSUB	r30,	r28,	r8
	FPMUL	r7,	r10,	r30
	FPMUL	r8,	r29,	r23
	FPRSUB	r9,	r7,	r8
	FPRSUB	r24,	r27,	r20
	FPMUL	r7,	r10,	r24
	LWI	r25,	r1,	576
	FPMUL	r8,	r25,	r23
	FPRSUB	r22,	r8,	r7
	FPRSUB	r5,	r27,	r5
	FPRSUB	r20,	r28,	r12
	FPMUL	r7,	r22,	r20
	FPMUL	r8,	r9,	r5
	FPADD	r11,	r8,	r7
	FPMUL	r7,	r29,	r24
	ADD	r3,	r29,	r0
	FPMUL	r8,	r25,	r30
	FPRSUB	r26,	r7,	r8
	FPRSUB	r12,	r31,	r4
	FPMUL	r4,	r26,	r12
	FPADD	r29,	r11,	r4
	ORI	r4,	r0,	0
	FPGE	r7,	r29,	r4
	FPUN	r4,	r29,	r4
	BITOR	r4,	r4,	r7
	bneid	r4,	$0BB58_467
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_467:
	bneid	r11,	$0BB58_469
	ADD	r4,	r29,	r0
	FPNEG	r4,	r29
$0BB58_469:
	ORI	r7,	r0,	953267991
	FPLT	r7,	r4,	r7
	bneid	r7,	$0BB58_471
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB58_471:
	bneid	r4,	$0BB58_472
	NOP
	ADD	r25,	r10,	r0
	LWI	r4,	r1,	560
	FPRSUB	r31,	r31,	r4
	LWI	r4,	r1,	568
	FPRSUB	r27,	r27,	r4
	LWI	r4,	r1,	564
	FPRSUB	r28,	r28,	r4
	FPMUL	r4,	r28,	r5
	FPMUL	r11,	r27,	r20
	FPMUL	r7,	r31,	r5
	FPMUL	r8,	r27,	r12
	FPMUL	r10,	r31,	r20
	FPMUL	r12,	r28,	r12
	FPRSUB	r5,	r4,	r11
	FPRSUB	r20,	r8,	r7
	FPRSUB	r12,	r10,	r12
	FPMUL	r4,	r20,	r30
	FPMUL	r7,	r12,	r24
	FPADD	r4,	r7,	r4
	FPMUL	r7,	r5,	r23
	FPADD	r7,	r4,	r7
	ORI	r4,	r0,	1065353216
	FPDIV	r4,	r4,	r29
	FPMUL	r23,	r7,	r4
	ORI	r7,	r0,	0
	FPLT	r7,	r23,	r7
	bneid	r7,	$0BB58_548
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB58_548:
	bneid	r11,	$0BB58_549
	ADD	r29,	r3,	r0
	FPMUL	r7,	r22,	r28
	FPMUL	r8,	r9,	r27
	FPADD	r7,	r8,	r7
	FPMUL	r8,	r20,	r29
	LWI	r3,	r1,	576
	FPMUL	r9,	r12,	r3
	FPMUL	r10,	r26,	r31
	FPADD	r7,	r7,	r10
	FPADD	r8,	r9,	r8
	ADD	r10,	r25,	r0
	FPMUL	r5,	r5,	r10
	FPADD	r5,	r8,	r5
	FPMUL	r5,	r5,	r4
	FPMUL	r4,	r7,	r4
	FPADD	r7,	r4,	r5
	ORI	r8,	r0,	1065353216
	FPLE	r22,	r7,	r8
	ORI	r7,	r0,	0
	FPGE	r4,	r4,	r7
	FPGE	r20,	r5,	r7
	LWI	r3,	r1,	572
	FPGE	r5,	r23,	r3
	FPUN	r7,	r23,	r3
	BITOR	r24,	r7,	r5
	ADDI	r12,	r0,	0
	ADDI	r9,	r0,	1
	bneid	r22,	$0BB58_552
	ADD	r5,	r9,	r0
	ADD	r5,	r12,	r0
$0BB58_552:
	bneid	r24,	$0BB58_554
	ADD	r22,	r9,	r0
	ADD	r22,	r12,	r0
$0BB58_554:
	bneid	r20,	$0BB58_556
	ADD	r24,	r9,	r0
	ADD	r24,	r12,	r0
$0BB58_556:
	bneid	r4,	$0BB58_558
	NOP
	ADD	r9,	r12,	r0
$0BB58_558:
	bneid	r22,	$0BB58_561
	NOP
	BITAND	r4,	r9,	r24
	BITAND	r4,	r4,	r5
	ADDI	r5,	r0,	1
	CMP	r4,	r5,	r4
	bneid	r4,	$0BB58_561
	NOP
	ADDI	r4,	r6,	10
	LOAD	r3,	r4,	0
	SWI	r3,	r1,	592
	SWI	r6,	r1,	596
	brid	$0BB58_561
	SWI	r23,	r1,	572
$0BB58_472:
	brid	$0BB58_561
	ADD	r29,	r3,	r0
$0BB58_549:
	ADD	r10,	r25,	r0
$0BB58_561:
	ADDI	r21,	r21,	1
	LWI	r3,	r1,	584
	CMP	r4,	r3,	r21
	bneid	r4,	$0BB58_465
	NOP
	LWI	r9,	r1,	608
	brid	$0BB58_563
	LWI	r25,	r1,	628
$0BB58_533:
	LWI	r9,	r1,	608
$0BB58_563:
	bgtid	r9,	$0BB58_421
	NOP
	ORI	r3,	r0,	1203982336
	LWI	r4,	r1,	572
	FPNE	r4,	r4,	r3
	bneid	r4,	$0BB58_566
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_566:
	beqid	r3,	$0BB58_567
	NOP
	LWI	r8,	r1,	596
	LOAD	r3,	r8,	0
	LOAD	r4,	r8,	1
	LOAD	r6,	r8,	2
	ADDI	r7,	r8,	3
	LOAD	r5,	r7,	0
	LOAD	r9,	r7,	1
	LOAD	r20,	r7,	2
	ADDI	r8,	r8,	6
	LOAD	r11,	r8,	0
	LOAD	r7,	r8,	1
	LOAD	r8,	r8,	2
	FPRSUB	r12,	r8,	r6
	FPRSUB	r20,	r20,	r6
	FPRSUB	r6,	r9,	r4
	FPRSUB	r9,	r7,	r4
	FPMUL	r4,	r20,	r9
	FPMUL	r7,	r6,	r12
	FPRSUB	r4,	r4,	r7
	FPRSUB	r5,	r5,	r3
	FPRSUB	r11,	r11,	r3
	FPMUL	r3,	r20,	r11
	FPMUL	r7,	r5,	r12
	FPRSUB	r3,	r7,	r3
	FPMUL	r7,	r3,	r3
	FPMUL	r8,	r4,	r4
	FPADD	r7,	r8,	r7
	FPMUL	r6,	r6,	r11
	FPMUL	r5,	r5,	r9
	FPRSUB	r5,	r6,	r5
	FPMUL	r6,	r5,	r5
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r6,	r7,	r6
	FPDIV	r23,	r3,	r6
	FPMUL	r3,	r23,	r29
	FPDIV	r24,	r4,	r6
	LWI	r4,	r1,	576
	FPMUL	r4,	r24,	r4
	FPADD	r3,	r4,	r3
	FPDIV	r22,	r5,	r6
	FPMUL	r4,	r22,	r10
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB58_571
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB58_571:
	bneid	r3,	$0BB58_573
	NOP
	FPNEG	r22,	r22
	FPNEG	r23,	r23
	FPNEG	r24,	r24
$0BB58_573:
	LWI	r7,	r1,	572
	FPMUL	r3,	r29,	r7
	LWI	r4,	r1,	564
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	576
	FPMUL	r4,	r4,	r7
	LWI	r5,	r1,	568
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	696
	FPRSUB	r8,	r4,	r5
	LWI	r5,	r1,	700
	FPRSUB	r9,	r3,	r5
	FPMUL	r5,	r9,	r9
	FPMUL	r6,	r8,	r8
	FPADD	r6,	r6,	r5
	FPMUL	r5,	r10,	r7
	LWI	r7,	r1,	560
	FPADD	r5,	r7,	r5
	LWI	r7,	r1,	704
	FPRSUB	r10,	r5,	r7
	FPMUL	r7,	r10,	r10
	FPADD	r7,	r6,	r7
	LWI	r6,	r1,	592
	MULI	r6,	r6,	25
	LWI	r11,	r1,	716
	ADD	r11,	r11,	r6
	ORI	r6,	r0,	1028443341
	LOAD	r12,	r11,	0
	SWI	r12,	r1,	628
	LOAD	r12,	r11,	1
	SWI	r12,	r1,	632
	LOAD	r11,	r11,	2
	SWI	r11,	r1,	636
	FPINVSQRT	r11,	r7
	SWI	r11,	r1,	692
	FPINVSQRT	r11,	r7
	ORI	r7,	r0,	1065353216
	FPDIV	r11,	r7,	r11
	FPDIV	r12,	r8,	r11
	SWI	r12,	r1,	680
	FPDIV	r20,	r9,	r11
	SWI	r20,	r1,	684
	FPMUL	r8,	r20,	r20
	FPMUL	r9,	r12,	r12
	FPADD	r8,	r9,	r8
	FPDIV	r21,	r10,	r11
	SWI	r21,	r1,	688
	FPMUL	r9,	r21,	r21
	FPADD	r8,	r8,	r9
	FPINVSQRT	r8,	r8
	FPDIV	r9,	r7,	r8
	ORI	r8,	r0,	0
	FPDIV	r30,	r12,	r9
	FPDIV	r26,	r7,	r30
	FPGE	r10,	r26,	r8
	FPUN	r12,	r26,	r8
	FPDIV	r11,	r20,	r9
	SWI	r11,	r1,	572
	FPDIV	r25,	r7,	r11
	SWI	r25,	r1,	584
	FPGE	r11,	r25,	r8
	FPUN	r20,	r25,	r8
	BITOR	r20,	r20,	r11
	FPDIV	r9,	r21,	r9
	SWI	r9,	r1,	576
	FPDIV	r11,	r7,	r9
	SWI	r11,	r1,	592
	FPGE	r7,	r11,	r8
	FPUN	r9,	r11,	r8
	BITOR	r21,	r9,	r7
	FPLT	r7,	r26,	r8
	FPLT	r9,	r25,	r8
	FPLT	r11,	r11,	r8
	ADDI	r8,	r0,	0
	ADDI	r25,	r0,	1
	bneid	r21,	$0BB58_575
	SWI	r25,	r1,	600
	SWI	r8,	r1,	600
$0BB58_575:
	ADD	r21,	r22,	r0
	BITOR	r10,	r12,	r10
	bneid	r20,	$0BB58_577
	SWI	r25,	r1,	604
	SWI	r8,	r1,	604
$0BB58_577:
	SWI	r25,	r1,	608
	ADD	r12,	r21,	r0
	bneid	r10,	$0BB58_579
	ADD	r20,	r23,	r0
	SWI	r8,	r1,	608
$0BB58_579:
	bneid	r11,	$0BB58_581
	SWI	r25,	r1,	612
	SWI	r8,	r1,	612
$0BB58_581:
	SWI	r25,	r1,	616
	ADD	r22,	r26,	r0
	bneid	r9,	$0BB58_583
	SWI	r22,	r1,	624
	SWI	r8,	r1,	616
$0BB58_583:
	FPMUL	r9,	r24,	r6
	SWI	r24,	r1,	676
	FPMUL	r10,	r20,	r6
	SWI	r20,	r1,	672
	FPMUL	r6,	r12,	r6
	SWI	r12,	r1,	640
	bneid	r7,	$0BB58_585
	SWI	r25,	r1,	620
	SWI	r8,	r1,	620
$0BB58_585:
	FPADD	r5,	r5,	r6
	SWI	r5,	r1,	560
	FPADD	r3,	r3,	r10
	SWI	r3,	r1,	564
	FPADD	r3,	r4,	r9
	SWI	r3,	r1,	568
	ORI	r21,	r0,	1203982336
	brid	$0BB58_586
	SWI	r0,	r1,	336
$0BB58_691:
	beqid	r23,	$0BB58_692
	NOP
	FPGT	r3,	r7,	r4
	ADDI	r8,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r3,	$0BB58_695
	ADD	r9,	r5,	r0
	ADD	r9,	r8,	r0
$0BB58_695:
	bneid	r9,	$0BB58_697
	NOP
	ADD	r7,	r4,	r0
$0BB58_697:
	FPGE	r3,	r28,	r7
	FPUN	r4,	r28,	r7
	BITOR	r3,	r4,	r3
	bneid	r3,	$0BB58_699
	NOP
	ADD	r5,	r8,	r0
$0BB58_699:
	bneid	r5,	$0BB58_701
	NOP
	bslli	r3,	r25,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	SWI	r6,	r5,	-4
	LWI	r5,	r1,	580
	SW	r5,	r4,	r3
	brid	$0BB58_725
	ADDI	r25,	r25,	1
$0BB58_692:
	bslli	r3,	r25,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	brid	$0BB58_725
	SWI	r6,	r3,	-4
$0BB58_701:
	bslli	r3,	r25,	2
	ADDI	r4,	r1,	336
	ADD	r5,	r4,	r3
	LWI	r7,	r1,	580
	SWI	r7,	r5,	-4
	SW	r6,	r4,	r3
	brid	$0BB58_725
	ADDI	r25,	r25,	1
$0BB58_586:
	ADDI	r3,	r0,	-1
	ADD	r27,	r25,	r3
	bslli	r3,	r27,	2
	ADDI	r4,	r1,	336
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	588
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	468
	SWI	r0,	r1,	464
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	464
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	468
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	472
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	476
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	480
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	484
	LWI	r4,	r1,	604
	MULI	r11,	r4,	12
	ADDI	r7,	r1,	464
	ADD	r4,	r7,	r11
	ADDI	r5,	r3,	7
	ADDI	r3,	r3,	6
	LWI	r6,	r1,	620
	MULI	r29,	r6,	12
	LOAD	r23,	r3,	0
	SWI	r23,	r1,	488
	LOAD	r3,	r5,	0
	SWI	r3,	r1,	580
	SWI	r3,	r1,	492
	LWI	r3,	r4,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	584
	FPMUL	r24,	r3,	r4
	LW	r3,	r7,	r29
	LWI	r4,	r1,	568
	FPRSUB	r3,	r4,	r3
	FPMUL	r5,	r3,	r22
	ADDI	r4,	r0,	1
	FPGT	r3,	r5,	r24
	bneid	r3,	$0BB58_588
	NOP
	ADDI	r4,	r0,	0
$0BB58_588:
	bneid	r4,	$0BB58_589
	NOP
	LWI	r3,	r1,	616
	MULI	r4,	r3,	12
	ADD	r3,	r7,	r4
	LWI	r3,	r3,	4
	LWI	r6,	r1,	564
	FPRSUB	r3,	r6,	r3
	LWI	r6,	r1,	584
	FPMUL	r6,	r3,	r6
	LWI	r3,	r1,	608
	MULI	r8,	r3,	12
	LW	r3,	r7,	r8
	LWI	r7,	r1,	568
	FPRSUB	r3,	r7,	r3
	FPMUL	r10,	r3,	r22
	FPGT	r3,	r6,	r10
	bneid	r3,	$0BB58_592
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB58_592:
	bneid	r7,	$0BB58_593
	NOP
	FPGT	r3,	r6,	r5
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r3,	$0BB58_596
	ADD	r7,	r12,	r0
	ADD	r7,	r20,	r0
$0BB58_596:
	bneid	r7,	$0BB58_598
	NOP
	ADD	r6,	r5,	r0
$0BB58_598:
	LWI	r3,	r1,	600
	MULI	r7,	r3,	12
	ADDI	r26,	r1,	464
	ADD	r5,	r26,	r7
	FPLT	r3,	r24,	r10
	bneid	r3,	$0BB58_600
	ADD	r28,	r12,	r0
	ADD	r28,	r20,	r0
$0BB58_600:
	LWI	r3,	r5,	8
	LWI	r5,	r1,	560
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	592
	FPMUL	r5,	r3,	r5
	FPGT	r3,	r6,	r5
	bneid	r3,	$0BB58_602
	NOP
	ADD	r12,	r20,	r0
$0BB58_602:
	bneid	r28,	$0BB58_604
	NOP
	ADD	r24,	r10,	r0
$0BB58_604:
	bneid	r12,	$0BB58_605
	NOP
	LWI	r3,	r1,	612
	MULI	r10,	r3,	12
	ADD	r3,	r26,	r10
	LWI	r3,	r3,	8
	LWI	r9,	r1,	560
	FPRSUB	r3,	r9,	r3
	LWI	r9,	r1,	592
	FPMUL	r26,	r3,	r9
	FPGT	r3,	r26,	r24
	bneid	r3,	$0BB58_608
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB58_608:
	bneid	r9,	$0BB58_609
	NOP
	FPLT	r3,	r5,	r24
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r3,	$0BB58_612
	ADD	r9,	r12,	r0
	ADD	r9,	r20,	r0
$0BB58_612:
	bneid	r9,	$0BB58_614
	NOP
	ADD	r5,	r24,	r0
$0BB58_614:
	ORI	r3,	r0,	0
	FPLT	r3,	r5,	r3
	bneid	r3,	$0BB58_616
	NOP
	ADD	r12,	r20,	r0
$0BB58_616:
	bneid	r12,	$0BB58_617
	NOP
	FPGT	r3,	r26,	r6
	ADDI	r12,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r3,	$0BB58_620
	ADD	r9,	r5,	r0
	ADD	r9,	r12,	r0
$0BB58_620:
	bneid	r9,	$0BB58_622
	NOP
	ADD	r26,	r6,	r0
$0BB58_622:
	FPLT	r3,	r21,	r26
	bneid	r3,	$0BB58_624
	NOP
	ADD	r5,	r12,	r0
$0BB58_624:
	bneid	r5,	$0BB58_625
	NOP
	beqid	r23,	$0BB58_627
	NOP
	SWI	r27,	r1,	596
	ADD	r6,	r0,	r0
	ADDI	r3,	r0,	-1
	CMP	r3,	r3,	r23
	bneid	r3,	$0BB58_702
	NOP
	LWI	r3,	r1,	580
	bslli	r3,	r3,	3
	LWI	r5,	r1,	588
	ADD	r5,	r3,	r5
	SWI	r0,	r1,	500
	SWI	r0,	r1,	496
	LOAD	r3,	r5,	0
	SWI	r3,	r1,	496
	LOAD	r3,	r5,	1
	SWI	r3,	r1,	500
	LOAD	r3,	r5,	2
	SWI	r3,	r1,	504
	ADDI	r6,	r5,	3
	LOAD	r3,	r6,	0
	SWI	r3,	r1,	508
	LOAD	r3,	r6,	1
	SWI	r3,	r1,	512
	LOAD	r3,	r6,	2
	SWI	r3,	r1,	516
	ADDI	r3,	r5,	6
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	520
	ADDI	r3,	r5,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	524
	ADDI	r6,	r1,	496
	ADD	r3,	r6,	r11
	LWI	r3,	r3,	4
	LWI	r5,	r1,	564
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	584
	FPMUL	r31,	r3,	r5
	LW	r3,	r6,	r29
	LWI	r5,	r1,	568
	FPRSUB	r3,	r5,	r3
	FPMUL	r5,	r3,	r22
	ADDI	r12,	r0,	1
	FPGT	r3,	r5,	r31
	bneid	r3,	$0BB58_631
	NOP
	ADDI	r12,	r0,	0
$0BB58_631:
	ADD	r23,	r0,	r0
	ORI	r28,	r0,	1203982336
	bneid	r12,	$0BB58_661
	NOP
	ADD	r9,	r6,	r4
	LW	r3,	r6,	r8
	LWI	r6,	r9,	4
	LWI	r9,	r1,	564
	FPRSUB	r6,	r9,	r6
	LWI	r9,	r1,	584
	FPMUL	r6,	r6,	r9
	LWI	r9,	r1,	568
	FPRSUB	r3,	r9,	r3
	FPMUL	r24,	r3,	r22
	FPGT	r3,	r6,	r24
	bneid	r3,	$0BB58_634
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB58_634:
	bneid	r12,	$0BB58_661
	NOP
	FPGT	r3,	r6,	r5
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r3,	$0BB58_637
	ADD	r9,	r12,	r0
	ADD	r9,	r20,	r0
$0BB58_637:
	bneid	r9,	$0BB58_639
	NOP
	ADD	r6,	r5,	r0
$0BB58_639:
	ADDI	r5,	r1,	496
	ADD	r26,	r5,	r7
	FPLT	r3,	r31,	r24
	bneid	r3,	$0BB58_641
	ADD	r23,	r12,	r0
	ADD	r23,	r20,	r0
$0BB58_641:
	LWI	r3,	r26,	8
	LWI	r9,	r1,	560
	FPRSUB	r3,	r9,	r3
	LWI	r9,	r1,	592
	FPMUL	r26,	r3,	r9
	FPGT	r3,	r6,	r26
	bneid	r3,	$0BB58_643
	NOP
	ADD	r12,	r20,	r0
$0BB58_643:
	bneid	r23,	$0BB58_645
	NOP
	ADD	r31,	r24,	r0
$0BB58_645:
	ADD	r23,	r0,	r0
	ORI	r28,	r0,	1203982336
	bneid	r12,	$0BB58_661
	NOP
	ADD	r3,	r5,	r10
	LWI	r3,	r3,	8
	LWI	r5,	r1,	560
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	592
	FPMUL	r24,	r3,	r5
	FPGT	r3,	r24,	r31
	bneid	r3,	$0BB58_648
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_648:
	bneid	r5,	$0BB58_661
	NOP
	FPLT	r3,	r26,	r31
	ADDI	r12,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r3,	$0BB58_651
	ADD	r9,	r5,	r0
	ADD	r9,	r12,	r0
$0BB58_651:
	bneid	r9,	$0BB58_653
	NOP
	ADD	r26,	r31,	r0
$0BB58_653:
	ORI	r3,	r0,	0
	FPLT	r3,	r26,	r3
	bneid	r3,	$0BB58_655
	NOP
	ADD	r5,	r12,	r0
$0BB58_655:
	ADD	r23,	r0,	r0
	ORI	r28,	r0,	1203982336
	bneid	r5,	$0BB58_661
	NOP
	FPGT	r3,	r24,	r6
	ADDI	r23,	r0,	1
	bneid	r3,	$0BB58_658
	ADD	r5,	r23,	r0
	ADDI	r5,	r0,	0
$0BB58_658:
	bneid	r5,	$0BB58_660
	NOP
	ADD	r24,	r6,	r0
$0BB58_660:
	ADD	r28,	r24,	r0
$0BB58_661:
	LWI	r3,	r1,	580
	ADDI	r6,	r3,	1
	bslli	r3,	r6,	3
	LWI	r5,	r1,	588
	ADD	r5,	r3,	r5
	SWI	r0,	r1,	532
	SWI	r0,	r1,	528
	LOAD	r3,	r5,	0
	SWI	r3,	r1,	528
	LOAD	r3,	r5,	1
	SWI	r3,	r1,	532
	LOAD	r3,	r5,	2
	SWI	r3,	r1,	536
	ADDI	r9,	r5,	3
	LOAD	r3,	r9,	0
	SWI	r3,	r1,	540
	LOAD	r3,	r9,	1
	SWI	r3,	r1,	544
	LOAD	r3,	r9,	2
	SWI	r3,	r1,	548
	ADDI	r3,	r5,	6
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	552
	ADDI	r3,	r5,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	556
	ADDI	r12,	r1,	528
	ADD	r3,	r12,	r11
	LWI	r3,	r3,	4
	LWI	r5,	r1,	564
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	584
	FPMUL	r11,	r3,	r5
	LW	r3,	r12,	r29
	LWI	r5,	r1,	568
	FPRSUB	r3,	r5,	r3
	FPMUL	r5,	r3,	r22
	ADDI	r20,	r0,	1
	FPGT	r3,	r5,	r11
	bneid	r3,	$0BB58_663
	NOP
	ADDI	r20,	r0,	0
$0BB58_663:
	bneid	r20,	$0BB58_688
	LWI	r26,	r1,	596
	ADD	r3,	r12,	r4
	LWI	r3,	r3,	4
	LWI	r4,	r1,	564
	FPRSUB	r3,	r4,	r3
	LWI	r4,	r1,	584
	FPMUL	r4,	r3,	r4
	LW	r3,	r12,	r8
	LWI	r8,	r1,	568
	FPRSUB	r3,	r8,	r3
	FPMUL	r8,	r3,	r22
	FPGT	r3,	r4,	r8
	bneid	r3,	$0BB58_666
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB58_666:
	bneid	r9,	$0BB58_688
	NOP
	FPGT	r3,	r4,	r5
	ADDI	r24,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r3,	$0BB58_669
	ADD	r9,	r12,	r0
	ADD	r9,	r24,	r0
$0BB58_669:
	bneid	r9,	$0BB58_671
	NOP
	ADD	r4,	r5,	r0
$0BB58_671:
	ADDI	r5,	r1,	528
	ADD	r20,	r5,	r7
	FPLT	r3,	r11,	r8
	bneid	r3,	$0BB58_673
	ADD	r7,	r12,	r0
	ADD	r7,	r24,	r0
$0BB58_673:
	LWI	r3,	r20,	8
	LWI	r9,	r1,	560
	FPRSUB	r3,	r9,	r3
	LWI	r9,	r1,	592
	FPMUL	r20,	r3,	r9
	FPGT	r3,	r4,	r20
	bneid	r3,	$0BB58_675
	NOP
	ADD	r12,	r24,	r0
$0BB58_675:
	bneid	r7,	$0BB58_677
	NOP
	ADD	r11,	r8,	r0
$0BB58_677:
	bneid	r12,	$0BB58_688
	NOP
	ADD	r3,	r5,	r10
	LWI	r3,	r3,	8
	LWI	r5,	r1,	560
	FPRSUB	r3,	r5,	r3
	LWI	r5,	r1,	592
	FPMUL	r7,	r3,	r5
	FPGT	r3,	r7,	r11
	bneid	r3,	$0BB58_680
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_680:
	bneid	r5,	$0BB58_688
	NOP
	FPLT	r3,	r20,	r11
	ADDI	r8,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r3,	$0BB58_683
	ADD	r9,	r5,	r0
	ADD	r9,	r8,	r0
$0BB58_683:
	bneid	r9,	$0BB58_685
	NOP
	ADD	r20,	r11,	r0
$0BB58_685:
	ORI	r3,	r0,	0
	FPGE	r9,	r20,	r3
	FPUN	r3,	r20,	r3
	BITOR	r3,	r3,	r9
	bneid	r3,	$0BB58_687
	NOP
	ADD	r5,	r8,	r0
$0BB58_687:
	bneid	r5,	$0BB58_691
	NOP
$0BB58_688:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r23
	bneid	r3,	$0BB58_689
	NOP
	bslli	r3,	r25,	2
	ADDI	r4,	r1,	336
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	580
	brid	$0BB58_725
	SWI	r4,	r3,	-4
$0BB58_589:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_593:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_605:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_702:
	MULI	r3,	r6,	11
	LWI	r4,	r1,	580
	ADD	r5,	r3,	r4
	LOAD	r11,	r5,	0
	LOAD	r24,	r5,	1
	LOAD	r26,	r5,	2
	ADDI	r4,	r5,	6
	ADDI	r3,	r5,	3
	LOAD	r5,	r3,	0
	LOAD	r8,	r3,	1
	LOAD	r12,	r3,	2
	LOAD	r7,	r4,	0
	LOAD	r3,	r4,	1
	LOAD	r4,	r4,	2
	FPRSUB	r10,	r26,	r4
	FPRSUB	r25,	r24,	r3
	LWI	r9,	r1,	576
	FPMUL	r3,	r9,	r25
	LWI	r22,	r1,	572
	FPMUL	r4,	r22,	r10
	FPRSUB	r4,	r3,	r4
	FPRSUB	r29,	r11,	r7
	FPMUL	r3,	r9,	r29
	FPMUL	r7,	r30,	r10
	FPRSUB	r7,	r7,	r3
	FPRSUB	r5,	r11,	r5
	FPRSUB	r20,	r24,	r8
	FPMUL	r3,	r7,	r20
	FPMUL	r8,	r4,	r5
	FPADD	r9,	r8,	r3
	FPMUL	r3,	r22,	r29
	FPMUL	r8,	r30,	r25
	FPRSUB	r8,	r3,	r8
	FPRSUB	r12,	r26,	r12
	FPMUL	r3,	r8,	r12
	FPADD	r9,	r9,	r3
	ORI	r3,	r0,	0
	FPGE	r22,	r9,	r3
	FPUN	r3,	r9,	r3
	BITOR	r3,	r3,	r22
	bneid	r3,	$0BB58_704
	ADDI	r31,	r0,	1
	ADDI	r31,	r0,	0
$0BB58_704:
	bneid	r31,	$0BB58_706
	ADD	r28,	r9,	r0
	FPNEG	r28,	r9
$0BB58_706:
	ORI	r3,	r0,	953267991
	FPLT	r3,	r28,	r3
	bneid	r3,	$0BB58_708
	ADDI	r28,	r0,	1
	ADDI	r28,	r0,	0
$0BB58_708:
	bneid	r28,	$0BB58_723
	NOP
	LWI	r3,	r1,	560
	FPRSUB	r26,	r26,	r3
	LWI	r3,	r1,	568
	FPRSUB	r11,	r11,	r3
	LWI	r3,	r1,	564
	FPRSUB	r24,	r24,	r3
	FPMUL	r28,	r24,	r5
	FPMUL	r31,	r11,	r20
	FPMUL	r3,	r26,	r5
	FPMUL	r22,	r11,	r12
	FPMUL	r27,	r26,	r20
	FPMUL	r12,	r24,	r12
	FPRSUB	r5,	r28,	r31
	FPRSUB	r20,	r22,	r3
	FPRSUB	r12,	r27,	r12
	FPMUL	r3,	r20,	r25
	FPMUL	r22,	r12,	r29
	FPADD	r3,	r22,	r3
	FPMUL	r10,	r5,	r10
	FPADD	r3,	r3,	r10
	ORI	r10,	r0,	1065353216
	FPDIV	r25,	r10,	r9
	FPMUL	r10,	r3,	r25
	ORI	r3,	r0,	0
	FPLT	r3,	r10,	r3
	bneid	r3,	$0BB58_711
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB58_711:
	bneid	r9,	$0BB58_723
	NOP
	FPMUL	r3,	r7,	r24
	FPMUL	r4,	r4,	r11
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	572
	FPMUL	r4,	r20,	r4
	FPMUL	r7,	r12,	r30
	FPMUL	r8,	r8,	r26
	FPADD	r3,	r3,	r8
	FPADD	r4,	r7,	r4
	LWI	r7,	r1,	576
	FPMUL	r5,	r5,	r7
	FPADD	r4,	r4,	r5
	FPMUL	r4,	r4,	r25
	FPMUL	r3,	r3,	r25
	FPADD	r5,	r3,	r4
	ORI	r7,	r0,	1065353216
	FPLE	r8,	r5,	r7
	ORI	r5,	r0,	0
	FPGE	r11,	r3,	r5
	FPGE	r12,	r4,	r5
	ADDI	r7,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r8,	$0BB58_714
	ADD	r5,	r4,	r0
	ADD	r5,	r7,	r0
$0BB58_714:
	bneid	r12,	$0BB58_716
	ADD	r8,	r4,	r0
	ADD	r8,	r7,	r0
$0BB58_716:
	bneid	r11,	$0BB58_718
	ADD	r12,	r4,	r0
	ADD	r12,	r7,	r0
$0BB58_718:
	FPLT	r3,	r10,	r21
	bneid	r3,	$0BB58_720
	NOP
	ADD	r4,	r7,	r0
$0BB58_720:
	BITAND	r3,	r12,	r8
	BITAND	r3,	r3,	r5
	BITAND	r3,	r4,	r3
	bneid	r3,	$0BB58_722
	NOP
	ADD	r10,	r21,	r0
$0BB58_722:
	ADD	r21,	r10,	r0
$0BB58_723:
	ADDI	r6,	r6,	1
	CMP	r3,	r23,	r6
	bneid	r3,	$0BB58_702
	NOP
	LWI	r25,	r1,	596
	brid	$0BB58_725
	LWI	r22,	r1,	624
$0BB58_609:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_617:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_625:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_627:
	brid	$0BB58_725
	ADD	r25,	r27,	r0
$0BB58_689:
	ADD	r25,	r26,	r0
$0BB58_725:
	bgtid	r25,	$0BB58_586
	NOP
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	692
	FPDIV	r3,	r3,	r4
	FPGT	r4,	r3,	r21
	FPUN	r3,	r3,	r21
	BITOR	r3,	r3,	r4
	bneid	r3,	$0BB58_728
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_728:
	ORI	r3,	r0,	0
	ADD	r4,	r3,	r0
	ADD	r6,	r3,	r0
	LWI	r27,	r1,	640
	LWI	r28,	r1,	672
	bneid	r5,	$0BB58_730
	LWI	r20,	r1,	676
	LWI	r3,	r1,	684
	FPMUL	r3,	r28,	r3
	LWI	r4,	r1,	680
	FPMUL	r4,	r20,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	688
	FPMUL	r4,	r27,	r4
	FPADD	r3,	r3,	r4
	ORI	r5,	r0,	0
	FPMAX	r3,	r3,	r5
	LWI	r4,	r1,	628
	FPMUL	r6,	r4,	r3
	LWI	r4,	r1,	632
	FPMUL	r4,	r4,	r3
	LWI	r7,	r1,	636
	FPMUL	r3,	r7,	r3
	FPADD	r3,	r3,	r5
	FPADD	r4,	r4,	r5
	FPADD	r6,	r6,	r5
$0BB58_730:
	RAND	r5
	RAND	r7
	FPADD	r7,	r7,	r7
	ORI	r8,	r0,	-1082130432
	FPADD	r7,	r7,	r8
	FPADD	r5,	r5,	r5
	FPADD	r8,	r5,	r8
	FPMUL	r5,	r8,	r8
	FPMUL	r10,	r7,	r7
	FPADD	r9,	r5,	r10
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r9,	r11
	bneid	r11,	$0BB58_732
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB58_732:
	bneid	r9,	$0BB58_730
	NOP
	ORI	r9,	r0,	0
	FPGE	r11,	r20,	r9
	FPUN	r9,	r20,	r9
	BITOR	r9,	r9,	r11
	bneid	r9,	$0BB58_735
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB58_735:
	ORI	r9,	r0,	1065353216
	FPRSUB	r5,	r5,	r9
	FPRSUB	r5,	r10,	r5
	FPINVSQRT	r11,	r5
	bneid	r12,	$0BB58_737
	ADD	r10,	r20,	r0
	FPNEG	r10,	r20
$0BB58_737:
	ORI	r5,	r0,	0
	FPGE	r12,	r28,	r5
	FPUN	r5,	r28,	r5
	BITOR	r12,	r5,	r12
	bneid	r12,	$0BB58_739
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_739:
	ADD	r29,	r20,	r0
	ADD	r21,	r28,	r0
	bneid	r5,	$0BB58_741
	LWI	r25,	r1,	664
	FPNEG	r21,	r28
$0BB58_741:
	ORI	r5,	r0,	0
	FPGE	r12,	r27,	r5
	FPUN	r5,	r27,	r5
	BITOR	r5,	r5,	r12
	bneid	r5,	$0BB58_743
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB58_743:
	ADD	r5,	r27,	r0
	LWI	r20,	r1,	712
	LWI	r22,	r1,	644
	LWI	r23,	r1,	648
	bneid	r12,	$0BB58_745
	LWI	r24,	r1,	652
	FPNEG	r5,	r27
$0BB58_745:
	FPGE	r12,	r10,	r21
	FPUN	r20,	r10,	r21
	BITOR	r12,	r20,	r12
	ADDI	r20,	r0,	1
	bneid	r12,	$0BB58_747
	LWI	r26,	r1,	668
	ADDI	r20,	r0,	0
$0BB58_747:
	FPDIV	r9,	r9,	r11
	ORI	r12,	r0,	1065353216
	ORI	r11,	r0,	0
	bneid	r20,	$0BB58_751
	NOP
	FPLT	r10,	r10,	r5
	bneid	r10,	$0BB58_750
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB58_750:
	bneid	r20,	$0BB58_755
	ADD	r10,	r11,	r0
$0BB58_751:
	FPLT	r10,	r21,	r5
	bneid	r10,	$0BB58_753
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_753:
	ORI	r10,	r0,	1065353216
	ORI	r11,	r0,	0
	bneid	r5,	$0BB58_755
	ADD	r12,	r11,	r0
	ORI	r10,	r0,	0
	ORI	r11,	r0,	1065353216
	brid	$0BB58_755
	ADD	r12,	r10,	r0
$0BB58_23:
	LWI	r4,	r1,	644
	LWI	r5,	r1,	648
	brid	$0BB58_568
	LWI	r6,	r1,	652
$0BB58_567:
	ORI	r3,	r0,	1065151889
	LWI	r4,	r1,	664
	FPMUL	r3,	r4,	r3
	LWI	r4,	r1,	644
	FPADD	r4,	r4,	r3
	ORI	r3,	r0,	1060806590
	LWI	r5,	r1,	660
	FPMUL	r3,	r5,	r3
	LWI	r5,	r1,	648
	FPADD	r5,	r5,	r3
	ORI	r3,	r0,	1057988018
	LWI	r6,	r1,	656
	FPMUL	r3,	r6,	r3
	LWI	r6,	r1,	652
	FPADD	r6,	r6,	r3
$0BB58_568:
	LWI	r3,	r1,	720
	FPADD	r3,	r3,	r4
	SWI	r3,	r1,	720
	LWI	r3,	r1,	724
	FPADD	r3,	r3,	r5
	SWI	r3,	r1,	724
	LWI	r4,	r1,	732
	FPADD	r4,	r4,	r6
	LWI	r6,	r1,	728
	ADDI	r6,	r6,	1
	LWI	r3,	r1,	712
	CMP	r3,	r3,	r6
	bltid	r3,	$0BB58_15
	NOP
	LWI	r3,	r1,	800
	FPDIV	r4,	r4,	r3
	ORI	r3,	r0,	0
	FPLT	r6,	r4,	r3
	bneid	r6,	$0BB58_400
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_400:
	bneid	r5,	$0BB58_405
	LWI	r9,	r1,	808
	ORI	r3,	r0,	1065353216
	FPGT	r6,	r4,	r3
	bneid	r6,	$0BB58_403
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB58_403:
	bneid	r5,	$0BB58_405
	NOP
	ADD	r3,	r4,	r0
$0BB58_405:
	LWI	r4,	r1,	800
	LWI	r5,	r1,	724
	FPDIV	r5,	r5,	r4
	ORI	r4,	r0,	0
	FPLT	r7,	r5,	r4
	bneid	r7,	$0BB58_407
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB58_407:
	bneid	r6,	$0BB58_412
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r7,	r5,	r4
	bneid	r7,	$0BB58_410
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB58_410:
	bneid	r6,	$0BB58_412
	NOP
	ADD	r4,	r5,	r0
$0BB58_412:
	LWI	r5,	r1,	800
	LWI	r6,	r1,	720
	FPDIV	r6,	r6,	r5
	ORI	r5,	r0,	0
	FPLT	r8,	r6,	r5
	bneid	r8,	$0BB58_414
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB58_414:
	bneid	r7,	$0BB58_419
	NOP
	ORI	r5,	r0,	1065353216
	FPGT	r8,	r6,	r5
	bneid	r8,	$0BB58_417
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB58_417:
	bneid	r7,	$0BB58_419
	NOP
	ADD	r5,	r6,	r0
$0BB58_419:
	LWI	r6,	r1,	816
	LWI	r7,	r1,	820
	ADD	r6,	r6,	r7
	MULI	r6,	r6,	3
	ADD	r6,	r6,	r9
	STORE	r6,	r3,	0
	STORE	r6,	r4,	1
	STORE	r6,	r5,	2
	ATOMIC_INC	r3,	0
	LWI	r4,	r1,	804
	CMP	r4,	r4,	r3
	bltid	r4,	$0BB58_14
	NOP
	brid	$0BB58_757
	NOP
$0BB58_24:
	ADD	r10,	r9,	r0
	ORI	r4,	r0,	0
	LWI	r5,	r1,	800
	FPDIV	r5,	r4,	r5
	FPLT	r4,	r5,	r4
	ADDI	r8,	r0,	0
	ADDI	r7,	r0,	1
	bneid	r4,	$0BB58_26
	ADD	r6,	r7,	r0
	ADD	r6,	r8,	r0
$0BB58_26:
	ORI	r4,	r0,	1065353216
	FPGT	r9,	r5,	r4
	bneid	r9,	$0BB58_28
	NOP
	ADD	r7,	r8,	r0
$0BB58_28:
	bneid	r7,	$0BB58_30
	ADD	r8,	r10,	r0
	ADD	r4,	r5,	r0
$0BB58_30:
	beqid	r6,	$0BB58_31
	NOP
	ORI	r4,	r0,	1065353216
	FPLE	r6,	r5,	r4
	FPUN	r4,	r5,	r4
	BITOR	r5,	r4,	r6
	bneid	r5,	$0BB58_37
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB58_37:
	bneid	r4,	$0BB58_39
	NOP
$0BB58_38:
	MULI	r3,	r3,	3
	ADD	r3,	r3,	r8
	ORI	r4,	r0,	0
	STORE	r3,	r4,	0
	STORE	r3,	r4,	1
	STORE	r3,	r4,	2
	ATOMIC_INC	r3,	0
	CMP	r4,	r11,	r3
	bltid	r4,	$0BB58_38
	NOP
	brid	$0BB58_757
	NOP
$0BB58_39:
	MULI	r3,	r3,	3
	ADD	r3,	r3,	r8
	ORI	r4,	r0,	0
	STORE	r3,	r4,	0
	STORE	r3,	r4,	1
	STORE	r3,	r4,	2
	ATOMIC_INC	r3,	0
	CMP	r4,	r11,	r3
	bltid	r4,	$0BB58_39
	NOP
	brid	$0BB58_757
	NOP
$0BB58_31:
	ORI	r6,	r0,	1065353216
	FPGT	r7,	r5,	r6
	bneid	r7,	$0BB58_33
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB58_33:
	beqid	r6,	$0BB58_756
	NOP
$0BB58_34:
	MULI	r3,	r3,	3
	ADD	r3,	r3,	r8
	STORE	r3,	r4,	0
	STORE	r3,	r4,	1
	ORI	r5,	r0,	1065353216
	STORE	r3,	r5,	2
	ATOMIC_INC	r3,	0
	CMP	r5,	r11,	r3
	bltid	r5,	$0BB58_34
	NOP
	brid	$0BB58_757
	NOP
$0BB58_756:
	MULI	r3,	r3,	3
	ADD	r3,	r3,	r8
	STORE	r3,	r4,	0
	STORE	r3,	r4,	1
	STORE	r3,	r5,	2
	ATOMIC_INC	r3,	0
	CMP	r6,	r11,	r3
	bltid	r6,	$0BB58_756
	NOP
$0BB58_757:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	824
#	.end	_Z9trax_mainv
$0tmp58:
#	.size	_Z9trax_mainv, ($tmp58)-_Z9trax_mainv

#	.globl	_ZN5Scene4LoadEv
#	.align	2
#	.type	_ZN5Scene4LoadEv,@function
#	.ent	_ZN5Scene4LoadEv        # @_ZN5Scene4LoadEv
_ZN5Scene4LoadEv:
#	.frame	r1,0,r15
#	.mask	0x0
	ADD	r3,	r0,	r0
	LOAD	r4,	r3,	12
	LOAD	r6,	r4,	0
	SWI	r6,	r5,	0
	LOAD	r6,	r4,	1
	SWI	r6,	r5,	4
	LOAD	r4,	r4,	2
	SWI	r4,	r5,	8
	ADDI	r4,	r0,	1065353216
	SWI	r4,	r5,	12
	SWI	r4,	r5,	16
	SWI	r4,	r5,	20
	LOAD	r4,	r3,	28
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	29
	SWI	r4,	r5,	28
	LOAD	r4,	r3,	9
	SWI	r4,	r5,	32
	LOAD	r3,	r3,	8
	SWI	r3,	r5,	36
	ADDI	r3,	r0,	1057988018
	SWI	r3,	r5,	40
	ADDI	r3,	r0,	1060806590
	SWI	r3,	r5,	44
	ADDI	r3,	r0,	1065151889
	rtsd	r15,	8
	SWI	r3,	r5,	48
#	.end	_ZN5Scene4LoadEv
$0tmp59:
#	.size	_ZN5Scene4LoadEv, ($tmp59)-_ZN5Scene4LoadEv

#	.globl	_ZNK5Scene11BVHNodeAddrERKi
#	.align	2
#	.type	_ZNK5Scene11BVHNodeAddrERKi,@function
#	.ent	_ZNK5Scene11BVHNodeAddrERKi # @_ZNK5Scene11BVHNodeAddrERKi
_ZNK5Scene11BVHNodeAddrERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	36
	LWI	r4,	r6,	0
	bslli	r4,	r4,	3
	rtsd	r15,	8
	ADD	r3,	r4,	r3
#	.end	_ZNK5Scene11BVHNodeAddrERKi
$0tmp60:
#	.size	_ZNK5Scene11BVHNodeAddrERKi, ($tmp60)-_ZNK5Scene11BVHNodeAddrERKi

#	.globl	_ZNK5Scene8TraceRayERK3RayR9HitRecord
#	.align	2
#	.type	_ZNK5Scene8TraceRayERK3RayR9HitRecord,@function
#	.ent	_ZNK5Scene8TraceRayERK3RayR9HitRecord # @_ZNK5Scene8TraceRayERK3RayR9HitRecord
_ZNK5Scene8TraceRayERK3RayR9HitRecord:
#	.frame	r1,292,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-292
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r7,	r1,	304
	ADD	r30,	r6,	r0
	SWI	r30,	r1,	284
	SWI	r5,	r1,	296
	ADDI	r23,	r0,	1
	brid	$0BB61_1
	SWI	r0,	r1,	48
$0BB61_98:
	beqid	r9,	$0BB61_99
	NOP
	FPGT	r6,	r21,	r12
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB61_102
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB61_102:
	bneid	r5,	$0BB61_104
	NOP
	ADD	r21,	r12,	r0
$0BB61_104:
	FPGE	r5,	r10,	r21
	FPUN	r6,	r10,	r21
	BITOR	r5,	r6,	r5
	bneid	r5,	$0BB61_106
	NOP
	ADD	r3,	r4,	r0
$0BB61_106:
	bneid	r3,	$0BB61_108
	NOP
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r5,	r4,	r3
	SWI	r11,	r5,	-4
	SW	r26,	r4,	r3
	brid	$0BB61_134
	ADDI	r23,	r8,	1
$0BB61_99:
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r3,	r4,	r3
	SWI	r11,	r3,	-4
	brid	$0BB61_134
	ADD	r23,	r8,	r0
$0BB61_108:
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r5,	r4,	r3
	SWI	r26,	r5,	-4
	SW	r11,	r4,	r3
	brid	$0BB61_134
	ADDI	r23,	r8,	1
$0BB61_1:
	ADD	r8,	r23,	r0
	ADDI	r3,	r0,	-1
	ADD	r23,	r8,	r3
	bslli	r3,	r23,	2
	ADDI	r4,	r1,	48
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	296
	LWI	r4,	r4,	36
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	192
	SWI	r0,	r1,	188
	SWI	r0,	r1,	184
	SWI	r0,	r1,	180
	SWI	r0,	r1,	176
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	176
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	180
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	184
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	188
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	192
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	196
	ADDI	r4,	r3,	7
	ADDI	r3,	r3,	6
	LOAD	r25,	r3,	0
	SWI	r25,	r1,	200
	LOAD	r24,	r4,	0
	SWI	r24,	r1,	204
	LWI	r5,	r30,	40
	RSUBI	r3,	r5,	1
	MULI	r3,	r3,	12
	ADDI	r4,	r1,	176
	ADD	r6,	r4,	r3
	LWI	r9,	r30,	36
	MULI	r3,	r9,	12
	LW	r3,	r4,	r3
	LWI	r12,	r30,	0
	FPRSUB	r3,	r12,	r3
	LWI	r20,	r30,	24
	FPMUL	r3,	r3,	r20
	LWI	r6,	r6,	4
	LWI	r7,	r30,	4
	FPRSUB	r6,	r7,	r6
	LWI	r10,	r30,	28
	FPMUL	r11,	r6,	r10
	ADDI	r6,	r0,	1
	FPGT	r21,	r3,	r11
	bneid	r21,	$0BB61_3
	NOP
	ADDI	r6,	r0,	0
$0BB61_3:
	bneid	r6,	$0BB61_134
	NOP
	MULI	r5,	r5,	12
	ADD	r5,	r4,	r5
	LWI	r5,	r5,	4
	FPRSUB	r5,	r7,	r5
	FPMUL	r10,	r5,	r10
	RSUBI	r5,	r9,	1
	MULI	r5,	r5,	12
	LW	r4,	r4,	r5
	FPRSUB	r4,	r12,	r4
	FPMUL	r12,	r4,	r20
	FPGT	r5,	r10,	r12
	bneid	r5,	$0BB61_6
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_6:
	bneid	r4,	$0BB61_134
	NOP
	FPGT	r5,	r10,	r3
	ADDI	r21,	r0,	0
	ADDI	r20,	r0,	1
	bneid	r5,	$0BB61_9
	ADD	r4,	r20,	r0
	ADD	r4,	r21,	r0
$0BB61_9:
	bneid	r4,	$0BB61_11
	NOP
	ADD	r10,	r3,	r0
$0BB61_11:
	LWI	r3,	r30,	44
	RSUBI	r4,	r3,	1
	MULI	r5,	r4,	12
	ADDI	r4,	r1,	176
	ADD	r5,	r4,	r5
	FPLT	r6,	r11,	r12
	bneid	r6,	$0BB61_13
	ADD	r22,	r20,	r0
	ADD	r22,	r21,	r0
$0BB61_13:
	LWI	r5,	r5,	8
	LWI	r7,	r30,	8
	FPRSUB	r5,	r7,	r5
	LWI	r9,	r30,	32
	FPMUL	r5,	r5,	r9
	FPGT	r6,	r10,	r5
	bneid	r6,	$0BB61_15
	NOP
	ADD	r20,	r21,	r0
$0BB61_15:
	bneid	r22,	$0BB61_17
	NOP
	ADD	r11,	r12,	r0
$0BB61_17:
	bneid	r20,	$0BB61_134
	NOP
	ADD	r26,	r24,	r0
	MULI	r3,	r3,	12
	ADD	r3,	r4,	r3
	LWI	r3,	r3,	8
	FPRSUB	r3,	r7,	r3
	FPMUL	r3,	r3,	r9
	FPGT	r6,	r3,	r11
	bneid	r6,	$0BB61_20
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_20:
	bneid	r4,	$0BB61_134
	SWI	r26,	r1,	276
	FPLT	r9,	r5,	r11
	ADDI	r7,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r9,	$0BB61_23
	ADD	r6,	r4,	r0
	ADD	r6,	r7,	r0
$0BB61_23:
	bneid	r6,	$0BB61_25
	NOP
	ADD	r5,	r11,	r0
$0BB61_25:
	ORI	r6,	r0,	0
	FPLT	r5,	r5,	r6
	bneid	r5,	$0BB61_27
	NOP
	ADD	r4,	r7,	r0
$0BB61_27:
	bneid	r4,	$0BB61_134
	NOP
	FPGT	r7,	r3,	r10
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB61_30
	ADD	r6,	r4,	r0
	ADD	r6,	r5,	r0
$0BB61_30:
	bneid	r6,	$0BB61_32
	NOP
	ADD	r3,	r10,	r0
$0BB61_32:
	LWI	r6,	r1,	304
	LWI	r6,	r6,	0
	FPLT	r3,	r6,	r3
	bneid	r3,	$0BB61_34
	NOP
	ADD	r4,	r5,	r0
$0BB61_34:
	bneid	r4,	$0BB61_134
	NOP
	ADD	r4,	r25,	r0
	SWI	r4,	r1,	280
	beqid	r4,	$0BB61_134
	NOP
	SWI	r23,	r1,	288
	ADD	r10,	r0,	r0
	ADDI	r3,	r0,	-1
	CMP	r3,	r3,	r4
	bneid	r3,	$0BB61_109
	NOP
	LWI	r3,	r1,	296
	LWI	r3,	r3,	36
	bslli	r4,	r26,	3
	ADD	r3,	r4,	r3
	SWI	r0,	r1,	224
	SWI	r0,	r1,	220
	SWI	r0,	r1,	216
	SWI	r0,	r1,	212
	SWI	r0,	r1,	208
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	208
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	212
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	216
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	220
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	224
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	228
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	232
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	236
	LWI	r3,	r30,	40
	RSUBI	r4,	r3,	1
	MULI	r5,	r4,	12
	ADDI	r4,	r1,	208
	ADD	r6,	r4,	r5
	LWI	r11,	r30,	36
	MULI	r5,	r11,	12
	LW	r7,	r4,	r5
	LWI	r5,	r30,	0
	FPRSUB	r7,	r5,	r7
	LWI	r20,	r30,	24
	FPMUL	r21,	r7,	r20
	LWI	r6,	r6,	4
	LWI	r7,	r30,	4
	FPRSUB	r6,	r7,	r6
	LWI	r22,	r30,	28
	FPMUL	r12,	r6,	r22
	ADDI	r6,	r0,	1
	FPGT	r9,	r21,	r12
	bneid	r9,	$0BB61_39
	NOP
	ADDI	r6,	r0,	0
$0BB61_39:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r6,	$0BB61_69
	NOP
	RSUBI	r6,	r11,	1
	MULI	r6,	r6,	12
	LW	r6,	r4,	r6
	MULI	r3,	r3,	12
	ADD	r3,	r4,	r3
	LWI	r3,	r3,	4
	FPRSUB	r3,	r7,	r3
	FPMUL	r11,	r3,	r22
	FPRSUB	r3,	r5,	r6
	FPMUL	r20,	r3,	r20
	FPGT	r4,	r11,	r20
	bneid	r4,	$0BB61_42
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB61_42:
	bneid	r3,	$0BB61_69
	NOP
	FPGT	r4,	r11,	r21
	ADDI	r9,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r4,	$0BB61_45
	ADD	r3,	r5,	r0
	ADD	r3,	r9,	r0
$0BB61_45:
	bneid	r3,	$0BB61_47
	NOP
	ADD	r11,	r21,	r0
$0BB61_47:
	LWI	r21,	r30,	44
	RSUBI	r3,	r21,	1
	MULI	r3,	r3,	12
	ADDI	r4,	r1,	208
	ADD	r3,	r4,	r3
	FPLT	r6,	r12,	r20
	bneid	r6,	$0BB61_49
	ADD	r10,	r5,	r0
	ADD	r10,	r9,	r0
$0BB61_49:
	LWI	r3,	r3,	8
	LWI	r22,	r30,	8
	FPRSUB	r3,	r22,	r3
	LWI	r7,	r30,	32
	FPMUL	r3,	r3,	r7
	FPGT	r6,	r11,	r3
	bneid	r6,	$0BB61_51
	NOP
	ADD	r5,	r9,	r0
$0BB61_51:
	bneid	r10,	$0BB61_53
	NOP
	ADD	r12,	r20,	r0
$0BB61_53:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r5,	$0BB61_69
	NOP
	MULI	r5,	r21,	12
	ADD	r4,	r4,	r5
	LWI	r4,	r4,	8
	FPRSUB	r4,	r22,	r4
	FPMUL	r20,	r4,	r7
	FPGT	r5,	r20,	r12
	bneid	r5,	$0BB61_56
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_56:
	bneid	r4,	$0BB61_69
	NOP
	FPLT	r7,	r3,	r12
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB61_59
	ADD	r6,	r4,	r0
	ADD	r6,	r5,	r0
$0BB61_59:
	bneid	r6,	$0BB61_61
	NOP
	ADD	r3,	r12,	r0
$0BB61_61:
	ORI	r6,	r0,	0
	FPLT	r3,	r3,	r6
	bneid	r3,	$0BB61_63
	NOP
	ADD	r4,	r5,	r0
$0BB61_63:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r4,	$0BB61_69
	NOP
	FPGT	r4,	r20,	r11
	ADDI	r9,	r0,	1
	bneid	r4,	$0BB61_66
	ADD	r3,	r9,	r0
	ADDI	r3,	r0,	0
$0BB61_66:
	bneid	r3,	$0BB61_68
	NOP
	ADD	r20,	r11,	r0
$0BB61_68:
	ADD	r10,	r20,	r0
$0BB61_69:
	LWI	r3,	r1,	296
	LWI	r3,	r3,	36
	ADDI	r11,	r26,	1
	bslli	r4,	r11,	3
	ADD	r3,	r4,	r3
	SWI	r0,	r1,	256
	SWI	r0,	r1,	252
	SWI	r0,	r1,	248
	SWI	r0,	r1,	244
	SWI	r0,	r1,	240
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	240
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	244
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	248
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	252
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	256
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	260
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	264
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	268
	LWI	r4,	r30,	40
	RSUBI	r3,	r4,	1
	MULI	r5,	r3,	12
	ADDI	r3,	r1,	240
	ADD	r6,	r3,	r5
	LWI	r5,	r30,	36
	MULI	r7,	r5,	12
	LW	r7,	r3,	r7
	LWI	r21,	r30,	0
	FPRSUB	r7,	r21,	r7
	LWI	r23,	r30,	24
	FPMUL	r22,	r7,	r23
	LWI	r6,	r6,	4
	LWI	r7,	r30,	4
	FPRSUB	r6,	r7,	r6
	LWI	r12,	r30,	28
	FPMUL	r20,	r6,	r12
	ADDI	r6,	r0,	1
	FPGT	r24,	r22,	r20
	bneid	r24,	$0BB61_71
	NOP
	ADDI	r6,	r0,	0
$0BB61_71:
	bneid	r6,	$0BB61_96
	NOP
	MULI	r4,	r4,	12
	ADD	r4,	r3,	r4
	LWI	r4,	r4,	4
	FPRSUB	r4,	r7,	r4
	FPMUL	r12,	r4,	r12
	RSUBI	r4,	r5,	1
	MULI	r4,	r4,	12
	LW	r3,	r3,	r4
	FPRSUB	r3,	r21,	r3
	FPMUL	r21,	r3,	r23
	FPGT	r4,	r12,	r21
	bneid	r4,	$0BB61_74
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB61_74:
	bneid	r3,	$0BB61_96
	NOP
	FPGT	r4,	r12,	r22
	ADDI	r23,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r4,	$0BB61_77
	ADD	r3,	r5,	r0
	ADD	r3,	r23,	r0
$0BB61_77:
	bneid	r3,	$0BB61_79
	NOP
	ADD	r12,	r22,	r0
$0BB61_79:
	LWI	r4,	r30,	44
	RSUBI	r3,	r4,	1
	MULI	r3,	r3,	12
	ADDI	r22,	r1,	240
	ADD	r3,	r22,	r3
	FPLT	r6,	r20,	r21
	bneid	r6,	$0BB61_81
	ADD	r25,	r5,	r0
	ADD	r25,	r23,	r0
$0BB61_81:
	LWI	r3,	r3,	8
	LWI	r7,	r30,	8
	FPRSUB	r3,	r7,	r3
	LWI	r24,	r30,	32
	FPMUL	r3,	r3,	r24
	FPGT	r6,	r12,	r3
	bneid	r6,	$0BB61_83
	NOP
	ADD	r5,	r23,	r0
$0BB61_83:
	bneid	r25,	$0BB61_85
	NOP
	ADD	r20,	r21,	r0
$0BB61_85:
	bneid	r5,	$0BB61_96
	NOP
	MULI	r4,	r4,	12
	ADD	r4,	r22,	r4
	LWI	r4,	r4,	8
	FPRSUB	r4,	r7,	r4
	FPMUL	r21,	r4,	r24
	FPGT	r5,	r21,	r20
	bneid	r5,	$0BB61_88
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_88:
	bneid	r4,	$0BB61_96
	NOP
	FPLT	r7,	r3,	r20
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB61_91
	ADD	r6,	r4,	r0
	ADD	r6,	r5,	r0
$0BB61_91:
	bneid	r6,	$0BB61_93
	NOP
	ADD	r3,	r20,	r0
$0BB61_93:
	ORI	r6,	r0,	0
	FPGE	r7,	r3,	r6
	FPUN	r3,	r3,	r6
	BITOR	r3,	r3,	r7
	bneid	r3,	$0BB61_95
	NOP
	ADD	r4,	r5,	r0
$0BB61_95:
	bneid	r4,	$0BB61_98
	NOP
$0BB61_96:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r9
	bneid	r3,	$0BB61_134
	LWI	r23,	r1,	288
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r3,	r4,	r3
	SWI	r26,	r3,	-4
	brid	$0BB61_134
	ADD	r23,	r8,	r0
$0BB61_109:
	MULI	r3,	r10,	11
	ADD	r8,	r26,	r3
	LOAD	r25,	r8,	0
	LOAD	r26,	r8,	1
	LOAD	r28,	r8,	2
	ADDI	r4,	r8,	3
	LOAD	r3,	r4,	0
	LOAD	r5,	r4,	1
	LOAD	r4,	r4,	2
	ADDI	r6,	r8,	6
	LOAD	r7,	r6,	0
	LOAD	r9,	r6,	1
	LOAD	r6,	r6,	2
	FPRSUB	r22,	r28,	r6
	FPRSUB	r27,	r26,	r9
	LWI	r11,	r30,	20
	FPMUL	r6,	r11,	r27
	LWI	r12,	r30,	16
	FPMUL	r9,	r12,	r22
	FPRSUB	r9,	r6,	r9
	SWI	r9,	r1,	272
	FPRSUB	r29,	r25,	r7
	LWI	r20,	r30,	12
	FPMUL	r6,	r20,	r22
	FPMUL	r7,	r11,	r29
	FPRSUB	r23,	r6,	r7
	FPRSUB	r3,	r25,	r3
	FPRSUB	r31,	r26,	r5
	FPMUL	r5,	r23,	r31
	FPMUL	r6,	r9,	r3
	FPADD	r6,	r6,	r5
	FPMUL	r5,	r12,	r29
	FPMUL	r7,	r20,	r27
	FPRSUB	r24,	r5,	r7
	FPRSUB	r5,	r28,	r4
	FPMUL	r4,	r24,	r5
	FPADD	r6,	r6,	r4
	ORI	r4,	r0,	0
	FPGE	r7,	r6,	r4
	FPUN	r4,	r6,	r4
	BITOR	r4,	r4,	r7
	bneid	r4,	$0BB61_111
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB61_111:
	LWI	r4,	r30,	8
	LWI	r9,	r30,	0
	LWI	r21,	r30,	4
	bneid	r7,	$0BB61_113
	ADD	r30,	r6,	r0
	FPNEG	r30,	r6
$0BB61_113:
	ORI	r7,	r0,	953267991
	FPLT	r30,	r30,	r7
	bneid	r30,	$0BB61_115
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB61_115:
	bneid	r7,	$0BB61_132
	NOP
	FPRSUB	r28,	r28,	r4
	FPRSUB	r4,	r25,	r9
	FPRSUB	r9,	r26,	r21
	FPMUL	r7,	r9,	r3
	FPMUL	r21,	r4,	r31
	FPMUL	r25,	r28,	r3
	FPMUL	r26,	r4,	r5
	FPMUL	r30,	r28,	r31
	FPMUL	r5,	r9,	r5
	FPRSUB	r3,	r7,	r21
	FPRSUB	r25,	r26,	r25
	FPRSUB	r5,	r30,	r5
	FPMUL	r7,	r25,	r27
	FPMUL	r21,	r5,	r29
	FPADD	r7,	r21,	r7
	FPMUL	r21,	r3,	r22
	FPADD	r7,	r7,	r21
	ORI	r21,	r0,	1065353216
	FPDIV	r26,	r21,	r6
	FPMUL	r22,	r7,	r26
	ORI	r6,	r0,	0
	FPLT	r7,	r22,	r6
	bneid	r7,	$0BB61_118
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB61_118:
	bneid	r6,	$0BB61_132
	NOP
	FPMUL	r6,	r23,	r9
	LWI	r7,	r1,	272
	FPMUL	r4,	r7,	r4
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r24,	r28
	FPADD	r4,	r4,	r6
	FPMUL	r21,	r4,	r26
	ORI	r4,	r0,	0
	FPLT	r6,	r21,	r4
	FPUN	r4,	r21,	r4
	BITOR	r6,	r4,	r6
	bneid	r6,	$0BB61_121
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_121:
	bneid	r4,	$0BB61_132
	NOP
	FPMUL	r4,	r25,	r12
	FPMUL	r5,	r5,	r20
	FPADD	r4,	r5,	r4
	FPMUL	r3,	r3,	r11
	FPADD	r3,	r4,	r3
	FPMUL	r3,	r3,	r26
	ORI	r4,	r0,	0
	FPLT	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB61_124
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB61_124:
	bneid	r4,	$0BB61_132
	NOP
	FPADD	r3,	r21,	r3
	ORI	r4,	r0,	1065353216
	FPGT	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB61_127
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB61_127:
	bneid	r3,	$0BB61_132
	NOP
	LWI	r3,	r1,	304
	LWI	r3,	r3,	0
	FPGE	r4,	r22,	r3
	FPUN	r3,	r22,	r3
	BITOR	r4,	r3,	r4
	bneid	r4,	$0BB61_130
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB61_130:
	bneid	r3,	$0BB61_132
	NOP
	LWI	r4,	r1,	304
	SWI	r22,	r4,	0
	ADDI	r3,	r8,	10
	LOAD	r3,	r3,	0
	SWI	r3,	r4,	4
	SWI	r8,	r4,	8
$0BB61_132:
	ADDI	r10,	r10,	1
	LWI	r3,	r1,	280
	CMP	r3,	r3,	r10
	LWI	r30,	r1,	284
	bneid	r3,	$0BB61_109
	LWI	r26,	r1,	276
	LWI	r23,	r1,	288
$0BB61_134:
	bgtid	r23,	$0BB61_1
	NOP
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	292
#	.end	_ZNK5Scene8TraceRayERK3RayR9HitRecord
$0tmp61:
#	.size	_ZNK5Scene8TraceRayERK3RayR9HitRecord, ($tmp61)-_ZNK5Scene8TraceRayERK3RayR9HitRecord

#	.globl	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
#	.align	2
#	.type	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist,@function
#	.ent	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist # @_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
_ZNK5Scene14TraceShadowRayERK3RayR7HitDist:
#	.frame	r1,288,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-288
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r7,	r1,	300
	ADD	r29,	r6,	r0
	SWI	r29,	r1,	280
	SWI	r5,	r1,	292
	ADDI	r22,	r0,	1
	brid	$0BB62_1
	SWI	r0,	r1,	48
$0BB62_98:
	beqid	r9,	$0BB62_99
	NOP
	FPGT	r6,	r21,	r12
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB62_102
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB62_102:
	bneid	r5,	$0BB62_104
	NOP
	ADD	r21,	r12,	r0
$0BB62_104:
	FPGE	r5,	r10,	r21
	FPUN	r6,	r10,	r21
	BITOR	r5,	r6,	r5
	bneid	r5,	$0BB62_106
	NOP
	ADD	r3,	r4,	r0
$0BB62_106:
	bneid	r3,	$0BB62_108
	NOP
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r5,	r4,	r3
	SWI	r11,	r5,	-4
	SW	r26,	r4,	r3
	brid	$0BB62_134
	ADDI	r22,	r8,	1
$0BB62_99:
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r3,	r4,	r3
	SWI	r11,	r3,	-4
	brid	$0BB62_134
	ADD	r22,	r8,	r0
$0BB62_108:
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r5,	r4,	r3
	SWI	r26,	r5,	-4
	SW	r11,	r4,	r3
	brid	$0BB62_134
	ADDI	r22,	r8,	1
$0BB62_1:
	ADD	r8,	r22,	r0
	ADDI	r3,	r0,	-1
	ADD	r22,	r8,	r3
	bslli	r3,	r22,	2
	ADDI	r4,	r1,	48
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	292
	LWI	r4,	r4,	36
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	192
	SWI	r0,	r1,	188
	SWI	r0,	r1,	184
	SWI	r0,	r1,	180
	SWI	r0,	r1,	176
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	176
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	180
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	184
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	188
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	192
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	196
	ADDI	r4,	r3,	7
	ADDI	r3,	r3,	6
	LOAD	r25,	r3,	0
	SWI	r25,	r1,	200
	LOAD	r24,	r4,	0
	SWI	r24,	r1,	204
	LWI	r4,	r29,	40
	RSUBI	r3,	r4,	1
	MULI	r5,	r3,	12
	ADDI	r3,	r1,	176
	ADD	r7,	r3,	r5
	LWI	r5,	r29,	36
	MULI	r6,	r5,	12
	LW	r6,	r3,	r6
	LWI	r12,	r29,	0
	FPRSUB	r6,	r12,	r6
	LWI	r21,	r29,	24
	FPMUL	r20,	r6,	r21
	LWI	r6,	r7,	4
	LWI	r9,	r29,	4
	FPRSUB	r6,	r9,	r6
	LWI	r10,	r29,	28
	FPMUL	r11,	r6,	r10
	ADDI	r7,	r0,	1
	FPGT	r6,	r20,	r11
	bneid	r6,	$0BB62_3
	NOP
	ADDI	r7,	r0,	0
$0BB62_3:
	bneid	r7,	$0BB62_134
	NOP
	MULI	r4,	r4,	12
	ADD	r4,	r3,	r4
	LWI	r4,	r4,	4
	FPRSUB	r4,	r9,	r4
	FPMUL	r10,	r4,	r10
	RSUBI	r4,	r5,	1
	MULI	r4,	r4,	12
	LW	r3,	r3,	r4
	FPRSUB	r3,	r12,	r3
	FPMUL	r12,	r3,	r21
	FPGT	r4,	r10,	r12
	bneid	r4,	$0BB62_6
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_6:
	bneid	r3,	$0BB62_134
	NOP
	ADD	r7,	r22,	r0
	FPGT	r4,	r10,	r20
	ADDI	r22,	r0,	0
	ADDI	r21,	r0,	1
	bneid	r4,	$0BB62_9
	ADD	r3,	r21,	r0
	ADD	r3,	r22,	r0
$0BB62_9:
	bneid	r3,	$0BB62_11
	NOP
	ADD	r10,	r20,	r0
$0BB62_11:
	LWI	r5,	r29,	44
	RSUBI	r3,	r5,	1
	MULI	r3,	r3,	12
	ADDI	r4,	r1,	176
	ADD	r3,	r4,	r3
	FPLT	r6,	r11,	r12
	bneid	r6,	$0BB62_13
	ADD	r23,	r21,	r0
	ADD	r23,	r22,	r0
$0BB62_13:
	LWI	r3,	r3,	8
	LWI	r9,	r29,	8
	FPRSUB	r3,	r9,	r3
	LWI	r20,	r29,	32
	FPMUL	r3,	r3,	r20
	FPGT	r6,	r10,	r3
	bneid	r6,	$0BB62_15
	NOP
	ADD	r21,	r22,	r0
$0BB62_15:
	bneid	r23,	$0BB62_17
	NOP
	ADD	r11,	r12,	r0
$0BB62_17:
	bneid	r21,	$0BB62_134
	ADD	r22,	r7,	r0
	ADD	r26,	r24,	r0
	MULI	r5,	r5,	12
	ADD	r4,	r4,	r5
	LWI	r4,	r4,	8
	FPRSUB	r4,	r9,	r4
	FPMUL	r12,	r4,	r20
	FPGT	r5,	r12,	r11
	bneid	r5,	$0BB62_20
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB62_20:
	bneid	r4,	$0BB62_134
	SWI	r26,	r1,	272
	FPLT	r7,	r3,	r11
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB62_23
	ADD	r6,	r4,	r0
	ADD	r6,	r5,	r0
$0BB62_23:
	bneid	r6,	$0BB62_25
	NOP
	ADD	r3,	r11,	r0
$0BB62_25:
	ORI	r6,	r0,	0
	FPLT	r3,	r3,	r6
	bneid	r3,	$0BB62_27
	NOP
	ADD	r4,	r5,	r0
$0BB62_27:
	bneid	r4,	$0BB62_134
	NOP
	FPGT	r6,	r12,	r10
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB62_30
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB62_30:
	bneid	r5,	$0BB62_32
	NOP
	ADD	r12,	r10,	r0
$0BB62_32:
	LWI	r5,	r1,	300
	LWI	r5,	r5,	0
	FPLT	r5,	r5,	r12
	bneid	r5,	$0BB62_34
	NOP
	ADD	r3,	r4,	r0
$0BB62_34:
	ADD	r4,	r25,	r0
	bneid	r3,	$0BB62_134
	SWI	r4,	r1,	276
	beqid	r4,	$0BB62_134
	NOP
	SWI	r22,	r1,	284
	ADD	r10,	r0,	r0
	ADDI	r3,	r0,	-1
	CMP	r3,	r3,	r4
	bneid	r3,	$0BB62_109
	NOP
	LWI	r3,	r1,	292
	LWI	r3,	r3,	36
	bslli	r4,	r26,	3
	ADD	r3,	r4,	r3
	SWI	r0,	r1,	224
	SWI	r0,	r1,	220
	SWI	r0,	r1,	216
	SWI	r0,	r1,	212
	SWI	r0,	r1,	208
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	208
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	212
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	216
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	220
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	224
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	228
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	232
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	236
	LWI	r3,	r29,	40
	RSUBI	r4,	r3,	1
	MULI	r5,	r4,	12
	ADDI	r4,	r1,	208
	ADD	r7,	r4,	r5
	LWI	r11,	r29,	36
	MULI	r5,	r11,	12
	LW	r6,	r4,	r5
	LWI	r5,	r29,	0
	FPRSUB	r6,	r5,	r6
	LWI	r20,	r29,	24
	FPMUL	r21,	r6,	r20
	LWI	r6,	r7,	4
	LWI	r7,	r29,	4
	FPRSUB	r6,	r7,	r6
	LWI	r22,	r29,	28
	FPMUL	r12,	r6,	r22
	ADDI	r23,	r0,	1
	FPGT	r6,	r21,	r12
	bneid	r6,	$0BB62_39
	NOP
	ADDI	r23,	r0,	0
$0BB62_39:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r23,	$0BB62_69
	NOP
	RSUBI	r6,	r11,	1
	MULI	r6,	r6,	12
	LW	r6,	r4,	r6
	MULI	r3,	r3,	12
	ADD	r3,	r4,	r3
	LWI	r3,	r3,	4
	FPRSUB	r3,	r7,	r3
	FPMUL	r11,	r3,	r22
	FPRSUB	r3,	r5,	r6
	FPMUL	r20,	r3,	r20
	FPGT	r4,	r11,	r20
	bneid	r4,	$0BB62_42
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_42:
	bneid	r3,	$0BB62_69
	NOP
	FPGT	r5,	r11,	r21
	ADDI	r9,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r5,	$0BB62_45
	ADD	r4,	r3,	r0
	ADD	r4,	r9,	r0
$0BB62_45:
	bneid	r4,	$0BB62_47
	NOP
	ADD	r11,	r21,	r0
$0BB62_47:
	LWI	r22,	r29,	44
	RSUBI	r4,	r22,	1
	MULI	r4,	r4,	12
	ADDI	r5,	r1,	208
	ADD	r4,	r5,	r4
	FPLT	r6,	r12,	r20
	bneid	r6,	$0BB62_49
	ADD	r10,	r3,	r0
	ADD	r10,	r9,	r0
$0BB62_49:
	LWI	r6,	r4,	8
	LWI	r4,	r29,	8
	FPRSUB	r6,	r4,	r6
	LWI	r7,	r29,	32
	FPMUL	r21,	r6,	r7
	FPGT	r6,	r11,	r21
	bneid	r6,	$0BB62_51
	NOP
	ADD	r3,	r9,	r0
$0BB62_51:
	bneid	r10,	$0BB62_53
	NOP
	ADD	r12,	r20,	r0
$0BB62_53:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r3,	$0BB62_69
	NOP
	MULI	r3,	r22,	12
	ADD	r3,	r5,	r3
	LWI	r3,	r3,	8
	FPRSUB	r3,	r4,	r3
	FPMUL	r20,	r3,	r7
	FPGT	r4,	r20,	r12
	bneid	r4,	$0BB62_56
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_56:
	bneid	r3,	$0BB62_69
	NOP
	FPLT	r6,	r21,	r12
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB62_59
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB62_59:
	bneid	r5,	$0BB62_61
	NOP
	ADD	r21,	r12,	r0
$0BB62_61:
	ORI	r5,	r0,	0
	FPLT	r5,	r21,	r5
	bneid	r5,	$0BB62_63
	NOP
	ADD	r3,	r4,	r0
$0BB62_63:
	ADD	r9,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r3,	$0BB62_69
	NOP
	FPGT	r4,	r20,	r11
	ADDI	r9,	r0,	1
	bneid	r4,	$0BB62_66
	ADD	r3,	r9,	r0
	ADDI	r3,	r0,	0
$0BB62_66:
	bneid	r3,	$0BB62_68
	NOP
	ADD	r20,	r11,	r0
$0BB62_68:
	ADD	r10,	r20,	r0
$0BB62_69:
	LWI	r3,	r1,	292
	LWI	r3,	r3,	36
	ADDI	r11,	r26,	1
	bslli	r4,	r11,	3
	ADD	r3,	r4,	r3
	SWI	r0,	r1,	256
	SWI	r0,	r1,	252
	SWI	r0,	r1,	248
	SWI	r0,	r1,	244
	SWI	r0,	r1,	240
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	240
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	244
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	248
	ADDI	r4,	r3,	3
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	252
	LOAD	r5,	r4,	1
	SWI	r5,	r1,	256
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	260
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	264
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	268
	LWI	r4,	r29,	40
	RSUBI	r3,	r4,	1
	MULI	r5,	r3,	12
	ADDI	r3,	r1,	240
	ADD	r7,	r3,	r5
	LWI	r5,	r29,	36
	MULI	r6,	r5,	12
	LW	r6,	r3,	r6
	LWI	r21,	r29,	0
	FPRSUB	r6,	r21,	r6
	LWI	r23,	r29,	24
	FPMUL	r22,	r6,	r23
	LWI	r6,	r7,	4
	LWI	r7,	r29,	4
	FPRSUB	r6,	r7,	r6
	LWI	r12,	r29,	28
	FPMUL	r20,	r6,	r12
	ADDI	r24,	r0,	1
	FPGT	r6,	r22,	r20
	bneid	r6,	$0BB62_71
	NOP
	ADDI	r24,	r0,	0
$0BB62_71:
	bneid	r24,	$0BB62_96
	NOP
	MULI	r4,	r4,	12
	ADD	r4,	r3,	r4
	LWI	r4,	r4,	4
	FPRSUB	r4,	r7,	r4
	FPMUL	r12,	r4,	r12
	RSUBI	r4,	r5,	1
	MULI	r4,	r4,	12
	LW	r3,	r3,	r4
	FPRSUB	r3,	r21,	r3
	FPMUL	r21,	r3,	r23
	FPGT	r4,	r12,	r21
	bneid	r4,	$0BB62_74
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_74:
	bneid	r3,	$0BB62_96
	NOP
	FPGT	r5,	r12,	r22
	ADDI	r23,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r5,	$0BB62_77
	ADD	r4,	r3,	r0
	ADD	r4,	r23,	r0
$0BB62_77:
	bneid	r4,	$0BB62_79
	NOP
	ADD	r12,	r22,	r0
$0BB62_79:
	LWI	r5,	r29,	44
	RSUBI	r4,	r5,	1
	MULI	r6,	r4,	12
	ADDI	r4,	r1,	240
	ADD	r7,	r4,	r6
	FPLT	r6,	r20,	r21
	bneid	r6,	$0BB62_81
	ADD	r25,	r3,	r0
	ADD	r25,	r23,	r0
$0BB62_81:
	LWI	r6,	r7,	8
	LWI	r7,	r29,	8
	FPRSUB	r6,	r7,	r6
	LWI	r24,	r29,	32
	FPMUL	r22,	r6,	r24
	FPGT	r6,	r12,	r22
	bneid	r6,	$0BB62_83
	NOP
	ADD	r3,	r23,	r0
$0BB62_83:
	bneid	r25,	$0BB62_85
	NOP
	ADD	r20,	r21,	r0
$0BB62_85:
	bneid	r3,	$0BB62_96
	NOP
	MULI	r3,	r5,	12
	ADD	r3,	r4,	r3
	LWI	r3,	r3,	8
	FPRSUB	r3,	r7,	r3
	FPMUL	r21,	r3,	r24
	FPGT	r4,	r21,	r20
	bneid	r4,	$0BB62_88
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_88:
	bneid	r3,	$0BB62_96
	NOP
	FPLT	r6,	r22,	r20
	ADDI	r4,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB62_91
	ADD	r5,	r3,	r0
	ADD	r5,	r4,	r0
$0BB62_91:
	bneid	r5,	$0BB62_93
	NOP
	ADD	r22,	r20,	r0
$0BB62_93:
	ORI	r5,	r0,	0
	FPGE	r6,	r22,	r5
	FPUN	r5,	r22,	r5
	BITOR	r5,	r5,	r6
	bneid	r5,	$0BB62_95
	NOP
	ADD	r3,	r4,	r0
$0BB62_95:
	bneid	r3,	$0BB62_98
	NOP
$0BB62_96:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r9
	bneid	r3,	$0BB62_134
	LWI	r22,	r1,	284
	bslli	r3,	r8,	2
	ADDI	r4,	r1,	48
	ADD	r3,	r4,	r3
	SWI	r26,	r3,	-4
	brid	$0BB62_134
	ADD	r22,	r8,	r0
$0BB62_109:
	MULI	r3,	r10,	11
	ADD	r3,	r26,	r3
	LOAD	r24,	r3,	0
	LOAD	r25,	r3,	1
	LOAD	r27,	r3,	2
	ADDI	r6,	r3,	6
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	LOAD	r5,	r3,	1
	LOAD	r3,	r3,	2
	LOAD	r7,	r6,	0
	LOAD	r8,	r6,	1
	LOAD	r6,	r6,	2
	FPRSUB	r21,	r27,	r6
	FPRSUB	r26,	r25,	r8
	LWI	r8,	r29,	20
	FPMUL	r6,	r8,	r26
	LWI	r11,	r29,	16
	FPMUL	r9,	r11,	r21
	FPRSUB	r6,	r6,	r9
	FPRSUB	r28,	r24,	r7
	LWI	r12,	r29,	12
	FPMUL	r7,	r12,	r21
	FPMUL	r9,	r8,	r28
	FPRSUB	r22,	r7,	r9
	FPRSUB	r31,	r24,	r4
	FPRSUB	r30,	r25,	r5
	FPMUL	r4,	r22,	r30
	FPMUL	r5,	r6,	r31
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r11,	r28
	FPMUL	r7,	r12,	r26
	FPRSUB	r23,	r5,	r7
	FPRSUB	r3,	r27,	r3
	FPMUL	r5,	r23,	r3
	FPADD	r20,	r4,	r5
	ORI	r4,	r0,	0
	FPGE	r5,	r20,	r4
	FPUN	r4,	r20,	r4
	BITOR	r4,	r4,	r5
	bneid	r4,	$0BB62_111
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB62_111:
	LWI	r4,	r29,	8
	LWI	r5,	r29,	0
	LWI	r9,	r29,	4
	bneid	r7,	$0BB62_113
	ADD	r29,	r20,	r0
	FPNEG	r29,	r20
$0BB62_113:
	ORI	r7,	r0,	953267991
	FPLT	r29,	r29,	r7
	bneid	r29,	$0BB62_115
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB62_115:
	bneid	r7,	$0BB62_132
	NOP
	FPRSUB	r27,	r27,	r4
	FPRSUB	r5,	r24,	r5
	FPRSUB	r4,	r25,	r9
	FPMUL	r7,	r4,	r31
	FPMUL	r9,	r5,	r30
	FPMUL	r25,	r27,	r31
	FPMUL	r29,	r5,	r3
	FPMUL	r30,	r27,	r30
	FPMUL	r3,	r4,	r3
	FPRSUB	r24,	r7,	r9
	FPRSUB	r25,	r29,	r25
	FPRSUB	r3,	r30,	r3
	FPMUL	r7,	r25,	r26
	FPMUL	r9,	r3,	r28
	FPADD	r7,	r9,	r7
	FPMUL	r9,	r24,	r21
	FPADD	r7,	r7,	r9
	ORI	r9,	r0,	1065353216
	FPDIV	r26,	r9,	r20
	FPMUL	r21,	r7,	r26
	ORI	r7,	r0,	0
	FPLT	r9,	r21,	r7
	bneid	r9,	$0BB62_118
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB62_118:
	bneid	r7,	$0BB62_132
	NOP
	FPMUL	r4,	r22,	r4
	FPMUL	r5,	r6,	r5
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r23,	r27
	FPADD	r4,	r4,	r5
	FPMUL	r20,	r4,	r26
	ORI	r4,	r0,	0
	FPLT	r5,	r20,	r4
	FPUN	r4,	r20,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB62_121
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB62_121:
	bneid	r4,	$0BB62_132
	NOP
	FPMUL	r4,	r25,	r11
	FPMUL	r3,	r3,	r12
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r24,	r8
	FPADD	r3,	r3,	r4
	FPMUL	r3,	r3,	r26
	ORI	r4,	r0,	0
	FPLT	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB62_124
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB62_124:
	bneid	r4,	$0BB62_132
	NOP
	FPADD	r3,	r20,	r3
	ORI	r4,	r0,	1065353216
	FPGT	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB62_127
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_127:
	bneid	r3,	$0BB62_132
	NOP
	LWI	r3,	r1,	300
	LWI	r3,	r3,	0
	FPGE	r4,	r21,	r3
	FPUN	r3,	r21,	r3
	BITOR	r4,	r3,	r4
	bneid	r4,	$0BB62_130
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB62_130:
	bneid	r3,	$0BB62_132
	NOP
	LWI	r3,	r1,	300
	SWI	r21,	r3,	0
$0BB62_132:
	ADDI	r10,	r10,	1
	LWI	r3,	r1,	276
	CMP	r3,	r3,	r10
	LWI	r29,	r1,	280
	bneid	r3,	$0BB62_109
	LWI	r26,	r1,	272
	LWI	r22,	r1,	284
$0BB62_134:
	bgtid	r22,	$0BB62_1
	NOP
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	288
#	.end	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
$0tmp62:
#	.size	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist, ($tmp62)-_ZNK5Scene14TraceShadowRayERK3RayR7HitDist

#	.globl	_ZNK5Scene7HemiRayERK8syVectorS2_
#	.align	2
#	.type	_ZNK5Scene7HemiRayERK8syVectorS2_,@function
#	.ent	_ZNK5Scene7HemiRayERK8syVectorS2_ # @_ZNK5Scene7HemiRayERK8syVectorS2_
_ZNK5Scene7HemiRayERK8syVectorS2_:
#	.frame	r1,16,r15
#	.mask	0xf00000
	ADDI	r1,	r1,	-16
	SWI	r20,	r1,	12
	SWI	r21,	r1,	8
	SWI	r22,	r1,	4
	SWI	r23,	r1,	0
$0BB63_1:
	RAND	r4
	RAND	r3
	FPADD	r3,	r3,	r3
	ORI	r6,	r0,	-1082130432
	FPADD	r3,	r3,	r6
	FPADD	r4,	r4,	r4
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPMUL	r9,	r3,	r3
	FPADD	r10,	r6,	r9
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r10,	r11
	bneid	r11,	$0BB63_3
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB63_3:
	bneid	r10,	$0BB63_1
	NOP
	ORI	r10,	r0,	1065353216
	FPRSUB	r6,	r6,	r10
	FPRSUB	r6,	r9,	r6
	FPINVSQRT	r11,	r6
	LWI	r6,	r8,	0
	ORI	r9,	r0,	0
	FPGE	r12,	r6,	r9
	FPUN	r9,	r6,	r9
	BITOR	r12,	r9,	r12
	bneid	r12,	$0BB63_6
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB63_6:
	bneid	r9,	$0BB63_8
	ADD	r12,	r6,	r0
	FPNEG	r12,	r6
$0BB63_8:
	LWI	r9,	r8,	4
	ORI	r20,	r0,	0
	FPGE	r21,	r9,	r20
	FPUN	r20,	r9,	r20
	BITOR	r20,	r20,	r21
	bneid	r20,	$0BB63_10
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB63_10:
	bneid	r21,	$0BB63_12
	ADD	r20,	r9,	r0
	FPNEG	r20,	r9
$0BB63_12:
	LWI	r8,	r8,	8
	ORI	r21,	r0,	0
	FPGE	r22,	r8,	r21
	FPUN	r21,	r8,	r21
	BITOR	r21,	r21,	r22
	bneid	r21,	$0BB63_14
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB63_14:
	bneid	r22,	$0BB63_16
	ADD	r21,	r8,	r0
	FPNEG	r21,	r8
$0BB63_16:
	FPGE	r22,	r12,	r20
	FPUN	r23,	r12,	r20
	BITOR	r22,	r23,	r22
	bneid	r22,	$0BB63_18
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB63_18:
	FPDIV	r10,	r10,	r11
	ORI	r22,	r0,	1065353216
	ORI	r11,	r0,	0
	bneid	r23,	$0BB63_22
	NOP
	FPLT	r23,	r12,	r21
	bneid	r23,	$0BB63_21
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB63_21:
	bneid	r12,	$0BB63_26
	ADD	r23,	r11,	r0
$0BB63_22:
	FPLT	r11,	r20,	r21
	bneid	r11,	$0BB63_24
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB63_24:
	ORI	r23,	r0,	1065353216
	ORI	r11,	r0,	0
	bneid	r12,	$0BB63_26
	ADD	r22,	r11,	r0
	ORI	r23,	r0,	0
	ORI	r11,	r0,	1065353216
	ADD	r22,	r23,	r0
$0BB63_26:
	FPMUL	r12,	r9,	r22
	FPMUL	r20,	r6,	r23
	FPRSUB	r12,	r12,	r20
	FPMUL	r20,	r8,	r22
	FPMUL	r21,	r6,	r11
	FPRSUB	r21,	r21,	r20
	FPMUL	r20,	r8,	r21
	FPMUL	r22,	r9,	r12
	FPRSUB	r20,	r20,	r22
	FPMUL	r22,	r8,	r23
	FPMUL	r11,	r9,	r11
	FPRSUB	r22,	r22,	r11
	FPMUL	r11,	r6,	r12
	FPMUL	r23,	r8,	r22
	FPRSUB	r23,	r11,	r23
	FPMUL	r11,	r20,	r3
	FPMUL	r20,	r22,	r4
	FPADD	r11,	r20,	r11
	FPMUL	r20,	r6,	r10
	FPADD	r11,	r11,	r20
	FPMUL	r20,	r23,	r3
	FPMUL	r23,	r21,	r4
	FPADD	r20,	r23,	r20
	FPMUL	r23,	r9,	r10
	FPADD	r20,	r20,	r23
	FPMUL	r9,	r9,	r22
	FPMUL	r21,	r6,	r21
	FPMUL	r6,	r20,	r20
	FPMUL	r22,	r11,	r11
	FPADD	r6,	r22,	r6
	FPRSUB	r9,	r9,	r21
	FPMUL	r3,	r9,	r3
	FPMUL	r4,	r12,	r4
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r8,	r10
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r6,	r4
	FPINVSQRT	r4,	r4
	LWI	r6,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r7,	0
	SWI	r7,	r5,	0
	SWI	r8,	r5,	4
	SWI	r6,	r5,	8
	ORI	r9,	r0,	1065353216
	FPDIV	r7,	r9,	r4
	FPDIV	r4,	r11,	r7
	FPDIV	r6,	r20,	r7
	FPMUL	r8,	r6,	r6
	FPMUL	r10,	r4,	r4
	FPADD	r8,	r10,	r8
	FPDIV	r3,	r3,	r7
	FPMUL	r7,	r3,	r3
	FPADD	r7,	r8,	r7
	FPINVSQRT	r7,	r7
	FPDIV	r7,	r9,	r7
	FPDIV	r4,	r4,	r7
	FPDIV	r6,	r6,	r7
	FPDIV	r7,	r3,	r7
	FPDIV	r3,	r9,	r7
	FPDIV	r8,	r9,	r6
	FPDIV	r9,	r9,	r4
	ORI	r10,	r0,	0
	FPLT	r12,	r9,	r10
	FPLT	r22,	r8,	r10
	FPLT	r20,	r3,	r10
	ADDI	r21,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB63_28
	ADD	r10,	r11,	r0
	ADD	r10,	r21,	r0
$0BB63_28:
	bneid	r22,	$0BB63_30
	ADD	r20,	r11,	r0
	ADD	r20,	r21,	r0
$0BB63_30:
	bneid	r12,	$0BB63_32
	NOP
	ADD	r11,	r21,	r0
$0BB63_32:
	SWI	r4,	r5,	12
	SWI	r6,	r5,	16
	SWI	r7,	r5,	20
	SWI	r9,	r5,	24
	SWI	r8,	r5,	28
	SWI	r3,	r5,	32
	SWI	r11,	r5,	36
	SWI	r20,	r5,	40
	SWI	r10,	r5,	44
	LWI	r23,	r1,	0
	LWI	r22,	r1,	4
	LWI	r21,	r1,	8
	LWI	r20,	r1,	12
	rtsd	r15,	8
	ADDI	r1,	r1,	16
#	.end	_ZNK5Scene7HemiRayERK8syVectorS2_
$0tmp63:
#	.size	_ZNK5Scene7HemiRayERK8syVectorS2_, ($tmp63)-_ZNK5Scene7HemiRayERK8syVectorS2_

#	.globl	_ZNK5Scene8GetColorERK3RayRKi
#	.align	2
#	.type	_ZNK5Scene8GetColorERK3RayRKi,@function
#	.ent	_ZNK5Scene8GetColorERK3RayRKi # @_ZNK5Scene8GetColorERK3RayRKi
_ZNK5Scene8GetColorERK3RayRKi:
#	.frame	r1,532,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-532
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r8,	r1,	548
	SWI	r6,	r1,	540
	SWI	r5,	r1,	536
	SWI	r0,	r5,	0
	SWI	r0,	r5,	4
	SWI	r0,	r5,	8
	ADD	r4,	r0,	r0
	ORI	r10,	r0,	1065353216
	SWI	r10,	r1,	456
	ORI	r3,	r0,	0
	SWI	r3,	r1,	484
	LWI	r5,	r7,	44
	SWI	r5,	r1,	420
	LWI	r5,	r7,	40
	SWI	r5,	r1,	424
	LWI	r5,	r7,	36
	SWI	r5,	r1,	428
	LWI	r25,	r7,	32
	LWI	r30,	r7,	28
	LWI	r26,	r7,	24
	LWI	r29,	r7,	20
	LWI	r28,	r7,	16
	LWI	r27,	r7,	12
	LWI	r5,	r7,	8
	SWI	r5,	r1,	376
	LWI	r5,	r7,	4
	SWI	r5,	r1,	372
	LWI	r5,	r7,	0
	SWI	r5,	r1,	380
	ADD	r5,	r3,	r0
	SWI	r3,	r1,	488
	SWI	r10,	r1,	460
	SWI	r10,	r1,	464
	ADD	r8,	r10,	r0
	brid	$0BB64_1
	ADD	r9,	r10,	r0
$0BB64_346:
	LWI	r3,	r1,	456
	FPMUL	r8,	r8,	r3
	LWI	r3,	r1,	460
	FPMUL	r9,	r9,	r3
	LWI	r3,	r1,	464
	FPMUL	r10,	r10,	r3
	LWI	r3,	r1,	480
	SWI	r3,	r1,	420
	SWI	r12,	r1,	424
	SWI	r11,	r1,	428
	LWI	r25,	r1,	512
	LWI	r30,	r1,	516
	LWI	r26,	r1,	520
	LWI	r29,	r1,	508
	LWI	r28,	r1,	504
	LWI	r27,	r1,	500
	LWI	r3,	r1,	392
	SWI	r3,	r1,	376
	LWI	r3,	r1,	384
	SWI	r3,	r1,	372
	LWI	r3,	r1,	388
	SWI	r3,	r1,	380
$0BB64_1:
	SWI	r26,	r1,	440
	SWI	r30,	r1,	436
	SWI	r25,	r1,	432
	LWI	r3,	r1,	548
	LWI	r3,	r3,	0
	CMP	r3,	r3,	r4
	bgeid	r3,	$0BB64_347
	NOP
	SWI	r12,	r1,	528
	SWI	r11,	r1,	524
	SWI	r10,	r1,	476
	SWI	r9,	r1,	472
	SWI	r8,	r1,	468
	SWI	r5,	r1,	496
	ADDI	r4,	r4,	1
	SWI	r4,	r1,	492
	ADDI	r9,	r0,	1
	ORI	r3,	r0,	1203982336
	SWI	r3,	r1,	368
	ADDI	r4,	r0,	-1
	SWI	r4,	r1,	404
	LWI	r3,	r1,	420
	RSUBI	r3,	r3,	1
	SWI	r3,	r1,	444
	LWI	r3,	r1,	424
	RSUBI	r3,	r3,	1
	SWI	r3,	r1,	448
	LWI	r3,	r1,	428
	RSUBI	r3,	r3,	1
	SWI	r3,	r1,	452
	SWI	r0,	r1,	144
	brid	$0BB64_3
	SWI	r4,	r1,	400
$0BB64_132:
	beqid	r5,	$0BB64_133
	NOP
	FPGT	r10,	r3,	r8
	ADDI	r6,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r10,	$0BB64_136
	ADD	r7,	r5,	r0
	ADD	r7,	r6,	r0
$0BB64_136:
	bneid	r7,	$0BB64_138
	NOP
	ADD	r3,	r8,	r0
$0BB64_138:
	FPGE	r4,	r22,	r3
	FPUN	r3,	r22,	r3
	BITOR	r3,	r3,	r4
	bneid	r3,	$0BB64_140
	NOP
	ADD	r5,	r6,	r0
$0BB64_140:
	bneid	r5,	$0BB64_142
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	144
	ADD	r5,	r4,	r3
	SWI	r21,	r5,	-4
	LWI	r5,	r1,	396
	SW	r5,	r4,	r3
	brid	$0BB64_143
	NOP
$0BB64_133:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	144
	ADD	r3,	r4,	r3
	brid	$0BB64_144
	SWI	r21,	r3,	-4
$0BB64_142:
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	144
	ADD	r5,	r4,	r3
	LWI	r6,	r1,	396
	SWI	r6,	r5,	-4
	SW	r21,	r4,	r3
$0BB64_143:
	brid	$0BB64_144
	ADDI	r9,	r9,	1
$0BB64_3:
	ADDI	r3,	r0,	-1
	ADD	r10,	r9,	r3
	bslli	r3,	r10,	2
	ADDI	r4,	r1,	144
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	540
	LWI	r4,	r4,	36
	ADD	r4,	r3,	r4
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	SWI	r0,	r1,	52
	SWI	r0,	r1,	48
	LOAD	r3,	r4,	0
	SWI	r3,	r1,	48
	LOAD	r3,	r4,	1
	SWI	r3,	r1,	52
	LOAD	r3,	r4,	2
	SWI	r3,	r1,	56
	ADDI	r3,	r4,	3
	LOAD	r5,	r3,	0
	SWI	r5,	r1,	60
	LOAD	r5,	r3,	1
	SWI	r5,	r1,	64
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	68
	LWI	r3,	r1,	448
	MULI	r8,	r3,	12
	ADDI	r3,	r1,	48
	ADD	r6,	r3,	r8
	ADDI	r7,	r4,	7
	ADDI	r4,	r4,	6
	LWI	r5,	r1,	428
	MULI	r11,	r5,	12
	SWI	r11,	r1,	416
	LOAD	r5,	r4,	0
	SWI	r5,	r1,	72
	LOAD	r4,	r7,	0
	SWI	r4,	r1,	396
	SWI	r4,	r1,	76
	LWI	r4,	r6,	4
	LWI	r6,	r1,	372
	FPRSUB	r4,	r6,	r4
	FPMUL	r20,	r4,	r30
	LW	r4,	r3,	r11
	LWI	r6,	r1,	380
	FPRSUB	r4,	r6,	r4
	FPMUL	r7,	r4,	r26
	ADDI	r6,	r0,	1
	FPGT	r4,	r7,	r20
	bneid	r4,	$0BB64_5
	NOP
	ADDI	r6,	r0,	0
$0BB64_5:
	bneid	r6,	$0BB64_6
	NOP
	LWI	r4,	r1,	424
	MULI	r31,	r4,	12
	ADD	r4,	r3,	r31
	LWI	r4,	r4,	4
	LWI	r6,	r1,	372
	FPRSUB	r4,	r6,	r4
	FPMUL	r21,	r4,	r30
	LWI	r4,	r1,	452
	MULI	r6,	r4,	12
	LW	r3,	r3,	r6
	LWI	r4,	r1,	380
	FPRSUB	r3,	r4,	r3
	FPMUL	r3,	r3,	r26
	FPGT	r4,	r21,	r3
	bneid	r4,	$0BB64_9
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB64_9:
	bneid	r12,	$0BB64_10
	NOP
	FPGT	r24,	r21,	r7
	ADDI	r23,	r0,	0
	ADDI	r22,	r0,	1
	bneid	r24,	$0BB64_13
	ADD	r12,	r22,	r0
	ADD	r12,	r23,	r0
$0BB64_13:
	bneid	r12,	$0BB64_15
	NOP
	ADD	r21,	r7,	r0
$0BB64_15:
	LWI	r4,	r1,	444
	MULI	r4,	r4,	12
	SWI	r4,	r1,	412
	ADDI	r7,	r1,	48
	ADD	r12,	r7,	r4
	FPLT	r4,	r20,	r3
	bneid	r4,	$0BB64_17
	ADD	r24,	r22,	r0
	ADD	r24,	r23,	r0
$0BB64_17:
	LWI	r4,	r12,	8
	LWI	r12,	r1,	376
	FPRSUB	r4,	r12,	r4
	FPMUL	r12,	r4,	r25
	FPGT	r4,	r21,	r12
	bneid	r4,	$0BB64_19
	NOP
	ADD	r22,	r23,	r0
$0BB64_19:
	bneid	r24,	$0BB64_21
	NOP
	ADD	r20,	r3,	r0
$0BB64_21:
	bneid	r22,	$0BB64_22
	NOP
	LWI	r3,	r1,	420
	MULI	r3,	r3,	12
	ADD	r4,	r7,	r3
	LWI	r4,	r4,	8
	LWI	r7,	r1,	376
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r25
	FPGT	r4,	r7,	r20
	bneid	r4,	$0BB64_25
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB64_25:
	bneid	r22,	$0BB64_26
	NOP
	ADD	r11,	r26,	r0
	ADD	r26,	r25,	r0
	FPLT	r25,	r12,	r20
	ADDI	r23,	r0,	0
	ADDI	r22,	r0,	1
	bneid	r25,	$0BB64_29
	ADD	r24,	r22,	r0
	ADD	r24,	r23,	r0
$0BB64_29:
	bneid	r24,	$0BB64_31
	NOP
	ADD	r12,	r20,	r0
$0BB64_31:
	ORI	r4,	r0,	0
	FPLT	r4,	r12,	r4
	bneid	r4,	$0BB64_33
	ADD	r25,	r26,	r0
	ADD	r22,	r23,	r0
$0BB64_33:
	bneid	r22,	$0BB64_34
	ADD	r26,	r11,	r0
	FPGT	r23,	r7,	r21
	ADDI	r20,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r23,	$0BB64_37
	ADD	r22,	r12,	r0
	ADD	r22,	r20,	r0
$0BB64_37:
	bneid	r22,	$0BB64_39
	ADD	r11,	r8,	r0
	ADD	r7,	r21,	r0
$0BB64_39:
	LWI	r4,	r1,	368
	FPLT	r4,	r4,	r7
	bneid	r4,	$0BB64_41
	NOP
	ADD	r12,	r20,	r0
$0BB64_41:
	bneid	r12,	$0BB64_42
	LWI	r8,	r1,	416
	beqid	r5,	$0BB64_44
	NOP
	ADD	r22,	r0,	r0
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r5
	bneid	r4,	$0BB64_46
	NOP
	LWI	r4,	r1,	396
	bslli	r4,	r4,	3
	LWI	r5,	r1,	540
	LWI	r5,	r5,	36
	ADD	r5,	r5,	r4
	SWI	r0,	r1,	92
	SWI	r0,	r1,	88
	SWI	r0,	r1,	84
	SWI	r0,	r1,	80
	LOAD	r4,	r5,	0
	SWI	r4,	r1,	80
	LOAD	r4,	r5,	1
	SWI	r4,	r1,	84
	LOAD	r4,	r5,	2
	SWI	r4,	r1,	88
	ADDI	r7,	r5,	3
	LOAD	r4,	r7,	0
	SWI	r4,	r1,	92
	LOAD	r4,	r7,	1
	SWI	r4,	r1,	96
	LOAD	r4,	r7,	2
	SWI	r4,	r1,	100
	ADDI	r4,	r5,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	104
	ADDI	r4,	r5,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	108
	ADDI	r12,	r1,	80
	ADD	r4,	r12,	r11
	LWI	r4,	r4,	4
	LWI	r5,	r1,	372
	FPRSUB	r4,	r5,	r4
	FPMUL	r20,	r4,	r30
	LW	r4,	r12,	r8
	LWI	r5,	r1,	380
	FPRSUB	r4,	r5,	r4
	FPMUL	r7,	r4,	r26
	ADDI	r21,	r0,	1
	FPGT	r4,	r7,	r20
	bneid	r4,	$0BB64_72
	NOP
	ADDI	r21,	r0,	0
$0BB64_72:
	SWI	r10,	r1,	408
	ADD	r5,	r0,	r0
	ORI	r22,	r0,	1203982336
	bneid	r21,	$0BB64_102
	NOP
	ADD	r4,	r12,	r31
	LW	r12,	r12,	r6
	LWI	r4,	r4,	4
	LWI	r21,	r1,	372
	FPRSUB	r4,	r21,	r4
	FPMUL	r21,	r4,	r30
	LWI	r4,	r1,	380
	FPRSUB	r4,	r4,	r12
	FPMUL	r12,	r4,	r26
	FPGT	r4,	r21,	r12
	bneid	r4,	$0BB64_75
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB64_75:
	bneid	r23,	$0BB64_102
	NOP
	FPGT	r24,	r21,	r7
	ADDI	r5,	r0,	0
	ADDI	r23,	r0,	1
	bneid	r24,	$0BB64_78
	ADD	r22,	r23,	r0
	ADD	r22,	r5,	r0
$0BB64_78:
	bneid	r22,	$0BB64_80
	NOP
	ADD	r21,	r7,	r0
$0BB64_80:
	ADDI	r24,	r1,	80
	LWI	r4,	r1,	412
	ADD	r7,	r24,	r4
	FPLT	r4,	r20,	r12
	bneid	r4,	$0BB64_82
	ADD	r22,	r23,	r0
	ADD	r22,	r5,	r0
$0BB64_82:
	LWI	r4,	r7,	8
	LWI	r7,	r1,	376
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r25
	FPGT	r4,	r21,	r7
	bneid	r4,	$0BB64_84
	NOP
	ADD	r23,	r5,	r0
$0BB64_84:
	bneid	r22,	$0BB64_86
	NOP
	ADD	r20,	r12,	r0
$0BB64_86:
	ADD	r5,	r0,	r0
	ORI	r22,	r0,	1203982336
	bneid	r23,	$0BB64_102
	NOP
	ADD	r4,	r24,	r3
	LWI	r4,	r4,	8
	LWI	r12,	r1,	376
	FPRSUB	r4,	r12,	r4
	FPMUL	r12,	r4,	r25
	FPGT	r4,	r12,	r20
	bneid	r4,	$0BB64_89
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB64_89:
	bneid	r23,	$0BB64_102
	NOP
	FPLT	r24,	r7,	r20
	ADDI	r5,	r0,	0
	ADDI	r23,	r0,	1
	bneid	r24,	$0BB64_92
	ADD	r22,	r23,	r0
	ADD	r22,	r5,	r0
$0BB64_92:
	bneid	r22,	$0BB64_94
	NOP
	ADD	r7,	r20,	r0
$0BB64_94:
	ORI	r4,	r0,	0
	FPLT	r4,	r7,	r4
	bneid	r4,	$0BB64_96
	NOP
	ADD	r23,	r5,	r0
$0BB64_96:
	ADD	r5,	r0,	r0
	ORI	r22,	r0,	1203982336
	bneid	r23,	$0BB64_102
	NOP
	FPGT	r4,	r12,	r21
	ADDI	r5,	r0,	1
	bneid	r4,	$0BB64_99
	ADD	r7,	r5,	r0
	ADDI	r7,	r0,	0
$0BB64_99:
	bneid	r7,	$0BB64_101
	NOP
	ADD	r12,	r21,	r0
$0BB64_101:
	ADD	r22,	r12,	r0
$0BB64_102:
	LWI	r4,	r1,	540
	LWI	r4,	r4,	36
	LWI	r7,	r1,	396
	ADDI	r21,	r7,	1
	bslli	r7,	r21,	3
	ADD	r7,	r4,	r7
	SWI	r0,	r1,	124
	SWI	r0,	r1,	120
	SWI	r0,	r1,	116
	SWI	r0,	r1,	112
	LOAD	r4,	r7,	0
	SWI	r4,	r1,	112
	LOAD	r4,	r7,	1
	SWI	r4,	r1,	116
	LOAD	r4,	r7,	2
	SWI	r4,	r1,	120
	ADDI	r12,	r7,	3
	LOAD	r4,	r12,	0
	SWI	r4,	r1,	124
	LOAD	r4,	r12,	1
	SWI	r4,	r1,	128
	LOAD	r4,	r12,	2
	SWI	r4,	r1,	132
	ADDI	r4,	r7,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	136
	ADDI	r4,	r7,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	140
	ADDI	r12,	r1,	112
	ADD	r4,	r12,	r11
	LWI	r4,	r4,	4
	LWI	r7,	r1,	372
	FPRSUB	r4,	r7,	r4
	FPMUL	r10,	r4,	r30
	LW	r4,	r12,	r8
	LWI	r7,	r1,	380
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r26
	ADDI	r11,	r0,	1
	FPGT	r4,	r7,	r10
	bneid	r4,	$0BB64_104
	NOP
	ADDI	r11,	r0,	0
$0BB64_104:
	bneid	r11,	$0BB64_129
	NOP
	ADD	r4,	r12,	r31
	LWI	r4,	r4,	4
	LWI	r8,	r1,	372
	FPRSUB	r4,	r8,	r4
	FPMUL	r8,	r4,	r30
	LW	r4,	r12,	r6
	LWI	r6,	r1,	380
	FPRSUB	r4,	r6,	r4
	FPMUL	r6,	r4,	r26
	FPGT	r4,	r8,	r6
	bneid	r4,	$0BB64_107
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB64_107:
	bneid	r11,	$0BB64_129
	NOP
	FPGT	r23,	r8,	r7
	ADDI	r12,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r23,	$0BB64_110
	ADD	r20,	r11,	r0
	ADD	r20,	r12,	r0
$0BB64_110:
	bneid	r20,	$0BB64_112
	NOP
	ADD	r8,	r7,	r0
$0BB64_112:
	ADDI	r20,	r1,	112
	LWI	r4,	r1,	412
	ADD	r7,	r20,	r4
	FPLT	r4,	r10,	r6
	bneid	r4,	$0BB64_114
	ADD	r23,	r11,	r0
	ADD	r23,	r12,	r0
$0BB64_114:
	LWI	r4,	r7,	8
	LWI	r7,	r1,	376
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r25
	FPGT	r4,	r8,	r7
	bneid	r4,	$0BB64_116
	NOP
	ADD	r11,	r12,	r0
$0BB64_116:
	bneid	r23,	$0BB64_118
	NOP
	ADD	r10,	r6,	r0
$0BB64_118:
	bneid	r11,	$0BB64_129
	NOP
	ADD	r3,	r20,	r3
	LWI	r3,	r3,	8
	LWI	r4,	r1,	376
	FPRSUB	r3,	r4,	r3
	FPMUL	r3,	r3,	r25
	FPGT	r4,	r3,	r10
	bneid	r4,	$0BB64_121
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB64_121:
	bneid	r6,	$0BB64_129
	NOP
	FPLT	r20,	r7,	r10
	ADDI	r11,	r0,	0
	ADDI	r6,	r0,	1
	bneid	r20,	$0BB64_124
	ADD	r12,	r6,	r0
	ADD	r12,	r11,	r0
$0BB64_124:
	bneid	r12,	$0BB64_126
	NOP
	ADD	r7,	r10,	r0
$0BB64_126:
	ORI	r4,	r0,	0
	FPGE	r10,	r7,	r4
	FPUN	r4,	r7,	r4
	BITOR	r4,	r4,	r10
	bneid	r4,	$0BB64_128
	NOP
	ADD	r6,	r11,	r0
$0BB64_128:
	bneid	r6,	$0BB64_132
	NOP
$0BB64_129:
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r5
	bneid	r3,	$0BB64_130
	NOP
	bslli	r3,	r9,	2
	ADDI	r4,	r1,	144
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	396
	brid	$0BB64_144
	SWI	r4,	r3,	-4
$0BB64_6:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_10:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_22:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_26:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_34:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_42:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_44:
	brid	$0BB64_144
	ADD	r9,	r10,	r0
$0BB64_46:
	SWI	r10,	r1,	408
$0BB64_47:
	MULI	r3,	r22,	11
	LWI	r4,	r1,	396
	ADD	r3,	r3,	r4
	LOAD	r11,	r3,	0
	LOAD	r20,	r3,	1
	LOAD	r31,	r3,	2
	ADDI	r4,	r3,	3
	LOAD	r10,	r4,	0
	LOAD	r23,	r4,	1
	LOAD	r7,	r4,	2
	ADDI	r4,	r3,	6
	LOAD	r8,	r4,	0
	LOAD	r6,	r4,	1
	LOAD	r4,	r4,	2
	FPRSUB	r9,	r31,	r4
	FPRSUB	r21,	r20,	r6
	FPMUL	r4,	r29,	r21
	FPMUL	r6,	r28,	r9
	FPRSUB	r6,	r4,	r6
	FPRSUB	r12,	r11,	r8
	FPMUL	r4,	r29,	r12
	FPMUL	r8,	r27,	r9
	FPRSUB	r8,	r8,	r4
	FPRSUB	r24,	r11,	r10
	FPRSUB	r23,	r20,	r23
	FPMUL	r4,	r8,	r23
	FPMUL	r10,	r6,	r24
	FPADD	r25,	r10,	r4
	FPMUL	r4,	r28,	r12
	FPMUL	r10,	r27,	r21
	FPRSUB	r10,	r4,	r10
	FPRSUB	r26,	r31,	r7
	FPMUL	r4,	r10,	r26
	FPADD	r30,	r25,	r4
	ORI	r4,	r0,	0
	FPGE	r7,	r30,	r4
	FPUN	r4,	r30,	r4
	BITOR	r4,	r4,	r7
	bneid	r4,	$0BB64_49
	ADDI	r25,	r0,	1
	ADDI	r25,	r0,	0
$0BB64_49:
	bneid	r25,	$0BB64_51
	ADD	r7,	r30,	r0
	FPNEG	r7,	r30
$0BB64_51:
	ORI	r4,	r0,	953267991
	FPLT	r4,	r7,	r4
	bneid	r4,	$0BB64_53
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB64_53:
	bneid	r7,	$0BB64_68
	NOP
	LWI	r4,	r1,	376
	FPRSUB	r31,	r31,	r4
	LWI	r4,	r1,	380
	FPRSUB	r11,	r11,	r4
	LWI	r4,	r1,	372
	FPRSUB	r25,	r20,	r4
	FPMUL	r7,	r25,	r24
	FPMUL	r20,	r11,	r23
	FPMUL	r24,	r31,	r24
	FPMUL	r4,	r11,	r26
	FPMUL	r23,	r31,	r23
	FPMUL	r26,	r25,	r26
	FPRSUB	r20,	r7,	r20
	FPRSUB	r24,	r4,	r24
	FPRSUB	r23,	r23,	r26
	FPMUL	r4,	r24,	r21
	FPMUL	r7,	r23,	r12
	FPADD	r4,	r7,	r4
	FPMUL	r7,	r20,	r9
	FPADD	r4,	r4,	r7
	ORI	r7,	r0,	1065353216
	FPDIV	r7,	r7,	r30
	FPMUL	r9,	r4,	r7
	ORI	r4,	r0,	0
	FPLT	r4,	r9,	r4
	bneid	r4,	$0BB64_56
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB64_56:
	bneid	r12,	$0BB64_68
	NOP
	FPMUL	r4,	r8,	r25
	FPMUL	r6,	r6,	r11
	FPADD	r4,	r6,	r4
	FPMUL	r8,	r24,	r28
	FPMUL	r11,	r23,	r27
	FPMUL	r6,	r10,	r31
	FPADD	r6,	r4,	r6
	FPADD	r4,	r11,	r8
	FPMUL	r8,	r20,	r29
	FPADD	r4,	r4,	r8
	FPMUL	r8,	r4,	r7
	FPMUL	r6,	r6,	r7
	FPADD	r4,	r6,	r8
	ORI	r7,	r0,	1065353216
	FPLE	r12,	r4,	r7
	ORI	r4,	r0,	0
	FPGE	r6,	r6,	r4
	FPGE	r11,	r8,	r4
	LWI	r7,	r1,	368
	FPGE	r4,	r9,	r7
	FPUN	r7,	r9,	r7
	BITOR	r20,	r7,	r4
	ADDI	r10,	r0,	0
	ADDI	r8,	r0,	1
	bneid	r12,	$0BB64_59
	ADD	r7,	r8,	r0
	ADD	r7,	r10,	r0
$0BB64_59:
	bneid	r20,	$0BB64_61
	ADD	r12,	r8,	r0
	ADD	r12,	r10,	r0
$0BB64_61:
	bneid	r11,	$0BB64_63
	ADD	r20,	r8,	r0
	ADD	r20,	r10,	r0
$0BB64_63:
	bneid	r6,	$0BB64_65
	NOP
	ADD	r8,	r10,	r0
$0BB64_65:
	bneid	r12,	$0BB64_68
	NOP
	BITAND	r4,	r8,	r20
	BITAND	r4,	r4,	r7
	ADDI	r6,	r0,	1
	CMP	r4,	r6,	r4
	bneid	r4,	$0BB64_68
	NOP
	ADDI	r4,	r3,	10
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	400
	SWI	r3,	r1,	404
	SWI	r9,	r1,	368
$0BB64_68:
	ADDI	r22,	r22,	1
	CMP	r3,	r5,	r22
	bneid	r3,	$0BB64_47
	NOP
	LWI	r9,	r1,	408
	LWI	r25,	r1,	432
	LWI	r30,	r1,	436
	brid	$0BB64_144
	LWI	r26,	r1,	440
$0BB64_130:
	LWI	r9,	r1,	408
$0BB64_144:
	bgtid	r9,	$0BB64_3
	NOP
	ORI	r3,	r0,	1203982336
	LWI	r4,	r1,	368
	FPNE	r4,	r4,	r3
	bneid	r4,	$0BB64_147
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB64_147:
	beqid	r3,	$0BB64_148
	NOP
	LWI	r9,	r1,	404
	LOAD	r3,	r9,	0
	LOAD	r4,	r9,	1
	LOAD	r6,	r9,	2
	ADDI	r8,	r9,	3
	LOAD	r5,	r8,	0
	LOAD	r7,	r8,	1
	LOAD	r10,	r8,	2
	ADDI	r9,	r9,	6
	LOAD	r8,	r9,	0
	LOAD	r11,	r9,	1
	LOAD	r9,	r9,	2
	FPRSUB	r9,	r9,	r6
	FPRSUB	r10,	r10,	r6
	FPRSUB	r6,	r7,	r4
	FPRSUB	r7,	r11,	r4
	FPMUL	r4,	r10,	r7
	FPMUL	r11,	r6,	r9
	FPRSUB	r4,	r4,	r11
	FPRSUB	r5,	r5,	r3
	FPRSUB	r8,	r8,	r3
	FPMUL	r3,	r10,	r8
	FPMUL	r9,	r5,	r9
	FPRSUB	r3,	r9,	r3
	FPMUL	r9,	r3,	r3
	FPMUL	r10,	r4,	r4
	FPADD	r9,	r10,	r9
	FPMUL	r6,	r6,	r8
	FPMUL	r5,	r5,	r7
	FPRSUB	r5,	r6,	r5
	FPMUL	r6,	r5,	r5
	FPADD	r6,	r9,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r6,	r7,	r6
	FPDIV	r25,	r3,	r6
	FPMUL	r3,	r25,	r28
	FPDIV	r30,	r4,	r6
	FPMUL	r4,	r30,	r27
	FPADD	r3,	r4,	r3
	FPDIV	r24,	r5,	r6
	FPMUL	r4,	r24,	r29
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB64_151
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB64_151:
	bneid	r3,	$0BB64_153
	NOP
	FPNEG	r24,	r24
	FPNEG	r25,	r25
	FPNEG	r30,	r30
$0BB64_153:
	LWI	r5,	r1,	368
	FPMUL	r4,	r27,	r5
	FPMUL	r3,	r29,	r5
	FPMUL	r6,	r28,	r5
	LWI	r5,	r1,	376
	FPADD	r3,	r5,	r3
	LWI	r5,	r1,	380
	FPADD	r5,	r5,	r4
	ORI	r7,	r0,	1028443341
	LWI	r4,	r1,	372
	FPADD	r6,	r4,	r6
	LWI	r4,	r1,	400
	MULI	r4,	r4,	25
	LWI	r11,	r1,	540
	LWI	r8,	r11,	32
	ADD	r4,	r4,	r8
	ADDI	r4,	r4,	4
	LOAD	r8,	r4,	0
	SWI	r8,	r1,	464
	LOAD	r8,	r4,	1
	SWI	r8,	r1,	460
	LOAD	r4,	r4,	2
	SWI	r4,	r1,	456
	LWI	r4,	r11,	4
	FPRSUB	r4,	r6,	r4
	LWI	r8,	r11,	0
	FPRSUB	r9,	r5,	r8
	FPMUL	r8,	r4,	r4
	FPMUL	r10,	r9,	r9
	FPADD	r8,	r10,	r8
	LWI	r10,	r11,	8
	FPRSUB	r10,	r3,	r10
	FPMUL	r11,	r10,	r10
	FPADD	r8,	r8,	r11
	FPINVSQRT	r11,	r8
	SWI	r11,	r1,	500
	FPINVSQRT	r11,	r8
	ORI	r8,	r0,	1065353216
	FPDIV	r11,	r8,	r11
	FPDIV	r12,	r9,	r11
	SWI	r12,	r1,	448
	FPDIV	r20,	r4,	r11
	SWI	r20,	r1,	452
	FPMUL	r4,	r20,	r20
	FPMUL	r9,	r12,	r12
	FPADD	r4,	r9,	r4
	FPDIV	r23,	r10,	r11
	SWI	r23,	r1,	480
	FPMUL	r9,	r23,	r23
	FPADD	r4,	r4,	r9
	FPINVSQRT	r4,	r4
	FPDIV	r10,	r8,	r4
	ORI	r9,	r0,	0
	FPDIV	r26,	r12,	r10
	FPDIV	r28,	r8,	r26
	SWI	r28,	r1,	424
	FPGE	r11,	r28,	r9
	FPUN	r21,	r28,	r9
	FPDIV	r4,	r20,	r10
	SWI	r4,	r1,	372
	FPDIV	r27,	r8,	r4
	FPGE	r4,	r27,	r9
	FPUN	r12,	r27,	r9
	BITOR	r22,	r12,	r4
	FPDIV	r4,	r23,	r10
	SWI	r4,	r1,	376
	FPDIV	r29,	r8,	r4
	FPGE	r4,	r29,	r9
	FPUN	r8,	r29,	r9
	BITOR	r23,	r8,	r4
	FPLT	r8,	r28,	r9
	FPLT	r10,	r27,	r9
	FPLT	r20,	r29,	r9
	ADDI	r9,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r23,	$0BB64_155
	SWI	r12,	r1,	400
	SWI	r9,	r1,	400
$0BB64_155:
	ADD	r23,	r25,	r0
	ADD	r4,	r24,	r0
	BITOR	r11,	r21,	r11
	bneid	r22,	$0BB64_157
	SWI	r12,	r1,	404
	SWI	r9,	r1,	404
$0BB64_157:
	SWI	r12,	r1,	408
	bneid	r11,	$0BB64_159
	ADD	r21,	r23,	r0
	SWI	r9,	r1,	408
$0BB64_159:
	bneid	r20,	$0BB64_161
	SWI	r12,	r1,	412
	SWI	r9,	r1,	412
$0BB64_161:
	SWI	r12,	r1,	416
	ADD	r24,	r27,	r0
	SWI	r24,	r1,	428
	bneid	r10,	$0BB64_163
	SWI	r29,	r1,	432
	SWI	r9,	r1,	416
$0BB64_163:
	FPMUL	r11,	r21,	r7
	SWI	r21,	r1,	440
	FPMUL	r10,	r4,	r7
	SWI	r4,	r1,	436
	FPMUL	r7,	r30,	r7
	SWI	r30,	r1,	444
	bneid	r8,	$0BB64_165
	SWI	r12,	r1,	420
	SWI	r9,	r1,	420
$0BB64_165:
	FPADD	r4,	r6,	r11
	SWI	r4,	r1,	384
	FPADD	r4,	r5,	r7
	SWI	r4,	r1,	388
	FPADD	r3,	r3,	r10
	SWI	r3,	r1,	392
	ORI	r11,	r0,	1203982336
	brid	$0BB64_166
	SWI	r0,	r1,	144
$0BB64_148:
	LWI	r4,	r1,	540
	LWI	r3,	r4,	48
	LWI	r5,	r4,	44
	LWI	r4,	r4,	40
	LWI	r8,	r1,	468
	LWI	r9,	r1,	472
	LWI	r10,	r1,	476
	LWI	r11,	r1,	524
	brid	$0BB64_343
	LWI	r12,	r1,	528
$0BB64_295:
	beqid	r3,	$0BB64_296
	NOP
	FPGT	r9,	r5,	r6
	ADDI	r7,	r0,	0
	ADDI	r3,	r0,	1
	bneid	r9,	$0BB64_299
	ADD	r8,	r3,	r0
	ADD	r8,	r7,	r0
$0BB64_299:
	bneid	r8,	$0BB64_301
	NOP
	ADD	r5,	r6,	r0
$0BB64_301:
	FPGE	r4,	r10,	r5
	FPUN	r5,	r10,	r5
	BITOR	r4,	r5,	r4
	bneid	r4,	$0BB64_303
	NOP
	ADD	r3,	r7,	r0
$0BB64_303:
	bneid	r3,	$0BB64_305
	NOP
	bslli	r3,	r12,	2
	ADDI	r4,	r1,	144
	ADD	r5,	r4,	r3
	SWI	r21,	r5,	-4
	LWI	r5,	r1,	380
	SW	r5,	r4,	r3
	brid	$0BB64_306
	NOP
$0BB64_296:
	bslli	r3,	r12,	2
	ADDI	r4,	r1,	144
	ADD	r3,	r4,	r3
	brid	$0BB64_307
	SWI	r21,	r3,	-4
$0BB64_305:
	bslli	r3,	r12,	2
	ADDI	r4,	r1,	144
	ADD	r5,	r4,	r3
	LWI	r6,	r1,	380
	SWI	r6,	r5,	-4
	SW	r21,	r4,	r3
$0BB64_306:
	brid	$0BB64_307
	ADDI	r12,	r12,	1
$0BB64_166:
	ADDI	r3,	r0,	-1
	ADD	r27,	r12,	r3
	bslli	r3,	r27,	2
	ADDI	r4,	r1,	144
	LW	r3,	r4,	r3
	bslli	r3,	r3,	3
	LWI	r4,	r1,	540
	LWI	r4,	r4,	36
	ADD	r3,	r3,	r4
	SWI	r0,	r1,	280
	SWI	r0,	r1,	276
	SWI	r0,	r1,	272
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	272
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	276
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	280
	ADDI	r5,	r3,	3
	LOAD	r4,	r5,	0
	SWI	r4,	r1,	284
	LOAD	r4,	r5,	1
	SWI	r4,	r1,	288
	LOAD	r4,	r5,	2
	SWI	r4,	r1,	292
	LWI	r4,	r1,	404
	MULI	r30,	r4,	12
	ADDI	r8,	r1,	272
	ADD	r5,	r8,	r30
	ADDI	r6,	r3,	7
	ADDI	r3,	r3,	6
	LWI	r4,	r1,	420
	MULI	r31,	r4,	12
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	296
	LOAD	r4,	r6,	0
	SWI	r4,	r1,	380
	SWI	r4,	r1,	300
	LWI	r4,	r5,	4
	LWI	r5,	r1,	384
	FPRSUB	r4,	r5,	r4
	FPMUL	r20,	r4,	r24
	LW	r4,	r8,	r31
	LWI	r5,	r1,	388
	FPRSUB	r4,	r5,	r4
	FPMUL	r7,	r4,	r28
	ADDI	r5,	r0,	1
	FPGT	r4,	r7,	r20
	bneid	r4,	$0BB64_168
	NOP
	ADDI	r5,	r0,	0
$0BB64_168:
	bneid	r5,	$0BB64_169
	NOP
	LWI	r4,	r1,	416
	MULI	r6,	r4,	12
	ADD	r4,	r8,	r6
	LWI	r4,	r4,	4
	LWI	r5,	r1,	384
	FPRSUB	r4,	r5,	r4
	FPMUL	r21,	r4,	r24
	LWI	r4,	r1,	408
	MULI	r5,	r4,	12
	LW	r4,	r8,	r5
	LWI	r8,	r1,	388
	FPRSUB	r4,	r8,	r4
	FPMUL	r8,	r4,	r28
	FPGT	r4,	r21,	r8
	bneid	r4,	$0BB64_172
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB64_172:
	bneid	r9,	$0BB64_173
	NOP
	FPGT	r23,	r21,	r7
	ADDI	r22,	r0,	0
	ADDI	r10,	r0,	1
	bneid	r23,	$0BB64_176
	ADD	r9,	r10,	r0
	ADD	r9,	r22,	r0
$0BB64_176:
	bneid	r9,	$0BB64_178
	NOP
	ADD	r21,	r7,	r0
$0BB64_178:
	LWI	r4,	r1,	400
	MULI	r7,	r4,	12
	ADDI	r23,	r1,	272
	ADD	r9,	r23,	r7
	FPLT	r4,	r20,	r8
	bneid	r4,	$0BB64_180
	ADD	r25,	r10,	r0
	ADD	r25,	r22,	r0
$0BB64_180:
	LWI	r4,	r9,	8
	LWI	r9,	r1,	392
	FPRSUB	r4,	r9,	r4
	FPMUL	r9,	r4,	r29
	FPGT	r4,	r21,	r9
	bneid	r4,	$0BB64_182
	NOP
	ADD	r10,	r22,	r0
$0BB64_182:
	bneid	r25,	$0BB64_184
	NOP
	ADD	r20,	r8,	r0
$0BB64_184:
	bneid	r10,	$0BB64_185
	NOP
	LWI	r4,	r1,	412
	MULI	r8,	r4,	12
	ADD	r4,	r23,	r8
	LWI	r4,	r4,	8
	LWI	r10,	r1,	392
	FPRSUB	r4,	r10,	r4
	FPMUL	r10,	r4,	r29
	FPGT	r4,	r10,	r20
	bneid	r4,	$0BB64_188
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB64_188:
	bneid	r22,	$0BB64_189
	NOP
	ADD	r4,	r27,	r0
	FPLT	r27,	r9,	r20
	ADDI	r23,	r0,	0
	ADDI	r22,	r0,	1
	bneid	r27,	$0BB64_192
	ADD	r25,	r22,	r0
	ADD	r25,	r23,	r0
$0BB64_192:
	bneid	r25,	$0BB64_194
	ADD	r27,	r4,	r0
	ADD	r9,	r20,	r0
$0BB64_194:
	ORI	r4,	r0,	0
	FPLT	r4,	r9,	r4
	bneid	r4,	$0BB64_196
	NOP
	ADD	r22,	r23,	r0
$0BB64_196:
	bneid	r22,	$0BB64_197
	NOP
	FPGT	r23,	r10,	r21
	ADDI	r20,	r0,	0
	ADDI	r9,	r0,	1
	bneid	r23,	$0BB64_200
	ADD	r22,	r9,	r0
	ADD	r22,	r20,	r0
$0BB64_200:
	bneid	r22,	$0BB64_202
	NOP
	ADD	r10,	r21,	r0
$0BB64_202:
	FPLT	r4,	r11,	r10
	bneid	r4,	$0BB64_204
	NOP
	ADD	r9,	r20,	r0
$0BB64_204:
	bneid	r9,	$0BB64_205
	NOP
	beqid	r3,	$0BB64_207
	NOP
	ADD	r21,	r0,	r0
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r3
	bneid	r4,	$0BB64_209
	NOP
	LWI	r3,	r1,	380
	bslli	r3,	r3,	3
	LWI	r4,	r1,	540
	LWI	r4,	r4,	36
	ADD	r3,	r4,	r3
	SWI	r0,	r1,	312
	SWI	r0,	r1,	308
	SWI	r0,	r1,	304
	LOAD	r4,	r3,	0
	SWI	r4,	r1,	304
	LOAD	r4,	r3,	1
	SWI	r4,	r1,	308
	LOAD	r4,	r3,	2
	SWI	r4,	r1,	312
	ADDI	r9,	r3,	3
	LOAD	r4,	r9,	0
	SWI	r4,	r1,	316
	LOAD	r4,	r9,	1
	SWI	r4,	r1,	320
	LOAD	r4,	r9,	2
	SWI	r4,	r1,	324
	ADDI	r4,	r3,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	328
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	SWI	r3,	r1,	332
	ADDI	r21,	r1,	304
	ADD	r3,	r21,	r30
	LWI	r3,	r3,	4
	LWI	r4,	r1,	384
	FPRSUB	r3,	r4,	r3
	FPMUL	r9,	r3,	r24
	LW	r3,	r21,	r31
	LWI	r4,	r1,	388
	FPRSUB	r3,	r4,	r3
	FPMUL	r20,	r3,	r28
	ADDI	r22,	r0,	1
	FPGT	r3,	r20,	r9
	bneid	r3,	$0BB64_235
	NOP
	ADDI	r22,	r0,	0
$0BB64_235:
	SWI	r27,	r1,	396
	ADD	r3,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r22,	$0BB64_265
	NOP
	ADD	r4,	r21,	r6
	LW	r22,	r21,	r5
	LWI	r4,	r4,	4
	LWI	r21,	r1,	384
	FPRSUB	r4,	r21,	r4
	FPMUL	r21,	r4,	r24
	LWI	r4,	r1,	388
	FPRSUB	r4,	r4,	r22
	FPMUL	r22,	r4,	r28
	FPGT	r4,	r21,	r22
	bneid	r4,	$0BB64_238
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB64_238:
	bneid	r23,	$0BB64_265
	NOP
	FPGT	r25,	r21,	r20
	ADDI	r3,	r0,	0
	ADDI	r23,	r0,	1
	bneid	r25,	$0BB64_241
	ADD	r10,	r23,	r0
	ADD	r10,	r3,	r0
$0BB64_241:
	bneid	r10,	$0BB64_243
	NOP
	ADD	r21,	r20,	r0
$0BB64_243:
	ADDI	r25,	r1,	304
	ADD	r20,	r25,	r7
	FPLT	r4,	r9,	r22
	bneid	r4,	$0BB64_245
	ADD	r10,	r23,	r0
	ADD	r10,	r3,	r0
$0BB64_245:
	LWI	r4,	r20,	8
	LWI	r20,	r1,	392
	FPRSUB	r4,	r20,	r4
	FPMUL	r20,	r4,	r29
	FPGT	r4,	r21,	r20
	bneid	r4,	$0BB64_247
	NOP
	ADD	r23,	r3,	r0
$0BB64_247:
	bneid	r10,	$0BB64_249
	NOP
	ADD	r9,	r22,	r0
$0BB64_249:
	ADD	r3,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r23,	$0BB64_265
	NOP
	ADD	r4,	r25,	r8
	LWI	r4,	r4,	8
	LWI	r22,	r1,	392
	FPRSUB	r4,	r22,	r4
	FPMUL	r22,	r4,	r29
	FPGT	r4,	r22,	r9
	bneid	r4,	$0BB64_252
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB64_252:
	bneid	r23,	$0BB64_265
	NOP
	FPLT	r25,	r20,	r9
	ADDI	r3,	r0,	0
	ADDI	r23,	r0,	1
	bneid	r25,	$0BB64_255
	ADD	r10,	r23,	r0
	ADD	r10,	r3,	r0
$0BB64_255:
	bneid	r10,	$0BB64_257
	NOP
	ADD	r20,	r9,	r0
$0BB64_257:
	ORI	r4,	r0,	0
	FPLT	r4,	r20,	r4
	bneid	r4,	$0BB64_259
	NOP
	ADD	r23,	r3,	r0
$0BB64_259:
	ADD	r3,	r0,	r0
	ORI	r10,	r0,	1203982336
	bneid	r23,	$0BB64_265
	NOP
	FPGT	r4,	r22,	r21
	ADDI	r3,	r0,	1
	bneid	r4,	$0BB64_262
	ADD	r9,	r3,	r0
	ADDI	r9,	r0,	0
$0BB64_262:
	bneid	r9,	$0BB64_264
	NOP
	ADD	r22,	r21,	r0
$0BB64_264:
	ADD	r10,	r22,	r0
$0BB64_265:
	LWI	r4,	r1,	540
	LWI	r4,	r4,	36
	LWI	r9,	r1,	380
	ADDI	r21,	r9,	1
	bslli	r9,	r21,	3
	ADD	r9,	r4,	r9
	SWI	r0,	r1,	344
	SWI	r0,	r1,	340
	SWI	r0,	r1,	336
	LOAD	r4,	r9,	0
	SWI	r4,	r1,	336
	LOAD	r4,	r9,	1
	SWI	r4,	r1,	340
	LOAD	r4,	r9,	2
	SWI	r4,	r1,	344
	ADDI	r20,	r9,	3
	LOAD	r4,	r20,	0
	SWI	r4,	r1,	348
	LOAD	r4,	r20,	1
	SWI	r4,	r1,	352
	LOAD	r4,	r20,	2
	SWI	r4,	r1,	356
	ADDI	r4,	r9,	6
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	360
	ADDI	r4,	r9,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	364
	ADDI	r22,	r1,	336
	ADD	r4,	r22,	r30
	LWI	r4,	r4,	4
	LWI	r9,	r1,	384
	FPRSUB	r4,	r9,	r4
	FPMUL	r20,	r4,	r24
	LW	r4,	r22,	r31
	LWI	r9,	r1,	388
	FPRSUB	r4,	r9,	r4
	FPMUL	r9,	r4,	r28
	ADDI	r23,	r0,	1
	FPGT	r4,	r9,	r20
	bneid	r4,	$0BB64_267
	NOP
	ADDI	r23,	r0,	0
$0BB64_267:
	bneid	r23,	$0BB64_292
	NOP
	ADD	r4,	r22,	r6
	LWI	r4,	r4,	4
	LWI	r6,	r1,	384
	FPRSUB	r4,	r6,	r4
	FPMUL	r6,	r4,	r24
	LW	r4,	r22,	r5
	LWI	r5,	r1,	388
	FPRSUB	r4,	r5,	r4
	FPMUL	r5,	r4,	r28
	FPGT	r4,	r6,	r5
	bneid	r4,	$0BB64_270
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB64_270:
	bneid	r22,	$0BB64_292
	NOP
	FPGT	r27,	r6,	r9
	ADDI	r23,	r0,	0
	ADDI	r22,	r0,	1
	bneid	r27,	$0BB64_273
	ADD	r25,	r22,	r0
	ADD	r25,	r23,	r0
$0BB64_273:
	bneid	r25,	$0BB64_275
	NOP
	ADD	r6,	r9,	r0
$0BB64_275:
	ADDI	r9,	r1,	336
	ADD	r7,	r9,	r7
	FPLT	r4,	r20,	r5
	bneid	r4,	$0BB64_277
	ADD	r25,	r22,	r0
	ADD	r25,	r23,	r0
$0BB64_277:
	LWI	r4,	r7,	8
	LWI	r7,	r1,	392
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r29
	FPGT	r4,	r6,	r7
	bneid	r4,	$0BB64_279
	NOP
	ADD	r22,	r23,	r0
$0BB64_279:
	bneid	r25,	$0BB64_281
	NOP
	ADD	r20,	r5,	r0
$0BB64_281:
	bneid	r22,	$0BB64_292
	NOP
	ADD	r4,	r9,	r8
	LWI	r4,	r4,	8
	LWI	r5,	r1,	392
	FPRSUB	r4,	r5,	r4
	FPMUL	r5,	r4,	r29
	FPGT	r4,	r5,	r20
	bneid	r4,	$0BB64_284
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB64_284:
	bneid	r8,	$0BB64_292
	NOP
	FPLT	r23,	r7,	r20
	ADDI	r9,	r0,	0
	ADDI	r8,	r0,	1
	bneid	r23,	$0BB64_287
	ADD	r22,	r8,	r0
	ADD	r22,	r9,	r0
$0BB64_287:
	bneid	r22,	$0BB64_289
	NOP
	ADD	r7,	r20,	r0
$0BB64_289:
	ORI	r4,	r0,	0
	FPGE	r20,	r7,	r4
	FPUN	r4,	r7,	r4
	BITOR	r4,	r4,	r20
	bneid	r4,	$0BB64_291
	NOP
	ADD	r8,	r9,	r0
$0BB64_291:
	bneid	r8,	$0BB64_295
	NOP
$0BB64_292:
	ADDI	r4,	r0,	1
	CMP	r3,	r4,	r3
	bneid	r3,	$0BB64_293
	LWI	r4,	r1,	396
	bslli	r3,	r12,	2
	ADDI	r4,	r1,	144
	ADD	r3,	r4,	r3
	LWI	r4,	r1,	380
	brid	$0BB64_307
	SWI	r4,	r3,	-4
$0BB64_169:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_173:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_185:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_189:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_197:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_205:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_207:
	brid	$0BB64_307
	ADD	r12,	r27,	r0
$0BB64_209:
	SWI	r27,	r1,	396
$0BB64_210:
	MULI	r4,	r21,	11
	LWI	r5,	r1,	380
	ADD	r6,	r4,	r5
	LOAD	r12,	r6,	0
	LOAD	r20,	r6,	1
	LOAD	r30,	r6,	2
	ADDI	r5,	r6,	6
	ADDI	r4,	r6,	3
	LOAD	r7,	r4,	0
	LOAD	r9,	r4,	1
	LOAD	r10,	r4,	2
	LOAD	r6,	r5,	0
	LOAD	r4,	r5,	1
	LOAD	r5,	r5,	2
	FPRSUB	r8,	r30,	r5
	FPRSUB	r22,	r20,	r4
	LWI	r23,	r1,	376
	FPMUL	r4,	r23,	r22
	LWI	r24,	r1,	372
	FPMUL	r5,	r24,	r8
	FPRSUB	r5,	r4,	r5
	FPRSUB	r31,	r12,	r6
	FPMUL	r4,	r23,	r31
	FPMUL	r6,	r26,	r8
	FPRSUB	r6,	r6,	r4
	FPRSUB	r23,	r12,	r7
	FPRSUB	r9,	r20,	r9
	FPMUL	r4,	r6,	r9
	FPMUL	r7,	r5,	r23
	FPADD	r27,	r7,	r4
	FPMUL	r4,	r24,	r31
	FPMUL	r7,	r26,	r22
	FPRSUB	r7,	r4,	r7
	FPRSUB	r25,	r30,	r10
	FPMUL	r4,	r7,	r25
	FPADD	r29,	r27,	r4
	ORI	r4,	r0,	0
	FPGE	r10,	r29,	r4
	FPUN	r4,	r29,	r4
	BITOR	r4,	r4,	r10
	bneid	r4,	$0BB64_212
	ADDI	r27,	r0,	1
	ADDI	r27,	r0,	0
$0BB64_212:
	bneid	r27,	$0BB64_214
	ADD	r10,	r29,	r0
	FPNEG	r10,	r29
$0BB64_214:
	ORI	r4,	r0,	953267991
	FPLT	r4,	r10,	r4
	bneid	r4,	$0BB64_216
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB64_216:
	bneid	r10,	$0BB64_231
	NOP
	LWI	r4,	r1,	392
	FPRSUB	r30,	r30,	r4
	LWI	r4,	r1,	388
	FPRSUB	r12,	r12,	r4
	LWI	r4,	r1,	384
	FPRSUB	r20,	r20,	r4
	FPMUL	r10,	r20,	r23
	FPMUL	r27,	r12,	r9
	FPMUL	r23,	r30,	r23
	FPMUL	r28,	r12,	r25
	FPMUL	r4,	r30,	r9
	FPMUL	r24,	r20,	r25
	FPRSUB	r9,	r10,	r27
	FPRSUB	r25,	r28,	r23
	FPRSUB	r23,	r4,	r24
	FPMUL	r4,	r25,	r22
	FPMUL	r10,	r23,	r31
	FPADD	r4,	r10,	r4
	FPMUL	r8,	r9,	r8
	FPADD	r4,	r4,	r8
	ORI	r8,	r0,	1065353216
	FPDIV	r10,	r8,	r29
	FPMUL	r8,	r4,	r10
	ORI	r4,	r0,	0
	FPLT	r4,	r8,	r4
	bneid	r4,	$0BB64_219
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB64_219:
	bneid	r22,	$0BB64_231
	NOP
	FPMUL	r4,	r6,	r20
	FPMUL	r5,	r5,	r12
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	372
	FPMUL	r5,	r25,	r5
	FPMUL	r12,	r23,	r26
	FPMUL	r6,	r7,	r30
	FPADD	r6,	r4,	r6
	FPADD	r4,	r12,	r5
	LWI	r5,	r1,	376
	FPMUL	r5,	r9,	r5
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r4,	r10
	FPMUL	r6,	r6,	r10
	FPADD	r4,	r6,	r5
	ORI	r7,	r0,	1065353216
	FPLE	r9,	r4,	r7
	ORI	r4,	r0,	0
	FPGE	r10,	r6,	r4
	FPGE	r12,	r5,	r4
	ADDI	r7,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r9,	$0BB64_222
	ADD	r6,	r5,	r0
	ADD	r6,	r7,	r0
$0BB64_222:
	bneid	r12,	$0BB64_224
	ADD	r9,	r5,	r0
	ADD	r9,	r7,	r0
$0BB64_224:
	bneid	r10,	$0BB64_226
	ADD	r12,	r5,	r0
	ADD	r12,	r7,	r0
$0BB64_226:
	FPLT	r4,	r8,	r11
	bneid	r4,	$0BB64_228
	NOP
	ADD	r5,	r7,	r0
$0BB64_228:
	BITAND	r4,	r12,	r9
	BITAND	r4,	r4,	r6
	BITAND	r4,	r5,	r4
	bneid	r4,	$0BB64_230
	NOP
	ADD	r8,	r11,	r0
$0BB64_230:
	ADD	r11,	r8,	r0
$0BB64_231:
	ADDI	r21,	r21,	1
	CMP	r4,	r3,	r21
	bneid	r4,	$0BB64_210
	NOP
	LWI	r12,	r1,	396
	LWI	r28,	r1,	424
	LWI	r24,	r1,	428
	brid	$0BB64_307
	LWI	r29,	r1,	432
$0BB64_293:
	ADD	r12,	r4,	r0
$0BB64_307:
	bgtid	r12,	$0BB64_166
	NOP
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	500
	FPDIV	r3,	r3,	r4
	FPGT	r4,	r3,	r11
	FPUN	r3,	r3,	r11
	BITOR	r3,	r3,	r4
	bneid	r3,	$0BB64_310
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB64_310:
	ORI	r3,	r0,	0
	ADD	r5,	r3,	r0
	ADD	r4,	r3,	r0
	LWI	r23,	r1,	436
	LWI	r22,	r1,	440
	bneid	r6,	$0BB64_312
	LWI	r20,	r1,	444
	LWI	r3,	r1,	452
	FPMUL	r3,	r22,	r3
	LWI	r4,	r1,	448
	FPMUL	r4,	r20,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	480
	FPMUL	r4,	r23,	r4
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPMAX	r3,	r3,	r4
	LWI	r8,	r1,	540
	LWI	r5,	r8,	20
	LWI	r6,	r1,	456
	FPMUL	r5,	r6,	r5
	LWI	r6,	r8,	16
	LWI	r7,	r1,	460
	FPMUL	r7,	r7,	r6
	LWI	r6,	r8,	12
	LWI	r8,	r1,	464
	FPMUL	r6,	r8,	r6
	FPMUL	r6,	r6,	r3
	FPMUL	r7,	r7,	r3
	FPMUL	r3,	r5,	r3
	FPADD	r3,	r3,	r4
	FPADD	r5,	r7,	r4
	FPADD	r4,	r6,	r4
$0BB64_312:
	RAND	r7
	RAND	r6
	FPADD	r6,	r6,	r6
	ORI	r8,	r0,	-1082130432
	FPADD	r6,	r6,	r8
	FPADD	r7,	r7,	r7
	FPADD	r7,	r7,	r8
	FPMUL	r9,	r7,	r7
	FPMUL	r10,	r6,	r6
	FPADD	r8,	r9,	r10
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r8,	r11
	bneid	r11,	$0BB64_314
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB64_314:
	bneid	r8,	$0BB64_312
	NOP
	ORI	r8,	r0,	0
	FPGE	r11,	r20,	r8
	FPUN	r8,	r20,	r8
	BITOR	r8,	r8,	r11
	bneid	r8,	$0BB64_317
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB64_317:
	ORI	r8,	r0,	1065353216
	FPRSUB	r9,	r9,	r8
	FPRSUB	r9,	r10,	r9
	FPINVSQRT	r10,	r9
	bneid	r11,	$0BB64_319
	ADD	r9,	r20,	r0
	FPNEG	r9,	r20
$0BB64_319:
	ORI	r11,	r0,	0
	FPGE	r12,	r22,	r11
	FPUN	r11,	r22,	r11
	BITOR	r11,	r11,	r12
	bneid	r11,	$0BB64_321
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB64_321:
	bneid	r12,	$0BB64_323
	ADD	r11,	r22,	r0
	FPNEG	r11,	r22
$0BB64_323:
	ADD	r21,	r20,	r0
	ORI	r12,	r0,	0
	FPGE	r20,	r23,	r12
	FPUN	r12,	r23,	r12
	BITOR	r12,	r12,	r20
	bneid	r12,	$0BB64_325
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB64_325:
	ADD	r12,	r23,	r0
	bneid	r20,	$0BB64_327
	ADD	r24,	r21,	r0
	FPNEG	r12,	r23
$0BB64_327:
	FPGE	r20,	r9,	r11
	FPUN	r21,	r9,	r11
	BITOR	r20,	r21,	r20
	bneid	r20,	$0BB64_329
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB64_329:
	FPDIV	r8,	r8,	r10
	ORI	r20,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r21,	$0BB64_333
	NOP
	FPLT	r21,	r9,	r12
	bneid	r21,	$0BB64_332
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB64_332:
	bneid	r9,	$0BB64_337
	ADD	r21,	r10,	r0
$0BB64_333:
	FPLT	r10,	r11,	r12
	bneid	r10,	$0BB64_335
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB64_335:
	ORI	r21,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r9,	$0BB64_337
	ADD	r20,	r10,	r0
	ORI	r21,	r0,	0
	ORI	r10,	r0,	1065353216
	ADD	r20,	r21,	r0
$0BB64_337:
	FPMUL	r9,	r22,	r20
	FPMUL	r11,	r24,	r21
	FPRSUB	r9,	r9,	r11
	FPMUL	r11,	r23,	r20
	FPMUL	r12,	r24,	r10
	FPRSUB	r11,	r12,	r11
	FPMUL	r12,	r23,	r11
	FPMUL	r20,	r22,	r9
	FPRSUB	r12,	r12,	r20
	FPMUL	r20,	r23,	r21
	FPMUL	r10,	r22,	r10
	FPRSUB	r20,	r20,	r10
	FPMUL	r10,	r24,	r9
	FPMUL	r21,	r23,	r20
	FPRSUB	r21,	r10,	r21
	FPMUL	r10,	r12,	r6
	FPMUL	r12,	r20,	r7
	FPADD	r10,	r12,	r10
	FPMUL	r12,	r24,	r8
	FPADD	r10,	r10,	r12
	FPMUL	r12,	r21,	r6
	FPMUL	r21,	r11,	r7
	FPADD	r12,	r21,	r12
	FPMUL	r21,	r22,	r8
	FPADD	r12,	r12,	r21
	FPMUL	r20,	r22,	r20
	FPMUL	r21,	r24,	r11
	FPMUL	r11,	r12,	r12
	FPMUL	r22,	r10,	r10
	FPADD	r11,	r22,	r11
	FPRSUB	r20,	r20,	r21
	FPMUL	r6,	r20,	r6
	FPMUL	r7,	r9,	r7
	FPADD	r6,	r7,	r6
	FPMUL	r7,	r23,	r8
	FPADD	r7,	r6,	r7
	FPMUL	r6,	r7,	r7
	FPADD	r6,	r11,	r6
	FPINVSQRT	r8,	r6
	ORI	r6,	r0,	1065353216
	FPDIV	r11,	r6,	r8
	FPDIV	r8,	r10,	r11
	FPDIV	r9,	r12,	r11
	FPMUL	r10,	r9,	r9
	FPMUL	r12,	r8,	r8
	FPADD	r10,	r12,	r10
	FPDIV	r7,	r7,	r11
	FPMUL	r11,	r7,	r7
	FPADD	r10,	r10,	r11
	FPINVSQRT	r10,	r10
	FPDIV	r10,	r6,	r10
	FPDIV	r8,	r8,	r10
	SWI	r8,	r1,	500
	FPDIV	r9,	r9,	r10
	SWI	r9,	r1,	504
	FPDIV	r7,	r7,	r10
	SWI	r7,	r1,	508
	FPDIV	r10,	r6,	r7
	SWI	r10,	r1,	512
	FPDIV	r9,	r6,	r9
	SWI	r9,	r1,	516
	FPDIV	r6,	r6,	r8
	SWI	r6,	r1,	520
	ORI	r7,	r0,	0
	FPLT	r6,	r6,	r7
	FPLT	r8,	r9,	r7
	FPLT	r9,	r10,	r7
	ADDI	r7,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r9,	$0BB64_339
	SWI	r11,	r1,	480
	SWI	r7,	r1,	480
$0BB64_339:
	bneid	r8,	$0BB64_341
	ADD	r12,	r11,	r0
	ADD	r12,	r7,	r0
$0BB64_341:
	LWI	r8,	r1,	468
	LWI	r9,	r1,	472
	bneid	r6,	$0BB64_343
	LWI	r10,	r1,	476
	ADD	r11,	r7,	r0
$0BB64_343:
	ORI	r6,	r0,	1203982336
	LWI	r7,	r1,	368
	FPEQ	r7,	r7,	r6
	bneid	r7,	$0BB64_345
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB64_345:
	FPMUL	r5,	r5,	r9
	FPMUL	r3,	r3,	r8
	LWI	r7,	r1,	484
	FPADD	r7,	r7,	r3
	SWI	r7,	r1,	484
	LWI	r3,	r1,	496
	FPADD	r3,	r3,	r5
	ADD	r5,	r3,	r0
	FPMUL	r3,	r4,	r10
	LWI	r4,	r1,	488
	FPADD	r4,	r4,	r3
	SWI	r4,	r1,	488
	LWI	r3,	r1,	536
	SWI	r4,	r3,	0
	SWI	r5,	r3,	4
	SWI	r7,	r3,	8
	beqid	r6,	$0BB64_346
	LWI	r4,	r1,	492
$0BB64_347:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	532
#	.end	_ZNK5Scene8GetColorERK3RayRKi
$0tmp64:
#	.size	_ZNK5Scene8GetColorERK3RayRKi, ($tmp64)-_ZNK5Scene8GetColorERK3RayRKi

#	.globl	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
#	.align	2
#	.type	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color,@function
#	.ent	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color # @_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color:
#	.frame	r1,376,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-376
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	ADD	r23,	r10,	r0
	SWI	r9,	r1,	396
	LWI	r3,	r8,	0
	ORI	r4,	r0,	1203982336
	FPNE	r4,	r3,	r4
	bneid	r4,	$0BB65_2
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB65_2:
	beqid	r3,	$0BB65_3
	NOP
	SWI	r5,	r1,	352
	LWI	r9,	r8,	8
	LOAD	r4,	r9,	0
	LOAD	r3,	r9,	1
	LOAD	r21,	r9,	2
	ADDI	r10,	r9,	3
	LOAD	r5,	r10,	0
	LOAD	r12,	r10,	1
	LOAD	r20,	r10,	2
	ADDI	r11,	r9,	6
	LOAD	r10,	r11,	0
	LOAD	r9,	r11,	1
	LOAD	r11,	r11,	2
	FPRSUB	r11,	r11,	r21
	FPRSUB	r20,	r20,	r21
	FPRSUB	r21,	r12,	r3
	FPRSUB	r9,	r9,	r3
	FPMUL	r3,	r20,	r9
	FPMUL	r12,	r21,	r11
	FPRSUB	r3,	r3,	r12
	FPRSUB	r12,	r5,	r4
	FPRSUB	r4,	r10,	r4
	FPMUL	r5,	r20,	r4
	FPMUL	r10,	r12,	r11
	FPRSUB	r5,	r10,	r5
	FPMUL	r10,	r5,	r5
	FPMUL	r11,	r3,	r3
	FPADD	r10,	r11,	r10
	FPMUL	r4,	r21,	r4
	FPMUL	r9,	r12,	r9
	FPRSUB	r4,	r4,	r9
	FPMUL	r9,	r4,	r4
	FPADD	r9,	r10,	r9
	FPINVSQRT	r10,	r9
	ORI	r9,	r0,	1065353216
	FPDIV	r11,	r9,	r10
	FPDIV	r9,	r5,	r11
	LWI	r5,	r7,	16
	FPMUL	r5,	r9,	r5
	FPDIV	r10,	r3,	r11
	LWI	r3,	r7,	12
	FPMUL	r3,	r10,	r3
	FPADD	r3,	r3,	r5
	FPDIV	r11,	r4,	r11
	LWI	r4,	r7,	20
	FPMUL	r4,	r11,	r4
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB65_6
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB65_6:
	bneid	r3,	$0BB65_7
	NOP
	FPNEG	r11,	r11
	SWI	r11,	r1,	340
	FPNEG	r9,	r9
	SWI	r9,	r1,	344
	FPNEG	r10,	r10
	brid	$0BB65_9
	SWI	r10,	r1,	348
$0BB65_3:
	LWI	r3,	r6,	40
	SWI	r3,	r5,	0
	LWI	r3,	r6,	44
	SWI	r3,	r5,	4
	LWI	r3,	r6,	48
	brid	$0BB65_198
	SWI	r3,	r5,	8
$0BB65_7:
	SWI	r10,	r1,	348
	SWI	r9,	r1,	344
	SWI	r11,	r1,	340
$0BB65_9:
	LWI	r3,	r8,	4
	MULI	r3,	r3,	25
	ADD	r12,	r6,	r0
	SWI	r12,	r1,	304
	LWI	r4,	r12,	32
	ADD	r3,	r4,	r3
	ADDI	r3,	r3,	4
	LOAD	r4,	r3,	0
	LOAD	r5,	r3,	1
	LOAD	r3,	r3,	2
	SWI	r4,	r23,	0
	SWI	r5,	r23,	4
	SWI	r3,	r23,	8
	ORI	r4,	r0,	1028443341
	LWI	r5,	r8,	0
	LWI	r3,	r7,	16
	FPMUL	r3,	r3,	r5
	LWI	r6,	r7,	4
	FPADD	r3,	r6,	r3
	LWI	r6,	r7,	12
	FPMUL	r6,	r6,	r5
	LWI	r8,	r7,	0
	FPADD	r6,	r8,	r6
	LWI	r8,	r12,	0
	FPRSUB	r8,	r6,	r8
	LWI	r9,	r12,	4
	FPRSUB	r9,	r3,	r9
	FPMUL	r10,	r9,	r9
	FPMUL	r11,	r8,	r8
	FPADD	r11,	r11,	r10
	LWI	r10,	r7,	20
	FPMUL	r5,	r10,	r5
	LWI	r7,	r7,	8
	FPADD	r7,	r7,	r5
	LWI	r5,	r12,	8
	FPRSUB	r10,	r7,	r5
	FPMUL	r5,	r10,	r10
	FPADD	r5,	r11,	r5
	FPINVSQRT	r11,	r5
	SWI	r11,	r1,	372
	FPINVSQRT	r11,	r5
	ORI	r5,	r0,	1065353216
	FPDIV	r11,	r5,	r11
	FPDIV	r12,	r8,	r11
	SWI	r12,	r1,	360
	FPDIV	r21,	r9,	r11
	SWI	r21,	r1,	364
	FPMUL	r8,	r21,	r21
	FPMUL	r9,	r12,	r12
	FPADD	r8,	r9,	r8
	FPDIV	r22,	r10,	r11
	SWI	r22,	r1,	368
	FPMUL	r9,	r22,	r22
	FPADD	r8,	r8,	r9
	FPINVSQRT	r8,	r8
	FPDIV	r8,	r5,	r8
	ORI	r9,	r0,	0
	FPDIV	r28,	r12,	r8
	FPDIV	r24,	r5,	r28
	FPGE	r11,	r24,	r9
	FPUN	r20,	r24,	r9
	FPDIV	r10,	r21,	r8
	SWI	r10,	r1,	272
	FPDIV	r25,	r5,	r10
	SWI	r25,	r1,	296
	FPGE	r10,	r25,	r9
	FPUN	r12,	r25,	r9
	BITOR	r21,	r12,	r10
	FPDIV	r8,	r22,	r8
	SWI	r8,	r1,	276
	FPDIV	r12,	r5,	r8
	SWI	r12,	r1,	300
	FPGE	r5,	r12,	r9
	FPUN	r8,	r12,	r9
	BITOR	r22,	r8,	r5
	FPLT	r8,	r24,	r9
	FPLT	r10,	r25,	r9
	FPLT	r12,	r12,	r9
	ADDI	r9,	r0,	0
	ADDI	r5,	r0,	1
	bneid	r22,	$0BB65_11
	SWI	r5,	r1,	312
	SWI	r9,	r1,	312
$0BB65_11:
	BITOR	r11,	r20,	r11
	bneid	r21,	$0BB65_13
	SWI	r5,	r1,	316
	SWI	r9,	r1,	316
$0BB65_13:
	SWI	r23,	r1,	356
	bneid	r11,	$0BB65_15
	SWI	r5,	r1,	320
	SWI	r9,	r1,	320
$0BB65_15:
	bneid	r12,	$0BB65_17
	SWI	r5,	r1,	324
	SWI	r9,	r1,	324
$0BB65_17:
	bneid	r10,	$0BB65_19
	SWI	r5,	r1,	328
	SWI	r9,	r1,	328
$0BB65_19:
	LWI	r10,	r1,	348
	FPMUL	r10,	r10,	r4
	LWI	r11,	r1,	344
	FPMUL	r11,	r11,	r4
	LWI	r12,	r1,	340
	FPMUL	r4,	r12,	r4
	bneid	r8,	$0BB65_21
	SWI	r5,	r1,	332
	SWI	r9,	r1,	332
$0BB65_21:
	FPADD	r4,	r7,	r4
	SWI	r4,	r1,	280
	FPADD	r3,	r3,	r11
	SWI	r3,	r1,	284
	FPADD	r3,	r6,	r10
	SWI	r3,	r1,	288
	ORI	r3,	r0,	1203982336
	SWI	r0,	r1,	48
	ADD	r12,	r24,	r0
	brid	$0BB65_22
	SWI	r12,	r1,	336
$0BB65_127:
	beqid	r21,	$0BB65_128
	NOP
	FPGT	r9,	r11,	r4
	ADDI	r7,	r0,	0
	ADDI	r6,	r0,	1
	bneid	r9,	$0BB65_131
	ADD	r8,	r6,	r0
	ADD	r8,	r7,	r0
$0BB65_131:
	bneid	r8,	$0BB65_133
	NOP
	ADD	r11,	r4,	r0
$0BB65_133:
	FPGE	r4,	r27,	r11
	FPUN	r8,	r27,	r11
	BITOR	r4,	r8,	r4
	bneid	r4,	$0BB65_135
	NOP
	ADD	r6,	r7,	r0
$0BB65_135:
	bneid	r6,	$0BB65_137
	NOP
	bslli	r4,	r5,	2
	ADDI	r6,	r1,	48
	ADD	r7,	r6,	r4
	SWI	r25,	r7,	-4
	LWI	r7,	r1,	292
	SW	r7,	r6,	r4
	brid	$0BB65_161
	ADDI	r5,	r5,	1
$0BB65_128:
	bslli	r4,	r5,	2
	ADDI	r6,	r1,	48
	ADD	r4,	r6,	r4
	brid	$0BB65_161
	SWI	r25,	r4,	-4
$0BB65_137:
	bslli	r4,	r5,	2
	ADDI	r6,	r1,	48
	ADD	r7,	r6,	r4
	LWI	r8,	r1,	292
	SWI	r8,	r7,	-4
	SW	r25,	r6,	r4
	brid	$0BB65_161
	ADDI	r5,	r5,	1
$0BB65_22:
	ADDI	r4,	r0,	-1
	ADD	r24,	r5,	r4
	bslli	r4,	r24,	2
	ADDI	r6,	r1,	48
	LW	r4,	r6,	r4
	bslli	r4,	r4,	3
	LWI	r6,	r1,	304
	LWI	r6,	r6,	36
	ADD	r4,	r4,	r6
	SWI	r0,	r1,	188
	SWI	r0,	r1,	184
	SWI	r0,	r1,	180
	SWI	r0,	r1,	176
	LOAD	r6,	r4,	0
	SWI	r6,	r1,	176
	LOAD	r6,	r4,	1
	SWI	r6,	r1,	180
	LOAD	r6,	r4,	2
	SWI	r6,	r1,	184
	ADDI	r6,	r4,	3
	LOAD	r7,	r6,	0
	SWI	r7,	r1,	188
	LOAD	r7,	r6,	1
	SWI	r7,	r1,	192
	LOAD	r6,	r6,	2
	SWI	r6,	r1,	196
	LWI	r6,	r1,	316
	MULI	r26,	r6,	12
	ADDI	r8,	r1,	176
	ADD	r6,	r8,	r26
	ADDI	r7,	r4,	7
	ADDI	r4,	r4,	6
	LWI	r9,	r1,	332
	MULI	r11,	r9,	12
	LOAD	r21,	r4,	0
	SWI	r21,	r1,	200
	LOAD	r4,	r7,	0
	SWI	r4,	r1,	292
	SWI	r4,	r1,	204
	LWI	r4,	r6,	4
	LWI	r6,	r1,	284
	FPRSUB	r4,	r6,	r4
	LWI	r6,	r1,	296
	FPMUL	r6,	r4,	r6
	LW	r4,	r8,	r11
	LWI	r7,	r1,	288
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r12
	ADDI	r4,	r0,	1
	FPGT	r9,	r7,	r6
	bneid	r9,	$0BB65_24
	NOP
	ADDI	r4,	r0,	0
$0BB65_24:
	bneid	r4,	$0BB65_25
	NOP
	LWI	r4,	r1,	328
	MULI	r30,	r4,	12
	ADD	r4,	r8,	r30
	LWI	r4,	r4,	4
	LWI	r9,	r1,	284
	FPRSUB	r4,	r9,	r4
	LWI	r9,	r1,	296
	FPMUL	r22,	r4,	r9
	LWI	r4,	r1,	320
	MULI	r23,	r4,	12
	LW	r4,	r8,	r23
	LWI	r8,	r1,	288
	FPRSUB	r4,	r8,	r4
	FPMUL	r20,	r4,	r12
	FPGT	r8,	r22,	r20
	bneid	r8,	$0BB65_28
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB65_28:
	bneid	r4,	$0BB65_29
	NOP
	FPGT	r9,	r22,	r7
	ADDI	r25,	r0,	0
	ADDI	r8,	r0,	1
	bneid	r9,	$0BB65_32
	ADD	r4,	r8,	r0
	ADD	r4,	r25,	r0
$0BB65_32:
	bneid	r4,	$0BB65_34
	NOP
	ADD	r22,	r7,	r0
$0BB65_34:
	LWI	r4,	r1,	312
	MULI	r31,	r4,	12
	ADDI	r4,	r1,	176
	ADD	r7,	r4,	r31
	FPLT	r9,	r6,	r20
	bneid	r9,	$0BB65_36
	ADD	r27,	r8,	r0
	ADD	r27,	r25,	r0
$0BB65_36:
	LWI	r7,	r7,	8
	LWI	r9,	r1,	280
	FPRSUB	r7,	r9,	r7
	LWI	r9,	r1,	300
	FPMUL	r7,	r7,	r9
	FPGT	r9,	r22,	r7
	bneid	r9,	$0BB65_38
	NOP
	ADD	r8,	r25,	r0
$0BB65_38:
	bneid	r27,	$0BB65_40
	NOP
	ADD	r6,	r20,	r0
$0BB65_40:
	bneid	r8,	$0BB65_41
	NOP
	LWI	r8,	r1,	324
	MULI	r20,	r8,	12
	ADD	r4,	r4,	r20
	LWI	r4,	r4,	8
	LWI	r8,	r1,	280
	FPRSUB	r4,	r8,	r4
	LWI	r8,	r1,	300
	FPMUL	r25,	r4,	r8
	FPGT	r8,	r25,	r6
	bneid	r8,	$0BB65_44
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB65_44:
	bneid	r4,	$0BB65_45
	NOP
	FPLT	r10,	r7,	r6
	ADDI	r8,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r10,	$0BB65_48
	ADD	r9,	r4,	r0
	ADD	r9,	r8,	r0
$0BB65_48:
	bneid	r9,	$0BB65_50
	NOP
	ADD	r7,	r6,	r0
$0BB65_50:
	ORI	r6,	r0,	0
	FPLT	r6,	r7,	r6
	bneid	r6,	$0BB65_52
	NOP
	ADD	r4,	r8,	r0
$0BB65_52:
	bneid	r4,	$0BB65_53
	NOP
	FPGT	r8,	r25,	r22
	ADDI	r6,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r8,	$0BB65_56
	ADD	r7,	r4,	r0
	ADD	r7,	r6,	r0
$0BB65_56:
	bneid	r7,	$0BB65_58
	NOP
	ADD	r25,	r22,	r0
$0BB65_58:
	FPLT	r7,	r3,	r25
	bneid	r7,	$0BB65_60
	NOP
	ADD	r4,	r6,	r0
$0BB65_60:
	bneid	r4,	$0BB65_61
	NOP
	beqid	r21,	$0BB65_63
	NOP
	SWI	r24,	r1,	308
	ADD	r22,	r0,	r0
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r21
	bneid	r4,	$0BB65_138
	NOP
	LWI	r4,	r1,	292
	bslli	r4,	r4,	3
	LWI	r6,	r1,	304
	LWI	r6,	r6,	36
	ADD	r4,	r6,	r4
	SWI	r0,	r1,	220
	SWI	r0,	r1,	216
	SWI	r0,	r1,	212
	SWI	r0,	r1,	208
	LOAD	r6,	r4,	0
	SWI	r6,	r1,	208
	LOAD	r6,	r4,	1
	SWI	r6,	r1,	212
	LOAD	r6,	r4,	2
	SWI	r6,	r1,	216
	ADDI	r6,	r4,	3
	LOAD	r7,	r6,	0
	SWI	r7,	r1,	220
	LOAD	r7,	r6,	1
	SWI	r7,	r1,	224
	LOAD	r6,	r6,	2
	SWI	r6,	r1,	228
	ADDI	r6,	r4,	6
	LOAD	r6,	r6,	0
	SWI	r6,	r1,	232
	ADDI	r4,	r4,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	236
	ADDI	r4,	r1,	208
	ADD	r6,	r4,	r26
	LWI	r6,	r6,	4
	LWI	r7,	r1,	284
	FPRSUB	r6,	r7,	r6
	LWI	r7,	r1,	296
	FPMUL	r29,	r6,	r7
	LW	r6,	r4,	r11
	LWI	r7,	r1,	288
	FPRSUB	r6,	r7,	r6
	FPMUL	r6,	r6,	r12
	ADDI	r7,	r0,	1
	FPGT	r8,	r6,	r29
	bneid	r8,	$0BB65_67
	NOP
	ADDI	r7,	r0,	0
$0BB65_67:
	ADD	r21,	r0,	r0
	ORI	r27,	r0,	1203982336
	bneid	r7,	$0BB65_97
	NOP
	ADD	r7,	r4,	r30
	LW	r4,	r4,	r23
	LWI	r7,	r7,	4
	LWI	r8,	r1,	284
	FPRSUB	r7,	r8,	r7
	LWI	r8,	r1,	296
	FPMUL	r25,	r7,	r8
	LWI	r7,	r1,	288
	FPRSUB	r4,	r7,	r4
	FPMUL	r22,	r4,	r12
	FPGT	r7,	r25,	r22
	bneid	r7,	$0BB65_70
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB65_70:
	bneid	r4,	$0BB65_97
	NOP
	FPGT	r9,	r25,	r6
	ADDI	r8,	r0,	0
	ADDI	r7,	r0,	1
	bneid	r9,	$0BB65_73
	ADD	r4,	r7,	r0
	ADD	r4,	r8,	r0
$0BB65_73:
	bneid	r4,	$0BB65_75
	NOP
	ADD	r25,	r6,	r0
$0BB65_75:
	ADDI	r4,	r1,	208
	ADD	r6,	r4,	r31
	FPLT	r9,	r29,	r22
	bneid	r9,	$0BB65_77
	ADD	r21,	r7,	r0
	ADD	r21,	r8,	r0
$0BB65_77:
	LWI	r6,	r6,	8
	LWI	r9,	r1,	280
	FPRSUB	r6,	r9,	r6
	LWI	r9,	r1,	300
	FPMUL	r6,	r6,	r9
	FPGT	r9,	r25,	r6
	bneid	r9,	$0BB65_79
	NOP
	ADD	r7,	r8,	r0
$0BB65_79:
	bneid	r21,	$0BB65_81
	NOP
	ADD	r29,	r22,	r0
$0BB65_81:
	ADD	r21,	r0,	r0
	ORI	r27,	r0,	1203982336
	bneid	r7,	$0BB65_97
	NOP
	ADD	r4,	r4,	r20
	LWI	r4,	r4,	8
	LWI	r7,	r1,	280
	FPRSUB	r4,	r7,	r4
	LWI	r7,	r1,	300
	FPMUL	r22,	r4,	r7
	FPGT	r7,	r22,	r29
	bneid	r7,	$0BB65_84
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB65_84:
	bneid	r4,	$0BB65_97
	NOP
	FPLT	r9,	r6,	r29
	ADDI	r7,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r9,	$0BB65_87
	ADD	r8,	r4,	r0
	ADD	r8,	r7,	r0
$0BB65_87:
	bneid	r8,	$0BB65_89
	NOP
	ADD	r6,	r29,	r0
$0BB65_89:
	ORI	r8,	r0,	0
	FPLT	r6,	r6,	r8
	bneid	r6,	$0BB65_91
	NOP
	ADD	r4,	r7,	r0
$0BB65_91:
	ADD	r21,	r0,	r0
	ORI	r27,	r0,	1203982336
	bneid	r4,	$0BB65_97
	NOP
	FPGT	r6,	r22,	r25
	ADDI	r21,	r0,	1
	bneid	r6,	$0BB65_94
	ADD	r4,	r21,	r0
	ADDI	r4,	r0,	0
$0BB65_94:
	bneid	r4,	$0BB65_96
	NOP
	ADD	r22,	r25,	r0
$0BB65_96:
	ADD	r27,	r22,	r0
$0BB65_97:
	LWI	r4,	r1,	304
	LWI	r4,	r4,	36
	LWI	r6,	r1,	292
	ADDI	r25,	r6,	1
	bslli	r6,	r25,	3
	ADD	r4,	r4,	r6
	SWI	r0,	r1,	252
	SWI	r0,	r1,	248
	SWI	r0,	r1,	244
	SWI	r0,	r1,	240
	LOAD	r6,	r4,	0
	SWI	r6,	r1,	240
	LOAD	r6,	r4,	1
	SWI	r6,	r1,	244
	LOAD	r6,	r4,	2
	SWI	r6,	r1,	248
	ADDI	r6,	r4,	3
	LOAD	r7,	r6,	0
	SWI	r7,	r1,	252
	LOAD	r7,	r6,	1
	SWI	r7,	r1,	256
	LOAD	r6,	r6,	2
	SWI	r6,	r1,	260
	ADDI	r6,	r4,	6
	LOAD	r6,	r6,	0
	SWI	r6,	r1,	264
	ADDI	r4,	r4,	7
	LOAD	r4,	r4,	0
	SWI	r4,	r1,	268
	ADDI	r8,	r1,	240
	ADD	r4,	r8,	r26
	LWI	r4,	r4,	4
	LWI	r6,	r1,	284
	FPRSUB	r4,	r6,	r4
	LWI	r6,	r1,	296
	FPMUL	r6,	r4,	r6
	LW	r4,	r8,	r11
	LWI	r7,	r1,	288
	FPRSUB	r4,	r7,	r4
	FPMUL	r7,	r4,	r12
	ADDI	r4,	r0,	1
	FPGT	r9,	r7,	r6
	bneid	r9,	$0BB65_99
	LWI	r24,	r1,	308
	ADDI	r4,	r0,	0
$0BB65_99:
	bneid	r4,	$0BB65_124
	NOP
	ADD	r4,	r8,	r30
	LWI	r4,	r4,	4
	LWI	r9,	r1,	284
	FPRSUB	r4,	r9,	r4
	LWI	r9,	r1,	296
	FPMUL	r4,	r4,	r9
	LW	r8,	r8,	r23
	LWI	r9,	r1,	288
	FPRSUB	r8,	r9,	r8
	FPMUL	r11,	r8,	r12
	FPGT	r9,	r4,	r11
	bneid	r9,	$0BB65_102
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB65_102:
	bneid	r8,	$0BB65_124
	NOP
	FPGT	r10,	r4,	r7
	ADDI	r23,	r0,	0
	ADDI	r8,	r0,	1
	bneid	r10,	$0BB65_105
	ADD	r9,	r8,	r0
	ADD	r9,	r23,	r0
$0BB65_105:
	bneid	r9,	$0BB65_107
	NOP
	ADD	r4,	r7,	r0
$0BB65_107:
	ADDI	r7,	r1,	240
	ADD	r22,	r7,	r31
	FPLT	r9,	r6,	r11
	bneid	r9,	$0BB65_109
	ADD	r29,	r8,	r0
	ADD	r29,	r23,	r0
$0BB65_109:
	LWI	r9,	r22,	8
	LWI	r10,	r1,	280
	FPRSUB	r9,	r10,	r9
	LWI	r10,	r1,	300
	FPMUL	r22,	r9,	r10
	FPGT	r9,	r4,	r22
	bneid	r9,	$0BB65_111
	NOP
	ADD	r8,	r23,	r0
$0BB65_111:
	bneid	r29,	$0BB65_113
	NOP
	ADD	r6,	r11,	r0
$0BB65_113:
	bneid	r8,	$0BB65_124
	NOP
	ADD	r7,	r7,	r20
	LWI	r7,	r7,	8
	LWI	r8,	r1,	280
	FPRSUB	r7,	r8,	r7
	LWI	r8,	r1,	300
	FPMUL	r11,	r7,	r8
	FPGT	r8,	r11,	r6
	bneid	r8,	$0BB65_116
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB65_116:
	bneid	r7,	$0BB65_124
	NOP
	FPLT	r10,	r22,	r6
	ADDI	r8,	r0,	0
	ADDI	r7,	r0,	1
	bneid	r10,	$0BB65_119
	ADD	r9,	r7,	r0
	ADD	r9,	r8,	r0
$0BB65_119:
	bneid	r9,	$0BB65_121
	NOP
	ADD	r22,	r6,	r0
$0BB65_121:
	ORI	r6,	r0,	0
	FPGE	r9,	r22,	r6
	FPUN	r6,	r22,	r6
	BITOR	r6,	r6,	r9
	bneid	r6,	$0BB65_123
	NOP
	ADD	r7,	r8,	r0
$0BB65_123:
	bneid	r7,	$0BB65_127
	NOP
$0BB65_124:
	ADDI	r4,	r0,	1
	CMP	r4,	r4,	r21
	bneid	r4,	$0BB65_125
	NOP
	bslli	r4,	r5,	2
	ADDI	r6,	r1,	48
	ADD	r4,	r6,	r4
	LWI	r6,	r1,	292
	brid	$0BB65_161
	SWI	r6,	r4,	-4
$0BB65_25:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_29:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_41:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_138:
	MULI	r4,	r22,	11
	LWI	r5,	r1,	292
	ADD	r5,	r4,	r5
	LOAD	r20,	r5,	0
	LOAD	r23,	r5,	1
	LOAD	r30,	r5,	2
	ADDI	r4,	r5,	6
	ADDI	r5,	r5,	3
	LOAD	r6,	r5,	0
	LOAD	r27,	r5,	1
	LOAD	r8,	r5,	2
	LOAD	r5,	r4,	0
	LOAD	r7,	r4,	1
	LOAD	r4,	r4,	2
	FPRSUB	r11,	r30,	r4
	FPRSUB	r25,	r23,	r7
	LWI	r9,	r1,	276
	FPMUL	r4,	r9,	r25
	LWI	r10,	r1,	272
	FPMUL	r7,	r10,	r11
	FPRSUB	r4,	r4,	r7
	FPRSUB	r31,	r20,	r5
	FPMUL	r5,	r9,	r31
	FPMUL	r7,	r28,	r11
	FPRSUB	r5,	r7,	r5
	FPRSUB	r7,	r20,	r6
	FPRSUB	r29,	r23,	r27
	FPMUL	r6,	r5,	r29
	FPMUL	r9,	r4,	r7
	FPADD	r9,	r9,	r6
	FPMUL	r6,	r10,	r31
	FPMUL	r10,	r28,	r25
	FPRSUB	r6,	r6,	r10
	FPRSUB	r8,	r30,	r8
	FPMUL	r10,	r6,	r8
	FPADD	r9,	r9,	r10
	ORI	r10,	r0,	0
	FPGE	r12,	r9,	r10
	FPUN	r10,	r9,	r10
	BITOR	r10,	r10,	r12
	bneid	r10,	$0BB65_140
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB65_140:
	bneid	r12,	$0BB65_142
	ADD	r27,	r9,	r0
	FPNEG	r27,	r9
$0BB65_142:
	ORI	r10,	r0,	953267991
	FPLT	r10,	r27,	r10
	bneid	r10,	$0BB65_144
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB65_144:
	bneid	r12,	$0BB65_159
	NOP
	LWI	r10,	r1,	280
	FPRSUB	r30,	r30,	r10
	LWI	r10,	r1,	288
	FPRSUB	r20,	r20,	r10
	LWI	r10,	r1,	284
	FPRSUB	r23,	r23,	r10
	FPMUL	r12,	r23,	r7
	FPMUL	r27,	r20,	r29
	FPMUL	r10,	r30,	r7
	FPMUL	r24,	r20,	r8
	FPMUL	r26,	r30,	r29
	FPMUL	r8,	r23,	r8
	FPRSUB	r7,	r12,	r27
	FPRSUB	r29,	r24,	r10
	FPRSUB	r8,	r26,	r8
	FPMUL	r10,	r29,	r25
	FPMUL	r12,	r8,	r31
	FPADD	r10,	r12,	r10
	FPMUL	r11,	r7,	r11
	FPADD	r10,	r10,	r11
	ORI	r11,	r0,	1065353216
	FPDIV	r25,	r11,	r9
	FPMUL	r11,	r10,	r25
	ORI	r9,	r0,	0
	FPLT	r10,	r11,	r9
	bneid	r10,	$0BB65_147
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB65_147:
	bneid	r9,	$0BB65_159
	NOP
	FPMUL	r5,	r5,	r23
	FPMUL	r4,	r4,	r20
	FPADD	r4,	r4,	r5
	LWI	r5,	r1,	272
	FPMUL	r9,	r29,	r5
	FPMUL	r8,	r8,	r28
	FPMUL	r5,	r6,	r30
	FPADD	r5,	r4,	r5
	FPADD	r4,	r8,	r9
	LWI	r6,	r1,	276
	FPMUL	r6,	r7,	r6
	FPADD	r4,	r4,	r6
	FPMUL	r4,	r4,	r25
	FPMUL	r5,	r5,	r25
	FPADD	r6,	r5,	r4
	ORI	r7,	r0,	1065353216
	FPLE	r7,	r6,	r7
	ORI	r6,	r0,	0
	FPGE	r8,	r5,	r6
	FPGE	r20,	r4,	r6
	ADDI	r6,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB65_150
	ADD	r5,	r4,	r0
	ADD	r5,	r6,	r0
$0BB65_150:
	bneid	r20,	$0BB65_152
	ADD	r7,	r4,	r0
	ADD	r7,	r6,	r0
$0BB65_152:
	bneid	r8,	$0BB65_154
	ADD	r20,	r4,	r0
	ADD	r20,	r6,	r0
$0BB65_154:
	FPLT	r8,	r11,	r3
	bneid	r8,	$0BB65_156
	NOP
	ADD	r4,	r6,	r0
$0BB65_156:
	BITAND	r6,	r20,	r7
	BITAND	r5,	r6,	r5
	BITAND	r4,	r4,	r5
	bneid	r4,	$0BB65_158
	NOP
	ADD	r11,	r3,	r0
$0BB65_158:
	ADD	r3,	r11,	r0
$0BB65_159:
	ADDI	r22,	r22,	1
	CMP	r4,	r21,	r22
	bneid	r4,	$0BB65_138
	NOP
	LWI	r5,	r1,	308
	brid	$0BB65_161
	LWI	r12,	r1,	336
$0BB65_45:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_53:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_61:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_63:
	brid	$0BB65_161
	ADD	r5,	r24,	r0
$0BB65_125:
	ADD	r5,	r24,	r0
$0BB65_161:
	bgtid	r5,	$0BB65_22
	NOP
	ORI	r4,	r0,	1065353216
	LWI	r5,	r1,	372
	FPDIV	r4,	r4,	r5
	FPGT	r5,	r4,	r3
	FPUN	r3,	r4,	r3
	BITOR	r3,	r3,	r5
	ADDI	r6,	r0,	1
	LWI	r23,	r1,	340
	bneid	r3,	$0BB65_164
	LWI	r24,	r1,	344
	ADDI	r6,	r0,	0
$0BB65_164:
	ORI	r3,	r0,	0
	ADD	r4,	r3,	r0
	ADD	r5,	r3,	r0
	bneid	r6,	$0BB65_166
	LWI	r25,	r1,	348
	LWI	r3,	r1,	364
	FPMUL	r3,	r24,	r3
	LWI	r4,	r1,	360
	FPMUL	r4,	r25,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	368
	FPMUL	r4,	r23,	r4
	FPADD	r3,	r3,	r4
	ORI	r5,	r0,	0
	FPMAX	r3,	r3,	r5
	LWI	r8,	r1,	304
	LWI	r4,	r8,	20
	LWI	r9,	r1,	356
	LWI	r6,	r9,	8
	FPMUL	r4,	r6,	r4
	LWI	r6,	r8,	16
	LWI	r7,	r9,	4
	FPMUL	r6,	r7,	r6
	LWI	r7,	r8,	12
	LWI	r8,	r9,	0
	FPMUL	r7,	r8,	r7
	FPMUL	r7,	r7,	r3
	FPMUL	r6,	r6,	r3
	FPMUL	r3,	r4,	r3
	FPADD	r3,	r3,	r5
	FPADD	r4,	r6,	r5
	FPADD	r5,	r7,	r5
$0BB65_166:
	RAND	r7
	RAND	r6
	FPADD	r6,	r6,	r6
	ORI	r8,	r0,	-1082130432
	FPADD	r6,	r6,	r8
	FPADD	r7,	r7,	r7
	FPADD	r9,	r7,	r8
	FPMUL	r7,	r9,	r9
	FPMUL	r8,	r6,	r6
	FPADD	r10,	r7,	r8
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r10,	r11
	bneid	r11,	$0BB65_168
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB65_168:
	bneid	r10,	$0BB65_166
	NOP
	ORI	r10,	r0,	0
	FPGE	r11,	r25,	r10
	FPUN	r10,	r25,	r10
	BITOR	r10,	r10,	r11
	bneid	r10,	$0BB65_171
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB65_171:
	ORI	r10,	r0,	1065353216
	FPRSUB	r7,	r7,	r10
	FPRSUB	r7,	r8,	r7
	FPINVSQRT	r20,	r7
	bneid	r21,	$0BB65_173
	ADD	r11,	r25,	r0
	FPNEG	r11,	r25
$0BB65_173:
	ORI	r7,	r0,	0
	FPGE	r8,	r24,	r7
	FPUN	r7,	r24,	r7
	BITOR	r8,	r7,	r8
	bneid	r8,	$0BB65_175
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB65_175:
	bneid	r7,	$0BB65_177
	ADD	r21,	r24,	r0
	FPNEG	r21,	r24
$0BB65_177:
	ORI	r7,	r0,	0
	FPGE	r8,	r23,	r7
	FPUN	r7,	r23,	r7
	BITOR	r8,	r7,	r8
	bneid	r8,	$0BB65_179
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB65_179:
	bneid	r7,	$0BB65_181
	ADD	r8,	r23,	r0
	FPNEG	r8,	r23
$0BB65_181:
	FPGE	r7,	r11,	r21
	FPUN	r12,	r11,	r21
	BITOR	r7,	r12,	r7
	bneid	r7,	$0BB65_183
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB65_183:
	FPDIV	r10,	r10,	r20
	ORI	r20,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r22,	$0BB65_187
	NOP
	FPLT	r11,	r11,	r8
	bneid	r11,	$0BB65_186
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB65_186:
	bneid	r22,	$0BB65_191
	ADD	r11,	r7,	r0
$0BB65_187:
	FPLT	r7,	r21,	r8
	bneid	r7,	$0BB65_189
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB65_189:
	ORI	r11,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r8,	$0BB65_191
	ADD	r20,	r7,	r0
	ORI	r11,	r0,	0
	ORI	r7,	r0,	1065353216
	ADD	r20,	r11,	r0
$0BB65_191:
	FPMUL	r8,	r24,	r20
	FPMUL	r12,	r25,	r11
	FPRSUB	r8,	r8,	r12
	FPMUL	r12,	r23,	r20
	FPMUL	r20,	r25,	r7
	FPRSUB	r20,	r20,	r12
	FPMUL	r12,	r23,	r20
	FPMUL	r21,	r24,	r8
	FPRSUB	r12,	r12,	r21
	FPMUL	r11,	r23,	r11
	FPMUL	r7,	r24,	r7
	FPRSUB	r21,	r11,	r7
	FPMUL	r7,	r25,	r8
	FPMUL	r11,	r23,	r21
	FPRSUB	r11,	r7,	r11
	FPMUL	r7,	r12,	r6
	FPMUL	r12,	r21,	r9
	FPADD	r7,	r12,	r7
	FPMUL	r12,	r25,	r10
	FPADD	r7,	r7,	r12
	FPMUL	r11,	r11,	r6
	FPMUL	r12,	r20,	r9
	FPADD	r11,	r12,	r11
	FPMUL	r12,	r24,	r10
	FPADD	r11,	r11,	r12
	FPMUL	r12,	r24,	r21
	FPMUL	r21,	r25,	r20
	FPMUL	r20,	r11,	r11
	FPMUL	r22,	r7,	r7
	FPADD	r20,	r22,	r20
	FPRSUB	r12,	r12,	r21
	FPMUL	r6,	r12,	r6
	FPMUL	r8,	r8,	r9
	FPADD	r6,	r8,	r6
	FPMUL	r8,	r23,	r10
	FPADD	r6,	r6,	r8
	FPMUL	r8,	r6,	r6
	FPADD	r8,	r20,	r8
	FPINVSQRT	r8,	r8
	ORI	r20,	r0,	1065353216
	FPDIV	r9,	r20,	r8
	FPDIV	r7,	r7,	r9
	FPDIV	r8,	r11,	r9
	FPMUL	r10,	r8,	r8
	FPMUL	r11,	r7,	r7
	FPADD	r10,	r11,	r10
	FPDIV	r6,	r6,	r9
	FPMUL	r9,	r6,	r6
	FPADD	r9,	r10,	r9
	FPINVSQRT	r9,	r9
	FPDIV	r9,	r20,	r9
	FPDIV	r7,	r7,	r9
	FPDIV	r8,	r8,	r9
	FPDIV	r9,	r6,	r9
	FPDIV	r6,	r20,	r9
	FPDIV	r10,	r20,	r8
	FPDIV	r11,	r20,	r7
	ORI	r12,	r0,	0
	FPLT	r23,	r11,	r12
	FPLT	r25,	r10,	r12
	FPLT	r12,	r6,	r12
	ADDI	r24,	r0,	0
	ADDI	r21,	r0,	1
	bneid	r12,	$0BB65_193
	ADD	r20,	r21,	r0
	ADD	r20,	r24,	r0
$0BB65_193:
	bneid	r25,	$0BB65_195
	ADD	r22,	r21,	r0
	ADD	r22,	r24,	r0
$0BB65_195:
	bneid	r23,	$0BB65_197
	NOP
	ADD	r21,	r24,	r0
$0BB65_197:
	LWI	r12,	r1,	396
	LWI	r23,	r1,	288
	SWI	r23,	r12,	0
	LWI	r23,	r1,	284
	SWI	r23,	r12,	4
	LWI	r23,	r1,	280
	SWI	r23,	r12,	8
	SWI	r7,	r12,	12
	SWI	r8,	r12,	16
	SWI	r9,	r12,	20
	SWI	r11,	r12,	24
	SWI	r10,	r12,	28
	SWI	r6,	r12,	32
	SWI	r21,	r12,	36
	SWI	r22,	r12,	40
	SWI	r20,	r12,	44
	LWI	r6,	r1,	352
	SWI	r5,	r6,	0
	SWI	r4,	r6,	4
	SWI	r3,	r6,	8
$0BB65_198:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	376
#	.end	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
$0tmp65:
#	.size	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color, ($tmp65)-_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color

#	.globl	_ZN5syBoxC2Ev
#	.align	2
#	.type	_ZN5syBoxC2Ev,@function
#	.ent	_ZN5syBoxC2Ev           # @_ZN5syBoxC2Ev
_ZN5syBoxC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r0,	r5,	20
	SWI	r0,	r5,	16
	SWI	r0,	r5,	12
	SWI	r0,	r5,	8
	SWI	r0,	r5,	4
	rtsd	r15,	8
	SWI	r0,	r5,	0
#	.end	_ZN5syBoxC2Ev
$0tmp66:
#	.size	_ZN5syBoxC2Ev, ($tmp66)-_ZN5syBoxC2Ev

#	.globl	_ZN5syBox3SetEv
#	.align	2
#	.type	_ZN5syBox3SetEv,@function
#	.ent	_ZN5syBox3SetEv         # @_ZN5syBox3SetEv
_ZN5syBox3SetEv:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r0,	r5,	20
	SWI	r0,	r5,	16
	SWI	r0,	r5,	12
	SWI	r0,	r5,	8
	SWI	r0,	r5,	4
	rtsd	r15,	8
	SWI	r0,	r5,	0
#	.end	_ZN5syBox3SetEv
$0tmp67:
#	.size	_ZN5syBox3SetEv, ($tmp67)-_ZN5syBox3SetEv

#	.globl	_ZN5syBoxC2ERK8syVectorS2_
#	.align	2
#	.type	_ZN5syBoxC2ERK8syVectorS2_,@function
#	.ent	_ZN5syBoxC2ERK8syVectorS2_ # @_ZN5syBoxC2ERK8syVectorS2_
_ZN5syBoxC2ERK8syVectorS2_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	SWI	r3,	r5,	12
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	8
	rtsd	r15,	8
	SWI	r3,	r5,	20
#	.end	_ZN5syBoxC2ERK8syVectorS2_
$0tmp68:
#	.size	_ZN5syBoxC2ERK8syVectorS2_, ($tmp68)-_ZN5syBoxC2ERK8syVectorS2_

#	.globl	_ZN5syBox3SetERK8syVectorS2_
#	.align	2
#	.type	_ZN5syBox3SetERK8syVectorS2_,@function
#	.ent	_ZN5syBox3SetERK8syVectorS2_ # @_ZN5syBox3SetERK8syVectorS2_
_ZN5syBox3SetERK8syVectorS2_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	SWI	r3,	r5,	12
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	8
	rtsd	r15,	8
	SWI	r3,	r5,	20
#	.end	_ZN5syBox3SetERK8syVectorS2_
$0tmp69:
#	.size	_ZN5syBox3SetERK8syVectorS2_, ($tmp69)-_ZN5syBox3SetERK8syVectorS2_

#	.globl	_ZN5syBox14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN5syBox14LoadFromMemoryERKi,@function
#	.ent	_ZN5syBox14LoadFromMemoryERKi # @_ZN5syBox14LoadFromMemoryERKi
_ZN5syBox14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	20
#	.end	_ZN5syBox14LoadFromMemoryERKi
$0tmp70:
#	.size	_ZN5syBox14LoadFromMemoryERKi, ($tmp70)-_ZN5syBox14LoadFromMemoryERKi

#	.globl	_ZNK5syBox11IntersectsWERK3RayR7HitDist
#	.align	2
#	.type	_ZNK5syBox11IntersectsWERK3RayR7HitDist,@function
#	.ent	_ZNK5syBox11IntersectsWERK3RayR7HitDist # @_ZNK5syBox11IntersectsWERK3RayR7HitDist
_ZNK5syBox11IntersectsWERK3RayR7HitDist:
#	.frame	r1,12,r15
#	.mask	0x700000
	ADDI	r1,	r1,	-12
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	SWI	r22,	r1,	0
	LWI	r4,	r6,	40
	RSUBI	r3,	r4,	1
	MULI	r3,	r3,	12
	ADD	r3,	r5,	r3
	LWI	r9,	r6,	36
	MULI	r8,	r9,	12
	LW	r8,	r5,	r8
	LWI	r11,	r6,	0
	FPRSUB	r8,	r11,	r8
	LWI	r12,	r6,	24
	FPMUL	r10,	r8,	r12
	LWI	r3,	r3,	4
	LWI	r20,	r6,	4
	FPRSUB	r3,	r20,	r3
	LWI	r21,	r6,	28
	FPMUL	r8,	r3,	r21
	ADDI	r22,	r0,	1
	FPGT	r3,	r10,	r8
	bneid	r3,	$0BB71_2
	NOP
	ADDI	r22,	r0,	0
$0BB71_2:
	bneid	r22,	$0BB71_32
	ADD	r3,	r0,	r0
	MULI	r4,	r4,	12
	ADD	r4,	r5,	r4
	LWI	r4,	r4,	4
	FPRSUB	r4,	r20,	r4
	FPMUL	r4,	r4,	r21
	RSUBI	r9,	r9,	1
	MULI	r9,	r9,	12
	LW	r9,	r5,	r9
	FPRSUB	r9,	r11,	r9
	FPMUL	r9,	r9,	r12
	FPGT	r12,	r4,	r9
	bneid	r12,	$0BB71_5
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB71_5:
	bneid	r11,	$0BB71_32
	NOP
	FPGT	r20,	r4,	r10
	ADDI	r3,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB71_8
	ADD	r12,	r11,	r0
	ADD	r12,	r3,	r0
$0BB71_8:
	bneid	r12,	$0BB71_10
	NOP
	ADD	r4,	r10,	r0
$0BB71_10:
	LWI	r10,	r6,	44
	RSUBI	r12,	r10,	1
	MULI	r12,	r12,	12
	ADD	r12,	r5,	r12
	FPLT	r20,	r8,	r9
	bneid	r20,	$0BB71_12
	ADD	r21,	r11,	r0
	ADD	r21,	r3,	r0
$0BB71_12:
	LWI	r20,	r12,	8
	LWI	r12,	r6,	8
	FPRSUB	r22,	r12,	r20
	LWI	r20,	r6,	32
	FPMUL	r6,	r22,	r20
	FPGT	r22,	r4,	r6
	bneid	r22,	$0BB71_14
	NOP
	ADD	r11,	r3,	r0
$0BB71_14:
	bneid	r21,	$0BB71_16
	NOP
	ADD	r8,	r9,	r0
$0BB71_16:
	bneid	r11,	$0BB71_32
	ADD	r3,	r0,	r0
	MULI	r9,	r10,	12
	ADD	r5,	r5,	r9
	LWI	r5,	r5,	8
	FPRSUB	r5,	r12,	r5
	FPMUL	r5,	r5,	r20
	FPGT	r10,	r5,	r8
	bneid	r10,	$0BB71_19
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB71_19:
	bneid	r9,	$0BB71_32
	NOP
	FPLT	r11,	r6,	r8
	ADDI	r3,	r0,	0
	ADDI	r9,	r0,	1
	bneid	r11,	$0BB71_22
	ADD	r10,	r9,	r0
	ADD	r10,	r3,	r0
$0BB71_22:
	bneid	r10,	$0BB71_24
	NOP
	ADD	r6,	r8,	r0
$0BB71_24:
	ORI	r8,	r0,	0
	FPLT	r6,	r6,	r8
	bneid	r6,	$0BB71_26
	NOP
	ADD	r9,	r3,	r0
$0BB71_26:
	bneid	r9,	$0BB71_32
	ADD	r3,	r0,	r0
	FPGT	r8,	r5,	r4
	ADDI	r3,	r0,	1
	bneid	r8,	$0BB71_29
	ADD	r6,	r3,	r0
	ADDI	r6,	r0,	0
$0BB71_29:
	bneid	r6,	$0BB71_31
	NOP
	ADD	r5,	r4,	r0
$0BB71_31:
	SWI	r5,	r7,	0
$0BB71_32:
	LWI	r22,	r1,	0
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_ZNK5syBox11IntersectsWERK3RayR7HitDist
$0tmp71:
#	.size	_ZNK5syBox11IntersectsWERK3RayR7HitDist, ($tmp71)-_ZNK5syBox11IntersectsWERK3RayR7HitDist

#	.globl	_ZN9syBVHNodeC2Ei
#	.align	2
#	.type	_ZN9syBVHNodeC2Ei,@function
#	.ent	_ZN9syBVHNodeC2Ei       # @_ZN9syBVHNodeC2Ei
_ZN9syBVHNodeC2Ei:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r0,	r5,	20
	SWI	r0,	r5,	16
	SWI	r0,	r5,	12
	SWI	r0,	r5,	8
	SWI	r0,	r5,	4
	SWI	r0,	r5,	0
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	SWI	r3,	r5,	8
	ADDI	r3,	r6,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	ADDI	r3,	r6,	6
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	24
	ADDI	r3,	r6,	7
	LOAD	r3,	r3,	0
	rtsd	r15,	8
	SWI	r3,	r5,	28
#	.end	_ZN9syBVHNodeC2Ei
$0tmp72:
#	.size	_ZN9syBVHNodeC2Ei, ($tmp72)-_ZN9syBVHNodeC2Ei

#	.globl	_ZN9syBVHNode14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN9syBVHNode14LoadFromMemoryERKi,@function
#	.ent	_ZN9syBVHNode14LoadFromMemoryERKi # @_ZN9syBVHNode14LoadFromMemoryERKi
_ZN9syBVHNode14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	6
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	24
	LWI	r3,	r6,	0
	ADDI	r3,	r3,	7
	LOAD	r3,	r3,	0
	rtsd	r15,	8
	SWI	r3,	r5,	28
#	.end	_ZN9syBVHNode14LoadFromMemoryERKi
$0tmp73:
#	.size	_ZN9syBVHNode14LoadFromMemoryERKi, ($tmp73)-_ZN9syBVHNode14LoadFromMemoryERKi

#	.globl	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
#	.align	2
#	.type	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist,@function
#	.ent	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist # @_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist:
#	.frame	r1,12,r15
#	.mask	0x700000
	ADDI	r1,	r1,	-12
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	SWI	r22,	r1,	0
	LWI	r4,	r6,	40
	RSUBI	r3,	r4,	1
	MULI	r3,	r3,	12
	ADD	r3,	r5,	r3
	LWI	r9,	r6,	36
	MULI	r8,	r9,	12
	LW	r8,	r5,	r8
	LWI	r11,	r6,	0
	FPRSUB	r8,	r11,	r8
	LWI	r12,	r6,	24
	FPMUL	r10,	r8,	r12
	LWI	r3,	r3,	4
	LWI	r20,	r6,	4
	FPRSUB	r3,	r20,	r3
	LWI	r21,	r6,	28
	FPMUL	r8,	r3,	r21
	ADDI	r22,	r0,	1
	FPGT	r3,	r10,	r8
	bneid	r3,	$0BB74_2
	NOP
	ADDI	r22,	r0,	0
$0BB74_2:
	bneid	r22,	$0BB74_32
	ADD	r3,	r0,	r0
	MULI	r4,	r4,	12
	ADD	r4,	r5,	r4
	LWI	r4,	r4,	4
	FPRSUB	r4,	r20,	r4
	FPMUL	r4,	r4,	r21
	RSUBI	r9,	r9,	1
	MULI	r9,	r9,	12
	LW	r9,	r5,	r9
	FPRSUB	r9,	r11,	r9
	FPMUL	r9,	r9,	r12
	FPGT	r12,	r4,	r9
	bneid	r12,	$0BB74_5
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB74_5:
	bneid	r11,	$0BB74_32
	NOP
	FPGT	r20,	r4,	r10
	ADDI	r3,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r20,	$0BB74_8
	ADD	r12,	r11,	r0
	ADD	r12,	r3,	r0
$0BB74_8:
	bneid	r12,	$0BB74_10
	NOP
	ADD	r4,	r10,	r0
$0BB74_10:
	LWI	r10,	r6,	44
	RSUBI	r12,	r10,	1
	MULI	r12,	r12,	12
	ADD	r12,	r5,	r12
	FPLT	r20,	r8,	r9
	bneid	r20,	$0BB74_12
	ADD	r21,	r11,	r0
	ADD	r21,	r3,	r0
$0BB74_12:
	LWI	r20,	r12,	8
	LWI	r12,	r6,	8
	FPRSUB	r22,	r12,	r20
	LWI	r20,	r6,	32
	FPMUL	r6,	r22,	r20
	FPGT	r22,	r4,	r6
	bneid	r22,	$0BB74_14
	NOP
	ADD	r11,	r3,	r0
$0BB74_14:
	bneid	r21,	$0BB74_16
	NOP
	ADD	r8,	r9,	r0
$0BB74_16:
	bneid	r11,	$0BB74_32
	ADD	r3,	r0,	r0
	MULI	r9,	r10,	12
	ADD	r5,	r5,	r9
	LWI	r5,	r5,	8
	FPRSUB	r5,	r12,	r5
	FPMUL	r5,	r5,	r20
	FPGT	r10,	r5,	r8
	bneid	r10,	$0BB74_19
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB74_19:
	bneid	r9,	$0BB74_32
	NOP
	FPLT	r11,	r6,	r8
	ADDI	r3,	r0,	0
	ADDI	r9,	r0,	1
	bneid	r11,	$0BB74_22
	ADD	r10,	r9,	r0
	ADD	r10,	r3,	r0
$0BB74_22:
	bneid	r10,	$0BB74_24
	NOP
	ADD	r6,	r8,	r0
$0BB74_24:
	ORI	r8,	r0,	0
	FPLT	r6,	r6,	r8
	bneid	r6,	$0BB74_26
	NOP
	ADD	r9,	r3,	r0
$0BB74_26:
	bneid	r9,	$0BB74_32
	ADD	r3,	r0,	r0
	FPGT	r8,	r5,	r4
	ADDI	r3,	r0,	1
	bneid	r8,	$0BB74_29
	ADD	r6,	r3,	r0
	ADDI	r6,	r0,	0
$0BB74_29:
	bneid	r6,	$0BB74_31
	NOP
	ADD	r5,	r4,	r0
$0BB74_31:
	SWI	r5,	r7,	0
$0BB74_32:
	LWI	r22,	r1,	0
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
$0tmp74:
#	.size	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist, ($tmp74)-_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist

#	.globl	_ZNK9syBVHNode8InteriorEv
#	.align	2
#	.type	_ZNK9syBVHNode8InteriorEv,@function
#	.ent	_ZNK9syBVHNode8InteriorEv # @_ZNK9syBVHNode8InteriorEv
_ZNK9syBVHNode8InteriorEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	24
	ADDI	r4,	r0,	-1
	CMP	r4,	r4,	r3
	beqid	r4,	$0BB75_2
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB75_2:
	rtsd	r15,	8
	NOP
#	.end	_ZNK9syBVHNode8InteriorEv
$0tmp75:
#	.size	_ZNK9syBVHNode8InteriorEv, ($tmp75)-_ZNK9syBVHNode8InteriorEv

#	.globl	_ZNK9syBVHNode11LeftChildIDEv
#	.align	2
#	.type	_ZNK9syBVHNode11LeftChildIDEv,@function
#	.ent	_ZNK9syBVHNode11LeftChildIDEv # @_ZNK9syBVHNode11LeftChildIDEv
_ZNK9syBVHNode11LeftChildIDEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	LWI	r3,	r5,	28
#	.end	_ZNK9syBVHNode11LeftChildIDEv
$0tmp76:
#	.size	_ZNK9syBVHNode11LeftChildIDEv, ($tmp76)-_ZNK9syBVHNode11LeftChildIDEv

#	.globl	_ZNK9syBVHNode12RightChildIDEv
#	.align	2
#	.type	_ZNK9syBVHNode12RightChildIDEv,@function
#	.ent	_ZNK9syBVHNode12RightChildIDEv # @_ZNK9syBVHNode12RightChildIDEv
_ZNK9syBVHNode12RightChildIDEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	28
	rtsd	r15,	8
	ADDI	r3,	r3,	1
#	.end	_ZNK9syBVHNode12RightChildIDEv
$0tmp77:
#	.size	_ZNK9syBVHNode12RightChildIDEv, ($tmp77)-_ZNK9syBVHNode12RightChildIDEv

#	.globl	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
#	.align	2
#	.type	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist,@function
#	.ent	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist # @_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist:
#	.frame	r1,52,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-52
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r7,	r1,	64
	LWI	r3,	r5,	24
	beqid	r3,	$0BB78_26
	SWI	r5,	r1,	56
	ADD	r3,	r0,	r0
$0BB78_2:
	MULI	r4,	r3,	11
	LWI	r5,	r5,	28
	ADD	r5,	r5,	r4
	LOAD	r21,	r5,	0
	LOAD	r22,	r5,	1
	LOAD	r24,	r5,	2
	ADDI	r4,	r5,	6
	ADDI	r5,	r5,	3
	LOAD	r7,	r5,	0
	LOAD	r20,	r5,	1
	LOAD	r10,	r5,	2
	LOAD	r9,	r4,	0
	LOAD	r5,	r4,	1
	LOAD	r4,	r4,	2
	FPRSUB	r11,	r24,	r4
	FPRSUB	r23,	r22,	r5
	LWI	r4,	r6,	20
	FPMUL	r5,	r4,	r23
	LWI	r8,	r6,	16
	FPMUL	r12,	r8,	r11
	FPRSUB	r26,	r5,	r12
	SWI	r26,	r1,	48
	FPRSUB	r25,	r21,	r9
	LWI	r9,	r6,	12
	FPMUL	r5,	r9,	r11
	FPMUL	r12,	r4,	r25
	FPRSUB	r12,	r5,	r12
	FPRSUB	r28,	r21,	r7
	FPRSUB	r27,	r22,	r20
	FPMUL	r5,	r12,	r27
	FPMUL	r7,	r26,	r28
	FPADD	r5,	r7,	r5
	FPMUL	r7,	r8,	r25
	FPMUL	r20,	r9,	r23
	FPRSUB	r20,	r7,	r20
	FPRSUB	r29,	r24,	r10
	FPMUL	r7,	r20,	r29
	FPADD	r5,	r5,	r7
	ORI	r7,	r0,	0
	FPGE	r10,	r5,	r7
	FPUN	r7,	r5,	r7
	BITOR	r7,	r7,	r10
	bneid	r7,	$0BB78_4
	ADDI	r30,	r0,	1
	ADDI	r30,	r0,	0
$0BB78_4:
	LWI	r10,	r6,	8
	LWI	r31,	r6,	0
	LWI	r7,	r6,	4
	bneid	r30,	$0BB78_6
	ADD	r26,	r5,	r0
	FPNEG	r26,	r5
$0BB78_6:
	ORI	r30,	r0,	953267991
	FPLT	r30,	r26,	r30
	bneid	r30,	$0BB78_8
	ADDI	r26,	r0,	1
	ADDI	r26,	r0,	0
$0BB78_8:
	bneid	r26,	$0BB78_25
	NOP
	FPRSUB	r30,	r24,	r10
	FPRSUB	r31,	r21,	r31
	FPRSUB	r10,	r22,	r7
	FPMUL	r7,	r10,	r28
	FPMUL	r21,	r31,	r27
	FPMUL	r22,	r30,	r28
	FPMUL	r24,	r31,	r29
	FPMUL	r26,	r30,	r27
	FPMUL	r27,	r10,	r29
	FPRSUB	r21,	r7,	r21
	FPRSUB	r24,	r24,	r22
	FPRSUB	r22,	r26,	r27
	FPMUL	r7,	r24,	r23
	FPMUL	r23,	r22,	r25
	FPADD	r7,	r23,	r7
	FPMUL	r11,	r21,	r11
	FPADD	r7,	r7,	r11
	ORI	r11,	r0,	1065353216
	FPDIV	r23,	r11,	r5
	FPMUL	r11,	r7,	r23
	ORI	r5,	r0,	0
	FPLT	r7,	r11,	r5
	bneid	r7,	$0BB78_11
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB78_11:
	bneid	r5,	$0BB78_25
	NOP
	FPMUL	r5,	r12,	r10
	LWI	r7,	r1,	48
	FPMUL	r7,	r7,	r31
	FPADD	r5,	r7,	r5
	FPMUL	r7,	r20,	r30
	FPADD	r5,	r5,	r7
	FPMUL	r10,	r5,	r23
	ORI	r5,	r0,	0
	FPLT	r7,	r10,	r5
	FPUN	r5,	r10,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB78_14
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB78_14:
	bneid	r5,	$0BB78_25
	NOP
	FPMUL	r5,	r24,	r8
	FPMUL	r7,	r22,	r9
	FPADD	r5,	r7,	r5
	FPMUL	r4,	r21,	r4
	FPADD	r4,	r5,	r4
	FPMUL	r4,	r4,	r23
	ORI	r5,	r0,	0
	FPLT	r7,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB78_17
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB78_17:
	bneid	r5,	$0BB78_25
	NOP
	FPADD	r4,	r10,	r4
	ORI	r5,	r0,	1065353216
	FPGT	r7,	r4,	r5
	FPUN	r4,	r4,	r5
	BITOR	r5,	r4,	r7
	bneid	r5,	$0BB78_20
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB78_20:
	bneid	r4,	$0BB78_25
	NOP
	LWI	r4,	r1,	64
	LWI	r4,	r4,	0
	FPGE	r5,	r11,	r4
	FPUN	r4,	r11,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB78_23
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB78_23:
	bneid	r4,	$0BB78_25
	NOP
	LWI	r4,	r1,	64
	SWI	r11,	r4,	0
$0BB78_25:
	ADDI	r3,	r3,	1
	LWI	r5,	r1,	56
	LWI	r4,	r5,	24
	CMP	r4,	r4,	r3
	bneid	r4,	$0BB78_2
	NOP
$0BB78_26:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	52
#	.end	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
$0tmp78:
#	.size	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist, ($tmp78)-_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist

#	.globl	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
#	.align	2
#	.type	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord,@function
#	.ent	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord # @_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord:
#	.frame	r1,56,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-56
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r7,	r1,	68
	SWI	r6,	r1,	64
	LWI	r3,	r5,	24
	beqid	r3,	$0BB79_26
	SWI	r5,	r1,	60
	ADD	r3,	r0,	r0
$0BB79_2:
	MULI	r4,	r3,	11
	LWI	r5,	r5,	28
	ADD	r4,	r5,	r4
	LOAD	r22,	r4,	0
	LOAD	r23,	r4,	1
	LOAD	r25,	r4,	2
	ADDI	r5,	r4,	3
	LOAD	r7,	r5,	0
	LOAD	r11,	r5,	1
	LOAD	r5,	r5,	2
	ADDI	r9,	r4,	6
	LOAD	r10,	r9,	0
	LOAD	r8,	r9,	1
	LOAD	r9,	r9,	2
	FPRSUB	r12,	r25,	r9
	FPRSUB	r24,	r23,	r8
	LWI	r8,	r6,	20
	FPMUL	r21,	r8,	r24
	LWI	r9,	r6,	16
	FPMUL	r20,	r9,	r12
	FPRSUB	r27,	r21,	r20
	SWI	r27,	r1,	48
	FPRSUB	r26,	r22,	r10
	LWI	r10,	r6,	12
	FPMUL	r21,	r10,	r12
	FPMUL	r20,	r8,	r26
	FPRSUB	r20,	r21,	r20
	SWI	r20,	r1,	52
	FPRSUB	r29,	r22,	r7
	FPRSUB	r28,	r23,	r11
	FPMUL	r7,	r20,	r28
	FPMUL	r11,	r27,	r29
	FPADD	r7,	r11,	r7
	FPMUL	r11,	r9,	r26
	FPMUL	r20,	r10,	r24
	FPRSUB	r21,	r11,	r20
	FPRSUB	r30,	r25,	r5
	FPMUL	r5,	r21,	r30
	FPADD	r20,	r7,	r5
	ORI	r5,	r0,	0
	FPGE	r7,	r20,	r5
	FPUN	r5,	r20,	r5
	BITOR	r5,	r5,	r7
	bneid	r5,	$0BB79_4
	ADDI	r31,	r0,	1
	ADDI	r31,	r0,	0
$0BB79_4:
	LWI	r5,	r6,	8
	LWI	r11,	r6,	0
	LWI	r7,	r6,	4
	bneid	r31,	$0BB79_6
	ADD	r27,	r20,	r0
	FPNEG	r27,	r20
$0BB79_6:
	ORI	r31,	r0,	953267991
	FPLT	r31,	r27,	r31
	bneid	r31,	$0BB79_8
	ADDI	r27,	r0,	1
	ADDI	r27,	r0,	0
$0BB79_8:
	bneid	r27,	$0BB79_25
	NOP
	FPRSUB	r31,	r25,	r5
	FPRSUB	r11,	r22,	r11
	FPRSUB	r5,	r23,	r7
	FPMUL	r7,	r5,	r29
	FPMUL	r22,	r11,	r28
	FPMUL	r23,	r31,	r29
	FPMUL	r25,	r11,	r30
	FPMUL	r27,	r31,	r28
	FPMUL	r28,	r5,	r30
	FPRSUB	r22,	r7,	r22
	FPRSUB	r25,	r25,	r23
	FPRSUB	r23,	r27,	r28
	FPMUL	r7,	r25,	r24
	FPMUL	r24,	r23,	r26
	FPADD	r7,	r24,	r7
	FPMUL	r12,	r22,	r12
	FPADD	r7,	r7,	r12
	ORI	r12,	r0,	1065353216
	FPDIV	r24,	r12,	r20
	FPMUL	r12,	r7,	r24
	ORI	r7,	r0,	0
	FPLT	r20,	r12,	r7
	bneid	r20,	$0BB79_11
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB79_11:
	bneid	r7,	$0BB79_25
	NOP
	LWI	r6,	r1,	52
	FPMUL	r5,	r6,	r5
	LWI	r6,	r1,	48
	FPMUL	r6,	r6,	r11
	FPADD	r5,	r6,	r5
	FPMUL	r6,	r21,	r31
	FPADD	r5,	r5,	r6
	FPMUL	r11,	r5,	r24
	ORI	r5,	r0,	0
	FPLT	r6,	r11,	r5
	FPUN	r5,	r11,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB79_14
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB79_14:
	bneid	r5,	$0BB79_25
	NOP
	FPMUL	r5,	r25,	r9
	FPMUL	r6,	r23,	r10
	FPADD	r5,	r6,	r5
	FPMUL	r6,	r22,	r8
	FPADD	r5,	r5,	r6
	FPMUL	r5,	r5,	r24
	ORI	r6,	r0,	0
	FPLT	r7,	r5,	r6
	FPUN	r6,	r5,	r6
	BITOR	r7,	r6,	r7
	bneid	r7,	$0BB79_17
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB79_17:
	bneid	r6,	$0BB79_25
	NOP
	FPADD	r5,	r11,	r5
	ORI	r6,	r0,	1065353216
	FPGT	r7,	r5,	r6
	FPUN	r5,	r5,	r6
	BITOR	r6,	r5,	r7
	bneid	r6,	$0BB79_20
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB79_20:
	bneid	r5,	$0BB79_25
	NOP
	LWI	r5,	r1,	68
	LWI	r5,	r5,	0
	FPGE	r6,	r12,	r5
	FPUN	r5,	r12,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB79_23
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB79_23:
	bneid	r5,	$0BB79_25
	NOP
	LWI	r6,	r1,	68
	SWI	r12,	r6,	0
	ADDI	r5,	r4,	10
	LOAD	r5,	r5,	0
	SWI	r5,	r6,	4
	SWI	r4,	r6,	8
$0BB79_25:
	ADDI	r3,	r3,	1
	LWI	r5,	r1,	60
	LWI	r4,	r5,	24
	CMP	r4,	r4,	r3
	bneid	r4,	$0BB79_2
	LWI	r6,	r1,	64
$0BB79_26:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	56
#	.end	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
$0tmp79:
#	.size	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord, ($tmp79)-_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord

#	.globl	_ZN8syMatrixC2Ev
#	.align	2
#	.type	_ZN8syMatrixC2Ev,@function
#	.ent	_ZN8syMatrixC2Ev        # @_ZN8syMatrixC2Ev
_ZN8syMatrixC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN8syMatrixC2Ev
$0tmp80:
#	.size	_ZN8syMatrixC2Ev, ($tmp80)-_ZN8syMatrixC2Ev

#	.globl	_ZN8syMatrix4ZeroEv
#	.align	2
#	.type	_ZN8syMatrix4ZeroEv,@function
#	.ent	_ZN8syMatrix4ZeroEv     # @_ZN8syMatrix4ZeroEv
_ZN8syMatrix4ZeroEv:
#	.frame	r1,4,r15
#	.mask	0x8000
	ADDI	r1,	r1,	-4
	SWI	r15,	r1,	0
	ADD	r6,	r0,	r0
	brlid	r15,	memset
	ADDI	r7,	r0,	36
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_ZN8syMatrix4ZeroEv
$0tmp81:
#	.size	_ZN8syMatrix4ZeroEv, ($tmp81)-_ZN8syMatrix4ZeroEv

#	.globl	_ZN8syMatrix8IdentityEv
#	.align	2
#	.type	_ZN8syMatrix8IdentityEv,@function
#	.ent	_ZN8syMatrix8IdentityEv # @_ZN8syMatrix8IdentityEv
_ZN8syMatrix8IdentityEv:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	0
	SWI	r0,	r5,	4
	SWI	r0,	r5,	8
	SWI	r0,	r5,	12
	SWI	r3,	r5,	16
	SWI	r0,	r5,	20
	SWI	r0,	r5,	24
	SWI	r0,	r5,	28
	rtsd	r15,	8
	SWI	r3,	r5,	32
#	.end	_ZN8syMatrix8IdentityEv
$0tmp82:
#	.size	_ZN8syMatrix8IdentityEv, ($tmp82)-_ZN8syMatrix8IdentityEv

#	.globl	_ZNK8syMatrix9GetColumnEi
#	.align	2
#	.type	_ZNK8syMatrix9GetColumnEi,@function
#	.ent	_ZNK8syMatrix9GetColumnEi # @_ZNK8syMatrix9GetColumnEi
_ZNK8syMatrix9GetColumnEi:
#	.frame	r1,0,r15
#	.mask	0x0
	MULI	r3,	r7,	3
	bslli	r3,	r3,	2
	LW	r4,	r6,	r3
	SWI	r4,	r5,	0
	ADD	r3,	r6,	r3
	LWI	r4,	r3,	4
	SWI	r4,	r5,	4
	LWI	r3,	r3,	8
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syMatrix9GetColumnEi
$0tmp83:
#	.size	_ZNK8syMatrix9GetColumnEi, ($tmp83)-_ZNK8syMatrix9GetColumnEi

#	.globl	_ZN8syMatrix3LUDEPiRi
#	.align	2
#	.type	_ZN8syMatrix3LUDEPiRi,@function
#	.ent	_ZN8syMatrix3LUDEPiRi   # @_ZN8syMatrix3LUDEPiRi
_ZN8syMatrix3LUDEPiRi:
#	.frame	r1,24,r15
#	.mask	0x700000
	ADDI	r1,	r1,	-24
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	SWI	r22,	r1,	0
	ADDI	r3,	r0,	1
	SWI	r3,	r7,	0
	brid	$0BB84_1
	ADD	r4,	r0,	r0
$0BB84_92:
	ORI	r3,	r0,	1065353216
	FPDIV	r3,	r3,	r9
	ADDI	r9,	r1,	12
	SW	r3,	r9,	r8
	ADDI	r4,	r4,	1
$0BB84_1:
	ADDI	r3,	r0,	2
	CMP	r3,	r3,	r4
	bgtid	r3,	$0BB84_2
	NOP
	bslli	r8,	r4,	2
	LW	r3,	r5,	r8
	ORI	r9,	r0,	0
	FPLT	r10,	r3,	r9
	bneid	r10,	$0BB84_34
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB84_34:
	beqid	r9,	$0BB84_35
	NOP
	ORI	r9,	r0,	-2147483648
	FPGE	r10,	r3,	r9
	FPUN	r9,	r3,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB84_41
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_41:
	ORI	r9,	r0,	0
	bneid	r10,	$0BB84_43
	NOP
	ADD	r9,	r3,	r0
$0BB84_43:
	bneid	r10,	$0BB84_45
	NOP
	FPNEG	r9,	r3
	brid	$0BB84_45
	NOP
$0BB84_35:
	ORI	r9,	r0,	0
	FPLE	r10,	r3,	r9
	FPUN	r11,	r3,	r9
	BITOR	r11,	r11,	r10
	bneid	r11,	$0BB84_37
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_37:
	bneid	r10,	$0BB84_45
	NOP
	ADD	r9,	r3,	r0
$0BB84_45:
	ADD	r3,	r8,	r5
	LWI	r3,	r3,	12
	ORI	r10,	r0,	0
	FPLT	r11,	r3,	r10
	bneid	r11,	$0BB84_47
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_47:
	beqid	r10,	$0BB84_48
	NOP
	FPNEG	r10,	r3
	FPGE	r11,	r9,	r10
	FPUN	r12,	r9,	r10
	BITOR	r12,	r12,	r11
	bneid	r12,	$0BB84_72
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB84_72:
	bneid	r11,	$0BB84_74
	NOP
	ADD	r9,	r3,	r0
$0BB84_74:
	bneid	r11,	$0BB84_76
	NOP
	brid	$0BB84_76
	ADD	r9,	r10,	r0
$0BB84_48:
	FPLE	r10,	r3,	r9
	FPUN	r11,	r3,	r9
	BITOR	r11,	r11,	r10
	bneid	r11,	$0BB84_50
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_50:
	bneid	r10,	$0BB84_76
	NOP
	ADD	r9,	r3,	r0
$0BB84_76:
	ADD	r3,	r8,	r5
	LWI	r3,	r3,	24
	ORI	r10,	r0,	0
	FPLT	r11,	r3,	r10
	bneid	r11,	$0BB84_78
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_78:
	beqid	r10,	$0BB84_79
	NOP
	FPNEG	r10,	r3
	FPGE	r11,	r9,	r10
	FPUN	r12,	r9,	r10
	BITOR	r12,	r12,	r11
	bneid	r12,	$0BB84_85
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB84_85:
	bneid	r11,	$0BB84_87
	NOP
	ADD	r9,	r3,	r0
$0BB84_87:
	bneid	r11,	$0BB84_89
	NOP
	brid	$0BB84_89
	ADD	r9,	r10,	r0
$0BB84_79:
	FPLE	r10,	r3,	r9
	FPUN	r11,	r3,	r9
	BITOR	r11,	r11,	r10
	bneid	r11,	$0BB84_81
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_81:
	bneid	r10,	$0BB84_89
	NOP
	ADD	r9,	r3,	r0
$0BB84_89:
	ORI	r3,	r0,	0
	FPEQ	r3,	r9,	r3
	bneid	r3,	$0BB84_91
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB84_91:
	bneid	r10,	$0BB84_31
	ADD	r3,	r0,	r0
	brid	$0BB84_92
	NOP
$0BB84_2:
	ADD	r3,	r0,	r0
$0BB84_3:
	ORI	r9,	r0,	0
	ADDI	r8,	r0,	1
	CMP	r10,	r8,	r3
	bltid	r10,	$0BB84_4
	MULI	r8,	r3,	3
	ADD	r10,	r0,	r0
$0BB84_53:
	ADDI	r9,	r0,	1
	CMP	r9,	r9,	r10
	bltid	r9,	$0BB84_55
	NOP
	ADD	r9,	r10,	r8
	bslli	r11,	r8,	2
	LW	r11,	r5,	r11
	bslli	r12,	r10,	2
	LW	r12,	r5,	r12
	FPMUL	r11,	r12,	r11
	bslli	r9,	r9,	2
	LW	r12,	r5,	r9
	FPRSUB	r11,	r11,	r12
	SW	r11,	r5,	r9
$0BB84_55:
	ORI	r9,	r0,	0
	ADDI	r10,	r10,	1
	CMP	r11,	r3,	r10
	bltid	r11,	$0BB84_53
	NOP
	ADD	r10,	r3,	r0
$0BB84_65:
	ADD	r11,	r10,	r8
	bslli	r12,	r11,	2
	LW	r11,	r5,	r12
	ADD	r20,	r0,	r0
$0BB84_66:
	MULI	r21,	r20,	3
	ADD	r21,	r21,	r10
	ADD	r22,	r20,	r8
	bslli	r22,	r22,	2
	LW	r22,	r5,	r22
	bslli	r21,	r21,	2
	LW	r21,	r5,	r21
	FPMUL	r21,	r21,	r22
	FPRSUB	r11,	r21,	r11
	ADDI	r20,	r20,	1
	CMP	r21,	r3,	r20
	bltid	r21,	$0BB84_66
	NOP
	SW	r11,	r5,	r12
	ORI	r12,	r0,	0
	FPLT	r12,	r11,	r12
	bneid	r12,	$0BB84_69
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB84_69:
	bslli	r12,	r10,	2
	ADDI	r21,	r1,	12
	beqid	r20,	$0BB84_58
	LW	r12,	r21,	r12
	FPNEG	r11,	r11
$0BB84_58:
	FPMUL	r11,	r12,	r11
	FPLT	r12,	r11,	r9
	FPUN	r20,	r11,	r9
	BITOR	r20,	r20,	r12
	bneid	r20,	$0BB84_60
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB84_60:
	bneid	r12,	$0BB84_62
	NOP
	ADD	r9,	r11,	r0
$0BB84_62:
	bneid	r12,	$0BB84_64
	NOP
	ADD	r4,	r10,	r0
$0BB84_64:
	ADDI	r10,	r10,	1
	ADDI	r11,	r0,	3
	CMP	r11,	r11,	r10
	bgeid	r11,	$0BB84_16
	NOP
	brid	$0BB84_65
	NOP
$0BB84_4:
	ADD	r10,	r3,	r0
$0BB84_5:
	ADD	r11,	r10,	r8
	bslli	r11,	r11,	2
	LW	r11,	r5,	r11
	ORI	r12,	r0,	0
	FPGE	r20,	r11,	r12
	FPUN	r12,	r11,	r12
	BITOR	r12,	r12,	r20
	bneid	r12,	$0BB84_7
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB84_7:
	bslli	r12,	r10,	2
	ADDI	r21,	r1,	12
	bneid	r20,	$0BB84_9
	LW	r12,	r21,	r12
	FPNEG	r11,	r11
$0BB84_9:
	FPMUL	r11,	r12,	r11
	FPLT	r12,	r11,	r9
	FPUN	r20,	r11,	r9
	BITOR	r20,	r20,	r12
	bneid	r20,	$0BB84_11
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB84_11:
	bneid	r12,	$0BB84_13
	NOP
	ADD	r9,	r11,	r0
$0BB84_13:
	bneid	r12,	$0BB84_15
	NOP
	ADD	r4,	r10,	r0
$0BB84_15:
	ADDI	r10,	r10,	1
	ADDI	r11,	r0,	3
	CMP	r11,	r11,	r10
	bltid	r11,	$0BB84_5
	NOP
$0BB84_16:
	CMP	r9,	r4,	r3
	beqid	r9,	$0BB84_18
	NOP
	bslli	r9,	r3,	2
	bslli	r10,	r4,	2
	LW	r11,	r5,	r10
	LW	r12,	r5,	r9
	SW	r12,	r5,	r10
	SW	r11,	r5,	r9
	ADD	r11,	r5,	r9
	ADD	r12,	r5,	r10
	LWI	r20,	r12,	12
	LWI	r21,	r11,	12
	SWI	r21,	r12,	12
	SWI	r20,	r11,	12
	LWI	r20,	r12,	24
	LWI	r21,	r11,	24
	SWI	r21,	r12,	24
	SWI	r20,	r11,	24
	LWI	r11,	r7,	0
	RSUBI	r11,	r11,	0
	SWI	r11,	r7,	0
	ADDI	r11,	r1,	12
	LW	r9,	r11,	r9
	SW	r9,	r11,	r10
$0BB84_18:
	bslli	r9,	r3,	2
	SW	r4,	r6,	r9
	ADDI	r9,	r0,	2
	CMP	r9,	r9,	r3
	beqid	r9,	$0BB84_27
	NOP
	ADD	r9,	r8,	r3
	bslli	r10,	r9,	2
	LW	r9,	r5,	r10
	ORI	r11,	r0,	0
	FPNE	r12,	r9,	r11
	bneid	r12,	$0BB84_21
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB84_21:
	bneid	r11,	$0BB84_23
	NOP
	ADDI	r9,	r0,	786163455
	SW	r9,	r5,	r10
	ORI	r9,	r0,	786163455
$0BB84_23:
	ORI	r10,	r0,	1065353216
	ADDI	r3,	r3,	1
	ADDI	r11,	r0,	2
	CMP	r11,	r11,	r3
	bgtid	r11,	$0BB84_27
	NOP
	FPDIV	r9,	r10,	r9
	ADD	r10,	r3,	r0
$0BB84_25:
	ADD	r11,	r10,	r8
	bslli	r11,	r11,	2
	LW	r12,	r5,	r11
	FPMUL	r12,	r12,	r9
	SW	r12,	r5,	r11
	ADDI	r10,	r10,	1
	ADDI	r11,	r0,	3
	CMP	r12,	r11,	r10
	bltid	r12,	$0BB84_25
	NOP
	CMP	r8,	r11,	r3
	bltid	r8,	$0BB84_3
	NOP
$0BB84_27:
	LWI	r3,	r5,	32
	ORI	r4,	r0,	0
	FPNE	r6,	r3,	r4
	ADDI	r3,	r0,	1
	bneid	r6,	$0BB84_29
	ADD	r4,	r3,	r0
	ADDI	r4,	r0,	0
$0BB84_29:
	bneid	r4,	$0BB84_31
	NOP
	ADDI	r3,	r0,	786163455
	SWI	r3,	r5,	32
	ADDI	r3,	r0,	1
$0BB84_31:
	LWI	r22,	r1,	0
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	24
#	.end	_ZN8syMatrix3LUDEPiRi
$0tmp84:
#	.size	_ZN8syMatrix3LUDEPiRi, ($tmp84)-_ZN8syMatrix3LUDEPiRi

#	.globl	_ZN8syMatrix5LUBKSEPKiPf
#	.align	2
#	.type	_ZN8syMatrix5LUBKSEPKiPf,@function
#	.ent	_ZN8syMatrix5LUBKSEPKiPf # @_ZN8syMatrix5LUBKSEPKiPf
_ZN8syMatrix5LUBKSEPKiPf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	bslli	r4,	r3,	2
	LW	r3,	r7,	r4
	LWI	r8,	r7,	0
	SW	r8,	r7,	r4
	SWI	r3,	r7,	0
	LWI	r4,	r6,	4
	bslli	r8,	r4,	2
	LW	r4,	r7,	r8
	LWI	r9,	r7,	4
	SW	r9,	r7,	r8
	ORI	r8,	r0,	0
	FPEQ	r3,	r3,	r8
	bneid	r3,	$0BB85_2
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB85_2:
	bneid	r8,	$0BB85_4
	ADDI	r3,	r0,	-1
	ADD	r3,	r0,	r0
$0BB85_4:
	beqid	r8,	$0BB85_5
	NOP
	ORI	r3,	r0,	0
	FPEQ	r3,	r4,	r3
	bneid	r3,	$0BB85_9
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB85_9:
	bneid	r8,	$0BB85_11
	ADDI	r3,	r0,	-1
	brid	$0BB85_11
	ADDI	r3,	r0,	1
$0BB85_5:
	ADD	r9,	r3,	r0
$0BB85_6:
	ADD	r8,	r9,	r0
	bslli	r9,	r8,	2
	LW	r9,	r7,	r9
	MULI	r10,	r8,	3
	bslli	r10,	r10,	2
	ADD	r10,	r10,	r5
	LWI	r10,	r10,	4
	FPMUL	r9,	r10,	r9
	FPRSUB	r4,	r9,	r4
	ADDI	r9,	r8,	1
	bltid	r8,	$0BB85_6
	NOP
$0BB85_11:
	SWI	r4,	r7,	4
	LWI	r4,	r6,	8
	bslli	r6,	r4,	2
	LW	r4,	r7,	r6
	LWI	r8,	r7,	8
	SW	r8,	r7,	r6
	ADDI	r6,	r0,	-1
	CMP	r6,	r6,	r3
	beqid	r6,	$0BB85_13
	NOP
$0BB85_12:
	bslli	r6,	r3,	2
	LW	r6,	r7,	r6
	MULI	r8,	r3,	3
	bslli	r8,	r8,	2
	ADD	r8,	r8,	r5
	LWI	r8,	r8,	8
	FPMUL	r6,	r8,	r6
	FPRSUB	r4,	r6,	r4
	ADDI	r3,	r3,	1
	ADDI	r6,	r0,	2
	CMP	r6,	r6,	r3
	bltid	r6,	$0BB85_12
	NOP
$0BB85_13:
	SWI	r4,	r7,	8
	LWI	r3,	r5,	32
	FPDIV	r3,	r4,	r3
	SWI	r3,	r7,	8
	LWI	r4,	r5,	28
	FPMUL	r4,	r4,	r3
	LWI	r6,	r7,	4
	FPRSUB	r4,	r4,	r6
	LWI	r6,	r5,	16
	FPDIV	r4,	r4,	r6
	SWI	r4,	r7,	4
	LWI	r6,	r5,	24
	FPMUL	r3,	r6,	r3
	LWI	r6,	r5,	12
	FPMUL	r4,	r6,	r4
	LWI	r6,	r7,	0
	FPRSUB	r4,	r4,	r6
	FPRSUB	r3,	r3,	r4
	LWI	r4,	r5,	0
	FPDIV	r3,	r3,	r4
	rtsd	r15,	8
	SWI	r3,	r7,	0
#	.end	_ZN8syMatrix5LUBKSEPKiPf
$0tmp85:
#	.size	_ZN8syMatrix5LUBKSEPKiPf, ($tmp85)-_ZN8syMatrix5LUBKSEPKiPf

#	.globl	_ZNK8syMatrix10GetInverseERS_
#	.align	2
#	.type	_ZNK8syMatrix10GetInverseERS_,@function
#	.ent	_ZNK8syMatrix10GetInverseERS_ # @_ZNK8syMatrix10GetInverseERS_
_ZNK8syMatrix10GetInverseERS_:
#	.frame	r1,116,r15
#	.mask	0x1ff08000
	ADDI	r1,	r1,	-116
	SWI	r15,	r1,	0
	SWI	r20,	r1,	36
	SWI	r21,	r1,	32
	SWI	r22,	r1,	28
	SWI	r23,	r1,	24
	SWI	r24,	r1,	20
	SWI	r25,	r1,	16
	SWI	r26,	r1,	12
	SWI	r27,	r1,	8
	SWI	r28,	r1,	4
	ADD	r20,	r6,	r0
	LWI	r3,	r5,	0
	SWI	r3,	r20,	0
	LWI	r3,	r5,	4
	SWI	r3,	r20,	4
	LWI	r3,	r5,	8
	SWI	r3,	r20,	8
	LWI	r3,	r5,	12
	SWI	r3,	r20,	12
	LWI	r3,	r5,	16
	SWI	r3,	r20,	16
	LWI	r3,	r5,	20
	SWI	r3,	r20,	20
	LWI	r3,	r5,	24
	SWI	r3,	r20,	24
	LWI	r3,	r5,	28
	SWI	r3,	r20,	28
	LWI	r3,	r5,	32
	SWI	r3,	r20,	32
	ADDI	r5,	r1,	80
	brlid	r15,	memcpy
	ADDI	r7,	r0,	36
	ADD	r5,	r0,	r0
	ADDI	r3,	r20,	24
	brid	$0BB86_1
	ADDI	r4,	r20,	12
$0BB86_97:
	ORI	r8,	r0,	1065353216
	FPDIV	r7,	r8,	r7
	ADDI	r8,	r1,	40
	SW	r7,	r8,	r6
	ADDI	r5,	r5,	1
$0BB86_1:
	ADDI	r6,	r0,	2
	CMP	r6,	r6,	r5
	bgtid	r6,	$0BB86_2
	NOP
	bslli	r6,	r5,	2
	ADDI	r7,	r1,	80
	LW	r8,	r7,	r6
	ORI	r7,	r0,	0
	FPLT	r9,	r8,	r7
	bneid	r9,	$0BB86_28
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB86_28:
	beqid	r7,	$0BB86_29
	NOP
	ORI	r7,	r0,	-2147483648
	FPGE	r9,	r8,	r7
	FPUN	r7,	r8,	r7
	BITOR	r7,	r7,	r9
	bneid	r7,	$0BB86_35
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_35:
	ORI	r7,	r0,	0
	bneid	r9,	$0BB86_37
	NOP
	ADD	r7,	r8,	r0
$0BB86_37:
	bneid	r9,	$0BB86_39
	NOP
	FPNEG	r7,	r8
	brid	$0BB86_39
	NOP
$0BB86_29:
	ORI	r7,	r0,	0
	FPLE	r9,	r8,	r7
	FPUN	r10,	r8,	r7
	BITOR	r10,	r10,	r9
	bneid	r10,	$0BB86_31
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_31:
	bneid	r9,	$0BB86_39
	NOP
	ADD	r7,	r8,	r0
$0BB86_39:
	ADDI	r8,	r1,	80
	ADD	r8,	r6,	r8
	LWI	r8,	r8,	12
	ORI	r9,	r0,	0
	FPLT	r10,	r8,	r9
	bneid	r10,	$0BB86_41
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_41:
	beqid	r9,	$0BB86_42
	NOP
	FPNEG	r9,	r8
	FPGE	r10,	r7,	r9
	FPUN	r11,	r7,	r9
	BITOR	r11,	r11,	r10
	bneid	r11,	$0BB86_77
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB86_77:
	bneid	r10,	$0BB86_79
	NOP
	ADD	r7,	r8,	r0
$0BB86_79:
	bneid	r10,	$0BB86_81
	NOP
	brid	$0BB86_81
	ADD	r7,	r9,	r0
$0BB86_42:
	FPLE	r9,	r8,	r7
	FPUN	r10,	r8,	r7
	BITOR	r10,	r10,	r9
	bneid	r10,	$0BB86_44
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_44:
	bneid	r9,	$0BB86_81
	NOP
	ADD	r7,	r8,	r0
$0BB86_81:
	ADDI	r8,	r1,	80
	ADD	r8,	r6,	r8
	LWI	r8,	r8,	24
	ORI	r9,	r0,	0
	FPLT	r10,	r8,	r9
	bneid	r10,	$0BB86_83
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_83:
	beqid	r9,	$0BB86_84
	NOP
	FPNEG	r9,	r8
	FPGE	r10,	r7,	r9
	FPUN	r11,	r7,	r9
	BITOR	r11,	r11,	r10
	bneid	r11,	$0BB86_90
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB86_90:
	bneid	r10,	$0BB86_92
	NOP
	ADD	r7,	r8,	r0
$0BB86_92:
	bneid	r10,	$0BB86_94
	NOP
	brid	$0BB86_94
	ADD	r7,	r9,	r0
$0BB86_84:
	FPLE	r9,	r8,	r7
	FPUN	r10,	r8,	r7
	BITOR	r10,	r10,	r9
	bneid	r10,	$0BB86_86
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB86_86:
	bneid	r9,	$0BB86_94
	NOP
	ADD	r7,	r8,	r0
$0BB86_94:
	ORI	r8,	r0,	0
	FPEQ	r9,	r7,	r8
	bneid	r9,	$0BB86_96
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB86_96:
	bneid	r8,	$0BB86_133
	NOP
	brid	$0BB86_97
	NOP
$0BB86_2:
	brid	$0BB86_3
	ADD	r5,	r0,	r0
$0BB86_19:
	ADD	r8,	r7,	r5
	bslli	r9,	r8,	2
	ADDI	r8,	r1,	80
	LW	r8,	r8,	r9
	ORI	r10,	r0,	0
	FPNE	r11,	r8,	r10
	bneid	r11,	$0BB86_21
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB86_21:
	bneid	r10,	$0BB86_23
	NOP
	ADDI	r8,	r1,	80
	ADDI	r10,	r0,	786163455
	SW	r10,	r8,	r9
	ORI	r8,	r0,	786163455
$0BB86_23:
	ORI	r9,	r0,	1065353216
	ADDI	r5,	r5,	1
	ADDI	r10,	r0,	2
	CMP	r10,	r10,	r5
	bgtid	r10,	$0BB86_64
	NOP
	FPDIV	r8,	r9,	r8
	ADD	r9,	r5,	r0
$0BB86_25:
	ADD	r10,	r9,	r7
	bslli	r10,	r10,	2
	ADDI	r11,	r1,	80
	LW	r12,	r11,	r10
	FPMUL	r12,	r12,	r8
	SW	r12,	r11,	r10
	ADDI	r9,	r9,	1
	ADDI	r10,	r0,	3
	CMP	r10,	r10,	r9
	bltid	r10,	$0BB86_25
	NOP
$0BB86_3:
	ORI	r8,	r0,	0
	ADDI	r7,	r0,	1
	CMP	r9,	r7,	r5
	bltid	r9,	$0BB86_4
	MULI	r7,	r5,	3
	ADD	r9,	r0,	r0
$0BB86_47:
	ADDI	r8,	r0,	1
	CMP	r8,	r8,	r9
	bltid	r8,	$0BB86_49
	NOP
	ADD	r10,	r9,	r7
	bslli	r11,	r7,	2
	ADDI	r8,	r1,	80
	LW	r11,	r8,	r11
	bslli	r12,	r9,	2
	LW	r12,	r8,	r12
	FPMUL	r11,	r12,	r11
	bslli	r10,	r10,	2
	LW	r12,	r8,	r10
	FPRSUB	r11,	r11,	r12
	SW	r11,	r8,	r10
$0BB86_49:
	ORI	r8,	r0,	0
	ADDI	r9,	r9,	1
	CMP	r10,	r5,	r9
	bltid	r10,	$0BB86_47
	NOP
	ADD	r9,	r5,	r0
$0BB86_59:
	ADD	r10,	r9,	r7
	bslli	r11,	r10,	2
	ADDI	r12,	r1,	80
	LW	r10,	r12,	r11
	ADD	r21,	r0,	r0
$0BB86_60:
	MULI	r22,	r21,	3
	ADD	r22,	r22,	r9
	ADD	r23,	r21,	r7
	bslli	r23,	r23,	2
	LW	r23,	r12,	r23
	bslli	r22,	r22,	2
	LW	r22,	r12,	r22
	FPMUL	r22,	r22,	r23
	FPRSUB	r10,	r22,	r10
	ADDI	r21,	r21,	1
	CMP	r22,	r5,	r21
	bltid	r22,	$0BB86_60
	NOP
	ADDI	r12,	r1,	80
	SW	r10,	r12,	r11
	ORI	r11,	r0,	0
	FPLT	r11,	r10,	r11
	bneid	r11,	$0BB86_63
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB86_63:
	bslli	r11,	r9,	2
	ADDI	r21,	r1,	40
	beqid	r12,	$0BB86_52
	LW	r11,	r21,	r11
	FPNEG	r10,	r10
$0BB86_52:
	FPMUL	r10,	r11,	r10
	FPLT	r11,	r10,	r8
	FPUN	r12,	r10,	r8
	BITOR	r12,	r12,	r11
	bneid	r12,	$0BB86_54
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB86_54:
	bneid	r11,	$0BB86_56
	NOP
	ADD	r8,	r10,	r0
$0BB86_56:
	bneid	r11,	$0BB86_58
	NOP
	ADD	r6,	r9,	r0
$0BB86_58:
	ADDI	r9,	r9,	1
	ADDI	r10,	r0,	3
	CMP	r10,	r10,	r9
	bgeid	r10,	$0BB86_16
	NOP
	brid	$0BB86_59
	NOP
$0BB86_4:
	ADD	r9,	r5,	r0
$0BB86_5:
	ADD	r10,	r9,	r7
	bslli	r10,	r10,	2
	ADDI	r11,	r1,	80
	LW	r10,	r11,	r10
	ORI	r11,	r0,	0
	FPGE	r12,	r10,	r11
	FPUN	r11,	r10,	r11
	BITOR	r11,	r11,	r12
	bneid	r11,	$0BB86_7
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB86_7:
	bslli	r11,	r9,	2
	ADDI	r21,	r1,	40
	bneid	r12,	$0BB86_9
	LW	r11,	r21,	r11
	FPNEG	r10,	r10
$0BB86_9:
	FPMUL	r10,	r11,	r10
	FPLT	r11,	r10,	r8
	FPUN	r12,	r10,	r8
	BITOR	r12,	r12,	r11
	bneid	r12,	$0BB86_11
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB86_11:
	bneid	r11,	$0BB86_13
	NOP
	ADD	r8,	r10,	r0
$0BB86_13:
	bneid	r11,	$0BB86_15
	NOP
	ADD	r6,	r9,	r0
$0BB86_15:
	ADDI	r9,	r9,	1
	ADDI	r10,	r0,	3
	CMP	r10,	r10,	r9
	bltid	r10,	$0BB86_5
	NOP
$0BB86_16:
	CMP	r8,	r6,	r5
	beqid	r8,	$0BB86_18
	NOP
	bslli	r8,	r5,	2
	bslli	r9,	r6,	2
	ADDI	r11,	r1,	80
	LW	r10,	r11,	r9
	LW	r12,	r11,	r8
	SW	r12,	r11,	r9
	SW	r10,	r11,	r8
	ADD	r10,	r11,	r8
	ADD	r11,	r11,	r9
	LWI	r12,	r11,	12
	LWI	r21,	r10,	12
	SWI	r21,	r11,	12
	SWI	r12,	r10,	12
	LWI	r12,	r11,	24
	LWI	r21,	r10,	24
	SWI	r21,	r11,	24
	SWI	r12,	r10,	24
	ADDI	r10,	r1,	40
	LW	r8,	r10,	r8
	SW	r8,	r10,	r9
$0BB86_18:
	bslli	r8,	r5,	2
	ADDI	r9,	r1,	68
	SW	r6,	r9,	r8
	ADDI	r8,	r0,	2
	CMP	r8,	r8,	r5
	bneid	r8,	$0BB86_19
	NOP
$0BB86_64:
	ORI	r6,	r0,	0
	LWI	r5,	r1,	112
	FPNE	r7,	r5,	r6
	bneid	r7,	$0BB86_66
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB86_66:
	bneid	r6,	$0BB86_68
	NOP
	ADDI	r5,	r0,	786163455
	SWI	r5,	r1,	112
	ORI	r5,	r0,	786163455
$0BB86_68:
	SWI	r0,	r1,	64
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r7,	r0,	1065353216
	SWI	r7,	r1,	56
	LWI	r6,	r1,	68
	bslli	r6,	r6,	2
	ADDI	r10,	r1,	56
	LW	r8,	r10,	r6
	SW	r7,	r10,	r6
	SWI	r8,	r1,	56
	LWI	r7,	r1,	72
	bslli	r7,	r7,	2
	LW	r9,	r10,	r7
	LWI	r11,	r1,	60
	SW	r11,	r10,	r7
	ORI	r10,	r0,	0
	FPEQ	r8,	r8,	r10
	bneid	r8,	$0BB86_70
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB86_70:
	bneid	r10,	$0BB86_72
	ADDI	r8,	r0,	-1
	ADD	r8,	r0,	r0
$0BB86_72:
	beqid	r10,	$0BB86_73
	NOP
	ORI	r8,	r0,	0
	FPEQ	r8,	r9,	r8
	bneid	r8,	$0BB86_100
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB86_100:
	bneid	r10,	$0BB86_102
	ADDI	r8,	r0,	-1
	brid	$0BB86_102
	ADDI	r8,	r0,	1
$0BB86_73:
	ADD	r11,	r8,	r0
$0BB86_74:
	ADD	r10,	r11,	r0
	bslli	r11,	r10,	2
	ADDI	r12,	r1,	56
	LW	r11,	r12,	r11
	MULI	r12,	r10,	3
	bslli	r12,	r12,	2
	ADDI	r21,	r1,	80
	ADD	r12,	r12,	r21
	LWI	r12,	r12,	4
	FPMUL	r11,	r12,	r11
	FPRSUB	r9,	r11,	r9
	ADDI	r11,	r10,	1
	bltid	r10,	$0BB86_74
	NOP
$0BB86_102:
	SWI	r9,	r1,	60
	LWI	r9,	r1,	76
	bslli	r21,	r9,	2
	ADDI	r22,	r1,	56
	LW	r9,	r22,	r21
	LWI	r10,	r1,	64
	SW	r10,	r22,	r21
	ADDI	r10,	r0,	-1
	CMP	r10,	r10,	r8
	beqid	r10,	$0BB86_104
	NOP
$0BB86_103:
	bslli	r10,	r8,	2
	ADDI	r11,	r1,	56
	LW	r10,	r11,	r10
	MULI	r11,	r8,	3
	bslli	r11,	r11,	2
	ADDI	r12,	r1,	80
	ADD	r11,	r11,	r12
	LWI	r11,	r11,	8
	FPMUL	r10,	r11,	r10
	FPRSUB	r9,	r10,	r9
	ADDI	r8,	r8,	1
	ADDI	r10,	r0,	2
	CMP	r10,	r10,	r8
	bltid	r10,	$0BB86_103
	NOP
$0BB86_104:
	FPDIV	r11,	r9,	r5
	SWI	r11,	r1,	64
	LWI	r8,	r1,	108
	FPMUL	r9,	r8,	r11
	LWI	r10,	r1,	60
	FPRSUB	r10,	r9,	r10
	LWI	r9,	r1,	96
	FPDIV	r12,	r10,	r9
	SWI	r12,	r1,	60
	LWI	r10,	r1,	92
	FPMUL	r12,	r10,	r12
	LWI	r23,	r1,	56
	FPRSUB	r23,	r12,	r23
	LWI	r12,	r1,	104
	FPMUL	r11,	r12,	r11
	FPRSUB	r23,	r11,	r23
	LWI	r11,	r1,	80
	FPDIV	r23,	r23,	r11
	SWI	r23,	r1,	56
	LWI	r23,	r1,	64
	SWI	r23,	r20,	8
	LWI	r23,	r1,	60
	SWI	r23,	r20,	4
	LWI	r23,	r1,	56
	SWI	r23,	r20,	0
	SWI	r0,	r1,	64
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r20,	r0,	1065353216
	SWI	r20,	r1,	60
	ADD	r25,	r0,	r0
	LW	r23,	r22,	r6
	SW	r25,	r22,	r6
	SWI	r23,	r1,	56
	LW	r24,	r22,	r7
	LWI	r20,	r1,	60
	SW	r20,	r22,	r7
	ORI	r20,	r0,	0
	FPEQ	r20,	r23,	r20
	bneid	r20,	$0BB86_106
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB86_106:
	bneid	r22,	$0BB86_108
	ADDI	r20,	r0,	-1
	ADD	r20,	r25,	r0
$0BB86_108:
	beqid	r22,	$0BB86_109
	NOP
	SWI	r24,	r1,	60
	ADDI	r20,	r1,	56
	LW	r22,	r20,	r21
	LWI	r23,	r1,	64
	SW	r23,	r20,	r21
	ORI	r20,	r0,	0
	FPNE	r24,	r24,	r20
	ADDI	r20,	r0,	1
	bneid	r24,	$0BB86_116
	ADD	r23,	r20,	r0
	ADDI	r23,	r0,	0
$0BB86_116:
	beqid	r23,	$0BB86_118
	NOP
	brid	$0BB86_117
	NOP
$0BB86_109:
	ADD	r26,	r20,	r0
$0BB86_110:
	ADD	r22,	r26,	r0
	bslli	r26,	r22,	2
	ADDI	r25,	r1,	56
	LW	r26,	r25,	r26
	MULI	r27,	r22,	3
	bslli	r27,	r27,	2
	ADDI	r28,	r1,	80
	ADD	r27,	r27,	r28
	LWI	r27,	r27,	4
	FPMUL	r26,	r27,	r26
	FPRSUB	r24,	r26,	r24
	ADDI	r26,	r22,	1
	bltid	r22,	$0BB86_110
	NOP
	SWI	r24,	r1,	60
	LW	r22,	r25,	r21
	LWI	r24,	r1,	64
	SW	r24,	r25,	r21
	ORI	r24,	r0,	0
	FPEQ	r24,	r23,	r24
	bneid	r24,	$0BB86_113
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB86_113:
	bneid	r23,	$0BB86_118
	NOP
$0BB86_117:
	bslli	r23,	r20,	2
	ADDI	r24,	r1,	56
	LW	r23,	r24,	r23
	MULI	r24,	r20,	3
	bslli	r24,	r24,	2
	ADDI	r25,	r1,	80
	ADD	r24,	r24,	r25
	LWI	r24,	r24,	8
	FPMUL	r23,	r24,	r23
	FPRSUB	r22,	r23,	r22
	ADDI	r20,	r20,	1
	ADDI	r23,	r0,	2
	CMP	r23,	r23,	r20
	bltid	r23,	$0BB86_117
	NOP
$0BB86_118:
	FPDIV	r20,	r22,	r5
	SWI	r20,	r1,	64
	FPMUL	r22,	r8,	r20
	FPMUL	r20,	r12,	r20
	LWI	r23,	r1,	60
	FPRSUB	r22,	r22,	r23
	FPDIV	r22,	r22,	r9
	SWI	r22,	r1,	60
	FPMUL	r22,	r10,	r22
	LWI	r23,	r1,	56
	FPRSUB	r22,	r22,	r23
	FPRSUB	r20,	r20,	r22
	FPDIV	r20,	r20,	r11
	SWI	r20,	r1,	56
	LWI	r20,	r1,	64
	SWI	r20,	r4,	8
	LWI	r20,	r1,	60
	SWI	r20,	r4,	4
	LWI	r20,	r1,	56
	SWI	r20,	r4,	0
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r4,	r0,	1065353216
	SWI	r4,	r1,	64
	ADD	r23,	r0,	r0
	ADDI	r4,	r1,	56
	LW	r20,	r4,	r6
	SW	r23,	r4,	r6
	SWI	r20,	r1,	56
	LW	r22,	r4,	r7
	LWI	r6,	r1,	60
	SW	r6,	r4,	r7
	ORI	r4,	r0,	0
	FPEQ	r4,	r20,	r4
	bneid	r4,	$0BB86_120
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB86_120:
	bneid	r6,	$0BB86_122
	ADDI	r4,	r0,	-1
	ADD	r4,	r23,	r0
$0BB86_122:
	beqid	r6,	$0BB86_123
	NOP
	SWI	r22,	r1,	60
	ADDI	r4,	r1,	56
	LW	r6,	r4,	r21
	LWI	r7,	r1,	64
	SW	r7,	r4,	r21
	ORI	r4,	r0,	0
	FPNE	r20,	r22,	r4
	ADDI	r4,	r0,	1
	bneid	r20,	$0BB86_130
	ADD	r7,	r4,	r0
	ADDI	r7,	r0,	0
$0BB86_130:
	beqid	r7,	$0BB86_132
	NOP
	brid	$0BB86_131
	NOP
$0BB86_123:
	ADD	r23,	r4,	r0
$0BB86_124:
	ADD	r6,	r23,	r0
	bslli	r23,	r6,	2
	ADDI	r7,	r1,	56
	LW	r23,	r7,	r23
	MULI	r24,	r6,	3
	bslli	r24,	r24,	2
	ADDI	r25,	r1,	80
	ADD	r24,	r24,	r25
	LWI	r24,	r24,	4
	FPMUL	r23,	r24,	r23
	FPRSUB	r22,	r23,	r22
	ADDI	r23,	r6,	1
	bltid	r6,	$0BB86_124
	NOP
	SWI	r22,	r1,	60
	LW	r6,	r7,	r21
	LWI	r22,	r1,	64
	SW	r22,	r7,	r21
	ORI	r7,	r0,	0
	FPEQ	r20,	r20,	r7
	bneid	r20,	$0BB86_127
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB86_127:
	bneid	r7,	$0BB86_132
	NOP
$0BB86_131:
	bslli	r7,	r4,	2
	ADDI	r20,	r1,	56
	LW	r7,	r20,	r7
	MULI	r20,	r4,	3
	bslli	r20,	r20,	2
	ADDI	r21,	r1,	80
	ADD	r20,	r20,	r21
	LWI	r20,	r20,	8
	FPMUL	r7,	r20,	r7
	FPRSUB	r6,	r7,	r6
	ADDI	r4,	r4,	1
	ADDI	r7,	r0,	2
	CMP	r7,	r7,	r4
	bltid	r7,	$0BB86_131
	NOP
$0BB86_132:
	FPDIV	r4,	r6,	r5
	SWI	r4,	r1,	64
	FPMUL	r5,	r8,	r4
	FPMUL	r4,	r12,	r4
	LWI	r6,	r1,	60
	FPRSUB	r5,	r5,	r6
	FPDIV	r5,	r5,	r9
	SWI	r5,	r1,	60
	FPMUL	r5,	r10,	r5
	LWI	r6,	r1,	56
	FPRSUB	r5,	r5,	r6
	FPRSUB	r4,	r4,	r5
	FPDIV	r4,	r4,	r11
	SWI	r4,	r1,	56
	LWI	r4,	r1,	64
	SWI	r4,	r3,	8
	LWI	r4,	r1,	60
	SWI	r4,	r3,	4
	LWI	r4,	r1,	56
	SWI	r4,	r3,	0
$0BB86_133:
	LWI	r28,	r1,	4
	LWI	r27,	r1,	8
	LWI	r26,	r1,	12
	LWI	r25,	r1,	16
	LWI	r24,	r1,	20
	LWI	r23,	r1,	24
	LWI	r22,	r1,	28
	LWI	r21,	r1,	32
	LWI	r20,	r1,	36
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	116
#	.end	_ZNK8syMatrix10GetInverseERS_
$0tmp86:
#	.size	_ZNK8syMatrix10GetInverseERS_, ($tmp86)-_ZNK8syMatrix10GetInverseERS_

#	.globl	_ZN8syMatrix6InvertEv
#	.align	2
#	.type	_ZN8syMatrix6InvertEv,@function
#	.ent	_ZN8syMatrix6InvertEv   # @_ZN8syMatrix6InvertEv
_ZN8syMatrix6InvertEv:
#	.frame	r1,116,r15
#	.mask	0x1ff08000
	ADDI	r1,	r1,	-116
	SWI	r15,	r1,	0
	SWI	r20,	r1,	36
	SWI	r21,	r1,	32
	SWI	r22,	r1,	28
	SWI	r23,	r1,	24
	SWI	r24,	r1,	20
	SWI	r25,	r1,	16
	SWI	r26,	r1,	12
	SWI	r27,	r1,	8
	SWI	r28,	r1,	4
	ADD	r20,	r5,	r0
	ADDI	r5,	r1,	80
	ADDI	r7,	r0,	36
	brlid	r15,	memcpy
	ADD	r6,	r20,	r0
	brid	$0BB87_1
	ADD	r3,	r0,	r0
$0BB87_97:
	ORI	r6,	r0,	1065353216
	FPDIV	r5,	r6,	r5
	ADDI	r6,	r1,	40
	SW	r5,	r6,	r4
	ADDI	r3,	r3,	1
$0BB87_1:
	ADDI	r4,	r0,	2
	CMP	r4,	r4,	r3
	bgtid	r4,	$0BB87_2
	NOP
	bslli	r4,	r3,	2
	ADDI	r5,	r1,	80
	LW	r6,	r5,	r4
	ORI	r5,	r0,	0
	FPLT	r7,	r6,	r5
	bneid	r7,	$0BB87_28
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB87_28:
	beqid	r5,	$0BB87_29
	NOP
	ORI	r5,	r0,	-2147483648
	FPGE	r7,	r6,	r5
	FPUN	r5,	r6,	r5
	BITOR	r5,	r5,	r7
	bneid	r5,	$0BB87_35
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_35:
	ORI	r5,	r0,	0
	bneid	r7,	$0BB87_37
	NOP
	ADD	r5,	r6,	r0
$0BB87_37:
	bneid	r7,	$0BB87_39
	NOP
	FPNEG	r5,	r6
	brid	$0BB87_39
	NOP
$0BB87_29:
	ORI	r5,	r0,	0
	FPLE	r7,	r6,	r5
	FPUN	r8,	r6,	r5
	BITOR	r8,	r8,	r7
	bneid	r8,	$0BB87_31
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_31:
	bneid	r7,	$0BB87_39
	NOP
	ADD	r5,	r6,	r0
$0BB87_39:
	ADDI	r6,	r1,	80
	ADD	r6,	r4,	r6
	LWI	r6,	r6,	12
	ORI	r7,	r0,	0
	FPLT	r8,	r6,	r7
	bneid	r8,	$0BB87_41
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_41:
	beqid	r7,	$0BB87_42
	NOP
	FPNEG	r7,	r6
	FPGE	r8,	r5,	r7
	FPUN	r9,	r5,	r7
	BITOR	r9,	r9,	r8
	bneid	r9,	$0BB87_77
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB87_77:
	bneid	r8,	$0BB87_79
	NOP
	ADD	r5,	r6,	r0
$0BB87_79:
	bneid	r8,	$0BB87_81
	NOP
	brid	$0BB87_81
	ADD	r5,	r7,	r0
$0BB87_42:
	FPLE	r7,	r6,	r5
	FPUN	r8,	r6,	r5
	BITOR	r8,	r8,	r7
	bneid	r8,	$0BB87_44
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_44:
	bneid	r7,	$0BB87_81
	NOP
	ADD	r5,	r6,	r0
$0BB87_81:
	ADDI	r6,	r1,	80
	ADD	r6,	r4,	r6
	LWI	r6,	r6,	24
	ORI	r7,	r0,	0
	FPLT	r8,	r6,	r7
	bneid	r8,	$0BB87_83
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_83:
	beqid	r7,	$0BB87_84
	NOP
	FPNEG	r7,	r6
	FPGE	r8,	r5,	r7
	FPUN	r9,	r5,	r7
	BITOR	r9,	r9,	r8
	bneid	r9,	$0BB87_90
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB87_90:
	bneid	r8,	$0BB87_92
	NOP
	ADD	r5,	r6,	r0
$0BB87_92:
	bneid	r8,	$0BB87_94
	NOP
	brid	$0BB87_94
	ADD	r5,	r7,	r0
$0BB87_84:
	FPLE	r7,	r6,	r5
	FPUN	r8,	r6,	r5
	BITOR	r8,	r8,	r7
	bneid	r8,	$0BB87_86
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB87_86:
	bneid	r7,	$0BB87_94
	NOP
	ADD	r5,	r6,	r0
$0BB87_94:
	ORI	r6,	r0,	0
	FPEQ	r7,	r5,	r6
	bneid	r7,	$0BB87_96
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB87_96:
	bneid	r6,	$0BB87_133
	NOP
	brid	$0BB87_97
	NOP
$0BB87_2:
	brid	$0BB87_3
	ADD	r3,	r0,	r0
$0BB87_19:
	ADD	r6,	r5,	r3
	bslli	r7,	r6,	2
	ADDI	r6,	r1,	80
	LW	r6,	r6,	r7
	ORI	r8,	r0,	0
	FPNE	r9,	r6,	r8
	bneid	r9,	$0BB87_21
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB87_21:
	bneid	r8,	$0BB87_23
	NOP
	ADDI	r6,	r1,	80
	ADDI	r8,	r0,	786163455
	SW	r8,	r6,	r7
	ORI	r6,	r0,	786163455
$0BB87_23:
	ORI	r7,	r0,	1065353216
	ADDI	r3,	r3,	1
	ADDI	r8,	r0,	2
	CMP	r8,	r8,	r3
	bgtid	r8,	$0BB87_64
	NOP
	FPDIV	r6,	r7,	r6
	ADD	r7,	r3,	r0
$0BB87_25:
	ADD	r8,	r7,	r5
	bslli	r8,	r8,	2
	ADDI	r9,	r1,	80
	LW	r10,	r9,	r8
	FPMUL	r10,	r10,	r6
	SW	r10,	r9,	r8
	ADDI	r7,	r7,	1
	ADDI	r8,	r0,	3
	CMP	r8,	r8,	r7
	bltid	r8,	$0BB87_25
	NOP
$0BB87_3:
	ORI	r6,	r0,	0
	ADDI	r5,	r0,	1
	CMP	r7,	r5,	r3
	bltid	r7,	$0BB87_4
	MULI	r5,	r3,	3
	ADD	r7,	r0,	r0
$0BB87_47:
	ADDI	r6,	r0,	1
	CMP	r6,	r6,	r7
	bltid	r6,	$0BB87_49
	NOP
	ADD	r8,	r7,	r5
	bslli	r9,	r5,	2
	ADDI	r6,	r1,	80
	LW	r9,	r6,	r9
	bslli	r10,	r7,	2
	LW	r10,	r6,	r10
	FPMUL	r9,	r10,	r9
	bslli	r8,	r8,	2
	LW	r10,	r6,	r8
	FPRSUB	r9,	r9,	r10
	SW	r9,	r6,	r8
$0BB87_49:
	ORI	r6,	r0,	0
	ADDI	r7,	r7,	1
	CMP	r8,	r3,	r7
	bltid	r8,	$0BB87_47
	NOP
	ADD	r7,	r3,	r0
$0BB87_59:
	ADD	r8,	r7,	r5
	bslli	r9,	r8,	2
	ADDI	r10,	r1,	80
	LW	r8,	r10,	r9
	ADD	r11,	r0,	r0
$0BB87_60:
	MULI	r12,	r11,	3
	ADD	r12,	r12,	r7
	ADD	r21,	r11,	r5
	bslli	r21,	r21,	2
	LW	r21,	r10,	r21
	bslli	r12,	r12,	2
	LW	r12,	r10,	r12
	FPMUL	r12,	r12,	r21
	FPRSUB	r8,	r12,	r8
	ADDI	r11,	r11,	1
	CMP	r12,	r3,	r11
	bltid	r12,	$0BB87_60
	NOP
	ADDI	r10,	r1,	80
	SW	r8,	r10,	r9
	ORI	r9,	r0,	0
	FPLT	r9,	r8,	r9
	bneid	r9,	$0BB87_63
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB87_63:
	bslli	r9,	r7,	2
	ADDI	r11,	r1,	40
	beqid	r10,	$0BB87_52
	LW	r9,	r11,	r9
	FPNEG	r8,	r8
$0BB87_52:
	FPMUL	r8,	r9,	r8
	FPLT	r9,	r8,	r6
	FPUN	r10,	r8,	r6
	BITOR	r10,	r10,	r9
	bneid	r10,	$0BB87_54
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB87_54:
	bneid	r9,	$0BB87_56
	NOP
	ADD	r6,	r8,	r0
$0BB87_56:
	bneid	r9,	$0BB87_58
	NOP
	ADD	r4,	r7,	r0
$0BB87_58:
	ADDI	r7,	r7,	1
	ADDI	r8,	r0,	3
	CMP	r8,	r8,	r7
	bgeid	r8,	$0BB87_16
	NOP
	brid	$0BB87_59
	NOP
$0BB87_4:
	ADD	r7,	r3,	r0
$0BB87_5:
	ADD	r8,	r7,	r5
	bslli	r8,	r8,	2
	ADDI	r9,	r1,	80
	LW	r8,	r9,	r8
	ORI	r9,	r0,	0
	FPGE	r10,	r8,	r9
	FPUN	r9,	r8,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB87_7
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB87_7:
	bslli	r9,	r7,	2
	ADDI	r11,	r1,	40
	bneid	r10,	$0BB87_9
	LW	r9,	r11,	r9
	FPNEG	r8,	r8
$0BB87_9:
	FPMUL	r8,	r9,	r8
	FPLT	r9,	r8,	r6
	FPUN	r10,	r8,	r6
	BITOR	r10,	r10,	r9
	bneid	r10,	$0BB87_11
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB87_11:
	bneid	r9,	$0BB87_13
	NOP
	ADD	r6,	r8,	r0
$0BB87_13:
	bneid	r9,	$0BB87_15
	NOP
	ADD	r4,	r7,	r0
$0BB87_15:
	ADDI	r7,	r7,	1
	ADDI	r8,	r0,	3
	CMP	r8,	r8,	r7
	bltid	r8,	$0BB87_5
	NOP
$0BB87_16:
	CMP	r6,	r4,	r3
	beqid	r6,	$0BB87_18
	NOP
	bslli	r6,	r3,	2
	bslli	r7,	r4,	2
	ADDI	r9,	r1,	80
	LW	r8,	r9,	r7
	LW	r10,	r9,	r6
	SW	r10,	r9,	r7
	SW	r8,	r9,	r6
	ADD	r8,	r9,	r6
	ADD	r9,	r9,	r7
	LWI	r10,	r9,	12
	LWI	r11,	r8,	12
	SWI	r11,	r9,	12
	SWI	r10,	r8,	12
	LWI	r10,	r9,	24
	LWI	r11,	r8,	24
	SWI	r11,	r9,	24
	SWI	r10,	r8,	24
	ADDI	r8,	r1,	40
	LW	r6,	r8,	r6
	SW	r6,	r8,	r7
$0BB87_18:
	bslli	r6,	r3,	2
	ADDI	r7,	r1,	68
	SW	r4,	r7,	r6
	ADDI	r6,	r0,	2
	CMP	r6,	r6,	r3
	bneid	r6,	$0BB87_19
	NOP
$0BB87_64:
	ORI	r4,	r0,	0
	LWI	r3,	r1,	112
	FPNE	r5,	r3,	r4
	bneid	r5,	$0BB87_66
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB87_66:
	bneid	r4,	$0BB87_68
	NOP
	ADDI	r3,	r0,	786163455
	SWI	r3,	r1,	112
	ORI	r3,	r0,	786163455
$0BB87_68:
	SWI	r0,	r1,	64
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r5,	r0,	1065353216
	SWI	r5,	r1,	56
	LWI	r4,	r1,	68
	bslli	r4,	r4,	2
	ADDI	r8,	r1,	56
	LW	r6,	r8,	r4
	SW	r5,	r8,	r4
	SWI	r6,	r1,	56
	LWI	r5,	r1,	72
	bslli	r5,	r5,	2
	LW	r7,	r8,	r5
	LWI	r9,	r1,	60
	SW	r9,	r8,	r5
	ORI	r8,	r0,	0
	FPEQ	r6,	r6,	r8
	bneid	r6,	$0BB87_70
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB87_70:
	bneid	r8,	$0BB87_72
	ADDI	r6,	r0,	-1
	ADD	r6,	r0,	r0
$0BB87_72:
	beqid	r8,	$0BB87_73
	NOP
	ORI	r6,	r0,	0
	FPEQ	r6,	r7,	r6
	bneid	r6,	$0BB87_100
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB87_100:
	bneid	r8,	$0BB87_102
	ADDI	r6,	r0,	-1
	brid	$0BB87_102
	ADDI	r6,	r0,	1
$0BB87_73:
	ADD	r9,	r6,	r0
$0BB87_74:
	ADD	r8,	r9,	r0
	bslli	r9,	r8,	2
	ADDI	r10,	r1,	56
	LW	r9,	r10,	r9
	MULI	r10,	r8,	3
	bslli	r10,	r10,	2
	ADDI	r11,	r1,	80
	ADD	r10,	r10,	r11
	LWI	r10,	r10,	4
	FPMUL	r9,	r10,	r9
	FPRSUB	r7,	r9,	r7
	ADDI	r9,	r8,	1
	bltid	r8,	$0BB87_74
	NOP
$0BB87_102:
	SWI	r7,	r1,	60
	LWI	r7,	r1,	76
	bslli	r11,	r7,	2
	ADDI	r12,	r1,	56
	LW	r7,	r12,	r11
	LWI	r8,	r1,	64
	SW	r8,	r12,	r11
	ADDI	r8,	r0,	-1
	CMP	r8,	r8,	r6
	beqid	r8,	$0BB87_104
	NOP
$0BB87_103:
	bslli	r8,	r6,	2
	ADDI	r9,	r1,	56
	LW	r8,	r9,	r8
	MULI	r9,	r6,	3
	bslli	r9,	r9,	2
	ADDI	r10,	r1,	80
	ADD	r9,	r9,	r10
	LWI	r9,	r9,	8
	FPMUL	r8,	r9,	r8
	FPRSUB	r7,	r8,	r7
	ADDI	r6,	r6,	1
	ADDI	r8,	r0,	2
	CMP	r8,	r8,	r6
	bltid	r8,	$0BB87_103
	NOP
$0BB87_104:
	FPDIV	r9,	r7,	r3
	SWI	r9,	r1,	64
	LWI	r6,	r1,	108
	FPMUL	r7,	r6,	r9
	LWI	r8,	r1,	60
	FPRSUB	r8,	r7,	r8
	LWI	r7,	r1,	96
	FPDIV	r10,	r8,	r7
	SWI	r10,	r1,	60
	LWI	r8,	r1,	92
	FPMUL	r10,	r8,	r10
	LWI	r21,	r1,	56
	FPRSUB	r21,	r10,	r21
	LWI	r10,	r1,	104
	FPMUL	r9,	r10,	r9
	FPRSUB	r21,	r9,	r21
	LWI	r9,	r1,	80
	FPDIV	r21,	r21,	r9
	SWI	r21,	r1,	56
	LWI	r21,	r1,	64
	SWI	r21,	r20,	8
	LWI	r21,	r1,	60
	SWI	r21,	r20,	4
	LWI	r21,	r1,	56
	SWI	r21,	r20,	0
	SWI	r0,	r1,	64
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r21,	r0,	1065353216
	SWI	r21,	r1,	60
	ADD	r22,	r0,	r0
	LW	r23,	r12,	r4
	SW	r22,	r12,	r4
	SWI	r23,	r1,	56
	LW	r24,	r12,	r5
	LWI	r21,	r1,	60
	SW	r21,	r12,	r5
	ORI	r12,	r0,	0
	FPEQ	r12,	r23,	r12
	bneid	r12,	$0BB87_106
	ADDI	r25,	r0,	1
	ADDI	r25,	r0,	0
$0BB87_106:
	bneid	r25,	$0BB87_108
	ADDI	r21,	r0,	-1
	ADD	r21,	r22,	r0
$0BB87_108:
	beqid	r25,	$0BB87_109
	ADDI	r12,	r20,	12
	SWI	r24,	r1,	60
	ADDI	r21,	r1,	56
	LW	r22,	r21,	r11
	LWI	r23,	r1,	64
	SW	r23,	r21,	r11
	ORI	r21,	r0,	0
	FPNE	r24,	r24,	r21
	ADDI	r21,	r0,	1
	bneid	r24,	$0BB87_116
	ADD	r23,	r21,	r0
	ADDI	r23,	r0,	0
$0BB87_116:
	beqid	r23,	$0BB87_118
	NOP
	brid	$0BB87_117
	NOP
$0BB87_109:
	ADD	r26,	r21,	r0
$0BB87_110:
	ADD	r22,	r26,	r0
	bslli	r26,	r22,	2
	ADDI	r25,	r1,	56
	LW	r26,	r25,	r26
	MULI	r27,	r22,	3
	bslli	r27,	r27,	2
	ADDI	r28,	r1,	80
	ADD	r27,	r27,	r28
	LWI	r27,	r27,	4
	FPMUL	r26,	r27,	r26
	FPRSUB	r24,	r26,	r24
	ADDI	r26,	r22,	1
	bltid	r22,	$0BB87_110
	NOP
	SWI	r24,	r1,	60
	LW	r22,	r25,	r11
	LWI	r24,	r1,	64
	SW	r24,	r25,	r11
	ORI	r24,	r0,	0
	FPEQ	r24,	r23,	r24
	bneid	r24,	$0BB87_113
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB87_113:
	bneid	r23,	$0BB87_118
	NOP
$0BB87_117:
	bslli	r23,	r21,	2
	ADDI	r24,	r1,	56
	LW	r23,	r24,	r23
	MULI	r24,	r21,	3
	bslli	r24,	r24,	2
	ADDI	r25,	r1,	80
	ADD	r24,	r24,	r25
	LWI	r24,	r24,	8
	FPMUL	r23,	r24,	r23
	FPRSUB	r22,	r23,	r22
	ADDI	r21,	r21,	1
	ADDI	r23,	r0,	2
	CMP	r23,	r23,	r21
	bltid	r23,	$0BB87_117
	NOP
$0BB87_118:
	FPDIV	r21,	r22,	r3
	SWI	r21,	r1,	64
	FPMUL	r22,	r6,	r21
	FPMUL	r21,	r10,	r21
	LWI	r23,	r1,	60
	FPRSUB	r22,	r22,	r23
	FPDIV	r22,	r22,	r7
	SWI	r22,	r1,	60
	FPMUL	r22,	r8,	r22
	LWI	r23,	r1,	56
	FPRSUB	r22,	r22,	r23
	FPRSUB	r21,	r21,	r22
	FPDIV	r21,	r21,	r9
	SWI	r21,	r1,	56
	LWI	r21,	r1,	64
	SWI	r21,	r12,	8
	LWI	r21,	r1,	60
	SWI	r21,	r12,	4
	LWI	r21,	r1,	56
	SWI	r21,	r12,	0
	SWI	r0,	r1,	60
	SWI	r0,	r1,	56
	ADDI	r12,	r0,	1065353216
	SWI	r12,	r1,	64
	ADD	r12,	r0,	r0
	ADDI	r23,	r1,	56
	LW	r21,	r23,	r4
	SW	r12,	r23,	r4
	SWI	r21,	r1,	56
	LW	r22,	r23,	r5
	LWI	r4,	r1,	60
	SW	r4,	r23,	r5
	ORI	r4,	r0,	0
	FPEQ	r4,	r21,	r4
	bneid	r4,	$0BB87_120
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB87_120:
	bneid	r23,	$0BB87_122
	ADDI	r5,	r0,	-1
	ADD	r5,	r12,	r0
$0BB87_122:
	beqid	r23,	$0BB87_123
	ADDI	r4,	r20,	24
	SWI	r22,	r1,	60
	ADDI	r5,	r1,	56
	LW	r12,	r5,	r11
	LWI	r20,	r1,	64
	SW	r20,	r5,	r11
	ORI	r5,	r0,	0
	FPNE	r20,	r22,	r5
	ADDI	r5,	r0,	1
	bneid	r20,	$0BB87_130
	ADD	r11,	r5,	r0
	ADDI	r11,	r0,	0
$0BB87_130:
	beqid	r11,	$0BB87_132
	NOP
	brid	$0BB87_131
	NOP
$0BB87_123:
	ADD	r23,	r5,	r0
$0BB87_124:
	ADD	r12,	r23,	r0
	bslli	r23,	r12,	2
	ADDI	r20,	r1,	56
	LW	r23,	r20,	r23
	MULI	r24,	r12,	3
	bslli	r24,	r24,	2
	ADDI	r25,	r1,	80
	ADD	r24,	r24,	r25
	LWI	r24,	r24,	4
	FPMUL	r23,	r24,	r23
	FPRSUB	r22,	r23,	r22
	ADDI	r23,	r12,	1
	bltid	r12,	$0BB87_124
	NOP
	SWI	r22,	r1,	60
	LW	r12,	r20,	r11
	LWI	r22,	r1,	64
	SW	r22,	r20,	r11
	ORI	r11,	r0,	0
	FPEQ	r20,	r21,	r11
	bneid	r20,	$0BB87_127
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB87_127:
	bneid	r11,	$0BB87_132
	NOP
$0BB87_131:
	bslli	r11,	r5,	2
	ADDI	r20,	r1,	56
	LW	r11,	r20,	r11
	MULI	r20,	r5,	3
	bslli	r20,	r20,	2
	ADDI	r21,	r1,	80
	ADD	r20,	r20,	r21
	LWI	r20,	r20,	8
	FPMUL	r11,	r20,	r11
	FPRSUB	r12,	r11,	r12
	ADDI	r5,	r5,	1
	ADDI	r11,	r0,	2
	CMP	r11,	r11,	r5
	bltid	r11,	$0BB87_131
	NOP
$0BB87_132:
	FPDIV	r3,	r12,	r3
	SWI	r3,	r1,	64
	FPMUL	r5,	r6,	r3
	FPMUL	r3,	r10,	r3
	LWI	r6,	r1,	60
	FPRSUB	r5,	r5,	r6
	FPDIV	r5,	r5,	r7
	SWI	r5,	r1,	60
	FPMUL	r5,	r8,	r5
	LWI	r6,	r1,	56
	FPRSUB	r5,	r5,	r6
	FPRSUB	r3,	r3,	r5
	FPDIV	r3,	r3,	r9
	SWI	r3,	r1,	56
	LWI	r3,	r1,	64
	SWI	r3,	r4,	8
	LWI	r3,	r1,	60
	SWI	r3,	r4,	4
	LWI	r3,	r1,	56
	SWI	r3,	r4,	0
$0BB87_133:
	LWI	r28,	r1,	4
	LWI	r27,	r1,	8
	LWI	r26,	r1,	12
	LWI	r25,	r1,	16
	LWI	r24,	r1,	20
	LWI	r23,	r1,	24
	LWI	r22,	r1,	28
	LWI	r21,	r1,	32
	LWI	r20,	r1,	36
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	116
#	.end	_ZN8syMatrix6InvertEv
$0tmp87:
#	.size	_ZN8syMatrix6InvertEv, ($tmp87)-_ZN8syMatrix6InvertEv

#	.globl	_ZN8syMatrixmLERKS_
#	.align	2
#	.type	_ZN8syMatrixmLERKS_,@function
#	.ent	_ZN8syMatrixmLERKS_     # @_ZN8syMatrixmLERKS_
_ZN8syMatrixmLERKS_:
#	.frame	r1,68,r15
#	.mask	0x3f08000
	ADDI	r1,	r1,	-68
	SWI	r15,	r1,	0
	SWI	r20,	r1,	24
	SWI	r21,	r1,	20
	SWI	r22,	r1,	16
	SWI	r23,	r1,	12
	SWI	r24,	r1,	8
	SWI	r25,	r1,	4
	ADD	r20,	r5,	r0
	LWI	r7,	r6,	0
	LWI	r21,	r20,	0
	FPMUL	r4,	r21,	r7
	ORI	r3,	r0,	0
	FPADD	r4,	r4,	r3
	LWI	r11,	r6,	4
	LWI	r22,	r20,	12
	FPMUL	r5,	r22,	r11
	FPADD	r4,	r4,	r5
	LWI	r9,	r6,	8
	LWI	r23,	r20,	24
	FPMUL	r5,	r23,	r9
	FPADD	r24,	r4,	r5
	LWI	r4,	r6,	32
	LWI	r5,	r6,	24
	LWI	r8,	r6,	28
	LWI	r10,	r6,	20
	LWI	r12,	r6,	16
	LWI	r6,	r6,	12
	SWI	r24,	r1,	32
	FPMUL	r24,	r21,	r6
	FPADD	r24,	r24,	r3
	FPMUL	r25,	r22,	r12
	FPADD	r24,	r24,	r25
	FPMUL	r25,	r23,	r10
	FPADD	r24,	r24,	r25
	SWI	r24,	r1,	44
	FPMUL	r22,	r22,	r8
	FPMUL	r21,	r21,	r5
	FPADD	r21,	r21,	r3
	FPADD	r21,	r21,	r22
	FPMUL	r22,	r23,	r4
	FPADD	r21,	r21,	r22
	SWI	r21,	r1,	56
	LWI	r21,	r20,	4
	FPMUL	r22,	r21,	r7
	FPADD	r22,	r22,	r3
	LWI	r23,	r20,	16
	FPMUL	r24,	r23,	r11
	FPADD	r24,	r22,	r24
	LWI	r22,	r20,	28
	FPMUL	r25,	r22,	r9
	FPADD	r24,	r24,	r25
	SWI	r24,	r1,	36
	FPMUL	r24,	r21,	r6
	FPADD	r24,	r24,	r3
	FPMUL	r25,	r23,	r12
	FPADD	r24,	r24,	r25
	FPMUL	r25,	r22,	r10
	FPADD	r24,	r24,	r25
	SWI	r24,	r1,	48
	FPMUL	r23,	r23,	r8
	FPMUL	r21,	r21,	r5
	FPADD	r21,	r21,	r3
	FPADD	r21,	r21,	r23
	FPMUL	r22,	r22,	r4
	FPADD	r21,	r21,	r22
	SWI	r21,	r1,	60
	LWI	r21,	r20,	20
	FPMUL	r22,	r21,	r11
	LWI	r11,	r20,	8
	FPMUL	r7,	r11,	r7
	FPADD	r7,	r7,	r3
	FPADD	r22,	r7,	r22
	LWI	r7,	r20,	32
	FPMUL	r9,	r7,	r9
	FPADD	r9,	r22,	r9
	SWI	r9,	r1,	40
	FPMUL	r9,	r21,	r12
	FPMUL	r6,	r11,	r6
	FPADD	r6,	r6,	r3
	FPADD	r6,	r6,	r9
	FPMUL	r9,	r7,	r10
	FPADD	r6,	r6,	r9
	SWI	r6,	r1,	52
	FPMUL	r6,	r21,	r8
	FPMUL	r5,	r11,	r5
	FPADD	r3,	r5,	r3
	FPADD	r3,	r3,	r6
	FPMUL	r4,	r7,	r4
	FPADD	r3,	r3,	r4
	SWI	r3,	r1,	64
	ADDI	r6,	r1,	32
	ADDI	r7,	r0,	36
	brlid	r15,	memcpy
	ADD	r5,	r20,	r0
	ADD	r3,	r20,	r0
	LWI	r25,	r1,	4
	LWI	r24,	r1,	8
	LWI	r23,	r1,	12
	LWI	r22,	r1,	16
	LWI	r21,	r1,	20
	LWI	r20,	r1,	24
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	68
#	.end	_ZN8syMatrixmLERKS_
$0tmp88:
#	.size	_ZN8syMatrixmLERKS_, ($tmp88)-_ZN8syMatrixmLERKS_

#	.globl	_ZNK8syMatrixmlERK8syVector
#	.align	2
#	.type	_ZNK8syMatrixmlERK8syVector,@function
#	.ent	_ZNK8syMatrixmlERK8syVector # @_ZNK8syMatrixmlERK8syVector
_ZNK8syMatrixmlERK8syVector:
#	.frame	r1,4,r15
#	.mask	0x100000
	ADDI	r1,	r1,	-4
	SWI	r20,	r1,	0
	LWI	r4,	r6,	12
	LWI	r3,	r7,	4
	FPMUL	r8,	r3,	r4
	LWI	r9,	r6,	0
	LWI	r4,	r7,	0
	FPMUL	r9,	r4,	r9
	FPADD	r8,	r9,	r8
	LWI	r7,	r7,	8
	LWI	r9,	r6,	24
	FPMUL	r9,	r7,	r9
	FPADD	r11,	r8,	r9
	LWI	r8,	r6,	32
	LWI	r9,	r6,	8
	LWI	r10,	r6,	20
	LWI	r12,	r6,	28
	LWI	r20,	r6,	4
	LWI	r6,	r6,	16
	SWI	r11,	r5,	0
	FPMUL	r6,	r3,	r6
	FPMUL	r11,	r4,	r20
	FPADD	r6,	r11,	r6
	FPMUL	r11,	r7,	r12
	FPADD	r6,	r6,	r11
	SWI	r6,	r5,	4
	FPMUL	r3,	r3,	r10
	FPMUL	r4,	r4,	r9
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r7,	r8
	FPADD	r3,	r3,	r4
	SWI	r3,	r5,	8
	LWI	r20,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_ZNK8syMatrixmlERK8syVector
$0tmp89:
#	.size	_ZNK8syMatrixmlERK8syVector, ($tmp89)-_ZNK8syMatrixmlERK8syVector

#	.globl	_ZN8sySphereC2Ev
#	.align	2
#	.type	_ZN8sySphereC2Ev,@function
#	.ent	_ZN8sySphereC2Ev        # @_ZN8sySphereC2Ev
_ZN8sySphereC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN8sySphereC2Ev
$0tmp90:
#	.size	_ZN8sySphereC2Ev, ($tmp90)-_ZN8sySphereC2Ev

#	.globl	_ZN8sySphereC2ERK8syVectorRKfRKiS6_
#	.align	2
#	.type	_ZN8sySphereC2ERK8syVectorRKfRKiS6_,@function
#	.ent	_ZN8sySphereC2ERK8syVectorRKfRKiS6_ # @_ZN8sySphereC2ERK8syVectorRKfRKiS6_
_ZN8sySphereC2ERK8syVectorRKfRKiS6_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	8
	LWI	r3,	r6,	4
	SWI	r3,	r5,	12
	LWI	r3,	r6,	8
	SWI	r3,	r5,	16
	LWI	r3,	r7,	0
	SWI	r3,	r5,	20
	LWI	r3,	r8,	0
	SWI	r3,	r5,	0
	LWI	r3,	r9,	0
	rtsd	r15,	8
	SWI	r3,	r5,	4
#	.end	_ZN8sySphereC2ERK8syVectorRKfRKiS6_
$0tmp91:
#	.size	_ZN8sySphereC2ERK8syVectorRKfRKiS6_, ($tmp91)-_ZN8sySphereC2ERK8syVectorRKfRKiS6_

#	.globl	_ZN8sySphere3SetERK8syVectorRKfRKiS6_
#	.align	2
#	.type	_ZN8sySphere3SetERK8syVectorRKfRKiS6_,@function
#	.ent	_ZN8sySphere3SetERK8syVectorRKfRKiS6_ # @_ZN8sySphere3SetERK8syVectorRKfRKiS6_
_ZN8sySphere3SetERK8syVectorRKfRKiS6_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	8
	LWI	r3,	r6,	4
	SWI	r3,	r5,	12
	LWI	r3,	r6,	8
	SWI	r3,	r5,	16
	LWI	r3,	r7,	0
	SWI	r3,	r5,	20
	LWI	r3,	r8,	0
	SWI	r3,	r5,	0
	LWI	r3,	r9,	0
	rtsd	r15,	8
	SWI	r3,	r5,	4
#	.end	_ZN8sySphere3SetERK8syVectorRKfRKiS6_
$0tmp92:
#	.size	_ZN8sySphere3SetERK8syVectorRKfRKiS6_, ($tmp92)-_ZN8sySphere3SetERK8syVectorRKfRKiS6_

#	.globl	_ZN8sySphere3SetERK8syVectorRKf
#	.align	2
#	.type	_ZN8sySphere3SetERK8syVectorRKf,@function
#	.ent	_ZN8sySphere3SetERK8syVectorRKf # @_ZN8sySphere3SetERK8syVectorRKf
_ZN8sySphere3SetERK8syVectorRKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	8
	LWI	r3,	r6,	4
	SWI	r3,	r5,	12
	LWI	r3,	r6,	8
	SWI	r3,	r5,	16
	LWI	r3,	r7,	0
	rtsd	r15,	8
	SWI	r3,	r5,	20
#	.end	_ZN8sySphere3SetERK8syVectorRKf
$0tmp93:
#	.size	_ZN8sySphere3SetERK8syVectorRKf, ($tmp93)-_ZN8sySphere3SetERK8syVectorRKf

#	.globl	_ZNK8sySphere20IntersectsWShadowRayERK3Ray
#	.align	2
#	.type	_ZNK8sySphere20IntersectsWShadowRayERK3Ray,@function
#	.ent	_ZNK8sySphere20IntersectsWShadowRayERK3Ray # @_ZNK8sySphere20IntersectsWShadowRayERK3Ray
_ZNK8sySphere20IntersectsWShadowRayERK3Ray:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	8
	LWI	r4,	r6,	0
	FPRSUB	r3,	r3,	r4
	LWI	r4,	r5,	12
	LWI	r7,	r6,	4
	FPRSUB	r4,	r4,	r7
	FPMUL	r7,	r4,	r4
	FPMUL	r8,	r3,	r3
	FPADD	r8,	r8,	r7
	LWI	r7,	r5,	16
	LWI	r9,	r6,	8
	FPRSUB	r7,	r7,	r9
	FPMUL	r9,	r7,	r7
	FPADD	r8,	r8,	r9
	LWI	r5,	r5,	20
	FPMUL	r5,	r5,	r5
	FPRSUB	r8,	r5,	r8
	LWI	r9,	r6,	16
	FPMUL	r5,	r9,	r9
	LWI	r10,	r6,	12
	FPMUL	r11,	r10,	r10
	FPADD	r5,	r11,	r5
	LWI	r6,	r6,	20
	FPMUL	r11,	r6,	r6
	FPADD	r5,	r5,	r11
	ORI	r11,	r0,	-1065353216
	FPMUL	r11,	r5,	r11
	FPMUL	r8,	r11,	r8
	FPMUL	r4,	r9,	r4
	FPMUL	r3,	r10,	r3
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r6,	r7
	FPADD	r3,	r3,	r4
	FPADD	r4,	r3,	r3
	FPMUL	r3,	r4,	r4
	FPADD	r6,	r3,	r8
	ORI	r3,	r0,	0
	FPLT	r3,	r6,	r3
	bneid	r3,	$0BB94_2
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB94_2:
	ORI	r3,	r0,	1203982336
	bneid	r7,	$0BB94_10
	NOP
	ORI	r3,	r0,	1056964608
	FPDIV	r5,	r3,	r5
	FPINVSQRT	r3,	r6
	ORI	r6,	r0,	1065353216
	FPDIV	r6,	r6,	r3
	FPNEG	r3,	r4
	FPRSUB	r3,	r6,	r3
	FPMUL	r3,	r3,	r5
	ORI	r7,	r0,	0
	FPGT	r8,	r3,	r7
	bneid	r8,	$0BB94_5
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB94_5:
	bneid	r7,	$0BB94_10
	NOP
	FPRSUB	r3,	r4,	r6
	FPMUL	r3,	r3,	r5
	ORI	r4,	r0,	0
	FPGT	r5,	r3,	r4
	bneid	r5,	$0BB94_8
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB94_8:
	ORI	r5,	r0,	1203982336
	bneid	r4,	$0BB94_10
	NOP
	ADD	r3,	r5,	r0
$0BB94_10:
	rtsd	r15,	8
	NOP
#	.end	_ZNK8sySphere20IntersectsWShadowRayERK3Ray
$0tmp94:
#	.size	_ZNK8sySphere20IntersectsWShadowRayERK3Ray, ($tmp94)-_ZNK8sySphere20IntersectsWShadowRayERK3Ray

#	.globl	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
#	.align	2
#	.type	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord,@function
#	.ent	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord # @_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
_ZNK8sySphere11IntersectsWERK3RayR9HitRecord:
#	.frame	r1,4,r15
#	.mask	0x100000
	ADDI	r1,	r1,	-4
	SWI	r20,	r1,	0
	LWI	r3,	r5,	8
	LWI	r4,	r6,	0
	FPRSUB	r3,	r3,	r4
	LWI	r4,	r5,	12
	LWI	r8,	r6,	4
	FPRSUB	r4,	r4,	r8
	FPMUL	r8,	r4,	r4
	FPMUL	r9,	r3,	r3
	FPADD	r9,	r9,	r8
	LWI	r8,	r5,	16
	LWI	r10,	r6,	8
	FPRSUB	r8,	r8,	r10
	FPMUL	r10,	r8,	r8
	FPADD	r9,	r9,	r10
	LWI	r10,	r5,	20
	FPMUL	r10,	r10,	r10
	FPRSUB	r9,	r10,	r9
	LWI	r10,	r6,	16
	FPMUL	r12,	r10,	r10
	LWI	r11,	r6,	12
	FPMUL	r20,	r11,	r11
	FPADD	r20,	r20,	r12
	LWI	r12,	r6,	20
	FPMUL	r6,	r12,	r12
	FPADD	r6,	r20,	r6
	ORI	r20,	r0,	-1065353216
	FPMUL	r20,	r6,	r20
	FPMUL	r9,	r20,	r9
	FPMUL	r4,	r10,	r4
	FPMUL	r3,	r11,	r3
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r12,	r8
	FPADD	r3,	r3,	r4
	FPADD	r4,	r3,	r3
	FPMUL	r3,	r4,	r4
	FPADD	r8,	r3,	r9
	ORI	r3,	r0,	0
	FPLT	r3,	r8,	r3
	bneid	r3,	$0BB95_2
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB95_2:
	bneid	r9,	$0BB95_18
	ADD	r3,	r0,	r0
	ORI	r3,	r0,	1056964608
	FPDIV	r3,	r3,	r6
	FPINVSQRT	r6,	r8
	ORI	r8,	r0,	1065353216
	FPDIV	r6,	r8,	r6
	FPNEG	r8,	r4
	FPRSUB	r8,	r6,	r8
	FPMUL	r8,	r8,	r3
	ORI	r9,	r0,	0
	FPLE	r10,	r8,	r9
	FPUN	r9,	r8,	r9
	BITOR	r10,	r9,	r10
	bneid	r10,	$0BB95_5
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB95_5:
	bneid	r9,	$0BB95_10
	NOP
	LWI	r9,	r7,	0
	FPGE	r10,	r8,	r9
	FPUN	r9,	r8,	r9
	BITOR	r10,	r9,	r10
	bneid	r10,	$0BB95_8
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB95_8:
	bneid	r9,	$0BB95_10
	NOP
	brid	$0BB95_17
	SWI	r8,	r7,	0
$0BB95_10:
	FPRSUB	r4,	r4,	r6
	FPMUL	r4,	r4,	r3
	ORI	r3,	r0,	0
	FPLE	r6,	r4,	r3
	FPUN	r3,	r4,	r3
	BITOR	r3,	r3,	r6
	bneid	r3,	$0BB95_12
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB95_12:
	bneid	r6,	$0BB95_18
	ADD	r3,	r0,	r0
	LWI	r3,	r7,	0
	FPGE	r6,	r4,	r3
	FPUN	r3,	r4,	r3
	BITOR	r3,	r3,	r6
	bneid	r3,	$0BB95_15
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB95_15:
	bneid	r6,	$0BB95_18
	ADD	r3,	r0,	r0
	SWI	r4,	r7,	0
$0BB95_17:
	LWI	r3,	r5,	4
	SWI	r3,	r7,	4
	LWI	r3,	r5,	0
	SWI	r3,	r7,	8
	ADDI	r3,	r0,	1
$0BB95_18:
	LWI	r20,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
$0tmp95:
#	.size	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord, ($tmp95)-_ZNK8sySphere11IntersectsWERK3RayR9HitRecord

#	.globl	_ZNK8sySphere6ID_MATEv
#	.align	2
#	.type	_ZNK8sySphere6ID_MATEv,@function
#	.ent	_ZNK8sySphere6ID_MATEv  # @_ZNK8sySphere6ID_MATEv
_ZNK8sySphere6ID_MATEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADDI	r3,	r5,	4
#	.end	_ZNK8sySphere6ID_MATEv
$0tmp96:
#	.size	_ZNK8sySphere6ID_MATEv, ($tmp96)-_ZNK8sySphere6ID_MATEv

#	.globl	_ZNK8sySphere6ID_OBJEv
#	.align	2
#	.type	_ZNK8sySphere6ID_OBJEv,@function
#	.ent	_ZNK8sySphere6ID_OBJEv  # @_ZNK8sySphere6ID_OBJEv
_ZNK8sySphere6ID_OBJEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZNK8sySphere6ID_OBJEv
$0tmp97:
#	.size	_ZNK8sySphere6ID_OBJEv, ($tmp97)-_ZNK8sySphere6ID_OBJEv

#	.globl	_ZNK8sySphere6NormalERK8syVector
#	.align	2
#	.type	_ZNK8sySphere6NormalERK8syVector,@function
#	.ent	_ZNK8sySphere6NormalERK8syVector # @_ZNK8sySphere6NormalERK8syVector
_ZNK8sySphere6NormalERK8syVector:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	8
	LWI	r4,	r7,	0
	FPRSUB	r3,	r3,	r4
	LWI	r4,	r6,	12
	LWI	r8,	r7,	4
	FPRSUB	r4,	r4,	r8
	FPMUL	r8,	r4,	r4
	FPMUL	r9,	r3,	r3
	FPADD	r8,	r9,	r8
	LWI	r6,	r6,	16
	LWI	r7,	r7,	8
	FPRSUB	r6,	r6,	r7
	FPMUL	r7,	r6,	r6
	FPADD	r7,	r8,	r7
	FPINVSQRT	r7,	r7
	ORI	r8,	r0,	1065353216
	FPDIV	r7,	r8,	r7
	FPDIV	r3,	r3,	r7
	SWI	r3,	r5,	0
	FPDIV	r3,	r4,	r7
	SWI	r3,	r5,	4
	FPDIV	r3,	r6,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8sySphere6NormalERK8syVector
$0tmp98:
#	.size	_ZNK8sySphere6NormalERK8syVector, ($tmp98)-_ZNK8sySphere6NormalERK8syVector

#	.globl	_ZN10syTriangleC2Ei
#	.align	2
#	.type	_ZN10syTriangleC2Ei,@function
#	.ent	_ZN10syTriangleC2Ei     # @_ZN10syTriangleC2Ei
_ZN10syTriangleC2Ei:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r6,	r5,	36
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	6
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	32
	ADD	r3,	r0,	r0
	sbi	r3,	r5,	40
	rtsd	r15,	8
	sbi	r3,	r5,	48
#	.end	_ZN10syTriangleC2Ei
$0tmp99:
#	.size	_ZN10syTriangleC2Ei, ($tmp99)-_ZN10syTriangleC2Ei

#	.globl	_ZN10syTriangle14LoadFromMemoryEi
#	.align	2
#	.type	_ZN10syTriangle14LoadFromMemoryEi,@function
#	.ent	_ZN10syTriangle14LoadFromMemoryEi # @_ZN10syTriangle14LoadFromMemoryEi
_ZN10syTriangle14LoadFromMemoryEi:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r6,	r5,	36
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	6
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	32
	ADD	r3,	r0,	r0
	sbi	r3,	r5,	40
	rtsd	r15,	8
	sbi	r3,	r5,	48
#	.end	_ZN10syTriangle14LoadFromMemoryEi
$0tmp100:
#	.size	_ZN10syTriangle14LoadFromMemoryEi, ($tmp100)-_ZN10syTriangle14LoadFromMemoryEi

#	.globl	_ZN10syTriangleC2Ev
#	.align	2
#	.type	_ZN10syTriangleC2Ev,@function
#	.ent	_ZN10syTriangleC2Ev     # @_ZN10syTriangleC2Ev
_ZN10syTriangleC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN10syTriangleC2Ev
$0tmp101:
#	.size	_ZN10syTriangleC2Ev, ($tmp101)-_ZN10syTriangleC2Ev

#	.globl	_ZN10syTriangleC2ERKS_
#	.align	2
#	.type	_ZN10syTriangleC2ERKS_,@function
#	.ent	_ZN10syTriangleC2ERKS_  # @_ZN10syTriangleC2ERKS_
_ZN10syTriangleC2ERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r6,	12
	SWI	r3,	r5,	12
	LWI	r3,	r6,	16
	SWI	r3,	r5,	16
	LWI	r3,	r6,	20
	SWI	r3,	r5,	20
	LWI	r3,	r6,	24
	SWI	r3,	r5,	24
	LWI	r3,	r6,	28
	SWI	r3,	r5,	28
	LWI	r3,	r6,	32
	SWI	r3,	r5,	32
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	sbi	r3,	r5,	48
#	.end	_ZN10syTriangleC2ERKS_
$0tmp102:
#	.size	_ZN10syTriangleC2ERKS_, ($tmp102)-_ZN10syTriangleC2ERKS_

#	.globl	_ZN10syTriangle3SetERK8syVectorS2_S2_
#	.align	2
#	.type	_ZN10syTriangle3SetERK8syVectorS2_S2_,@function
#	.ent	_ZN10syTriangle3SetERK8syVectorS2_S2_ # @_ZN10syTriangle3SetERK8syVectorS2_S2_
_ZN10syTriangle3SetERK8syVectorS2_S2_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	SWI	r3,	r5,	12
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	8
	SWI	r3,	r5,	20
	LWI	r3,	r8,	0
	SWI	r3,	r5,	24
	LWI	r3,	r8,	4
	SWI	r3,	r5,	28
	LWI	r3,	r8,	8
	SWI	r3,	r5,	32
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	sbi	r3,	r5,	48
#	.end	_ZN10syTriangle3SetERK8syVectorS2_S2_
$0tmp103:
#	.size	_ZN10syTriangle3SetERK8syVectorS2_S2_, ($tmp103)-_ZN10syTriangle3SetERK8syVectorS2_S2_

#	.globl	_ZN10syTriangleC2ERK8syVectorS2_S2_
#	.align	2
#	.type	_ZN10syTriangleC2ERK8syVectorS2_S2_,@function
#	.ent	_ZN10syTriangleC2ERK8syVectorS2_S2_ # @_ZN10syTriangleC2ERK8syVectorS2_S2_
_ZN10syTriangleC2ERK8syVectorS2_S2_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r3,	r7,	0
	SWI	r3,	r5,	12
	LWI	r3,	r7,	4
	SWI	r3,	r5,	16
	LWI	r3,	r7,	8
	SWI	r3,	r5,	20
	LWI	r3,	r8,	0
	SWI	r3,	r5,	24
	LWI	r3,	r8,	4
	SWI	r3,	r5,	28
	LWI	r3,	r8,	8
	SWI	r3,	r5,	32
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	sbi	r3,	r5,	48
#	.end	_ZN10syTriangleC2ERK8syVectorS2_S2_
$0tmp104:
#	.size	_ZN10syTriangleC2ERK8syVectorS2_S2_, ($tmp104)-_ZN10syTriangleC2ERK8syVectorS2_S2_

#	.globl	_ZN10syTriangle10LoadVertexEv
#	.align	2
#	.type	_ZN10syTriangle10LoadVertexEv,@function
#	.ent	_ZN10syTriangle10LoadVertexEv # @_ZN10syTriangle10LoadVertexEv
_ZN10syTriangle10LoadVertexEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r5,	36
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	8
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	3
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	12
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	16
	LOAD	r3,	r3,	2
	SWI	r3,	r5,	20
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	6
	LOAD	r4,	r3,	0
	SWI	r4,	r5,	24
	LOAD	r4,	r3,	1
	SWI	r4,	r5,	28
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	32
#	.end	_ZN10syTriangle10LoadVertexEv
$0tmp105:
#	.size	_ZN10syTriangle10LoadVertexEv, ($tmp105)-_ZN10syTriangle10LoadVertexEv

#	.globl	_ZNK10syTriangle12LoadMaterialEv
#	.align	2
#	.type	_ZNK10syTriangle12LoadMaterialEv,@function
#	.ent	_ZNK10syTriangle12LoadMaterialEv # @_ZNK10syTriangle12LoadMaterialEv
_ZNK10syTriangle12LoadMaterialEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	10
	LOAD	r3,	r3,	0
	rtsd	r15,	8
	SWI	r3,	r5,	44
#	.end	_ZNK10syTriangle12LoadMaterialEv
$0tmp106:
#	.size	_ZNK10syTriangle12LoadMaterialEv, ($tmp106)-_ZNK10syTriangle12LoadMaterialEv

#	.globl	_ZNK10syTriangle6NormalEv
#	.align	2
#	.type	_ZNK10syTriangle6NormalEv,@function
#	.ent	_ZNK10syTriangle6NormalEv # @_ZNK10syTriangle6NormalEv
_ZNK10syTriangle6NormalEv:
#	.frame	r1,0,r15
#	.mask	0x0
	lbui	r3,	r6,	48
	bneid	r3,	$0BB107_2
	NOP
	LWI	r4,	r6,	20
	LWI	r3,	r6,	8
	FPRSUB	r7,	r4,	r3
	LWI	r4,	r6,	32
	FPRSUB	r8,	r4,	r3
	LWI	r3,	r6,	24
	LWI	r4,	r6,	0
	FPRSUB	r3,	r3,	r4
	LWI	r9,	r6,	12
	FPRSUB	r4,	r9,	r4
	FPMUL	r9,	r4,	r8
	FPMUL	r10,	r7,	r3
	FPRSUB	r9,	r9,	r10
	LWI	r10,	r6,	16
	LWI	r11,	r6,	4
	FPRSUB	r10,	r10,	r11
	LWI	r12,	r6,	28
	FPRSUB	r11,	r12,	r11
	FPMUL	r7,	r7,	r11
	FPMUL	r8,	r10,	r8
	FPRSUB	r7,	r7,	r8
	SWI	r7,	r6,	52
	SWI	r9,	r6,	56
	FPMUL	r8,	r9,	r9
	FPMUL	r7,	r7,	r7
	FPADD	r7,	r7,	r8
	FPMUL	r3,	r10,	r3
	FPMUL	r4,	r4,	r11
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r6,	60
	FPMUL	r3,	r3,	r3
	FPADD	r3,	r7,	r3
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	LWI	r4,	r6,	52
	FPDIV	r4,	r4,	r3
	SWI	r4,	r6,	52
	LWI	r4,	r6,	56
	FPDIV	r4,	r4,	r3
	SWI	r4,	r6,	56
	LWI	r4,	r6,	60
	FPDIV	r3,	r4,	r3
	SWI	r3,	r6,	60
	ADDI	r3,	r0,	1
	sbi	r3,	r6,	48
$0BB107_2:
	LWI	r3,	r6,	60
	SWI	r3,	r5,	8
	LWI	r3,	r6,	56
	SWI	r3,	r5,	4
	LWI	r3,	r6,	52
	rtsd	r15,	8
	SWI	r3,	r5,	0
#	.end	_ZNK10syTriangle6NormalEv
$0tmp107:
#	.size	_ZNK10syTriangle6NormalEv, ($tmp107)-_ZNK10syTriangle6NormalEv

#	.globl	_ZNK10syTriangle6ID_MATEv
#	.align	2
#	.type	_ZNK10syTriangle6ID_MATEv,@function
#	.ent	_ZNK10syTriangle6ID_MATEv # @_ZNK10syTriangle6ID_MATEv
_ZNK10syTriangle6ID_MATEv:
#	.frame	r1,0,r15
#	.mask	0x0
	lbui	r3,	r5,	40
	beqid	r3,	$0BB108_2
	NOP
	rtsd	r15,	8
	LWI	r3,	r5,	44
$0BB108_2:
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	10
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	44
	ADDI	r4,	r0,	1
	rtsd	r15,	8
	sbi	r4,	r5,	40
#	.end	_ZNK10syTriangle6ID_MATEv
$0tmp108:
#	.size	_ZNK10syTriangle6ID_MATEv, ($tmp108)-_ZNK10syTriangle6ID_MATEv

#	.globl	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
#	.align	2
#	.type	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord,@function
#	.ent	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord # @_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord:
#	.frame	r1,48,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-48
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r7,	r1,	60
	LWI	r23,	r5,	8
	LWI	r3,	r5,	32
	FPRSUB	r12,	r23,	r3
	LWI	r24,	r5,	4
	LWI	r3,	r5,	28
	FPRSUB	r21,	r24,	r3
	LWI	r4,	r6,	20
	FPMUL	r3,	r4,	r21
	LWI	r8,	r6,	16
	FPMUL	r7,	r8,	r12
	FPRSUB	r7,	r3,	r7
	LWI	r25,	r5,	0
	LWI	r3,	r5,	24
	FPRSUB	r22,	r25,	r3
	LWI	r9,	r6,	12
	FPMUL	r3,	r9,	r12
	FPMUL	r10,	r4,	r22
	FPRSUB	r11,	r3,	r10
	LWI	r3,	r5,	12
	FPRSUB	r27,	r25,	r3
	LWI	r3,	r5,	16
	FPRSUB	r28,	r24,	r3
	FPMUL	r3,	r11,	r28
	FPMUL	r10,	r7,	r27
	FPADD	r3,	r10,	r3
	FPMUL	r10,	r8,	r22
	FPMUL	r20,	r9,	r21
	FPRSUB	r20,	r10,	r20
	LWI	r10,	r5,	20
	FPRSUB	r29,	r23,	r10
	FPMUL	r10,	r20,	r29
	FPADD	r26,	r3,	r10
	ORI	r3,	r0,	0
	FPGE	r10,	r26,	r3
	FPUN	r3,	r26,	r3
	BITOR	r10,	r3,	r10
	bneid	r10,	$0BB109_2
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB109_2:
	LWI	r10,	r6,	8
	LWI	r31,	r6,	0
	LWI	r6,	r6,	4
	bneid	r3,	$0BB109_4
	ADD	r30,	r26,	r0
	FPNEG	r30,	r26
$0BB109_4:
	ORI	r3,	r0,	953267991
	FPLT	r3,	r30,	r3
	bneid	r3,	$0BB109_6
	ADDI	r30,	r0,	1
	ADDI	r30,	r0,	0
$0BB109_6:
	bneid	r30,	$0BB109_26
	ADD	r3,	r0,	r0
	FPRSUB	r30,	r23,	r10
	FPRSUB	r31,	r25,	r31
	FPRSUB	r10,	r24,	r6
	FPMUL	r3,	r10,	r27
	FPMUL	r6,	r31,	r28
	FPMUL	r24,	r30,	r27
	FPMUL	r25,	r31,	r29
	FPMUL	r27,	r30,	r28
	FPMUL	r28,	r10,	r29
	FPRSUB	r23,	r3,	r6
	FPRSUB	r25,	r25,	r24
	FPRSUB	r24,	r27,	r28
	FPMUL	r3,	r25,	r21
	FPMUL	r6,	r24,	r22
	FPADD	r3,	r6,	r3
	FPMUL	r6,	r23,	r12
	FPADD	r3,	r3,	r6
	ORI	r6,	r0,	1065353216
	FPDIV	r12,	r6,	r26
	FPMUL	r6,	r3,	r12
	ORI	r3,	r0,	0
	FPLT	r3,	r6,	r3
	bneid	r3,	$0BB109_9
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB109_9:
	bneid	r21,	$0BB109_26
	ADD	r3,	r0,	r0
	FPMUL	r3,	r11,	r10
	FPMUL	r7,	r7,	r31
	FPADD	r3,	r7,	r3
	FPMUL	r7,	r20,	r30
	FPADD	r3,	r3,	r7
	FPMUL	r10,	r3,	r12
	ORI	r3,	r0,	0
	FPLT	r7,	r10,	r3
	FPUN	r3,	r10,	r3
	BITOR	r3,	r3,	r7
	bneid	r3,	$0BB109_12
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB109_12:
	bneid	r7,	$0BB109_26
	ADD	r3,	r0,	r0
	FPMUL	r7,	r25,	r8
	FPMUL	r8,	r24,	r9
	FPADD	r7,	r8,	r7
	FPMUL	r4,	r23,	r4
	FPADD	r4,	r7,	r4
	FPMUL	r4,	r4,	r12
	ORI	r7,	r0,	0
	FPLT	r8,	r4,	r7
	FPUN	r7,	r4,	r7
	BITOR	r8,	r7,	r8
	bneid	r8,	$0BB109_15
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB109_15:
	bneid	r7,	$0BB109_26
	NOP
	FPADD	r4,	r10,	r4
	ORI	r7,	r0,	1065353216
	FPGT	r8,	r4,	r7
	FPUN	r4,	r4,	r7
	BITOR	r7,	r4,	r8
	bneid	r7,	$0BB109_18
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB109_18:
	bneid	r4,	$0BB109_26
	NOP
	LWI	r3,	r1,	60
	LWI	r3,	r3,	0
	FPGE	r4,	r6,	r3
	FPUN	r3,	r6,	r3
	BITOR	r3,	r3,	r4
	bneid	r3,	$0BB109_21
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB109_21:
	bneid	r4,	$0BB109_26
	ADD	r3,	r0,	r0
	LWI	r3,	r1,	60
	SWI	r6,	r3,	0
	lbui	r3,	r5,	40
	beqid	r3,	$0BB109_24
	NOP
	ADDI	r4,	r5,	36
	brid	$0BB109_25
	LWI	r3,	r5,	44
$0BB109_24:
	LWI	r3,	r5,	36
	ADDI	r3,	r3,	10
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	44
	ADDI	r4,	r0,	1
	sbi	r4,	r5,	40
	ADDI	r4,	r5,	36
$0BB109_25:
	LWI	r5,	r1,	60
	SWI	r3,	r5,	4
	LWI	r3,	r4,	0
	SWI	r3,	r5,	8
	ADDI	r3,	r0,	1
$0BB109_26:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	48
#	.end	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
$0tmp109:
#	.size	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord, ($tmp109)-_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord

#	.globl	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
#	.align	2
#	.type	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist,@function
#	.ent	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist # @_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist:
#	.frame	r1,48,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-48
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	LWI	r23,	r5,	8
	LWI	r3,	r5,	32
	FPRSUB	r12,	r23,	r3
	LWI	r24,	r5,	4
	LWI	r3,	r5,	28
	FPRSUB	r21,	r24,	r3
	LWI	r4,	r6,	20
	FPMUL	r3,	r4,	r21
	LWI	r8,	r6,	16
	FPMUL	r9,	r8,	r12
	FPRSUB	r10,	r3,	r9
	LWI	r25,	r5,	0
	LWI	r3,	r5,	24
	FPRSUB	r22,	r25,	r3
	LWI	r9,	r6,	12
	FPMUL	r3,	r9,	r12
	FPMUL	r11,	r4,	r22
	FPRSUB	r11,	r3,	r11
	LWI	r3,	r5,	12
	FPRSUB	r26,	r25,	r3
	LWI	r3,	r5,	16
	FPRSUB	r27,	r24,	r3
	FPMUL	r3,	r11,	r27
	FPMUL	r20,	r10,	r26
	FPADD	r3,	r20,	r3
	FPMUL	r20,	r8,	r22
	FPMUL	r28,	r9,	r21
	FPRSUB	r20,	r20,	r28
	LWI	r5,	r5,	20
	FPRSUB	r28,	r23,	r5
	FPMUL	r5,	r20,	r28
	FPADD	r5,	r3,	r5
	ORI	r3,	r0,	0
	FPGE	r29,	r5,	r3
	FPUN	r3,	r5,	r3
	BITOR	r29,	r3,	r29
	bneid	r29,	$0BB110_2
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB110_2:
	LWI	r29,	r6,	8
	LWI	r30,	r6,	0
	LWI	r6,	r6,	4
	bneid	r3,	$0BB110_4
	ADD	r31,	r5,	r0
	FPNEG	r31,	r5
$0BB110_4:
	ORI	r3,	r0,	953267991
	FPLT	r3,	r31,	r3
	bneid	r3,	$0BB110_6
	ADDI	r31,	r0,	1
	ADDI	r31,	r0,	0
$0BB110_6:
	bneid	r31,	$0BB110_23
	ADD	r3,	r0,	r0
	FPRSUB	r29,	r23,	r29
	FPRSUB	r25,	r25,	r30
	FPRSUB	r30,	r24,	r6
	FPMUL	r3,	r30,	r26
	FPMUL	r6,	r25,	r27
	FPMUL	r23,	r29,	r26
	FPMUL	r24,	r25,	r28
	FPMUL	r26,	r29,	r27
	FPMUL	r27,	r30,	r28
	FPRSUB	r6,	r3,	r6
	FPRSUB	r24,	r24,	r23
	FPRSUB	r23,	r26,	r27
	FPMUL	r3,	r24,	r21
	FPMUL	r21,	r23,	r22
	FPADD	r3,	r21,	r3
	FPMUL	r12,	r6,	r12
	FPADD	r3,	r3,	r12
	ORI	r12,	r0,	1065353216
	FPDIV	r12,	r12,	r5
	FPMUL	r5,	r3,	r12
	ORI	r3,	r0,	0
	FPLT	r3,	r5,	r3
	bneid	r3,	$0BB110_9
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB110_9:
	bneid	r21,	$0BB110_23
	ADD	r3,	r0,	r0
	FPMUL	r3,	r11,	r30
	FPMUL	r10,	r10,	r25
	FPADD	r3,	r10,	r3
	FPMUL	r10,	r20,	r29
	FPADD	r3,	r3,	r10
	FPMUL	r10,	r3,	r12
	ORI	r3,	r0,	0
	FPLT	r11,	r10,	r3
	FPUN	r3,	r10,	r3
	BITOR	r3,	r3,	r11
	bneid	r3,	$0BB110_12
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB110_12:
	bneid	r11,	$0BB110_23
	ADD	r3,	r0,	r0
	FPMUL	r8,	r24,	r8
	FPMUL	r9,	r23,	r9
	FPADD	r8,	r9,	r8
	FPMUL	r4,	r6,	r4
	FPADD	r4,	r8,	r4
	FPMUL	r4,	r4,	r12
	ORI	r6,	r0,	0
	FPLT	r8,	r4,	r6
	FPUN	r6,	r4,	r6
	BITOR	r8,	r6,	r8
	bneid	r8,	$0BB110_15
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB110_15:
	bneid	r6,	$0BB110_23
	NOP
	FPADD	r4,	r10,	r4
	ORI	r6,	r0,	1065353216
	FPGT	r8,	r4,	r6
	FPUN	r4,	r4,	r6
	BITOR	r6,	r4,	r8
	bneid	r6,	$0BB110_18
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB110_18:
	bneid	r4,	$0BB110_23
	NOP
	LWI	r3,	r7,	0
	FPGE	r4,	r5,	r3
	FPUN	r3,	r5,	r3
	BITOR	r3,	r3,	r4
	bneid	r3,	$0BB110_21
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB110_21:
	bneid	r4,	$0BB110_23
	ADD	r3,	r0,	r0
	SWI	r5,	r7,	0
	ADDI	r3,	r0,	1
$0BB110_23:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	48
#	.end	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
$0tmp110:
#	.size	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist, ($tmp110)-_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist

#	.globl	_ZN8syVectorC2Ei
#	.align	2
#	.type	_ZN8syVectorC2Ei,@function
#	.ent	_ZN8syVectorC2Ei        # @_ZN8syVectorC2Ei
_ZN8syVectorC2Ei:
#	.frame	r1,0,r15
#	.mask	0x0
	LOAD	r3,	r6,	0
	SWI	r3,	r5,	0
	LOAD	r3,	r6,	1
	SWI	r3,	r5,	4
	LOAD	r3,	r6,	2
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVectorC2Ei
$0tmp111:
#	.size	_ZN8syVectorC2Ei, ($tmp111)-_ZN8syVectorC2Ei

#	.globl	_ZN8syVector14LoadFromMemoryERKi
#	.align	2
#	.type	_ZN8syVector14LoadFromMemoryERKi,@function
#	.ent	_ZN8syVector14LoadFromMemoryERKi # @_ZN8syVector14LoadFromMemoryERKi
_ZN8syVector14LoadFromMemoryERKi:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	1
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LOAD	r3,	r3,	2
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVector14LoadFromMemoryERKi
$0tmp112:
#	.size	_ZN8syVector14LoadFromMemoryERKi, ($tmp112)-_ZN8syVector14LoadFromMemoryERKi

#	.globl	_ZN8syVectorC2Ev
#	.align	2
#	.type	_ZN8syVectorC2Ev,@function
#	.ent	_ZN8syVectorC2Ev        # @_ZN8syVectorC2Ev
_ZN8syVectorC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN8syVectorC2Ev
$0tmp113:
#	.size	_ZN8syVectorC2Ev, ($tmp113)-_ZN8syVectorC2Ev

#	.globl	_ZN8syVectorC2ERKfS1_S1_
#	.align	2
#	.type	_ZN8syVectorC2ERKfS1_S1_,@function
#	.ent	_ZN8syVectorC2ERKfS1_S1_ # @_ZN8syVectorC2ERKfS1_S1_
_ZN8syVectorC2ERKfS1_S1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r7,	0
	SWI	r3,	r5,	4
	LWI	r3,	r8,	0
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVectorC2ERKfS1_S1_
$0tmp114:
#	.size	_ZN8syVectorC2ERKfS1_S1_, ($tmp114)-_ZN8syVectorC2ERKfS1_S1_

#	.globl	_ZN8syVector3SetERKfS1_S1_
#	.align	2
#	.type	_ZN8syVector3SetERKfS1_S1_,@function
#	.ent	_ZN8syVector3SetERKfS1_S1_ # @_ZN8syVector3SetERKfS1_S1_
_ZN8syVector3SetERKfS1_S1_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r7,	0
	SWI	r3,	r5,	4
	LWI	r3,	r8,	0
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVector3SetERKfS1_S1_
$0tmp115:
#	.size	_ZN8syVector3SetERKfS1_S1_, ($tmp115)-_ZN8syVector3SetERKfS1_S1_

#	.globl	_ZN8syVectorC2EPKf
#	.align	2
#	.type	_ZN8syVectorC2EPKf,@function
#	.ent	_ZN8syVectorC2EPKf      # @_ZN8syVectorC2EPKf
_ZN8syVectorC2EPKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVectorC2EPKf
$0tmp116:
#	.size	_ZN8syVectorC2EPKf, ($tmp116)-_ZN8syVectorC2EPKf

#	.globl	_ZN8syVector4ZeroEv
#	.align	2
#	.type	_ZN8syVector4ZeroEv,@function
#	.ent	_ZN8syVector4ZeroEv     # @_ZN8syVector4ZeroEv
_ZN8syVector4ZeroEv:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r0,	r5,	0
	SWI	r0,	r5,	4
	rtsd	r15,	8
	SWI	r0,	r5,	8
#	.end	_ZN8syVector4ZeroEv
$0tmp117:
#	.size	_ZN8syVector4ZeroEv, ($tmp117)-_ZN8syVector4ZeroEv

#	.globl	_ZNK8syVector13LengthSquaredEv
#	.align	2
#	.type	_ZNK8syVector13LengthSquaredEv,@function
#	.ent	_ZNK8syVector13LengthSquaredEv # @_ZNK8syVector13LengthSquaredEv
_ZNK8syVector13LengthSquaredEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	4
	FPMUL	r3,	r3,	r3
	LWI	r4,	r5,	0
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r5,	8
	FPMUL	r4,	r4,	r4
	rtsd	r15,	8
	FPADD	r3,	r3,	r4
#	.end	_ZNK8syVector13LengthSquaredEv
$0tmp118:
#	.size	_ZNK8syVector13LengthSquaredEv, ($tmp118)-_ZNK8syVector13LengthSquaredEv

#	.globl	_ZNK8syVector6LengthEv
#	.align	2
#	.type	_ZNK8syVector6LengthEv,@function
#	.ent	_ZNK8syVector6LengthEv  # @_ZNK8syVector6LengthEv
_ZNK8syVector6LengthEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	4
	FPMUL	r3,	r3,	r3
	LWI	r4,	r5,	0
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r5,	8
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	rtsd	r15,	8
	FPDIV	r3,	r4,	r3
#	.end	_ZNK8syVector6LengthEv
$0tmp119:
#	.size	_ZNK8syVector6LengthEv, ($tmp119)-_ZNK8syVector6LengthEv

#	.globl	_ZN8syVector9NormalizeEv
#	.align	2
#	.type	_ZN8syVector9NormalizeEv,@function
#	.ent	_ZN8syVector9NormalizeEv # @_ZN8syVector9NormalizeEv
_ZN8syVector9NormalizeEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	4
	FPMUL	r3,	r3,	r3
	LWI	r4,	r5,	0
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r5,	8
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	LWI	r4,	r5,	0
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	0
	LWI	r4,	r5,	4
	FPDIV	r4,	r4,	r3
	SWI	r4,	r5,	4
	LWI	r4,	r5,	8
	FPDIV	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN8syVector9NormalizeEv
$0tmp120:
#	.size	_ZN8syVector9NormalizeEv, ($tmp120)-_ZN8syVector9NormalizeEv

#	.globl	_ZNK8syVector13GetNormalizedEv
#	.align	2
#	.type	_ZNK8syVector13GetNormalizedEv,@function
#	.ent	_ZNK8syVector13GetNormalizedEv # @_ZNK8syVector13GetNormalizedEv
_ZNK8syVector13GetNormalizedEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	4
	FPMUL	r3,	r3,	r3
	LWI	r4,	r6,	0
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r6,	8
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	LWI	r4,	r6,	0
	FPDIV	r7,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r6,	r6,	4
	SWI	r7,	r5,	0
	FPDIV	r6,	r6,	r3
	SWI	r6,	r5,	4
	FPDIV	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVector13GetNormalizedEv
$0tmp121:
#	.size	_ZNK8syVector13GetNormalizedEv, ($tmp121)-_ZNK8syVector13GetNormalizedEv

#	.globl	_ZNK8syVectorngEv
#	.align	2
#	.type	_ZNK8syVectorngEv,@function
#	.ent	_ZNK8syVectorngEv       # @_ZNK8syVectorngEv
_ZNK8syVectorngEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	8
	LWI	r4,	r6,	4
	LWI	r6,	r6,	0
	FPNEG	r6,	r6
	SWI	r6,	r5,	0
	FPNEG	r4,	r4
	SWI	r4,	r5,	4
	FPNEG	r3,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectorngEv
$0tmp122:
#	.size	_ZNK8syVectorngEv, ($tmp122)-_ZNK8syVectorngEv

#	.globl	_ZNK8syVector8GetOrthoEv
#	.align	2
#	.type	_ZNK8syVector8GetOrthoEv,@function
#	.ent	_ZNK8syVector8GetOrthoEv # @_ZNK8syVector8GetOrthoEv
_ZNK8syVector8GetOrthoEv:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	ORI	r4,	r0,	0
	FPGE	r7,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r7,	r4,	r7
	bneid	r7,	$0BB123_2
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB123_2:
	bneid	r4,	$0BB123_4
	ADD	r7,	r3,	r0
	FPNEG	r7,	r3
$0BB123_4:
	LWI	r4,	r6,	4
	ORI	r8,	r0,	0
	FPGE	r9,	r4,	r8
	FPUN	r8,	r4,	r8
	BITOR	r8,	r8,	r9
	bneid	r8,	$0BB123_6
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB123_6:
	bneid	r9,	$0BB123_8
	ADD	r8,	r4,	r0
	FPNEG	r8,	r4
$0BB123_8:
	LWI	r6,	r6,	8
	ORI	r9,	r0,	0
	FPGE	r10,	r6,	r9
	FPUN	r9,	r6,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB123_10
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB123_10:
	bneid	r10,	$0BB123_12
	ADD	r9,	r6,	r0
	FPNEG	r9,	r6
$0BB123_12:
	FPGE	r10,	r7,	r8
	FPUN	r11,	r7,	r8
	BITOR	r10,	r11,	r10
	bneid	r10,	$0BB123_14
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB123_14:
	ORI	r11,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r12,	$0BB123_18
	NOP
	FPLT	r7,	r7,	r9
	bneid	r7,	$0BB123_17
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB123_17:
	bneid	r12,	$0BB123_22
	ADD	r7,	r10,	r0
$0BB123_18:
	FPLT	r7,	r8,	r9
	bneid	r7,	$0BB123_20
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB123_20:
	ORI	r7,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r8,	$0BB123_22
	ADD	r11,	r10,	r0
	ORI	r7,	r0,	0
	ORI	r10,	r0,	1065353216
	ADD	r11,	r7,	r0
$0BB123_22:
	FPMUL	r8,	r6,	r7
	FPMUL	r9,	r4,	r10
	FPRSUB	r8,	r8,	r9
	SWI	r8,	r5,	0
	FPMUL	r8,	r3,	r10
	FPMUL	r6,	r6,	r11
	FPRSUB	r6,	r8,	r6
	SWI	r6,	r5,	4
	FPMUL	r4,	r4,	r11
	FPMUL	r3,	r3,	r7
	FPRSUB	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVector8GetOrthoEv
$0tmp123:
#	.size	_ZNK8syVector8GetOrthoEv, ($tmp123)-_ZNK8syVector8GetOrthoEv

#	.globl	_ZN8syVectoraSERKS_
#	.align	2
#	.type	_ZN8syVectoraSERKS_,@function
#	.ent	_ZN8syVectoraSERKS_     # @_ZN8syVectoraSERKS_
_ZN8syVectoraSERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectoraSERKS_
$0tmp124:
#	.size	_ZN8syVectoraSERKS_, ($tmp124)-_ZN8syVectoraSERKS_

#	.globl	_ZNK8syVector5CrossERKS_
#	.align	2
#	.type	_ZNK8syVector5CrossERKS_,@function
#	.ent	_ZNK8syVector5CrossERKS_ # @_ZNK8syVector5CrossERKS_
_ZNK8syVector5CrossERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	4
	LWI	r4,	r6,	8
	FPMUL	r10,	r4,	r3
	LWI	r9,	r7,	8
	LWI	r8,	r6,	4
	FPMUL	r11,	r8,	r9
	FPRSUB	r10,	r10,	r11
	LWI	r7,	r7,	0
	LWI	r6,	r6,	0
	SWI	r10,	r5,	0
	FPMUL	r9,	r6,	r9
	FPMUL	r4,	r4,	r7
	FPRSUB	r4,	r9,	r4
	SWI	r4,	r5,	4
	FPMUL	r4,	r8,	r7
	FPMUL	r3,	r6,	r3
	FPRSUB	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVector5CrossERKS_
$0tmp125:
#	.size	_ZNK8syVector5CrossERKS_, ($tmp125)-_ZNK8syVector5CrossERKS_

#	.globl	_ZNK8syVector3DotERKS_
#	.align	2
#	.type	_ZNK8syVector3DotERKS_,@function
#	.ent	_ZNK8syVector3DotERKS_  # @_ZNK8syVector3DotERKS_
_ZNK8syVector3DotERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPMUL	r3,	r4,	r3
	LWI	r4,	r6,	0
	LWI	r7,	r5,	0
	FPMUL	r4,	r7,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r5,	r5,	8
	FPMUL	r4,	r5,	r4
	rtsd	r15,	8
	FPADD	r3,	r3,	r4
#	.end	_ZNK8syVector3DotERKS_
$0tmp126:
#	.size	_ZNK8syVector3DotERKS_, ($tmp126)-_ZNK8syVector3DotERKS_

#	.globl	_ZNK8syVectorrmERKS_
#	.align	2
#	.type	_ZNK8syVectorrmERKS_,@function
#	.ent	_ZNK8syVectorrmERKS_    # @_ZNK8syVectorrmERKS_
_ZNK8syVectorrmERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPMUL	r3,	r4,	r3
	LWI	r4,	r6,	0
	LWI	r7,	r5,	0
	FPMUL	r4,	r7,	r4
	FPADD	r3,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r5,	r5,	8
	FPMUL	r4,	r5,	r4
	rtsd	r15,	8
	FPADD	r3,	r3,	r4
#	.end	_ZNK8syVectorrmERKS_
$0tmp127:
#	.size	_ZNK8syVectorrmERKS_, ($tmp127)-_ZNK8syVectorrmERKS_

#	.globl	_ZNK8syVectoreoERKS_
#	.align	2
#	.type	_ZNK8syVectoreoERKS_,@function
#	.ent	_ZNK8syVectoreoERKS_    # @_ZNK8syVectoreoERKS_
_ZNK8syVectoreoERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	4
	LWI	r4,	r6,	8
	FPMUL	r10,	r4,	r3
	LWI	r9,	r7,	8
	LWI	r8,	r6,	4
	FPMUL	r11,	r8,	r9
	FPRSUB	r10,	r10,	r11
	LWI	r7,	r7,	0
	LWI	r6,	r6,	0
	SWI	r10,	r5,	0
	FPMUL	r9,	r6,	r9
	FPMUL	r4,	r4,	r7
	FPRSUB	r4,	r9,	r4
	SWI	r4,	r5,	4
	FPMUL	r4,	r8,	r7
	FPMUL	r3,	r6,	r3
	FPRSUB	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectoreoERKS_
$0tmp128:
#	.size	_ZNK8syVectoreoERKS_, ($tmp128)-_ZNK8syVectoreoERKS_

#	.globl	_ZNK8syVectorplERKS_
#	.align	2
#	.type	_ZNK8syVectorplERKS_,@function
#	.ent	_ZNK8syVectorplERKS_    # @_ZNK8syVectorplERKS_
_ZNK8syVectorplERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPADD	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPADD	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPADD	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectorplERKS_
$0tmp129:
#	.size	_ZNK8syVectorplERKS_, ($tmp129)-_ZNK8syVectorplERKS_

#	.globl	_ZNK8syVectormiERKS_
#	.align	2
#	.type	_ZNK8syVectormiERKS_,@function
#	.ent	_ZNK8syVectormiERKS_    # @_ZNK8syVectormiERKS_
_ZNK8syVectormiERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPRSUB	r4,	r3,	r4
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPRSUB	r4,	r8,	r6
	SWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r7
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectormiERKS_
$0tmp130:
#	.size	_ZNK8syVectormiERKS_, ($tmp130)-_ZNK8syVectormiERKS_

#	.globl	_ZNK8syVectormlERKS_
#	.align	2
#	.type	_ZNK8syVectormlERKS_,@function
#	.ent	_ZNK8syVectormlERKS_    # @_ZNK8syVectormlERKS_
_ZNK8syVectormlERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPMUL	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPMUL	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPMUL	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectormlERKS_
$0tmp131:
#	.size	_ZNK8syVectormlERKS_, ($tmp131)-_ZNK8syVectormlERKS_

#	.globl	_ZNK8syVectordvERKS_
#	.align	2
#	.type	_ZNK8syVectordvERKS_,@function
#	.ent	_ZNK8syVectordvERKS_    # @_ZNK8syVectordvERKS_
_ZNK8syVectordvERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPDIV	r4,	r4,	r3
	LWI	r3,	r7,	8
	LWI	r8,	r7,	4
	LWI	r7,	r6,	8
	LWI	r6,	r6,	4
	SWI	r4,	r5,	0
	FPDIV	r4,	r6,	r8
	SWI	r4,	r5,	4
	FPDIV	r3,	r7,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectordvERKS_
$0tmp132:
#	.size	_ZNK8syVectordvERKS_, ($tmp132)-_ZNK8syVectordvERKS_

#	.globl	_ZNK8syVectorplERKf
#	.align	2
#	.type	_ZNK8syVectorplERKf,@function
#	.ent	_ZNK8syVectorplERKf     # @_ZNK8syVectorplERKf
_ZNK8syVectorplERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPADD	r7,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r6,	r6,	4
	SWI	r7,	r5,	0
	FPADD	r6,	r6,	r3
	SWI	r6,	r5,	4
	FPADD	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectorplERKf
$0tmp133:
#	.size	_ZNK8syVectorplERKf, ($tmp133)-_ZNK8syVectorplERKf

#	.globl	_ZNK8syVectormiERKf
#	.align	2
#	.type	_ZNK8syVectormiERKf,@function
#	.ent	_ZNK8syVectormiERKf     # @_ZNK8syVectormiERKf
_ZNK8syVectormiERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPRSUB	r7,	r3,	r4
	LWI	r4,	r6,	8
	LWI	r6,	r6,	4
	SWI	r7,	r5,	0
	FPRSUB	r6,	r3,	r6
	SWI	r6,	r5,	4
	FPRSUB	r3,	r3,	r4
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectormiERKf
$0tmp134:
#	.size	_ZNK8syVectormiERKf, ($tmp134)-_ZNK8syVectormiERKf

#	.globl	_ZNK8syVectormlERKf
#	.align	2
#	.type	_ZNK8syVectormlERKf,@function
#	.ent	_ZNK8syVectormlERKf     # @_ZNK8syVectormlERKf
_ZNK8syVectormlERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPMUL	r7,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r6,	r6,	4
	SWI	r7,	r5,	0
	FPMUL	r6,	r6,	r3
	SWI	r6,	r5,	4
	FPMUL	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectormlERKf
$0tmp135:
#	.size	_ZNK8syVectormlERKf, ($tmp135)-_ZNK8syVectormlERKf

#	.globl	_ZNK8syVectordvERKf
#	.align	2
#	.type	_ZNK8syVectordvERKf,@function
#	.ent	_ZNK8syVectordvERKf     # @_ZNK8syVectordvERKf
_ZNK8syVectordvERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r7,	0
	LWI	r4,	r6,	0
	FPDIV	r7,	r4,	r3
	LWI	r4,	r6,	8
	LWI	r6,	r6,	4
	SWI	r7,	r5,	0
	FPDIV	r6,	r6,	r3
	SWI	r6,	r5,	4
	FPDIV	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZNK8syVectordvERKf
$0tmp136:
#	.size	_ZNK8syVectordvERKf, ($tmp136)-_ZNK8syVectordvERKf

#	.globl	_ZN8syVectorpLERKS_
#	.align	2
#	.type	_ZN8syVectorpLERKS_,@function
#	.ent	_ZN8syVectorpLERKS_     # @_ZN8syVectorpLERKS_
_ZN8syVectorpLERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectorpLERKS_
$0tmp137:
#	.size	_ZN8syVectorpLERKS_, ($tmp137)-_ZN8syVectorpLERKS_

#	.globl	_ZN8syVectormIERKS_
#	.align	2
#	.type	_ZN8syVectormIERKS_,@function
#	.ent	_ZN8syVectormIERKS_     # @_ZN8syVectormIERKS_
_ZN8syVectormIERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectormIERKS_
$0tmp138:
#	.size	_ZN8syVectormIERKS_, ($tmp138)-_ZN8syVectormIERKS_

#	.globl	_ZN8syVectormLERKS_
#	.align	2
#	.type	_ZN8syVectormLERKS_,@function
#	.ent	_ZN8syVectormLERKS_     # @_ZN8syVectormLERKS_
_ZN8syVectormLERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectormLERKS_
$0tmp139:
#	.size	_ZN8syVectormLERKS_, ($tmp139)-_ZN8syVectormLERKS_

#	.globl	_ZN8syVectordVERKS_
#	.align	2
#	.type	_ZN8syVectordVERKS_,@function
#	.ent	_ZN8syVectordVERKS_     # @_ZN8syVectordVERKS_
_ZN8syVectordVERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	LWI	r4,	r5,	4
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	LWI	r4,	r5,	8
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectordVERKS_
$0tmp140:
#	.size	_ZN8syVectordVERKS_, ($tmp140)-_ZN8syVectordVERKS_

#	.globl	_ZN8syVectorpLERKf
#	.align	2
#	.type	_ZN8syVectorpLERKf,@function
#	.ent	_ZN8syVectorpLERKf      # @_ZN8syVectorpLERKf
_ZN8syVectorpLERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	4
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LWI	r4,	r5,	8
	FPADD	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectorpLERKf
$0tmp141:
#	.size	_ZN8syVectorpLERKf, ($tmp141)-_ZN8syVectorpLERKf

#	.globl	_ZN8syVectormIERKf
#	.align	2
#	.type	_ZN8syVectormIERKf,@function
#	.ent	_ZN8syVectormIERKf      # @_ZN8syVectormIERKf
_ZN8syVectormIERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	4
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LWI	r4,	r5,	8
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectormIERKf
$0tmp142:
#	.size	_ZN8syVectormIERKf, ($tmp142)-_ZN8syVectormIERKf

#	.globl	_ZN8syVectormLERKf
#	.align	2
#	.type	_ZN8syVectormLERKf,@function
#	.ent	_ZN8syVectormLERKf      # @_ZN8syVectormLERKf
_ZN8syVectormLERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	4
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LWI	r4,	r5,	8
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectormLERKf
$0tmp143:
#	.size	_ZN8syVectormLERKf, ($tmp143)-_ZN8syVectormLERKf

#	.globl	_ZN8syVectordVERKf
#	.align	2
#	.type	_ZN8syVectordVERKf,@function
#	.ent	_ZN8syVectordVERKf      # @_ZN8syVectordVERKf
_ZN8syVectordVERKf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	0
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	0
	LWI	r3,	r6,	0
	LWI	r4,	r5,	4
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	4
	LWI	r3,	r6,	0
	LWI	r4,	r5,	8
	FPDIV	r3,	r4,	r3
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN8syVectordVERKf
$0tmp144:
#	.size	_ZN8syVectordVERKf, ($tmp144)-_ZN8syVectordVERKf

#	.globl	_ZNK8syVectoreqERKS_
#	.align	2
#	.type	_ZNK8syVectoreqERKS_,@function
#	.ent	_ZNK8syVectoreqERKS_    # @_ZNK8syVectoreqERKS_
_ZNK8syVectoreqERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	LWI	r4,	r6,	0
	FPNE	r3,	r4,	r3
	bneid	r3,	$0BB145_2
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB145_2:
	bneid	r4,	$0BB145_8
	ADD	r3,	r0,	r0
	LWI	r3,	r5,	4
	LWI	r4,	r6,	4
	FPNE	r3,	r4,	r3
	bneid	r3,	$0BB145_5
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB145_5:
	bneid	r4,	$0BB145_8
	ADD	r3,	r0,	r0
	LWI	r3,	r5,	8
	LWI	r4,	r6,	8
	FPEQ	r4,	r4,	r3
	bneid	r4,	$0BB145_8
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB145_8:
	rtsd	r15,	8
	NOP
#	.end	_ZNK8syVectoreqERKS_
$0tmp145:
#	.size	_ZNK8syVectoreqERKS_, ($tmp145)-_ZNK8syVectoreqERKS_

#	.globl	_ZNK8syVectorneERKS_
#	.align	2
#	.type	_ZNK8syVectorneERKS_,@function
#	.ent	_ZNK8syVectorneERKS_    # @_ZNK8syVectorneERKS_
_ZNK8syVectorneERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r5,	0
	LWI	r4,	r6,	0
	FPNE	r7,	r4,	r3
	ADDI	r3,	r0,	1
	bneid	r7,	$0BB146_2
	ADD	r4,	r3,	r0
	ADDI	r4,	r0,	0
$0BB146_2:
	bneid	r4,	$0BB146_8
	NOP
	LWI	r3,	r5,	4
	LWI	r4,	r6,	4
	FPNE	r7,	r4,	r3
	ADDI	r3,	r0,	1
	bneid	r7,	$0BB146_5
	ADD	r4,	r3,	r0
	ADDI	r4,	r0,	0
$0BB146_5:
	bneid	r4,	$0BB146_8
	NOP
	LWI	r3,	r5,	8
	LWI	r4,	r6,	8
	FPNE	r4,	r4,	r3
	bneid	r4,	$0BB146_8
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB146_8:
	rtsd	r15,	8
	NOP
#	.end	_ZNK8syVectorneERKS_
$0tmp146:
#	.size	_ZNK8syVectorneERKS_, ($tmp146)-_ZNK8syVectorneERKS_

#	.globl	main
#	.align	2
#	.type	main,@function
#	.ent	main                    # @main
main:
#	.frame	r1,4,r15
#	.mask	0x8000
	ADDI	r1,	r1,	-4
	SWI	r15,	r1,	0
	brlid	r15,	_Z9trax_mainv
	NOP
	ADD	r3,	r0,	r0
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	main
$0tmp147:
#	.size	main, ($tmp147)-main

#	.globl	_Z6printfPKcz
#	.align	2
#	.type	_Z6printfPKcz,@function
#	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
#	.frame	r1,4,r15
#	.mask	0x0
	ADDI	r1,	r1,	-4
	SWI	r10,	r1,	28
	SWI	r9,	r1,	24
	SWI	r8,	r1,	20
	SWI	r7,	r1,	16
	SWI	r6,	r1,	12
	SWI	r5,	r1,	8
	ADDI	r3,	r1,	8
	PRINTF	r3
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_Z6printfPKcz
$0tmp148:
#	.size	_Z6printfPKcz, ($tmp148)-_Z6printfPKcz


#	.globl	_ZN5ColorC1Ei
#_ZN5ColorC1Ei = _ZN5ColorC2Ei
#	.globl	_ZN5ColorC1Ev
#_ZN5ColorC1Ev = _ZN5ColorC2Ev
#	.globl	_ZN5ColorC1ERKS_
#_ZN5ColorC1ERKS_ = _ZN5ColorC2ERKS_
#	.globl	_ZN5ColorC1ERKfS1_S1_
#_ZN5ColorC1ERKfS1_S1_ = _ZN5ColorC2ERKfS1_S1_
#	.globl	_ZN9HitRecordC1Ev
#_ZN9HitRecordC1Ev = _ZN9HitRecordC2Ev
#	.globl	_ZN7HitDistC1Ev
#_ZN7HitDistC1Ev = _ZN7HitDistC2Ev
#	.globl	_ZN5ImageC1ERKiS1_S1_
#_ZN5ImageC1ERKiS1_S1_ = _ZN5ImageC2ERKiS1_S1_
#	.globl	_ZN13PinholeCameraC1EiRKiS1_
#_ZN13PinholeCameraC1EiRKiS1_ = _ZN13PinholeCameraC2EiRKiS1_
#	.globl	_ZN13PinholeCameraC1Ev
#_ZN13PinholeCameraC1Ev = _ZN13PinholeCameraC2Ev
#	.globl	_ZN13PinholeCameraC1ERK8syVectorS2_S2_RKfRKiS6_
#_ZN13PinholeCameraC1ERK8syVectorS2_S2_RKfRKiS6_ = _ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
#	.globl	_ZN10PointLightC1EiRK5Color
#_ZN10PointLightC1EiRK5Color = _ZN10PointLightC2EiRK5Color
#	.globl	_ZN10PointLightC1Ev
#_ZN10PointLightC1Ev = _ZN10PointLightC2Ev
#	.globl	_ZN10PointLightC1ERK8syVectorRK5Color
#_ZN10PointLightC1ERK8syVectorRK5Color = _ZN10PointLightC2ERK8syVectorRK5Color
#	.globl	_ZN3RayC1Ev
#_ZN3RayC1Ev = _ZN3RayC2Ev
#	.globl	_ZN3RayC1E8syVectorS0_
#_ZN3RayC1E8syVectorS0_ = _ZN3RayC2E8syVectorS0_
#	.globl	_ZN3RayC1ERKS_
#_ZN3RayC1ERKS_ = _ZN3RayC2ERKS_
#	.globl	_ZN5syBoxC1Ev
#_ZN5syBoxC1Ev = _ZN5syBoxC2Ev
#	.globl	_ZN5syBoxC1ERK8syVectorS2_
#_ZN5syBoxC1ERK8syVectorS2_ = _ZN5syBoxC2ERK8syVectorS2_
#	.globl	_ZN9syBVHNodeC1Ei
#_ZN9syBVHNodeC1Ei = _ZN9syBVHNodeC2Ei
#	.globl	_ZN8syMatrixC1Ev
#_ZN8syMatrixC1Ev = _ZN8syMatrixC2Ev
#	.globl	_ZN8sySphereC1Ev
#_ZN8sySphereC1Ev = _ZN8sySphereC2Ev
#	.globl	_ZN8sySphereC1ERK8syVectorRKfRKiS6_
#_ZN8sySphereC1ERK8syVectorRKfRKiS6_ = _ZN8sySphereC2ERK8syVectorRKfRKiS6_
#	.globl	_ZN10syTriangleC1Ei
#_ZN10syTriangleC1Ei = _ZN10syTriangleC2Ei
#	.globl	_ZN10syTriangleC1Ev
#_ZN10syTriangleC1Ev = _ZN10syTriangleC2Ev
#	.globl	_ZN10syTriangleC1ERKS_
#_ZN10syTriangleC1ERKS_ = _ZN10syTriangleC2ERKS_
#	.globl	_ZN10syTriangleC1ERK8syVectorS2_S2_
#_ZN10syTriangleC1ERK8syVectorS2_S2_ = _ZN10syTriangleC2ERK8syVectorS2_S2_
#	.globl	_ZN8syVectorC1Ei
#_ZN8syVectorC1Ei = _ZN8syVectorC2Ei
#	.globl	_ZN8syVectorC1Ev
#_ZN8syVectorC1Ev = _ZN8syVectorC2Ev
#	.globl	_ZN8syVectorC1ERKfS1_S1_
#_ZN8syVectorC1ERKfS1_S1_ = _ZN8syVectorC2ERKfS1_S1_
#	.globl	_ZN8syVectorC1EPKf
#_ZN8syVectorC1EPKf = _ZN8syVectorC2EPKf
#	.file	"rt.bc"
#	.text
#	.globl	_Z6memsetPvij
#	.align	2
#	.type	_Z6memsetPvij,@function
#	.ent	_Z6memsetPvij           # @_Z6memsetPvij
memset:
#	.frame	r1,12,r15
#	.mask	0x0
	ADDI	r1,	r1,	-12
	SWI	r5,	r1,	16
	SWI	r6,	r1,	20
	SWI	r7,	r1,	24
	beqid	r7,	$1BB0_4
	NOP
	LWI	r3,	r1,	16
	SWI	r3,	r1,	4
	brid	$1BB0_2
	SWI	r0,	r1,	8
$1BB0_3:
	LWI	r3,	r1,	4
	LWI	r4,	r1,	8
	LWI	r5,	r1,	20
	sb	r5,	r3,	r4
	LWI	r3,	r1,	8
	ADDI	r3,	r3,	1
	SWI	r3,	r1,	8
$1BB0_2:
	LWI	r3,	r1,	8
	LWI	r4,	r1,	24
	CMPU	r3,	r4,	r3
	bltid	r3,	$1BB0_3
	NOP
$1BB0_4:
	LWI	r3,	r1,	16
	SWI	r3,	r1,	0
	LWI	r3,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_Z6memsetPvij
$1tmp0:
#	.size	_Z6memsetPvij, ($tmp0)-_Z6memsetPvij


#	.file	"rt.bc"
#	.text
#	.globl	_Z6memcpyPvPKvj
#	.align	2
#	.type	_Z6memcpyPvPKvj,@function
#	.ent	_Z6memcpyPvPKvj         # @_Z6memcpyPvPKvj
memcpy:
#	.frame	r1,0,r15
#	.mask	0x0
	beqid	r7,	$2BB0_3
	NOP
	ADD	r3,	r0,	r0
$2BB0_2:
	lbu	r4,	r6,	r3
	sb	r4,	r5,	r3
	ADDI	r3,	r3,	1
	CMPU	r4,	r7,	r3
	bltid	r4,	$2BB0_2
	NOP
$2BB0_3:
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_Z6memcpyPvPKvj
$2tmp0:
#	.size	_Z6memcpyPvPKvj, ($tmp0)-_Z6memcpyPvPKvj


