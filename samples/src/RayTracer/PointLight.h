#ifndef _PLight_H
#define _PLight_H

#include "trax.hpp"
#include "syVector.h"
#include "Color.h"


class PointLight {
private:
	syVector position;
	Color intensity;

public:
	// load from the memory
	PointLight(int _addr, const Color &_c);
	// 
	PointLight();
	PointLight(const syVector &_p, const Color &_c);

	//
	void LoadFromMemory(const int &_addr);
	void SetIntensity(const Color &_c);
	//
	const syVector& Position() const;
	const Color& Intensity() const;
	syVector Direction(const syVector &_p) const;
};

#endif