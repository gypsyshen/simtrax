	.file	"rt.bc"
	.text
	.globl	_ZN5ColorC2Ei
	.align	2
	.type	_ZN5ColorC2Ei,@function
	.ent	_ZN5ColorC2Ei           # @_ZN5ColorC2Ei
_ZN5ColorC2Ei:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5ColorC2Ei
$tmp0:
	.size	_ZN5ColorC2Ei, ($tmp0)-_ZN5ColorC2Ei

	.globl	_ZN5Color14LoadFromMemoryERKi
	.align	2
	.type	_ZN5Color14LoadFromMemoryERKi,@function
	.ent	_ZN5Color14LoadFromMemoryERKi # @_ZN5Color14LoadFromMemoryERKi
_ZN5Color14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5Color14LoadFromMemoryERKi
$tmp1:
	.size	_ZN5Color14LoadFromMemoryERKi, ($tmp1)-_ZN5Color14LoadFromMemoryERKi

	.globl	_ZN5ColorC2Ev
	.align	2
	.type	_ZN5ColorC2Ev,@function
	.ent	_ZN5ColorC2Ev           # @_ZN5ColorC2Ev
_ZN5ColorC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 0
	SWI       r3, r5, 4
	rtsd      r15, 8
	SWI       r3, r5, 8
	.end	_ZN5ColorC2Ev
$tmp2:
	.size	_ZN5ColorC2Ev, ($tmp2)-_ZN5ColorC2Ev

	.globl	_ZN5ColorC2ERKS_
	.align	2
	.type	_ZN5ColorC2ERKS_,@function
	.ent	_ZN5ColorC2ERKS_        # @_ZN5ColorC2ERKS_
_ZN5ColorC2ERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5ColorC2ERKS_
$tmp3:
	.size	_ZN5ColorC2ERKS_, ($tmp3)-_ZN5ColorC2ERKS_

	.globl	_ZN5ColorC2ERKfS1_S1_
	.align	2
	.type	_ZN5ColorC2ERKfS1_S1_,@function
	.ent	_ZN5ColorC2ERKfS1_S1_   # @_ZN5ColorC2ERKfS1_S1_
_ZN5ColorC2ERKfS1_S1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r7, 0
	SWI        r3, r5, 4
	LWI        r3, r8, 0
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5ColorC2ERKfS1_S1_
$tmp4:
	.size	_ZN5ColorC2ERKfS1_S1_, ($tmp4)-_ZN5ColorC2ERKfS1_S1_

	.globl	_ZNK5ColorplERKS_
	.align	2
	.type	_ZNK5ColorplERKS_,@function
	.ent	_ZNK5ColorplERKS_       # @_ZNK5ColorplERKS_
_ZNK5ColorplERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPADD      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPADD      r4, r6, r8
	SWI        r4, r5, 4
	FPADD      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColorplERKS_
$tmp5:
	.size	_ZNK5ColorplERKS_, ($tmp5)-_ZNK5ColorplERKS_

	.globl	_ZNK5ColormiERKS_
	.align	2
	.type	_ZNK5ColormiERKS_,@function
	.ent	_ZNK5ColormiERKS_       # @_ZNK5ColormiERKS_
_ZNK5ColormiERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPRSUB     r4, r3, r4
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPRSUB     r4, r8, r6
	SWI        r4, r5, 4
	FPRSUB     r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColormiERKS_
$tmp6:
	.size	_ZNK5ColormiERKS_, ($tmp6)-_ZNK5ColormiERKS_

	.globl	_ZNK5ColormlERKS_
	.align	2
	.type	_ZNK5ColormlERKS_,@function
	.ent	_ZNK5ColormlERKS_       # @_ZNK5ColormlERKS_
_ZNK5ColormlERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPMUL      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPMUL      r4, r6, r8
	SWI        r4, r5, 4
	FPMUL      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColormlERKS_
$tmp7:
	.size	_ZNK5ColormlERKS_, ($tmp7)-_ZNK5ColormlERKS_

	.globl	_ZNK5ColordvERKS_
	.align	2
	.type	_ZNK5ColordvERKS_,@function
	.ent	_ZNK5ColordvERKS_       # @_ZNK5ColordvERKS_
_ZNK5ColordvERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPDIV      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPDIV      r4, r6, r8
	SWI        r4, r5, 4
	FPDIV      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColordvERKS_
$tmp8:
	.size	_ZNK5ColordvERKS_, ($tmp8)-_ZNK5ColordvERKS_

	.globl	_ZNK5ColorplEf
	.align	2
	.type	_ZNK5ColorplEf,@function
	.ent	_ZNK5ColorplEf          # @_ZNK5ColorplEf
_ZNK5ColorplEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	FPADD      r4, r3, r7
	LWI        r3, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPADD      r4, r6, r7
	SWI        r4, r5, 4
	FPADD      r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColorplEf
$tmp9:
	.size	_ZNK5ColorplEf, ($tmp9)-_ZNK5ColorplEf

	.globl	_ZNK5ColormiEf
	.align	2
	.type	_ZNK5ColormiEf,@function
	.ent	_ZNK5ColormiEf          # @_ZNK5ColormiEf
_ZNK5ColormiEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	FPRSUB     r4, r7, r3
	LWI        r3, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPRSUB     r4, r7, r6
	SWI        r4, r5, 4
	FPRSUB     r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColormiEf
$tmp10:
	.size	_ZNK5ColormiEf, ($tmp10)-_ZNK5ColormiEf

	.globl	_ZNK5ColormlEf
	.align	2
	.type	_ZNK5ColormlEf,@function
	.ent	_ZNK5ColormlEf          # @_ZNK5ColormlEf
_ZNK5ColormlEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	FPMUL      r4, r3, r7
	LWI        r3, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPMUL      r4, r6, r7
	SWI        r4, r5, 4
	FPMUL      r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColormlEf
$tmp11:
	.size	_ZNK5ColormlEf, ($tmp11)-_ZNK5ColormlEf

	.globl	_ZNK5ColordvEf
	.align	2
	.type	_ZNK5ColordvEf,@function
	.ent	_ZNK5ColordvEf          # @_ZNK5ColordvEf
_ZNK5ColordvEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	FPDIV      r4, r3, r7
	LWI        r3, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPDIV      r4, r6, r7
	SWI        r4, r5, 4
	FPDIV      r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5ColordvEf
$tmp12:
	.size	_ZNK5ColordvEf, ($tmp12)-_ZNK5ColordvEf

	.globl	_ZN5ColorpLERKS_
	.align	2
	.type	_ZN5ColorpLERKS_,@function
	.ent	_ZN5ColorpLERKS_        # @_ZN5ColorpLERKS_
_ZN5ColorpLERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPADD      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPADD      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPADD      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColorpLERKS_
$tmp13:
	.size	_ZN5ColorpLERKS_, ($tmp13)-_ZN5ColorpLERKS_

	.globl	_ZN5ColormIERKS_
	.align	2
	.type	_ZN5ColormIERKS_,@function
	.ent	_ZN5ColormIERKS_        # @_ZN5ColormIERKS_
_ZN5ColormIERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColormIERKS_
$tmp14:
	.size	_ZN5ColormIERKS_, ($tmp14)-_ZN5ColormIERKS_

	.globl	_ZN5ColormLERKS_
	.align	2
	.type	_ZN5ColormLERKS_,@function
	.ent	_ZN5ColormLERKS_        # @_ZN5ColormLERKS_
_ZN5ColormLERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPMUL      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPMUL      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPMUL      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColormLERKS_
$tmp15:
	.size	_ZN5ColormLERKS_, ($tmp15)-_ZN5ColormLERKS_

	.globl	_ZN5ColordVERKS_
	.align	2
	.type	_ZN5ColordVERKS_,@function
	.ent	_ZN5ColordVERKS_        # @_ZN5ColordVERKS_
_ZN5ColordVERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPDIV      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPDIV      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPDIV      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColordVERKS_
$tmp16:
	.size	_ZN5ColordVERKS_, ($tmp16)-_ZN5ColordVERKS_

	.globl	_ZN5ColorpLEf
	.align	2
	.type	_ZN5ColorpLEf,@function
	.ent	_ZN5ColorpLEf           # @_ZN5ColorpLEf
_ZN5ColorpLEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	FPADD      r3, r3, r6
	SWI        r3, r5, 0
	LWI        r3, r5, 4
	FPADD      r3, r3, r6
	SWI        r3, r5, 4
	LWI        r3, r5, 8
	FPADD      r3, r3, r6
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColorpLEf
$tmp17:
	.size	_ZN5ColorpLEf, ($tmp17)-_ZN5ColorpLEf

	.globl	_ZN5ColormIEf
	.align	2
	.type	_ZN5ColormIEf,@function
	.ent	_ZN5ColormIEf           # @_ZN5ColormIEf
_ZN5ColormIEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	FPRSUB     r3, r6, r3
	SWI        r3, r5, 0
	LWI        r3, r5, 4
	FPRSUB     r3, r6, r3
	SWI        r3, r5, 4
	LWI        r3, r5, 8
	FPRSUB     r3, r6, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColormIEf
$tmp18:
	.size	_ZN5ColormIEf, ($tmp18)-_ZN5ColormIEf

	.globl	_ZN5ColormLEf
	.align	2
	.type	_ZN5ColormLEf,@function
	.ent	_ZN5ColormLEf           # @_ZN5ColormLEf
_ZN5ColormLEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	FPMUL      r3, r3, r6
	SWI        r3, r5, 0
	LWI        r3, r5, 4
	FPMUL      r3, r3, r6
	SWI        r3, r5, 4
	LWI        r3, r5, 8
	FPMUL      r3, r3, r6
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColormLEf
$tmp19:
	.size	_ZN5ColormLEf, ($tmp19)-_ZN5ColormLEf

	.globl	_ZN5ColordVEf
	.align	2
	.type	_ZN5ColordVEf,@function
	.ent	_ZN5ColordVEf           # @_ZN5ColordVEf
_ZN5ColordVEf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	FPDIV      r3, r3, r6
	SWI        r3, r5, 0
	LWI        r3, r5, 4
	FPDIV      r3, r3, r6
	SWI        r3, r5, 4
	LWI        r3, r5, 8
	FPDIV      r3, r3, r6
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColordVEf
$tmp20:
	.size	_ZN5ColordVEf, ($tmp20)-_ZN5ColordVEf

	.globl	_ZNK5Color5ClampEv
	.align	2
	.type	_ZNK5Color5ClampEv,@function
	.ent	_ZNK5Color5ClampEv      # @_ZNK5Color5ClampEv
_ZNK5Color5ClampEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 0
	SWI       r3, r5, 4
	SWI       r3, r5, 8
	LWI        r3, r6, 0
	ORI       r4, r0, 0
	FPLT   r8, r3, r4
	bneid     r8, ($BB21_2)
	ADDI      r7, r0, 1
# BB#1:                                 # %entry
	ADDI      r7, r0, 0
$BB21_2:                                # %entry
	bneid     r7, ($BB21_7)
	NOP    
# BB#3:                                 # %cond.false
	ORI       r4, r0, 1065353216
	FPGT   r8, r3, r4
	bneid     r8, ($BB21_5)
	ADDI      r7, r0, 1
# BB#4:                                 # %cond.false
	ADDI      r7, r0, 0
$BB21_5:                                # %cond.false
	bneid     r7, ($BB21_7)
	NOP    
# BB#6:                                 # %cond.false5
	ADD      r4, r3, r0
$BB21_7:                                # %cond.end7
	SWI        r4, r5, 0
	LWI        r3, r6, 4
	ORI       r4, r0, 0
	FPLT   r8, r3, r4
	bneid     r8, ($BB21_9)
	ADDI      r7, r0, 1
# BB#8:                                 # %cond.end7
	ADDI      r7, r0, 0
$BB21_9:                                # %cond.end7
	bneid     r7, ($BB21_14)
	NOP    
# BB#10:                                # %cond.false12
	ORI       r4, r0, 1065353216
	FPGT   r8, r3, r4
	bneid     r8, ($BB21_12)
	ADDI      r7, r0, 1
# BB#11:                                # %cond.false12
	ADDI      r7, r0, 0
$BB21_12:                               # %cond.false12
	bneid     r7, ($BB21_14)
	NOP    
# BB#13:                                # %cond.false16
	ADD      r4, r3, r0
$BB21_14:                               # %cond.end20
	SWI        r4, r5, 4
	LWI        r3, r6, 8
	ORI       r4, r0, 0
	FPLT   r7, r3, r4
	bneid     r7, ($BB21_16)
	ADDI      r6, r0, 1
# BB#15:                                # %cond.end20
	ADDI      r6, r0, 0
$BB21_16:                               # %cond.end20
	bneid     r6, ($BB21_21)
	NOP    
# BB#17:                                # %cond.false25
	ORI       r4, r0, 1065353216
	FPGT   r7, r3, r4
	bneid     r7, ($BB21_19)
	ADDI      r6, r0, 1
# BB#18:                                # %cond.false25
	ADDI      r6, r0, 0
$BB21_19:                               # %cond.false25
	bneid     r6, ($BB21_21)
	NOP    
# BB#20:                                # %cond.false29
	ADD      r4, r3, r0
$BB21_21:                               # %cond.end33
	rtsd      r15, 8
	SWI        r4, r5, 8
	.end	_ZNK5Color5ClampEv
$tmp21:
	.size	_ZNK5Color5ClampEv, ($tmp21)-_ZNK5Color5ClampEv

	.globl	_ZNK5Color1REv
	.align	2
	.type	_ZNK5Color1REv,@function
	.ent	_ZNK5Color1REv          # @_ZNK5Color1REv
_ZNK5Color1REv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZNK5Color1REv
$tmp22:
	.size	_ZNK5Color1REv, ($tmp22)-_ZNK5Color1REv

	.globl	_ZNK5Color1GEv
	.align	2
	.type	_ZNK5Color1GEv,@function
	.ent	_ZNK5Color1GEv          # @_ZNK5Color1GEv
_ZNK5Color1GEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADDI      r3, r5, 4
	.end	_ZNK5Color1GEv
$tmp23:
	.size	_ZNK5Color1GEv, ($tmp23)-_ZNK5Color1GEv

	.globl	_ZNK5Color1BEv
	.align	2
	.type	_ZNK5Color1BEv,@function
	.ent	_ZNK5Color1BEv          # @_ZNK5Color1BEv
_ZNK5Color1BEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADDI      r3, r5, 8
	.end	_ZNK5Color1BEv
$tmp24:
	.size	_ZNK5Color1BEv, ($tmp24)-_ZNK5Color1BEv

	.globl	_ZNK5Color5ClampERKS_S1_
	.align	2
	.type	_ZNK5Color5ClampERKS_S1_,@function
	.ent	_ZNK5Color5ClampERKS_S1_ # @_ZNK5Color5ClampERKS_S1_
_ZNK5Color5ClampERKS_S1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 0
	SWI       r3, r5, 4
	SWI       r3, r5, 8
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPLT   r10, r4, r3
	bneid     r10, ($BB25_2)
	ADDI      r9, r0, 1
# BB#1:                                 # %entry
	ADDI      r9, r0, 0
$BB25_2:                                # %entry
	bneid     r9, ($BB25_7)
	NOP    
# BB#3:                                 # %cond.false
	LWI        r3, r8, 0
	FPGT   r10, r4, r3
	bneid     r10, ($BB25_5)
	ADDI      r9, r0, 1
# BB#4:                                 # %cond.false
	ADDI      r9, r0, 0
$BB25_5:                                # %cond.false
	bneid     r9, ($BB25_7)
	NOP    
# BB#6:                                 # %cond.false9
	ADD      r3, r4, r0
$BB25_7:                                # %cond.end11
	SWI        r3, r5, 0
	LWI        r3, r7, 4
	LWI        r4, r6, 4
	FPLT   r10, r4, r3
	bneid     r10, ($BB25_9)
	ADDI      r9, r0, 1
# BB#8:                                 # %cond.end11
	ADDI      r9, r0, 0
$BB25_9:                                # %cond.end11
	bneid     r9, ($BB25_14)
	NOP    
# BB#10:                                # %cond.false18
	LWI        r3, r8, 4
	FPGT   r10, r4, r3
	bneid     r10, ($BB25_12)
	ADDI      r9, r0, 1
# BB#11:                                # %cond.false18
	ADDI      r9, r0, 0
$BB25_12:                               # %cond.false18
	bneid     r9, ($BB25_14)
	NOP    
# BB#13:                                # %cond.false24
	ADD      r3, r4, r0
$BB25_14:                               # %cond.end28
	SWI        r3, r5, 4
	LWI        r3, r7, 8
	LWI        r4, r6, 8
	FPLT   r7, r4, r3
	bneid     r7, ($BB25_16)
	ADDI      r6, r0, 1
# BB#15:                                # %cond.end28
	ADDI      r6, r0, 0
$BB25_16:                               # %cond.end28
	bneid     r6, ($BB25_21)
	NOP    
# BB#17:                                # %cond.false35
	LWI        r3, r8, 8
	FPGT   r7, r4, r3
	bneid     r7, ($BB25_19)
	ADDI      r6, r0, 1
# BB#18:                                # %cond.false35
	ADDI      r6, r0, 0
$BB25_19:                               # %cond.false35
	bneid     r6, ($BB25_21)
	NOP    
# BB#20:                                # %cond.false41
	ADD      r3, r4, r0
$BB25_21:                               # %cond.end45
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK5Color5ClampERKS_S1_
$tmp25:
	.size	_ZNK5Color5ClampERKS_S1_, ($tmp25)-_ZNK5Color5ClampERKS_S1_

	.globl	_ZN9HitRecordC2Ev
	.align	2
	.type	_ZN9HitRecordC2Ev,@function
	.ent	_ZN9HitRecordC2Ev       # @_ZN9HitRecordC2Ev
_ZN9HitRecordC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1203982336
	SWI       r3, r5, 0
	ADDI      r3, r0, -1
	SWI       r3, r5, 4
	rtsd      r15, 8
	SWI       r3, r5, 8
	.end	_ZN9HitRecordC2Ev
$tmp26:
	.size	_ZN9HitRecordC2Ev, ($tmp26)-_ZN9HitRecordC2Ev

	.globl	_ZN9HitRecord4InitEv
	.align	2
	.type	_ZN9HitRecord4InitEv,@function
	.ent	_ZN9HitRecord4InitEv    # @_ZN9HitRecord4InitEv
_ZN9HitRecord4InitEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1203982336
	SWI       r3, r5, 0
	ADDI      r3, r0, -1
	SWI       r3, r5, 4
	rtsd      r15, 8
	SWI       r3, r5, 8
	.end	_ZN9HitRecord4InitEv
$tmp27:
	.size	_ZN9HitRecord4InitEv, ($tmp27)-_ZN9HitRecord4InitEv

	.globl	_ZNK9HitRecord3HitEv
	.align	2
	.type	_ZNK9HitRecord3HitEv,@function
	.ent	_ZNK9HitRecord3HitEv    # @_ZNK9HitRecord3HitEv
_ZNK9HitRecord3HitEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	ORI       r4, r0, 1203982336
	FPNE   r4, r3, r4
	bneid     r4, ($BB28_2)
	ADDI      r3, r0, 1
# BB#1:                                 # %entry
	ADDI      r3, r0, 0
$BB28_2:                                # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZNK9HitRecord3HitEv
$tmp28:
	.size	_ZNK9HitRecord3HitEv, ($tmp28)-_ZNK9HitRecord3HitEv

	.globl	_ZN9HitRecordaSERKS_
	.align	2
	.type	_ZN9HitRecordaSERKS_,@function
	.ent	_ZN9HitRecordaSERKS_    # @_ZN9HitRecordaSERKS_
_ZN9HitRecordaSERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 4
	SWI       r3, r5, 4
	LWI       r3, r6, 8
	SWI       r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN9HitRecordaSERKS_
$tmp29:
	.size	_ZN9HitRecordaSERKS_, ($tmp29)-_ZN9HitRecordaSERKS_

	.globl	_ZN7HitDist4InitEv
	.align	2
	.type	_ZN7HitDist4InitEv,@function
	.ent	_ZN7HitDist4InitEv      # @_ZN7HitDist4InitEv
_ZN7HitDist4InitEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1203982336
	rtsd      r15, 8
	SWI       r3, r5, 0
	.end	_ZN7HitDist4InitEv
$tmp30:
	.size	_ZN7HitDist4InitEv, ($tmp30)-_ZN7HitDist4InitEv

	.globl	_ZN7HitDistC2Ev
	.align	2
	.type	_ZN7HitDistC2Ev,@function
	.ent	_ZN7HitDistC2Ev         # @_ZN7HitDistC2Ev
_ZN7HitDistC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1203982336
	rtsd      r15, 8
	SWI       r3, r5, 0
	.end	_ZN7HitDistC2Ev
$tmp31:
	.size	_ZN7HitDistC2Ev, ($tmp31)-_ZN7HitDistC2Ev

	.globl	_ZN5ImageC2ERKiS1_S1_
	.align	2
	.type	_ZN5ImageC2ERKiS1_S1_,@function
	.ent	_ZN5ImageC2ERKiS1_S1_   # @_ZN5ImageC2ERKiS1_S1_
_ZN5ImageC2ERKiS1_S1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	SWI       r3, r5, 0
	LWI       r3, r7, 0
	SWI       r3, r5, 4
	LWI       r3, r8, 0
	rtsd      r15, 8
	SWI       r3, r5, 8
	.end	_ZN5ImageC2ERKiS1_S1_
$tmp32:
	.size	_ZN5ImageC2ERKiS1_S1_, ($tmp32)-_ZN5ImageC2ERKiS1_S1_

	.globl	_ZN5Image3SetERKiS1_RK5Color
	.align	2
	.type	_ZN5Image3SetERKiS1_RK5Color,@function
	.ent	_ZN5Image3SetERKiS1_RK5Color # @_ZN5Image3SetERKiS1_RK5Color
_ZN5Image3SetERKiS1_RK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r4, r8, 0
	ORI       r3, r0, 0
	FPLT   r10, r4, r3
	bneid     r10, ($BB33_2)
	ADDI      r9, r0, 1
# BB#1:                                 # %entry
	ADDI      r9, r0, 0
$BB33_2:                                # %entry
	bneid     r9, ($BB33_7)
	NOP    
# BB#3:                                 # %cond.false.i
	ORI       r3, r0, 1065353216
	FPGT   r10, r4, r3
	bneid     r10, ($BB33_5)
	ADDI      r9, r0, 1
# BB#4:                                 # %cond.false.i
	ADDI      r9, r0, 0
$BB33_5:                                # %cond.false.i
	bneid     r9, ($BB33_7)
	NOP    
# BB#6:                                 # %cond.false5.i
	ADD      r3, r4, r0
$BB33_7:                                # %cond.end7.i
	LWI        r9, r8, 4
	ORI       r4, r0, 0
	FPLT   r11, r9, r4
	bneid     r11, ($BB33_9)
	ADDI      r10, r0, 1
# BB#8:                                 # %cond.end7.i
	ADDI      r10, r0, 0
$BB33_9:                                # %cond.end7.i
	bneid     r10, ($BB33_14)
	NOP    
# BB#10:                                # %cond.false12.i
	ORI       r4, r0, 1065353216
	FPGT   r11, r9, r4
	bneid     r11, ($BB33_12)
	ADDI      r10, r0, 1
# BB#11:                                # %cond.false12.i
	ADDI      r10, r0, 0
$BB33_12:                               # %cond.false12.i
	bneid     r10, ($BB33_14)
	NOP    
# BB#13:                                # %cond.false16.i
	ADD      r4, r9, r0
$BB33_14:                               # %cond.end20.i
	LWI        r9, r8, 8
	ORI       r8, r0, 0
	FPLT   r11, r9, r8
	bneid     r11, ($BB33_16)
	ADDI      r10, r0, 1
# BB#15:                                # %cond.end20.i
	ADDI      r10, r0, 0
$BB33_16:                               # %cond.end20.i
	bneid     r10, ($BB33_21)
	NOP    
# BB#17:                                # %cond.false25.i
	ORI       r8, r0, 1065353216
	FPGT   r11, r9, r8
	bneid     r11, ($BB33_19)
	ADDI      r10, r0, 1
# BB#18:                                # %cond.false25.i
	ADDI      r10, r0, 0
$BB33_19:                               # %cond.false25.i
	bneid     r10, ($BB33_21)
	NOP    
# BB#20:                                # %cond.false29.i
	ADD      r8, r9, r0
$BB33_21:                               # %_ZNK5Color5ClampEv.exit
	LWI       r9, r6, 0
	LWI       r10, r5, 0
	MUL       r9, r10, r9
	LWI       r10, r7, 0
	ADD      r9, r9, r10
	MULI      r9, r9, 3
	LWI       r10, r5, 8
	ADD      r9, r9, r10
	STORE     r9, r3, 0
	LWI       r3, r6, 0
	LWI       r9, r5, 0
	MUL       r3, r9, r3
	LWI       r9, r7, 0
	ADD      r3, r3, r9
	MULI      r3, r3, 3
	LWI       r9, r5, 8
	ADD      r3, r3, r9
	STORE     r3, r4, 1
	LWI       r3, r6, 0
	LWI       r4, r5, 0
	MUL       r3, r4, r3
	LWI       r4, r7, 0
	ADD      r3, r3, r4
	MULI      r3, r3, 3
	LWI       r4, r5, 8
	ADD      r3, r3, r4
	STORE     r3, r8, 2
	rtsd      r15, 8
	NOP    
	.end	_ZN5Image3SetERKiS1_RK5Color
$tmp33:
	.size	_ZN5Image3SetERKiS1_RK5Color, ($tmp33)-_ZN5Image3SetERKiS1_RK5Color

	.globl	_ZN10Lambertian10SetDiffuseERK5Color
	.align	2
	.type	_ZN10Lambertian10SetDiffuseERK5Color,@function
	.ent	_ZN10Lambertian10SetDiffuseERK5Color # @_ZN10Lambertian10SetDiffuseERK5Color
_ZN10Lambertian10SetDiffuseERK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 8
	SWI       r3, r5, 8
	LWI       r3, r6, 4
	SWI       r3, r5, 4
	LWI       r3, r6, 0
	rtsd      r15, 8
	SWI       r3, r5, 0
	.end	_ZN10Lambertian10SetDiffuseERK5Color
$tmp34:
	.size	_ZN10Lambertian10SetDiffuseERK5Color, ($tmp34)-_ZN10Lambertian10SetDiffuseERK5Color

	.globl	_ZNK10Lambertian7DiffuseEv
	.align	2
	.type	_ZNK10Lambertian7DiffuseEv,@function
	.ent	_ZNK10Lambertian7DiffuseEv # @_ZNK10Lambertian7DiffuseEv
_ZNK10Lambertian7DiffuseEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZNK10Lambertian7DiffuseEv
$tmp35:
	.size	_ZNK10Lambertian7DiffuseEv, ($tmp35)-_ZNK10Lambertian7DiffuseEv

	.globl	_ZN13PinholeCameraC2EiRKiS1_
	.align	2
	.type	_ZN13PinholeCameraC2EiRKiS1_,@function
	.ent	_ZN13PinholeCameraC2EiRKiS1_ # @_ZN13PinholeCameraC2EiRKiS1_
_ZN13PinholeCameraC2EiRKiS1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r7, 0
	FPCONV       r4, r3
	ORI       r3, r0, 1065353216
	FPDIV      r4, r3, r4
	SWI        r4, r5, 64
	LWI       r4, r8, 0
	FPCONV       r4, r4
	FPDIV      r3, r3, r4
	SWI        r3, r5, 68
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	SWI        r3, r5, 8
	ADDI      r3, r6, 9
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	ADDI      r3, r6, 12
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	SWI        r3, r5, 32
	ADDI      r3, r6, 15
	LOAD      r4, r3, 0
	SWI        r4, r5, 40
	LOAD      r4, r3, 1
	SWI        r4, r5, 44
	LOAD      r3, r3, 2
	SWI        r3, r5, 48
	ADDI      r3, r6, 18
	LOAD      r4, r3, 0
	SWI        r4, r5, 52
	LOAD      r4, r3, 1
	SWI        r4, r5, 56
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 60
	.end	_ZN13PinholeCameraC2EiRKiS1_
$tmp36:
	.size	_ZN13PinholeCameraC2EiRKiS1_, ($tmp36)-_ZN13PinholeCameraC2EiRKiS1_

	.globl	_ZN13PinholeCamera3SetEiRKiS1_
	.align	2
	.type	_ZN13PinholeCamera3SetEiRKiS1_,@function
	.ent	_ZN13PinholeCamera3SetEiRKiS1_ # @_ZN13PinholeCamera3SetEiRKiS1_
_ZN13PinholeCamera3SetEiRKiS1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r7, 0
	FPCONV       r4, r3
	ORI       r3, r0, 1065353216
	FPDIV      r4, r3, r4
	SWI        r4, r5, 64
	LWI       r4, r8, 0
	FPCONV       r4, r4
	FPDIV      r3, r3, r4
	SWI        r3, r5, 68
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	SWI        r3, r5, 8
	ADDI      r3, r6, 9
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	ADDI      r3, r6, 12
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	SWI        r3, r5, 32
	ADDI      r3, r6, 15
	LOAD      r4, r3, 0
	SWI        r4, r5, 40
	LOAD      r4, r3, 1
	SWI        r4, r5, 44
	LOAD      r3, r3, 2
	SWI        r3, r5, 48
	ADDI      r3, r6, 18
	LOAD      r4, r3, 0
	SWI        r4, r5, 52
	LOAD      r4, r3, 1
	SWI        r4, r5, 56
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 60
	.end	_ZN13PinholeCamera3SetEiRKiS1_
$tmp37:
	.size	_ZN13PinholeCamera3SetEiRKiS1_, ($tmp37)-_ZN13PinholeCamera3SetEiRKiS1_

	.globl	_ZN13PinholeCameraC2Ev
	.align	2
	.type	_ZN13PinholeCameraC2Ev,@function
	.ent	_ZN13PinholeCameraC2Ev  # @_ZN13PinholeCameraC2Ev
_ZN13PinholeCameraC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN13PinholeCameraC2Ev
$tmp38:
	.size	_ZN13PinholeCameraC2Ev, ($tmp38)-_ZN13PinholeCameraC2Ev

	.globl	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
	.align	2
	.type	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_,@function
	.ent	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_ # @_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_:
	.frame	r1,12,r15
	.mask	0x300000
# BB#0:                                 # %entry
	ADDI      r1, r1, -12
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	LWI       r3, r10, 0
	FPCONV       r4, r3
	ORI       r3, r0, 1065353216
	FPDIV      r4, r3, r4
	SWI        r4, r5, 64
	LWI       r4, r1, 40
	LWI       r4, r4, 0
	FPCONV       r4, r4
	FPDIV      r4, r3, r4
	SWI        r4, r5, 68
	LWI        r4, r6, 0
	SWI        r4, r5, 0
	LWI        r4, r6, 4
	SWI        r4, r5, 4
	LWI        r4, r6, 8
	SWI        r4, r5, 8
	LWI        r4, r7, 0
	SWI        r4, r5, 12
	LWI        r4, r7, 4
	SWI        r4, r5, 16
	LWI        r4, r7, 8
	SWI        r4, r5, 20
	LWI        r4, r9, 0
	SWI        r4, r5, 36
	LWI        r4, r8, 0
	SWI        r4, r5, 24
	LWI        r6, r8, 4
	SWI        r6, r5, 28
	FPMUL      r4, r4, r4
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	LWI        r6, r8, 8
	SWI        r6, r5, 32
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	FPINVSQRT r4, r4
	FPDIV      r7, r3, r4
	LWI        r4, r5, 24
	FPDIV      r4, r4, r7
	SWI        r4, r5, 24
	LWI        r6, r5, 28
	FPDIV      r6, r6, r7
	SWI        r6, r5, 28
	LWI        r8, r5, 32
	FPDIV      r9, r8, r7
	SWI        r9, r5, 32
	LWI        r11, r5, 20
	FPMUL      r7, r4, r11
	LWI        r8, r5, 12
	FPMUL      r12, r9, r8
	FPRSUB     r7, r7, r12
	FPMUL      r8, r6, r8
	LWI        r20, r5, 16
	FPMUL      r12, r4, r20
	FPRSUB     r8, r8, r12
	FPMUL      r12, r8, r6
	FPMUL      r21, r7, r9
	FPRSUB     r12, r12, r21
	FPMUL      r20, r9, r20
	FPMUL      r11, r6, r11
	FPRSUB     r11, r20, r11
	SWI        r11, r5, 40
	SWI        r7, r5, 44
	SWI        r8, r5, 48
	SWI        r12, r5, 52
	FPMUL      r9, r11, r9
	FPMUL      r12, r8, r4
	FPRSUB     r9, r9, r12
	SWI        r9, r5, 56
	FPMUL      r4, r7, r4
	FPMUL      r6, r11, r6
	FPRSUB     r4, r4, r6
	SWI        r4, r5, 60
	FPMUL      r4, r7, r7
	FPMUL      r6, r11, r11
	FPADD      r4, r6, r4
	FPMUL      r6, r8, r8
	FPADD      r4, r4, r6
	FPINVSQRT r4, r4
	FPDIV      r4, r3, r4
	LWI        r6, r5, 40
	FPDIV      r6, r6, r4
	SWI        r6, r5, 40
	LWI        r6, r5, 44
	FPDIV      r6, r6, r4
	SWI        r6, r5, 44
	LWI        r6, r5, 48
	FPDIV      r4, r6, r4
	SWI        r4, r5, 48
	LWI        r4, r5, 56
	FPMUL      r4, r4, r4
	LWI        r6, r5, 52
	FPMUL      r6, r6, r6
	FPADD      r4, r6, r4
	LWI        r6, r5, 60
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	FPINVSQRT r8, r4
	LWI        r4, r5, 36
	LWI        r6, r5, 40
	FPMUL      r11, r6, r4
	LWI       r6, r10, 0
	LWI        r7, r5, 68
	LWI        r9, r5, 52
	LWI        r10, r5, 56
	LWI        r12, r5, 60
	SWI        r11, r5, 40
	LWI        r11, r5, 44
	FPMUL      r11, r11, r4
	SWI        r11, r5, 44
	FPDIV      r11, r3, r8
	FPDIV      r3, r12, r11
	FPDIV      r8, r10, r11
	FPDIV      r9, r9, r11
	LWI        r10, r5, 48
	FPMUL      r10, r10, r4
	SWI        r10, r5, 48
	FPCONV       r6, r6
	FPMUL      r6, r6, r7
	FPDIV      r4, r4, r6
	FPMUL      r6, r9, r4
	SWI        r6, r5, 52
	FPMUL      r6, r8, r4
	SWI        r6, r5, 56
	FPMUL      r3, r3, r4
	SWI        r3, r5, 60
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 12
	.end	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
$tmp39:
	.size	_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_, ($tmp39)-_ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_

	.globl	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
	.align	2
	.type	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_,@function
	.ent	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_ # @_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_:
	.frame	r1,12,r15
	.mask	0x300000
# BB#0:                                 # %entry
	ADDI      r1, r1, -12
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	LWI       r3, r10, 0
	FPCONV       r4, r3
	ORI       r3, r0, 1065353216
	FPDIV      r4, r3, r4
	SWI        r4, r5, 64
	LWI       r4, r1, 40
	LWI       r4, r4, 0
	FPCONV       r4, r4
	FPDIV      r4, r3, r4
	SWI        r4, r5, 68
	LWI        r4, r6, 0
	SWI        r4, r5, 0
	LWI        r4, r6, 4
	SWI        r4, r5, 4
	LWI        r4, r6, 8
	SWI        r4, r5, 8
	LWI        r4, r7, 0
	SWI        r4, r5, 12
	LWI        r4, r7, 4
	SWI        r4, r5, 16
	LWI        r4, r7, 8
	SWI        r4, r5, 20
	LWI        r4, r9, 0
	SWI        r4, r5, 36
	LWI        r4, r8, 0
	SWI        r4, r5, 24
	LWI        r6, r8, 4
	SWI        r6, r5, 28
	FPMUL      r4, r4, r4
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	LWI        r6, r8, 8
	SWI        r6, r5, 32
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	FPINVSQRT r4, r4
	FPDIV      r7, r3, r4
	LWI        r4, r5, 24
	FPDIV      r4, r4, r7
	SWI        r4, r5, 24
	LWI        r6, r5, 28
	FPDIV      r6, r6, r7
	SWI        r6, r5, 28
	LWI        r8, r5, 32
	FPDIV      r9, r8, r7
	SWI        r9, r5, 32
	LWI        r11, r5, 20
	FPMUL      r7, r4, r11
	LWI        r8, r5, 12
	FPMUL      r12, r9, r8
	FPRSUB     r7, r7, r12
	FPMUL      r8, r6, r8
	LWI        r20, r5, 16
	FPMUL      r12, r4, r20
	FPRSUB     r8, r8, r12
	FPMUL      r12, r8, r6
	FPMUL      r21, r7, r9
	FPRSUB     r12, r12, r21
	FPMUL      r20, r9, r20
	FPMUL      r11, r6, r11
	FPRSUB     r11, r20, r11
	SWI        r11, r5, 40
	SWI        r7, r5, 44
	SWI        r8, r5, 48
	SWI        r12, r5, 52
	FPMUL      r9, r11, r9
	FPMUL      r12, r8, r4
	FPRSUB     r9, r9, r12
	SWI        r9, r5, 56
	FPMUL      r4, r7, r4
	FPMUL      r6, r11, r6
	FPRSUB     r4, r4, r6
	SWI        r4, r5, 60
	FPMUL      r4, r7, r7
	FPMUL      r6, r11, r11
	FPADD      r4, r6, r4
	FPMUL      r6, r8, r8
	FPADD      r4, r4, r6
	FPINVSQRT r4, r4
	FPDIV      r4, r3, r4
	LWI        r6, r5, 40
	FPDIV      r6, r6, r4
	SWI        r6, r5, 40
	LWI        r6, r5, 44
	FPDIV      r6, r6, r4
	SWI        r6, r5, 44
	LWI        r6, r5, 48
	FPDIV      r4, r6, r4
	SWI        r4, r5, 48
	LWI        r4, r5, 56
	FPMUL      r4, r4, r4
	LWI        r6, r5, 52
	FPMUL      r6, r6, r6
	FPADD      r4, r6, r4
	LWI        r6, r5, 60
	FPMUL      r6, r6, r6
	FPADD      r4, r4, r6
	FPINVSQRT r8, r4
	LWI        r4, r5, 36
	LWI        r6, r5, 40
	FPMUL      r11, r6, r4
	LWI       r6, r10, 0
	LWI        r7, r5, 68
	LWI        r9, r5, 52
	LWI        r10, r5, 56
	LWI        r12, r5, 60
	SWI        r11, r5, 40
	LWI        r11, r5, 44
	FPMUL      r11, r11, r4
	SWI        r11, r5, 44
	FPDIV      r11, r3, r8
	FPDIV      r3, r12, r11
	FPDIV      r8, r10, r11
	FPDIV      r9, r9, r11
	LWI        r10, r5, 48
	FPMUL      r10, r10, r4
	SWI        r10, r5, 48
	FPCONV       r6, r6
	FPMUL      r6, r6, r7
	FPDIV      r4, r4, r6
	FPMUL      r6, r9, r4
	SWI        r6, r5, 52
	FPMUL      r6, r8, r4
	SWI        r6, r5, 56
	FPMUL      r3, r3, r4
	SWI        r3, r5, 60
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 12
	.end	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_
$tmp40:
	.size	_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_, ($tmp40)-_ZN13PinholeCamera3SetERK8syVectorS2_S2_RKfRKiS6_

	.globl	_ZN13PinholeCamera14LoadFromMemoryERKi
	.align	2
	.type	_ZN13PinholeCamera14LoadFromMemoryERKi,@function
	.ent	_ZN13PinholeCamera14LoadFromMemoryERKi # @_ZN13PinholeCamera14LoadFromMemoryERKi
_ZN13PinholeCamera14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r6, 0
	ADDI      r3, r3, 9
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	LWI       r3, r6, 0
	ADDI      r3, r3, 12
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	SWI        r3, r5, 32
	LWI       r3, r6, 0
	ADDI      r3, r3, 15
	LOAD      r4, r3, 0
	SWI        r4, r5, 40
	LOAD      r4, r3, 1
	SWI        r4, r5, 44
	LOAD      r3, r3, 2
	SWI        r3, r5, 48
	LWI       r3, r6, 0
	ADDI      r3, r3, 18
	LOAD      r4, r3, 0
	SWI        r4, r5, 52
	LOAD      r4, r3, 1
	SWI        r4, r5, 56
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 60
	.end	_ZN13PinholeCamera14LoadFromMemoryERKi
$tmp41:
	.size	_ZN13PinholeCamera14LoadFromMemoryERKi, ($tmp41)-_ZN13PinholeCamera14LoadFromMemoryERKi

	.globl	_ZNK13PinholeCamera11GenerateRayERKiS1_
	.align	2
	.type	_ZNK13PinholeCamera11GenerateRayERKiS1_,@function
	.ent	_ZNK13PinholeCamera11GenerateRayERKiS1_ # @_ZNK13PinholeCamera11GenerateRayERKiS1_
_ZNK13PinholeCamera11GenerateRayERKiS1_:
	.frame	r1,28,r15
	.mask	0x7f00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -28
	SWI       r20, r1, 24
	SWI       r21, r1, 20
	SWI       r22, r1, 16
	SWI       r23, r1, 12
	SWI       r24, r1, 8
	SWI       r25, r1, 4
	SWI       r26, r1, 0
	LWI       r10, r8, 0
	LWI       r21, r7, 0
	LWI        r3, r6, 32
	LWI        r4, r6, 48
	LWI        r7, r6, 60
	LWI        r8, r6, 8
	LWI        r9, r6, 56
	LWI        r11, r6, 28
	LWI        r12, r6, 44
	LWI        r20, r6, 52
	LWI        r22, r6, 68
	LWI        r23, r6, 24
	LWI        r24, r6, 40
	LWI        r25, r6, 4
	LWI        r26, r6, 64
	LWI        r6, r6, 0
	SWI        r6, r5, 0
	FPCONV       r6, r21
	FPMUL      r6, r6, r26
	SWI        r25, r5, 4
	FPADD      r6, r6, r6
	ORI       r21, r0, -1082130432
	FPADD      r6, r6, r21
	FPMUL      r24, r24, r6
	FPADD      r23, r23, r24
	FPCONV       r10, r10
	FPMUL      r10, r10, r22
	FPADD      r10, r10, r10
	FPADD      r21, r10, r21
	FPMUL      r10, r20, r21
	FPADD      r10, r23, r10
	FPMUL      r12, r12, r6
	FPADD      r11, r11, r12
	FPMUL      r9, r9, r21
	FPADD      r9, r11, r9
	SWI        r8, r5, 8
	FPMUL      r8, r9, r9
	FPMUL      r11, r10, r10
	FPADD      r8, r11, r8
	FPMUL      r7, r7, r21
	FPMUL      r4, r4, r6
	FPADD      r3, r3, r4
	FPADD      r3, r3, r7
	FPMUL      r4, r3, r3
	FPADD      r4, r8, r4
	FPINVSQRT r4, r4
	ORI       r11, r0, 1065353216
	FPDIV      r7, r11, r4
	FPDIV      r4, r10, r7
	FPDIV      r6, r9, r7
	FPDIV      r7, r3, r7
	FPDIV      r3, r11, r7
	FPDIV      r8, r11, r6
	FPDIV      r9, r11, r4
	ORI       r10, r0, 0
	FPLT   r12, r9, r10
	FPLT   r22, r8, r10
	FPLT   r20, r3, r10
	ADDI      r21, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB42_2)
	ADD      r10, r11, r0
# BB#1:                                 # %entry
	ADD      r10, r21, r0
$BB42_2:                                # %entry
	bneid     r22, ($BB42_4)
	ADD      r20, r11, r0
# BB#3:                                 # %entry
	ADD      r20, r21, r0
$BB42_4:                                # %entry
	bneid     r12, ($BB42_6)
	NOP    
# BB#5:                                 # %entry
	ADD      r11, r21, r0
$BB42_6:                                # %entry
	SWI        r4, r5, 12
	SWI        r6, r5, 16
	SWI        r7, r5, 20
	SWI        r9, r5, 24
	SWI        r8, r5, 28
	SWI        r3, r5, 32
	SWI       r11, r5, 36
	SWI       r20, r5, 40
	SWI       r10, r5, 44
	LWI       r26, r1, 0
	LWI       r25, r1, 4
	LWI       r24, r1, 8
	LWI       r23, r1, 12
	LWI       r22, r1, 16
	LWI       r21, r1, 20
	LWI       r20, r1, 24
	rtsd      r15, 8
	ADDI      r1, r1, 28
	.end	_ZNK13PinholeCamera11GenerateRayERKiS1_
$tmp42:
	.size	_ZNK13PinholeCamera11GenerateRayERKiS1_, ($tmp42)-_ZNK13PinholeCamera11GenerateRayERKiS1_

	.globl	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
	.align	2
	.type	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_,@function
	.ent	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_ # @_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
_ZNK13PinholeCamera17GenerateSampleRayERKiS1_:
	.frame	r1,36,r15
	.mask	0x1ff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -36
	SWI       r20, r1, 32
	SWI       r21, r1, 28
	SWI       r22, r1, 24
	SWI       r23, r1, 20
	SWI       r24, r1, 16
	SWI       r25, r1, 12
	SWI       r26, r1, 8
	SWI       r27, r1, 4
	SWI       r28, r1, 0
	RAND      r23
	RAND      r21
	LWI       r11, r8, 0
	LWI       r24, r7, 0
	LWI        r3, r6, 32
	LWI        r4, r6, 48
	LWI        r7, r6, 60
	LWI        r8, r6, 8
	LWI        r9, r6, 56
	LWI        r10, r6, 28
	LWI        r12, r6, 44
	LWI        r20, r6, 52
	LWI        r22, r6, 68
	LWI        r25, r6, 24
	LWI        r26, r6, 40
	LWI        r27, r6, 4
	LWI        r28, r6, 64
	LWI        r6, r6, 0
	SWI        r6, r5, 0
	ORI       r6, r0, -1090519040
	FPADD      r23, r23, r6
	FPCONV       r24, r24
	FPADD      r23, r24, r23
	FPADD      r21, r21, r6
	FPMUL      r6, r23, r28
	SWI        r27, r5, 4
	FPADD      r6, r6, r6
	ORI       r23, r0, -1082130432
	FPADD      r6, r6, r23
	FPMUL      r24, r26, r6
	FPADD      r24, r25, r24
	FPCONV       r11, r11
	FPADD      r11, r11, r21
	FPMUL      r11, r11, r22
	FPADD      r11, r11, r11
	FPADD      r21, r11, r23
	FPMUL      r11, r20, r21
	FPADD      r11, r24, r11
	FPMUL      r12, r12, r6
	FPADD      r10, r10, r12
	FPMUL      r9, r9, r21
	FPADD      r9, r10, r9
	SWI        r8, r5, 8
	FPMUL      r8, r9, r9
	FPMUL      r10, r11, r11
	FPADD      r8, r10, r8
	FPMUL      r7, r7, r21
	FPMUL      r4, r4, r6
	FPADD      r3, r3, r4
	FPADD      r3, r3, r7
	FPMUL      r4, r3, r3
	FPADD      r4, r8, r4
	FPINVSQRT r4, r4
	ORI       r10, r0, 1065353216
	FPDIV      r7, r10, r4
	FPDIV      r4, r11, r7
	FPDIV      r6, r9, r7
	FPDIV      r7, r3, r7
	FPDIV      r3, r10, r7
	FPDIV      r8, r10, r6
	FPDIV      r9, r10, r4
	ORI       r10, r0, 0
	FPLT   r12, r9, r10
	FPLT   r22, r8, r10
	FPLT   r20, r3, r10
	ADDI      r21, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB43_2)
	ADD      r10, r11, r0
# BB#1:                                 # %entry
	ADD      r10, r21, r0
$BB43_2:                                # %entry
	bneid     r22, ($BB43_4)
	ADD      r20, r11, r0
# BB#3:                                 # %entry
	ADD      r20, r21, r0
$BB43_4:                                # %entry
	bneid     r12, ($BB43_6)
	NOP    
# BB#5:                                 # %entry
	ADD      r11, r21, r0
$BB43_6:                                # %entry
	SWI        r4, r5, 12
	SWI        r6, r5, 16
	SWI        r7, r5, 20
	SWI        r9, r5, 24
	SWI        r8, r5, 28
	SWI        r3, r5, 32
	SWI       r11, r5, 36
	SWI       r20, r5, 40
	SWI       r10, r5, 44
	LWI       r28, r1, 0
	LWI       r27, r1, 4
	LWI       r26, r1, 8
	LWI       r25, r1, 12
	LWI       r24, r1, 16
	LWI       r23, r1, 20
	LWI       r22, r1, 24
	LWI       r21, r1, 28
	LWI       r20, r1, 32
	rtsd      r15, 8
	ADDI      r1, r1, 36
	.end	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_
$tmp43:
	.size	_ZNK13PinholeCamera17GenerateSampleRayERKiS1_, ($tmp43)-_ZNK13PinholeCamera17GenerateSampleRayERKiS1_

	.globl	_ZN10PointLightC2EiRK5Color
	.align	2
	.type	_ZN10PointLightC2EiRK5Color,@function
	.ent	_ZN10PointLightC2EiRK5Color # @_ZN10PointLightC2EiRK5Color
_ZN10PointLightC2EiRK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 12
	SWI       r3, r5, 16
	SWI       r3, r5, 20
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	SWI        r3, r5, 8
	LWI       r3, r7, 8
	SWI       r3, r5, 20
	LWI       r3, r7, 4
	SWI       r3, r5, 16
	LWI       r3, r7, 0
	rtsd      r15, 8
	SWI       r3, r5, 12
	.end	_ZN10PointLightC2EiRK5Color
$tmp44:
	.size	_ZN10PointLightC2EiRK5Color, ($tmp44)-_ZN10PointLightC2EiRK5Color

	.globl	_ZN10PointLight14LoadFromMemoryERKi
	.align	2
	.type	_ZN10PointLight14LoadFromMemoryERKi,@function
	.ent	_ZN10PointLight14LoadFromMemoryERKi # @_ZN10PointLight14LoadFromMemoryERKi
_ZN10PointLight14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN10PointLight14LoadFromMemoryERKi
$tmp45:
	.size	_ZN10PointLight14LoadFromMemoryERKi, ($tmp45)-_ZN10PointLight14LoadFromMemoryERKi

	.globl	_ZN10PointLightC2Ev
	.align	2
	.type	_ZN10PointLightC2Ev,@function
	.ent	_ZN10PointLightC2Ev     # @_ZN10PointLightC2Ev
_ZN10PointLightC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r0, r5, 20
	SWI       r0, r5, 16
	SWI       r0, r5, 12
	SWI       r0, r5, 8
	SWI       r0, r5, 4
	rtsd      r15, 8
	SWI       r0, r5, 0
	.end	_ZN10PointLightC2Ev
$tmp46:
	.size	_ZN10PointLightC2Ev, ($tmp46)-_ZN10PointLightC2Ev

	.globl	_ZN10PointLightC2ERK8syVectorRK5Color
	.align	2
	.type	_ZN10PointLightC2ERK8syVectorRK5Color,@function
	.ent	_ZN10PointLightC2ERK8syVectorRK5Color # @_ZN10PointLightC2ERK8syVectorRK5Color
_ZN10PointLightC2ERK8syVectorRK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 12
	SWI       r3, r5, 16
	SWI       r3, r5, 20
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI       r3, r7, 8
	SWI       r3, r5, 20
	LWI       r3, r7, 4
	SWI       r3, r5, 16
	LWI       r3, r7, 0
	rtsd      r15, 8
	SWI       r3, r5, 12
	.end	_ZN10PointLightC2ERK8syVectorRK5Color
$tmp47:
	.size	_ZN10PointLightC2ERK8syVectorRK5Color, ($tmp47)-_ZN10PointLightC2ERK8syVectorRK5Color

	.globl	_ZN10PointLight12SetIntensityERK5Color
	.align	2
	.type	_ZN10PointLight12SetIntensityERK5Color,@function
	.ent	_ZN10PointLight12SetIntensityERK5Color # @_ZN10PointLight12SetIntensityERK5Color
_ZN10PointLight12SetIntensityERK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 8
	SWI       r3, r5, 20
	LWI       r3, r6, 4
	SWI       r3, r5, 16
	LWI       r3, r6, 0
	rtsd      r15, 8
	SWI       r3, r5, 12
	.end	_ZN10PointLight12SetIntensityERK5Color
$tmp48:
	.size	_ZN10PointLight12SetIntensityERK5Color, ($tmp48)-_ZN10PointLight12SetIntensityERK5Color

	.globl	_ZNK10PointLight8PositionEv
	.align	2
	.type	_ZNK10PointLight8PositionEv,@function
	.ent	_ZNK10PointLight8PositionEv # @_ZNK10PointLight8PositionEv
_ZNK10PointLight8PositionEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZNK10PointLight8PositionEv
$tmp49:
	.size	_ZNK10PointLight8PositionEv, ($tmp49)-_ZNK10PointLight8PositionEv

	.globl	_ZNK10PointLight9IntensityEv
	.align	2
	.type	_ZNK10PointLight9IntensityEv,@function
	.ent	_ZNK10PointLight9IntensityEv # @_ZNK10PointLight9IntensityEv
_ZNK10PointLight9IntensityEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADDI      r3, r5, 12
	.end	_ZNK10PointLight9IntensityEv
$tmp50:
	.size	_ZNK10PointLight9IntensityEv, ($tmp50)-_ZNK10PointLight9IntensityEv

	.globl	_ZNK10PointLight9DirectionERK8syVector
	.align	2
	.type	_ZNK10PointLight9DirectionERK8syVector,@function
	.ent	_ZNK10PointLight9DirectionERK8syVector # @_ZNK10PointLight9DirectionERK8syVector
_ZNK10PointLight9DirectionERK8syVector:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPRSUB     r4, r3, r4
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPRSUB     r4, r8, r6
	SWI        r4, r5, 4
	FPRSUB     r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK10PointLight9DirectionERK8syVector
$tmp51:
	.size	_ZNK10PointLight9DirectionERK8syVector, ($tmp51)-_ZNK10PointLight9DirectionERK8syVector

	.globl	_ZN3RayC2Ev
	.align	2
	.type	_ZN3RayC2Ev,@function
	.ent	_ZN3RayC2Ev             # @_ZN3RayC2Ev
_ZN3RayC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN3RayC2Ev
$tmp52:
	.size	_ZN3RayC2Ev, ($tmp52)-_ZN3RayC2Ev

	.globl	_ZN3RayC2E8syVectorS0_
	.align	2
	.type	_ZN3RayC2E8syVectorS0_,@function
	.ent	_ZN3RayC2E8syVectorS0_  # @_ZN3RayC2E8syVectorS0_
_ZN3RayC2E8syVectorS0_:
	.frame	r1,4,r15
	.mask	0x100000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r20, r1, 0
	LWI        r3, r7, 8
	LWI        r4, r7, 4
	LWI        r7, r7, 0
	LWI        r8, r6, 8
	LWI        r9, r6, 4
	LWI        r6, r6, 0
	SWI        r6, r5, 0
	SWI        r9, r5, 4
	SWI        r8, r5, 8
	SWI        r7, r5, 12
	SWI        r4, r5, 16
	FPMUL      r4, r4, r4
	FPMUL      r6, r7, r7
	FPADD      r4, r6, r4
	SWI        r3, r5, 20
	FPMUL      r3, r3, r3
	FPADD      r3, r4, r3
	FPINVSQRT r3, r3
	ORI       r9, r0, 1065353216
	FPDIV      r3, r9, r3
	LWI        r4, r5, 12
	FPDIV      r11, r4, r3
	SWI        r11, r5, 12
	LWI        r4, r5, 16
	FPDIV      r4, r4, r3
	SWI        r4, r5, 16
	LWI        r6, r5, 20
	FPDIV      r6, r6, r3
	FPDIV      r3, r9, r4
	FPDIV      r4, r9, r6
	ADDI      r10, r0, 0
	ADDI      r7, r0, 1
	ORI       r12, r0, 0
	FPLT   r20, r4, r12
	bneid     r20, ($BB53_2)
	ADD      r8, r7, r0
# BB#1:                                 # %entry
	ADD      r8, r10, r0
$BB53_2:                                # %entry
	FPDIV      r11, r9, r11
	FPLT   r20, r3, r12
	bneid     r20, ($BB53_4)
	ADD      r9, r7, r0
# BB#3:                                 # %entry
	ADD      r9, r10, r0
$BB53_4:                                # %entry
	FPLT   r12, r11, r12
	bneid     r12, ($BB53_6)
	NOP    
# BB#5:                                 # %entry
	ADD      r7, r10, r0
$BB53_6:                                # %entry
	SWI        r6, r5, 20
	SWI        r11, r5, 24
	SWI        r3, r5, 28
	SWI        r4, r5, 32
	SWI       r7, r5, 36
	SWI       r9, r5, 40
	SWI       r8, r5, 44
	LWI       r20, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_ZN3RayC2E8syVectorS0_
$tmp53:
	.size	_ZN3RayC2E8syVectorS0_, ($tmp53)-_ZN3RayC2E8syVectorS0_

	.globl	_ZN3Ray3SetE8syVectorS0_
	.align	2
	.type	_ZN3Ray3SetE8syVectorS0_,@function
	.ent	_ZN3Ray3SetE8syVectorS0_ # @_ZN3Ray3SetE8syVectorS0_
_ZN3Ray3SetE8syVectorS0_:
	.frame	r1,4,r15
	.mask	0x100000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r20, r1, 0
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r7, 0
	SWI        r3, r5, 12
	LWI        r4, r7, 4
	SWI        r4, r5, 16
	FPMUL      r3, r3, r3
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	LWI        r4, r7, 8
	SWI        r4, r5, 20
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r9, r0, 1065353216
	FPDIV      r3, r9, r3
	LWI        r4, r5, 12
	FPDIV      r11, r4, r3
	SWI        r11, r5, 12
	LWI        r4, r5, 16
	FPDIV      r4, r4, r3
	SWI        r4, r5, 16
	LWI        r6, r5, 20
	FPDIV      r6, r6, r3
	FPDIV      r3, r9, r4
	FPDIV      r4, r9, r6
	ADDI      r10, r0, 0
	ADDI      r7, r0, 1
	ORI       r12, r0, 0
	FPLT   r20, r4, r12
	bneid     r20, ($BB54_2)
	ADD      r8, r7, r0
# BB#1:                                 # %entry
	ADD      r8, r10, r0
$BB54_2:                                # %entry
	FPDIV      r11, r9, r11
	FPLT   r20, r3, r12
	bneid     r20, ($BB54_4)
	ADD      r9, r7, r0
# BB#3:                                 # %entry
	ADD      r9, r10, r0
$BB54_4:                                # %entry
	FPLT   r12, r11, r12
	bneid     r12, ($BB54_6)
	NOP    
# BB#5:                                 # %entry
	ADD      r7, r10, r0
$BB54_6:                                # %entry
	SWI        r6, r5, 20
	SWI        r11, r5, 24
	SWI        r3, r5, 28
	SWI        r4, r5, 32
	SWI       r7, r5, 36
	SWI       r9, r5, 40
	SWI       r8, r5, 44
	LWI       r20, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_ZN3Ray3SetE8syVectorS0_
$tmp54:
	.size	_ZN3Ray3SetE8syVectorS0_, ($tmp54)-_ZN3Ray3SetE8syVectorS0_

	.globl	_ZN3RayC2ERKS_
	.align	2
	.type	_ZN3RayC2ERKS_,@function
	.ent	_ZN3RayC2ERKS_          # @_ZN3RayC2ERKS_
_ZN3RayC2ERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r6, 12
	SWI        r3, r5, 12
	LWI        r3, r6, 16
	SWI        r3, r5, 16
	LWI        r3, r6, 20
	SWI        r3, r5, 20
	LWI        r3, r6, 24
	SWI        r3, r5, 24
	LWI        r3, r6, 28
	SWI        r3, r5, 28
	LWI        r3, r6, 32
	SWI        r3, r5, 32
	LWI       r3, r6, 36
	SWI       r3, r5, 36
	LWI       r3, r6, 40
	SWI       r3, r5, 40
	LWI       r3, r6, 44
	rtsd      r15, 8
	SWI       r3, r5, 44
	.end	_ZN3RayC2ERKS_
$tmp55:
	.size	_ZN3RayC2ERKS_, ($tmp55)-_ZN3RayC2ERKS_

	.globl	_ZN3RayaSERKS_
	.align	2
	.type	_ZN3RayaSERKS_,@function
	.ent	_ZN3RayaSERKS_          # @_ZN3RayaSERKS_
_ZN3RayaSERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r6, 12
	SWI        r3, r5, 12
	LWI        r3, r6, 16
	SWI        r3, r5, 16
	LWI        r3, r6, 20
	SWI        r3, r5, 20
	LWI        r3, r6, 24
	SWI        r3, r5, 24
	LWI        r3, r6, 28
	SWI        r3, r5, 28
	LWI        r3, r6, 32
	SWI        r3, r5, 32
	LWI       r3, r6, 36
	SWI       r3, r5, 36
	LWI       r3, r6, 40
	SWI       r3, r5, 40
	LWI       r3, r6, 44
	SWI       r3, r5, 44
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN3RayaSERKS_
$tmp56:
	.size	_ZN3RayaSERKS_, ($tmp56)-_ZN3RayaSERKS_

	.globl	_ZN3Ray9NormalizeEv
	.align	2
	.type	_ZN3Ray9NormalizeEv,@function
	.ent	_ZN3Ray9NormalizeEv     # @_ZN3Ray9NormalizeEv
_ZN3Ray9NormalizeEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 16
	FPMUL      r3, r3, r3
	LWI        r4, r5, 12
	FPMUL      r4, r4, r4
	FPADD      r3, r4, r3
	LWI        r4, r5, 20
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	LWI        r4, r5, 12
	FPDIV      r4, r4, r3
	SWI        r4, r5, 12
	LWI        r4, r5, 16
	FPDIV      r4, r4, r3
	SWI        r4, r5, 16
	LWI        r4, r5, 20
	FPDIV      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 20
	.end	_ZN3Ray9NormalizeEv
$tmp57:
	.size	_ZN3Ray9NormalizeEv, ($tmp57)-_ZN3Ray9NormalizeEv

	.globl	_Z9trax_mainv
	.align	2
	.type	_Z9trax_mainv,@function
	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
	.frame	r1,824,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -824
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	ADD      r8, r0, r0
	LOAD      r12, r8, 1
	SWI       r12, r1, 812
	FPCONV       r5, r12
	ORI       r6, r0, 1065353216
	LOAD      r3, r8, 4
	FPCONV       r7, r3
	LOAD      r11, r8, 7
	SWI       r11, r1, 808
	LOAD      r4, r8, 12
	LOAD      r9, r4, 0
	SWI       r9, r1, 696
	LOAD      r9, r4, 1
	SWI       r9, r1, 700
	LOAD      r4, r4, 2
	SWI       r4, r1, 704
	LOAD      r4, r8, 28
	LOAD      r4, r8, 29
	ADDI      r9, r0, 17
	LOAD      r4, r8, 9
	LOAD      r10, r8, 8
	SWI       r10, r1, 588
	LOAD      r20, r9, 0
	SWI       r20, r1, 712
	ADDI      r9, r0, 16
	LOAD      r9, r9, 0
	SWI       r9, r1, 708
	LOAD      r8, r8, 10
	LOAD      r9, r8, 0
	SWI       r9, r1, 736
	LOAD      r9, r8, 1
	SWI       r9, r1, 740
	LOAD      r9, r8, 2
	SWI       r9, r1, 744
	ADDI      r9, r8, 9
	LOAD      r10, r9, 0
	LOAD      r10, r9, 1
	LOAD      r9, r9, 2
	ADD      r9, r11, r0
	MUL       r11, r3, r12
	ADDI      r3, r8, 12
	LOAD      r10, r3, 0
	SWI       r10, r1, 748
	LOAD      r10, r3, 1
	SWI       r10, r1, 752
	LOAD      r3, r3, 2
	SWI       r3, r1, 756
	ADDI      r3, r8, 15
	LOAD      r10, r3, 0
	SWI       r10, r1, 760
	LOAD      r10, r3, 1
	SWI       r10, r1, 764
	LOAD      r3, r3, 2
	SWI       r3, r1, 768
	ADDI      r3, r8, 18
	LOAD      r8, r3, 0
	SWI       r8, r1, 772
	LOAD      r8, r3, 1
	SWI       r8, r1, 776
	LOAD      r3, r3, 2
	SWI       r3, r1, 780
	ATOMIC_INC r3, 0
	CMP       r8, r11, r3
	bgeid     r8, ($BB58_757)
	NOP    
# BB#1:                                 # %for.body.lr.ph
	FPDIV      r5, r6, r5
	SWI       r5, r1, 784
	FPDIV      r5, r6, r7
	SWI       r5, r1, 788
	ADDI      r5, r0, 1
	CMP       r6, r5, r20
	FPCONV       r7, r20
	bneid     r6, ($BB58_12)
	SWI       r7, r1, 800
# BB#2:                                 # %if.end.us.preheader
	ADDI      r4, r4, 4
	SWI       r4, r1, 712
	ADD      r9, r11, r0
	SWI       r9, r1, 804
$BB58_3:                                # %if.end.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB58_4 Depth 2
                                        #       Child Loop BB58_372 Depth 3
                                        #       Child Loop BB58_226 Depth 3
                                        #         Child Loop BB58_270 Depth 4
                                        #       Child Loop BB58_41 Depth 3
                                        #         Child Loop BB58_157 Depth 4
	LWI       r4, r1, 812
	DIV      r5, r4, r3
	MUL       r4, r5, r4
	SWI       r4, r1, 716
	RSUB     r3, r4, r3
	SWI       r3, r1, 720
	FPCONV       r3, r3
	LWI       r4, r1, 784
	FPMUL      r3, r3, r4
	FPADD      r4, r3, r3
	ORI       r3, r0, -1082130432
	FPADD      r4, r4, r3
	LWI       r6, r1, 760
	FPMUL      r6, r6, r4
	LWI       r7, r1, 748
	FPADD      r7, r7, r6
	FPCONV       r5, r5
	LWI       r6, r1, 788
	FPMUL      r5, r5, r6
	FPADD      r5, r5, r5
	FPADD      r6, r5, r3
	LWI       r3, r1, 772
	FPMUL      r3, r3, r6
	FPADD      r3, r7, r3
	LWI       r5, r1, 764
	FPMUL      r5, r5, r4
	LWI       r7, r1, 752
	FPADD      r5, r7, r5
	LWI       r7, r1, 776
	FPMUL      r7, r7, r6
	FPADD      r5, r5, r7
	FPMUL      r7, r5, r5
	FPMUL      r8, r3, r3
	FPADD      r7, r8, r7
	LWI       r8, r1, 780
	FPMUL      r6, r8, r6
	LWI       r8, r1, 768
	FPMUL      r4, r8, r4
	LWI       r8, r1, 756
	FPADD      r4, r8, r4
	FPADD      r4, r4, r6
	FPMUL      r6, r4, r4
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r24, r0, 1065353216
	FPDIV      r6, r24, r6
	FPDIV      r8, r4, r6
	FPDIV      r4, r5, r6
	FPDIV      r5, r3, r6
	ADD      r25, r0, r0
	ORI       r22, r0, 0
	ADD      r21, r22, r0
	ADD      r23, r22, r0
	LWI       r3, r1, 744
	SWI       r3, r1, 560
	LWI       r3, r1, 740
	SWI       r3, r1, 564
	LWI       r3, r1, 736
	SWI       r3, r1, 568
	ADD      r27, r24, r0
	brid      ($BB58_4)
	ADD      r26, r24, r0
$BB58_397:                              # %if.end.i977
                                        #   in Loop: Header=BB58_4 Depth=2
	FPMUL      r11, r28, r9
	FPMUL      r12, r29, r10
	FPRSUB     r12, r11, r12
	FPMUL      r9, r30, r9
	FPMUL      r11, r29, r20
	FPRSUB     r9, r11, r9
	FPMUL      r10, r30, r10
	FPMUL      r11, r28, r20
	FPRSUB     r20, r10, r11
	FPMUL      r10, r28, r20
	FPMUL      r11, r29, r9
	FPRSUB     r10, r10, r11
	FPMUL      r11, r30, r9
	FPMUL      r21, r28, r12
	FPRSUB     r11, r11, r21
	FPMUL      r11, r11, r6
	FPMUL      r21, r20, r7
	FPADD      r11, r21, r11
	FPMUL      r10, r10, r6
	FPMUL      r21, r12, r7
	FPADD      r10, r21, r10
	FPMUL      r21, r30, r8
	FPADD      r10, r10, r21
	FPMUL      r21, r29, r8
	FPADD      r11, r11, r21
	FPMUL      r12, r29, r12
	FPMUL      r20, r30, r20
	FPMUL      r3, r3, r24
	LWI       r21, r1, 644
	FPMUL      r24, r24, r21
	FPMUL      r4, r4, r27
	LWI       r21, r1, 640
	FPMUL      r27, r27, r21
	FPMUL      r5, r5, r26
	LWI       r21, r1, 636
	FPMUL      r26, r26, r21
	FPADD      r22, r22, r3
	LWI       r21, r1, 656
	FPADD      r21, r21, r4
	FPADD      r23, r23, r5
	FPMUL      r3, r11, r11
	FPMUL      r4, r10, r10
	FPADD      r3, r4, r3
	FPRSUB     r4, r12, r20
	FPMUL      r4, r4, r6
	FPMUL      r5, r9, r7
	FPADD      r4, r5, r4
	FPMUL      r5, r28, r8
	FPADD      r4, r4, r5
	FPMUL      r5, r4, r4
	FPADD      r3, r3, r5
	FPINVSQRT r3, r3
	ORI       r5, r0, 1065353216
	FPDIV      r6, r5, r3
	FPDIV      r3, r10, r6
	FPDIV      r7, r11, r6
	FPMUL      r8, r7, r7
	FPMUL      r9, r3, r3
	FPADD      r8, r9, r8
	FPDIV      r4, r4, r6
	FPMUL      r6, r4, r4
	FPADD      r6, r8, r6
	FPINVSQRT r6, r6
	FPDIV      r5, r5, r6
	FPDIV      r8, r4, r5
	FPDIV      r4, r7, r5
	FPDIV      r5, r3, r5
$BB58_4:                                # %while.cond.i205
                                        #   Parent Loop BB58_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB58_372 Depth 3
                                        #       Child Loop BB58_226 Depth 3
                                        #         Child Loop BB58_270 Depth 4
                                        #       Child Loop BB58_41 Depth 3
                                        #         Child Loop BB58_157 Depth 4
	SWI       r5, r1, 576
	ORI       r3, r0, 1065353216
	FPDIV      r7, r3, r8
	SWI       r7, r1, 600
	FPDIV      r9, r3, r4
	SWI       r9, r1, 604
	ADD      r6, r4, r0
	FPDIV      r3, r3, r5
	SWI       r3, r1, 608
	ORI       r4, r0, 0
	FPLT   r3, r3, r4
	FPLT   r5, r9, r4
	FPLT   r9, r7, r4
	ADDI      r4, r0, 0
	ADDI      r7, r0, 1
	SWI       r7, r1, 616
	bneid     r9, ($BB58_6)
	SWI       r7, r1, 620
# BB#5:                                 # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r4, r1, 620
$BB58_6:                                # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r23, r1, 652
	SWI       r22, r1, 648
	LWI       r7, r1, 616
	bneid     r5, ($BB58_8)
	SWI       r7, r1, 624
# BB#7:                                 # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r4, r1, 624
$BB58_8:                                # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	bneid     r3, ($BB58_10)
	NOP    
# BB#9:                                 # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r4, r1, 616
$BB58_10:                               # %while.cond.i205
                                        #   in Loop: Header=BB58_4 Depth=2
	LWI       r3, r1, 708
	CMP       r3, r3, r25
	bgeid     r3, ($BB58_11)
	NOP    
# BB#40:                                # %while.body.i227
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r27, r1, 668
	SWI       r26, r1, 664
	SWI       r24, r1, 660
	SWI       r21, r1, 656
	ADDI      r25, r25, 1
	SWI       r25, r1, 672
	ADDI      r9, r0, 1
	ORI       r3, r0, 1203982336
	SWI       r3, r1, 572
	ADDI      r4, r0, -1
	SWI       r4, r1, 596
	LWI       r3, r1, 620
	XORI      r3, r3, 1
	SWI       r3, r1, 628
	LWI       r3, r1, 624
	XORI      r3, r3, 1
	SWI       r3, r1, 632
	LWI       r3, r1, 616
	XORI      r3, r3, 1
	SWI       r3, r1, 636
	SWI       r0, r1, 336
	SWI       r4, r1, 592
	brid      ($BB58_41)
	ADD      r26, r6, r0
$BB58_146:                              # %if.else24.i.i366
                                        #   in Loop: Header=BB58_41 Depth=3
	beqid     r4, ($BB58_147)
	NOP    
# BB#148:                               # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r11, r5, r21
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r11, ($BB58_150)
	ADD      r10, r3, r0
# BB#149:                               # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r10, r4, r0
$BB58_150:                              # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r10, ($BB58_152)
	NOP    
# BB#151:                               # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r5, r21, r0
$BB58_152:                              # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGE   r6, r12, r5
	FPUN   r5, r12, r5
	BITOR        r5, r5, r6
	bneid     r5, ($BB58_154)
	NOP    
# BB#153:                               # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r3, r4, r0
$BB58_154:                              # %if.else31.i.i371
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r3, ($BB58_156)
	NOP    
# BB#155:                               # %if.then35.i.i374
                                        #   in Loop: Header=BB58_41 Depth=3
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	SWI       r29, r5, -4
	LWI       r5, r1, 580
	SW        r5, r4, r3
	brid      ($BB58_182)
	ADDI      r9, r9, 1
$BB58_147:                              # %if.then28.i.i367
                                        #   in Loop: Header=BB58_41 Depth=3
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	brid      ($BB58_182)
	SWI       r29, r3, -4
$BB58_156:                              # %if.else40.i.i377
                                        #   in Loop: Header=BB58_41 Depth=3
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	LWI       r6, r1, 580
	SWI       r6, r5, -4
	SW        r29, r4, r3
	brid      ($BB58_182)
	ADDI      r9, r9, 1
$BB58_41:                               # %while.body.i.i255
                                        #   Parent Loop BB58_3 Depth=1
                                        #     Parent Loop BB58_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB58_157 Depth 4
	ADDI      r3, r0, -1
	ADD      r30, r9, r3
	bslli     r3, r30, 2
	ADDI      r4, r1, 336
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r4, r3, r4
	SWI       r0, r1, 56
	SWI       r0, r1, 52
	SWI       r0, r1, 48
	LOAD      r3, r4, 0
	SWI        r3, r1, 48
	LOAD      r3, r4, 1
	SWI        r3, r1, 52
	LOAD      r3, r4, 2
	SWI        r3, r1, 56
	ADDI      r3, r4, 3
	LOAD      r5, r3, 0
	SWI        r5, r1, 60
	LOAD      r5, r3, 1
	SWI        r5, r1, 64
	LOAD      r3, r3, 2
	SWI        r3, r1, 68
	LWI       r3, r1, 632
	MULI      r27, r3, 12
	ADDI      r3, r1, 48
	ADD      r5, r3, r27
	ADDI      r10, r4, 7
	ADDI      r4, r4, 6
	LWI       r6, r1, 616
	MULI      r23, r6, 12
	LOAD      r31, r4, 0
	SWI       r31, r1, 72
	LOAD      r4, r10, 0
	SWI       r4, r1, 580
	SWI       r4, r1, 76
	LWI        r5, r5, 4
	LWI       r6, r1, 564
	FPRSUB     r5, r6, r5
	LWI       r4, r1, 604
	FPMUL      r29, r5, r4
	LW         r5, r3, r23
	LWI       r6, r1, 568
	FPRSUB     r5, r6, r5
	LWI       r4, r1, 608
	FPMUL      r10, r5, r4
	ADDI      r5, r0, 1
	FPGT   r6, r10, r29
	bneid     r6, ($BB58_43)
	NOP    
# BB#42:                                # %while.body.i.i255
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r5, r0, 0
$BB58_43:                               # %while.body.i.i255
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r5, ($BB58_44)
	NOP    
# BB#45:                                # %while.body.i.i255
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r4, r1, 624
	MULI      r21, r4, 12
	ADD      r5, r3, r21
	LWI        r5, r5, 4
	LWI       r6, r1, 564
	FPRSUB     r5, r6, r5
	LWI       r4, r1, 604
	FPMUL      r12, r5, r4
	LWI       r4, r1, 636
	MULI      r5, r4, 12
	LW         r3, r3, r5
	LWI       r6, r1, 568
	FPRSUB     r3, r6, r3
	LWI       r4, r1, 608
	FPMUL      r3, r3, r4
	FPGT   r6, r12, r3
	bneid     r6, ($BB58_47)
	ADDI      r11, r0, 1
# BB#46:                                # %while.body.i.i255
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_47:                               # %while.body.i.i255
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_48)
	NOP    
# BB#49:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r24, r12, r10
	ADDI      r20, r0, 0
	ADDI      r11, r0, 1
	bneid     r24, ($BB58_51)
	ADD      r22, r11, r0
# BB#50:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r22, r20, r0
$BB58_51:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r22, ($BB58_53)
	NOP    
# BB#52:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r12, r10, r0
$BB58_53:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r4, r1, 628
	MULI      r25, r4, 12
	ADDI      r28, r1, 48
	ADD      r10, r28, r25
	FPLT   r6, r29, r3
	bneid     r6, ($BB58_55)
	ADD      r22, r11, r0
# BB#54:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r22, r20, r0
$BB58_55:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI        r6, r10, 8
	LWI       r7, r1, 560
	FPRSUB     r6, r7, r6
	LWI       r4, r1, 600
	FPMUL      r10, r6, r4
	FPGT   r6, r12, r10
	bneid     r6, ($BB58_57)
	NOP    
# BB#56:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r11, r20, r0
$BB58_57:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r22, ($BB58_59)
	NOP    
# BB#58:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r29, r3, r0
$BB58_59:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_60)
	NOP    
# BB#61:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r3, r1, 620
	MULI      r22, r3, 12
	ADD      r3, r28, r22
	LWI        r3, r3, 8
	LWI       r6, r1, 560
	FPRSUB     r3, r6, r3
	LWI       r4, r1, 600
	FPMUL      r3, r3, r4
	FPGT   r6, r3, r29
	bneid     r6, ($BB58_63)
	ADDI      r11, r0, 1
# BB#62:                                # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_63:                               # %if.end.i.i133.i.i267
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_64)
	NOP    
# BB#65:                                # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r4, r26, r0
	FPLT   r26, r10, r29
	ADDI      r20, r0, 0
	ADDI      r11, r0, 1
	bneid     r26, ($BB58_67)
	ADD      r24, r11, r0
# BB#66:                                # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r24, r20, r0
$BB58_67:                               # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r24, ($BB58_69)
	ADD      r26, r4, r0
# BB#68:                                # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r10, r29, r0
$BB58_69:                               # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	ORI       r6, r0, 0
	FPLT   r6, r10, r6
	bneid     r6, ($BB58_71)
	NOP    
# BB#70:                                # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r11, r20, r0
$BB58_71:                               # %if.end72.i.i137.i.i271
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_72)
	NOP    
# BB#73:                                # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r24, r3, r12
	ADDI      r11, r0, 0
	ADDI      r10, r0, 1
	bneid     r24, ($BB58_75)
	ADD      r20, r10, r0
# BB#74:                                # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r20, r11, r0
$BB58_75:                               # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r20, ($BB58_77)
	NOP    
# BB#76:                                # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r3, r12, r0
$BB58_77:                               # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r4, r1, 572
	FPLT   r3, r4, r3
	bneid     r3, ($BB58_79)
	NOP    
# BB#78:                                # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r10, r11, r0
$BB58_79:                               # %if.end.i.i275
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r10, ($BB58_80)
	NOP    
# BB#81:                                # %if.end7.i.i276
                                        #   in Loop: Header=BB58_41 Depth=3
	beqid     r31, ($BB58_82)
	NOP    
# BB#83:                                # %if.end7.i.i276
                                        #   in Loop: Header=BB58_41 Depth=3
	SWI       r30, r1, 612
	ADD      r12, r0, r0
	SWI       r31, r1, 584
	ADDI      r3, r0, -1
	CMP       r3, r3, r31
	bneid     r3, ($BB58_157)
	NOP    
# BB#84:                                # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r3, r1, 580
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r3, r3, r4
	SWI       r0, r1, 88
	SWI       r0, r1, 84
	SWI       r0, r1, 80
	LOAD      r4, r3, 0
	SWI        r4, r1, 80
	LOAD      r4, r3, 1
	SWI        r4, r1, 84
	LOAD      r4, r3, 2
	SWI        r4, r1, 88
	ADDI      r4, r3, 3
	LOAD      r6, r4, 0
	SWI        r6, r1, 92
	LOAD      r6, r4, 1
	SWI        r6, r1, 96
	LOAD      r4, r4, 2
	SWI        r4, r1, 100
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 104
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 108
	ADDI      r10, r1, 80
	ADD      r3, r10, r27
	LWI        r3, r3, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 604
	FPMUL      r30, r3, r4
	LW         r3, r10, r23
	LWI       r4, r1, 568
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 608
	FPMUL      r3, r3, r4
	ADDI      r11, r0, 1
	FPGT   r4, r3, r30
	bneid     r4, ($BB58_86)
	NOP    
# BB#85:                                # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_86:                               # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r4, r0, r0
	ORI       r12, r0, 1203982336
	bneid     r11, ($BB58_116)
	NOP    
# BB#87:                                # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r6, r10, r21
	LW         r10, r10, r5
	LWI        r6, r6, 4
	LWI       r7, r1, 564
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 604
	FPMUL      r29, r6, r7
	LWI       r6, r1, 568
	FPRSUB     r6, r6, r10
	LWI       r7, r1, 608
	FPMUL      r10, r6, r7
	FPGT   r6, r29, r10
	bneid     r6, ($BB58_89)
	ADDI      r11, r0, 1
# BB#88:                                # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_89:                               # %if.then9.i.i301
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_116)
	NOP    
# BB#90:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r20, r29, r3
	ADDI      r4, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB58_92)
	ADD      r12, r11, r0
# BB#91:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r12, r4, r0
$BB58_92:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r12, ($BB58_94)
	NOP    
# BB#93:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r29, r3, r0
$BB58_94:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r20, r1, 80
	ADD      r3, r20, r25
	FPLT   r6, r30, r10
	bneid     r6, ($BB58_96)
	ADD      r12, r11, r0
# BB#95:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r12, r4, r0
$BB58_96:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI        r3, r3, 8
	LWI       r6, r1, 560
	FPRSUB     r3, r6, r3
	LWI       r6, r1, 600
	FPMUL      r3, r3, r6
	FPGT   r6, r29, r3
	bneid     r6, ($BB58_98)
	NOP    
# BB#97:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r11, r4, r0
$BB58_98:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r12, ($BB58_100)
	NOP    
# BB#99:                                # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r30, r10, r0
$BB58_100:                              # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r4, r0, r0
	ORI       r12, r0, 1203982336
	bneid     r11, ($BB58_116)
	NOP    
# BB#101:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r6, r20, r22
	LWI        r6, r6, 8
	LWI       r7, r1, 560
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 600
	FPMUL      r10, r6, r7
	FPGT   r6, r10, r30
	bneid     r6, ($BB58_103)
	ADDI      r11, r0, 1
# BB#102:                               # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_103:                              # %if.end.i.i59.i.i313
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_116)
	NOP    
# BB#104:                               # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	FPLT   r20, r3, r30
	ADDI      r4, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB58_106)
	ADD      r12, r11, r0
# BB#105:                               # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r12, r4, r0
$BB58_106:                              # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r12, ($BB58_108)
	NOP    
# BB#107:                               # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r3, r30, r0
$BB58_108:                              # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	ORI       r6, r0, 0
	FPLT   r3, r3, r6
	bneid     r3, ($BB58_110)
	NOP    
# BB#109:                               # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r11, r4, r0
$BB58_110:                              # %if.end72.i.i63.i.i317
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r4, r0, r0
	ORI       r12, r0, 1203982336
	bneid     r11, ($BB58_116)
	NOP    
# BB#111:                               # %if.end81.i.i67.i.i320
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r6, r10, r29
	ADDI      r4, r0, 1
	bneid     r6, ($BB58_113)
	ADD      r3, r4, r0
# BB#112:                               # %if.end81.i.i67.i.i320
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r3, r0, 0
$BB58_113:                              # %if.end81.i.i67.i.i320
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r3, ($BB58_115)
	NOP    
# BB#114:                               # %if.end81.i.i67.i.i320
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r10, r29, r0
$BB58_115:                              # %if.end81.i.i67.i.i320
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r12, r10, r0
$BB58_116:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI       r3, r1, 580
	ADDI      r29, r3, 1
	bslli     r3, r29, 3
	LWI       r6, r1, 588
	ADD      r3, r3, r6
	SWI       r0, r1, 120
	SWI       r0, r1, 116
	SWI       r0, r1, 112
	LOAD      r6, r3, 0
	SWI        r6, r1, 112
	LOAD      r6, r3, 1
	SWI        r6, r1, 116
	LOAD      r6, r3, 2
	SWI        r6, r1, 120
	ADDI      r10, r3, 3
	LOAD      r6, r10, 0
	SWI        r6, r1, 124
	LOAD      r6, r10, 1
	SWI        r6, r1, 128
	LOAD      r6, r10, 2
	SWI        r6, r1, 132
	ADDI      r6, r3, 6
	LOAD      r6, r6, 0
	SWI       r6, r1, 136
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 140
	ADDI      r3, r1, 112
	ADD      r6, r3, r27
	LWI        r6, r6, 4
	LWI       r7, r1, 564
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 604
	FPMUL      r27, r6, r7
	LW         r6, r3, r23
	LWI       r7, r1, 568
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 608
	FPMUL      r10, r6, r7
	ADDI      r11, r0, 1
	FPGT   r6, r10, r27
	bneid     r6, ($BB58_118)
	NOP    
# BB#117:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r11, r0, 0
$BB58_118:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r11, ($BB58_143)
	LWI       r24, r1, 612
# BB#119:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r6, r3, r21
	LWI        r6, r6, 4
	LWI       r7, r1, 564
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 604
	FPMUL      r21, r6, r7
	LW         r3, r3, r5
	LWI       r5, r1, 568
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 608
	FPMUL      r3, r3, r5
	FPGT   r6, r21, r3
	bneid     r6, ($BB58_121)
	ADDI      r5, r0, 1
# BB#120:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r5, r0, 0
$BB58_121:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i347
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r5, ($BB58_143)
	NOP    
# BB#122:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	FPGT   r23, r21, r10
	ADDI      r11, r0, 0
	ADDI      r5, r0, 1
	bneid     r23, ($BB58_124)
	ADD      r20, r5, r0
# BB#123:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r20, r11, r0
$BB58_124:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r20, ($BB58_126)
	NOP    
# BB#125:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r21, r10, r0
$BB58_126:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r20, r1, 112
	ADD      r10, r20, r25
	FPLT   r6, r27, r3
	bneid     r6, ($BB58_128)
	ADD      r23, r5, r0
# BB#127:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r23, r11, r0
$BB58_128:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	LWI        r6, r10, 8
	LWI       r7, r1, 560
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 600
	FPMUL      r10, r6, r7
	FPGT   r6, r21, r10
	bneid     r6, ($BB58_130)
	NOP    
# BB#129:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r5, r11, r0
$BB58_130:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r23, ($BB58_132)
	NOP    
# BB#131:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r27, r3, r0
$BB58_132:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r5, ($BB58_143)
	NOP    
# BB#133:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r3, r20, r22
	LWI        r3, r3, 8
	LWI       r5, r1, 560
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 600
	FPMUL      r5, r3, r5
	FPGT   r6, r5, r27
	bneid     r6, ($BB58_135)
	ADDI      r3, r0, 1
# BB#134:                               # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r3, r0, 0
$BB58_135:                              # %if.end.i.i7.i.i359
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r3, ($BB58_143)
	NOP    
# BB#136:                               # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	FPLT   r22, r10, r27
	ADDI      r11, r0, 0
	ADDI      r3, r0, 1
	bneid     r22, ($BB58_138)
	ADD      r20, r3, r0
# BB#137:                               # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r20, r11, r0
$BB58_138:                              # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r20, ($BB58_140)
	NOP    
# BB#139:                               # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r10, r27, r0
$BB58_140:                              # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	ORI       r6, r0, 0
	FPGE   r7, r10, r6
	FPUN   r6, r10, r6
	BITOR        r6, r6, r7
	bneid     r6, ($BB58_142)
	NOP    
# BB#141:                               # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	ADD      r3, r11, r0
$BB58_142:                              # %if.end72.i.i.i.i363
                                        #   in Loop: Header=BB58_41 Depth=3
	bneid     r3, ($BB58_146)
	NOP    
$BB58_143:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i.i364
                                        #   in Loop: Header=BB58_41 Depth=3
	ADDI      r3, r0, 1
	CMP       r3, r3, r4
	bneid     r3, ($BB58_144)
	NOP    
# BB#145:                               # %if.then21.i.i365
                                        #   in Loop: Header=BB58_41 Depth=3
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	LWI       r4, r1, 580
	brid      ($BB58_182)
	SWI       r4, r3, -4
$BB58_44:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_48:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_60:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_157:                              # %for.body.i.i.i425
                                        #   Parent Loop BB58_3 Depth=1
                                        #     Parent Loop BB58_4 Depth=2
                                        #       Parent Loop BB58_41 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	MULI      r3, r12, 11
	LWI       r4, r1, 580
	ADD      r5, r3, r4
	LOAD      r25, r5, 0
	LOAD      r27, r5, 1
	LOAD      r30, r5, 2
	ADDI      r3, r5, 3
	LOAD      r11, r3, 0
	LOAD      r23, r3, 1
	LOAD      r3, r3, 2
	ADDI      r6, r5, 6
	LOAD      r10, r6, 0
	LOAD      r7, r6, 1
	LOAD      r6, r6, 2
	FPRSUB     r22, r30, r6
	FPRSUB     r29, r27, r7
	FPMUL      r6, r8, r29
	FPMUL      r7, r26, r22
	FPRSUB     r9, r6, r7
	FPRSUB     r10, r25, r10
	FPMUL      r6, r8, r10
	LWI       r28, r1, 576
	FPMUL      r7, r28, r22
	FPRSUB     r21, r7, r6
	FPRSUB     r20, r25, r11
	FPRSUB     r11, r27, r23
	FPMUL      r6, r21, r11
	FPMUL      r7, r9, r20
	FPADD      r24, r7, r6
	FPMUL      r6, r26, r10
	ADD      r4, r26, r0
	FPMUL      r7, r28, r29
	FPRSUB     r23, r6, r7
	FPRSUB     r28, r30, r3
	FPMUL      r3, r23, r28
	FPADD      r26, r24, r3
	ORI       r3, r0, 0
	FPGE   r6, r26, r3
	FPUN   r3, r26, r3
	BITOR        r3, r3, r6
	bneid     r3, ($BB58_159)
	ADDI      r24, r0, 1
# BB#158:                               # %for.body.i.i.i425
                                        #   in Loop: Header=BB58_157 Depth=4
	ADDI      r24, r0, 0
$BB58_159:                              # %for.body.i.i.i425
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r24, ($BB58_161)
	ADD      r3, r26, r0
# BB#160:                               # %if.then.i.i.i.i33.i427
                                        #   in Loop: Header=BB58_157 Depth=4
	FPNEG      r3, r26
$BB58_161:                              # %_ZN4util4fabsERKf.exit.i.i.i36.i430
                                        #   in Loop: Header=BB58_157 Depth=4
	ORI       r6, r0, 953267991
	FPLT   r6, r3, r6
	bneid     r6, ($BB58_163)
	ADDI      r3, r0, 1
# BB#162:                               # %_ZN4util4fabsERKf.exit.i.i.i36.i430
                                        #   in Loop: Header=BB58_157 Depth=4
	ADDI      r3, r0, 0
$BB58_163:                              # %_ZN4util4fabsERKf.exit.i.i.i36.i430
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r3, ($BB58_164)
	NOP    
# BB#165:                               # %if.end.i.i.i.i439
                                        #   in Loop: Header=BB58_157 Depth=4
	ADD      r31, r8, r0
	LWI       r3, r1, 560
	FPRSUB     r30, r30, r3
	LWI       r3, r1, 568
	FPRSUB     r25, r25, r3
	LWI       r3, r1, 564
	FPRSUB     r27, r27, r3
	FPMUL      r3, r27, r20
	FPMUL      r24, r25, r11
	FPMUL      r20, r30, r20
	FPMUL      r6, r25, r28
	FPMUL      r7, r30, r11
	FPMUL      r8, r27, r28
	FPRSUB     r11, r3, r24
	FPRSUB     r28, r6, r20
	FPRSUB     r20, r7, r8
	FPMUL      r3, r28, r29
	FPMUL      r6, r20, r10
	FPADD      r3, r6, r3
	FPMUL      r6, r11, r22
	FPADD      r6, r3, r6
	ORI       r3, r0, 1065353216
	FPDIV      r3, r3, r26
	FPMUL      r22, r6, r3
	ORI       r6, r0, 0
	FPLT   r6, r22, r6
	bneid     r6, ($BB58_167)
	ADDI      r10, r0, 1
# BB#166:                               # %if.end.i.i.i.i439
                                        #   in Loop: Header=BB58_157 Depth=4
	ADDI      r10, r0, 0
$BB58_167:                              # %if.end.i.i.i.i439
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r10, ($BB58_168)
	ADD      r26, r4, r0
# BB#169:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	FPMUL      r6, r21, r27
	FPMUL      r7, r9, r25
	FPADD      r6, r7, r6
	FPMUL      r7, r28, r26
	LWI       r4, r1, 576
	FPMUL      r8, r20, r4
	FPMUL      r9, r23, r30
	FPADD      r10, r6, r9
	FPADD      r6, r8, r7
	ADD      r8, r31, r0
	FPMUL      r7, r11, r8
	FPADD      r6, r6, r7
	FPMUL      r9, r6, r3
	FPMUL      r3, r10, r3
	FPADD      r6, r3, r9
	ORI       r7, r0, 1065353216
	FPLE   r21, r6, r7
	ORI       r6, r0, 0
	FPGE   r3, r3, r6
	FPGE   r20, r9, r6
	LWI       r4, r1, 572
	FPGE   r6, r22, r4
	FPUN   r7, r22, r4
	BITOR        r23, r7, r6
	ADDI      r11, r0, 0
	ADDI      r10, r0, 1
	bneid     r21, ($BB58_171)
	ADD      r9, r10, r0
# BB#170:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	ADD      r9, r11, r0
$BB58_171:                              # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r23, ($BB58_173)
	ADD      r21, r10, r0
# BB#172:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	ADD      r21, r11, r0
$BB58_173:                              # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r20, ($BB58_175)
	ADD      r23, r10, r0
# BB#174:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	ADD      r23, r11, r0
$BB58_175:                              # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r3, ($BB58_177)
	NOP    
# BB#176:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	ADD      r10, r11, r0
$BB58_177:                              # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	bneid     r21, ($BB58_180)
	NOP    
# BB#178:                               # %if.end9.i.i.i.i461
                                        #   in Loop: Header=BB58_157 Depth=4
	BITAND       r3, r10, r23
	BITAND       r3, r3, r9
	ADDI      r6, r0, 1
	CMP       r3, r6, r3
	bneid     r3, ($BB58_180)
	NOP    
# BB#179:                               # %_ZNK10syTriangle6ID_MATEv.exit.i.i.i.i464
                                        #   in Loop: Header=BB58_157 Depth=4
	ADDI      r3, r5, 10
	LOAD      r3, r3, 0
	SWI       r3, r1, 592
	SWI       r5, r1, 596
	brid      ($BB58_180)
	SWI       r22, r1, 572
$BB58_164:                              #   in Loop: Header=BB58_157 Depth=4
	brid      ($BB58_180)
	ADD      r26, r4, r0
$BB58_168:                              #   in Loop: Header=BB58_157 Depth=4
	ADD      r8, r31, r0
$BB58_180:                              # %_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord.exit.i.i.i467
                                        #   in Loop: Header=BB58_157 Depth=4
	ADDI      r12, r12, 1
	LWI       r3, r1, 584
	CMP       r3, r3, r12
	bneid     r3, ($BB58_157)
	NOP    
# BB#181:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	LWI       r9, r1, 612
$BB58_64:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_72:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_80:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_82:                               #   in Loop: Header=BB58_41 Depth=3
	brid      ($BB58_182)
	ADD      r9, r30, r0
$BB58_144:                              #   in Loop: Header=BB58_41 Depth=3
	ADD      r9, r24, r0
$BB58_182:                              # %while.cond.backedge.i.i470
                                        #   in Loop: Header=BB58_41 Depth=3
	bgtid     r9, ($BB58_41)
	NOP    
# BB#183:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i472
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r3, r0, 1203982336
	LWI       r4, r1, 572
	FPNE   r4, r4, r3
	bneid     r4, ($BB58_185)
	ADDI      r3, r0, 1
# BB#184:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i472
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r3, r0, 0
$BB58_185:                              # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i472
                                        #   in Loop: Header=BB58_4 Depth=2
	beqid     r3, ($BB58_186)
	NOP    
# BB#209:                               # %_ZNK10syTriangle6NormalEv.exit.i.i516
                                        #   in Loop: Header=BB58_4 Depth=2
	LWI       r7, r1, 596
	LOAD      r3, r7, 0
	LOAD      r4, r7, 1
	LOAD      r9, r7, 2
	ADDI      r6, r7, 3
	LOAD      r5, r6, 0
	LOAD      r10, r6, 1
	LOAD      r20, r6, 2
	ADDI      r6, r7, 6
	LOAD      r11, r6, 0
	LOAD      r21, r6, 1
	LOAD      r6, r6, 2
	FPRSUB     r12, r6, r9
	FPRSUB     r20, r20, r9
	FPRSUB     r9, r10, r4
	FPRSUB     r10, r21, r4
	FPMUL      r4, r20, r10
	FPMUL      r6, r9, r12
	FPRSUB     r4, r4, r6
	FPRSUB     r5, r5, r3
	FPRSUB     r11, r11, r3
	FPMUL      r3, r20, r11
	FPMUL      r6, r5, r12
	FPRSUB     r3, r6, r3
	FPMUL      r6, r3, r3
	FPMUL      r7, r4, r4
	FPADD      r12, r7, r6
	FPMUL      r6, r9, r11
	FPMUL      r5, r5, r10
	FPRSUB     r5, r6, r5
	FPMUL      r6, r5, r5
	FPADD      r6, r12, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r9, r7, r6
	FPDIV      r3, r3, r9
	SWI       r3, r1, 632
	FPMUL      r3, r3, r26
	FPDIV      r22, r4, r9
	LWI       r4, r1, 576
	FPMUL      r4, r22, r4
	FPADD      r3, r4, r3
	FPDIV      r4, r5, r9
	SWI       r4, r1, 628
	FPMUL      r4, r4, r8
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB58_211)
	ADDI      r3, r0, 1
# BB#210:                               # %_ZNK10syTriangle6NormalEv.exit.i.i516
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r3, r0, 0
$BB58_211:                              # %_ZNK10syTriangle6NormalEv.exit.i.i516
                                        #   in Loop: Header=BB58_4 Depth=2
	bneid     r3, ($BB58_213)
	NOP    
# BB#212:                               # %if.then3.i.i520
                                        #   in Loop: Header=BB58_4 Depth=2
	LWI       r3, r1, 628
	FPNEG      r3, r3
	SWI       r3, r1, 628
	LWI       r3, r1, 632
	FPNEG      r3, r3
	SWI       r3, r1, 632
	FPNEG      r22, r22
$BB58_213:                              # %if.then3.i.i520
                                        #   in Loop: Header=BB58_4 Depth=2
	LWI       r5, r1, 576
	LWI       r10, r1, 572
	FPMUL      r3, r26, r10
	LWI       r4, r1, 564
	FPADD      r3, r4, r3
	FPMUL      r4, r5, r10
	LWI       r5, r1, 568
	FPADD      r4, r5, r4
	LWI       r5, r1, 696
	FPRSUB     r5, r4, r5
	LWI       r6, r1, 700
	FPRSUB     r9, r3, r6
	FPMUL      r6, r9, r9
	FPMUL      r7, r5, r5
	FPADD      r7, r7, r6
	FPMUL      r6, r8, r10
	LWI       r8, r1, 560
	FPADD      r6, r8, r6
	LWI       r8, r1, 704
	FPRSUB     r10, r6, r8
	FPMUL      r8, r10, r10
	FPADD      r8, r7, r8
	LWI       r7, r1, 592
	MULI      r7, r7, 25
	LWI       r11, r1, 712
	ADD      r11, r11, r7
	ORI       r7, r0, 1028443341
	LOAD      r12, r11, 0
	SWI       r12, r1, 636
	LOAD      r12, r11, 1
	SWI       r12, r1, 640
	LOAD      r11, r11, 2
	SWI       r11, r1, 644
	FPINVSQRT r11, r8
	SWI       r11, r1, 692
	FPINVSQRT r11, r8
	ORI       r8, r0, 1065353216
	FPDIV      r11, r8, r11
	FPDIV      r12, r5, r11
	SWI       r12, r1, 680
	FPDIV      r21, r9, r11
	SWI       r21, r1, 684
	FPMUL      r5, r21, r21
	FPMUL      r9, r12, r12
	FPADD      r5, r9, r5
	FPDIV      r23, r10, r11
	SWI       r23, r1, 688
	FPMUL      r9, r23, r23
	FPADD      r5, r5, r9
	FPINVSQRT r5, r5
	FPDIV      r10, r8, r5
	ORI       r9, r0, 0
	FPDIV      r25, r12, r10
	FPDIV      r27, r8, r25
	SWI       r27, r1, 620
	FPGE   r11, r27, r9
	FPUN   r20, r27, r9
	FPDIV      r5, r21, r10
	SWI       r5, r1, 572
	FPDIV      r24, r8, r5
	FPGE   r5, r24, r9
	FPUN   r12, r24, r9
	BITOR        r21, r12, r5
	FPDIV      r5, r23, r10
	SWI       r5, r1, 576
	FPDIV      r12, r8, r5
	SWI       r12, r1, 584
	FPGE   r5, r12, r9
	FPUN   r8, r12, r9
	BITOR        r23, r8, r5
	FPLT   r8, r27, r9
	FPLT   r10, r24, r9
	FPLT   r12, r12, r9
	ADDI      r9, r0, 0
	ADDI      r31, r0, 1
	bneid     r23, ($BB58_215)
	SWI       r31, r1, 596
# BB#214:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 596
$BB58_215:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	ADD      r5, r22, r0
	BITOR        r11, r20, r11
	bneid     r21, ($BB58_217)
	SWI       r31, r1, 600
# BB#216:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 600
$BB58_217:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	bneid     r11, ($BB58_219)
	SWI       r31, r1, 604
# BB#218:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 604
$BB58_219:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	bneid     r12, ($BB58_221)
	SWI       r31, r1, 608
# BB#220:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 608
$BB58_221:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r31, r1, 612
	ADD      r22, r24, r0
	bneid     r10, ($BB58_223)
	SWI       r22, r1, 624
# BB#222:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 612
$BB58_223:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	FPMUL      r10, r5, r7
	SWI       r5, r1, 676
	LWI       r5, r1, 632
	FPMUL      r11, r5, r7
	LWI       r5, r1, 628
	FPMUL      r7, r5, r7
	bneid     r8, ($BB58_225)
	SWI       r31, r1, 616
# BB#224:                               # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	SWI       r9, r1, 616
$BB58_225:                              # %if.end6.i.i593
                                        #   in Loop: Header=BB58_4 Depth=2
	FPADD      r5, r6, r7
	SWI       r5, r1, 560
	FPADD      r3, r3, r11
	SWI       r3, r1, 564
	FPADD      r3, r4, r10
	SWI       r3, r1, 568
	ORI       r10, r0, 1203982336
	brid      ($BB58_226)
	SWI       r0, r1, 336
$BB58_355:                              # %if.else24.i.i.i732
                                        #   in Loop: Header=BB58_226 Depth=3
	beqid     r12, ($BB58_356)
	NOP    
# BB#357:                               # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r11, r6, r3
	ADDI      r8, r0, 0
	ADDI      r7, r0, 1
	bneid     r11, ($BB58_359)
	ADD      r9, r7, r0
# BB#358:                               # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r9, r8, r0
$BB58_359:                              # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r9, ($BB58_361)
	NOP    
# BB#360:                               # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r6, r3, r0
$BB58_361:                              # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGE   r3, r23, r6
	FPUN   r4, r23, r6
	BITOR        r3, r4, r3
	bneid     r3, ($BB58_363)
	NOP    
# BB#362:                               # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r7, r8, r0
$BB58_363:                              # %if.else31.i.i.i737
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r7, ($BB58_365)
	NOP    
# BB#364:                               # %if.then35.i.i.i740
                                        #   in Loop: Header=BB58_226 Depth=3
	bslli     r3, r31, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	SWI       r30, r5, -4
	LWI       r5, r1, 580
	SW        r5, r4, r3
	brid      ($BB58_366)
	NOP    
$BB58_356:                              # %if.then28.i.i.i733
                                        #   in Loop: Header=BB58_226 Depth=3
	bslli     r3, r31, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	brid      ($BB58_367)
	SWI       r30, r3, -4
$BB58_365:                              # %if.else40.i.i.i743
                                        #   in Loop: Header=BB58_226 Depth=3
	bslli     r3, r31, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	LWI       r6, r1, 580
	SWI       r6, r5, -4
	SW        r30, r4, r3
$BB58_366:                              # %if.else40.i.i.i743
                                        #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADDI      r31, r31, 1
$BB58_226:                              # %while.body.i.i.i621
                                        #   Parent Loop BB58_3 Depth=1
                                        #     Parent Loop BB58_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB58_270 Depth 4
	ADDI      r3, r0, -1
	ADD      r30, r31, r3
	bslli     r3, r30, 2
	ADDI      r4, r1, 336
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r3, r3, r4
	SWI       r0, r1, 148
	SWI       r0, r1, 144
	LOAD      r4, r3, 0
	SWI        r4, r1, 144
	LOAD      r4, r3, 1
	SWI        r4, r1, 148
	LOAD      r4, r3, 2
	SWI        r4, r1, 152
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 156
	LOAD      r5, r4, 1
	SWI        r5, r1, 160
	LOAD      r4, r4, 2
	SWI        r4, r1, 164
	LWI       r4, r1, 600
	MULI      r9, r4, 12
	ADDI      r8, r1, 144
	ADD      r6, r8, r9
	ADDI      r4, r3, 7
	ADDI      r3, r3, 6
	LWI       r5, r1, 616
	MULI      r24, r5, 12
	LOAD      r12, r3, 0
	SWI       r12, r1, 168
	LOAD      r3, r4, 0
	SWI       r3, r1, 580
	SWI       r3, r1, 172
	LWI        r3, r6, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	FPMUL      r21, r3, r22
	LW         r3, r8, r24
	LWI       r4, r1, 568
	FPRSUB     r3, r4, r3
	FPMUL      r6, r3, r27
	ADDI      r3, r0, 1
	FPGT   r4, r6, r21
	bneid     r4, ($BB58_228)
	NOP    
# BB#227:                               # %while.body.i.i.i621
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r3, r0, 0
$BB58_228:                              # %while.body.i.i.i621
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r3, ($BB58_229)
	NOP    
# BB#230:                               # %while.body.i.i.i621
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI       r3, r1, 612
	MULI      r3, r3, 12
	ADD      r4, r8, r3
	LWI        r4, r4, 4
	LWI       r5, r1, 564
	FPRSUB     r4, r5, r4
	FPMUL      r29, r4, r22
	LWI       r4, r1, 604
	MULI      r7, r4, 12
	LW         r4, r8, r7
	LWI       r5, r1, 568
	FPRSUB     r4, r5, r4
	FPMUL      r8, r4, r27
	FPGT   r4, r29, r8
	bneid     r4, ($BB58_232)
	ADDI      r11, r0, 1
# BB#231:                               # %while.body.i.i.i621
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r11, r0, 0
$BB58_232:                              # %while.body.i.i.i621
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r11, ($BB58_233)
	NOP    
# BB#234:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r26, r29, r6
	ADDI      r23, r0, 0
	ADDI      r20, r0, 1
	bneid     r26, ($BB58_236)
	ADD      r11, r20, r0
# BB#235:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r11, r23, r0
$BB58_236:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r11, ($BB58_238)
	NOP    
# BB#237:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r29, r6, r0
$BB58_238:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI       r4, r1, 596
	MULI      r6, r4, 12
	ADDI      r26, r1, 144
	ADD      r11, r26, r6
	FPLT   r4, r21, r8
	bneid     r4, ($BB58_240)
	ADD      r28, r20, r0
# BB#239:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r28, r23, r0
$BB58_240:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI        r4, r11, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r11, r4, r5
	FPGT   r4, r29, r11
	bneid     r4, ($BB58_242)
	NOP    
# BB#241:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r23, r0
$BB58_242:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r28, ($BB58_244)
	NOP    
# BB#243:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r21, r8, r0
$BB58_244:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_245)
	NOP    
# BB#246:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI       r4, r1, 608
	MULI      r8, r4, 12
	ADD      r4, r26, r8
	LWI        r4, r4, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r23, r4, r5
	FPGT   r4, r23, r21
	bneid     r4, ($BB58_248)
	ADDI      r20, r0, 1
# BB#247:                               # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r20, r0, 0
$BB58_248:                              # %if.end.i.i133.i.i.i633
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_249)
	NOP    
# BB#250:                               # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r5, r27, r0
	FPLT   r28, r11, r21
	ADDI      r26, r0, 0
	ADDI      r20, r0, 1
	bneid     r28, ($BB58_252)
	ADD      r27, r20, r0
# BB#251:                               # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r27, r26, r0
$BB58_252:                              # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r27, ($BB58_254)
	NOP    
# BB#253:                               # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r11, r21, r0
$BB58_254:                              # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	ORI       r4, r0, 0
	FPLT   r4, r11, r4
	bneid     r4, ($BB58_256)
	ADD      r27, r5, r0
# BB#255:                               # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r26, r0
$BB58_256:                              # %if.end72.i.i137.i.i.i637
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_257)
	NOP    
# BB#258:                               # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r26, r23, r29
	ADDI      r20, r0, 0
	ADDI      r11, r0, 1
	bneid     r26, ($BB58_260)
	ADD      r21, r11, r0
# BB#259:                               # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r21, r20, r0
$BB58_260:                              # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r21, ($BB58_262)
	NOP    
# BB#261:                               # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r23, r29, r0
$BB58_262:                              # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	FPLT   r4, r10, r23
	bneid     r4, ($BB58_264)
	NOP    
# BB#263:                               # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r11, r20, r0
$BB58_264:                              # %if.end.i.i.i641
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r11, ($BB58_265)
	NOP    
# BB#266:                               # %if.end7.i.i.i642
                                        #   in Loop: Header=BB58_226 Depth=3
	beqid     r12, ($BB58_267)
	NOP    
# BB#268:                               # %if.end7.i.i.i642
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r29, r0, r0
	ADDI      r4, r0, -1
	CMP       r4, r4, r12
	bneid     r4, ($BB58_269)
	NOP    
# BB#293:                               # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI       r4, r1, 580
	bslli     r4, r4, 3
	LWI       r5, r1, 588
	ADD      r11, r4, r5
	SWI       r0, r1, 180
	SWI       r0, r1, 176
	LOAD      r4, r11, 0
	SWI        r4, r1, 176
	LOAD      r4, r11, 1
	SWI        r4, r1, 180
	LOAD      r4, r11, 2
	SWI        r4, r1, 184
	ADDI      r12, r11, 3
	LOAD      r4, r12, 0
	SWI        r4, r1, 188
	LOAD      r4, r12, 1
	SWI        r4, r1, 192
	LOAD      r4, r12, 2
	SWI        r4, r1, 196
	ADDI      r4, r11, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 200
	ADDI      r4, r11, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 204
	ADDI      r20, r1, 176
	ADD      r4, r20, r9
	LWI        r4, r4, 4
	LWI       r5, r1, 564
	FPRSUB     r4, r5, r4
	FPMUL      r26, r4, r22
	LW         r4, r20, r24
	LWI       r5, r1, 568
	FPRSUB     r4, r5, r4
	FPMUL      r11, r4, r27
	ADDI      r21, r0, 1
	FPGT   r4, r11, r26
	bneid     r4, ($BB58_295)
	NOP    
# BB#294:                               # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r21, r0, 0
$BB58_295:                              # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r28, r30, r0
	ADD      r12, r0, r0
	ORI       r23, r0, 1203982336
	bneid     r21, ($BB58_325)
	NOP    
# BB#296:                               # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r4, r20, r3
	LW         r20, r20, r7
	LWI        r4, r4, 4
	LWI       r5, r1, 564
	FPRSUB     r4, r5, r4
	FPMUL      r30, r4, r22
	LWI       r4, r1, 568
	FPRSUB     r4, r4, r20
	FPMUL      r29, r4, r27
	FPGT   r4, r30, r29
	bneid     r4, ($BB58_298)
	ADDI      r20, r0, 1
# BB#297:                               # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r20, r0, 0
$BB58_298:                              # %if.then9.i.i.i667
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_325)
	NOP    
# BB#299:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r23, r30, r11
	ADDI      r12, r0, 0
	ADDI      r20, r0, 1
	bneid     r23, ($BB58_301)
	ADD      r21, r20, r0
# BB#300:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r21, r12, r0
$BB58_301:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r21, ($BB58_303)
	NOP    
# BB#302:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r30, r11, r0
$BB58_303:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r11, r1, 176
	ADD      r21, r11, r6
	FPLT   r4, r26, r29
	bneid     r4, ($BB58_305)
	ADD      r23, r20, r0
# BB#304:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r23, r12, r0
$BB58_305:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI        r4, r21, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r21, r4, r5
	FPGT   r4, r30, r21
	bneid     r4, ($BB58_307)
	NOP    
# BB#306:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r12, r0
$BB58_307:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r23, ($BB58_309)
	NOP    
# BB#308:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r26, r29, r0
$BB58_309:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r12, r0, r0
	ORI       r23, r0, 1203982336
	bneid     r20, ($BB58_325)
	NOP    
# BB#310:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r4, r11, r8
	LWI        r4, r4, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r29, r4, r5
	FPGT   r4, r29, r26
	bneid     r4, ($BB58_312)
	ADDI      r11, r0, 1
# BB#311:                               # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r11, r0, 0
$BB58_312:                              # %if.end.i.i59.i.i.i679
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r11, ($BB58_325)
	NOP    
# BB#313:                               # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	FPLT   r23, r21, r26
	ADDI      r12, r0, 0
	ADDI      r11, r0, 1
	bneid     r23, ($BB58_315)
	ADD      r20, r11, r0
# BB#314:                               # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r12, r0
$BB58_315:                              # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_317)
	NOP    
# BB#316:                               # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r21, r26, r0
$BB58_317:                              # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	ORI       r4, r0, 0
	FPLT   r4, r21, r4
	bneid     r4, ($BB58_319)
	NOP    
# BB#318:                               # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r11, r12, r0
$BB58_319:                              # %if.end72.i.i63.i.i.i683
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r12, r0, r0
	ORI       r23, r0, 1203982336
	bneid     r11, ($BB58_325)
	NOP    
# BB#320:                               # %if.end81.i.i67.i.i.i686
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r4, r29, r30
	ADDI      r12, r0, 1
	bneid     r4, ($BB58_322)
	ADD      r11, r12, r0
# BB#321:                               # %if.end81.i.i67.i.i.i686
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r11, r0, 0
$BB58_322:                              # %if.end81.i.i67.i.i.i686
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r11, ($BB58_324)
	NOP    
# BB#323:                               # %if.end81.i.i67.i.i.i686
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r29, r30, r0
$BB58_324:                              # %if.end81.i.i67.i.i.i686
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r23, r29, r0
$BB58_325:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI       r4, r1, 580
	ADDI      r30, r4, 1
	bslli     r4, r30, 3
	LWI       r5, r1, 588
	ADD      r11, r4, r5
	SWI       r0, r1, 212
	SWI       r0, r1, 208
	LOAD      r4, r11, 0
	SWI        r4, r1, 208
	LOAD      r4, r11, 1
	SWI        r4, r1, 212
	LOAD      r4, r11, 2
	SWI        r4, r1, 216
	ADDI      r20, r11, 3
	LOAD      r4, r20, 0
	SWI        r4, r1, 220
	LOAD      r4, r20, 1
	SWI        r4, r1, 224
	LOAD      r4, r20, 2
	SWI        r4, r1, 228
	ADDI      r4, r11, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 232
	ADDI      r4, r11, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 236
	ADDI      r20, r1, 208
	ADD      r4, r20, r9
	LWI        r4, r4, 4
	LWI       r5, r1, 564
	FPRSUB     r4, r5, r4
	FPMUL      r9, r4, r22
	LW         r4, r20, r24
	LWI       r5, r1, 568
	FPRSUB     r4, r5, r4
	FPMUL      r11, r4, r27
	ADDI      r21, r0, 1
	FPGT   r4, r11, r9
	bneid     r4, ($BB58_327)
	NOP    
# BB#326:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r21, r0, 0
$BB58_327:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r21, ($BB58_352)
	NOP    
# BB#328:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r3, r20, r3
	LWI        r3, r3, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	FPMUL      r3, r3, r22
	LW         r4, r20, r7
	LWI       r5, r1, 568
	FPRSUB     r4, r5, r4
	FPMUL      r7, r4, r27
	FPGT   r4, r3, r7
	bneid     r4, ($BB58_330)
	ADDI      r20, r0, 1
# BB#329:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r20, r0, 0
$BB58_330:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i713
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_352)
	NOP    
# BB#331:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	FPGT   r26, r3, r11
	ADDI      r21, r0, 0
	ADDI      r20, r0, 1
	bneid     r26, ($BB58_333)
	ADD      r24, r20, r0
# BB#332:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r24, r21, r0
$BB58_333:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r24, ($BB58_335)
	NOP    
# BB#334:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r3, r11, r0
$BB58_335:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r24, r1, 208
	ADD      r11, r24, r6
	FPLT   r4, r9, r7
	bneid     r4, ($BB58_337)
	ADD      r6, r20, r0
# BB#336:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r6, r21, r0
$BB58_337:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	LWI        r4, r11, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r11, r4, r5
	FPGT   r4, r3, r11
	bneid     r4, ($BB58_339)
	NOP    
# BB#338:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r21, r0
$BB58_339:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r6, ($BB58_341)
	NOP    
# BB#340:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r9, r7, r0
$BB58_341:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_352)
	NOP    
# BB#342:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r4, r24, r8
	LWI        r4, r4, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 584
	FPMUL      r6, r4, r5
	FPGT   r4, r6, r9
	bneid     r4, ($BB58_344)
	ADDI      r7, r0, 1
# BB#343:                               # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r7, r0, 0
$BB58_344:                              # %if.end.i.i7.i.i.i725
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r7, ($BB58_352)
	NOP    
# BB#345:                               # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	FPLT   r21, r11, r9
	ADDI      r8, r0, 0
	ADDI      r7, r0, 1
	bneid     r21, ($BB58_347)
	ADD      r20, r7, r0
# BB#346:                               # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r20, r8, r0
$BB58_347:                              # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r20, ($BB58_349)
	NOP    
# BB#348:                               # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r11, r9, r0
$BB58_349:                              # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	ORI       r4, r0, 0
	FPGE   r5, r11, r4
	FPUN   r4, r11, r4
	BITOR        r4, r4, r5
	bneid     r4, ($BB58_351)
	NOP    
# BB#350:                               # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	ADD      r7, r8, r0
$BB58_351:                              # %if.end72.i.i.i.i.i729
                                        #   in Loop: Header=BB58_226 Depth=3
	bneid     r7, ($BB58_355)
	NOP    
$BB58_352:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i.i.i730
                                        #   in Loop: Header=BB58_226 Depth=3
	ADDI      r3, r0, 1
	CMP       r3, r3, r12
	bneid     r3, ($BB58_353)
	NOP    
# BB#354:                               # %if.then21.i.i.i731
                                        #   in Loop: Header=BB58_226 Depth=3
	bslli     r3, r31, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	LWI       r4, r1, 580
	brid      ($BB58_367)
	SWI       r4, r3, -4
$BB58_229:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_233:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_245:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_249:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_257:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_265:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_267:                              #   in Loop: Header=BB58_226 Depth=3
	brid      ($BB58_367)
	ADD      r31, r30, r0
$BB58_269:                              #   in Loop: Header=BB58_226 Depth=3
	SWI       r30, r1, 592
$BB58_270:                              # %for.body.i.i.i.i791
                                        #   Parent Loop BB58_3 Depth=1
                                        #     Parent Loop BB58_4 Depth=2
                                        #       Parent Loop BB58_226 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	MULI      r3, r29, 11
	LWI       r4, r1, 580
	ADD      r6, r3, r4
	LOAD      r9, r6, 0
	LOAD      r21, r6, 1
	LOAD      r30, r6, 2
	ADDI      r3, r6, 6
	ADDI      r4, r6, 3
	LOAD      r7, r4, 0
	LOAD      r11, r4, 1
	LOAD      r23, r4, 2
	LOAD      r6, r3, 0
	LOAD      r4, r3, 1
	LOAD      r3, r3, 2
	FPRSUB     r8, r30, r3
	FPRSUB     r24, r21, r4
	LWI       r5, r1, 576
	FPMUL      r3, r5, r24
	LWI       r22, r1, 572
	FPMUL      r4, r22, r8
	FPRSUB     r3, r3, r4
	FPRSUB     r31, r9, r6
	FPMUL      r4, r5, r31
	FPMUL      r5, r25, r8
	FPRSUB     r6, r5, r4
	FPRSUB     r20, r9, r7
	FPRSUB     r11, r21, r11
	FPMUL      r4, r6, r11
	FPMUL      r5, r3, r20
	FPADD      r27, r5, r4
	FPMUL      r4, r22, r31
	FPMUL      r5, r25, r24
	FPRSUB     r7, r4, r5
	FPRSUB     r26, r30, r23
	FPMUL      r4, r7, r26
	FPADD      r27, r27, r4
	ORI       r4, r0, 0
	FPGE   r5, r27, r4
	FPUN   r4, r27, r4
	BITOR        r4, r4, r5
	bneid     r4, ($BB58_272)
	ADDI      r28, r0, 1
# BB#271:                               # %for.body.i.i.i.i791
                                        #   in Loop: Header=BB58_270 Depth=4
	ADDI      r28, r0, 0
$BB58_272:                              # %for.body.i.i.i.i791
                                        #   in Loop: Header=BB58_270 Depth=4
	bneid     r28, ($BB58_274)
	ADD      r23, r27, r0
# BB#273:                               # %if.then.i.i.i.i.i.i793
                                        #   in Loop: Header=BB58_270 Depth=4
	FPNEG      r23, r27
$BB58_274:                              # %_ZN4util4fabsERKf.exit.i.i.i.i.i796
                                        #   in Loop: Header=BB58_270 Depth=4
	ORI       r4, r0, 953267991
	FPLT   r4, r23, r4
	bneid     r4, ($BB58_276)
	ADDI      r23, r0, 1
# BB#275:                               # %_ZN4util4fabsERKf.exit.i.i.i.i.i796
                                        #   in Loop: Header=BB58_270 Depth=4
	ADDI      r23, r0, 0
$BB58_276:                              # %_ZN4util4fabsERKf.exit.i.i.i.i.i796
                                        #   in Loop: Header=BB58_270 Depth=4
	bneid     r23, ($BB58_291)
	NOP    
# BB#277:                               # %if.end.i.i.i.i.i805
                                        #   in Loop: Header=BB58_270 Depth=4
	LWI       r4, r1, 560
	FPRSUB     r28, r30, r4
	LWI       r4, r1, 568
	FPRSUB     r9, r9, r4
	LWI       r4, r1, 564
	FPRSUB     r21, r21, r4
	FPMUL      r23, r21, r20
	FPMUL      r30, r9, r11
	FPMUL      r20, r28, r20
	FPMUL      r4, r9, r26
	FPMUL      r5, r28, r11
	FPMUL      r22, r21, r26
	FPRSUB     r11, r23, r30
	FPRSUB     r26, r4, r20
	FPRSUB     r20, r5, r22
	FPMUL      r4, r26, r24
	FPMUL      r5, r20, r31
	FPADD      r4, r5, r4
	FPMUL      r5, r11, r8
	FPADD      r4, r4, r5
	ORI       r5, r0, 1065353216
	FPDIV      r23, r5, r27
	FPMUL      r8, r4, r23
	ORI       r4, r0, 0
	FPLT   r4, r8, r4
	bneid     r4, ($BB58_279)
	ADDI      r24, r0, 1
# BB#278:                               # %if.end.i.i.i.i.i805
                                        #   in Loop: Header=BB58_270 Depth=4
	ADDI      r24, r0, 0
$BB58_279:                              # %if.end.i.i.i.i.i805
                                        #   in Loop: Header=BB58_270 Depth=4
	bneid     r24, ($BB58_291)
	NOP    
# BB#280:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	FPMUL      r4, r6, r21
	FPMUL      r3, r3, r9
	FPADD      r3, r3, r4
	LWI       r4, r1, 572
	FPMUL      r4, r26, r4
	FPMUL      r5, r20, r25
	FPMUL      r6, r7, r28
	FPADD      r6, r3, r6
	FPADD      r3, r5, r4
	LWI       r4, r1, 576
	FPMUL      r4, r11, r4
	FPADD      r3, r3, r4
	FPMUL      r3, r3, r23
	FPMUL      r6, r6, r23
	FPADD      r4, r6, r3
	ORI       r5, r0, 1065353216
	FPLE   r9, r4, r5
	ORI       r4, r0, 0
	FPGE   r11, r6, r4
	FPGE   r20, r3, r4
	ADDI      r7, r0, 0
	ADDI      r3, r0, 1
	bneid     r9, ($BB58_282)
	ADD      r6, r3, r0
# BB#281:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r6, r7, r0
$BB58_282:                              # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	bneid     r20, ($BB58_284)
	ADD      r9, r3, r0
# BB#283:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r9, r7, r0
$BB58_284:                              # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	bneid     r11, ($BB58_286)
	ADD      r20, r3, r0
# BB#285:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r20, r7, r0
$BB58_286:                              # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	FPLT   r4, r8, r10
	bneid     r4, ($BB58_288)
	NOP    
# BB#287:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r3, r7, r0
$BB58_288:                              # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	BITAND       r4, r20, r9
	BITAND       r4, r4, r6
	BITAND       r3, r3, r4
	bneid     r3, ($BB58_290)
	NOP    
# BB#289:                               # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r8, r10, r0
$BB58_290:                              # %if.end9.i.i.i.i.i828
                                        #   in Loop: Header=BB58_270 Depth=4
	ADD      r10, r8, r0
$BB58_291:                              # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit.i.i.i.i831
                                        #   in Loop: Header=BB58_270 Depth=4
	ADDI      r29, r29, 1
	CMP       r3, r12, r29
	bneid     r3, ($BB58_270)
	NOP    
# BB#292:                               #   in Loop: Header=BB58_226 Depth=3
	LWI       r31, r1, 592
	LWI       r27, r1, 620
	brid      ($BB58_367)
	LWI       r22, r1, 624
$BB58_353:                              #   in Loop: Header=BB58_226 Depth=3
	ADD      r31, r28, r0
$BB58_367:                              # %while.cond.backedge.i.i.i834
                                        #   in Loop: Header=BB58_226 Depth=3
	bgtid     r31, ($BB58_226)
	NOP    
# BB#368:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i837
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 692
	FPDIV      r3, r3, r4
	FPGT   r4, r3, r10
	FPUN   r3, r3, r10
	BITOR        r3, r3, r4
	ADDI      r6, r0, 1
	LWI       r28, r1, 628
	bneid     r3, ($BB58_370)
	LWI       r29, r1, 632
# BB#369:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i837
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r6, r0, 0
$BB58_370:                              # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i837
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r3, r0, 0
	ADD      r4, r3, r0
	ADD      r5, r3, r0
	bneid     r6, ($BB58_372)
	LWI       r12, r1, 676
# BB#371:                               # %if.then20.i.i853
                                        #   in Loop: Header=BB58_4 Depth=2
	LWI       r3, r1, 684
	FPMUL      r3, r29, r3
	LWI       r4, r1, 680
	FPMUL      r4, r12, r4
	FPADD      r3, r4, r3
	LWI       r4, r1, 688
	FPMUL      r4, r28, r4
	FPADD      r3, r3, r4
	ORI       r5, r0, 0
	FPMAX     r3, r3, r5
	LWI       r4, r1, 636
	FPMUL      r6, r4, r3
	LWI       r4, r1, 640
	FPMUL      r4, r4, r3
	LWI       r7, r1, 644
	FPMUL      r3, r7, r3
	FPADD      r3, r3, r5
	FPADD      r4, r4, r5
	FPADD      r5, r6, r5
$BB58_372:                              # %do.body.i.i.i865
                                        #   Parent Loop BB58_3 Depth=1
                                        #     Parent Loop BB58_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	RAND      r7
	RAND      r6
	FPADD      r6, r6, r6
	ORI       r8, r0, -1082130432
	FPADD      r6, r6, r8
	FPADD      r7, r7, r7
	FPADD      r7, r7, r8
	FPMUL      r9, r7, r7
	FPMUL      r10, r6, r6
	FPADD      r8, r9, r10
	ORI       r11, r0, 1065353216
	FPGE   r11, r8, r11
	bneid     r11, ($BB58_374)
	ADDI      r8, r0, 1
# BB#373:                               # %do.body.i.i.i865
                                        #   in Loop: Header=BB58_372 Depth=3
	ADDI      r8, r0, 0
$BB58_374:                              # %do.body.i.i.i865
                                        #   in Loop: Header=BB58_372 Depth=3
	bneid     r8, ($BB58_372)
	NOP    
# BB#375:                               # %do.end.i.i.i871
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r8, r0, 0
	FPGE   r11, r12, r8
	FPUN   r8, r12, r8
	BITOR        r8, r8, r11
	bneid     r8, ($BB58_377)
	ADDI      r11, r0, 1
# BB#376:                               # %do.end.i.i.i871
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r11, r0, 0
$BB58_377:                              # %do.end.i.i.i871
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r8, r0, 1065353216
	FPRSUB     r9, r9, r8
	FPRSUB     r9, r10, r9
	FPINVSQRT r10, r9
	bneid     r11, ($BB58_379)
	ADD      r9, r12, r0
# BB#378:                               # %if.then.i59.i.i.i.i873
                                        #   in Loop: Header=BB58_4 Depth=2
	FPNEG      r9, r12
$BB58_379:                              # %_ZN4util4fabsERKf.exit61.i.i.i.i876
                                        #   in Loop: Header=BB58_4 Depth=2
	ADD      r30, r12, r0
	ORI       r11, r0, 0
	FPGE   r12, r29, r11
	FPUN   r11, r29, r11
	BITOR        r12, r11, r12
	bneid     r12, ($BB58_381)
	ADDI      r11, r0, 1
# BB#380:                               # %_ZN4util4fabsERKf.exit61.i.i.i.i876
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r11, r0, 0
$BB58_381:                              # %_ZN4util4fabsERKf.exit61.i.i.i.i876
                                        #   in Loop: Header=BB58_4 Depth=2
	ADD      r12, r29, r0
	LWI       r24, r1, 660
	LWI       r26, r1, 664
	bneid     r11, ($BB58_383)
	LWI       r27, r1, 668
# BB#382:                               # %if.then.i54.i.i.i.i878
                                        #   in Loop: Header=BB58_4 Depth=2
	FPNEG      r12, r29
$BB58_383:                              # %_ZN4util4fabsERKf.exit56.i.i.i.i881
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r11, r0, 0
	FPGE   r20, r28, r11
	FPUN   r11, r28, r11
	BITOR        r11, r11, r20
	ADDI      r20, r0, 1
	bneid     r11, ($BB58_385)
	LWI       r22, r1, 648
# BB#384:                               # %_ZN4util4fabsERKf.exit56.i.i.i.i881
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r20, r0, 0
$BB58_385:                              # %_ZN4util4fabsERKf.exit56.i.i.i.i881
                                        #   in Loop: Header=BB58_4 Depth=2
	ADD      r11, r28, r0
	bneid     r20, ($BB58_387)
	LWI       r23, r1, 652
# BB#386:                               # %if.then.i.i.i.i.i883
                                        #   in Loop: Header=BB58_4 Depth=2
	FPNEG      r11, r28
$BB58_387:                              # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	FPGE   r20, r9, r12
	FPUN   r21, r9, r12
	BITOR        r20, r21, r20
	ADDI      r21, r0, 1
	bneid     r20, ($BB58_389)
	LWI       r25, r1, 672
# BB#388:                               # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r21, r0, 0
$BB58_389:                              # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	FPDIV      r8, r8, r10
	ORI       r20, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r21, ($BB58_393)
	NOP    
# BB#390:                               # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	FPLT   r9, r9, r11
	bneid     r9, ($BB58_392)
	ADDI      r21, r0, 1
# BB#391:                               # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r21, r0, 0
$BB58_392:                              # %_ZN4util4fabsERKf.exit.i.i.i.i888
                                        #   in Loop: Header=BB58_4 Depth=2
	bneid     r21, ($BB58_397)
	ADD      r9, r10, r0
$BB58_393:                              # %if.else.i.i.i.i890
                                        #   in Loop: Header=BB58_4 Depth=2
	FPLT   r9, r12, r11
	bneid     r9, ($BB58_395)
	ADDI      r11, r0, 1
# BB#394:                               # %if.else.i.i.i.i890
                                        #   in Loop: Header=BB58_4 Depth=2
	ADDI      r11, r0, 0
$BB58_395:                              # %if.else.i.i.i.i890
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r9, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r11, ($BB58_397)
	ADD      r20, r10, r0
# BB#396:                               # %if.else19.i.i.i.i891
                                        #   in Loop: Header=BB58_4 Depth=2
	ORI       r9, r0, 0
	ORI       r10, r0, 1065353216
	brid      ($BB58_397)
	ADD      r20, r9, r0
$BB58_11:                               #   in Loop: Header=BB58_3 Depth=1
	LWI       r10, r1, 648
	brid      ($BB58_187)
	LWI       r6, r1, 652
$BB58_186:                              # %_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color.exit.i973.thread
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r3, r0, 1065151889
	LWI       r4, r1, 660
	FPMUL      r3, r4, r3
	LWI       r10, r1, 648
	FPADD      r10, r10, r3
	ORI       r3, r0, 1060806590
	LWI       r4, r1, 668
	FPMUL      r3, r4, r3
	LWI       r21, r1, 656
	FPADD      r21, r21, r3
	ORI       r3, r0, 1057988018
	LWI       r4, r1, 664
	FPMUL      r3, r4, r3
	LWI       r6, r1, 652
	FPADD      r6, r6, r3
$BB58_187:                              # %_ZNK5Scene8GetColorERK3RayRKi.exit978
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r3, r0, 0
	FPLT   r5, r6, r3
	ADDI      r4, r0, 1
	bneid     r5, ($BB58_189)
	LWI       r8, r1, 808
# BB#188:                               # %_ZNK5Scene8GetColorERK3RayRKi.exit978
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r4, r0, 0
$BB58_189:                              # %_ZNK5Scene8GetColorERK3RayRKi.exit978
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r4, ($BB58_194)
	NOP    
# BB#190:                               # %cond.false.i.i76
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r3, r0, 1065353216
	FPGT   r5, r6, r3
	bneid     r5, ($BB58_192)
	ADDI      r4, r0, 1
# BB#191:                               # %cond.false.i.i76
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r4, r0, 0
$BB58_192:                              # %cond.false.i.i76
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r4, ($BB58_194)
	NOP    
# BB#193:                               # %cond.false5.i.i77
                                        #   in Loop: Header=BB58_3 Depth=1
	ADD      r3, r6, r0
$BB58_194:                              # %cond.end7.i.i81
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r4, r0, 0
	FPLT   r6, r21, r4
	bneid     r6, ($BB58_196)
	ADDI      r5, r0, 1
# BB#195:                               # %cond.end7.i.i81
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r5, r0, 0
$BB58_196:                              # %cond.end7.i.i81
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r5, ($BB58_201)
	NOP    
# BB#197:                               # %cond.false12.i.i83
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r4, r0, 1065353216
	FPGT   r6, r21, r4
	bneid     r6, ($BB58_199)
	ADDI      r5, r0, 1
# BB#198:                               # %cond.false12.i.i83
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r5, r0, 0
$BB58_199:                              # %cond.false12.i.i83
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r5, ($BB58_201)
	NOP    
# BB#200:                               # %cond.false16.i.i84
                                        #   in Loop: Header=BB58_3 Depth=1
	ADD      r4, r21, r0
$BB58_201:                              # %cond.end20.i.i88
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r5, r0, 0
	FPLT   r7, r10, r5
	ADDI      r6, r0, 1
	bneid     r7, ($BB58_203)
	LWI       r9, r1, 804
# BB#202:                               # %cond.end20.i.i88
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r6, r0, 0
$BB58_203:                              # %cond.end20.i.i88
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r6, ($BB58_208)
	NOP    
# BB#204:                               # %cond.false25.i.i90
                                        #   in Loop: Header=BB58_3 Depth=1
	ORI       r5, r0, 1065353216
	FPGT   r7, r10, r5
	bneid     r7, ($BB58_206)
	ADDI      r6, r0, 1
# BB#205:                               # %cond.false25.i.i90
                                        #   in Loop: Header=BB58_3 Depth=1
	ADDI      r6, r0, 0
$BB58_206:                              # %cond.false25.i.i90
                                        #   in Loop: Header=BB58_3 Depth=1
	bneid     r6, ($BB58_208)
	NOP    
# BB#207:                               # %cond.false29.i.i91
                                        #   in Loop: Header=BB58_3 Depth=1
	ADD      r5, r10, r0
$BB58_208:                              # %_ZN5Image3SetERKiS1_RK5Color.exit107
                                        #   in Loop: Header=BB58_3 Depth=1
	LWI       r6, r1, 716
	LWI       r7, r1, 720
	ADD      r6, r6, r7
	MULI      r6, r6, 3
	ADD      r6, r6, r8
	STORE     r6, r3, 0
	STORE     r6, r4, 1
	STORE     r6, r5, 2
	ATOMIC_INC r3, 0
	CMP       r4, r9, r3
	bltid     r4, ($BB58_3)
	NOP    
	brid      ($BB58_757)
	NOP    
$BB58_12:                               # %for.body.lr.ph.for.body.lr.ph.split_crit_edge
	CMP       r5, r5, r20
	bltid     r5, ($BB58_24)
	NOP    
# BB#13:                                # %for.body13.lr.ph.us46.preheader
	ADDI      r4, r4, 4
	SWI       r4, r1, 716
	ADD      r4, r11, r0
	SWI       r4, r1, 804
$BB58_14:                               # %for.body13.lr.ph.us46
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB58_15 Depth 2
                                        #       Child Loop BB58_16 Depth 3
                                        #         Child Loop BB58_730 Depth 4
                                        #         Child Loop BB58_586 Depth 4
                                        #           Child Loop BB58_702 Depth 5
                                        #         Child Loop BB58_421 Depth 4
                                        #           Child Loop BB58_465 Depth 5
	LWI       r5, r1, 812
	DIV      r4, r5, r3
	MUL       r5, r4, r5
	SWI       r5, r1, 816
	RSUB     r3, r5, r3
	SWI       r3, r1, 820
	ADD      r6, r0, r0
	ORI       r5, r0, 0
	SWI       r5, r1, 720
	FPCONV       r4, r4
	SWI       r4, r1, 792
	FPCONV       r3, r3
	SWI       r3, r1, 796
	SWI       r5, r1, 724
	ADD      r4, r5, r0
$BB58_15:                               # %for.body13.us40
                                        #   Parent Loop BB58_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB58_16 Depth 3
                                        #         Child Loop BB58_730 Depth 4
                                        #         Child Loop BB58_586 Depth 4
                                        #           Child Loop BB58_702 Depth 5
                                        #         Child Loop BB58_421 Depth 4
                                        #           Child Loop BB58_465 Depth 5
	SWI       r4, r1, 732
	SWI       r6, r1, 728
	ORI       r3, r0, -1090519040
	RAND      r4
	FPADD      r4, r4, r3
	LWI       r5, r1, 796
	FPADD      r4, r5, r4
	LWI       r5, r1, 784
	FPMUL      r4, r4, r5
	RAND      r5
	FPADD      r5, r5, r3
	FPADD      r4, r4, r4
	ORI       r3, r0, -1082130432
	FPADD      r4, r4, r3
	LWI       r6, r1, 760
	FPMUL      r6, r6, r4
	LWI       r7, r1, 748
	FPADD      r7, r7, r6
	LWI       r6, r1, 792
	FPADD      r5, r6, r5
	LWI       r6, r1, 788
	FPMUL      r5, r5, r6
	FPADD      r5, r5, r5
	FPADD      r6, r5, r3
	LWI       r3, r1, 772
	FPMUL      r3, r3, r6
	FPADD      r3, r7, r3
	LWI       r5, r1, 764
	FPMUL      r5, r5, r4
	LWI       r7, r1, 752
	FPADD      r5, r7, r5
	LWI       r7, r1, 776
	FPMUL      r7, r7, r6
	FPADD      r5, r5, r7
	FPMUL      r7, r5, r5
	FPMUL      r8, r3, r3
	FPADD      r7, r8, r7
	LWI       r8, r1, 780
	FPMUL      r6, r8, r6
	LWI       r8, r1, 768
	FPMUL      r4, r8, r4
	LWI       r8, r1, 756
	FPADD      r4, r8, r4
	FPADD      r4, r4, r6
	FPMUL      r6, r4, r4
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r25, r0, 1065353216
	FPDIV      r6, r25, r6
	FPDIV      r10, r4, r6
	FPDIV      r29, r5, r6
	FPDIV      r4, r3, r6
	ADD      r26, r0, r0
	ORI       r22, r0, 0
	ADD      r23, r22, r0
	ADD      r24, r22, r0
	LWI       r3, r1, 744
	SWI       r3, r1, 560
	LWI       r3, r1, 740
	SWI       r3, r1, 564
	LWI       r3, r1, 736
	SWI       r3, r1, 568
	SWI       r25, r1, 660
	brid      ($BB58_16)
	SWI       r25, r1, 656
$BB58_755:                              # %if.end.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPMUL      r5, r27, r10
	FPMUL      r20, r28, r11
	FPRSUB     r20, r5, r20
	FPMUL      r5, r29, r10
	FPMUL      r10, r28, r12
	FPRSUB     r5, r10, r5
	FPMUL      r10, r29, r11
	FPMUL      r11, r27, r12
	FPRSUB     r12, r10, r11
	FPMUL      r10, r27, r12
	FPMUL      r11, r28, r5
	FPRSUB     r10, r10, r11
	FPMUL      r11, r29, r5
	FPMUL      r21, r27, r20
	FPRSUB     r11, r11, r21
	FPMUL      r11, r11, r7
	FPMUL      r21, r12, r8
	FPADD      r11, r21, r11
	FPMUL      r10, r10, r7
	FPMUL      r21, r20, r8
	FPADD      r10, r21, r10
	FPMUL      r21, r29, r9
	FPADD      r10, r10, r21
	FPMUL      r21, r28, r9
	FPADD      r11, r11, r21
	FPMUL      r20, r28, r20
	FPMUL      r12, r29, r12
	FPMUL      r3, r3, r25
	LWI       r21, r1, 636
	FPMUL      r25, r25, r21
	LWI       r21, r1, 660
	FPMUL      r4, r4, r21
	LWI       r28, r1, 632
	FPMUL      r21, r21, r28
	SWI       r21, r1, 660
	LWI       r21, r1, 656
	FPMUL      r6, r6, r21
	LWI       r28, r1, 628
	FPMUL      r21, r21, r28
	SWI       r21, r1, 656
	FPADD      r22, r22, r3
	FPADD      r23, r23, r4
	FPADD      r24, r24, r6
	FPMUL      r3, r11, r11
	FPMUL      r4, r10, r10
	FPADD      r3, r4, r3
	FPRSUB     r4, r20, r12
	FPMUL      r4, r4, r7
	FPMUL      r5, r5, r8
	FPADD      r4, r5, r4
	FPMUL      r5, r27, r9
	FPADD      r4, r4, r5
	FPMUL      r5, r4, r4
	FPADD      r3, r3, r5
	FPINVSQRT r3, r3
	ORI       r5, r0, 1065353216
	FPDIV      r7, r5, r3
	FPDIV      r3, r10, r7
	FPDIV      r6, r11, r7
	FPMUL      r8, r6, r6
	FPMUL      r9, r3, r3
	FPADD      r8, r9, r8
	FPDIV      r4, r4, r7
	FPMUL      r7, r4, r4
	FPADD      r7, r8, r7
	FPINVSQRT r7, r7
	FPDIV      r5, r5, r7
	FPDIV      r10, r4, r5
	FPDIV      r29, r6, r5
	FPDIV      r4, r3, r5
$BB58_16:                               # %while.cond.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB58_730 Depth 4
                                        #         Child Loop BB58_586 Depth 4
                                        #           Child Loop BB58_702 Depth 5
                                        #         Child Loop BB58_421 Depth 4
                                        #           Child Loop BB58_465 Depth 5
	SWI       r4, r1, 576
	ORI       r3, r0, 1065353216
	FPDIV      r7, r3, r10
	FPDIV      r5, r3, r29
	SWI       r5, r1, 600
	FPDIV      r3, r3, r4
	SWI       r3, r1, 604
	ORI       r4, r0, 0
	FPLT   r3, r3, r4
	FPLT   r5, r5, r4
	FPLT   r6, r7, r4
	ADDI      r4, r0, 0
	ADDI      r8, r0, 1
	SWI       r8, r1, 616
	bneid     r6, ($BB58_18)
	SWI       r8, r1, 620
# BB#17:                                # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r4, r1, 620
$BB58_18:                               # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r24, r1, 652
	SWI       r23, r1, 648
	SWI       r22, r1, 644
	LWI       r6, r1, 616
	bneid     r5, ($BB58_20)
	SWI       r6, r1, 624
# BB#19:                                # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r4, r1, 624
$BB58_20:                               # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	bneid     r3, ($BB58_22)
	NOP    
# BB#21:                                # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r4, r1, 616
$BB58_22:                               # %while.cond.i
                                        #   in Loop: Header=BB58_16 Depth=3
	LWI       r3, r1, 708
	CMP       r3, r3, r26
	bgeid     r3, ($BB58_23)
	NOP    
# BB#420:                               # %while.body.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r25, r1, 664
	ADDI      r26, r26, 1
	SWI       r26, r1, 668
	ADDI      r9, r0, 1
	ORI       r3, r0, 1203982336
	SWI       r3, r1, 572
	ADDI      r4, r0, -1
	SWI       r4, r1, 596
	LWI       r3, r1, 620
	XORI      r3, r3, 1
	SWI       r3, r1, 632
	LWI       r3, r1, 624
	XORI      r3, r3, 1
	SWI       r3, r1, 636
	LWI       r3, r1, 616
	XORI      r3, r3, 1
	SWI       r3, r1, 640
	SWI       r0, r1, 336
	SWI       r4, r1, 592
	ADD      r25, r7, r0
	brid      ($BB58_421)
	SWI       r25, r1, 628
$BB58_535:                              # %if.else24.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	beqid     r3, ($BB58_536)
	NOP    
# BB#537:                               # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r7, r22, r26
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r7, ($BB58_539)
	ADD      r5, r3, r0
# BB#538:                               # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r4, r0
$BB58_539:                              # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r5, ($BB58_541)
	NOP    
# BB#540:                               # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r22, r26, r0
$BB58_541:                              # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGE   r5, r21, r22
	FPUN   r7, r21, r22
	BITOR        r5, r7, r5
	bneid     r5, ($BB58_543)
	NOP    
# BB#542:                               # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r3, r4, r0
$BB58_543:                              # %if.else31.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r3, ($BB58_545)
	NOP    
# BB#544:                               # %if.then35.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	SWI       r6, r5, -4
	LWI       r5, r1, 580
	SW        r5, r4, r3
	brid      ($BB58_563)
	ADDI      r9, r9, 1
$BB58_536:                              # %if.then28.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	brid      ($BB58_563)
	SWI       r6, r3, -4
$BB58_545:                              # %if.else40.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	LWI       r7, r1, 580
	SWI       r7, r5, -4
	SW        r6, r4, r3
	brid      ($BB58_563)
	ADDI      r9, r9, 1
$BB58_421:                              # %while.body.i.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        #       Parent Loop BB58_16 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB58_465 Depth 5
	ADDI      r3, r0, -1
	ADD      r8, r9, r3
	bslli     r3, r8, 2
	ADDI      r4, r1, 336
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r3, r3, r4
	SWI       r0, r1, 248
	SWI       r0, r1, 244
	SWI       r0, r1, 240
	LOAD      r4, r3, 0
	SWI        r4, r1, 240
	LOAD      r4, r3, 1
	SWI        r4, r1, 244
	LOAD      r4, r3, 2
	SWI        r4, r1, 248
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 252
	LOAD      r5, r4, 1
	SWI        r5, r1, 256
	LOAD      r4, r4, 2
	SWI        r4, r1, 260
	LWI       r4, r1, 636
	MULI      r5, r4, 12
	SWI       r5, r1, 612
	ADDI      r4, r1, 240
	ADD      r5, r4, r5
	ADDI      r6, r3, 7
	ADDI      r3, r3, 6
	LWI       r7, r1, 616
	MULI      r28, r7, 12
	LOAD      r31, r3, 0
	SWI       r31, r1, 264
	LOAD      r3, r6, 0
	SWI       r3, r1, 580
	SWI       r3, r1, 268
	LWI        r5, r5, 4
	LWI       r6, r1, 564
	FPRSUB     r5, r6, r5
	LWI       r3, r1, 600
	FPMUL      r21, r5, r3
	LW         r5, r4, r28
	LWI       r6, r1, 568
	FPRSUB     r5, r6, r5
	LWI       r3, r1, 604
	FPMUL      r5, r5, r3
	ADDI      r6, r0, 1
	FPGT   r7, r5, r21
	bneid     r7, ($BB58_423)
	NOP    
# BB#422:                               # %while.body.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r6, r0, 0
$BB58_423:                              # %while.body.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r6, ($BB58_424)
	NOP    
# BB#425:                               # %while.body.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r3, r1, 624
	MULI      r26, r3, 12
	ADD      r6, r4, r26
	LWI        r6, r6, 4
	LWI       r7, r1, 564
	FPRSUB     r6, r7, r6
	LWI       r3, r1, 600
	FPMUL      r6, r6, r3
	LWI       r3, r1, 640
	MULI      r22, r3, 12
	LW         r4, r4, r22
	LWI       r7, r1, 568
	FPRSUB     r4, r7, r4
	LWI       r3, r1, 604
	FPMUL      r4, r4, r3
	FPGT   r7, r6, r4
	bneid     r7, ($BB58_427)
	ADDI      r11, r0, 1
# BB#426:                               # %while.body.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r11, r0, 0
$BB58_427:                              # %while.body.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_428)
	NOP    
# BB#429:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r7, r6, r5
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r7, ($BB58_431)
	ADD      r11, r12, r0
# BB#430:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r20, r0
$BB58_431:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_433)
	NOP    
# BB#432:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r6, r5, r0
$BB58_433:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r3, r1, 632
	MULI      r30, r3, 12
	ADDI      r23, r1, 240
	ADD      r5, r23, r30
	FPLT   r7, r21, r4
	bneid     r7, ($BB58_435)
	ADD      r24, r12, r0
# BB#434:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r24, r20, r0
$BB58_435:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI        r5, r5, 8
	LWI       r7, r1, 560
	FPRSUB     r5, r7, r5
	FPMUL      r5, r5, r25
	FPGT   r7, r6, r5
	bneid     r7, ($BB58_437)
	NOP    
# BB#436:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r12, r20, r0
$BB58_437:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r24, ($BB58_439)
	NOP    
# BB#438:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r21, r4, r0
$BB58_439:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r12, ($BB58_440)
	NOP    
# BB#441:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r3, r1, 620
	MULI      r27, r3, 12
	ADD      r4, r23, r27
	LWI        r4, r4, 8
	LWI       r7, r1, 560
	FPRSUB     r4, r7, r4
	FPMUL      r4, r4, r25
	FPGT   r7, r4, r21
	bneid     r7, ($BB58_443)
	ADDI      r11, r0, 1
# BB#442:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r11, r0, 0
$BB58_443:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_444)
	NOP    
# BB#445:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPLT   r7, r5, r21
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r7, ($BB58_447)
	ADD      r11, r12, r0
# BB#446:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r20, r0
$BB58_447:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_449)
	NOP    
# BB#448:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r21, r0
$BB58_449:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ORI       r7, r0, 0
	FPLT   r5, r5, r7
	bneid     r5, ($BB58_451)
	NOP    
# BB#450:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r12, r20, r0
$BB58_451:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r12, ($BB58_452)
	NOP    
# BB#453:                               # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r7, r4, r6
	ADDI      r12, r0, 0
	ADDI      r5, r0, 1
	bneid     r7, ($BB58_455)
	ADD      r11, r5, r0
# BB#454:                               # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r12, r0
$BB58_455:                              # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_457)
	NOP    
# BB#456:                               # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r4, r6, r0
$BB58_457:                              # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r3, r1, 572
	FPLT   r4, r3, r4
	bneid     r4, ($BB58_459)
	NOP    
# BB#458:                               # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r12, r0
$BB58_459:                              # %if.end.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r3, r31, r0
	bneid     r5, ($BB58_460)
	SWI       r3, r1, 584
# BB#461:                               # %if.end7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	beqid     r3, ($BB58_462)
	NOP    
# BB#463:                               # %if.end7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r21, r0, r0
	ADDI      r4, r0, -1
	CMP       r4, r4, r3
	bneid     r4, ($BB58_464)
	NOP    
# BB#473:                               # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r3, r1, 580
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r3, r3, r4
	SWI       r0, r1, 280
	SWI       r0, r1, 276
	SWI       r0, r1, 272
	LOAD      r4, r3, 0
	SWI        r4, r1, 272
	LOAD      r4, r3, 1
	SWI        r4, r1, 276
	LOAD      r4, r3, 2
	SWI        r4, r1, 280
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 284
	LOAD      r5, r4, 1
	SWI        r5, r1, 288
	LOAD      r4, r4, 2
	SWI        r4, r1, 292
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 296
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 300
	ADDI      r5, r1, 272
	LWI       r31, r1, 612
	ADD      r3, r5, r31
	LWI        r3, r3, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 600
	FPMUL      r23, r3, r4
	LW         r3, r5, r28
	LWI       r4, r1, 568
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 604
	FPMUL      r4, r3, r4
	ADDI      r6, r0, 1
	FPGT   r3, r4, r23
	bneid     r3, ($BB58_475)
	NOP    
# BB#474:                               # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r6, r0, 0
$BB58_475:                              # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	SWI       r8, r1, 608
	ADD      r3, r0, r0
	ORI       r21, r0, 1203982336
	bneid     r6, ($BB58_505)
	NOP    
# BB#476:                               # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r6, r5, r26
	LW         r5, r5, r22
	LWI        r6, r6, 4
	LWI       r7, r1, 564
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 600
	FPMUL      r6, r6, r7
	LWI       r7, r1, 568
	FPRSUB     r5, r7, r5
	LWI       r7, r1, 604
	FPMUL      r24, r5, r7
	FPGT   r7, r6, r24
	bneid     r7, ($BB58_478)
	ADDI      r5, r0, 1
# BB#477:                               # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r5, r0, 0
$BB58_478:                              # %if.then9.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r5, ($BB58_505)
	NOP    
# BB#479:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r7, r6, r4
	ADDI      r3, r0, 0
	ADDI      r5, r0, 1
	bneid     r7, ($BB58_481)
	ADD      r11, r5, r0
# BB#480:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r3, r0
$BB58_481:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_483)
	NOP    
# BB#482:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r6, r4, r0
$BB58_483:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r12, r1, 272
	ADD      r4, r12, r30
	FPLT   r7, r23, r24
	bneid     r7, ($BB58_485)
	ADD      r20, r5, r0
# BB#484:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r20, r3, r0
$BB58_485:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI        r4, r4, 8
	LWI       r7, r1, 560
	FPRSUB     r4, r7, r4
	FPMUL      r4, r4, r25
	FPGT   r7, r6, r4
	bneid     r7, ($BB58_487)
	NOP    
# BB#486:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r3, r0
$BB58_487:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r20, ($BB58_489)
	NOP    
# BB#488:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r23, r24, r0
$BB58_489:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r3, r0, r0
	ORI       r21, r0, 1203982336
	bneid     r5, ($BB58_505)
	NOP    
# BB#490:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r12, r27
	LWI        r5, r5, 8
	LWI       r7, r1, 560
	FPRSUB     r5, r7, r5
	FPMUL      r24, r5, r25
	FPGT   r7, r24, r23
	bneid     r7, ($BB58_492)
	ADDI      r5, r0, 1
# BB#491:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r5, r0, 0
$BB58_492:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r5, ($BB58_505)
	NOP    
# BB#493:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPLT   r7, r4, r23
	ADDI      r3, r0, 0
	ADDI      r5, r0, 1
	bneid     r7, ($BB58_495)
	ADD      r11, r5, r0
# BB#494:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r3, r0
$BB58_495:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_497)
	NOP    
# BB#496:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r4, r23, r0
$BB58_497:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ORI       r7, r0, 0
	FPLT   r4, r4, r7
	bneid     r4, ($BB58_499)
	NOP    
# BB#498:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r5, r3, r0
$BB58_499:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r3, r0, r0
	ORI       r21, r0, 1203982336
	bneid     r5, ($BB58_505)
	NOP    
# BB#500:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r5, r24, r6
	ADDI      r3, r0, 1
	bneid     r5, ($BB58_502)
	ADD      r4, r3, r0
# BB#501:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r4, r0, 0
$BB58_502:                              # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r4, ($BB58_504)
	NOP    
# BB#503:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r24, r6, r0
$BB58_504:                              # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r21, r24, r0
$BB58_505:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI       r4, r1, 580
	ADDI      r6, r4, 1
	bslli     r4, r6, 3
	LWI       r5, r1, 588
	ADD      r4, r4, r5
	SWI       r0, r1, 312
	SWI       r0, r1, 308
	SWI       r0, r1, 304
	LOAD      r5, r4, 0
	SWI        r5, r1, 304
	LOAD      r5, r4, 1
	SWI        r5, r1, 308
	LOAD      r5, r4, 2
	SWI        r5, r1, 312
	ADDI      r5, r4, 3
	LOAD      r7, r5, 0
	SWI        r7, r1, 316
	LOAD      r7, r5, 1
	SWI        r7, r1, 320
	LOAD      r5, r5, 2
	SWI        r5, r1, 324
	ADDI      r5, r4, 6
	LOAD      r5, r5, 0
	SWI       r5, r1, 328
	ADDI      r4, r4, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 332
	ADDI      r4, r1, 304
	ADD      r5, r4, r31
	LWI        r5, r5, 4
	LWI       r7, r1, 564
	FPRSUB     r5, r7, r5
	LWI       r7, r1, 600
	FPMUL      r23, r5, r7
	LW         r5, r4, r28
	LWI       r7, r1, 568
	FPRSUB     r5, r7, r5
	LWI       r7, r1, 604
	FPMUL      r5, r5, r7
	ADDI      r12, r0, 1
	FPGT   r7, r5, r23
	bneid     r7, ($BB58_507)
	NOP    
# BB#506:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r12, r0, 0
$BB58_507:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r12, ($BB58_532)
	NOP    
# BB#508:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r7, r4, r26
	LWI        r7, r7, 4
	LWI       r8, r1, 564
	FPRSUB     r7, r8, r7
	LWI       r8, r1, 600
	FPMUL      r26, r7, r8
	LW         r4, r4, r22
	LWI       r7, r1, 568
	FPRSUB     r4, r7, r4
	LWI       r7, r1, 604
	FPMUL      r4, r4, r7
	FPGT   r7, r26, r4
	bneid     r7, ($BB58_510)
	ADDI      r11, r0, 1
# BB#509:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r11, r0, 0
$BB58_510:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_532)
	NOP    
# BB#511:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPGT   r7, r26, r5
	ADDI      r22, r0, 0
	ADDI      r12, r0, 1
	bneid     r7, ($BB58_513)
	ADD      r11, r12, r0
# BB#512:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r22, r0
$BB58_513:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_515)
	NOP    
# BB#514:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r26, r5, r0
$BB58_515:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r5, r1, 304
	ADD      r20, r5, r30
	FPLT   r7, r23, r4
	bneid     r7, ($BB58_517)
	ADD      r24, r12, r0
# BB#516:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r24, r22, r0
$BB58_517:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	LWI        r7, r20, 8
	LWI       r8, r1, 560
	FPRSUB     r7, r8, r7
	FPMUL      r20, r7, r25
	FPGT   r7, r26, r20
	bneid     r7, ($BB58_519)
	NOP    
# BB#518:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r12, r22, r0
$BB58_519:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r24, ($BB58_521)
	NOP    
# BB#520:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r23, r4, r0
$BB58_521:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r12, ($BB58_532)
	NOP    
# BB#522:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r4, r5, r27
	LWI        r4, r4, 8
	LWI       r5, r1, 560
	FPRSUB     r4, r5, r4
	FPMUL      r22, r4, r25
	FPGT   r5, r22, r23
	bneid     r5, ($BB58_524)
	ADDI      r4, r0, 1
# BB#523:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r4, r0, 0
$BB58_524:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r4, ($BB58_532)
	NOP    
# BB#525:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	FPLT   r7, r20, r23
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB58_527)
	ADD      r11, r4, r0
# BB#526:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r11, r5, r0
$BB58_527:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r11, ($BB58_529)
	NOP    
# BB#528:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r20, r23, r0
$BB58_529:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ORI       r7, r0, 0
	FPGE   r8, r20, r7
	FPUN   r7, r20, r7
	BITOR        r7, r7, r8
	bneid     r7, ($BB58_531)
	NOP    
# BB#530:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADD      r4, r5, r0
$BB58_531:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bneid     r4, ($BB58_535)
	NOP    
$BB58_532:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	ADDI      r4, r0, 1
	CMP       r3, r4, r3
	bneid     r3, ($BB58_533)
	NOP    
# BB#534:                               # %if.then21.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bslli     r3, r9, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	LWI       r4, r1, 580
	brid      ($BB58_563)
	SWI       r4, r3, -4
$BB58_424:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_428:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_440:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_444:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_452:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_460:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_462:                              #   in Loop: Header=BB58_421 Depth=4
	brid      ($BB58_563)
	ADD      r9, r8, r0
$BB58_464:                              #   in Loop: Header=BB58_421 Depth=4
	SWI       r8, r1, 608
$BB58_465:                              # %for.body.i.i.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        #       Parent Loop BB58_16 Depth=3
                                        #         Parent Loop BB58_421 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	MULI      r4, r21, 11
	LWI       r3, r1, 580
	ADD      r6, r4, r3
	LOAD      r27, r6, 0
	LOAD      r28, r6, 1
	LOAD      r31, r6, 2
	ADDI      r4, r6, 3
	LOAD      r5, r4, 0
	LOAD      r12, r4, 1
	LOAD      r4, r4, 2
	ADDI      r7, r6, 6
	LOAD      r20, r7, 0
	LOAD      r8, r7, 1
	LOAD      r7, r7, 2
	FPRSUB     r23, r31, r7
	FPRSUB     r30, r28, r8
	FPMUL      r7, r10, r30
	FPMUL      r8, r29, r23
	FPRSUB     r9, r7, r8
	FPRSUB     r24, r27, r20
	FPMUL      r7, r10, r24
	LWI       r25, r1, 576
	FPMUL      r8, r25, r23
	FPRSUB     r22, r8, r7
	FPRSUB     r5, r27, r5
	FPRSUB     r20, r28, r12
	FPMUL      r7, r22, r20
	FPMUL      r8, r9, r5
	FPADD      r11, r8, r7
	FPMUL      r7, r29, r24
	ADD      r3, r29, r0
	FPMUL      r8, r25, r30
	FPRSUB     r26, r7, r8
	FPRSUB     r12, r31, r4
	FPMUL      r4, r26, r12
	FPADD      r29, r11, r4
	ORI       r4, r0, 0
	FPGE   r7, r29, r4
	FPUN   r4, r29, r4
	BITOR        r4, r4, r7
	bneid     r4, ($BB58_467)
	ADDI      r11, r0, 1
# BB#466:                               # %for.body.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADDI      r11, r0, 0
$BB58_467:                              # %for.body.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r11, ($BB58_469)
	ADD      r4, r29, r0
# BB#468:                               # %if.then.i.i.i.i33.i
                                        #   in Loop: Header=BB58_465 Depth=5
	FPNEG      r4, r29
$BB58_469:                              # %_ZN4util4fabsERKf.exit.i.i.i36.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ORI       r7, r0, 953267991
	FPLT   r7, r4, r7
	bneid     r7, ($BB58_471)
	ADDI      r4, r0, 1
# BB#470:                               # %_ZN4util4fabsERKf.exit.i.i.i36.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADDI      r4, r0, 0
$BB58_471:                              # %_ZN4util4fabsERKf.exit.i.i.i36.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r4, ($BB58_472)
	NOP    
# BB#546:                               # %if.end.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADD      r25, r10, r0
	LWI       r4, r1, 560
	FPRSUB     r31, r31, r4
	LWI       r4, r1, 568
	FPRSUB     r27, r27, r4
	LWI       r4, r1, 564
	FPRSUB     r28, r28, r4
	FPMUL      r4, r28, r5
	FPMUL      r11, r27, r20
	FPMUL      r7, r31, r5
	FPMUL      r8, r27, r12
	FPMUL      r10, r31, r20
	FPMUL      r12, r28, r12
	FPRSUB     r5, r4, r11
	FPRSUB     r20, r8, r7
	FPRSUB     r12, r10, r12
	FPMUL      r4, r20, r30
	FPMUL      r7, r12, r24
	FPADD      r4, r7, r4
	FPMUL      r7, r5, r23
	FPADD      r7, r4, r7
	ORI       r4, r0, 1065353216
	FPDIV      r4, r4, r29
	FPMUL      r23, r7, r4
	ORI       r7, r0, 0
	FPLT   r7, r23, r7
	bneid     r7, ($BB58_548)
	ADDI      r11, r0, 1
# BB#547:                               # %if.end.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADDI      r11, r0, 0
$BB58_548:                              # %if.end.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r11, ($BB58_549)
	ADD      r29, r3, r0
# BB#550:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	FPMUL      r7, r22, r28
	FPMUL      r8, r9, r27
	FPADD      r7, r8, r7
	FPMUL      r8, r20, r29
	LWI       r3, r1, 576
	FPMUL      r9, r12, r3
	FPMUL      r10, r26, r31
	FPADD      r7, r7, r10
	FPADD      r8, r9, r8
	ADD      r10, r25, r0
	FPMUL      r5, r5, r10
	FPADD      r5, r8, r5
	FPMUL      r5, r5, r4
	FPMUL      r4, r7, r4
	FPADD      r7, r4, r5
	ORI       r8, r0, 1065353216
	FPLE   r22, r7, r8
	ORI       r7, r0, 0
	FPGE   r4, r4, r7
	FPGE   r20, r5, r7
	LWI       r3, r1, 572
	FPGE   r5, r23, r3
	FPUN   r7, r23, r3
	BITOR        r24, r7, r5
	ADDI      r12, r0, 0
	ADDI      r9, r0, 1
	bneid     r22, ($BB58_552)
	ADD      r5, r9, r0
# BB#551:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADD      r5, r12, r0
$BB58_552:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r24, ($BB58_554)
	ADD      r22, r9, r0
# BB#553:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADD      r22, r12, r0
$BB58_554:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r20, ($BB58_556)
	ADD      r24, r9, r0
# BB#555:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADD      r24, r12, r0
$BB58_556:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r4, ($BB58_558)
	NOP    
# BB#557:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADD      r9, r12, r0
$BB58_558:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	bneid     r22, ($BB58_561)
	NOP    
# BB#559:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	BITAND       r4, r9, r24
	BITAND       r4, r4, r5
	ADDI      r5, r0, 1
	CMP       r4, r5, r4
	bneid     r4, ($BB58_561)
	NOP    
# BB#560:                               # %_ZNK10syTriangle6ID_MATEv.exit.i.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADDI      r4, r6, 10
	LOAD      r3, r4, 0
	SWI       r3, r1, 592
	SWI       r6, r1, 596
	brid      ($BB58_561)
	SWI       r23, r1, 572
$BB58_472:                              #   in Loop: Header=BB58_465 Depth=5
	brid      ($BB58_561)
	ADD      r29, r3, r0
$BB58_549:                              #   in Loop: Header=BB58_465 Depth=5
	ADD      r10, r25, r0
$BB58_561:                              # %_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord.exit.i.i.i
                                        #   in Loop: Header=BB58_465 Depth=5
	ADDI      r21, r21, 1
	LWI       r3, r1, 584
	CMP       r4, r3, r21
	bneid     r4, ($BB58_465)
	NOP    
# BB#562:                               #   in Loop: Header=BB58_421 Depth=4
	LWI       r9, r1, 608
	brid      ($BB58_563)
	LWI       r25, r1, 628
$BB58_533:                              #   in Loop: Header=BB58_421 Depth=4
	LWI       r9, r1, 608
$BB58_563:                              # %while.cond.backedge.i.i
                                        #   in Loop: Header=BB58_421 Depth=4
	bgtid     r9, ($BB58_421)
	NOP    
# BB#564:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r3, r0, 1203982336
	LWI       r4, r1, 572
	FPNE   r4, r4, r3
	bneid     r4, ($BB58_566)
	ADDI      r3, r0, 1
# BB#565:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r3, r0, 0
$BB58_566:                              # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit.i
                                        #   in Loop: Header=BB58_16 Depth=3
	beqid     r3, ($BB58_567)
	NOP    
# BB#569:                               # %_ZNK10syTriangle6NormalEv.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	LWI       r8, r1, 596
	LOAD      r3, r8, 0
	LOAD      r4, r8, 1
	LOAD      r6, r8, 2
	ADDI      r7, r8, 3
	LOAD      r5, r7, 0
	LOAD      r9, r7, 1
	LOAD      r20, r7, 2
	ADDI      r8, r8, 6
	LOAD      r11, r8, 0
	LOAD      r7, r8, 1
	LOAD      r8, r8, 2
	FPRSUB     r12, r8, r6
	FPRSUB     r20, r20, r6
	FPRSUB     r6, r9, r4
	FPRSUB     r9, r7, r4
	FPMUL      r4, r20, r9
	FPMUL      r7, r6, r12
	FPRSUB     r4, r4, r7
	FPRSUB     r5, r5, r3
	FPRSUB     r11, r11, r3
	FPMUL      r3, r20, r11
	FPMUL      r7, r5, r12
	FPRSUB     r3, r7, r3
	FPMUL      r7, r3, r3
	FPMUL      r8, r4, r4
	FPADD      r7, r8, r7
	FPMUL      r6, r6, r11
	FPMUL      r5, r5, r9
	FPRSUB     r5, r6, r5
	FPMUL      r6, r5, r5
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r6, r7, r6
	FPDIV      r23, r3, r6
	FPMUL      r3, r23, r29
	FPDIV      r24, r4, r6
	LWI       r4, r1, 576
	FPMUL      r4, r24, r4
	FPADD      r3, r4, r3
	FPDIV      r22, r5, r6
	FPMUL      r4, r22, r10
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB58_571)
	ADDI      r3, r0, 1
# BB#570:                               # %_ZNK10syTriangle6NormalEv.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r3, r0, 0
$BB58_571:                              # %_ZNK10syTriangle6NormalEv.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	bneid     r3, ($BB58_573)
	NOP    
# BB#572:                               # %if.then3.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPNEG      r22, r22
	FPNEG      r23, r23
	FPNEG      r24, r24
$BB58_573:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	LWI       r7, r1, 572
	FPMUL      r3, r29, r7
	LWI       r4, r1, 564
	FPADD      r3, r4, r3
	LWI       r4, r1, 576
	FPMUL      r4, r4, r7
	LWI       r5, r1, 568
	FPADD      r4, r5, r4
	LWI       r5, r1, 696
	FPRSUB     r8, r4, r5
	LWI       r5, r1, 700
	FPRSUB     r9, r3, r5
	FPMUL      r5, r9, r9
	FPMUL      r6, r8, r8
	FPADD      r6, r6, r5
	FPMUL      r5, r10, r7
	LWI       r7, r1, 560
	FPADD      r5, r7, r5
	LWI       r7, r1, 704
	FPRSUB     r10, r5, r7
	FPMUL      r7, r10, r10
	FPADD      r7, r6, r7
	LWI       r6, r1, 592
	MULI      r6, r6, 25
	LWI       r11, r1, 716
	ADD      r11, r11, r6
	ORI       r6, r0, 1028443341
	LOAD      r12, r11, 0
	SWI       r12, r1, 628
	LOAD      r12, r11, 1
	SWI       r12, r1, 632
	LOAD      r11, r11, 2
	SWI       r11, r1, 636
	FPINVSQRT r11, r7
	SWI       r11, r1, 692
	FPINVSQRT r11, r7
	ORI       r7, r0, 1065353216
	FPDIV      r11, r7, r11
	FPDIV      r12, r8, r11
	SWI       r12, r1, 680
	FPDIV      r20, r9, r11
	SWI       r20, r1, 684
	FPMUL      r8, r20, r20
	FPMUL      r9, r12, r12
	FPADD      r8, r9, r8
	FPDIV      r21, r10, r11
	SWI       r21, r1, 688
	FPMUL      r9, r21, r21
	FPADD      r8, r8, r9
	FPINVSQRT r8, r8
	FPDIV      r9, r7, r8
	ORI       r8, r0, 0
	FPDIV      r30, r12, r9
	FPDIV      r26, r7, r30
	FPGE   r10, r26, r8
	FPUN   r12, r26, r8
	FPDIV      r11, r20, r9
	SWI       r11, r1, 572
	FPDIV      r25, r7, r11
	SWI       r25, r1, 584
	FPGE   r11, r25, r8
	FPUN   r20, r25, r8
	BITOR        r20, r20, r11
	FPDIV      r9, r21, r9
	SWI       r9, r1, 576
	FPDIV      r11, r7, r9
	SWI       r11, r1, 592
	FPGE   r7, r11, r8
	FPUN   r9, r11, r8
	BITOR        r21, r9, r7
	FPLT   r7, r26, r8
	FPLT   r9, r25, r8
	FPLT   r11, r11, r8
	ADDI      r8, r0, 0
	ADDI      r25, r0, 1
	bneid     r21, ($BB58_575)
	SWI       r25, r1, 600
# BB#574:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 600
$BB58_575:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADD      r21, r22, r0
	BITOR        r10, r12, r10
	bneid     r20, ($BB58_577)
	SWI       r25, r1, 604
# BB#576:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 604
$BB58_577:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r25, r1, 608
	ADD      r12, r21, r0
	bneid     r10, ($BB58_579)
	ADD      r20, r23, r0
# BB#578:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 608
$BB58_579:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	bneid     r11, ($BB58_581)
	SWI       r25, r1, 612
# BB#580:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 612
$BB58_581:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r25, r1, 616
	ADD      r22, r26, r0
	bneid     r9, ($BB58_583)
	SWI       r22, r1, 624
# BB#582:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 616
$BB58_583:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPMUL      r9, r24, r6
	SWI       r24, r1, 676
	FPMUL      r10, r20, r6
	SWI       r20, r1, 672
	FPMUL      r6, r12, r6
	SWI       r12, r1, 640
	bneid     r7, ($BB58_585)
	SWI       r25, r1, 620
# BB#584:                               # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	SWI       r8, r1, 620
$BB58_585:                              # %if.end6.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPADD      r5, r5, r6
	SWI       r5, r1, 560
	FPADD      r3, r3, r10
	SWI       r3, r1, 564
	FPADD      r3, r4, r9
	SWI       r3, r1, 568
	ORI       r21, r0, 1203982336
	brid      ($BB58_586)
	SWI       r0, r1, 336
$BB58_691:                              # %if.else24.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	beqid     r23, ($BB58_692)
	NOP    
# BB#693:                               # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r7, r4
	ADDI      r8, r0, 0
	ADDI      r5, r0, 1
	bneid     r3, ($BB58_695)
	ADD      r9, r5, r0
# BB#694:                               # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r8, r0
$BB58_695:                              # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_697)
	NOP    
# BB#696:                               # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r7, r4, r0
$BB58_697:                              # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGE   r3, r28, r7
	FPUN   r4, r28, r7
	BITOR        r3, r4, r3
	bneid     r3, ($BB58_699)
	NOP    
# BB#698:                               # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r5, r8, r0
$BB58_699:                              # %if.else31.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_701)
	NOP    
# BB#700:                               # %if.then35.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bslli     r3, r25, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	SWI       r6, r5, -4
	LWI       r5, r1, 580
	SW        r5, r4, r3
	brid      ($BB58_725)
	ADDI      r25, r25, 1
$BB58_692:                              # %if.then28.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bslli     r3, r25, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	brid      ($BB58_725)
	SWI       r6, r3, -4
$BB58_701:                              # %if.else40.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bslli     r3, r25, 2
	ADDI      r4, r1, 336
	ADD      r5, r4, r3
	LWI       r7, r1, 580
	SWI       r7, r5, -4
	SW        r6, r4, r3
	brid      ($BB58_725)
	ADDI      r25, r25, 1
$BB58_586:                              # %while.body.i.i.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        #       Parent Loop BB58_16 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB58_702 Depth 5
	ADDI      r3, r0, -1
	ADD      r27, r25, r3
	bslli     r3, r27, 2
	ADDI      r4, r1, 336
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 588
	ADD      r3, r3, r4
	SWI       r0, r1, 468
	SWI       r0, r1, 464
	LOAD      r4, r3, 0
	SWI        r4, r1, 464
	LOAD      r4, r3, 1
	SWI        r4, r1, 468
	LOAD      r4, r3, 2
	SWI        r4, r1, 472
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 476
	LOAD      r5, r4, 1
	SWI        r5, r1, 480
	LOAD      r4, r4, 2
	SWI        r4, r1, 484
	LWI       r4, r1, 604
	MULI      r11, r4, 12
	ADDI      r7, r1, 464
	ADD      r4, r7, r11
	ADDI      r5, r3, 7
	ADDI      r3, r3, 6
	LWI       r6, r1, 620
	MULI      r29, r6, 12
	LOAD      r23, r3, 0
	SWI       r23, r1, 488
	LOAD      r3, r5, 0
	SWI       r3, r1, 580
	SWI       r3, r1, 492
	LWI        r3, r4, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 584
	FPMUL      r24, r3, r4
	LW         r3, r7, r29
	LWI       r4, r1, 568
	FPRSUB     r3, r4, r3
	FPMUL      r5, r3, r22
	ADDI      r4, r0, 1
	FPGT   r3, r5, r24
	bneid     r3, ($BB58_588)
	NOP    
# BB#587:                               # %while.body.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r4, r0, 0
$BB58_588:                              # %while.body.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r4, ($BB58_589)
	NOP    
# BB#590:                               # %while.body.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI       r3, r1, 616
	MULI      r4, r3, 12
	ADD      r3, r7, r4
	LWI        r3, r3, 4
	LWI       r6, r1, 564
	FPRSUB     r3, r6, r3
	LWI       r6, r1, 584
	FPMUL      r6, r3, r6
	LWI       r3, r1, 608
	MULI      r8, r3, 12
	LW         r3, r7, r8
	LWI       r7, r1, 568
	FPRSUB     r3, r7, r3
	FPMUL      r10, r3, r22
	FPGT   r3, r6, r10
	bneid     r3, ($BB58_592)
	ADDI      r7, r0, 1
# BB#591:                               # %while.body.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r7, r0, 0
$BB58_592:                              # %while.body.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r7, ($BB58_593)
	NOP    
# BB#594:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r6, r5
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r3, ($BB58_596)
	ADD      r7, r12, r0
# BB#595:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r7, r20, r0
$BB58_596:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r7, ($BB58_598)
	NOP    
# BB#597:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r6, r5, r0
$BB58_598:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI       r3, r1, 600
	MULI      r7, r3, 12
	ADDI      r26, r1, 464
	ADD      r5, r26, r7
	FPLT   r3, r24, r10
	bneid     r3, ($BB58_600)
	ADD      r28, r12, r0
# BB#599:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r28, r20, r0
$BB58_600:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI        r3, r5, 8
	LWI       r5, r1, 560
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 592
	FPMUL      r5, r3, r5
	FPGT   r3, r6, r5
	bneid     r3, ($BB58_602)
	NOP    
# BB#601:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r12, r20, r0
$BB58_602:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r28, ($BB58_604)
	NOP    
# BB#603:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r24, r10, r0
$BB58_604:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r12, ($BB58_605)
	NOP    
# BB#606:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI       r3, r1, 612
	MULI      r10, r3, 12
	ADD      r3, r26, r10
	LWI        r3, r3, 8
	LWI       r9, r1, 560
	FPRSUB     r3, r9, r3
	LWI       r9, r1, 592
	FPMUL      r26, r3, r9
	FPGT   r3, r26, r24
	bneid     r3, ($BB58_608)
	ADDI      r9, r0, 1
# BB#607:                               # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r9, r0, 0
$BB58_608:                              # %if.end.i.i133.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_609)
	NOP    
# BB#610:                               # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPLT   r3, r5, r24
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r3, ($BB58_612)
	ADD      r9, r12, r0
# BB#611:                               # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r20, r0
$BB58_612:                              # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_614)
	NOP    
# BB#613:                               # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r5, r24, r0
$BB58_614:                              # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ORI       r3, r0, 0
	FPLT   r3, r5, r3
	bneid     r3, ($BB58_616)
	NOP    
# BB#615:                               # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r12, r20, r0
$BB58_616:                              # %if.end72.i.i137.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r12, ($BB58_617)
	NOP    
# BB#618:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r26, r6
	ADDI      r12, r0, 0
	ADDI      r5, r0, 1
	bneid     r3, ($BB58_620)
	ADD      r9, r5, r0
# BB#619:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r12, r0
$BB58_620:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_622)
	NOP    
# BB#621:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r26, r6, r0
$BB58_622:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPLT   r3, r21, r26
	bneid     r3, ($BB58_624)
	NOP    
# BB#623:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r5, r12, r0
$BB58_624:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_625)
	NOP    
# BB#626:                               # %if.end7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	beqid     r23, ($BB58_627)
	NOP    
# BB#628:                               # %if.end7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	SWI       r27, r1, 596
	ADD      r6, r0, r0
	ADDI      r3, r0, -1
	CMP       r3, r3, r23
	bneid     r3, ($BB58_702)
	NOP    
# BB#629:                               # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI       r3, r1, 580
	bslli     r3, r3, 3
	LWI       r5, r1, 588
	ADD      r5, r3, r5
	SWI       r0, r1, 500
	SWI       r0, r1, 496
	LOAD      r3, r5, 0
	SWI        r3, r1, 496
	LOAD      r3, r5, 1
	SWI        r3, r1, 500
	LOAD      r3, r5, 2
	SWI        r3, r1, 504
	ADDI      r6, r5, 3
	LOAD      r3, r6, 0
	SWI        r3, r1, 508
	LOAD      r3, r6, 1
	SWI        r3, r1, 512
	LOAD      r3, r6, 2
	SWI        r3, r1, 516
	ADDI      r3, r5, 6
	LOAD      r3, r3, 0
	SWI       r3, r1, 520
	ADDI      r3, r5, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 524
	ADDI      r6, r1, 496
	ADD      r3, r6, r11
	LWI        r3, r3, 4
	LWI       r5, r1, 564
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 584
	FPMUL      r31, r3, r5
	LW         r3, r6, r29
	LWI       r5, r1, 568
	FPRSUB     r3, r5, r3
	FPMUL      r5, r3, r22
	ADDI      r12, r0, 1
	FPGT   r3, r5, r31
	bneid     r3, ($BB58_631)
	NOP    
# BB#630:                               # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r12, r0, 0
$BB58_631:                              # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r23, r0, r0
	ORI       r28, r0, 1203982336
	bneid     r12, ($BB58_661)
	NOP    
# BB#632:                               # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r6, r4
	LW         r3, r6, r8
	LWI        r6, r9, 4
	LWI       r9, r1, 564
	FPRSUB     r6, r9, r6
	LWI       r9, r1, 584
	FPMUL      r6, r6, r9
	LWI       r9, r1, 568
	FPRSUB     r3, r9, r3
	FPMUL      r24, r3, r22
	FPGT   r3, r6, r24
	bneid     r3, ($BB58_634)
	ADDI      r12, r0, 1
# BB#633:                               # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r12, r0, 0
$BB58_634:                              # %if.then9.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r12, ($BB58_661)
	NOP    
# BB#635:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r6, r5
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r3, ($BB58_637)
	ADD      r9, r12, r0
# BB#636:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r20, r0
$BB58_637:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_639)
	NOP    
# BB#638:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r6, r5, r0
$BB58_639:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r5, r1, 496
	ADD      r26, r5, r7
	FPLT   r3, r31, r24
	bneid     r3, ($BB58_641)
	ADD      r23, r12, r0
# BB#640:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r23, r20, r0
$BB58_641:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI        r3, r26, 8
	LWI       r9, r1, 560
	FPRSUB     r3, r9, r3
	LWI       r9, r1, 592
	FPMUL      r26, r3, r9
	FPGT   r3, r6, r26
	bneid     r3, ($BB58_643)
	NOP    
# BB#642:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r12, r20, r0
$BB58_643:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r23, ($BB58_645)
	NOP    
# BB#644:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r31, r24, r0
$BB58_645:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r23, r0, r0
	ORI       r28, r0, 1203982336
	bneid     r12, ($BB58_661)
	NOP    
# BB#646:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r3, r5, r10
	LWI        r3, r3, 8
	LWI       r5, r1, 560
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 592
	FPMUL      r24, r3, r5
	FPGT   r3, r24, r31
	bneid     r3, ($BB58_648)
	ADDI      r5, r0, 1
# BB#647:                               # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r5, r0, 0
$BB58_648:                              # %if.end.i.i59.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_661)
	NOP    
# BB#649:                               # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPLT   r3, r26, r31
	ADDI      r12, r0, 0
	ADDI      r5, r0, 1
	bneid     r3, ($BB58_651)
	ADD      r9, r5, r0
# BB#650:                               # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r12, r0
$BB58_651:                              # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_653)
	NOP    
# BB#652:                               # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r26, r31, r0
$BB58_653:                              # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ORI       r3, r0, 0
	FPLT   r3, r26, r3
	bneid     r3, ($BB58_655)
	NOP    
# BB#654:                               # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r5, r12, r0
$BB58_655:                              # %if.end72.i.i63.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r23, r0, r0
	ORI       r28, r0, 1203982336
	bneid     r5, ($BB58_661)
	NOP    
# BB#656:                               # %if.end81.i.i67.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r24, r6
	ADDI      r23, r0, 1
	bneid     r3, ($BB58_658)
	ADD      r5, r23, r0
# BB#657:                               # %if.end81.i.i67.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r5, r0, 0
$BB58_658:                              # %if.end81.i.i67.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_660)
	NOP    
# BB#659:                               # %if.end81.i.i67.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r24, r6, r0
$BB58_660:                              # %if.end81.i.i67.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r28, r24, r0
$BB58_661:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI       r3, r1, 580
	ADDI      r6, r3, 1
	bslli     r3, r6, 3
	LWI       r5, r1, 588
	ADD      r5, r3, r5
	SWI       r0, r1, 532
	SWI       r0, r1, 528
	LOAD      r3, r5, 0
	SWI        r3, r1, 528
	LOAD      r3, r5, 1
	SWI        r3, r1, 532
	LOAD      r3, r5, 2
	SWI        r3, r1, 536
	ADDI      r9, r5, 3
	LOAD      r3, r9, 0
	SWI        r3, r1, 540
	LOAD      r3, r9, 1
	SWI        r3, r1, 544
	LOAD      r3, r9, 2
	SWI        r3, r1, 548
	ADDI      r3, r5, 6
	LOAD      r3, r3, 0
	SWI       r3, r1, 552
	ADDI      r3, r5, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 556
	ADDI      r12, r1, 528
	ADD      r3, r12, r11
	LWI        r3, r3, 4
	LWI       r5, r1, 564
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 584
	FPMUL      r11, r3, r5
	LW         r3, r12, r29
	LWI       r5, r1, 568
	FPRSUB     r3, r5, r3
	FPMUL      r5, r3, r22
	ADDI      r20, r0, 1
	FPGT   r3, r5, r11
	bneid     r3, ($BB58_663)
	NOP    
# BB#662:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r20, r0, 0
$BB58_663:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r20, ($BB58_688)
	LWI       r26, r1, 596
# BB#664:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r3, r12, r4
	LWI        r3, r3, 4
	LWI       r4, r1, 564
	FPRSUB     r3, r4, r3
	LWI       r4, r1, 584
	FPMUL      r4, r3, r4
	LW         r3, r12, r8
	LWI       r8, r1, 568
	FPRSUB     r3, r8, r3
	FPMUL      r8, r3, r22
	FPGT   r3, r4, r8
	bneid     r3, ($BB58_666)
	ADDI      r9, r0, 1
# BB#665:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r9, r0, 0
$BB58_666:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_688)
	NOP    
# BB#667:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPGT   r3, r4, r5
	ADDI      r24, r0, 0
	ADDI      r12, r0, 1
	bneid     r3, ($BB58_669)
	ADD      r9, r12, r0
# BB#668:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r24, r0
$BB58_669:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_671)
	NOP    
# BB#670:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r4, r5, r0
$BB58_671:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r5, r1, 528
	ADD      r20, r5, r7
	FPLT   r3, r11, r8
	bneid     r3, ($BB58_673)
	ADD      r7, r12, r0
# BB#672:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r7, r24, r0
$BB58_673:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	LWI        r3, r20, 8
	LWI       r9, r1, 560
	FPRSUB     r3, r9, r3
	LWI       r9, r1, 592
	FPMUL      r20, r3, r9
	FPGT   r3, r4, r20
	bneid     r3, ($BB58_675)
	NOP    
# BB#674:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r12, r24, r0
$BB58_675:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r7, ($BB58_677)
	NOP    
# BB#676:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r11, r8, r0
$BB58_677:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r12, ($BB58_688)
	NOP    
# BB#678:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r3, r5, r10
	LWI        r3, r3, 8
	LWI       r5, r1, 560
	FPRSUB     r3, r5, r3
	LWI       r5, r1, 592
	FPMUL      r7, r3, r5
	FPGT   r3, r7, r11
	bneid     r3, ($BB58_680)
	ADDI      r5, r0, 1
# BB#679:                               # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r5, r0, 0
$BB58_680:                              # %if.end.i.i7.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_688)
	NOP    
# BB#681:                               # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	FPLT   r3, r20, r11
	ADDI      r8, r0, 0
	ADDI      r5, r0, 1
	bneid     r3, ($BB58_683)
	ADD      r9, r5, r0
# BB#682:                               # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r9, r8, r0
$BB58_683:                              # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r9, ($BB58_685)
	NOP    
# BB#684:                               # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r20, r11, r0
$BB58_685:                              # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ORI       r3, r0, 0
	FPGE   r9, r20, r3
	FPUN   r3, r20, r3
	BITOR        r3, r3, r9
	bneid     r3, ($BB58_687)
	NOP    
# BB#686:                               # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADD      r5, r8, r0
$BB58_687:                              # %if.end72.i.i.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bneid     r5, ($BB58_691)
	NOP    
$BB58_688:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	ADDI      r3, r0, 1
	CMP       r3, r3, r23
	bneid     r3, ($BB58_689)
	NOP    
# BB#690:                               # %if.then21.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bslli     r3, r25, 2
	ADDI      r4, r1, 336
	ADD      r3, r4, r3
	LWI       r4, r1, 580
	brid      ($BB58_725)
	SWI       r4, r3, -4
$BB58_589:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_593:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_605:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_702:                              # %for.body.i.i.i.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        #       Parent Loop BB58_16 Depth=3
                                        #         Parent Loop BB58_586 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	MULI      r3, r6, 11
	LWI       r4, r1, 580
	ADD      r5, r3, r4
	LOAD      r11, r5, 0
	LOAD      r24, r5, 1
	LOAD      r26, r5, 2
	ADDI      r4, r5, 6
	ADDI      r3, r5, 3
	LOAD      r5, r3, 0
	LOAD      r8, r3, 1
	LOAD      r12, r3, 2
	LOAD      r7, r4, 0
	LOAD      r3, r4, 1
	LOAD      r4, r4, 2
	FPRSUB     r10, r26, r4
	FPRSUB     r25, r24, r3
	LWI       r9, r1, 576
	FPMUL      r3, r9, r25
	LWI       r22, r1, 572
	FPMUL      r4, r22, r10
	FPRSUB     r4, r3, r4
	FPRSUB     r29, r11, r7
	FPMUL      r3, r9, r29
	FPMUL      r7, r30, r10
	FPRSUB     r7, r7, r3
	FPRSUB     r5, r11, r5
	FPRSUB     r20, r24, r8
	FPMUL      r3, r7, r20
	FPMUL      r8, r4, r5
	FPADD      r9, r8, r3
	FPMUL      r3, r22, r29
	FPMUL      r8, r30, r25
	FPRSUB     r8, r3, r8
	FPRSUB     r12, r26, r12
	FPMUL      r3, r8, r12
	FPADD      r9, r9, r3
	ORI       r3, r0, 0
	FPGE   r22, r9, r3
	FPUN   r3, r9, r3
	BITOR        r3, r3, r22
	bneid     r3, ($BB58_704)
	ADDI      r31, r0, 1
# BB#703:                               # %for.body.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADDI      r31, r0, 0
$BB58_704:                              # %for.body.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	bneid     r31, ($BB58_706)
	ADD      r28, r9, r0
# BB#705:                               # %if.then.i.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	FPNEG      r28, r9
$BB58_706:                              # %_ZN4util4fabsERKf.exit.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ORI       r3, r0, 953267991
	FPLT   r3, r28, r3
	bneid     r3, ($BB58_708)
	ADDI      r28, r0, 1
# BB#707:                               # %_ZN4util4fabsERKf.exit.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADDI      r28, r0, 0
$BB58_708:                              # %_ZN4util4fabsERKf.exit.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	bneid     r28, ($BB58_723)
	NOP    
# BB#709:                               # %if.end.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	LWI       r3, r1, 560
	FPRSUB     r26, r26, r3
	LWI       r3, r1, 568
	FPRSUB     r11, r11, r3
	LWI       r3, r1, 564
	FPRSUB     r24, r24, r3
	FPMUL      r28, r24, r5
	FPMUL      r31, r11, r20
	FPMUL      r3, r26, r5
	FPMUL      r22, r11, r12
	FPMUL      r27, r26, r20
	FPMUL      r12, r24, r12
	FPRSUB     r5, r28, r31
	FPRSUB     r20, r22, r3
	FPRSUB     r12, r27, r12
	FPMUL      r3, r20, r25
	FPMUL      r22, r12, r29
	FPADD      r3, r22, r3
	FPMUL      r10, r5, r10
	FPADD      r3, r3, r10
	ORI       r10, r0, 1065353216
	FPDIV      r25, r10, r9
	FPMUL      r10, r3, r25
	ORI       r3, r0, 0
	FPLT   r3, r10, r3
	bneid     r3, ($BB58_711)
	ADDI      r9, r0, 1
# BB#710:                               # %if.end.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADDI      r9, r0, 0
$BB58_711:                              # %if.end.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	bneid     r9, ($BB58_723)
	NOP    
# BB#712:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	FPMUL      r3, r7, r24
	FPMUL      r4, r4, r11
	FPADD      r3, r4, r3
	LWI       r4, r1, 572
	FPMUL      r4, r20, r4
	FPMUL      r7, r12, r30
	FPMUL      r8, r8, r26
	FPADD      r3, r3, r8
	FPADD      r4, r7, r4
	LWI       r7, r1, 576
	FPMUL      r5, r5, r7
	FPADD      r4, r4, r5
	FPMUL      r4, r4, r25
	FPMUL      r3, r3, r25
	FPADD      r5, r3, r4
	ORI       r7, r0, 1065353216
	FPLE   r8, r5, r7
	ORI       r5, r0, 0
	FPGE   r11, r3, r5
	FPGE   r12, r4, r5
	ADDI      r7, r0, 0
	ADDI      r4, r0, 1
	bneid     r8, ($BB58_714)
	ADD      r5, r4, r0
# BB#713:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r5, r7, r0
$BB58_714:                              # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	bneid     r12, ($BB58_716)
	ADD      r8, r4, r0
# BB#715:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r8, r7, r0
$BB58_716:                              # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	bneid     r11, ($BB58_718)
	ADD      r12, r4, r0
# BB#717:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r12, r7, r0
$BB58_718:                              # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	FPLT   r3, r10, r21
	bneid     r3, ($BB58_720)
	NOP    
# BB#719:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r4, r7, r0
$BB58_720:                              # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	BITAND       r3, r12, r8
	BITAND       r3, r3, r5
	BITAND       r3, r4, r3
	bneid     r3, ($BB58_722)
	NOP    
# BB#721:                               # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r10, r21, r0
$BB58_722:                              # %if.end9.i.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADD      r21, r10, r0
$BB58_723:                              # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit.i.i.i.i
                                        #   in Loop: Header=BB58_702 Depth=5
	ADDI      r6, r6, 1
	CMP       r3, r23, r6
	bneid     r3, ($BB58_702)
	NOP    
# BB#724:                               #   in Loop: Header=BB58_586 Depth=4
	LWI       r25, r1, 596
	brid      ($BB58_725)
	LWI       r22, r1, 624
$BB58_609:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_617:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_625:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_627:                              #   in Loop: Header=BB58_586 Depth=4
	brid      ($BB58_725)
	ADD      r25, r27, r0
$BB58_689:                              #   in Loop: Header=BB58_586 Depth=4
	ADD      r25, r26, r0
$BB58_725:                              # %while.cond.backedge.i.i.i
                                        #   in Loop: Header=BB58_586 Depth=4
	bgtid     r25, ($BB58_586)
	NOP    
# BB#726:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 692
	FPDIV      r3, r3, r4
	FPGT   r4, r3, r21
	FPUN   r3, r3, r21
	BITOR        r3, r3, r4
	bneid     r3, ($BB58_728)
	ADDI      r5, r0, 1
# BB#727:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r5, r0, 0
$BB58_728:                              # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r3, r0, 0
	ADD      r4, r3, r0
	ADD      r6, r3, r0
	LWI       r27, r1, 640
	LWI       r28, r1, 672
	bneid     r5, ($BB58_730)
	LWI       r20, r1, 676
# BB#729:                               # %if.then20.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	LWI       r3, r1, 684
	FPMUL      r3, r28, r3
	LWI       r4, r1, 680
	FPMUL      r4, r20, r4
	FPADD      r3, r4, r3
	LWI       r4, r1, 688
	FPMUL      r4, r27, r4
	FPADD      r3, r3, r4
	ORI       r5, r0, 0
	FPMAX     r3, r3, r5
	LWI       r4, r1, 628
	FPMUL      r6, r4, r3
	LWI       r4, r1, 632
	FPMUL      r4, r4, r3
	LWI       r7, r1, 636
	FPMUL      r3, r7, r3
	FPADD      r3, r3, r5
	FPADD      r4, r4, r5
	FPADD      r6, r6, r5
$BB58_730:                              # %do.body.i.i.i
                                        #   Parent Loop BB58_14 Depth=1
                                        #     Parent Loop BB58_15 Depth=2
                                        #       Parent Loop BB58_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	RAND      r5
	RAND      r7
	FPADD      r7, r7, r7
	ORI       r8, r0, -1082130432
	FPADD      r7, r7, r8
	FPADD      r5, r5, r5
	FPADD      r8, r5, r8
	FPMUL      r5, r8, r8
	FPMUL      r10, r7, r7
	FPADD      r9, r5, r10
	ORI       r11, r0, 1065353216
	FPGE   r11, r9, r11
	bneid     r11, ($BB58_732)
	ADDI      r9, r0, 1
# BB#731:                               # %do.body.i.i.i
                                        #   in Loop: Header=BB58_730 Depth=4
	ADDI      r9, r0, 0
$BB58_732:                              # %do.body.i.i.i
                                        #   in Loop: Header=BB58_730 Depth=4
	bneid     r9, ($BB58_730)
	NOP    
# BB#733:                               # %do.end.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r9, r0, 0
	FPGE   r11, r20, r9
	FPUN   r9, r20, r9
	BITOR        r9, r9, r11
	bneid     r9, ($BB58_735)
	ADDI      r12, r0, 1
# BB#734:                               # %do.end.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r12, r0, 0
$BB58_735:                              # %do.end.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r9, r0, 1065353216
	FPRSUB     r5, r5, r9
	FPRSUB     r5, r10, r5
	FPINVSQRT r11, r5
	bneid     r12, ($BB58_737)
	ADD      r10, r20, r0
# BB#736:                               # %if.then.i59.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPNEG      r10, r20
$BB58_737:                              # %_ZN4util4fabsERKf.exit61.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r5, r0, 0
	FPGE   r12, r28, r5
	FPUN   r5, r28, r5
	BITOR        r12, r5, r12
	bneid     r12, ($BB58_739)
	ADDI      r5, r0, 1
# BB#738:                               # %_ZN4util4fabsERKf.exit61.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r5, r0, 0
$BB58_739:                              # %_ZN4util4fabsERKf.exit61.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADD      r29, r20, r0
	ADD      r21, r28, r0
	bneid     r5, ($BB58_741)
	LWI       r25, r1, 664
# BB#740:                               # %if.then.i54.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPNEG      r21, r28
$BB58_741:                              # %_ZN4util4fabsERKf.exit56.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r5, r0, 0
	FPGE   r12, r27, r5
	FPUN   r5, r27, r5
	BITOR        r5, r5, r12
	bneid     r5, ($BB58_743)
	ADDI      r12, r0, 1
# BB#742:                               # %_ZN4util4fabsERKf.exit56.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r12, r0, 0
$BB58_743:                              # %_ZN4util4fabsERKf.exit56.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADD      r5, r27, r0
	LWI       r20, r1, 712
	LWI       r22, r1, 644
	LWI       r23, r1, 648
	bneid     r12, ($BB58_745)
	LWI       r24, r1, 652
# BB#744:                               # %if.then.i.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPNEG      r5, r27
$BB58_745:                              # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPGE   r12, r10, r21
	FPUN   r20, r10, r21
	BITOR        r12, r20, r12
	ADDI      r20, r0, 1
	bneid     r12, ($BB58_747)
	LWI       r26, r1, 668
# BB#746:                               # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r20, r0, 0
$BB58_747:                              # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPDIV      r9, r9, r11
	ORI       r12, r0, 1065353216
	ORI       r11, r0, 0
	bneid     r20, ($BB58_751)
	NOP    
# BB#748:                               # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPLT   r10, r10, r5
	bneid     r10, ($BB58_750)
	ADDI      r20, r0, 1
# BB#749:                               # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r20, r0, 0
$BB58_750:                              # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	bneid     r20, ($BB58_755)
	ADD      r10, r11, r0
$BB58_751:                              # %if.else.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	FPLT   r10, r21, r5
	bneid     r10, ($BB58_753)
	ADDI      r5, r0, 1
# BB#752:                               # %if.else.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ADDI      r5, r0, 0
$BB58_753:                              # %if.else.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r10, r0, 1065353216
	ORI       r11, r0, 0
	bneid     r5, ($BB58_755)
	ADD      r12, r11, r0
# BB#754:                               # %if.else19.i.i.i.i
                                        #   in Loop: Header=BB58_16 Depth=3
	ORI       r10, r0, 0
	ORI       r11, r0, 1065353216
	brid      ($BB58_755)
	ADD      r12, r10, r0
$BB58_23:                               #   in Loop: Header=BB58_15 Depth=2
	LWI       r4, r1, 644
	LWI       r5, r1, 648
	brid      ($BB58_568)
	LWI       r6, r1, 652
$BB58_567:                              # %_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color.exit.i.thread
                                        #   in Loop: Header=BB58_15 Depth=2
	ORI       r3, r0, 1065151889
	LWI       r4, r1, 664
	FPMUL      r3, r4, r3
	LWI       r4, r1, 644
	FPADD      r4, r4, r3
	ORI       r3, r0, 1060806590
	LWI       r5, r1, 660
	FPMUL      r3, r5, r3
	LWI       r5, r1, 648
	FPADD      r5, r5, r3
	ORI       r3, r0, 1057988018
	LWI       r6, r1, 656
	FPMUL      r3, r6, r3
	LWI       r6, r1, 652
	FPADD      r6, r6, r3
$BB58_568:                              # %_ZNK5Scene8GetColorERK3RayRKi.exit
                                        #   in Loop: Header=BB58_15 Depth=2
	LWI       r3, r1, 720
	FPADD      r3, r3, r4
	SWI       r3, r1, 720
	LWI       r3, r1, 724
	FPADD      r3, r3, r5
	SWI       r3, r1, 724
	LWI       r4, r1, 732
	FPADD      r4, r4, r6
	LWI       r6, r1, 728
	ADDI      r6, r6, 1
	LWI       r3, r1, 712
	CMP       r3, r3, r6
	bltid     r3, ($BB58_15)
	NOP    
# BB#398:                               # %if.end.us33
                                        #   in Loop: Header=BB58_14 Depth=1
	LWI       r3, r1, 800
	FPDIV      r4, r4, r3
	ORI       r3, r0, 0
	FPLT   r6, r4, r3
	bneid     r6, ($BB58_400)
	ADDI      r5, r0, 1
# BB#399:                               # %if.end.us33
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r5, r0, 0
$BB58_400:                              # %if.end.us33
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r5, ($BB58_405)
	LWI       r9, r1, 808
# BB#401:                               # %cond.false.i.i35
                                        #   in Loop: Header=BB58_14 Depth=1
	ORI       r3, r0, 1065353216
	FPGT   r6, r4, r3
	bneid     r6, ($BB58_403)
	ADDI      r5, r0, 1
# BB#402:                               # %cond.false.i.i35
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r5, r0, 0
$BB58_403:                              # %cond.false.i.i35
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r5, ($BB58_405)
	NOP    
# BB#404:                               # %cond.false5.i.i36
                                        #   in Loop: Header=BB58_14 Depth=1
	ADD      r3, r4, r0
$BB58_405:                              # %cond.end7.i.i40
                                        #   in Loop: Header=BB58_14 Depth=1
	LWI       r4, r1, 800
	LWI       r5, r1, 724
	FPDIV      r5, r5, r4
	ORI       r4, r0, 0
	FPLT   r7, r5, r4
	bneid     r7, ($BB58_407)
	ADDI      r6, r0, 1
# BB#406:                               # %cond.end7.i.i40
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r6, r0, 0
$BB58_407:                              # %cond.end7.i.i40
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r6, ($BB58_412)
	NOP    
# BB#408:                               # %cond.false12.i.i42
                                        #   in Loop: Header=BB58_14 Depth=1
	ORI       r4, r0, 1065353216
	FPGT   r7, r5, r4
	bneid     r7, ($BB58_410)
	ADDI      r6, r0, 1
# BB#409:                               # %cond.false12.i.i42
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r6, r0, 0
$BB58_410:                              # %cond.false12.i.i42
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r6, ($BB58_412)
	NOP    
# BB#411:                               # %cond.false16.i.i43
                                        #   in Loop: Header=BB58_14 Depth=1
	ADD      r4, r5, r0
$BB58_412:                              # %cond.end20.i.i47
                                        #   in Loop: Header=BB58_14 Depth=1
	LWI       r5, r1, 800
	LWI       r6, r1, 720
	FPDIV      r6, r6, r5
	ORI       r5, r0, 0
	FPLT   r8, r6, r5
	bneid     r8, ($BB58_414)
	ADDI      r7, r0, 1
# BB#413:                               # %cond.end20.i.i47
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r7, r0, 0
$BB58_414:                              # %cond.end20.i.i47
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r7, ($BB58_419)
	NOP    
# BB#415:                               # %cond.false25.i.i49
                                        #   in Loop: Header=BB58_14 Depth=1
	ORI       r5, r0, 1065353216
	FPGT   r8, r6, r5
	bneid     r8, ($BB58_417)
	ADDI      r7, r0, 1
# BB#416:                               # %cond.false25.i.i49
                                        #   in Loop: Header=BB58_14 Depth=1
	ADDI      r7, r0, 0
$BB58_417:                              # %cond.false25.i.i49
                                        #   in Loop: Header=BB58_14 Depth=1
	bneid     r7, ($BB58_419)
	NOP    
# BB#418:                               # %cond.false29.i.i50
                                        #   in Loop: Header=BB58_14 Depth=1
	ADD      r5, r6, r0
$BB58_419:                              # %_ZN5Image3SetERKiS1_RK5Color.exit66
                                        #   in Loop: Header=BB58_14 Depth=1
	LWI       r6, r1, 816
	LWI       r7, r1, 820
	ADD      r6, r6, r7
	MULI      r6, r6, 3
	ADD      r6, r6, r9
	STORE     r6, r3, 0
	STORE     r6, r4, 1
	STORE     r6, r5, 2
	ATOMIC_INC r3, 0
	LWI       r4, r1, 804
	CMP       r4, r4, r3
	bltid     r4, ($BB58_14)
	NOP    
	brid      ($BB58_757)
	NOP    
$BB58_24:                               # %if.end.preheader
	ADD      r10, r9, r0
	ORI       r4, r0, 0
	LWI       r5, r1, 800
	FPDIV      r5, r4, r5
	FPLT   r4, r5, r4
	ADDI      r8, r0, 0
	ADDI      r7, r0, 1
	bneid     r4, ($BB58_26)
	ADD      r6, r7, r0
# BB#25:                                # %if.end.preheader
	ADD      r6, r8, r0
$BB58_26:                               # %if.end.preheader
	ORI       r4, r0, 1065353216
	FPGT   r9, r5, r4
	bneid     r9, ($BB58_28)
	NOP    
# BB#27:                                # %if.end.preheader
	ADD      r7, r8, r0
$BB58_28:                               # %if.end.preheader
	bneid     r7, ($BB58_30)
	ADD      r8, r10, r0
# BB#29:                                # %if.end.preheader
	ADD      r4, r5, r0
$BB58_30:                               # %if.end.preheader
	beqid     r6, ($BB58_31)
	NOP    
# BB#35:                                # %if.end.preheader.split.us
	ORI       r4, r0, 1065353216
	FPLE   r6, r5, r4
	FPUN   r4, r5, r4
	BITOR        r5, r4, r6
	bneid     r5, ($BB58_37)
	ADDI      r4, r0, 1
# BB#36:                                # %if.end.preheader.split.us
	ADDI      r4, r0, 0
$BB58_37:                               # %if.end.preheader.split.us
	bneid     r4, ($BB58_39)
	NOP    
$BB58_38:                               # %_ZN5Image3SetERKiS1_RK5Color.exit.us.us
                                        # =>This Inner Loop Header: Depth=1
	MULI      r3, r3, 3
	ADD      r3, r3, r8
	ORI       r4, r0, 0
	STORE     r3, r4, 0
	STORE     r3, r4, 1
	STORE     r3, r4, 2
	ATOMIC_INC r3, 0
	CMP       r4, r11, r3
	bltid     r4, ($BB58_38)
	NOP    
	brid      ($BB58_757)
	NOP    
$BB58_39:                               # %_ZN5Image3SetERKiS1_RK5Color.exit.us
                                        # =>This Inner Loop Header: Depth=1
	MULI      r3, r3, 3
	ADD      r3, r3, r8
	ORI       r4, r0, 0
	STORE     r3, r4, 0
	STORE     r3, r4, 1
	STORE     r3, r4, 2
	ATOMIC_INC r3, 0
	CMP       r4, r11, r3
	bltid     r4, ($BB58_39)
	NOP    
	brid      ($BB58_757)
	NOP    
$BB58_31:                               # %if.end.preheader.if.end.preheader.split_crit_edge
	ORI       r6, r0, 1065353216
	FPGT   r7, r5, r6
	bneid     r7, ($BB58_33)
	ADDI      r6, r0, 1
# BB#32:                                # %if.end.preheader.if.end.preheader.split_crit_edge
	ADDI      r6, r0, 0
$BB58_33:                               # %if.end.preheader.if.end.preheader.split_crit_edge
	beqid     r6, ($BB58_756)
	NOP    
$BB58_34:                               # %_ZN5Image3SetERKiS1_RK5Color.exit.us1138
                                        # =>This Inner Loop Header: Depth=1
	MULI      r3, r3, 3
	ADD      r3, r3, r8
	STORE     r3, r4, 0
	STORE     r3, r4, 1
	ORI       r5, r0, 1065353216
	STORE     r3, r5, 2
	ATOMIC_INC r3, 0
	CMP       r5, r11, r3
	bltid     r5, ($BB58_34)
	NOP    
	brid      ($BB58_757)
	NOP    
$BB58_756:                              # %_ZN5Image3SetERKiS1_RK5Color.exit
                                        # =>This Inner Loop Header: Depth=1
	MULI      r3, r3, 3
	ADD      r3, r3, r8
	STORE     r3, r4, 0
	STORE     r3, r4, 1
	STORE     r3, r5, 2
	ATOMIC_INC r3, 0
	CMP       r6, r11, r3
	bltid     r6, ($BB58_756)
	NOP    
$BB58_757:                              # %for.end20
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 824
	.end	_Z9trax_mainv
$tmp58:
	.size	_Z9trax_mainv, ($tmp58)-_Z9trax_mainv

	.globl	_ZN5Scene4LoadEv
	.align	2
	.type	_ZN5Scene4LoadEv,@function
	.ent	_ZN5Scene4LoadEv        # @_ZN5Scene4LoadEv
_ZN5Scene4LoadEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADD      r3, r0, r0
	LOAD      r4, r3, 12
	LOAD      r6, r4, 0
	SWI        r6, r5, 0
	LOAD      r6, r4, 1
	SWI        r6, r5, 4
	LOAD      r4, r4, 2
	SWI        r4, r5, 8
	ADDI      r4, r0, 1065353216
	SWI       r4, r5, 12
	SWI       r4, r5, 16
	SWI       r4, r5, 20
	LOAD      r4, r3, 28
	SWI       r4, r5, 24
	LOAD      r4, r3, 29
	SWI       r4, r5, 28
	LOAD      r4, r3, 9
	SWI       r4, r5, 32
	LOAD      r3, r3, 8
	SWI       r3, r5, 36
	ADDI      r3, r0, 1057988018
	SWI       r3, r5, 40
	ADDI      r3, r0, 1060806590
	SWI       r3, r5, 44
	ADDI      r3, r0, 1065151889
	rtsd      r15, 8
	SWI       r3, r5, 48
	.end	_ZN5Scene4LoadEv
$tmp59:
	.size	_ZN5Scene4LoadEv, ($tmp59)-_ZN5Scene4LoadEv

	.globl	_ZNK5Scene11BVHNodeAddrERKi
	.align	2
	.type	_ZNK5Scene11BVHNodeAddrERKi,@function
	.ent	_ZNK5Scene11BVHNodeAddrERKi # @_ZNK5Scene11BVHNodeAddrERKi
_ZNK5Scene11BVHNodeAddrERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r5, 36
	LWI       r4, r6, 0
	bslli     r4, r4, 3
	rtsd      r15, 8
	ADD      r3, r4, r3
	.end	_ZNK5Scene11BVHNodeAddrERKi
$tmp60:
	.size	_ZNK5Scene11BVHNodeAddrERKi, ($tmp60)-_ZNK5Scene11BVHNodeAddrERKi

	.globl	_ZNK5Scene8TraceRayERK3RayR9HitRecord
	.align	2
	.type	_ZNK5Scene8TraceRayERK3RayR9HitRecord,@function
	.ent	_ZNK5Scene8TraceRayERK3RayR9HitRecord # @_ZNK5Scene8TraceRayERK3RayR9HitRecord
_ZNK5Scene8TraceRayERK3RayR9HitRecord:
	.frame	r1,292,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -292
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r7, r1, 304
	ADD      r30, r6, r0
	SWI       r30, r1, 284
	SWI       r5, r1, 296
	ADDI      r23, r0, 1
	brid      ($BB61_1)
	SWI       r0, r1, 48
$BB61_98:                               # %if.else24
                                        #   in Loop: Header=BB61_1 Depth=1
	beqid     r9, ($BB61_99)
	NOP    
# BB#100:                               # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r6, r21, r12
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r6, ($BB61_102)
	ADD      r5, r3, r0
# BB#101:                               # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r5, r4, r0
$BB61_102:                              # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r5, ($BB61_104)
	NOP    
# BB#103:                               # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r21, r12, r0
$BB61_104:                              # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGE   r5, r10, r21
	FPUN   r6, r10, r21
	BITOR        r5, r6, r5
	bneid     r5, ($BB61_106)
	NOP    
# BB#105:                               # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r4, r0
$BB61_106:                              # %if.else31
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_108)
	NOP    
# BB#107:                               # %if.then35
                                        #   in Loop: Header=BB61_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r5, r4, r3
	SWI       r11, r5, -4
	SW        r26, r4, r3
	brid      ($BB61_134)
	ADDI      r23, r8, 1
$BB61_99:                               # %if.then28
                                        #   in Loop: Header=BB61_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r3, r4, r3
	SWI       r11, r3, -4
	brid      ($BB61_134)
	ADD      r23, r8, r0
$BB61_108:                              # %if.else40
                                        #   in Loop: Header=BB61_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r5, r4, r3
	SWI       r26, r5, -4
	SW        r11, r4, r3
	brid      ($BB61_134)
	ADDI      r23, r8, 1
$BB61_1:                                # %while.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_109 Depth 2
	ADD      r8, r23, r0
	ADDI      r3, r0, -1
	ADD      r23, r8, r3
	bslli     r3, r23, 2
	ADDI      r4, r1, 48
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 296
	LWI       r4, r4, 36
	ADD      r3, r3, r4
	SWI       r0, r1, 192
	SWI       r0, r1, 188
	SWI       r0, r1, 184
	SWI       r0, r1, 180
	SWI       r0, r1, 176
	LOAD      r4, r3, 0
	SWI        r4, r1, 176
	LOAD      r4, r3, 1
	SWI        r4, r1, 180
	LOAD      r4, r3, 2
	SWI        r4, r1, 184
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 188
	LOAD      r5, r4, 1
	SWI        r5, r1, 192
	LOAD      r4, r4, 2
	SWI        r4, r1, 196
	ADDI      r4, r3, 7
	ADDI      r3, r3, 6
	LOAD      r25, r3, 0
	SWI       r25, r1, 200
	LOAD      r24, r4, 0
	SWI       r24, r1, 204
	LWI       r5, r30, 40
	RSUBI     r3, r5, 1
	MULI      r3, r3, 12
	ADDI      r4, r1, 176
	ADD      r6, r4, r3
	LWI       r9, r30, 36
	MULI      r3, r9, 12
	LW         r3, r4, r3
	LWI        r12, r30, 0
	FPRSUB     r3, r12, r3
	LWI        r20, r30, 24
	FPMUL      r3, r3, r20
	LWI        r6, r6, 4
	LWI        r7, r30, 4
	FPRSUB     r6, r7, r6
	LWI        r10, r30, 28
	FPMUL      r11, r6, r10
	ADDI      r6, r0, 1
	FPGT   r21, r3, r11
	bneid     r21, ($BB61_3)
	NOP    
# BB#2:                                 # %while.body
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r6, r0, 0
$BB61_3:                                # %while.body
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_134)
	NOP    
# BB#4:                                 # %while.body
                                        #   in Loop: Header=BB61_1 Depth=1
	MULI      r5, r5, 12
	ADD      r5, r4, r5
	LWI        r5, r5, 4
	FPRSUB     r5, r7, r5
	FPMUL      r10, r5, r10
	RSUBI     r5, r9, 1
	MULI      r5, r5, 12
	LW         r4, r4, r5
	FPRSUB     r4, r12, r4
	FPMUL      r12, r4, r20
	FPGT   r5, r10, r12
	bneid     r5, ($BB61_6)
	ADDI      r4, r0, 1
# BB#5:                                 # %while.body
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r4, r0, 0
$BB61_6:                                # %while.body
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_134)
	NOP    
# BB#7:                                 # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r5, r10, r3
	ADDI      r21, r0, 0
	ADDI      r20, r0, 1
	bneid     r5, ($BB61_9)
	ADD      r4, r20, r0
# BB#8:                                 # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r21, r0
$BB61_9:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_11)
	NOP    
# BB#10:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r10, r3, r0
$BB61_11:                               # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r3, r30, 44
	RSUBI     r4, r3, 1
	MULI      r5, r4, 12
	ADDI      r4, r1, 176
	ADD      r5, r4, r5
	FPLT   r6, r11, r12
	bneid     r6, ($BB61_13)
	ADD      r22, r20, r0
# BB#12:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r22, r21, r0
$BB61_13:                               # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI        r5, r5, 8
	LWI        r7, r30, 8
	FPRSUB     r5, r7, r5
	LWI        r9, r30, 32
	FPMUL      r5, r5, r9
	FPGT   r6, r10, r5
	bneid     r6, ($BB61_15)
	NOP    
# BB#14:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r20, r21, r0
$BB61_15:                               # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r22, ($BB61_17)
	NOP    
# BB#16:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r11, r12, r0
$BB61_17:                               # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r20, ($BB61_134)
	NOP    
# BB#18:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r26, r24, r0
	MULI      r3, r3, 12
	ADD      r3, r4, r3
	LWI        r3, r3, 8
	FPRSUB     r3, r7, r3
	FPMUL      r3, r3, r9
	FPGT   r6, r3, r11
	bneid     r6, ($BB61_20)
	ADDI      r4, r0, 1
# BB#19:                                # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r4, r0, 0
$BB61_20:                               # %if.end.i.i133
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_134)
	SWI       r26, r1, 276
# BB#21:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	FPLT   r9, r5, r11
	ADDI      r7, r0, 0
	ADDI      r4, r0, 1
	bneid     r9, ($BB61_23)
	ADD      r6, r4, r0
# BB#22:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r6, r7, r0
$BB61_23:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_25)
	NOP    
# BB#24:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r5, r11, r0
$BB61_25:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	ORI       r6, r0, 0
	FPLT   r5, r5, r6
	bneid     r5, ($BB61_27)
	NOP    
# BB#26:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r7, r0
$BB61_27:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_134)
	NOP    
# BB#28:                                # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r7, r3, r10
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB61_30)
	ADD      r6, r4, r0
# BB#29:                                # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r6, r5, r0
$BB61_30:                               # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_32)
	NOP    
# BB#31:                                # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r10, r0
$BB61_32:                               # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r6, r1, 304
	LWI        r6, r6, 0
	FPLT   r3, r6, r3
	bneid     r3, ($BB61_34)
	NOP    
# BB#33:                                # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r5, r0
$BB61_34:                               # %if.end
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_134)
	NOP    
# BB#35:                                # %if.end7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r25, r0
	SWI       r4, r1, 280
	beqid     r4, ($BB61_134)
	NOP    
# BB#36:                                # %if.end7
                                        #   in Loop: Header=BB61_1 Depth=1
	SWI       r23, r1, 288
	ADD      r10, r0, r0
	ADDI      r3, r0, -1
	CMP       r3, r3, r4
	bneid     r3, ($BB61_109)
	NOP    
# BB#37:                                # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r3, r1, 296
	LWI       r3, r3, 36
	bslli     r4, r26, 3
	ADD      r3, r4, r3
	SWI       r0, r1, 224
	SWI       r0, r1, 220
	SWI       r0, r1, 216
	SWI       r0, r1, 212
	SWI       r0, r1, 208
	LOAD      r4, r3, 0
	SWI        r4, r1, 208
	LOAD      r4, r3, 1
	SWI        r4, r1, 212
	LOAD      r4, r3, 2
	SWI        r4, r1, 216
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 220
	LOAD      r5, r4, 1
	SWI        r5, r1, 224
	LOAD      r4, r4, 2
	SWI        r4, r1, 228
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 232
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 236
	LWI       r3, r30, 40
	RSUBI     r4, r3, 1
	MULI      r5, r4, 12
	ADDI      r4, r1, 208
	ADD      r6, r4, r5
	LWI       r11, r30, 36
	MULI      r5, r11, 12
	LW         r7, r4, r5
	LWI        r5, r30, 0
	FPRSUB     r7, r5, r7
	LWI        r20, r30, 24
	FPMUL      r21, r7, r20
	LWI        r6, r6, 4
	LWI        r7, r30, 4
	FPRSUB     r6, r7, r6
	LWI        r22, r30, 28
	FPMUL      r12, r6, r22
	ADDI      r6, r0, 1
	FPGT   r9, r21, r12
	bneid     r9, ($BB61_39)
	NOP    
# BB#38:                                # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r6, r0, 0
$BB61_39:                               # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r6, ($BB61_69)
	NOP    
# BB#40:                                # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	RSUBI     r6, r11, 1
	MULI      r6, r6, 12
	LW         r6, r4, r6
	MULI      r3, r3, 12
	ADD      r3, r4, r3
	LWI        r3, r3, 4
	FPRSUB     r3, r7, r3
	FPMUL      r11, r3, r22
	FPRSUB     r3, r5, r6
	FPMUL      r20, r3, r20
	FPGT   r4, r11, r20
	bneid     r4, ($BB61_42)
	ADDI      r3, r0, 1
# BB#41:                                # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r3, r0, 0
$BB61_42:                               # %if.then9
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_69)
	NOP    
# BB#43:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r4, r11, r21
	ADDI      r9, r0, 0
	ADDI      r5, r0, 1
	bneid     r4, ($BB61_45)
	ADD      r3, r5, r0
# BB#44:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r9, r0
$BB61_45:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_47)
	NOP    
# BB#46:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r11, r21, r0
$BB61_47:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r21, r30, 44
	RSUBI     r3, r21, 1
	MULI      r3, r3, 12
	ADDI      r4, r1, 208
	ADD      r3, r4, r3
	FPLT   r6, r12, r20
	bneid     r6, ($BB61_49)
	ADD      r10, r5, r0
# BB#48:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r10, r9, r0
$BB61_49:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI        r3, r3, 8
	LWI        r22, r30, 8
	FPRSUB     r3, r22, r3
	LWI        r7, r30, 32
	FPMUL      r3, r3, r7
	FPGT   r6, r11, r3
	bneid     r6, ($BB61_51)
	NOP    
# BB#50:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r5, r9, r0
$BB61_51:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r10, ($BB61_53)
	NOP    
# BB#52:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r12, r20, r0
$BB61_53:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r5, ($BB61_69)
	NOP    
# BB#54:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	MULI      r5, r21, 12
	ADD      r4, r4, r5
	LWI        r4, r4, 8
	FPRSUB     r4, r22, r4
	FPMUL      r20, r4, r7
	FPGT   r5, r20, r12
	bneid     r5, ($BB61_56)
	ADDI      r4, r0, 1
# BB#55:                                # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r4, r0, 0
$BB61_56:                               # %if.end.i.i59
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_69)
	NOP    
# BB#57:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	FPLT   r7, r3, r12
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB61_59)
	ADD      r6, r4, r0
# BB#58:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r6, r5, r0
$BB61_59:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_61)
	NOP    
# BB#60:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r12, r0
$BB61_61:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	ORI       r6, r0, 0
	FPLT   r3, r3, r6
	bneid     r3, ($BB61_63)
	NOP    
# BB#62:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r5, r0
$BB61_63:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r4, ($BB61_69)
	NOP    
# BB#64:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r4, r20, r11
	ADDI      r9, r0, 1
	bneid     r4, ($BB61_66)
	ADD      r3, r9, r0
# BB#65:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r3, r0, 0
$BB61_66:                               # %if.end81.i.i67
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_68)
	NOP    
# BB#67:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r20, r11, r0
$BB61_68:                               # %if.end81.i.i67
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r10, r20, r0
$BB61_69:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r3, r1, 296
	LWI       r3, r3, 36
	ADDI      r11, r26, 1
	bslli     r4, r11, 3
	ADD      r3, r4, r3
	SWI       r0, r1, 256
	SWI       r0, r1, 252
	SWI       r0, r1, 248
	SWI       r0, r1, 244
	SWI       r0, r1, 240
	LOAD      r4, r3, 0
	SWI        r4, r1, 240
	LOAD      r4, r3, 1
	SWI        r4, r1, 244
	LOAD      r4, r3, 2
	SWI        r4, r1, 248
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 252
	LOAD      r5, r4, 1
	SWI        r5, r1, 256
	LOAD      r4, r4, 2
	SWI        r4, r1, 260
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 264
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 268
	LWI       r4, r30, 40
	RSUBI     r3, r4, 1
	MULI      r5, r3, 12
	ADDI      r3, r1, 240
	ADD      r6, r3, r5
	LWI       r5, r30, 36
	MULI      r7, r5, 12
	LW         r7, r3, r7
	LWI        r21, r30, 0
	FPRSUB     r7, r21, r7
	LWI        r23, r30, 24
	FPMUL      r22, r7, r23
	LWI        r6, r6, 4
	LWI        r7, r30, 4
	FPRSUB     r6, r7, r6
	LWI        r12, r30, 28
	FPMUL      r20, r6, r12
	ADDI      r6, r0, 1
	FPGT   r24, r22, r20
	bneid     r24, ($BB61_71)
	NOP    
# BB#70:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r6, r0, 0
$BB61_71:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_96)
	NOP    
# BB#72:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	MULI      r4, r4, 12
	ADD      r4, r3, r4
	LWI        r4, r4, 4
	FPRSUB     r4, r7, r4
	FPMUL      r12, r4, r12
	RSUBI     r4, r5, 1
	MULI      r4, r4, 12
	LW         r3, r3, r4
	FPRSUB     r3, r21, r3
	FPMUL      r21, r3, r23
	FPGT   r4, r12, r21
	bneid     r4, ($BB61_74)
	ADDI      r3, r0, 1
# BB#73:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r3, r0, 0
$BB61_74:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_96)
	NOP    
# BB#75:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	FPGT   r4, r12, r22
	ADDI      r23, r0, 0
	ADDI      r5, r0, 1
	bneid     r4, ($BB61_77)
	ADD      r3, r5, r0
# BB#76:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r23, r0
$BB61_77:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r3, ($BB61_79)
	NOP    
# BB#78:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r12, r22, r0
$BB61_79:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI       r4, r30, 44
	RSUBI     r3, r4, 1
	MULI      r3, r3, 12
	ADDI      r22, r1, 240
	ADD      r3, r22, r3
	FPLT   r6, r20, r21
	bneid     r6, ($BB61_81)
	ADD      r25, r5, r0
# BB#80:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r25, r23, r0
$BB61_81:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	LWI        r3, r3, 8
	LWI        r7, r30, 8
	FPRSUB     r3, r7, r3
	LWI        r24, r30, 32
	FPMUL      r3, r3, r24
	FPGT   r6, r12, r3
	bneid     r6, ($BB61_83)
	NOP    
# BB#82:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r5, r23, r0
$BB61_83:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r25, ($BB61_85)
	NOP    
# BB#84:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r20, r21, r0
$BB61_85:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r5, ($BB61_96)
	NOP    
# BB#86:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	MULI      r4, r4, 12
	ADD      r4, r22, r4
	LWI        r4, r4, 8
	FPRSUB     r4, r7, r4
	FPMUL      r21, r4, r24
	FPGT   r5, r21, r20
	bneid     r5, ($BB61_88)
	ADDI      r4, r0, 1
# BB#87:                                # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r4, r0, 0
$BB61_88:                               # %if.end.i.i7
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_96)
	NOP    
# BB#89:                                # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	FPLT   r7, r3, r20
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB61_91)
	ADD      r6, r4, r0
# BB#90:                                # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r6, r5, r0
$BB61_91:                               # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r6, ($BB61_93)
	NOP    
# BB#92:                                # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r3, r20, r0
$BB61_93:                               # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	ORI       r6, r0, 0
	FPGE   r7, r3, r6
	FPUN   r3, r3, r6
	BITOR        r3, r3, r7
	bneid     r3, ($BB61_95)
	NOP    
# BB#94:                                # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	ADD      r4, r5, r0
$BB61_95:                               # %if.end72.i.i
                                        #   in Loop: Header=BB61_1 Depth=1
	bneid     r4, ($BB61_98)
	NOP    
$BB61_96:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit
                                        #   in Loop: Header=BB61_1 Depth=1
	ADDI      r3, r0, 1
	CMP       r3, r3, r9
	bneid     r3, ($BB61_134)
	LWI       r23, r1, 288
# BB#97:                                # %if.then21
                                        #   in Loop: Header=BB61_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r3, r4, r3
	SWI       r26, r3, -4
	brid      ($BB61_134)
	ADD      r23, r8, r0
$BB61_109:                              # %for.body.i
                                        #   Parent Loop BB61_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	MULI      r3, r10, 11
	ADD      r8, r26, r3
	LOAD      r25, r8, 0
	LOAD      r26, r8, 1
	LOAD      r28, r8, 2
	ADDI      r4, r8, 3
	LOAD      r3, r4, 0
	LOAD      r5, r4, 1
	LOAD      r4, r4, 2
	ADDI      r6, r8, 6
	LOAD      r7, r6, 0
	LOAD      r9, r6, 1
	LOAD      r6, r6, 2
	FPRSUB     r22, r28, r6
	FPRSUB     r27, r26, r9
	LWI        r11, r30, 20
	FPMUL      r6, r11, r27
	LWI        r12, r30, 16
	FPMUL      r9, r12, r22
	FPRSUB     r9, r6, r9
	SWI       r9, r1, 272
	FPRSUB     r29, r25, r7
	LWI        r20, r30, 12
	FPMUL      r6, r20, r22
	FPMUL      r7, r11, r29
	FPRSUB     r23, r6, r7
	FPRSUB     r3, r25, r3
	FPRSUB     r31, r26, r5
	FPMUL      r5, r23, r31
	FPMUL      r6, r9, r3
	FPADD      r6, r6, r5
	FPMUL      r5, r12, r29
	FPMUL      r7, r20, r27
	FPRSUB     r24, r5, r7
	FPRSUB     r5, r28, r4
	FPMUL      r4, r24, r5
	FPADD      r6, r6, r4
	ORI       r4, r0, 0
	FPGE   r7, r6, r4
	FPUN   r4, r6, r4
	BITOR        r4, r4, r7
	bneid     r4, ($BB61_111)
	ADDI      r7, r0, 1
# BB#110:                               # %for.body.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r7, r0, 0
$BB61_111:                              # %for.body.i
                                        #   in Loop: Header=BB61_109 Depth=2
	LWI        r4, r30, 8
	LWI        r9, r30, 0
	LWI        r21, r30, 4
	bneid     r7, ($BB61_113)
	ADD      r30, r6, r0
# BB#112:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	FPNEG      r30, r6
$BB61_113:                              # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ORI       r7, r0, 953267991
	FPLT   r30, r30, r7
	bneid     r30, ($BB61_115)
	ADDI      r7, r0, 1
# BB#114:                               # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r7, r0, 0
$BB61_115:                              # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r7, ($BB61_132)
	NOP    
# BB#116:                               # %if.end.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	FPRSUB     r28, r28, r4
	FPRSUB     r4, r25, r9
	FPRSUB     r9, r26, r21
	FPMUL      r7, r9, r3
	FPMUL      r21, r4, r31
	FPMUL      r25, r28, r3
	FPMUL      r26, r4, r5
	FPMUL      r30, r28, r31
	FPMUL      r5, r9, r5
	FPRSUB     r3, r7, r21
	FPRSUB     r25, r26, r25
	FPRSUB     r5, r30, r5
	FPMUL      r7, r25, r27
	FPMUL      r21, r5, r29
	FPADD      r7, r21, r7
	FPMUL      r21, r3, r22
	FPADD      r7, r7, r21
	ORI       r21, r0, 1065353216
	FPDIV      r26, r21, r6
	FPMUL      r22, r7, r26
	ORI       r6, r0, 0
	FPLT   r7, r22, r6
	bneid     r7, ($BB61_118)
	ADDI      r6, r0, 1
# BB#117:                               # %if.end.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r6, r0, 0
$BB61_118:                              # %if.end.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r6, ($BB61_132)
	NOP    
# BB#119:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	FPMUL      r6, r23, r9
	LWI       r7, r1, 272
	FPMUL      r4, r7, r4
	FPADD      r4, r4, r6
	FPMUL      r6, r24, r28
	FPADD      r4, r4, r6
	FPMUL      r21, r4, r26
	ORI       r4, r0, 0
	FPLT   r6, r21, r4
	FPUN   r4, r21, r4
	BITOR        r6, r4, r6
	bneid     r6, ($BB61_121)
	ADDI      r4, r0, 1
# BB#120:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r4, r0, 0
$BB61_121:                              # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r4, ($BB61_132)
	NOP    
# BB#122:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	FPMUL      r4, r25, r12
	FPMUL      r5, r5, r20
	FPADD      r4, r5, r4
	FPMUL      r3, r3, r11
	FPADD      r3, r4, r3
	FPMUL      r3, r3, r26
	ORI       r4, r0, 0
	FPLT   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB61_124)
	ADDI      r4, r0, 1
# BB#123:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r4, r0, 0
$BB61_124:                              # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r4, ($BB61_132)
	NOP    
# BB#125:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	FPADD      r3, r21, r3
	ORI       r4, r0, 1065353216
	FPGT   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB61_127)
	ADDI      r3, r0, 1
# BB#126:                               # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r3, r0, 0
$BB61_127:                              # %if.end9.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r3, ($BB61_132)
	NOP    
# BB#128:                               # %if.then19.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	LWI       r3, r1, 304
	LWI        r3, r3, 0
	FPGE   r4, r22, r3
	FPUN   r3, r22, r3
	BITOR        r4, r3, r4
	bneid     r4, ($BB61_130)
	ADDI      r3, r0, 1
# BB#129:                               # %if.then19.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r3, r0, 0
$BB61_130:                              # %if.then19.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	bneid     r3, ($BB61_132)
	NOP    
# BB#131:                               # %_ZNK10syTriangle6ID_MATEv.exit.i.i
                                        #   in Loop: Header=BB61_109 Depth=2
	LWI       r4, r1, 304
	SWI        r22, r4, 0
	ADDI      r3, r8, 10
	LOAD      r3, r3, 0
	SWI       r3, r4, 4
	SWI       r8, r4, 8
$BB61_132:                              # %_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord.exit.i
                                        #   in Loop: Header=BB61_109 Depth=2
	ADDI      r10, r10, 1
	LWI       r3, r1, 280
	CMP       r3, r3, r10
	LWI       r30, r1, 284
	bneid     r3, ($BB61_109)
	LWI       r26, r1, 276
# BB#133:                               #   in Loop: Header=BB61_1 Depth=1
	LWI       r23, r1, 288
$BB61_134:                              # %while.cond.backedge
                                        #   in Loop: Header=BB61_1 Depth=1
	bgtid     r23, ($BB61_1)
	NOP    
# BB#135:                               # %while.end
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 292
	.end	_ZNK5Scene8TraceRayERK3RayR9HitRecord
$tmp61:
	.size	_ZNK5Scene8TraceRayERK3RayR9HitRecord, ($tmp61)-_ZNK5Scene8TraceRayERK3RayR9HitRecord

	.globl	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
	.align	2
	.type	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist,@function
	.ent	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist # @_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
_ZNK5Scene14TraceShadowRayERK3RayR7HitDist:
	.frame	r1,288,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -288
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r7, r1, 300
	ADD      r29, r6, r0
	SWI       r29, r1, 280
	SWI       r5, r1, 292
	ADDI      r22, r0, 1
	brid      ($BB62_1)
	SWI       r0, r1, 48
$BB62_98:                               # %if.else24
                                        #   in Loop: Header=BB62_1 Depth=1
	beqid     r9, ($BB62_99)
	NOP    
# BB#100:                               # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGT   r6, r21, r12
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r6, ($BB62_102)
	ADD      r5, r3, r0
# BB#101:                               # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r5, r4, r0
$BB62_102:                              # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r5, ($BB62_104)
	NOP    
# BB#103:                               # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r21, r12, r0
$BB62_104:                              # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGE   r5, r10, r21
	FPUN   r6, r10, r21
	BITOR        r5, r6, r5
	bneid     r5, ($BB62_106)
	NOP    
# BB#105:                               # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r4, r0
$BB62_106:                              # %if.else31
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_108)
	NOP    
# BB#107:                               # %if.then35
                                        #   in Loop: Header=BB62_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r5, r4, r3
	SWI       r11, r5, -4
	SW        r26, r4, r3
	brid      ($BB62_134)
	ADDI      r22, r8, 1
$BB62_99:                               # %if.then28
                                        #   in Loop: Header=BB62_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r3, r4, r3
	SWI       r11, r3, -4
	brid      ($BB62_134)
	ADD      r22, r8, r0
$BB62_108:                              # %if.else40
                                        #   in Loop: Header=BB62_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r5, r4, r3
	SWI       r26, r5, -4
	SW        r11, r4, r3
	brid      ($BB62_134)
	ADDI      r22, r8, 1
$BB62_1:                                # %while.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB62_109 Depth 2
	ADD      r8, r22, r0
	ADDI      r3, r0, -1
	ADD      r22, r8, r3
	bslli     r3, r22, 2
	ADDI      r4, r1, 48
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 292
	LWI       r4, r4, 36
	ADD      r3, r3, r4
	SWI       r0, r1, 192
	SWI       r0, r1, 188
	SWI       r0, r1, 184
	SWI       r0, r1, 180
	SWI       r0, r1, 176
	LOAD      r4, r3, 0
	SWI        r4, r1, 176
	LOAD      r4, r3, 1
	SWI        r4, r1, 180
	LOAD      r4, r3, 2
	SWI        r4, r1, 184
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 188
	LOAD      r5, r4, 1
	SWI        r5, r1, 192
	LOAD      r4, r4, 2
	SWI        r4, r1, 196
	ADDI      r4, r3, 7
	ADDI      r3, r3, 6
	LOAD      r25, r3, 0
	SWI       r25, r1, 200
	LOAD      r24, r4, 0
	SWI       r24, r1, 204
	LWI       r4, r29, 40
	RSUBI     r3, r4, 1
	MULI      r5, r3, 12
	ADDI      r3, r1, 176
	ADD      r7, r3, r5
	LWI       r5, r29, 36
	MULI      r6, r5, 12
	LW         r6, r3, r6
	LWI        r12, r29, 0
	FPRSUB     r6, r12, r6
	LWI        r21, r29, 24
	FPMUL      r20, r6, r21
	LWI        r6, r7, 4
	LWI        r9, r29, 4
	FPRSUB     r6, r9, r6
	LWI        r10, r29, 28
	FPMUL      r11, r6, r10
	ADDI      r7, r0, 1
	FPGT   r6, r20, r11
	bneid     r6, ($BB62_3)
	NOP    
# BB#2:                                 # %while.body
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r7, r0, 0
$BB62_3:                                # %while.body
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r7, ($BB62_134)
	NOP    
# BB#4:                                 # %while.body
                                        #   in Loop: Header=BB62_1 Depth=1
	MULI      r4, r4, 12
	ADD      r4, r3, r4
	LWI        r4, r4, 4
	FPRSUB     r4, r9, r4
	FPMUL      r10, r4, r10
	RSUBI     r4, r5, 1
	MULI      r4, r4, 12
	LW         r3, r3, r4
	FPRSUB     r3, r12, r3
	FPMUL      r12, r3, r21
	FPGT   r4, r10, r12
	bneid     r4, ($BB62_6)
	ADDI      r3, r0, 1
# BB#5:                                 # %while.body
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_6:                                # %while.body
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_134)
	NOP    
# BB#7:                                 # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r7, r22, r0
	FPGT   r4, r10, r20
	ADDI      r22, r0, 0
	ADDI      r21, r0, 1
	bneid     r4, ($BB62_9)
	ADD      r3, r21, r0
# BB#8:                                 # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r22, r0
$BB62_9:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_11)
	NOP    
# BB#10:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r10, r20, r0
$BB62_11:                               # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r5, r29, 44
	RSUBI     r3, r5, 1
	MULI      r3, r3, 12
	ADDI      r4, r1, 176
	ADD      r3, r4, r3
	FPLT   r6, r11, r12
	bneid     r6, ($BB62_13)
	ADD      r23, r21, r0
# BB#12:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r23, r22, r0
$BB62_13:                               # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI        r3, r3, 8
	LWI        r9, r29, 8
	FPRSUB     r3, r9, r3
	LWI        r20, r29, 32
	FPMUL      r3, r3, r20
	FPGT   r6, r10, r3
	bneid     r6, ($BB62_15)
	NOP    
# BB#14:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r21, r22, r0
$BB62_15:                               # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r23, ($BB62_17)
	NOP    
# BB#16:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r11, r12, r0
$BB62_17:                               # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r21, ($BB62_134)
	ADD      r22, r7, r0
# BB#18:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r26, r24, r0
	MULI      r5, r5, 12
	ADD      r4, r4, r5
	LWI        r4, r4, 8
	FPRSUB     r4, r9, r4
	FPMUL      r12, r4, r20
	FPGT   r5, r12, r11
	bneid     r5, ($BB62_20)
	ADDI      r4, r0, 1
# BB#19:                                # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r4, r0, 0
$BB62_20:                               # %if.end.i.i133
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r4, ($BB62_134)
	SWI       r26, r1, 272
# BB#21:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	FPLT   r7, r3, r11
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB62_23)
	ADD      r6, r4, r0
# BB#22:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r6, r5, r0
$BB62_23:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r6, ($BB62_25)
	NOP    
# BB#24:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r11, r0
$BB62_25:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	ORI       r6, r0, 0
	FPLT   r3, r3, r6
	bneid     r3, ($BB62_27)
	NOP    
# BB#26:                                # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r4, r5, r0
$BB62_27:                               # %if.end72.i.i137
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r4, ($BB62_134)
	NOP    
# BB#28:                                # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGT   r6, r12, r10
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r6, ($BB62_30)
	ADD      r5, r3, r0
# BB#29:                                # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r5, r4, r0
$BB62_30:                               # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r5, ($BB62_32)
	NOP    
# BB#31:                                # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r12, r10, r0
$BB62_32:                               # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r5, r1, 300
	LWI        r5, r5, 0
	FPLT   r5, r5, r12
	bneid     r5, ($BB62_34)
	NOP    
# BB#33:                                # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r4, r0
$BB62_34:                               # %if.end
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r4, r25, r0
	bneid     r3, ($BB62_134)
	SWI       r4, r1, 276
# BB#35:                                # %if.end7
                                        #   in Loop: Header=BB62_1 Depth=1
	beqid     r4, ($BB62_134)
	NOP    
# BB#36:                                # %if.end7
                                        #   in Loop: Header=BB62_1 Depth=1
	SWI       r22, r1, 284
	ADD      r10, r0, r0
	ADDI      r3, r0, -1
	CMP       r3, r3, r4
	bneid     r3, ($BB62_109)
	NOP    
# BB#37:                                # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r3, r1, 292
	LWI       r3, r3, 36
	bslli     r4, r26, 3
	ADD      r3, r4, r3
	SWI       r0, r1, 224
	SWI       r0, r1, 220
	SWI       r0, r1, 216
	SWI       r0, r1, 212
	SWI       r0, r1, 208
	LOAD      r4, r3, 0
	SWI        r4, r1, 208
	LOAD      r4, r3, 1
	SWI        r4, r1, 212
	LOAD      r4, r3, 2
	SWI        r4, r1, 216
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 220
	LOAD      r5, r4, 1
	SWI        r5, r1, 224
	LOAD      r4, r4, 2
	SWI        r4, r1, 228
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 232
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 236
	LWI       r3, r29, 40
	RSUBI     r4, r3, 1
	MULI      r5, r4, 12
	ADDI      r4, r1, 208
	ADD      r7, r4, r5
	LWI       r11, r29, 36
	MULI      r5, r11, 12
	LW         r6, r4, r5
	LWI        r5, r29, 0
	FPRSUB     r6, r5, r6
	LWI        r20, r29, 24
	FPMUL      r21, r6, r20
	LWI        r6, r7, 4
	LWI        r7, r29, 4
	FPRSUB     r6, r7, r6
	LWI        r22, r29, 28
	FPMUL      r12, r6, r22
	ADDI      r23, r0, 1
	FPGT   r6, r21, r12
	bneid     r6, ($BB62_39)
	NOP    
# BB#38:                                # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r23, r0, 0
$BB62_39:                               # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r23, ($BB62_69)
	NOP    
# BB#40:                                # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	RSUBI     r6, r11, 1
	MULI      r6, r6, 12
	LW         r6, r4, r6
	MULI      r3, r3, 12
	ADD      r3, r4, r3
	LWI        r3, r3, 4
	FPRSUB     r3, r7, r3
	FPMUL      r11, r3, r22
	FPRSUB     r3, r5, r6
	FPMUL      r20, r3, r20
	FPGT   r4, r11, r20
	bneid     r4, ($BB62_42)
	ADDI      r3, r0, 1
# BB#41:                                # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_42:                               # %if.then9
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_69)
	NOP    
# BB#43:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGT   r5, r11, r21
	ADDI      r9, r0, 0
	ADDI      r3, r0, 1
	bneid     r5, ($BB62_45)
	ADD      r4, r3, r0
# BB#44:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r4, r9, r0
$BB62_45:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r4, ($BB62_47)
	NOP    
# BB#46:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r11, r21, r0
$BB62_47:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r22, r29, 44
	RSUBI     r4, r22, 1
	MULI      r4, r4, 12
	ADDI      r5, r1, 208
	ADD      r4, r5, r4
	FPLT   r6, r12, r20
	bneid     r6, ($BB62_49)
	ADD      r10, r3, r0
# BB#48:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r10, r9, r0
$BB62_49:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI        r6, r4, 8
	LWI        r4, r29, 8
	FPRSUB     r6, r4, r6
	LWI        r7, r29, 32
	FPMUL      r21, r6, r7
	FPGT   r6, r11, r21
	bneid     r6, ($BB62_51)
	NOP    
# BB#50:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r9, r0
$BB62_51:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r10, ($BB62_53)
	NOP    
# BB#52:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r12, r20, r0
$BB62_53:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r3, ($BB62_69)
	NOP    
# BB#54:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	MULI      r3, r22, 12
	ADD      r3, r5, r3
	LWI        r3, r3, 8
	FPRSUB     r3, r4, r3
	FPMUL      r20, r3, r7
	FPGT   r4, r20, r12
	bneid     r4, ($BB62_56)
	ADDI      r3, r0, 1
# BB#55:                                # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_56:                               # %if.end.i.i59
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_69)
	NOP    
# BB#57:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	FPLT   r6, r21, r12
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r6, ($BB62_59)
	ADD      r5, r3, r0
# BB#58:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r5, r4, r0
$BB62_59:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r5, ($BB62_61)
	NOP    
# BB#60:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r21, r12, r0
$BB62_61:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	ORI       r5, r0, 0
	FPLT   r5, r21, r5
	bneid     r5, ($BB62_63)
	NOP    
# BB#62:                                # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r4, r0
$BB62_63:                               # %if.end72.i.i63
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r9, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r3, ($BB62_69)
	NOP    
# BB#64:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGT   r4, r20, r11
	ADDI      r9, r0, 1
	bneid     r4, ($BB62_66)
	ADD      r3, r9, r0
# BB#65:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_66:                               # %if.end81.i.i67
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_68)
	NOP    
# BB#67:                                # %if.end81.i.i67
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r20, r11, r0
$BB62_68:                               # %if.end81.i.i67
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r10, r20, r0
$BB62_69:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r3, r1, 292
	LWI       r3, r3, 36
	ADDI      r11, r26, 1
	bslli     r4, r11, 3
	ADD      r3, r4, r3
	SWI       r0, r1, 256
	SWI       r0, r1, 252
	SWI       r0, r1, 248
	SWI       r0, r1, 244
	SWI       r0, r1, 240
	LOAD      r4, r3, 0
	SWI        r4, r1, 240
	LOAD      r4, r3, 1
	SWI        r4, r1, 244
	LOAD      r4, r3, 2
	SWI        r4, r1, 248
	ADDI      r4, r3, 3
	LOAD      r5, r4, 0
	SWI        r5, r1, 252
	LOAD      r5, r4, 1
	SWI        r5, r1, 256
	LOAD      r4, r4, 2
	SWI        r4, r1, 260
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 264
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 268
	LWI       r4, r29, 40
	RSUBI     r3, r4, 1
	MULI      r5, r3, 12
	ADDI      r3, r1, 240
	ADD      r7, r3, r5
	LWI       r5, r29, 36
	MULI      r6, r5, 12
	LW         r6, r3, r6
	LWI        r21, r29, 0
	FPRSUB     r6, r21, r6
	LWI        r23, r29, 24
	FPMUL      r22, r6, r23
	LWI        r6, r7, 4
	LWI        r7, r29, 4
	FPRSUB     r6, r7, r6
	LWI        r12, r29, 28
	FPMUL      r20, r6, r12
	ADDI      r24, r0, 1
	FPGT   r6, r22, r20
	bneid     r6, ($BB62_71)
	NOP    
# BB#70:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r24, r0, 0
$BB62_71:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r24, ($BB62_96)
	NOP    
# BB#72:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	MULI      r4, r4, 12
	ADD      r4, r3, r4
	LWI        r4, r4, 4
	FPRSUB     r4, r7, r4
	FPMUL      r12, r4, r12
	RSUBI     r4, r5, 1
	MULI      r4, r4, 12
	LW         r3, r3, r4
	FPRSUB     r3, r21, r3
	FPMUL      r21, r3, r23
	FPGT   r4, r12, r21
	bneid     r4, ($BB62_74)
	ADDI      r3, r0, 1
# BB#73:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_74:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_96)
	NOP    
# BB#75:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	FPGT   r5, r12, r22
	ADDI      r23, r0, 0
	ADDI      r3, r0, 1
	bneid     r5, ($BB62_77)
	ADD      r4, r3, r0
# BB#76:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r4, r23, r0
$BB62_77:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r4, ($BB62_79)
	NOP    
# BB#78:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r12, r22, r0
$BB62_79:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI       r5, r29, 44
	RSUBI     r4, r5, 1
	MULI      r6, r4, 12
	ADDI      r4, r1, 240
	ADD      r7, r4, r6
	FPLT   r6, r20, r21
	bneid     r6, ($BB62_81)
	ADD      r25, r3, r0
# BB#80:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r25, r23, r0
$BB62_81:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	LWI        r6, r7, 8
	LWI        r7, r29, 8
	FPRSUB     r6, r7, r6
	LWI        r24, r29, 32
	FPMUL      r22, r6, r24
	FPGT   r6, r12, r22
	bneid     r6, ($BB62_83)
	NOP    
# BB#82:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r23, r0
$BB62_83:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r25, ($BB62_85)
	NOP    
# BB#84:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r20, r21, r0
$BB62_85:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_96)
	NOP    
# BB#86:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	MULI      r3, r5, 12
	ADD      r3, r4, r3
	LWI        r3, r3, 8
	FPRSUB     r3, r7, r3
	FPMUL      r21, r3, r24
	FPGT   r4, r21, r20
	bneid     r4, ($BB62_88)
	ADDI      r3, r0, 1
# BB#87:                                # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 0
$BB62_88:                               # %if.end.i.i7
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_96)
	NOP    
# BB#89:                                # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	FPLT   r6, r22, r20
	ADDI      r4, r0, 0
	ADDI      r3, r0, 1
	bneid     r6, ($BB62_91)
	ADD      r5, r3, r0
# BB#90:                                # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r5, r4, r0
$BB62_91:                               # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r5, ($BB62_93)
	NOP    
# BB#92:                                # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r22, r20, r0
$BB62_93:                               # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	ORI       r5, r0, 0
	FPGE   r6, r22, r5
	FPUN   r5, r22, r5
	BITOR        r5, r5, r6
	bneid     r5, ($BB62_95)
	NOP    
# BB#94:                                # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	ADD      r3, r4, r0
$BB62_95:                               # %if.end72.i.i
                                        #   in Loop: Header=BB62_1 Depth=1
	bneid     r3, ($BB62_98)
	NOP    
$BB62_96:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit
                                        #   in Loop: Header=BB62_1 Depth=1
	ADDI      r3, r0, 1
	CMP       r3, r3, r9
	bneid     r3, ($BB62_134)
	LWI       r22, r1, 284
# BB#97:                                # %if.then21
                                        #   in Loop: Header=BB62_1 Depth=1
	bslli     r3, r8, 2
	ADDI      r4, r1, 48
	ADD      r3, r4, r3
	SWI       r26, r3, -4
	brid      ($BB62_134)
	ADD      r22, r8, r0
$BB62_109:                              # %for.body.i
                                        #   Parent Loop BB62_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	MULI      r3, r10, 11
	ADD      r3, r26, r3
	LOAD      r24, r3, 0
	LOAD      r25, r3, 1
	LOAD      r27, r3, 2
	ADDI      r6, r3, 6
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	LOAD      r5, r3, 1
	LOAD      r3, r3, 2
	LOAD      r7, r6, 0
	LOAD      r8, r6, 1
	LOAD      r6, r6, 2
	FPRSUB     r21, r27, r6
	FPRSUB     r26, r25, r8
	LWI        r8, r29, 20
	FPMUL      r6, r8, r26
	LWI        r11, r29, 16
	FPMUL      r9, r11, r21
	FPRSUB     r6, r6, r9
	FPRSUB     r28, r24, r7
	LWI        r12, r29, 12
	FPMUL      r7, r12, r21
	FPMUL      r9, r8, r28
	FPRSUB     r22, r7, r9
	FPRSUB     r31, r24, r4
	FPRSUB     r30, r25, r5
	FPMUL      r4, r22, r30
	FPMUL      r5, r6, r31
	FPADD      r4, r5, r4
	FPMUL      r5, r11, r28
	FPMUL      r7, r12, r26
	FPRSUB     r23, r5, r7
	FPRSUB     r3, r27, r3
	FPMUL      r5, r23, r3
	FPADD      r20, r4, r5
	ORI       r4, r0, 0
	FPGE   r5, r20, r4
	FPUN   r4, r20, r4
	BITOR        r4, r4, r5
	bneid     r4, ($BB62_111)
	ADDI      r7, r0, 1
# BB#110:                               # %for.body.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r7, r0, 0
$BB62_111:                              # %for.body.i
                                        #   in Loop: Header=BB62_109 Depth=2
	LWI        r4, r29, 8
	LWI        r5, r29, 0
	LWI        r9, r29, 4
	bneid     r7, ($BB62_113)
	ADD      r29, r20, r0
# BB#112:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	FPNEG      r29, r20
$BB62_113:                              # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ORI       r7, r0, 953267991
	FPLT   r29, r29, r7
	bneid     r29, ($BB62_115)
	ADDI      r7, r0, 1
# BB#114:                               # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r7, r0, 0
$BB62_115:                              # %_ZN4util4fabsERKf.exit.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r7, ($BB62_132)
	NOP    
# BB#116:                               # %if.end.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	FPRSUB     r27, r27, r4
	FPRSUB     r5, r24, r5
	FPRSUB     r4, r25, r9
	FPMUL      r7, r4, r31
	FPMUL      r9, r5, r30
	FPMUL      r25, r27, r31
	FPMUL      r29, r5, r3
	FPMUL      r30, r27, r30
	FPMUL      r3, r4, r3
	FPRSUB     r24, r7, r9
	FPRSUB     r25, r29, r25
	FPRSUB     r3, r30, r3
	FPMUL      r7, r25, r26
	FPMUL      r9, r3, r28
	FPADD      r7, r9, r7
	FPMUL      r9, r24, r21
	FPADD      r7, r7, r9
	ORI       r9, r0, 1065353216
	FPDIV      r26, r9, r20
	FPMUL      r21, r7, r26
	ORI       r7, r0, 0
	FPLT   r9, r21, r7
	bneid     r9, ($BB62_118)
	ADDI      r7, r0, 1
# BB#117:                               # %if.end.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r7, r0, 0
$BB62_118:                              # %if.end.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r7, ($BB62_132)
	NOP    
# BB#119:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	FPMUL      r4, r22, r4
	FPMUL      r5, r6, r5
	FPADD      r4, r5, r4
	FPMUL      r5, r23, r27
	FPADD      r4, r4, r5
	FPMUL      r20, r4, r26
	ORI       r4, r0, 0
	FPLT   r5, r20, r4
	FPUN   r4, r20, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB62_121)
	ADDI      r4, r0, 1
# BB#120:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r4, r0, 0
$BB62_121:                              # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r4, ($BB62_132)
	NOP    
# BB#122:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	FPMUL      r4, r25, r11
	FPMUL      r3, r3, r12
	FPADD      r3, r3, r4
	FPMUL      r4, r24, r8
	FPADD      r3, r3, r4
	FPMUL      r3, r3, r26
	ORI       r4, r0, 0
	FPLT   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB62_124)
	ADDI      r4, r0, 1
# BB#123:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r4, r0, 0
$BB62_124:                              # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r4, ($BB62_132)
	NOP    
# BB#125:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	FPADD      r3, r20, r3
	ORI       r4, r0, 1065353216
	FPGT   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB62_127)
	ADDI      r3, r0, 1
# BB#126:                               # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r3, r0, 0
$BB62_127:                              # %if.end9.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r3, ($BB62_132)
	NOP    
# BB#128:                               # %if.then19.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	LWI       r3, r1, 300
	LWI        r3, r3, 0
	FPGE   r4, r21, r3
	FPUN   r3, r21, r3
	BITOR        r4, r3, r4
	bneid     r4, ($BB62_130)
	ADDI      r3, r0, 1
# BB#129:                               # %if.then19.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r3, r0, 0
$BB62_130:                              # %if.then19.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	bneid     r3, ($BB62_132)
	NOP    
# BB#131:                               # %if.then21.i.i
                                        #   in Loop: Header=BB62_109 Depth=2
	LWI       r3, r1, 300
	SWI        r21, r3, 0
$BB62_132:                              # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB62_109 Depth=2
	ADDI      r10, r10, 1
	LWI       r3, r1, 276
	CMP       r3, r3, r10
	LWI       r29, r1, 280
	bneid     r3, ($BB62_109)
	LWI       r26, r1, 272
# BB#133:                               #   in Loop: Header=BB62_1 Depth=1
	LWI       r22, r1, 284
$BB62_134:                              # %while.cond.backedge
                                        #   in Loop: Header=BB62_1 Depth=1
	bgtid     r22, ($BB62_1)
	NOP    
# BB#135:                               # %while.end
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 288
	.end	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist
$tmp62:
	.size	_ZNK5Scene14TraceShadowRayERK3RayR7HitDist, ($tmp62)-_ZNK5Scene14TraceShadowRayERK3RayR7HitDist

	.globl	_ZNK5Scene7HemiRayERK8syVectorS2_
	.align	2
	.type	_ZNK5Scene7HemiRayERK8syVectorS2_,@function
	.ent	_ZNK5Scene7HemiRayERK8syVectorS2_ # @_ZNK5Scene7HemiRayERK8syVectorS2_
_ZNK5Scene7HemiRayERK8syVectorS2_:
	.frame	r1,16,r15
	.mask	0xf00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -16
	SWI       r20, r1, 12
	SWI       r21, r1, 8
	SWI       r22, r1, 4
	SWI       r23, r1, 0
$BB63_1:                                # %do.body
                                        # =>This Inner Loop Header: Depth=1
	RAND      r4
	RAND      r3
	FPADD      r3, r3, r3
	ORI       r6, r0, -1082130432
	FPADD      r3, r3, r6
	FPADD      r4, r4, r4
	FPADD      r4, r4, r6
	FPMUL      r6, r4, r4
	FPMUL      r9, r3, r3
	FPADD      r10, r6, r9
	ORI       r11, r0, 1065353216
	FPGE   r11, r10, r11
	bneid     r11, ($BB63_3)
	ADDI      r10, r0, 1
# BB#2:                                 # %do.body
                                        #   in Loop: Header=BB63_1 Depth=1
	ADDI      r10, r0, 0
$BB63_3:                                # %do.body
                                        #   in Loop: Header=BB63_1 Depth=1
	bneid     r10, ($BB63_1)
	NOP    
# BB#4:                                 # %do.end
	ORI       r10, r0, 1065353216
	FPRSUB     r6, r6, r10
	FPRSUB     r6, r9, r6
	FPINVSQRT r11, r6
	LWI        r6, r8, 0
	ORI       r9, r0, 0
	FPGE   r12, r6, r9
	FPUN   r9, r6, r9
	BITOR        r12, r9, r12
	bneid     r12, ($BB63_6)
	ADDI      r9, r0, 1
# BB#5:                                 # %do.end
	ADDI      r9, r0, 0
$BB63_6:                                # %do.end
	bneid     r9, ($BB63_8)
	ADD      r12, r6, r0
# BB#7:                                 # %if.then.i59.i
	FPNEG      r12, r6
$BB63_8:                                # %_ZN4util4fabsERKf.exit61.i
	LWI        r9, r8, 4
	ORI       r20, r0, 0
	FPGE   r21, r9, r20
	FPUN   r20, r9, r20
	BITOR        r20, r20, r21
	bneid     r20, ($BB63_10)
	ADDI      r21, r0, 1
# BB#9:                                 # %_ZN4util4fabsERKf.exit61.i
	ADDI      r21, r0, 0
$BB63_10:                               # %_ZN4util4fabsERKf.exit61.i
	bneid     r21, ($BB63_12)
	ADD      r20, r9, r0
# BB#11:                                # %if.then.i54.i
	FPNEG      r20, r9
$BB63_12:                               # %_ZN4util4fabsERKf.exit56.i
	LWI        r8, r8, 8
	ORI       r21, r0, 0
	FPGE   r22, r8, r21
	FPUN   r21, r8, r21
	BITOR        r21, r21, r22
	bneid     r21, ($BB63_14)
	ADDI      r22, r0, 1
# BB#13:                                # %_ZN4util4fabsERKf.exit56.i
	ADDI      r22, r0, 0
$BB63_14:                               # %_ZN4util4fabsERKf.exit56.i
	bneid     r22, ($BB63_16)
	ADD      r21, r8, r0
# BB#15:                                # %if.then.i.i
	FPNEG      r21, r8
$BB63_16:                               # %_ZN4util4fabsERKf.exit.i
	FPGE   r22, r12, r20
	FPUN   r23, r12, r20
	BITOR        r22, r23, r22
	bneid     r22, ($BB63_18)
	ADDI      r23, r0, 1
# BB#17:                                # %_ZN4util4fabsERKf.exit.i
	ADDI      r23, r0, 0
$BB63_18:                               # %_ZN4util4fabsERKf.exit.i
	FPDIV      r10, r10, r11
	ORI       r22, r0, 1065353216
	ORI       r11, r0, 0
	bneid     r23, ($BB63_22)
	NOP    
# BB#19:                                # %_ZN4util4fabsERKf.exit.i
	FPLT   r23, r12, r21
	bneid     r23, ($BB63_21)
	ADDI      r12, r0, 1
# BB#20:                                # %_ZN4util4fabsERKf.exit.i
	ADDI      r12, r0, 0
$BB63_21:                               # %_ZN4util4fabsERKf.exit.i
	bneid     r12, ($BB63_26)
	ADD      r23, r11, r0
$BB63_22:                               # %if.else.i
	FPLT   r11, r20, r21
	bneid     r11, ($BB63_24)
	ADDI      r12, r0, 1
# BB#23:                                # %if.else.i
	ADDI      r12, r0, 0
$BB63_24:                               # %if.else.i
	ORI       r23, r0, 1065353216
	ORI       r11, r0, 0
	bneid     r12, ($BB63_26)
	ADD      r22, r11, r0
# BB#25:                                # %if.else19.i
	ORI       r23, r0, 0
	ORI       r11, r0, 1065353216
	ADD      r22, r23, r0
$BB63_26:                               # %_ZNK8syVector8GetOrthoEv.exit
	FPMUL      r12, r9, r22
	FPMUL      r20, r6, r23
	FPRSUB     r12, r12, r20
	FPMUL      r20, r8, r22
	FPMUL      r21, r6, r11
	FPRSUB     r21, r21, r20
	FPMUL      r20, r8, r21
	FPMUL      r22, r9, r12
	FPRSUB     r20, r20, r22
	FPMUL      r22, r8, r23
	FPMUL      r11, r9, r11
	FPRSUB     r22, r22, r11
	FPMUL      r11, r6, r12
	FPMUL      r23, r8, r22
	FPRSUB     r23, r11, r23
	FPMUL      r11, r20, r3
	FPMUL      r20, r22, r4
	FPADD      r11, r20, r11
	FPMUL      r20, r6, r10
	FPADD      r11, r11, r20
	FPMUL      r20, r23, r3
	FPMUL      r23, r21, r4
	FPADD      r20, r23, r20
	FPMUL      r23, r9, r10
	FPADD      r20, r20, r23
	FPMUL      r9, r9, r22
	FPMUL      r21, r6, r21
	FPMUL      r6, r20, r20
	FPMUL      r22, r11, r11
	FPADD      r6, r22, r6
	FPRSUB     r9, r9, r21
	FPMUL      r3, r9, r3
	FPMUL      r4, r12, r4
	FPADD      r3, r4, r3
	FPMUL      r4, r8, r10
	FPADD      r3, r3, r4
	FPMUL      r4, r3, r3
	FPADD      r4, r6, r4
	FPINVSQRT r4, r4
	LWI        r6, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r7, 0
	SWI        r7, r5, 0
	SWI        r8, r5, 4
	SWI        r6, r5, 8
	ORI       r9, r0, 1065353216
	FPDIV      r7, r9, r4
	FPDIV      r4, r11, r7
	FPDIV      r6, r20, r7
	FPMUL      r8, r6, r6
	FPMUL      r10, r4, r4
	FPADD      r8, r10, r8
	FPDIV      r3, r3, r7
	FPMUL      r7, r3, r3
	FPADD      r7, r8, r7
	FPINVSQRT r7, r7
	FPDIV      r7, r9, r7
	FPDIV      r4, r4, r7
	FPDIV      r6, r6, r7
	FPDIV      r7, r3, r7
	FPDIV      r3, r9, r7
	FPDIV      r8, r9, r6
	FPDIV      r9, r9, r4
	ORI       r10, r0, 0
	FPLT   r12, r9, r10
	FPLT   r22, r8, r10
	FPLT   r20, r3, r10
	ADDI      r21, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB63_28)
	ADD      r10, r11, r0
# BB#27:                                # %_ZNK8syVector8GetOrthoEv.exit
	ADD      r10, r21, r0
$BB63_28:                               # %_ZNK8syVector8GetOrthoEv.exit
	bneid     r22, ($BB63_30)
	ADD      r20, r11, r0
# BB#29:                                # %_ZNK8syVector8GetOrthoEv.exit
	ADD      r20, r21, r0
$BB63_30:                               # %_ZNK8syVector8GetOrthoEv.exit
	bneid     r12, ($BB63_32)
	NOP    
# BB#31:                                # %_ZNK8syVector8GetOrthoEv.exit
	ADD      r11, r21, r0
$BB63_32:                               # %_ZNK8syVector8GetOrthoEv.exit
	SWI        r4, r5, 12
	SWI        r6, r5, 16
	SWI        r7, r5, 20
	SWI        r9, r5, 24
	SWI        r8, r5, 28
	SWI        r3, r5, 32
	SWI       r11, r5, 36
	SWI       r20, r5, 40
	SWI       r10, r5, 44
	LWI       r23, r1, 0
	LWI       r22, r1, 4
	LWI       r21, r1, 8
	LWI       r20, r1, 12
	rtsd      r15, 8
	ADDI      r1, r1, 16
	.end	_ZNK5Scene7HemiRayERK8syVectorS2_
$tmp63:
	.size	_ZNK5Scene7HemiRayERK8syVectorS2_, ($tmp63)-_ZNK5Scene7HemiRayERK8syVectorS2_

	.globl	_ZNK5Scene8GetColorERK3RayRKi
	.align	2
	.type	_ZNK5Scene8GetColorERK3RayRKi,@function
	.ent	_ZNK5Scene8GetColorERK3RayRKi # @_ZNK5Scene8GetColorERK3RayRKi
_ZNK5Scene8GetColorERK3RayRKi:
	.frame	r1,532,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -532
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r8, r1, 548
	SWI       r6, r1, 540
	SWI       r5, r1, 536
	SWI       r0, r5, 0
	SWI       r0, r5, 4
	SWI       r0, r5, 8
	ADD      r4, r0, r0
	ORI       r10, r0, 1065353216
	SWI       r10, r1, 456
	ORI       r3, r0, 0
	SWI       r3, r1, 484
	LWI       r5, r7, 44
	SWI       r5, r1, 420
	LWI       r5, r7, 40
	SWI       r5, r1, 424
	LWI       r5, r7, 36
	SWI       r5, r1, 428
	LWI        r25, r7, 32
	LWI        r30, r7, 28
	LWI        r26, r7, 24
	LWI        r29, r7, 20
	LWI        r28, r7, 16
	LWI        r27, r7, 12
	LWI        r5, r7, 8
	SWI       r5, r1, 376
	LWI        r5, r7, 4
	SWI       r5, r1, 372
	LWI        r5, r7, 0
	SWI       r5, r1, 380
	ADD      r5, r3, r0
	SWI       r3, r1, 488
	SWI       r10, r1, 460
	SWI       r10, r1, 464
	ADD      r8, r10, r0
	brid      ($BB64_1)
	ADD      r9, r10, r0
$BB64_346:                              # %if.end
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r3, r1, 456
	FPMUL      r8, r8, r3
	LWI       r3, r1, 460
	FPMUL      r9, r9, r3
	LWI       r3, r1, 464
	FPMUL      r10, r10, r3
	LWI       r3, r1, 480
	SWI       r3, r1, 420
	SWI       r12, r1, 424
	SWI       r11, r1, 428
	LWI       r25, r1, 512
	LWI       r30, r1, 516
	LWI       r26, r1, 520
	LWI       r29, r1, 508
	LWI       r28, r1, 504
	LWI       r27, r1, 500
	LWI       r3, r1, 392
	SWI       r3, r1, 376
	LWI       r3, r1, 384
	SWI       r3, r1, 372
	LWI       r3, r1, 388
	SWI       r3, r1, 380
$BB64_1:                                # %while.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_312 Depth 2
                                        #     Child Loop BB64_166 Depth 2
                                        #       Child Loop BB64_210 Depth 3
                                        #     Child Loop BB64_3 Depth 2
                                        #       Child Loop BB64_47 Depth 3
	SWI       r26, r1, 440
	SWI       r30, r1, 436
	SWI       r25, r1, 432
	LWI       r3, r1, 548
	LWI       r3, r3, 0
	CMP       r3, r3, r4
	bgeid     r3, ($BB64_347)
	NOP    
# BB#2:                                 # %while.body
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r12, r1, 528
	SWI       r11, r1, 524
	SWI       r10, r1, 476
	SWI       r9, r1, 472
	SWI       r8, r1, 468
	SWI       r5, r1, 496
	ADDI      r4, r4, 1
	SWI       r4, r1, 492
	ADDI      r9, r0, 1
	ORI       r3, r0, 1203982336
	SWI       r3, r1, 368
	ADDI      r4, r0, -1
	SWI       r4, r1, 404
	LWI       r3, r1, 420
	RSUBI     r3, r3, 1
	SWI       r3, r1, 444
	LWI       r3, r1, 424
	RSUBI     r3, r3, 1
	SWI       r3, r1, 448
	LWI       r3, r1, 428
	RSUBI     r3, r3, 1
	SWI       r3, r1, 452
	SWI       r0, r1, 144
	brid      ($BB64_3)
	SWI       r4, r1, 400
$BB64_132:                              # %if.else24.i
                                        #   in Loop: Header=BB64_3 Depth=2
	beqid     r5, ($BB64_133)
	NOP    
# BB#134:                               # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r10, r3, r8
	ADDI      r6, r0, 0
	ADDI      r5, r0, 1
	bneid     r10, ($BB64_136)
	ADD      r7, r5, r0
# BB#135:                               # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r7, r6, r0
$BB64_136:                              # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r7, ($BB64_138)
	NOP    
# BB#137:                               # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r3, r8, r0
$BB64_138:                              # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGE   r4, r22, r3
	FPUN   r3, r22, r3
	BITOR        r3, r3, r4
	bneid     r3, ($BB64_140)
	NOP    
# BB#139:                               # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r5, r6, r0
$BB64_140:                              # %if.else31.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r5, ($BB64_142)
	NOP    
# BB#141:                               # %if.then35.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bslli     r3, r9, 2
	ADDI      r4, r1, 144
	ADD      r5, r4, r3
	SWI       r21, r5, -4
	LWI       r5, r1, 396
	SW        r5, r4, r3
	brid      ($BB64_143)
	NOP    
$BB64_133:                              # %if.then28.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bslli     r3, r9, 2
	ADDI      r4, r1, 144
	ADD      r3, r4, r3
	brid      ($BB64_144)
	SWI       r21, r3, -4
$BB64_142:                              # %if.else40.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bslli     r3, r9, 2
	ADDI      r4, r1, 144
	ADD      r5, r4, r3
	LWI       r6, r1, 396
	SWI       r6, r5, -4
	SW        r21, r4, r3
$BB64_143:                              # %if.else40.i
                                        #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADDI      r9, r9, 1
$BB64_3:                                # %while.body.i
                                        #   Parent Loop BB64_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB64_47 Depth 3
	ADDI      r3, r0, -1
	ADD      r10, r9, r3
	bslli     r3, r10, 2
	ADDI      r4, r1, 144
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 540
	LWI       r4, r4, 36
	ADD      r4, r3, r4
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	SWI       r0, r1, 52
	SWI       r0, r1, 48
	LOAD      r3, r4, 0
	SWI        r3, r1, 48
	LOAD      r3, r4, 1
	SWI        r3, r1, 52
	LOAD      r3, r4, 2
	SWI        r3, r1, 56
	ADDI      r3, r4, 3
	LOAD      r5, r3, 0
	SWI        r5, r1, 60
	LOAD      r5, r3, 1
	SWI        r5, r1, 64
	LOAD      r3, r3, 2
	SWI        r3, r1, 68
	LWI       r3, r1, 448
	MULI      r8, r3, 12
	ADDI      r3, r1, 48
	ADD      r6, r3, r8
	ADDI      r7, r4, 7
	ADDI      r4, r4, 6
	LWI       r5, r1, 428
	MULI      r11, r5, 12
	SWI       r11, r1, 416
	LOAD      r5, r4, 0
	SWI       r5, r1, 72
	LOAD      r4, r7, 0
	SWI       r4, r1, 396
	SWI       r4, r1, 76
	LWI        r4, r6, 4
	LWI       r6, r1, 372
	FPRSUB     r4, r6, r4
	FPMUL      r20, r4, r30
	LW         r4, r3, r11
	LWI       r6, r1, 380
	FPRSUB     r4, r6, r4
	FPMUL      r7, r4, r26
	ADDI      r6, r0, 1
	FPGT   r4, r7, r20
	bneid     r4, ($BB64_5)
	NOP    
# BB#4:                                 # %while.body.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r6, r0, 0
$BB64_5:                                # %while.body.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r6, ($BB64_6)
	NOP    
# BB#7:                                 # %while.body.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r4, r1, 424
	MULI      r31, r4, 12
	ADD      r4, r3, r31
	LWI        r4, r4, 4
	LWI       r6, r1, 372
	FPRSUB     r4, r6, r4
	FPMUL      r21, r4, r30
	LWI       r4, r1, 452
	MULI      r6, r4, 12
	LW         r3, r3, r6
	LWI       r4, r1, 380
	FPRSUB     r3, r4, r3
	FPMUL      r3, r3, r26
	FPGT   r4, r21, r3
	bneid     r4, ($BB64_9)
	ADDI      r12, r0, 1
# BB#8:                                 # %while.body.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r12, r0, 0
$BB64_9:                                # %while.body.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r12, ($BB64_10)
	NOP    
# BB#11:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r24, r21, r7
	ADDI      r23, r0, 0
	ADDI      r22, r0, 1
	bneid     r24, ($BB64_13)
	ADD      r12, r22, r0
# BB#12:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r12, r23, r0
$BB64_13:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r12, ($BB64_15)
	NOP    
# BB#14:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r21, r7, r0
$BB64_15:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r4, r1, 444
	MULI      r4, r4, 12
	SWI       r4, r1, 412
	ADDI      r7, r1, 48
	ADD      r12, r7, r4
	FPLT   r4, r20, r3
	bneid     r4, ($BB64_17)
	ADD      r24, r22, r0
# BB#16:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r24, r23, r0
$BB64_17:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI        r4, r12, 8
	LWI       r12, r1, 376
	FPRSUB     r4, r12, r4
	FPMUL      r12, r4, r25
	FPGT   r4, r21, r12
	bneid     r4, ($BB64_19)
	NOP    
# BB#18:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r23, r0
$BB64_19:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r24, ($BB64_21)
	NOP    
# BB#20:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r20, r3, r0
$BB64_21:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_22)
	NOP    
# BB#23:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r3, r1, 420
	MULI      r3, r3, 12
	ADD      r4, r7, r3
	LWI        r4, r4, 8
	LWI       r7, r1, 376
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r25
	FPGT   r4, r7, r20
	bneid     r4, ($BB64_25)
	ADDI      r22, r0, 1
# BB#24:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r22, r0, 0
$BB64_25:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_26)
	NOP    
# BB#27:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r11, r26, r0
	ADD      r26, r25, r0
	FPLT   r25, r12, r20
	ADDI      r23, r0, 0
	ADDI      r22, r0, 1
	bneid     r25, ($BB64_29)
	ADD      r24, r22, r0
# BB#28:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r24, r23, r0
$BB64_29:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r24, ($BB64_31)
	NOP    
# BB#30:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r12, r20, r0
$BB64_31:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ORI       r4, r0, 0
	FPLT   r4, r12, r4
	bneid     r4, ($BB64_33)
	ADD      r25, r26, r0
# BB#32:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r23, r0
$BB64_33:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_34)
	ADD      r26, r11, r0
# BB#35:                                # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r23, r7, r21
	ADDI      r20, r0, 0
	ADDI      r12, r0, 1
	bneid     r23, ($BB64_37)
	ADD      r22, r12, r0
# BB#36:                                # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r20, r0
$BB64_37:                               # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_39)
	ADD      r11, r8, r0
# BB#38:                                # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r7, r21, r0
$BB64_39:                               # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r4, r1, 368
	FPLT   r4, r4, r7
	bneid     r4, ($BB64_41)
	NOP    
# BB#40:                                # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r12, r20, r0
$BB64_41:                               # %if.end.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r12, ($BB64_42)
	LWI       r8, r1, 416
# BB#43:                                # %if.end7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	beqid     r5, ($BB64_44)
	NOP    
# BB#45:                                # %if.end7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r0, r0
	ADDI      r4, r0, -1
	CMP       r4, r4, r5
	bneid     r4, ($BB64_46)
	NOP    
# BB#70:                                # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r4, r1, 396
	bslli     r4, r4, 3
	LWI       r5, r1, 540
	LWI       r5, r5, 36
	ADD      r5, r5, r4
	SWI       r0, r1, 92
	SWI       r0, r1, 88
	SWI       r0, r1, 84
	SWI       r0, r1, 80
	LOAD      r4, r5, 0
	SWI        r4, r1, 80
	LOAD      r4, r5, 1
	SWI        r4, r1, 84
	LOAD      r4, r5, 2
	SWI        r4, r1, 88
	ADDI      r7, r5, 3
	LOAD      r4, r7, 0
	SWI        r4, r1, 92
	LOAD      r4, r7, 1
	SWI        r4, r1, 96
	LOAD      r4, r7, 2
	SWI        r4, r1, 100
	ADDI      r4, r5, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 104
	ADDI      r4, r5, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 108
	ADDI      r12, r1, 80
	ADD      r4, r12, r11
	LWI        r4, r4, 4
	LWI       r5, r1, 372
	FPRSUB     r4, r5, r4
	FPMUL      r20, r4, r30
	LW         r4, r12, r8
	LWI       r5, r1, 380
	FPRSUB     r4, r5, r4
	FPMUL      r7, r4, r26
	ADDI      r21, r0, 1
	FPGT   r4, r7, r20
	bneid     r4, ($BB64_72)
	NOP    
# BB#71:                                # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r21, r0, 0
$BB64_72:                               # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	SWI       r10, r1, 408
	ADD      r5, r0, r0
	ORI       r22, r0, 1203982336
	bneid     r21, ($BB64_102)
	NOP    
# BB#73:                                # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r4, r12, r31
	LW         r12, r12, r6
	LWI        r4, r4, 4
	LWI       r21, r1, 372
	FPRSUB     r4, r21, r4
	FPMUL      r21, r4, r30
	LWI       r4, r1, 380
	FPRSUB     r4, r4, r12
	FPMUL      r12, r4, r26
	FPGT   r4, r21, r12
	bneid     r4, ($BB64_75)
	ADDI      r23, r0, 1
# BB#74:                                # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r23, r0, 0
$BB64_75:                               # %if.then9.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r23, ($BB64_102)
	NOP    
# BB#76:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r24, r21, r7
	ADDI      r5, r0, 0
	ADDI      r23, r0, 1
	bneid     r24, ($BB64_78)
	ADD      r22, r23, r0
# BB#77:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r5, r0
$BB64_78:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_80)
	NOP    
# BB#79:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r21, r7, r0
$BB64_80:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r24, r1, 80
	LWI       r4, r1, 412
	ADD      r7, r24, r4
	FPLT   r4, r20, r12
	bneid     r4, ($BB64_82)
	ADD      r22, r23, r0
# BB#81:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r5, r0
$BB64_82:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI        r4, r7, 8
	LWI       r7, r1, 376
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r25
	FPGT   r4, r21, r7
	bneid     r4, ($BB64_84)
	NOP    
# BB#83:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r23, r5, r0
$BB64_84:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_86)
	NOP    
# BB#85:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r20, r12, r0
$BB64_86:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r5, r0, r0
	ORI       r22, r0, 1203982336
	bneid     r23, ($BB64_102)
	NOP    
# BB#87:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r4, r24, r3
	LWI        r4, r4, 8
	LWI       r12, r1, 376
	FPRSUB     r4, r12, r4
	FPMUL      r12, r4, r25
	FPGT   r4, r12, r20
	bneid     r4, ($BB64_89)
	ADDI      r23, r0, 1
# BB#88:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r23, r0, 0
$BB64_89:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r23, ($BB64_102)
	NOP    
# BB#90:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPLT   r24, r7, r20
	ADDI      r5, r0, 0
	ADDI      r23, r0, 1
	bneid     r24, ($BB64_92)
	ADD      r22, r23, r0
# BB#91:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r5, r0
$BB64_92:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r22, ($BB64_94)
	NOP    
# BB#93:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r7, r20, r0
$BB64_94:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ORI       r4, r0, 0
	FPLT   r4, r7, r4
	bneid     r4, ($BB64_96)
	NOP    
# BB#95:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r23, r5, r0
$BB64_96:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r5, r0, r0
	ORI       r22, r0, 1203982336
	bneid     r23, ($BB64_102)
	NOP    
# BB#97:                                # %if.end81.i.i67.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r4, r12, r21
	ADDI      r5, r0, 1
	bneid     r4, ($BB64_99)
	ADD      r7, r5, r0
# BB#98:                                # %if.end81.i.i67.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r7, r0, 0
$BB64_99:                               # %if.end81.i.i67.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r7, ($BB64_101)
	NOP    
# BB#100:                               # %if.end81.i.i67.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r12, r21, r0
$BB64_101:                              # %if.end81.i.i67.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r22, r12, r0
$BB64_102:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI       r4, r1, 540
	LWI       r4, r4, 36
	LWI       r7, r1, 396
	ADDI      r21, r7, 1
	bslli     r7, r21, 3
	ADD      r7, r4, r7
	SWI       r0, r1, 124
	SWI       r0, r1, 120
	SWI       r0, r1, 116
	SWI       r0, r1, 112
	LOAD      r4, r7, 0
	SWI        r4, r1, 112
	LOAD      r4, r7, 1
	SWI        r4, r1, 116
	LOAD      r4, r7, 2
	SWI        r4, r1, 120
	ADDI      r12, r7, 3
	LOAD      r4, r12, 0
	SWI        r4, r1, 124
	LOAD      r4, r12, 1
	SWI        r4, r1, 128
	LOAD      r4, r12, 2
	SWI        r4, r1, 132
	ADDI      r4, r7, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 136
	ADDI      r4, r7, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 140
	ADDI      r12, r1, 112
	ADD      r4, r12, r11
	LWI        r4, r4, 4
	LWI       r7, r1, 372
	FPRSUB     r4, r7, r4
	FPMUL      r10, r4, r30
	LW         r4, r12, r8
	LWI       r7, r1, 380
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r26
	ADDI      r11, r0, 1
	FPGT   r4, r7, r10
	bneid     r4, ($BB64_104)
	NOP    
# BB#103:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r11, r0, 0
$BB64_104:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r11, ($BB64_129)
	NOP    
# BB#105:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r4, r12, r31
	LWI        r4, r4, 4
	LWI       r8, r1, 372
	FPRSUB     r4, r8, r4
	FPMUL      r8, r4, r30
	LW         r4, r12, r6
	LWI       r6, r1, 380
	FPRSUB     r4, r6, r4
	FPMUL      r6, r4, r26
	FPGT   r4, r8, r6
	bneid     r4, ($BB64_107)
	ADDI      r11, r0, 1
# BB#106:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r11, r0, 0
$BB64_107:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r11, ($BB64_129)
	NOP    
# BB#108:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPGT   r23, r8, r7
	ADDI      r12, r0, 0
	ADDI      r11, r0, 1
	bneid     r23, ($BB64_110)
	ADD      r20, r11, r0
# BB#109:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r20, r12, r0
$BB64_110:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r20, ($BB64_112)
	NOP    
# BB#111:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r8, r7, r0
$BB64_112:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r20, r1, 112
	LWI       r4, r1, 412
	ADD      r7, r20, r4
	FPLT   r4, r10, r6
	bneid     r4, ($BB64_114)
	ADD      r23, r11, r0
# BB#113:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r23, r12, r0
$BB64_114:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	LWI        r4, r7, 8
	LWI       r7, r1, 376
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r25
	FPGT   r4, r8, r7
	bneid     r4, ($BB64_116)
	NOP    
# BB#115:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r11, r12, r0
$BB64_116:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r23, ($BB64_118)
	NOP    
# BB#117:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r10, r6, r0
$BB64_118:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r11, ($BB64_129)
	NOP    
# BB#119:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r3, r20, r3
	LWI        r3, r3, 8
	LWI       r4, r1, 376
	FPRSUB     r3, r4, r3
	FPMUL      r3, r3, r25
	FPGT   r4, r3, r10
	bneid     r4, ($BB64_121)
	ADDI      r6, r0, 1
# BB#120:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r6, r0, 0
$BB64_121:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r6, ($BB64_129)
	NOP    
# BB#122:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	FPLT   r20, r7, r10
	ADDI      r11, r0, 0
	ADDI      r6, r0, 1
	bneid     r20, ($BB64_124)
	ADD      r12, r6, r0
# BB#123:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r12, r11, r0
$BB64_124:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r12, ($BB64_126)
	NOP    
# BB#125:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r7, r10, r0
$BB64_126:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ORI       r4, r0, 0
	FPGE   r10, r7, r4
	FPUN   r4, r7, r4
	BITOR        r4, r4, r10
	bneid     r4, ($BB64_128)
	NOP    
# BB#127:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADD      r6, r11, r0
$BB64_128:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bneid     r6, ($BB64_132)
	NOP    
$BB64_129:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB64_3 Depth=2
	ADDI      r3, r0, 1
	CMP       r3, r3, r5
	bneid     r3, ($BB64_130)
	NOP    
# BB#131:                               # %if.then21.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bslli     r3, r9, 2
	ADDI      r4, r1, 144
	ADD      r3, r4, r3
	LWI       r4, r1, 396
	brid      ($BB64_144)
	SWI       r4, r3, -4
$BB64_6:                                #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_10:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_22:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_26:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_34:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_42:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_44:                               #   in Loop: Header=BB64_3 Depth=2
	brid      ($BB64_144)
	ADD      r9, r10, r0
$BB64_46:                               #   in Loop: Header=BB64_3 Depth=2
	SWI       r10, r1, 408
$BB64_47:                               # %for.body.i.i
                                        #   Parent Loop BB64_1 Depth=1
                                        #     Parent Loop BB64_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	MULI      r3, r22, 11
	LWI       r4, r1, 396
	ADD      r3, r3, r4
	LOAD      r11, r3, 0
	LOAD      r20, r3, 1
	LOAD      r31, r3, 2
	ADDI      r4, r3, 3
	LOAD      r10, r4, 0
	LOAD      r23, r4, 1
	LOAD      r7, r4, 2
	ADDI      r4, r3, 6
	LOAD      r8, r4, 0
	LOAD      r6, r4, 1
	LOAD      r4, r4, 2
	FPRSUB     r9, r31, r4
	FPRSUB     r21, r20, r6
	FPMUL      r4, r29, r21
	FPMUL      r6, r28, r9
	FPRSUB     r6, r4, r6
	FPRSUB     r12, r11, r8
	FPMUL      r4, r29, r12
	FPMUL      r8, r27, r9
	FPRSUB     r8, r8, r4
	FPRSUB     r24, r11, r10
	FPRSUB     r23, r20, r23
	FPMUL      r4, r8, r23
	FPMUL      r10, r6, r24
	FPADD      r25, r10, r4
	FPMUL      r4, r28, r12
	FPMUL      r10, r27, r21
	FPRSUB     r10, r4, r10
	FPRSUB     r26, r31, r7
	FPMUL      r4, r10, r26
	FPADD      r30, r25, r4
	ORI       r4, r0, 0
	FPGE   r7, r30, r4
	FPUN   r4, r30, r4
	BITOR        r4, r4, r7
	bneid     r4, ($BB64_49)
	ADDI      r25, r0, 1
# BB#48:                                # %for.body.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADDI      r25, r0, 0
$BB64_49:                               # %for.body.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r25, ($BB64_51)
	ADD      r7, r30, r0
# BB#50:                                # %if.then.i.i.i.i33
                                        #   in Loop: Header=BB64_47 Depth=3
	FPNEG      r7, r30
$BB64_51:                               # %_ZN4util4fabsERKf.exit.i.i.i36
                                        #   in Loop: Header=BB64_47 Depth=3
	ORI       r4, r0, 953267991
	FPLT   r4, r7, r4
	bneid     r4, ($BB64_53)
	ADDI      r7, r0, 1
# BB#52:                                # %_ZN4util4fabsERKf.exit.i.i.i36
                                        #   in Loop: Header=BB64_47 Depth=3
	ADDI      r7, r0, 0
$BB64_53:                               # %_ZN4util4fabsERKf.exit.i.i.i36
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r7, ($BB64_68)
	NOP    
# BB#54:                                # %if.end.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	LWI       r4, r1, 376
	FPRSUB     r31, r31, r4
	LWI       r4, r1, 380
	FPRSUB     r11, r11, r4
	LWI       r4, r1, 372
	FPRSUB     r25, r20, r4
	FPMUL      r7, r25, r24
	FPMUL      r20, r11, r23
	FPMUL      r24, r31, r24
	FPMUL      r4, r11, r26
	FPMUL      r23, r31, r23
	FPMUL      r26, r25, r26
	FPRSUB     r20, r7, r20
	FPRSUB     r24, r4, r24
	FPRSUB     r23, r23, r26
	FPMUL      r4, r24, r21
	FPMUL      r7, r23, r12
	FPADD      r4, r7, r4
	FPMUL      r7, r20, r9
	FPADD      r4, r4, r7
	ORI       r7, r0, 1065353216
	FPDIV      r7, r7, r30
	FPMUL      r9, r4, r7
	ORI       r4, r0, 0
	FPLT   r4, r9, r4
	bneid     r4, ($BB64_56)
	ADDI      r12, r0, 1
# BB#55:                                # %if.end.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADDI      r12, r0, 0
$BB64_56:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r12, ($BB64_68)
	NOP    
# BB#57:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	FPMUL      r4, r8, r25
	FPMUL      r6, r6, r11
	FPADD      r4, r6, r4
	FPMUL      r8, r24, r28
	FPMUL      r11, r23, r27
	FPMUL      r6, r10, r31
	FPADD      r6, r4, r6
	FPADD      r4, r11, r8
	FPMUL      r8, r20, r29
	FPADD      r4, r4, r8
	FPMUL      r8, r4, r7
	FPMUL      r6, r6, r7
	FPADD      r4, r6, r8
	ORI       r7, r0, 1065353216
	FPLE   r12, r4, r7
	ORI       r4, r0, 0
	FPGE   r6, r6, r4
	FPGE   r11, r8, r4
	LWI       r7, r1, 368
	FPGE   r4, r9, r7
	FPUN   r7, r9, r7
	BITOR        r20, r7, r4
	ADDI      r10, r0, 0
	ADDI      r8, r0, 1
	bneid     r12, ($BB64_59)
	ADD      r7, r8, r0
# BB#58:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADD      r7, r10, r0
$BB64_59:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r20, ($BB64_61)
	ADD      r12, r8, r0
# BB#60:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADD      r12, r10, r0
$BB64_61:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r11, ($BB64_63)
	ADD      r20, r8, r0
# BB#62:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADD      r20, r10, r0
$BB64_63:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r6, ($BB64_65)
	NOP    
# BB#64:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADD      r8, r10, r0
$BB64_65:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	bneid     r12, ($BB64_68)
	NOP    
# BB#66:                                # %if.end9.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	BITAND       r4, r8, r20
	BITAND       r4, r4, r7
	ADDI      r6, r0, 1
	CMP       r4, r6, r4
	bneid     r4, ($BB64_68)
	NOP    
# BB#67:                                # %_ZNK10syTriangle6ID_MATEv.exit.i.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADDI      r4, r3, 10
	LOAD      r4, r4, 0
	SWI       r4, r1, 400
	SWI       r3, r1, 404
	SWI       r9, r1, 368
$BB64_68:                               # %_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord.exit.i.i
                                        #   in Loop: Header=BB64_47 Depth=3
	ADDI      r22, r22, 1
	CMP       r3, r5, r22
	bneid     r3, ($BB64_47)
	NOP    
# BB#69:                                #   in Loop: Header=BB64_3 Depth=2
	LWI       r9, r1, 408
	LWI       r25, r1, 432
	LWI       r30, r1, 436
	brid      ($BB64_144)
	LWI       r26, r1, 440
$BB64_130:                              #   in Loop: Header=BB64_3 Depth=2
	LWI       r9, r1, 408
$BB64_144:                              # %while.cond.backedge.i
                                        #   in Loop: Header=BB64_3 Depth=2
	bgtid     r9, ($BB64_3)
	NOP    
# BB#145:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r3, r0, 1203982336
	LWI       r4, r1, 368
	FPNE   r4, r4, r3
	bneid     r4, ($BB64_147)
	ADDI      r3, r0, 1
# BB#146:                               # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r3, r0, 0
$BB64_147:                              # %_ZNK5Scene8TraceRayERK3RayR9HitRecord.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	beqid     r3, ($BB64_148)
	NOP    
# BB#149:                               # %_ZNK10syTriangle6NormalEv.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r9, r1, 404
	LOAD      r3, r9, 0
	LOAD      r4, r9, 1
	LOAD      r6, r9, 2
	ADDI      r8, r9, 3
	LOAD      r5, r8, 0
	LOAD      r7, r8, 1
	LOAD      r10, r8, 2
	ADDI      r9, r9, 6
	LOAD      r8, r9, 0
	LOAD      r11, r9, 1
	LOAD      r9, r9, 2
	FPRSUB     r9, r9, r6
	FPRSUB     r10, r10, r6
	FPRSUB     r6, r7, r4
	FPRSUB     r7, r11, r4
	FPMUL      r4, r10, r7
	FPMUL      r11, r6, r9
	FPRSUB     r4, r4, r11
	FPRSUB     r5, r5, r3
	FPRSUB     r8, r8, r3
	FPMUL      r3, r10, r8
	FPMUL      r9, r5, r9
	FPRSUB     r3, r9, r3
	FPMUL      r9, r3, r3
	FPMUL      r10, r4, r4
	FPADD      r9, r10, r9
	FPMUL      r6, r6, r8
	FPMUL      r5, r5, r7
	FPRSUB     r5, r6, r5
	FPMUL      r6, r5, r5
	FPADD      r6, r9, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r6, r7, r6
	FPDIV      r25, r3, r6
	FPMUL      r3, r25, r28
	FPDIV      r30, r4, r6
	FPMUL      r4, r30, r27
	FPADD      r3, r4, r3
	FPDIV      r24, r5, r6
	FPMUL      r4, r24, r29
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB64_151)
	ADDI      r3, r0, 1
# BB#150:                               # %_ZNK10syTriangle6NormalEv.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r3, r0, 0
$BB64_151:                              # %_ZNK10syTriangle6NormalEv.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	bneid     r3, ($BB64_153)
	NOP    
# BB#152:                               # %if.then3.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPNEG      r24, r24
	FPNEG      r25, r25
	FPNEG      r30, r30
$BB64_153:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r5, r1, 368
	FPMUL      r4, r27, r5
	FPMUL      r3, r29, r5
	FPMUL      r6, r28, r5
	LWI       r5, r1, 376
	FPADD      r3, r5, r3
	LWI       r5, r1, 380
	FPADD      r5, r5, r4
	ORI       r7, r0, 1028443341
	LWI       r4, r1, 372
	FPADD      r6, r4, r6
	LWI       r4, r1, 400
	MULI      r4, r4, 25
	LWI       r11, r1, 540
	LWI       r8, r11, 32
	ADD      r4, r4, r8
	ADDI      r4, r4, 4
	LOAD      r8, r4, 0
	SWI       r8, r1, 464
	LOAD      r8, r4, 1
	SWI       r8, r1, 460
	LOAD      r4, r4, 2
	SWI       r4, r1, 456
	LWI        r4, r11, 4
	FPRSUB     r4, r6, r4
	LWI        r8, r11, 0
	FPRSUB     r9, r5, r8
	FPMUL      r8, r4, r4
	FPMUL      r10, r9, r9
	FPADD      r8, r10, r8
	LWI        r10, r11, 8
	FPRSUB     r10, r3, r10
	FPMUL      r11, r10, r10
	FPADD      r8, r8, r11
	FPINVSQRT r11, r8
	SWI       r11, r1, 500
	FPINVSQRT r11, r8
	ORI       r8, r0, 1065353216
	FPDIV      r11, r8, r11
	FPDIV      r12, r9, r11
	SWI       r12, r1, 448
	FPDIV      r20, r4, r11
	SWI       r20, r1, 452
	FPMUL      r4, r20, r20
	FPMUL      r9, r12, r12
	FPADD      r4, r9, r4
	FPDIV      r23, r10, r11
	SWI       r23, r1, 480
	FPMUL      r9, r23, r23
	FPADD      r4, r4, r9
	FPINVSQRT r4, r4
	FPDIV      r10, r8, r4
	ORI       r9, r0, 0
	FPDIV      r26, r12, r10
	FPDIV      r28, r8, r26
	SWI       r28, r1, 424
	FPGE   r11, r28, r9
	FPUN   r21, r28, r9
	FPDIV      r4, r20, r10
	SWI       r4, r1, 372
	FPDIV      r27, r8, r4
	FPGE   r4, r27, r9
	FPUN   r12, r27, r9
	BITOR        r22, r12, r4
	FPDIV      r4, r23, r10
	SWI       r4, r1, 376
	FPDIV      r29, r8, r4
	FPGE   r4, r29, r9
	FPUN   r8, r29, r9
	BITOR        r23, r8, r4
	FPLT   r8, r28, r9
	FPLT   r10, r27, r9
	FPLT   r20, r29, r9
	ADDI      r9, r0, 0
	ADDI      r12, r0, 1
	bneid     r23, ($BB64_155)
	SWI       r12, r1, 400
# BB#154:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 400
$BB64_155:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADD      r23, r25, r0
	ADD      r4, r24, r0
	BITOR        r11, r21, r11
	bneid     r22, ($BB64_157)
	SWI       r12, r1, 404
# BB#156:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 404
$BB64_157:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r12, r1, 408
	bneid     r11, ($BB64_159)
	ADD      r21, r23, r0
# BB#158:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 408
$BB64_159:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	bneid     r20, ($BB64_161)
	SWI       r12, r1, 412
# BB#160:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 412
$BB64_161:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r12, r1, 416
	ADD      r24, r27, r0
	SWI       r24, r1, 428
	bneid     r10, ($BB64_163)
	SWI       r29, r1, 432
# BB#162:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 416
$BB64_163:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPMUL      r11, r21, r7
	SWI       r21, r1, 440
	FPMUL      r10, r4, r7
	SWI       r4, r1, 436
	FPMUL      r7, r30, r7
	SWI       r30, r1, 444
	bneid     r8, ($BB64_165)
	SWI       r12, r1, 420
# BB#164:                               # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r9, r1, 420
$BB64_165:                              # %if.end6.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPADD      r4, r6, r11
	SWI       r4, r1, 384
	FPADD      r4, r5, r7
	SWI       r4, r1, 388
	FPADD      r3, r3, r10
	SWI       r3, r1, 392
	ORI       r11, r0, 1203982336
	brid      ($BB64_166)
	SWI       r0, r1, 144
$BB64_148:                              # %if.then.i
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r4, r1, 540
	LWI        r3, r4, 48
	LWI        r5, r4, 44
	LWI        r4, r4, 40
	LWI       r8, r1, 468
	LWI       r9, r1, 472
	LWI       r10, r1, 476
	LWI       r11, r1, 524
	brid      ($BB64_343)
	LWI       r12, r1, 528
$BB64_295:                              # %if.else24.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	beqid     r3, ($BB64_296)
	NOP    
# BB#297:                               # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r9, r5, r6
	ADDI      r7, r0, 0
	ADDI      r3, r0, 1
	bneid     r9, ($BB64_299)
	ADD      r8, r3, r0
# BB#298:                               # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r8, r7, r0
$BB64_299:                              # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r8, ($BB64_301)
	NOP    
# BB#300:                               # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r5, r6, r0
$BB64_301:                              # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGE   r4, r10, r5
	FPUN   r5, r10, r5
	BITOR        r4, r5, r4
	bneid     r4, ($BB64_303)
	NOP    
# BB#302:                               # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r3, r7, r0
$BB64_303:                              # %if.else31.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r3, ($BB64_305)
	NOP    
# BB#304:                               # %if.then35.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bslli     r3, r12, 2
	ADDI      r4, r1, 144
	ADD      r5, r4, r3
	SWI       r21, r5, -4
	LWI       r5, r1, 380
	SW        r5, r4, r3
	brid      ($BB64_306)
	NOP    
$BB64_296:                              # %if.then28.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bslli     r3, r12, 2
	ADDI      r4, r1, 144
	ADD      r3, r4, r3
	brid      ($BB64_307)
	SWI       r21, r3, -4
$BB64_305:                              # %if.else40.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bslli     r3, r12, 2
	ADDI      r4, r1, 144
	ADD      r5, r4, r3
	LWI       r6, r1, 380
	SWI       r6, r5, -4
	SW        r21, r4, r3
$BB64_306:                              # %if.else40.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADDI      r12, r12, 1
$BB64_166:                              # %while.body.i.i
                                        #   Parent Loop BB64_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB64_210 Depth 3
	ADDI      r3, r0, -1
	ADD      r27, r12, r3
	bslli     r3, r27, 2
	ADDI      r4, r1, 144
	LW        r3, r4, r3
	bslli     r3, r3, 3
	LWI       r4, r1, 540
	LWI       r4, r4, 36
	ADD      r3, r3, r4
	SWI       r0, r1, 280
	SWI       r0, r1, 276
	SWI       r0, r1, 272
	LOAD      r4, r3, 0
	SWI        r4, r1, 272
	LOAD      r4, r3, 1
	SWI        r4, r1, 276
	LOAD      r4, r3, 2
	SWI        r4, r1, 280
	ADDI      r5, r3, 3
	LOAD      r4, r5, 0
	SWI        r4, r1, 284
	LOAD      r4, r5, 1
	SWI        r4, r1, 288
	LOAD      r4, r5, 2
	SWI        r4, r1, 292
	LWI       r4, r1, 404
	MULI      r30, r4, 12
	ADDI      r8, r1, 272
	ADD      r5, r8, r30
	ADDI      r6, r3, 7
	ADDI      r3, r3, 6
	LWI       r4, r1, 420
	MULI      r31, r4, 12
	LOAD      r3, r3, 0
	SWI       r3, r1, 296
	LOAD      r4, r6, 0
	SWI       r4, r1, 380
	SWI       r4, r1, 300
	LWI        r4, r5, 4
	LWI       r5, r1, 384
	FPRSUB     r4, r5, r4
	FPMUL      r20, r4, r24
	LW         r4, r8, r31
	LWI       r5, r1, 388
	FPRSUB     r4, r5, r4
	FPMUL      r7, r4, r28
	ADDI      r5, r0, 1
	FPGT   r4, r7, r20
	bneid     r4, ($BB64_168)
	NOP    
# BB#167:                               # %while.body.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r5, r0, 0
$BB64_168:                              # %while.body.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r5, ($BB64_169)
	NOP    
# BB#170:                               # %while.body.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI       r4, r1, 416
	MULI      r6, r4, 12
	ADD      r4, r8, r6
	LWI        r4, r4, 4
	LWI       r5, r1, 384
	FPRSUB     r4, r5, r4
	FPMUL      r21, r4, r24
	LWI       r4, r1, 408
	MULI      r5, r4, 12
	LW         r4, r8, r5
	LWI       r8, r1, 388
	FPRSUB     r4, r8, r4
	FPMUL      r8, r4, r28
	FPGT   r4, r21, r8
	bneid     r4, ($BB64_172)
	ADDI      r9, r0, 1
# BB#171:                               # %while.body.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r9, r0, 0
$BB64_172:                              # %while.body.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r9, ($BB64_173)
	NOP    
# BB#174:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r23, r21, r7
	ADDI      r22, r0, 0
	ADDI      r10, r0, 1
	bneid     r23, ($BB64_176)
	ADD      r9, r10, r0
# BB#175:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r9, r22, r0
$BB64_176:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r9, ($BB64_178)
	NOP    
# BB#177:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r21, r7, r0
$BB64_178:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI       r4, r1, 400
	MULI      r7, r4, 12
	ADDI      r23, r1, 272
	ADD      r9, r23, r7
	FPLT   r4, r20, r8
	bneid     r4, ($BB64_180)
	ADD      r25, r10, r0
# BB#179:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r25, r22, r0
$BB64_180:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI        r4, r9, 8
	LWI       r9, r1, 392
	FPRSUB     r4, r9, r4
	FPMUL      r9, r4, r29
	FPGT   r4, r21, r9
	bneid     r4, ($BB64_182)
	NOP    
# BB#181:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r22, r0
$BB64_182:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r25, ($BB64_184)
	NOP    
# BB#183:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r20, r8, r0
$BB64_184:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r10, ($BB64_185)
	NOP    
# BB#186:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI       r4, r1, 412
	MULI      r8, r4, 12
	ADD      r4, r23, r8
	LWI        r4, r4, 8
	LWI       r10, r1, 392
	FPRSUB     r4, r10, r4
	FPMUL      r10, r4, r29
	FPGT   r4, r10, r20
	bneid     r4, ($BB64_188)
	ADDI      r22, r0, 1
# BB#187:                               # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r22, r0, 0
$BB64_188:                              # %if.end.i.i133.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_189)
	NOP    
# BB#190:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r4, r27, r0
	FPLT   r27, r9, r20
	ADDI      r23, r0, 0
	ADDI      r22, r0, 1
	bneid     r27, ($BB64_192)
	ADD      r25, r22, r0
# BB#191:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r25, r23, r0
$BB64_192:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r25, ($BB64_194)
	ADD      r27, r4, r0
# BB#193:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r9, r20, r0
$BB64_194:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ORI       r4, r0, 0
	FPLT   r4, r9, r4
	bneid     r4, ($BB64_196)
	NOP    
# BB#195:                               # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r22, r23, r0
$BB64_196:                              # %if.end72.i.i137.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_197)
	NOP    
# BB#198:                               # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r23, r10, r21
	ADDI      r20, r0, 0
	ADDI      r9, r0, 1
	bneid     r23, ($BB64_200)
	ADD      r22, r9, r0
# BB#199:                               # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r22, r20, r0
$BB64_200:                              # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_202)
	NOP    
# BB#201:                               # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r21, r0
$BB64_202:                              # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPLT   r4, r11, r10
	bneid     r4, ($BB64_204)
	NOP    
# BB#203:                               # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r9, r20, r0
$BB64_204:                              # %if.end.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r9, ($BB64_205)
	NOP    
# BB#206:                               # %if.end7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	beqid     r3, ($BB64_207)
	NOP    
# BB#208:                               # %if.end7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r21, r0, r0
	ADDI      r4, r0, -1
	CMP       r4, r4, r3
	bneid     r4, ($BB64_209)
	NOP    
# BB#233:                               # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI       r3, r1, 380
	bslli     r3, r3, 3
	LWI       r4, r1, 540
	LWI       r4, r4, 36
	ADD      r3, r4, r3
	SWI       r0, r1, 312
	SWI       r0, r1, 308
	SWI       r0, r1, 304
	LOAD      r4, r3, 0
	SWI        r4, r1, 304
	LOAD      r4, r3, 1
	SWI        r4, r1, 308
	LOAD      r4, r3, 2
	SWI        r4, r1, 312
	ADDI      r9, r3, 3
	LOAD      r4, r9, 0
	SWI        r4, r1, 316
	LOAD      r4, r9, 1
	SWI        r4, r1, 320
	LOAD      r4, r9, 2
	SWI        r4, r1, 324
	ADDI      r4, r3, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 328
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	SWI       r3, r1, 332
	ADDI      r21, r1, 304
	ADD      r3, r21, r30
	LWI        r3, r3, 4
	LWI       r4, r1, 384
	FPRSUB     r3, r4, r3
	FPMUL      r9, r3, r24
	LW         r3, r21, r31
	LWI       r4, r1, 388
	FPRSUB     r3, r4, r3
	FPMUL      r20, r3, r28
	ADDI      r22, r0, 1
	FPGT   r3, r20, r9
	bneid     r3, ($BB64_235)
	NOP    
# BB#234:                               # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r22, r0, 0
$BB64_235:                              # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	SWI       r27, r1, 396
	ADD      r3, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r22, ($BB64_265)
	NOP    
# BB#236:                               # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r4, r21, r6
	LW         r22, r21, r5
	LWI        r4, r4, 4
	LWI       r21, r1, 384
	FPRSUB     r4, r21, r4
	FPMUL      r21, r4, r24
	LWI       r4, r1, 388
	FPRSUB     r4, r4, r22
	FPMUL      r22, r4, r28
	FPGT   r4, r21, r22
	bneid     r4, ($BB64_238)
	ADDI      r23, r0, 1
# BB#237:                               # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r23, r0, 0
$BB64_238:                              # %if.then9.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r23, ($BB64_265)
	NOP    
# BB#239:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r25, r21, r20
	ADDI      r3, r0, 0
	ADDI      r23, r0, 1
	bneid     r25, ($BB64_241)
	ADD      r10, r23, r0
# BB#240:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r3, r0
$BB64_241:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r10, ($BB64_243)
	NOP    
# BB#242:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r21, r20, r0
$BB64_243:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r25, r1, 304
	ADD      r20, r25, r7
	FPLT   r4, r9, r22
	bneid     r4, ($BB64_245)
	ADD      r10, r23, r0
# BB#244:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r3, r0
$BB64_245:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI        r4, r20, 8
	LWI       r20, r1, 392
	FPRSUB     r4, r20, r4
	FPMUL      r20, r4, r29
	FPGT   r4, r21, r20
	bneid     r4, ($BB64_247)
	NOP    
# BB#246:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r23, r3, r0
$BB64_247:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r10, ($BB64_249)
	NOP    
# BB#248:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r9, r22, r0
$BB64_249:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r3, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r23, ($BB64_265)
	NOP    
# BB#250:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r4, r25, r8
	LWI        r4, r4, 8
	LWI       r22, r1, 392
	FPRSUB     r4, r22, r4
	FPMUL      r22, r4, r29
	FPGT   r4, r22, r9
	bneid     r4, ($BB64_252)
	ADDI      r23, r0, 1
# BB#251:                               # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r23, r0, 0
$BB64_252:                              # %if.end.i.i59.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r23, ($BB64_265)
	NOP    
# BB#253:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPLT   r25, r20, r9
	ADDI      r3, r0, 0
	ADDI      r23, r0, 1
	bneid     r25, ($BB64_255)
	ADD      r10, r23, r0
# BB#254:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r3, r0
$BB64_255:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r10, ($BB64_257)
	NOP    
# BB#256:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r20, r9, r0
$BB64_257:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ORI       r4, r0, 0
	FPLT   r4, r20, r4
	bneid     r4, ($BB64_259)
	NOP    
# BB#258:                               # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r23, r3, r0
$BB64_259:                              # %if.end72.i.i63.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r3, r0, r0
	ORI       r10, r0, 1203982336
	bneid     r23, ($BB64_265)
	NOP    
# BB#260:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r4, r22, r21
	ADDI      r3, r0, 1
	bneid     r4, ($BB64_262)
	ADD      r9, r3, r0
# BB#261:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r9, r0, 0
$BB64_262:                              # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r9, ($BB64_264)
	NOP    
# BB#263:                               # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r22, r21, r0
$BB64_264:                              # %if.end81.i.i67.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r10, r22, r0
$BB64_265:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI       r4, r1, 540
	LWI       r4, r4, 36
	LWI       r9, r1, 380
	ADDI      r21, r9, 1
	bslli     r9, r21, 3
	ADD      r9, r4, r9
	SWI       r0, r1, 344
	SWI       r0, r1, 340
	SWI       r0, r1, 336
	LOAD      r4, r9, 0
	SWI        r4, r1, 336
	LOAD      r4, r9, 1
	SWI        r4, r1, 340
	LOAD      r4, r9, 2
	SWI        r4, r1, 344
	ADDI      r20, r9, 3
	LOAD      r4, r20, 0
	SWI        r4, r1, 348
	LOAD      r4, r20, 1
	SWI        r4, r1, 352
	LOAD      r4, r20, 2
	SWI        r4, r1, 356
	ADDI      r4, r9, 6
	LOAD      r4, r4, 0
	SWI       r4, r1, 360
	ADDI      r4, r9, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 364
	ADDI      r22, r1, 336
	ADD      r4, r22, r30
	LWI        r4, r4, 4
	LWI       r9, r1, 384
	FPRSUB     r4, r9, r4
	FPMUL      r20, r4, r24
	LW         r4, r22, r31
	LWI       r9, r1, 388
	FPRSUB     r4, r9, r4
	FPMUL      r9, r4, r28
	ADDI      r23, r0, 1
	FPGT   r4, r9, r20
	bneid     r4, ($BB64_267)
	NOP    
# BB#266:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r23, r0, 0
$BB64_267:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r23, ($BB64_292)
	NOP    
# BB#268:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r4, r22, r6
	LWI        r4, r4, 4
	LWI       r6, r1, 384
	FPRSUB     r4, r6, r4
	FPMUL      r6, r4, r24
	LW         r4, r22, r5
	LWI       r5, r1, 388
	FPRSUB     r4, r5, r4
	FPMUL      r5, r4, r28
	FPGT   r4, r6, r5
	bneid     r4, ($BB64_270)
	ADDI      r22, r0, 1
# BB#269:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r22, r0, 0
$BB64_270:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_292)
	NOP    
# BB#271:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPGT   r27, r6, r9
	ADDI      r23, r0, 0
	ADDI      r22, r0, 1
	bneid     r27, ($BB64_273)
	ADD      r25, r22, r0
# BB#272:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r25, r23, r0
$BB64_273:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r25, ($BB64_275)
	NOP    
# BB#274:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r6, r9, r0
$BB64_275:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r9, r1, 336
	ADD      r7, r9, r7
	FPLT   r4, r20, r5
	bneid     r4, ($BB64_277)
	ADD      r25, r22, r0
# BB#276:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r25, r23, r0
$BB64_277:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	LWI        r4, r7, 8
	LWI       r7, r1, 392
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r29
	FPGT   r4, r6, r7
	bneid     r4, ($BB64_279)
	NOP    
# BB#278:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r22, r23, r0
$BB64_279:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r25, ($BB64_281)
	NOP    
# BB#280:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r20, r5, r0
$BB64_281:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_292)
	NOP    
# BB#282:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r4, r9, r8
	LWI        r4, r4, 8
	LWI       r5, r1, 392
	FPRSUB     r4, r5, r4
	FPMUL      r5, r4, r29
	FPGT   r4, r5, r20
	bneid     r4, ($BB64_284)
	ADDI      r8, r0, 1
# BB#283:                               # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r8, r0, 0
$BB64_284:                              # %if.end.i.i7.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r8, ($BB64_292)
	NOP    
# BB#285:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	FPLT   r23, r7, r20
	ADDI      r9, r0, 0
	ADDI      r8, r0, 1
	bneid     r23, ($BB64_287)
	ADD      r22, r8, r0
# BB#286:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r22, r9, r0
$BB64_287:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r22, ($BB64_289)
	NOP    
# BB#288:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r7, r20, r0
$BB64_289:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ORI       r4, r0, 0
	FPGE   r20, r7, r4
	FPUN   r4, r7, r4
	BITOR        r4, r4, r20
	bneid     r4, ($BB64_291)
	NOP    
# BB#290:                               # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADD      r8, r9, r0
$BB64_291:                              # %if.end72.i.i.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bneid     r8, ($BB64_295)
	NOP    
$BB64_292:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	ADDI      r4, r0, 1
	CMP       r3, r4, r3
	bneid     r3, ($BB64_293)
	LWI       r4, r1, 396
# BB#294:                               # %if.then21.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bslli     r3, r12, 2
	ADDI      r4, r1, 144
	ADD      r3, r4, r3
	LWI       r4, r1, 380
	brid      ($BB64_307)
	SWI       r4, r3, -4
$BB64_169:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_173:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_185:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_189:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_197:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_205:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_207:                              #   in Loop: Header=BB64_166 Depth=2
	brid      ($BB64_307)
	ADD      r12, r27, r0
$BB64_209:                              #   in Loop: Header=BB64_166 Depth=2
	SWI       r27, r1, 396
$BB64_210:                              # %for.body.i.i.i
                                        #   Parent Loop BB64_1 Depth=1
                                        #     Parent Loop BB64_166 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	MULI      r4, r21, 11
	LWI       r5, r1, 380
	ADD      r6, r4, r5
	LOAD      r12, r6, 0
	LOAD      r20, r6, 1
	LOAD      r30, r6, 2
	ADDI      r5, r6, 6
	ADDI      r4, r6, 3
	LOAD      r7, r4, 0
	LOAD      r9, r4, 1
	LOAD      r10, r4, 2
	LOAD      r6, r5, 0
	LOAD      r4, r5, 1
	LOAD      r5, r5, 2
	FPRSUB     r8, r30, r5
	FPRSUB     r22, r20, r4
	LWI       r23, r1, 376
	FPMUL      r4, r23, r22
	LWI       r24, r1, 372
	FPMUL      r5, r24, r8
	FPRSUB     r5, r4, r5
	FPRSUB     r31, r12, r6
	FPMUL      r4, r23, r31
	FPMUL      r6, r26, r8
	FPRSUB     r6, r6, r4
	FPRSUB     r23, r12, r7
	FPRSUB     r9, r20, r9
	FPMUL      r4, r6, r9
	FPMUL      r7, r5, r23
	FPADD      r27, r7, r4
	FPMUL      r4, r24, r31
	FPMUL      r7, r26, r22
	FPRSUB     r7, r4, r7
	FPRSUB     r25, r30, r10
	FPMUL      r4, r7, r25
	FPADD      r29, r27, r4
	ORI       r4, r0, 0
	FPGE   r10, r29, r4
	FPUN   r4, r29, r4
	BITOR        r4, r4, r10
	bneid     r4, ($BB64_212)
	ADDI      r27, r0, 1
# BB#211:                               # %for.body.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADDI      r27, r0, 0
$BB64_212:                              # %for.body.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	bneid     r27, ($BB64_214)
	ADD      r10, r29, r0
# BB#213:                               # %if.then.i.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	FPNEG      r10, r29
$BB64_214:                              # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ORI       r4, r0, 953267991
	FPLT   r4, r10, r4
	bneid     r4, ($BB64_216)
	ADDI      r10, r0, 1
# BB#215:                               # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADDI      r10, r0, 0
$BB64_216:                              # %_ZN4util4fabsERKf.exit.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	bneid     r10, ($BB64_231)
	NOP    
# BB#217:                               # %if.end.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	LWI       r4, r1, 392
	FPRSUB     r30, r30, r4
	LWI       r4, r1, 388
	FPRSUB     r12, r12, r4
	LWI       r4, r1, 384
	FPRSUB     r20, r20, r4
	FPMUL      r10, r20, r23
	FPMUL      r27, r12, r9
	FPMUL      r23, r30, r23
	FPMUL      r28, r12, r25
	FPMUL      r4, r30, r9
	FPMUL      r24, r20, r25
	FPRSUB     r9, r10, r27
	FPRSUB     r25, r28, r23
	FPRSUB     r23, r4, r24
	FPMUL      r4, r25, r22
	FPMUL      r10, r23, r31
	FPADD      r4, r10, r4
	FPMUL      r8, r9, r8
	FPADD      r4, r4, r8
	ORI       r8, r0, 1065353216
	FPDIV      r10, r8, r29
	FPMUL      r8, r4, r10
	ORI       r4, r0, 0
	FPLT   r4, r8, r4
	bneid     r4, ($BB64_219)
	ADDI      r22, r0, 1
# BB#218:                               # %if.end.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADDI      r22, r0, 0
$BB64_219:                              # %if.end.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	bneid     r22, ($BB64_231)
	NOP    
# BB#220:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	FPMUL      r4, r6, r20
	FPMUL      r5, r5, r12
	FPADD      r4, r5, r4
	LWI       r5, r1, 372
	FPMUL      r5, r25, r5
	FPMUL      r12, r23, r26
	FPMUL      r6, r7, r30
	FPADD      r6, r4, r6
	FPADD      r4, r12, r5
	LWI       r5, r1, 376
	FPMUL      r5, r9, r5
	FPADD      r4, r4, r5
	FPMUL      r5, r4, r10
	FPMUL      r6, r6, r10
	FPADD      r4, r6, r5
	ORI       r7, r0, 1065353216
	FPLE   r9, r4, r7
	ORI       r4, r0, 0
	FPGE   r10, r6, r4
	FPGE   r12, r5, r4
	ADDI      r7, r0, 0
	ADDI      r5, r0, 1
	bneid     r9, ($BB64_222)
	ADD      r6, r5, r0
# BB#221:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r6, r7, r0
$BB64_222:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	bneid     r12, ($BB64_224)
	ADD      r9, r5, r0
# BB#223:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r9, r7, r0
$BB64_224:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	bneid     r10, ($BB64_226)
	ADD      r12, r5, r0
# BB#225:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r12, r7, r0
$BB64_226:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	FPLT   r4, r8, r11
	bneid     r4, ($BB64_228)
	NOP    
# BB#227:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r5, r7, r0
$BB64_228:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	BITAND       r4, r12, r9
	BITAND       r4, r4, r6
	BITAND       r4, r5, r4
	bneid     r4, ($BB64_230)
	NOP    
# BB#229:                               # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r8, r11, r0
$BB64_230:                              # %if.end9.i.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADD      r11, r8, r0
$BB64_231:                              # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit.i.i.i
                                        #   in Loop: Header=BB64_210 Depth=3
	ADDI      r21, r21, 1
	CMP       r4, r3, r21
	bneid     r4, ($BB64_210)
	NOP    
# BB#232:                               #   in Loop: Header=BB64_166 Depth=2
	LWI       r12, r1, 396
	LWI       r28, r1, 424
	LWI       r24, r1, 428
	brid      ($BB64_307)
	LWI       r29, r1, 432
$BB64_293:                              #   in Loop: Header=BB64_166 Depth=2
	ADD      r12, r4, r0
$BB64_307:                              # %while.cond.backedge.i.i
                                        #   in Loop: Header=BB64_166 Depth=2
	bgtid     r12, ($BB64_166)
	NOP    
# BB#308:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 500
	FPDIV      r3, r3, r4
	FPGT   r4, r3, r11
	FPUN   r3, r3, r11
	BITOR        r3, r3, r4
	bneid     r3, ($BB64_310)
	ADDI      r6, r0, 1
# BB#309:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r6, r0, 0
$BB64_310:                              # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r3, r0, 0
	ADD      r5, r3, r0
	ADD      r4, r3, r0
	LWI       r23, r1, 436
	LWI       r22, r1, 440
	bneid     r6, ($BB64_312)
	LWI       r20, r1, 444
# BB#311:                               # %if.then20.i
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r3, r1, 452
	FPMUL      r3, r22, r3
	LWI       r4, r1, 448
	FPMUL      r4, r20, r4
	FPADD      r3, r4, r3
	LWI       r4, r1, 480
	FPMUL      r4, r23, r4
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPMAX     r3, r3, r4
	LWI       r8, r1, 540
	LWI        r5, r8, 20
	LWI       r6, r1, 456
	FPMUL      r5, r6, r5
	LWI        r6, r8, 16
	LWI       r7, r1, 460
	FPMUL      r7, r7, r6
	LWI        r6, r8, 12
	LWI       r8, r1, 464
	FPMUL      r6, r8, r6
	FPMUL      r6, r6, r3
	FPMUL      r7, r7, r3
	FPMUL      r3, r5, r3
	FPADD      r3, r3, r4
	FPADD      r5, r7, r4
	FPADD      r4, r6, r4
$BB64_312:                              # %do.body.i.i
                                        #   Parent Loop BB64_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	RAND      r7
	RAND      r6
	FPADD      r6, r6, r6
	ORI       r8, r0, -1082130432
	FPADD      r6, r6, r8
	FPADD      r7, r7, r7
	FPADD      r7, r7, r8
	FPMUL      r9, r7, r7
	FPMUL      r10, r6, r6
	FPADD      r8, r9, r10
	ORI       r11, r0, 1065353216
	FPGE   r11, r8, r11
	bneid     r11, ($BB64_314)
	ADDI      r8, r0, 1
# BB#313:                               # %do.body.i.i
                                        #   in Loop: Header=BB64_312 Depth=2
	ADDI      r8, r0, 0
$BB64_314:                              # %do.body.i.i
                                        #   in Loop: Header=BB64_312 Depth=2
	bneid     r8, ($BB64_312)
	NOP    
# BB#315:                               # %do.end.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r8, r0, 0
	FPGE   r11, r20, r8
	FPUN   r8, r20, r8
	BITOR        r8, r8, r11
	bneid     r8, ($BB64_317)
	ADDI      r11, r0, 1
# BB#316:                               # %do.end.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r11, r0, 0
$BB64_317:                              # %do.end.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r8, r0, 1065353216
	FPRSUB     r9, r9, r8
	FPRSUB     r9, r10, r9
	FPINVSQRT r10, r9
	bneid     r11, ($BB64_319)
	ADD      r9, r20, r0
# BB#318:                               # %if.then.i59.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPNEG      r9, r20
$BB64_319:                              # %_ZN4util4fabsERKf.exit61.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r11, r0, 0
	FPGE   r12, r22, r11
	FPUN   r11, r22, r11
	BITOR        r11, r11, r12
	bneid     r11, ($BB64_321)
	ADDI      r12, r0, 1
# BB#320:                               # %_ZN4util4fabsERKf.exit61.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r12, r0, 0
$BB64_321:                              # %_ZN4util4fabsERKf.exit61.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	bneid     r12, ($BB64_323)
	ADD      r11, r22, r0
# BB#322:                               # %if.then.i54.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPNEG      r11, r22
$BB64_323:                              # %_ZN4util4fabsERKf.exit56.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADD      r21, r20, r0
	ORI       r12, r0, 0
	FPGE   r20, r23, r12
	FPUN   r12, r23, r12
	BITOR        r12, r12, r20
	bneid     r12, ($BB64_325)
	ADDI      r20, r0, 1
# BB#324:                               # %_ZN4util4fabsERKf.exit56.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r20, r0, 0
$BB64_325:                              # %_ZN4util4fabsERKf.exit56.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADD      r12, r23, r0
	bneid     r20, ($BB64_327)
	ADD      r24, r21, r0
# BB#326:                               # %if.then.i.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPNEG      r12, r23
$BB64_327:                              # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPGE   r20, r9, r11
	FPUN   r21, r9, r11
	BITOR        r20, r21, r20
	bneid     r20, ($BB64_329)
	ADDI      r21, r0, 1
# BB#328:                               # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r21, r0, 0
$BB64_329:                              # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPDIV      r8, r8, r10
	ORI       r20, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r21, ($BB64_333)
	NOP    
# BB#330:                               # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPLT   r21, r9, r12
	bneid     r21, ($BB64_332)
	ADDI      r9, r0, 1
# BB#331:                               # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r9, r0, 0
$BB64_332:                              # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	bneid     r9, ($BB64_337)
	ADD      r21, r10, r0
$BB64_333:                              # %if.else.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPLT   r10, r11, r12
	bneid     r10, ($BB64_335)
	ADDI      r9, r0, 1
# BB#334:                               # %if.else.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r9, r0, 0
$BB64_335:                              # %if.else.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r21, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r9, ($BB64_337)
	ADD      r20, r10, r0
# BB#336:                               # %if.else19.i.i.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r21, r0, 0
	ORI       r10, r0, 1065353216
	ADD      r20, r21, r0
$BB64_337:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	FPMUL      r9, r22, r20
	FPMUL      r11, r24, r21
	FPRSUB     r9, r9, r11
	FPMUL      r11, r23, r20
	FPMUL      r12, r24, r10
	FPRSUB     r11, r12, r11
	FPMUL      r12, r23, r11
	FPMUL      r20, r22, r9
	FPRSUB     r12, r12, r20
	FPMUL      r20, r23, r21
	FPMUL      r10, r22, r10
	FPRSUB     r20, r20, r10
	FPMUL      r10, r24, r9
	FPMUL      r21, r23, r20
	FPRSUB     r21, r10, r21
	FPMUL      r10, r12, r6
	FPMUL      r12, r20, r7
	FPADD      r10, r12, r10
	FPMUL      r12, r24, r8
	FPADD      r10, r10, r12
	FPMUL      r12, r21, r6
	FPMUL      r21, r11, r7
	FPADD      r12, r21, r12
	FPMUL      r21, r22, r8
	FPADD      r12, r12, r21
	FPMUL      r20, r22, r20
	FPMUL      r21, r24, r11
	FPMUL      r11, r12, r12
	FPMUL      r22, r10, r10
	FPADD      r11, r22, r11
	FPRSUB     r20, r20, r21
	FPMUL      r6, r20, r6
	FPMUL      r7, r9, r7
	FPADD      r6, r7, r6
	FPMUL      r7, r23, r8
	FPADD      r7, r6, r7
	FPMUL      r6, r7, r7
	FPADD      r6, r11, r6
	FPINVSQRT r8, r6
	ORI       r6, r0, 1065353216
	FPDIV      r11, r6, r8
	FPDIV      r8, r10, r11
	FPDIV      r9, r12, r11
	FPMUL      r10, r9, r9
	FPMUL      r12, r8, r8
	FPADD      r10, r12, r10
	FPDIV      r7, r7, r11
	FPMUL      r11, r7, r7
	FPADD      r10, r10, r11
	FPINVSQRT r10, r10
	FPDIV      r10, r6, r10
	FPDIV      r8, r8, r10
	SWI       r8, r1, 500
	FPDIV      r9, r9, r10
	SWI       r9, r1, 504
	FPDIV      r7, r7, r10
	SWI       r7, r1, 508
	FPDIV      r10, r6, r7
	SWI       r10, r1, 512
	FPDIV      r9, r6, r9
	SWI       r9, r1, 516
	FPDIV      r6, r6, r8
	SWI       r6, r1, 520
	ORI       r7, r0, 0
	FPLT   r6, r6, r7
	FPLT   r8, r9, r7
	FPLT   r9, r10, r7
	ADDI      r7, r0, 0
	ADDI      r11, r0, 1
	bneid     r9, ($BB64_339)
	SWI       r11, r1, 480
# BB#338:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	SWI       r7, r1, 480
$BB64_339:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	bneid     r8, ($BB64_341)
	ADD      r12, r11, r0
# BB#340:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADD      r12, r7, r0
$BB64_341:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	LWI       r8, r1, 468
	LWI       r9, r1, 472
	bneid     r6, ($BB64_343)
	LWI       r10, r1, 476
# BB#342:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit.i
                                        #   in Loop: Header=BB64_1 Depth=1
	ADD      r11, r7, r0
$BB64_343:                              # %_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	ORI       r6, r0, 1203982336
	LWI       r7, r1, 368
	FPEQ   r7, r7, r6
	bneid     r7, ($BB64_345)
	ADDI      r6, r0, 1
# BB#344:                               # %_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	ADDI      r6, r0, 0
$BB64_345:                              # %_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color.exit
                                        #   in Loop: Header=BB64_1 Depth=1
	FPMUL      r5, r5, r9
	FPMUL      r3, r3, r8
	LWI       r7, r1, 484
	FPADD      r7, r7, r3
	SWI       r7, r1, 484
	LWI       r3, r1, 496
	FPADD      r3, r3, r5
	ADD      r5, r3, r0
	FPMUL      r3, r4, r10
	LWI       r4, r1, 488
	FPADD      r4, r4, r3
	SWI       r4, r1, 488
	LWI       r3, r1, 536
	SWI        r4, r3, 0
	SWI        r5, r3, 4
	SWI        r7, r3, 8
	beqid     r6, ($BB64_346)
	LWI       r4, r1, 492
$BB64_347:                              # %while.end
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 532
	.end	_ZNK5Scene8GetColorERK3RayRKi
$tmp64:
	.size	_ZNK5Scene8GetColorERK3RayRKi, ($tmp64)-_ZNK5Scene8GetColorERK3RayRKi

	.globl	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
	.align	2
	.type	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color,@function
	.ent	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color # @_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color:
	.frame	r1,376,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -376
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	ADD      r23, r10, r0
	SWI       r9, r1, 396
	LWI        r3, r8, 0
	ORI       r4, r0, 1203982336
	FPNE   r4, r3, r4
	bneid     r4, ($BB65_2)
	ADDI      r3, r0, 1
# BB#1:                                 # %entry
	ADDI      r3, r0, 0
$BB65_2:                                # %entry
	beqid     r3, ($BB65_3)
	NOP    
# BB#4:                                 # %_ZNK10syTriangle6NormalEv.exit
	SWI       r5, r1, 352
	LWI       r9, r8, 8
	LOAD      r4, r9, 0
	LOAD      r3, r9, 1
	LOAD      r21, r9, 2
	ADDI      r10, r9, 3
	LOAD      r5, r10, 0
	LOAD      r12, r10, 1
	LOAD      r20, r10, 2
	ADDI      r11, r9, 6
	LOAD      r10, r11, 0
	LOAD      r9, r11, 1
	LOAD      r11, r11, 2
	FPRSUB     r11, r11, r21
	FPRSUB     r20, r20, r21
	FPRSUB     r21, r12, r3
	FPRSUB     r9, r9, r3
	FPMUL      r3, r20, r9
	FPMUL      r12, r21, r11
	FPRSUB     r3, r3, r12
	FPRSUB     r12, r5, r4
	FPRSUB     r4, r10, r4
	FPMUL      r5, r20, r4
	FPMUL      r10, r12, r11
	FPRSUB     r5, r10, r5
	FPMUL      r10, r5, r5
	FPMUL      r11, r3, r3
	FPADD      r10, r11, r10
	FPMUL      r4, r21, r4
	FPMUL      r9, r12, r9
	FPRSUB     r4, r4, r9
	FPMUL      r9, r4, r4
	FPADD      r9, r10, r9
	FPINVSQRT r10, r9
	ORI       r9, r0, 1065353216
	FPDIV      r11, r9, r10
	FPDIV      r9, r5, r11
	LWI        r5, r7, 16
	FPMUL      r5, r9, r5
	FPDIV      r10, r3, r11
	LWI        r3, r7, 12
	FPMUL      r3, r10, r3
	FPADD      r3, r3, r5
	FPDIV      r11, r4, r11
	LWI        r4, r7, 20
	FPMUL      r4, r11, r4
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB65_6)
	ADDI      r3, r0, 1
# BB#5:                                 # %_ZNK10syTriangle6NormalEv.exit
	ADDI      r3, r0, 0
$BB65_6:                                # %_ZNK10syTriangle6NormalEv.exit
	bneid     r3, ($BB65_7)
	NOP    
# BB#8:                                 # %if.then3
	FPNEG      r11, r11
	SWI       r11, r1, 340
	FPNEG      r9, r9
	SWI       r9, r1, 344
	FPNEG      r10, r10
	brid      ($BB65_9)
	SWI       r10, r1, 348
$BB65_3:                                # %if.then
	LWI        r3, r6, 40
	SWI        r3, r5, 0
	LWI        r3, r6, 44
	SWI        r3, r5, 4
	LWI        r3, r6, 48
	brid      ($BB65_198)
	SWI        r3, r5, 8
$BB65_7:
	SWI       r10, r1, 348
	SWI       r9, r1, 344
	SWI       r11, r1, 340
$BB65_9:                                # %if.end6
	LWI       r3, r8, 4
	MULI      r3, r3, 25
	ADD      r12, r6, r0
	SWI       r12, r1, 304
	LWI       r4, r12, 32
	ADD      r3, r4, r3
	ADDI      r3, r3, 4
	LOAD      r4, r3, 0
	LOAD      r5, r3, 1
	LOAD      r3, r3, 2
	SWI        r4, r23, 0
	SWI        r5, r23, 4
	SWI        r3, r23, 8
	ORI       r4, r0, 1028443341
	LWI        r5, r8, 0
	LWI        r3, r7, 16
	FPMUL      r3, r3, r5
	LWI        r6, r7, 4
	FPADD      r3, r6, r3
	LWI        r6, r7, 12
	FPMUL      r6, r6, r5
	LWI        r8, r7, 0
	FPADD      r6, r8, r6
	LWI        r8, r12, 0
	FPRSUB     r8, r6, r8
	LWI        r9, r12, 4
	FPRSUB     r9, r3, r9
	FPMUL      r10, r9, r9
	FPMUL      r11, r8, r8
	FPADD      r11, r11, r10
	LWI        r10, r7, 20
	FPMUL      r5, r10, r5
	LWI        r7, r7, 8
	FPADD      r7, r7, r5
	LWI        r5, r12, 8
	FPRSUB     r10, r7, r5
	FPMUL      r5, r10, r10
	FPADD      r5, r11, r5
	FPINVSQRT r11, r5
	SWI       r11, r1, 372
	FPINVSQRT r11, r5
	ORI       r5, r0, 1065353216
	FPDIV      r11, r5, r11
	FPDIV      r12, r8, r11
	SWI       r12, r1, 360
	FPDIV      r21, r9, r11
	SWI       r21, r1, 364
	FPMUL      r8, r21, r21
	FPMUL      r9, r12, r12
	FPADD      r8, r9, r8
	FPDIV      r22, r10, r11
	SWI       r22, r1, 368
	FPMUL      r9, r22, r22
	FPADD      r8, r8, r9
	FPINVSQRT r8, r8
	FPDIV      r8, r5, r8
	ORI       r9, r0, 0
	FPDIV      r28, r12, r8
	FPDIV      r24, r5, r28
	FPGE   r11, r24, r9
	FPUN   r20, r24, r9
	FPDIV      r10, r21, r8
	SWI       r10, r1, 272
	FPDIV      r25, r5, r10
	SWI       r25, r1, 296
	FPGE   r10, r25, r9
	FPUN   r12, r25, r9
	BITOR        r21, r12, r10
	FPDIV      r8, r22, r8
	SWI       r8, r1, 276
	FPDIV      r12, r5, r8
	SWI       r12, r1, 300
	FPGE   r5, r12, r9
	FPUN   r8, r12, r9
	BITOR        r22, r8, r5
	FPLT   r8, r24, r9
	FPLT   r10, r25, r9
	FPLT   r12, r12, r9
	ADDI      r9, r0, 0
	ADDI      r5, r0, 1
	bneid     r22, ($BB65_11)
	SWI       r5, r1, 312
# BB#10:                                # %if.end6
	SWI       r9, r1, 312
$BB65_11:                               # %if.end6
	BITOR        r11, r20, r11
	bneid     r21, ($BB65_13)
	SWI       r5, r1, 316
# BB#12:                                # %if.end6
	SWI       r9, r1, 316
$BB65_13:                               # %if.end6
	SWI       r23, r1, 356
	bneid     r11, ($BB65_15)
	SWI       r5, r1, 320
# BB#14:                                # %if.end6
	SWI       r9, r1, 320
$BB65_15:                               # %if.end6
	bneid     r12, ($BB65_17)
	SWI       r5, r1, 324
# BB#16:                                # %if.end6
	SWI       r9, r1, 324
$BB65_17:                               # %if.end6
	bneid     r10, ($BB65_19)
	SWI       r5, r1, 328
# BB#18:                                # %if.end6
	SWI       r9, r1, 328
$BB65_19:                               # %if.end6
	LWI       r10, r1, 348
	FPMUL      r10, r10, r4
	LWI       r11, r1, 344
	FPMUL      r11, r11, r4
	LWI       r12, r1, 340
	FPMUL      r4, r12, r4
	bneid     r8, ($BB65_21)
	SWI       r5, r1, 332
# BB#20:                                # %if.end6
	SWI       r9, r1, 332
$BB65_21:                               # %if.end6
	FPADD      r4, r7, r4
	SWI       r4, r1, 280
	FPADD      r3, r3, r11
	SWI       r3, r1, 284
	FPADD      r3, r6, r10
	SWI       r3, r1, 288
	ORI       r3, r0, 1203982336
	SWI       r0, r1, 48
	ADD      r12, r24, r0
	brid      ($BB65_22)
	SWI       r12, r1, 336
$BB65_127:                              # %if.else24.i
                                        #   in Loop: Header=BB65_22 Depth=1
	beqid     r21, ($BB65_128)
	NOP    
# BB#129:                               # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r9, r11, r4
	ADDI      r7, r0, 0
	ADDI      r6, r0, 1
	bneid     r9, ($BB65_131)
	ADD      r8, r6, r0
# BB#130:                               # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r8, r7, r0
$BB65_131:                              # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r8, ($BB65_133)
	NOP    
# BB#132:                               # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r11, r4, r0
$BB65_133:                              # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGE   r4, r27, r11
	FPUN   r8, r27, r11
	BITOR        r4, r8, r4
	bneid     r4, ($BB65_135)
	NOP    
# BB#134:                               # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r6, r7, r0
$BB65_135:                              # %if.else31.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r6, ($BB65_137)
	NOP    
# BB#136:                               # %if.then35.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bslli     r4, r5, 2
	ADDI      r6, r1, 48
	ADD      r7, r6, r4
	SWI       r25, r7, -4
	LWI       r7, r1, 292
	SW        r7, r6, r4
	brid      ($BB65_161)
	ADDI      r5, r5, 1
$BB65_128:                              # %if.then28.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bslli     r4, r5, 2
	ADDI      r6, r1, 48
	ADD      r4, r6, r4
	brid      ($BB65_161)
	SWI       r25, r4, -4
$BB65_137:                              # %if.else40.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bslli     r4, r5, 2
	ADDI      r6, r1, 48
	ADD      r7, r6, r4
	LWI       r8, r1, 292
	SWI       r8, r7, -4
	SW        r25, r6, r4
	brid      ($BB65_161)
	ADDI      r5, r5, 1
$BB65_22:                               # %while.body.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_138 Depth 2
	ADDI      r4, r0, -1
	ADD      r24, r5, r4
	bslli     r4, r24, 2
	ADDI      r6, r1, 48
	LW        r4, r6, r4
	bslli     r4, r4, 3
	LWI       r6, r1, 304
	LWI       r6, r6, 36
	ADD      r4, r4, r6
	SWI       r0, r1, 188
	SWI       r0, r1, 184
	SWI       r0, r1, 180
	SWI       r0, r1, 176
	LOAD      r6, r4, 0
	SWI        r6, r1, 176
	LOAD      r6, r4, 1
	SWI        r6, r1, 180
	LOAD      r6, r4, 2
	SWI        r6, r1, 184
	ADDI      r6, r4, 3
	LOAD      r7, r6, 0
	SWI        r7, r1, 188
	LOAD      r7, r6, 1
	SWI        r7, r1, 192
	LOAD      r6, r6, 2
	SWI        r6, r1, 196
	LWI       r6, r1, 316
	MULI      r26, r6, 12
	ADDI      r8, r1, 176
	ADD      r6, r8, r26
	ADDI      r7, r4, 7
	ADDI      r4, r4, 6
	LWI       r9, r1, 332
	MULI      r11, r9, 12
	LOAD      r21, r4, 0
	SWI       r21, r1, 200
	LOAD      r4, r7, 0
	SWI       r4, r1, 292
	SWI       r4, r1, 204
	LWI        r4, r6, 4
	LWI       r6, r1, 284
	FPRSUB     r4, r6, r4
	LWI       r6, r1, 296
	FPMUL      r6, r4, r6
	LW         r4, r8, r11
	LWI       r7, r1, 288
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r12
	ADDI      r4, r0, 1
	FPGT   r9, r7, r6
	bneid     r9, ($BB65_24)
	NOP    
# BB#23:                                # %while.body.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_24:                               # %while.body.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_25)
	NOP    
# BB#26:                                # %while.body.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI       r4, r1, 328
	MULI      r30, r4, 12
	ADD      r4, r8, r30
	LWI        r4, r4, 4
	LWI       r9, r1, 284
	FPRSUB     r4, r9, r4
	LWI       r9, r1, 296
	FPMUL      r22, r4, r9
	LWI       r4, r1, 320
	MULI      r23, r4, 12
	LW         r4, r8, r23
	LWI       r8, r1, 288
	FPRSUB     r4, r8, r4
	FPMUL      r20, r4, r12
	FPGT   r8, r22, r20
	bneid     r8, ($BB65_28)
	ADDI      r4, r0, 1
# BB#27:                                # %while.body.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_28:                               # %while.body.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_29)
	NOP    
# BB#30:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r9, r22, r7
	ADDI      r25, r0, 0
	ADDI      r8, r0, 1
	bneid     r9, ($BB65_32)
	ADD      r4, r8, r0
# BB#31:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r25, r0
$BB65_32:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_34)
	NOP    
# BB#33:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r22, r7, r0
$BB65_34:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI       r4, r1, 312
	MULI      r31, r4, 12
	ADDI      r4, r1, 176
	ADD      r7, r4, r31
	FPLT   r9, r6, r20
	bneid     r9, ($BB65_36)
	ADD      r27, r8, r0
# BB#35:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r27, r25, r0
$BB65_36:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI        r7, r7, 8
	LWI       r9, r1, 280
	FPRSUB     r7, r9, r7
	LWI       r9, r1, 300
	FPMUL      r7, r7, r9
	FPGT   r9, r22, r7
	bneid     r9, ($BB65_38)
	NOP    
# BB#37:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r8, r25, r0
$BB65_38:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r27, ($BB65_40)
	NOP    
# BB#39:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r6, r20, r0
$BB65_40:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r8, ($BB65_41)
	NOP    
# BB#42:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI       r8, r1, 324
	MULI      r20, r8, 12
	ADD      r4, r4, r20
	LWI        r4, r4, 8
	LWI       r8, r1, 280
	FPRSUB     r4, r8, r4
	LWI       r8, r1, 300
	FPMUL      r25, r4, r8
	FPGT   r8, r25, r6
	bneid     r8, ($BB65_44)
	ADDI      r4, r0, 1
# BB#43:                                # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_44:                               # %if.end.i.i133.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_45)
	NOP    
# BB#46:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPLT   r10, r7, r6
	ADDI      r8, r0, 0
	ADDI      r4, r0, 1
	bneid     r10, ($BB65_48)
	ADD      r9, r4, r0
# BB#47:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r9, r8, r0
$BB65_48:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r9, ($BB65_50)
	NOP    
# BB#49:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r6, r0
$BB65_50:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ORI       r6, r0, 0
	FPLT   r6, r7, r6
	bneid     r6, ($BB65_52)
	NOP    
# BB#51:                                # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r8, r0
$BB65_52:                               # %if.end72.i.i137.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_53)
	NOP    
# BB#54:                                # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r8, r25, r22
	ADDI      r6, r0, 0
	ADDI      r4, r0, 1
	bneid     r8, ($BB65_56)
	ADD      r7, r4, r0
# BB#55:                                # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r6, r0
$BB65_56:                               # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r7, ($BB65_58)
	NOP    
# BB#57:                                # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r25, r22, r0
$BB65_58:                               # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPLT   r7, r3, r25
	bneid     r7, ($BB65_60)
	NOP    
# BB#59:                                # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r6, r0
$BB65_60:                               # %if.end.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_61)
	NOP    
# BB#62:                                # %if.end7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	beqid     r21, ($BB65_63)
	NOP    
# BB#64:                                # %if.end7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	SWI       r24, r1, 308
	ADD      r22, r0, r0
	ADDI      r4, r0, -1
	CMP       r4, r4, r21
	bneid     r4, ($BB65_138)
	NOP    
# BB#65:                                # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI       r4, r1, 292
	bslli     r4, r4, 3
	LWI       r6, r1, 304
	LWI       r6, r6, 36
	ADD      r4, r6, r4
	SWI       r0, r1, 220
	SWI       r0, r1, 216
	SWI       r0, r1, 212
	SWI       r0, r1, 208
	LOAD      r6, r4, 0
	SWI        r6, r1, 208
	LOAD      r6, r4, 1
	SWI        r6, r1, 212
	LOAD      r6, r4, 2
	SWI        r6, r1, 216
	ADDI      r6, r4, 3
	LOAD      r7, r6, 0
	SWI        r7, r1, 220
	LOAD      r7, r6, 1
	SWI        r7, r1, 224
	LOAD      r6, r6, 2
	SWI        r6, r1, 228
	ADDI      r6, r4, 6
	LOAD      r6, r6, 0
	SWI       r6, r1, 232
	ADDI      r4, r4, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 236
	ADDI      r4, r1, 208
	ADD      r6, r4, r26
	LWI        r6, r6, 4
	LWI       r7, r1, 284
	FPRSUB     r6, r7, r6
	LWI       r7, r1, 296
	FPMUL      r29, r6, r7
	LW         r6, r4, r11
	LWI       r7, r1, 288
	FPRSUB     r6, r7, r6
	FPMUL      r6, r6, r12
	ADDI      r7, r0, 1
	FPGT   r8, r6, r29
	bneid     r8, ($BB65_67)
	NOP    
# BB#66:                                # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r7, r0, 0
$BB65_67:                               # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r21, r0, r0
	ORI       r27, r0, 1203982336
	bneid     r7, ($BB65_97)
	NOP    
# BB#68:                                # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r4, r30
	LW         r4, r4, r23
	LWI        r7, r7, 4
	LWI       r8, r1, 284
	FPRSUB     r7, r8, r7
	LWI       r8, r1, 296
	FPMUL      r25, r7, r8
	LWI       r7, r1, 288
	FPRSUB     r4, r7, r4
	FPMUL      r22, r4, r12
	FPGT   r7, r25, r22
	bneid     r7, ($BB65_70)
	ADDI      r4, r0, 1
# BB#69:                                # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_70:                               # %if.then9.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_97)
	NOP    
# BB#71:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r9, r25, r6
	ADDI      r8, r0, 0
	ADDI      r7, r0, 1
	bneid     r9, ($BB65_73)
	ADD      r4, r7, r0
# BB#72:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r8, r0
$BB65_73:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_75)
	NOP    
# BB#74:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r25, r6, r0
$BB65_75:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r1, 208
	ADD      r6, r4, r31
	FPLT   r9, r29, r22
	bneid     r9, ($BB65_77)
	ADD      r21, r7, r0
# BB#76:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r21, r8, r0
$BB65_77:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI        r6, r6, 8
	LWI       r9, r1, 280
	FPRSUB     r6, r9, r6
	LWI       r9, r1, 300
	FPMUL      r6, r6, r9
	FPGT   r9, r25, r6
	bneid     r9, ($BB65_79)
	NOP    
# BB#78:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r8, r0
$BB65_79:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r21, ($BB65_81)
	NOP    
# BB#80:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r29, r22, r0
$BB65_81:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r21, r0, r0
	ORI       r27, r0, 1203982336
	bneid     r7, ($BB65_97)
	NOP    
# BB#82:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r4, r20
	LWI        r4, r4, 8
	LWI       r7, r1, 280
	FPRSUB     r4, r7, r4
	LWI       r7, r1, 300
	FPMUL      r22, r4, r7
	FPGT   r7, r22, r29
	bneid     r7, ($BB65_84)
	ADDI      r4, r0, 1
# BB#83:                                # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_84:                               # %if.end.i.i59.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_97)
	NOP    
# BB#85:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPLT   r9, r6, r29
	ADDI      r7, r0, 0
	ADDI      r4, r0, 1
	bneid     r9, ($BB65_87)
	ADD      r8, r4, r0
# BB#86:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r8, r7, r0
$BB65_87:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r8, ($BB65_89)
	NOP    
# BB#88:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r6, r29, r0
$BB65_89:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ORI       r8, r0, 0
	FPLT   r6, r6, r8
	bneid     r6, ($BB65_91)
	NOP    
# BB#90:                                # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r7, r0
$BB65_91:                               # %if.end72.i.i63.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r21, r0, r0
	ORI       r27, r0, 1203982336
	bneid     r4, ($BB65_97)
	NOP    
# BB#92:                                # %if.end81.i.i67.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r6, r22, r25
	ADDI      r21, r0, 1
	bneid     r6, ($BB65_94)
	ADD      r4, r21, r0
# BB#93:                                # %if.end81.i.i67.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_94:                               # %if.end81.i.i67.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_96)
	NOP    
# BB#95:                                # %if.end81.i.i67.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r22, r25, r0
$BB65_96:                               # %if.end81.i.i67.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r27, r22, r0
$BB65_97:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI       r4, r1, 304
	LWI       r4, r4, 36
	LWI       r6, r1, 292
	ADDI      r25, r6, 1
	bslli     r6, r25, 3
	ADD      r4, r4, r6
	SWI       r0, r1, 252
	SWI       r0, r1, 248
	SWI       r0, r1, 244
	SWI       r0, r1, 240
	LOAD      r6, r4, 0
	SWI        r6, r1, 240
	LOAD      r6, r4, 1
	SWI        r6, r1, 244
	LOAD      r6, r4, 2
	SWI        r6, r1, 248
	ADDI      r6, r4, 3
	LOAD      r7, r6, 0
	SWI        r7, r1, 252
	LOAD      r7, r6, 1
	SWI        r7, r1, 256
	LOAD      r6, r6, 2
	SWI        r6, r1, 260
	ADDI      r6, r4, 6
	LOAD      r6, r6, 0
	SWI       r6, r1, 264
	ADDI      r4, r4, 7
	LOAD      r4, r4, 0
	SWI       r4, r1, 268
	ADDI      r8, r1, 240
	ADD      r4, r8, r26
	LWI        r4, r4, 4
	LWI       r6, r1, 284
	FPRSUB     r4, r6, r4
	LWI       r6, r1, 296
	FPMUL      r6, r4, r6
	LW         r4, r8, r11
	LWI       r7, r1, 288
	FPRSUB     r4, r7, r4
	FPMUL      r7, r4, r12
	ADDI      r4, r0, 1
	FPGT   r9, r7, r6
	bneid     r9, ($BB65_99)
	LWI       r24, r1, 308
# BB#98:                                # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 0
$BB65_99:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r4, ($BB65_124)
	NOP    
# BB#100:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r8, r30
	LWI        r4, r4, 4
	LWI       r9, r1, 284
	FPRSUB     r4, r9, r4
	LWI       r9, r1, 296
	FPMUL      r4, r4, r9
	LW         r8, r8, r23
	LWI       r9, r1, 288
	FPRSUB     r8, r9, r8
	FPMUL      r11, r8, r12
	FPGT   r9, r4, r11
	bneid     r9, ($BB65_102)
	ADDI      r8, r0, 1
# BB#101:                               # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r8, r0, 0
$BB65_102:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit69.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r8, ($BB65_124)
	NOP    
# BB#103:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPGT   r10, r4, r7
	ADDI      r23, r0, 0
	ADDI      r8, r0, 1
	bneid     r10, ($BB65_105)
	ADD      r9, r8, r0
# BB#104:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r9, r23, r0
$BB65_105:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r9, ($BB65_107)
	NOP    
# BB#106:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r4, r7, r0
$BB65_107:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r7, r1, 240
	ADD      r22, r7, r31
	FPLT   r9, r6, r11
	bneid     r9, ($BB65_109)
	ADD      r29, r8, r0
# BB#108:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r29, r23, r0
$BB65_109:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	LWI        r9, r22, 8
	LWI       r10, r1, 280
	FPRSUB     r9, r10, r9
	LWI       r10, r1, 300
	FPMUL      r22, r9, r10
	FPGT   r9, r4, r22
	bneid     r9, ($BB65_111)
	NOP    
# BB#110:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r8, r23, r0
$BB65_111:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r29, ($BB65_113)
	NOP    
# BB#112:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r6, r11, r0
$BB65_113:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r8, ($BB65_124)
	NOP    
# BB#114:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r7, r20
	LWI        r7, r7, 8
	LWI       r8, r1, 280
	FPRSUB     r7, r8, r7
	LWI       r8, r1, 300
	FPMUL      r11, r7, r8
	FPGT   r8, r11, r6
	bneid     r8, ($BB65_116)
	ADDI      r7, r0, 1
# BB#115:                               # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r7, r0, 0
$BB65_116:                              # %if.end.i.i7.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r7, ($BB65_124)
	NOP    
# BB#117:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	FPLT   r10, r22, r6
	ADDI      r8, r0, 0
	ADDI      r7, r0, 1
	bneid     r10, ($BB65_119)
	ADD      r9, r7, r0
# BB#118:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r9, r8, r0
$BB65_119:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r9, ($BB65_121)
	NOP    
# BB#120:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r22, r6, r0
$BB65_121:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ORI       r6, r0, 0
	FPGE   r9, r22, r6
	FPUN   r6, r22, r6
	BITOR        r6, r6, r9
	bneid     r6, ($BB65_123)
	NOP    
# BB#122:                               # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADD      r7, r8, r0
$BB65_123:                              # %if.end72.i.i.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bneid     r7, ($BB65_127)
	NOP    
$BB65_124:                              # %_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist.exit.i
                                        #   in Loop: Header=BB65_22 Depth=1
	ADDI      r4, r0, 1
	CMP       r4, r4, r21
	bneid     r4, ($BB65_125)
	NOP    
# BB#126:                               # %if.then21.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bslli     r4, r5, 2
	ADDI      r6, r1, 48
	ADD      r4, r6, r4
	LWI       r6, r1, 292
	brid      ($BB65_161)
	SWI       r6, r4, -4
$BB65_25:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_29:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_41:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_138:                              # %for.body.i.i
                                        #   Parent Loop BB65_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	MULI      r4, r22, 11
	LWI       r5, r1, 292
	ADD      r5, r4, r5
	LOAD      r20, r5, 0
	LOAD      r23, r5, 1
	LOAD      r30, r5, 2
	ADDI      r4, r5, 6
	ADDI      r5, r5, 3
	LOAD      r6, r5, 0
	LOAD      r27, r5, 1
	LOAD      r8, r5, 2
	LOAD      r5, r4, 0
	LOAD      r7, r4, 1
	LOAD      r4, r4, 2
	FPRSUB     r11, r30, r4
	FPRSUB     r25, r23, r7
	LWI       r9, r1, 276
	FPMUL      r4, r9, r25
	LWI       r10, r1, 272
	FPMUL      r7, r10, r11
	FPRSUB     r4, r4, r7
	FPRSUB     r31, r20, r5
	FPMUL      r5, r9, r31
	FPMUL      r7, r28, r11
	FPRSUB     r5, r7, r5
	FPRSUB     r7, r20, r6
	FPRSUB     r29, r23, r27
	FPMUL      r6, r5, r29
	FPMUL      r9, r4, r7
	FPADD      r9, r9, r6
	FPMUL      r6, r10, r31
	FPMUL      r10, r28, r25
	FPRSUB     r6, r6, r10
	FPRSUB     r8, r30, r8
	FPMUL      r10, r6, r8
	FPADD      r9, r9, r10
	ORI       r10, r0, 0
	FPGE   r12, r9, r10
	FPUN   r10, r9, r10
	BITOR        r10, r10, r12
	bneid     r10, ($BB65_140)
	ADDI      r12, r0, 1
# BB#139:                               # %for.body.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADDI      r12, r0, 0
$BB65_140:                              # %for.body.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	bneid     r12, ($BB65_142)
	ADD      r27, r9, r0
# BB#141:                               # %if.then.i.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	FPNEG      r27, r9
$BB65_142:                              # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ORI       r10, r0, 953267991
	FPLT   r10, r27, r10
	bneid     r10, ($BB65_144)
	ADDI      r12, r0, 1
# BB#143:                               # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADDI      r12, r0, 0
$BB65_144:                              # %_ZN4util4fabsERKf.exit.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	bneid     r12, ($BB65_159)
	NOP    
# BB#145:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	LWI       r10, r1, 280
	FPRSUB     r30, r30, r10
	LWI       r10, r1, 288
	FPRSUB     r20, r20, r10
	LWI       r10, r1, 284
	FPRSUB     r23, r23, r10
	FPMUL      r12, r23, r7
	FPMUL      r27, r20, r29
	FPMUL      r10, r30, r7
	FPMUL      r24, r20, r8
	FPMUL      r26, r30, r29
	FPMUL      r8, r23, r8
	FPRSUB     r7, r12, r27
	FPRSUB     r29, r24, r10
	FPRSUB     r8, r26, r8
	FPMUL      r10, r29, r25
	FPMUL      r12, r8, r31
	FPADD      r10, r12, r10
	FPMUL      r11, r7, r11
	FPADD      r10, r10, r11
	ORI       r11, r0, 1065353216
	FPDIV      r25, r11, r9
	FPMUL      r11, r10, r25
	ORI       r9, r0, 0
	FPLT   r10, r11, r9
	bneid     r10, ($BB65_147)
	ADDI      r9, r0, 1
# BB#146:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADDI      r9, r0, 0
$BB65_147:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	bneid     r9, ($BB65_159)
	NOP    
# BB#148:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	FPMUL      r5, r5, r23
	FPMUL      r4, r4, r20
	FPADD      r4, r4, r5
	LWI       r5, r1, 272
	FPMUL      r9, r29, r5
	FPMUL      r8, r8, r28
	FPMUL      r5, r6, r30
	FPADD      r5, r4, r5
	FPADD      r4, r8, r9
	LWI       r6, r1, 276
	FPMUL      r6, r7, r6
	FPADD      r4, r4, r6
	FPMUL      r4, r4, r25
	FPMUL      r5, r5, r25
	FPADD      r6, r5, r4
	ORI       r7, r0, 1065353216
	FPLE   r7, r6, r7
	ORI       r6, r0, 0
	FPGE   r8, r5, r6
	FPGE   r20, r4, r6
	ADDI      r6, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB65_150)
	ADD      r5, r4, r0
# BB#149:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r5, r6, r0
$BB65_150:                              # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	bneid     r20, ($BB65_152)
	ADD      r7, r4, r0
# BB#151:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r7, r6, r0
$BB65_152:                              # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	bneid     r8, ($BB65_154)
	ADD      r20, r4, r0
# BB#153:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r20, r6, r0
$BB65_154:                              # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	FPLT   r8, r11, r3
	bneid     r8, ($BB65_156)
	NOP    
# BB#155:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r4, r6, r0
$BB65_156:                              # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	BITAND       r6, r20, r7
	BITAND       r5, r6, r5
	BITAND       r4, r4, r5
	bneid     r4, ($BB65_158)
	NOP    
# BB#157:                               # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r11, r3, r0
$BB65_158:                              # %if.end9.i.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADD      r3, r11, r0
$BB65_159:                              # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit.i.i
                                        #   in Loop: Header=BB65_138 Depth=2
	ADDI      r22, r22, 1
	CMP       r4, r21, r22
	bneid     r4, ($BB65_138)
	NOP    
# BB#160:                               #   in Loop: Header=BB65_22 Depth=1
	LWI       r5, r1, 308
	brid      ($BB65_161)
	LWI       r12, r1, 336
$BB65_45:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_53:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_61:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_63:                               #   in Loop: Header=BB65_22 Depth=1
	brid      ($BB65_161)
	ADD      r5, r24, r0
$BB65_125:                              #   in Loop: Header=BB65_22 Depth=1
	ADD      r5, r24, r0
$BB65_161:                              # %while.cond.backedge.i
                                        #   in Loop: Header=BB65_22 Depth=1
	bgtid     r5, ($BB65_22)
	NOP    
# BB#162:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit
	ORI       r4, r0, 1065353216
	LWI       r5, r1, 372
	FPDIV      r4, r4, r5
	FPGT   r5, r4, r3
	FPUN   r3, r4, r3
	BITOR        r3, r3, r5
	ADDI      r6, r0, 1
	LWI       r23, r1, 340
	bneid     r3, ($BB65_164)
	LWI       r24, r1, 344
# BB#163:                               # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit
	ADDI      r6, r0, 0
$BB65_164:                              # %_ZNK5Scene14TraceShadowRayERK3RayR7HitDist.exit
	ORI       r3, r0, 0
	ADD      r4, r3, r0
	ADD      r5, r3, r0
	bneid     r6, ($BB65_166)
	LWI       r25, r1, 348
# BB#165:                               # %if.then20
	LWI       r3, r1, 364
	FPMUL      r3, r24, r3
	LWI       r4, r1, 360
	FPMUL      r4, r25, r4
	FPADD      r3, r4, r3
	LWI       r4, r1, 368
	FPMUL      r4, r23, r4
	FPADD      r3, r3, r4
	ORI       r5, r0, 0
	FPMAX     r3, r3, r5
	LWI       r8, r1, 304
	LWI        r4, r8, 20
	LWI       r9, r1, 356
	LWI        r6, r9, 8
	FPMUL      r4, r6, r4
	LWI        r6, r8, 16
	LWI        r7, r9, 4
	FPMUL      r6, r7, r6
	LWI        r7, r8, 12
	LWI        r8, r9, 0
	FPMUL      r7, r8, r7
	FPMUL      r7, r7, r3
	FPMUL      r6, r6, r3
	FPMUL      r3, r4, r3
	FPADD      r3, r3, r5
	FPADD      r4, r6, r5
	FPADD      r5, r7, r5
$BB65_166:                              # %do.body.i
                                        # =>This Inner Loop Header: Depth=1
	RAND      r7
	RAND      r6
	FPADD      r6, r6, r6
	ORI       r8, r0, -1082130432
	FPADD      r6, r6, r8
	FPADD      r7, r7, r7
	FPADD      r9, r7, r8
	FPMUL      r7, r9, r9
	FPMUL      r8, r6, r6
	FPADD      r10, r7, r8
	ORI       r11, r0, 1065353216
	FPGE   r11, r10, r11
	bneid     r11, ($BB65_168)
	ADDI      r10, r0, 1
# BB#167:                               # %do.body.i
                                        #   in Loop: Header=BB65_166 Depth=1
	ADDI      r10, r0, 0
$BB65_168:                              # %do.body.i
                                        #   in Loop: Header=BB65_166 Depth=1
	bneid     r10, ($BB65_166)
	NOP    
# BB#169:                               # %do.end.i
	ORI       r10, r0, 0
	FPGE   r11, r25, r10
	FPUN   r10, r25, r10
	BITOR        r10, r10, r11
	bneid     r10, ($BB65_171)
	ADDI      r21, r0, 1
# BB#170:                               # %do.end.i
	ADDI      r21, r0, 0
$BB65_171:                              # %do.end.i
	ORI       r10, r0, 1065353216
	FPRSUB     r7, r7, r10
	FPRSUB     r7, r8, r7
	FPINVSQRT r20, r7
	bneid     r21, ($BB65_173)
	ADD      r11, r25, r0
# BB#172:                               # %if.then.i59.i.i
	FPNEG      r11, r25
$BB65_173:                              # %_ZN4util4fabsERKf.exit61.i.i
	ORI       r7, r0, 0
	FPGE   r8, r24, r7
	FPUN   r7, r24, r7
	BITOR        r8, r7, r8
	bneid     r8, ($BB65_175)
	ADDI      r7, r0, 1
# BB#174:                               # %_ZN4util4fabsERKf.exit61.i.i
	ADDI      r7, r0, 0
$BB65_175:                              # %_ZN4util4fabsERKf.exit61.i.i
	bneid     r7, ($BB65_177)
	ADD      r21, r24, r0
# BB#176:                               # %if.then.i54.i.i
	FPNEG      r21, r24
$BB65_177:                              # %_ZN4util4fabsERKf.exit56.i.i
	ORI       r7, r0, 0
	FPGE   r8, r23, r7
	FPUN   r7, r23, r7
	BITOR        r8, r7, r8
	bneid     r8, ($BB65_179)
	ADDI      r7, r0, 1
# BB#178:                               # %_ZN4util4fabsERKf.exit56.i.i
	ADDI      r7, r0, 0
$BB65_179:                              # %_ZN4util4fabsERKf.exit56.i.i
	bneid     r7, ($BB65_181)
	ADD      r8, r23, r0
# BB#180:                               # %if.then.i.i.i
	FPNEG      r8, r23
$BB65_181:                              # %_ZN4util4fabsERKf.exit.i.i
	FPGE   r7, r11, r21
	FPUN   r12, r11, r21
	BITOR        r7, r12, r7
	bneid     r7, ($BB65_183)
	ADDI      r22, r0, 1
# BB#182:                               # %_ZN4util4fabsERKf.exit.i.i
	ADDI      r22, r0, 0
$BB65_183:                              # %_ZN4util4fabsERKf.exit.i.i
	FPDIV      r10, r10, r20
	ORI       r20, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r22, ($BB65_187)
	NOP    
# BB#184:                               # %_ZN4util4fabsERKf.exit.i.i
	FPLT   r11, r11, r8
	bneid     r11, ($BB65_186)
	ADDI      r22, r0, 1
# BB#185:                               # %_ZN4util4fabsERKf.exit.i.i
	ADDI      r22, r0, 0
$BB65_186:                              # %_ZN4util4fabsERKf.exit.i.i
	bneid     r22, ($BB65_191)
	ADD      r11, r7, r0
$BB65_187:                              # %if.else.i.i
	FPLT   r7, r21, r8
	bneid     r7, ($BB65_189)
	ADDI      r8, r0, 1
# BB#188:                               # %if.else.i.i
	ADDI      r8, r0, 0
$BB65_189:                              # %if.else.i.i
	ORI       r11, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r8, ($BB65_191)
	ADD      r20, r7, r0
# BB#190:                               # %if.else19.i.i
	ORI       r11, r0, 0
	ORI       r7, r0, 1065353216
	ADD      r20, r11, r0
$BB65_191:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	FPMUL      r8, r24, r20
	FPMUL      r12, r25, r11
	FPRSUB     r8, r8, r12
	FPMUL      r12, r23, r20
	FPMUL      r20, r25, r7
	FPRSUB     r20, r20, r12
	FPMUL      r12, r23, r20
	FPMUL      r21, r24, r8
	FPRSUB     r12, r12, r21
	FPMUL      r11, r23, r11
	FPMUL      r7, r24, r7
	FPRSUB     r21, r11, r7
	FPMUL      r7, r25, r8
	FPMUL      r11, r23, r21
	FPRSUB     r11, r7, r11
	FPMUL      r7, r12, r6
	FPMUL      r12, r21, r9
	FPADD      r7, r12, r7
	FPMUL      r12, r25, r10
	FPADD      r7, r7, r12
	FPMUL      r11, r11, r6
	FPMUL      r12, r20, r9
	FPADD      r11, r12, r11
	FPMUL      r12, r24, r10
	FPADD      r11, r11, r12
	FPMUL      r12, r24, r21
	FPMUL      r21, r25, r20
	FPMUL      r20, r11, r11
	FPMUL      r22, r7, r7
	FPADD      r20, r22, r20
	FPRSUB     r12, r12, r21
	FPMUL      r6, r12, r6
	FPMUL      r8, r8, r9
	FPADD      r6, r8, r6
	FPMUL      r8, r23, r10
	FPADD      r6, r6, r8
	FPMUL      r8, r6, r6
	FPADD      r8, r20, r8
	FPINVSQRT r8, r8
	ORI       r20, r0, 1065353216
	FPDIV      r9, r20, r8
	FPDIV      r7, r7, r9
	FPDIV      r8, r11, r9
	FPMUL      r10, r8, r8
	FPMUL      r11, r7, r7
	FPADD      r10, r11, r10
	FPDIV      r6, r6, r9
	FPMUL      r9, r6, r6
	FPADD      r9, r10, r9
	FPINVSQRT r9, r9
	FPDIV      r9, r20, r9
	FPDIV      r7, r7, r9
	FPDIV      r8, r8, r9
	FPDIV      r9, r6, r9
	FPDIV      r6, r20, r9
	FPDIV      r10, r20, r8
	FPDIV      r11, r20, r7
	ORI       r12, r0, 0
	FPLT   r23, r11, r12
	FPLT   r25, r10, r12
	FPLT   r12, r6, r12
	ADDI      r24, r0, 0
	ADDI      r21, r0, 1
	bneid     r12, ($BB65_193)
	ADD      r20, r21, r0
# BB#192:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	ADD      r20, r24, r0
$BB65_193:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	bneid     r25, ($BB65_195)
	ADD      r22, r21, r0
# BB#194:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	ADD      r22, r24, r0
$BB65_195:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	bneid     r23, ($BB65_197)
	NOP    
# BB#196:                               # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	ADD      r21, r24, r0
$BB65_197:                              # %_ZNK5Scene7HemiRayERK8syVectorS2_.exit
	LWI       r12, r1, 396
	LWI       r23, r1, 288
	SWI        r23, r12, 0
	LWI       r23, r1, 284
	SWI        r23, r12, 4
	LWI       r23, r1, 280
	SWI        r23, r12, 8
	SWI        r7, r12, 12
	SWI        r8, r12, 16
	SWI        r9, r12, 20
	SWI        r11, r12, 24
	SWI        r10, r12, 28
	SWI        r6, r12, 32
	SWI       r21, r12, 36
	SWI       r22, r12, 40
	SWI       r20, r12, 44
	LWI       r6, r1, 352
	SWI        r5, r6, 0
	SWI        r4, r6, 4
	SWI        r3, r6, 8
$BB65_198:                              # %return
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 376
	.end	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color
$tmp65:
	.size	_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color, ($tmp65)-_ZNK5Scene5ShadeERK3RayRK9HitRecordRS0_R5Color

	.globl	_ZN5syBoxC2Ev
	.align	2
	.type	_ZN5syBoxC2Ev,@function
	.ent	_ZN5syBoxC2Ev           # @_ZN5syBoxC2Ev
_ZN5syBoxC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r0, r5, 20
	SWI       r0, r5, 16
	SWI       r0, r5, 12
	SWI       r0, r5, 8
	SWI       r0, r5, 4
	rtsd      r15, 8
	SWI       r0, r5, 0
	.end	_ZN5syBoxC2Ev
$tmp66:
	.size	_ZN5syBoxC2Ev, ($tmp66)-_ZN5syBoxC2Ev

	.globl	_ZN5syBox3SetEv
	.align	2
	.type	_ZN5syBox3SetEv,@function
	.ent	_ZN5syBox3SetEv         # @_ZN5syBox3SetEv
_ZN5syBox3SetEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r0, r5, 20
	SWI       r0, r5, 16
	SWI       r0, r5, 12
	SWI       r0, r5, 8
	SWI       r0, r5, 4
	rtsd      r15, 8
	SWI       r0, r5, 0
	.end	_ZN5syBox3SetEv
$tmp67:
	.size	_ZN5syBox3SetEv, ($tmp67)-_ZN5syBox3SetEv

	.globl	_ZN5syBoxC2ERK8syVectorS2_
	.align	2
	.type	_ZN5syBoxC2ERK8syVectorS2_,@function
	.ent	_ZN5syBoxC2ERK8syVectorS2_ # @_ZN5syBoxC2ERK8syVectorS2_
_ZN5syBoxC2ERK8syVectorS2_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r7, 0
	SWI        r3, r5, 12
	LWI        r3, r7, 4
	SWI        r3, r5, 16
	LWI        r3, r7, 8
	rtsd      r15, 8
	SWI        r3, r5, 20
	.end	_ZN5syBoxC2ERK8syVectorS2_
$tmp68:
	.size	_ZN5syBoxC2ERK8syVectorS2_, ($tmp68)-_ZN5syBoxC2ERK8syVectorS2_

	.globl	_ZN5syBox3SetERK8syVectorS2_
	.align	2
	.type	_ZN5syBox3SetERK8syVectorS2_,@function
	.ent	_ZN5syBox3SetERK8syVectorS2_ # @_ZN5syBox3SetERK8syVectorS2_
_ZN5syBox3SetERK8syVectorS2_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r7, 0
	SWI        r3, r5, 12
	LWI        r3, r7, 4
	SWI        r3, r5, 16
	LWI        r3, r7, 8
	rtsd      r15, 8
	SWI        r3, r5, 20
	.end	_ZN5syBox3SetERK8syVectorS2_
$tmp69:
	.size	_ZN5syBox3SetERK8syVectorS2_, ($tmp69)-_ZN5syBox3SetERK8syVectorS2_

	.globl	_ZN5syBox14LoadFromMemoryERKi
	.align	2
	.type	_ZN5syBox14LoadFromMemoryERKi,@function
	.ent	_ZN5syBox14LoadFromMemoryERKi # @_ZN5syBox14LoadFromMemoryERKi
_ZN5syBox14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r6, 0
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 20
	.end	_ZN5syBox14LoadFromMemoryERKi
$tmp70:
	.size	_ZN5syBox14LoadFromMemoryERKi, ($tmp70)-_ZN5syBox14LoadFromMemoryERKi

	.globl	_ZNK5syBox11IntersectsWERK3RayR7HitDist
	.align	2
	.type	_ZNK5syBox11IntersectsWERK3RayR7HitDist,@function
	.ent	_ZNK5syBox11IntersectsWERK3RayR7HitDist # @_ZNK5syBox11IntersectsWERK3RayR7HitDist
_ZNK5syBox11IntersectsWERK3RayR7HitDist:
	.frame	r1,12,r15
	.mask	0x700000
# BB#0:                                 # %entry
	ADDI      r1, r1, -12
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	SWI       r22, r1, 0
	LWI       r4, r6, 40
	RSUBI     r3, r4, 1
	MULI      r3, r3, 12
	ADD      r3, r5, r3
	LWI       r9, r6, 36
	MULI      r8, r9, 12
	LW         r8, r5, r8
	LWI        r11, r6, 0
	FPRSUB     r8, r11, r8
	LWI        r12, r6, 24
	FPMUL      r10, r8, r12
	LWI        r3, r3, 4
	LWI        r20, r6, 4
	FPRSUB     r3, r20, r3
	LWI        r21, r6, 28
	FPMUL      r8, r3, r21
	ADDI      r22, r0, 1
	FPGT   r3, r10, r8
	bneid     r3, ($BB71_2)
	NOP    
# BB#1:                                 # %entry
	ADDI      r22, r0, 0
$BB71_2:                                # %entry
	bneid     r22, ($BB71_32)
	ADD      r3, r0, r0
# BB#3:                                 # %entry
	MULI      r4, r4, 12
	ADD      r4, r5, r4
	LWI        r4, r4, 4
	FPRSUB     r4, r20, r4
	FPMUL      r4, r4, r21
	RSUBI     r9, r9, 1
	MULI      r9, r9, 12
	LW         r9, r5, r9
	FPRSUB     r9, r11, r9
	FPMUL      r9, r9, r12
	FPGT   r12, r4, r9
	bneid     r12, ($BB71_5)
	ADDI      r11, r0, 1
# BB#4:                                 # %entry
	ADDI      r11, r0, 0
$BB71_5:                                # %entry
	bneid     r11, ($BB71_32)
	NOP    
# BB#6:                                 # %if.end
	FPGT   r20, r4, r10
	ADDI      r3, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB71_8)
	ADD      r12, r11, r0
# BB#7:                                 # %if.end
	ADD      r12, r3, r0
$BB71_8:                                # %if.end
	bneid     r12, ($BB71_10)
	NOP    
# BB#9:                                 # %if.end
	ADD      r4, r10, r0
$BB71_10:                               # %if.end
	LWI       r10, r6, 44
	RSUBI     r12, r10, 1
	MULI      r12, r12, 12
	ADD      r12, r5, r12
	FPLT   r20, r8, r9
	bneid     r20, ($BB71_12)
	ADD      r21, r11, r0
# BB#11:                                # %if.end
	ADD      r21, r3, r0
$BB71_12:                               # %if.end
	LWI        r20, r12, 8
	LWI        r12, r6, 8
	FPRSUB     r22, r12, r20
	LWI        r20, r6, 32
	FPMUL      r6, r22, r20
	FPGT   r22, r4, r6
	bneid     r22, ($BB71_14)
	NOP    
# BB#13:                                # %if.end
	ADD      r11, r3, r0
$BB71_14:                               # %if.end
	bneid     r21, ($BB71_16)
	NOP    
# BB#15:                                # %if.end
	ADD      r8, r9, r0
$BB71_16:                               # %if.end
	bneid     r11, ($BB71_32)
	ADD      r3, r0, r0
# BB#17:                                # %if.end
	MULI      r9, r10, 12
	ADD      r5, r5, r9
	LWI        r5, r5, 8
	FPRSUB     r5, r12, r5
	FPMUL      r5, r5, r20
	FPGT   r10, r5, r8
	bneid     r10, ($BB71_19)
	ADDI      r9, r0, 1
# BB#18:                                # %if.end
	ADDI      r9, r0, 0
$BB71_19:                               # %if.end
	bneid     r9, ($BB71_32)
	NOP    
# BB#20:                                # %if.end72
	FPLT   r11, r6, r8
	ADDI      r3, r0, 0
	ADDI      r9, r0, 1
	bneid     r11, ($BB71_22)
	ADD      r10, r9, r0
# BB#21:                                # %if.end72
	ADD      r10, r3, r0
$BB71_22:                               # %if.end72
	bneid     r10, ($BB71_24)
	NOP    
# BB#23:                                # %if.end72
	ADD      r6, r8, r0
$BB71_24:                               # %if.end72
	ORI       r8, r0, 0
	FPLT   r6, r6, r8
	bneid     r6, ($BB71_26)
	NOP    
# BB#25:                                # %if.end72
	ADD      r9, r3, r0
$BB71_26:                               # %if.end72
	bneid     r9, ($BB71_32)
	ADD      r3, r0, r0
# BB#27:                                # %if.end81
	FPGT   r8, r5, r4
	ADDI      r3, r0, 1
	bneid     r8, ($BB71_29)
	ADD      r6, r3, r0
# BB#28:                                # %if.end81
	ADDI      r6, r0, 0
$BB71_29:                               # %if.end81
	bneid     r6, ($BB71_31)
	NOP    
# BB#30:                                # %if.end81
	ADD      r5, r4, r0
$BB71_31:                               # %if.end81
	SWI        r5, r7, 0
$BB71_32:                               # %return
	LWI       r22, r1, 0
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 12
	.end	_ZNK5syBox11IntersectsWERK3RayR7HitDist
$tmp71:
	.size	_ZNK5syBox11IntersectsWERK3RayR7HitDist, ($tmp71)-_ZNK5syBox11IntersectsWERK3RayR7HitDist

	.globl	_ZN9syBVHNodeC2Ei
	.align	2
	.type	_ZN9syBVHNodeC2Ei,@function
	.ent	_ZN9syBVHNodeC2Ei       # @_ZN9syBVHNodeC2Ei
_ZN9syBVHNodeC2Ei:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r0, r5, 20
	SWI       r0, r5, 16
	SWI       r0, r5, 12
	SWI       r0, r5, 8
	SWI       r0, r5, 4
	SWI       r0, r5, 0
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	SWI        r3, r5, 8
	ADDI      r3, r6, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	ADDI      r3, r6, 6
	LOAD      r3, r3, 0
	SWI       r3, r5, 24
	ADDI      r3, r6, 7
	LOAD      r3, r3, 0
	rtsd      r15, 8
	SWI       r3, r5, 28
	.end	_ZN9syBVHNodeC2Ei
$tmp72:
	.size	_ZN9syBVHNodeC2Ei, ($tmp72)-_ZN9syBVHNodeC2Ei

	.globl	_ZN9syBVHNode14LoadFromMemoryERKi
	.align	2
	.type	_ZN9syBVHNode14LoadFromMemoryERKi,@function
	.ent	_ZN9syBVHNode14LoadFromMemoryERKi # @_ZN9syBVHNode14LoadFromMemoryERKi
_ZN9syBVHNode14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r6, 0
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	LWI       r3, r6, 0
	ADDI      r3, r3, 6
	LOAD      r3, r3, 0
	SWI       r3, r5, 24
	LWI       r3, r6, 0
	ADDI      r3, r3, 7
	LOAD      r3, r3, 0
	rtsd      r15, 8
	SWI       r3, r5, 28
	.end	_ZN9syBVHNode14LoadFromMemoryERKi
$tmp73:
	.size	_ZN9syBVHNode14LoadFromMemoryERKi, ($tmp73)-_ZN9syBVHNode14LoadFromMemoryERKi

	.globl	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
	.align	2
	.type	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist,@function
	.ent	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist # @_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist:
	.frame	r1,12,r15
	.mask	0x700000
# BB#0:                                 # %entry
	ADDI      r1, r1, -12
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	SWI       r22, r1, 0
	LWI       r4, r6, 40
	RSUBI     r3, r4, 1
	MULI      r3, r3, 12
	ADD      r3, r5, r3
	LWI       r9, r6, 36
	MULI      r8, r9, 12
	LW         r8, r5, r8
	LWI        r11, r6, 0
	FPRSUB     r8, r11, r8
	LWI        r12, r6, 24
	FPMUL      r10, r8, r12
	LWI        r3, r3, 4
	LWI        r20, r6, 4
	FPRSUB     r3, r20, r3
	LWI        r21, r6, 28
	FPMUL      r8, r3, r21
	ADDI      r22, r0, 1
	FPGT   r3, r10, r8
	bneid     r3, ($BB74_2)
	NOP    
# BB#1:                                 # %entry
	ADDI      r22, r0, 0
$BB74_2:                                # %entry
	bneid     r22, ($BB74_32)
	ADD      r3, r0, r0
# BB#3:                                 # %entry
	MULI      r4, r4, 12
	ADD      r4, r5, r4
	LWI        r4, r4, 4
	FPRSUB     r4, r20, r4
	FPMUL      r4, r4, r21
	RSUBI     r9, r9, 1
	MULI      r9, r9, 12
	LW         r9, r5, r9
	FPRSUB     r9, r11, r9
	FPMUL      r9, r9, r12
	FPGT   r12, r4, r9
	bneid     r12, ($BB74_5)
	ADDI      r11, r0, 1
# BB#4:                                 # %entry
	ADDI      r11, r0, 0
$BB74_5:                                # %entry
	bneid     r11, ($BB74_32)
	NOP    
# BB#6:                                 # %if.end.i
	FPGT   r20, r4, r10
	ADDI      r3, r0, 0
	ADDI      r11, r0, 1
	bneid     r20, ($BB74_8)
	ADD      r12, r11, r0
# BB#7:                                 # %if.end.i
	ADD      r12, r3, r0
$BB74_8:                                # %if.end.i
	bneid     r12, ($BB74_10)
	NOP    
# BB#9:                                 # %if.end.i
	ADD      r4, r10, r0
$BB74_10:                               # %if.end.i
	LWI       r10, r6, 44
	RSUBI     r12, r10, 1
	MULI      r12, r12, 12
	ADD      r12, r5, r12
	FPLT   r20, r8, r9
	bneid     r20, ($BB74_12)
	ADD      r21, r11, r0
# BB#11:                                # %if.end.i
	ADD      r21, r3, r0
$BB74_12:                               # %if.end.i
	LWI        r20, r12, 8
	LWI        r12, r6, 8
	FPRSUB     r22, r12, r20
	LWI        r20, r6, 32
	FPMUL      r6, r22, r20
	FPGT   r22, r4, r6
	bneid     r22, ($BB74_14)
	NOP    
# BB#13:                                # %if.end.i
	ADD      r11, r3, r0
$BB74_14:                               # %if.end.i
	bneid     r21, ($BB74_16)
	NOP    
# BB#15:                                # %if.end.i
	ADD      r8, r9, r0
$BB74_16:                               # %if.end.i
	bneid     r11, ($BB74_32)
	ADD      r3, r0, r0
# BB#17:                                # %if.end.i
	MULI      r9, r10, 12
	ADD      r5, r5, r9
	LWI        r5, r5, 8
	FPRSUB     r5, r12, r5
	FPMUL      r5, r5, r20
	FPGT   r10, r5, r8
	bneid     r10, ($BB74_19)
	ADDI      r9, r0, 1
# BB#18:                                # %if.end.i
	ADDI      r9, r0, 0
$BB74_19:                               # %if.end.i
	bneid     r9, ($BB74_32)
	NOP    
# BB#20:                                # %if.end72.i
	FPLT   r11, r6, r8
	ADDI      r3, r0, 0
	ADDI      r9, r0, 1
	bneid     r11, ($BB74_22)
	ADD      r10, r9, r0
# BB#21:                                # %if.end72.i
	ADD      r10, r3, r0
$BB74_22:                               # %if.end72.i
	bneid     r10, ($BB74_24)
	NOP    
# BB#23:                                # %if.end72.i
	ADD      r6, r8, r0
$BB74_24:                               # %if.end72.i
	ORI       r8, r0, 0
	FPLT   r6, r6, r8
	bneid     r6, ($BB74_26)
	NOP    
# BB#25:                                # %if.end72.i
	ADD      r9, r3, r0
$BB74_26:                               # %if.end72.i
	bneid     r9, ($BB74_32)
	ADD      r3, r0, r0
# BB#27:                                # %if.end81.i
	FPGT   r8, r5, r4
	ADDI      r3, r0, 1
	bneid     r8, ($BB74_29)
	ADD      r6, r3, r0
# BB#28:                                # %if.end81.i
	ADDI      r6, r0, 0
$BB74_29:                               # %if.end81.i
	bneid     r6, ($BB74_31)
	NOP    
# BB#30:                                # %if.end81.i
	ADD      r5, r4, r0
$BB74_31:                               # %if.end81.i
	SWI        r5, r7, 0
$BB74_32:                               # %_ZNK5syBox11IntersectsWERK3RayR7HitDist.exit
	LWI       r22, r1, 0
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 12
	.end	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist
$tmp74:
	.size	_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist, ($tmp74)-_ZNK9syBVHNode13IntersectBoxWERK3RayR7HitDist

	.globl	_ZNK9syBVHNode8InteriorEv
	.align	2
	.type	_ZNK9syBVHNode8InteriorEv,@function
	.ent	_ZNK9syBVHNode8InteriorEv # @_ZNK9syBVHNode8InteriorEv
_ZNK9syBVHNode8InteriorEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r5, 24
	ADDI      r4, r0, -1
	CMP       r4, r4, r3
	beqid     r4, ($BB75_2)
	ADDI      r3, r0, 1
# BB#1:                                 # %entry
	ADDI      r3, r0, 0
$BB75_2:                                # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZNK9syBVHNode8InteriorEv
$tmp75:
	.size	_ZNK9syBVHNode8InteriorEv, ($tmp75)-_ZNK9syBVHNode8InteriorEv

	.globl	_ZNK9syBVHNode11LeftChildIDEv
	.align	2
	.type	_ZNK9syBVHNode11LeftChildIDEv,@function
	.ent	_ZNK9syBVHNode11LeftChildIDEv # @_ZNK9syBVHNode11LeftChildIDEv
_ZNK9syBVHNode11LeftChildIDEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	LWI       r3, r5, 28
	.end	_ZNK9syBVHNode11LeftChildIDEv
$tmp76:
	.size	_ZNK9syBVHNode11LeftChildIDEv, ($tmp76)-_ZNK9syBVHNode11LeftChildIDEv

	.globl	_ZNK9syBVHNode12RightChildIDEv
	.align	2
	.type	_ZNK9syBVHNode12RightChildIDEv,@function
	.ent	_ZNK9syBVHNode12RightChildIDEv # @_ZNK9syBVHNode12RightChildIDEv
_ZNK9syBVHNode12RightChildIDEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r5, 28
	rtsd      r15, 8
	ADDI      r3, r3, 1
	.end	_ZNK9syBVHNode12RightChildIDEv
$tmp77:
	.size	_ZNK9syBVHNode12RightChildIDEv, ($tmp77)-_ZNK9syBVHNode12RightChildIDEv

	.globl	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
	.align	2
	.type	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist,@function
	.ent	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist # @_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist:
	.frame	r1,52,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -52
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r7, r1, 64
	LWI       r3, r5, 24
	beqid     r3, ($BB78_26)
	SWI       r5, r1, 56
# BB#1:                                 # %for.body.lr.ph
	ADD      r3, r0, r0
$BB78_2:                                # %for.body
                                        # =>This Inner Loop Header: Depth=1
	MULI      r4, r3, 11
	LWI       r5, r5, 28
	ADD      r5, r5, r4
	LOAD      r21, r5, 0
	LOAD      r22, r5, 1
	LOAD      r24, r5, 2
	ADDI      r4, r5, 6
	ADDI      r5, r5, 3
	LOAD      r7, r5, 0
	LOAD      r20, r5, 1
	LOAD      r10, r5, 2
	LOAD      r9, r4, 0
	LOAD      r5, r4, 1
	LOAD      r4, r4, 2
	FPRSUB     r11, r24, r4
	FPRSUB     r23, r22, r5
	LWI        r4, r6, 20
	FPMUL      r5, r4, r23
	LWI        r8, r6, 16
	FPMUL      r12, r8, r11
	FPRSUB     r26, r5, r12
	SWI       r26, r1, 48
	FPRSUB     r25, r21, r9
	LWI        r9, r6, 12
	FPMUL      r5, r9, r11
	FPMUL      r12, r4, r25
	FPRSUB     r12, r5, r12
	FPRSUB     r28, r21, r7
	FPRSUB     r27, r22, r20
	FPMUL      r5, r12, r27
	FPMUL      r7, r26, r28
	FPADD      r5, r7, r5
	FPMUL      r7, r8, r25
	FPMUL      r20, r9, r23
	FPRSUB     r20, r7, r20
	FPRSUB     r29, r24, r10
	FPMUL      r7, r20, r29
	FPADD      r5, r5, r7
	ORI       r7, r0, 0
	FPGE   r10, r5, r7
	FPUN   r7, r5, r7
	BITOR        r7, r7, r10
	bneid     r7, ($BB78_4)
	ADDI      r30, r0, 1
# BB#3:                                 # %for.body
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r30, r0, 0
$BB78_4:                                # %for.body
                                        #   in Loop: Header=BB78_2 Depth=1
	LWI        r10, r6, 8
	LWI        r31, r6, 0
	LWI        r7, r6, 4
	bneid     r30, ($BB78_6)
	ADD      r26, r5, r0
# BB#5:                                 # %if.then.i.i
                                        #   in Loop: Header=BB78_2 Depth=1
	FPNEG      r26, r5
$BB78_6:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ORI       r30, r0, 953267991
	FPLT   r30, r26, r30
	bneid     r30, ($BB78_8)
	ADDI      r26, r0, 1
# BB#7:                                 # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r26, r0, 0
$BB78_8:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r26, ($BB78_25)
	NOP    
# BB#9:                                 # %if.end.i
                                        #   in Loop: Header=BB78_2 Depth=1
	FPRSUB     r30, r24, r10
	FPRSUB     r31, r21, r31
	FPRSUB     r10, r22, r7
	FPMUL      r7, r10, r28
	FPMUL      r21, r31, r27
	FPMUL      r22, r30, r28
	FPMUL      r24, r31, r29
	FPMUL      r26, r30, r27
	FPMUL      r27, r10, r29
	FPRSUB     r21, r7, r21
	FPRSUB     r24, r24, r22
	FPRSUB     r22, r26, r27
	FPMUL      r7, r24, r23
	FPMUL      r23, r22, r25
	FPADD      r7, r23, r7
	FPMUL      r11, r21, r11
	FPADD      r7, r7, r11
	ORI       r11, r0, 1065353216
	FPDIV      r23, r11, r5
	FPMUL      r11, r7, r23
	ORI       r5, r0, 0
	FPLT   r7, r11, r5
	bneid     r7, ($BB78_11)
	ADDI      r5, r0, 1
# BB#10:                                # %if.end.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r5, r0, 0
$BB78_11:                               # %if.end.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r5, ($BB78_25)
	NOP    
# BB#12:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	FPMUL      r5, r12, r10
	LWI       r7, r1, 48
	FPMUL      r7, r7, r31
	FPADD      r5, r7, r5
	FPMUL      r7, r20, r30
	FPADD      r5, r5, r7
	FPMUL      r10, r5, r23
	ORI       r5, r0, 0
	FPLT   r7, r10, r5
	FPUN   r5, r10, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB78_14)
	ADDI      r5, r0, 1
# BB#13:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r5, r0, 0
$BB78_14:                               # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r5, ($BB78_25)
	NOP    
# BB#15:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	FPMUL      r5, r24, r8
	FPMUL      r7, r22, r9
	FPADD      r5, r7, r5
	FPMUL      r4, r21, r4
	FPADD      r4, r5, r4
	FPMUL      r4, r4, r23
	ORI       r5, r0, 0
	FPLT   r7, r4, r5
	FPUN   r5, r4, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB78_17)
	ADDI      r5, r0, 1
# BB#16:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r5, r0, 0
$BB78_17:                               # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r5, ($BB78_25)
	NOP    
# BB#18:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	FPADD      r4, r10, r4
	ORI       r5, r0, 1065353216
	FPGT   r7, r4, r5
	FPUN   r4, r4, r5
	BITOR        r5, r4, r7
	bneid     r5, ($BB78_20)
	ADDI      r4, r0, 1
# BB#19:                                # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r4, r0, 0
$BB78_20:                               # %if.end9.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r4, ($BB78_25)
	NOP    
# BB#21:                                # %if.then19.i
                                        #   in Loop: Header=BB78_2 Depth=1
	LWI       r4, r1, 64
	LWI        r4, r4, 0
	FPGE   r5, r11, r4
	FPUN   r4, r11, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB78_23)
	ADDI      r4, r0, 1
# BB#22:                                # %if.then19.i
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r4, r0, 0
$BB78_23:                               # %if.then19.i
                                        #   in Loop: Header=BB78_2 Depth=1
	bneid     r4, ($BB78_25)
	NOP    
# BB#24:                                # %if.then21.i
                                        #   in Loop: Header=BB78_2 Depth=1
	LWI       r4, r1, 64
	SWI        r11, r4, 0
$BB78_25:                               # %_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist.exit
                                        #   in Loop: Header=BB78_2 Depth=1
	ADDI      r3, r3, 1
	LWI       r5, r1, 56
	LWI       r4, r5, 24
	CMP       r4, r4, r3
	bneid     r4, ($BB78_2)
	NOP    
$BB78_26:                               # %for.end
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 52
	.end	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist
$tmp78:
	.size	_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist, ($tmp78)-_ZNK9syBVHNode14TraceShadowRayERK3RayR7HitDist

	.globl	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
	.align	2
	.type	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord,@function
	.ent	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord # @_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord:
	.frame	r1,56,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -56
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r7, r1, 68
	SWI       r6, r1, 64
	LWI       r3, r5, 24
	beqid     r3, ($BB79_26)
	SWI       r5, r1, 60
# BB#1:                                 # %for.body.lr.ph
	ADD      r3, r0, r0
$BB79_2:                                # %for.body
                                        # =>This Inner Loop Header: Depth=1
	MULI      r4, r3, 11
	LWI       r5, r5, 28
	ADD      r4, r5, r4
	LOAD      r22, r4, 0
	LOAD      r23, r4, 1
	LOAD      r25, r4, 2
	ADDI      r5, r4, 3
	LOAD      r7, r5, 0
	LOAD      r11, r5, 1
	LOAD      r5, r5, 2
	ADDI      r9, r4, 6
	LOAD      r10, r9, 0
	LOAD      r8, r9, 1
	LOAD      r9, r9, 2
	FPRSUB     r12, r25, r9
	FPRSUB     r24, r23, r8
	LWI        r8, r6, 20
	FPMUL      r21, r8, r24
	LWI        r9, r6, 16
	FPMUL      r20, r9, r12
	FPRSUB     r27, r21, r20
	SWI       r27, r1, 48
	FPRSUB     r26, r22, r10
	LWI        r10, r6, 12
	FPMUL      r21, r10, r12
	FPMUL      r20, r8, r26
	FPRSUB     r20, r21, r20
	SWI       r20, r1, 52
	FPRSUB     r29, r22, r7
	FPRSUB     r28, r23, r11
	FPMUL      r7, r20, r28
	FPMUL      r11, r27, r29
	FPADD      r7, r11, r7
	FPMUL      r11, r9, r26
	FPMUL      r20, r10, r24
	FPRSUB     r21, r11, r20
	FPRSUB     r30, r25, r5
	FPMUL      r5, r21, r30
	FPADD      r20, r7, r5
	ORI       r5, r0, 0
	FPGE   r7, r20, r5
	FPUN   r5, r20, r5
	BITOR        r5, r5, r7
	bneid     r5, ($BB79_4)
	ADDI      r31, r0, 1
# BB#3:                                 # %for.body
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r31, r0, 0
$BB79_4:                                # %for.body
                                        #   in Loop: Header=BB79_2 Depth=1
	LWI        r5, r6, 8
	LWI        r11, r6, 0
	LWI        r7, r6, 4
	bneid     r31, ($BB79_6)
	ADD      r27, r20, r0
# BB#5:                                 # %if.then.i.i
                                        #   in Loop: Header=BB79_2 Depth=1
	FPNEG      r27, r20
$BB79_6:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ORI       r31, r0, 953267991
	FPLT   r31, r27, r31
	bneid     r31, ($BB79_8)
	ADDI      r27, r0, 1
# BB#7:                                 # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r27, r0, 0
$BB79_8:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r27, ($BB79_25)
	NOP    
# BB#9:                                 # %if.end.i
                                        #   in Loop: Header=BB79_2 Depth=1
	FPRSUB     r31, r25, r5
	FPRSUB     r11, r22, r11
	FPRSUB     r5, r23, r7
	FPMUL      r7, r5, r29
	FPMUL      r22, r11, r28
	FPMUL      r23, r31, r29
	FPMUL      r25, r11, r30
	FPMUL      r27, r31, r28
	FPMUL      r28, r5, r30
	FPRSUB     r22, r7, r22
	FPRSUB     r25, r25, r23
	FPRSUB     r23, r27, r28
	FPMUL      r7, r25, r24
	FPMUL      r24, r23, r26
	FPADD      r7, r24, r7
	FPMUL      r12, r22, r12
	FPADD      r7, r7, r12
	ORI       r12, r0, 1065353216
	FPDIV      r24, r12, r20
	FPMUL      r12, r7, r24
	ORI       r7, r0, 0
	FPLT   r20, r12, r7
	bneid     r20, ($BB79_11)
	ADDI      r7, r0, 1
# BB#10:                                # %if.end.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r7, r0, 0
$BB79_11:                               # %if.end.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r7, ($BB79_25)
	NOP    
# BB#12:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	LWI       r6, r1, 52
	FPMUL      r5, r6, r5
	LWI       r6, r1, 48
	FPMUL      r6, r6, r11
	FPADD      r5, r6, r5
	FPMUL      r6, r21, r31
	FPADD      r5, r5, r6
	FPMUL      r11, r5, r24
	ORI       r5, r0, 0
	FPLT   r6, r11, r5
	FPUN   r5, r11, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB79_14)
	ADDI      r5, r0, 1
# BB#13:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r5, r0, 0
$BB79_14:                               # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r5, ($BB79_25)
	NOP    
# BB#15:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	FPMUL      r5, r25, r9
	FPMUL      r6, r23, r10
	FPADD      r5, r6, r5
	FPMUL      r6, r22, r8
	FPADD      r5, r5, r6
	FPMUL      r5, r5, r24
	ORI       r6, r0, 0
	FPLT   r7, r5, r6
	FPUN   r6, r5, r6
	BITOR        r7, r6, r7
	bneid     r7, ($BB79_17)
	ADDI      r6, r0, 1
# BB#16:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r6, r0, 0
$BB79_17:                               # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r6, ($BB79_25)
	NOP    
# BB#18:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	FPADD      r5, r11, r5
	ORI       r6, r0, 1065353216
	FPGT   r7, r5, r6
	FPUN   r5, r5, r6
	BITOR        r6, r5, r7
	bneid     r6, ($BB79_20)
	ADDI      r5, r0, 1
# BB#19:                                # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r5, r0, 0
$BB79_20:                               # %if.end9.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r5, ($BB79_25)
	NOP    
# BB#21:                                # %if.then19.i
                                        #   in Loop: Header=BB79_2 Depth=1
	LWI       r5, r1, 68
	LWI        r5, r5, 0
	FPGE   r6, r12, r5
	FPUN   r5, r12, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB79_23)
	ADDI      r5, r0, 1
# BB#22:                                # %if.then19.i
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r5, r0, 0
$BB79_23:                               # %if.then19.i
                                        #   in Loop: Header=BB79_2 Depth=1
	bneid     r5, ($BB79_25)
	NOP    
# BB#24:                                # %_ZNK10syTriangle6ID_MATEv.exit.i
                                        #   in Loop: Header=BB79_2 Depth=1
	LWI       r6, r1, 68
	SWI        r12, r6, 0
	ADDI      r5, r4, 10
	LOAD      r5, r5, 0
	SWI       r5, r6, 4
	SWI       r4, r6, 8
$BB79_25:                               # %_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord.exit
                                        #   in Loop: Header=BB79_2 Depth=1
	ADDI      r3, r3, 1
	LWI       r5, r1, 60
	LWI       r4, r5, 24
	CMP       r4, r4, r3
	bneid     r4, ($BB79_2)
	LWI       r6, r1, 64
$BB79_26:                               # %for.end
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 56
	.end	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord
$tmp79:
	.size	_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord, ($tmp79)-_ZNK9syBVHNode8TraceRayERK3RayR9HitRecord

	.globl	_ZN8syMatrixC2Ev
	.align	2
	.type	_ZN8syMatrixC2Ev,@function
	.ent	_ZN8syMatrixC2Ev        # @_ZN8syMatrixC2Ev
_ZN8syMatrixC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN8syMatrixC2Ev
$tmp80:
	.size	_ZN8syMatrixC2Ev, ($tmp80)-_ZN8syMatrixC2Ev

	.globl	_ZN8syMatrix4ZeroEv
	.align	2
	.type	_ZN8syMatrix4ZeroEv,@function
	.ent	_ZN8syMatrix4ZeroEv     # @_ZN8syMatrix4ZeroEv
_ZN8syMatrix4ZeroEv:
	.frame	r1,4,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r15, r1, 0
	ADD      r6, r0, r0
	brlid     r15, memset
	ADDI      r7, r0, 36
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_ZN8syMatrix4ZeroEv
$tmp81:
	.size	_ZN8syMatrix4ZeroEv, ($tmp81)-_ZN8syMatrix4ZeroEv

	.globl	_ZN8syMatrix8IdentityEv
	.align	2
	.type	_ZN8syMatrix8IdentityEv,@function
	.ent	_ZN8syMatrix8IdentityEv # @_ZN8syMatrix8IdentityEv
_ZN8syMatrix8IdentityEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 0
	SWI       r0, r5, 4
	SWI       r0, r5, 8
	SWI       r0, r5, 12
	SWI       r3, r5, 16
	SWI       r0, r5, 20
	SWI       r0, r5, 24
	SWI       r0, r5, 28
	rtsd      r15, 8
	SWI       r3, r5, 32
	.end	_ZN8syMatrix8IdentityEv
$tmp82:
	.size	_ZN8syMatrix8IdentityEv, ($tmp82)-_ZN8syMatrix8IdentityEv

	.globl	_ZNK8syMatrix9GetColumnEi
	.align	2
	.type	_ZNK8syMatrix9GetColumnEi,@function
	.ent	_ZNK8syMatrix9GetColumnEi # @_ZNK8syMatrix9GetColumnEi
_ZNK8syMatrix9GetColumnEi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	MULI      r3, r7, 3
	bslli     r3, r3, 2
	LW         r4, r6, r3
	SWI        r4, r5, 0
	ADD      r3, r6, r3
	LWI        r4, r3, 4
	SWI        r4, r5, 4
	LWI        r3, r3, 8
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syMatrix9GetColumnEi
$tmp83:
	.size	_ZNK8syMatrix9GetColumnEi, ($tmp83)-_ZNK8syMatrix9GetColumnEi

	.globl	_ZN8syMatrix3LUDEPiRi
	.align	2
	.type	_ZN8syMatrix3LUDEPiRi,@function
	.ent	_ZN8syMatrix3LUDEPiRi   # @_ZN8syMatrix3LUDEPiRi
_ZN8syMatrix3LUDEPiRi:
	.frame	r1,24,r15
	.mask	0x700000
# BB#0:                                 # %entry
	ADDI      r1, r1, -24
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	SWI       r22, r1, 0
	ADDI      r3, r0, 1
	SWI       r3, r7, 0
	brid      ($BB84_1)
	ADD      r4, r0, r0
$BB84_92:                               # %if.end13
                                        #   in Loop: Header=BB84_1 Depth=1
	ORI       r3, r0, 1065353216
	FPDIV      r3, r3, r9
	ADDI      r9, r1, 12
	SW         r3, r9, r8
	ADDI      r4, r4, 1
$BB84_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	ADDI      r3, r0, 2
	CMP       r3, r3, r4
	bgtid     r3, ($BB84_2)
	NOP    
# BB#32:                                # %for.cond2.preheader
                                        #   in Loop: Header=BB84_1 Depth=1
	bslli     r8, r4, 2
	LW         r3, r5, r8
	ORI       r9, r0, 0
	FPLT   r10, r3, r9
	bneid     r10, ($BB84_34)
	ADDI      r9, r0, 1
# BB#33:                                # %for.cond2.preheader
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r9, r0, 0
$BB84_34:                               # %for.cond2.preheader
                                        #   in Loop: Header=BB84_1 Depth=1
	beqid     r9, ($BB84_35)
	NOP    
# BB#39:                                # %_ZN4util4fabsERKf.exit
                                        #   in Loop: Header=BB84_1 Depth=1
	ORI       r9, r0, -2147483648
	FPGE   r10, r3, r9
	FPUN   r9, r3, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB84_41)
	ADDI      r10, r0, 1
# BB#40:                                # %_ZN4util4fabsERKf.exit
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_41:                               # %_ZN4util4fabsERKf.exit
                                        #   in Loop: Header=BB84_1 Depth=1
	ORI       r9, r0, 0
	bneid     r10, ($BB84_43)
	NOP    
# BB#42:                                # %_ZN4util4fabsERKf.exit
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_43:                               # %_ZN4util4fabsERKf.exit
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r10, ($BB84_45)
	NOP    
# BB#44:                                # %if.then.i178
                                        #   in Loop: Header=BB84_1 Depth=1
	FPNEG      r9, r3
	brid      ($BB84_45)
	NOP    
$BB84_35:                               # %_ZN4util4fabsERKf.exit.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ORI       r9, r0, 0
	FPLE   r10, r3, r9
	FPUN   r11, r3, r9
	BITOR        r11, r11, r10
	bneid     r11, ($BB84_37)
	ADDI      r10, r0, 1
# BB#36:                                # %_ZN4util4fabsERKf.exit.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_37:                               # %_ZN4util4fabsERKf.exit.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r10, ($BB84_45)
	NOP    
# BB#38:                                # %_ZN4util4fabsERKf.exit.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_45:                               # %for.inc
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r3, r8, r5
	LWI        r3, r3, 12
	ORI       r10, r0, 0
	FPLT   r11, r3, r10
	bneid     r11, ($BB84_47)
	ADDI      r10, r0, 1
# BB#46:                                # %for.inc
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_47:                               # %for.inc
                                        #   in Loop: Header=BB84_1 Depth=1
	beqid     r10, ($BB84_48)
	NOP    
# BB#70:                                # %_ZN4util4fabsERKf.exit.1
                                        #   in Loop: Header=BB84_1 Depth=1
	FPNEG      r10, r3
	FPGE   r11, r9, r10
	FPUN   r12, r9, r10
	BITOR        r12, r12, r11
	bneid     r12, ($BB84_72)
	ADDI      r11, r0, 1
# BB#71:                                # %_ZN4util4fabsERKf.exit.1
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r11, r0, 0
$BB84_72:                               # %_ZN4util4fabsERKf.exit.1
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r11, ($BB84_74)
	NOP    
# BB#73:                                # %_ZN4util4fabsERKf.exit.1
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_74:                               # %_ZN4util4fabsERKf.exit.1
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r11, ($BB84_76)
	NOP    
# BB#75:                                # %if.then.i178.1
                                        #   in Loop: Header=BB84_1 Depth=1
	brid      ($BB84_76)
	ADD      r9, r10, r0
$BB84_48:                               # %_ZN4util4fabsERKf.exit.1.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	FPLE   r10, r3, r9
	FPUN   r11, r3, r9
	BITOR        r11, r11, r10
	bneid     r11, ($BB84_50)
	ADDI      r10, r0, 1
# BB#49:                                # %_ZN4util4fabsERKf.exit.1.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_50:                               # %_ZN4util4fabsERKf.exit.1.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r10, ($BB84_76)
	NOP    
# BB#51:                                # %_ZN4util4fabsERKf.exit.1.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_76:                               # %for.inc.1
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r3, r8, r5
	LWI        r3, r3, 24
	ORI       r10, r0, 0
	FPLT   r11, r3, r10
	bneid     r11, ($BB84_78)
	ADDI      r10, r0, 1
# BB#77:                                # %for.inc.1
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_78:                               # %for.inc.1
                                        #   in Loop: Header=BB84_1 Depth=1
	beqid     r10, ($BB84_79)
	NOP    
# BB#83:                                # %_ZN4util4fabsERKf.exit.2
                                        #   in Loop: Header=BB84_1 Depth=1
	FPNEG      r10, r3
	FPGE   r11, r9, r10
	FPUN   r12, r9, r10
	BITOR        r12, r12, r11
	bneid     r12, ($BB84_85)
	ADDI      r11, r0, 1
# BB#84:                                # %_ZN4util4fabsERKf.exit.2
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r11, r0, 0
$BB84_85:                               # %_ZN4util4fabsERKf.exit.2
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r11, ($BB84_87)
	NOP    
# BB#86:                                # %_ZN4util4fabsERKf.exit.2
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_87:                               # %_ZN4util4fabsERKf.exit.2
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r11, ($BB84_89)
	NOP    
# BB#88:                                # %if.then.i178.2
                                        #   in Loop: Header=BB84_1 Depth=1
	brid      ($BB84_89)
	ADD      r9, r10, r0
$BB84_79:                               # %_ZN4util4fabsERKf.exit.2.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	FPLE   r10, r3, r9
	FPUN   r11, r3, r9
	BITOR        r11, r11, r10
	bneid     r11, ($BB84_81)
	ADDI      r10, r0, 1
# BB#80:                                # %_ZN4util4fabsERKf.exit.2.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_81:                               # %_ZN4util4fabsERKf.exit.2.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r10, ($BB84_89)
	NOP    
# BB#82:                                # %_ZN4util4fabsERKf.exit.2.thread
                                        #   in Loop: Header=BB84_1 Depth=1
	ADD      r9, r3, r0
$BB84_89:                               # %for.inc.2
                                        #   in Loop: Header=BB84_1 Depth=1
	ORI       r3, r0, 0
	FPEQ   r3, r9, r3
	bneid     r3, ($BB84_91)
	ADDI      r10, r0, 1
# BB#90:                                # %for.inc.2
                                        #   in Loop: Header=BB84_1 Depth=1
	ADDI      r10, r0, 0
$BB84_91:                               # %for.inc.2
                                        #   in Loop: Header=BB84_1 Depth=1
	bneid     r10, ($BB84_31)
	ADD      r3, r0, r0
	brid      ($BB84_92)
	NOP    
$BB84_2:
	ADD      r3, r0, r0
$BB84_3:                                # %for.body20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB84_25 Depth 2
                                        #     Child Loop BB84_5 Depth 2
                                        #     Child Loop BB84_65 Depth 2
                                        #       Child Loop BB84_66 Depth 3
                                        #     Child Loop BB84_53 Depth 2
	ORI       r9, r0, 0
	ADDI      r8, r0, 1
	CMP       r10, r8, r3
	bltid     r10, ($BB84_4)
	MULI      r8, r3, 3
# BB#52:                                # %for.body25.preheader
                                        #   in Loop: Header=BB84_3 Depth=1
	ADD      r10, r0, r0
$BB84_53:                               # %for.body25
                                        #   Parent Loop BB84_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADDI      r9, r0, 1
	CMP       r9, r9, r10
	bltid     r9, ($BB84_55)
	NOP    
# BB#54:                                # %for.end46
                                        #   in Loop: Header=BB84_53 Depth=2
	ADD      r9, r10, r8
	bslli     r11, r8, 2
	LW         r11, r5, r11
	bslli     r12, r10, 2
	LW         r12, r5, r12
	FPMUL      r11, r12, r11
	bslli     r9, r9, 2
	LW         r12, r5, r9
	FPRSUB     r11, r11, r12
	SW         r11, r5, r9
$BB84_55:                               # %for.inc52
                                        #   in Loop: Header=BB84_53 Depth=2
	ORI       r9, r0, 0
	ADDI      r10, r10, 1
	CMP       r11, r3, r10
	bltid     r11, ($BB84_53)
	NOP    
# BB#56:                                #   in Loop: Header=BB84_3 Depth=1
	ADD      r10, r3, r0
$BB84_65:                               # %for.body67.lr.ph.us
                                        #   Parent Loop BB84_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB84_66 Depth 3
	ADD      r11, r10, r8
	bslli     r12, r11, 2
	LW         r11, r5, r12
	ADD      r20, r0, r0
$BB84_66:                               # %for.body67.us
                                        #   Parent Loop BB84_3 Depth=1
                                        #     Parent Loop BB84_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	MULI      r21, r20, 3
	ADD      r21, r21, r10
	ADD      r22, r20, r8
	bslli     r22, r22, 2
	LW         r22, r5, r22
	bslli     r21, r21, 2
	LW         r21, r5, r21
	FPMUL      r21, r21, r22
	FPRSUB     r11, r21, r11
	ADDI      r20, r20, 1
	CMP       r21, r3, r20
	bltid     r21, ($BB84_66)
	NOP    
# BB#67:                                # %if.end85.us
                                        #   in Loop: Header=BB84_65 Depth=2
	SW         r11, r5, r12
	ORI       r12, r0, 0
	FPLT   r12, r11, r12
	bneid     r12, ($BB84_69)
	ADDI      r20, r0, 1
# BB#68:                                # %if.end85.us
                                        #   in Loop: Header=BB84_65 Depth=2
	ADDI      r20, r0, 0
$BB84_69:                               # %if.end85.us
                                        #   in Loop: Header=BB84_65 Depth=2
	bslli     r12, r10, 2
	ADDI      r21, r1, 12
	beqid     r20, ($BB84_58)
	LW         r12, r21, r12
# BB#57:                                # %if.then.i173.us
                                        #   in Loop: Header=BB84_65 Depth=2
	FPNEG      r11, r11
$BB84_58:                               # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	FPMUL      r11, r12, r11
	FPLT   r12, r11, r9
	FPUN   r20, r11, r9
	BITOR        r20, r20, r12
	bneid     r20, ($BB84_60)
	ADDI      r12, r0, 1
# BB#59:                                # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	ADDI      r12, r0, 0
$BB84_60:                               # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	bneid     r12, ($BB84_62)
	NOP    
# BB#61:                                # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	ADD      r9, r11, r0
$BB84_62:                               # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	bneid     r12, ($BB84_64)
	NOP    
# BB#63:                                # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	ADD      r4, r10, r0
$BB84_64:                               # %_ZN4util4fabsERKf.exit175.us
                                        #   in Loop: Header=BB84_65 Depth=2
	ADDI      r10, r10, 1
	ADDI      r11, r0, 3
	CMP       r11, r11, r10
	bgeid     r11, ($BB84_16)
	NOP    
	brid      ($BB84_65)
	NOP    
$BB84_4:                                #   in Loop: Header=BB84_3 Depth=1
	ADD      r10, r3, r0
$BB84_5:                                # %if.end85
                                        #   Parent Loop BB84_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r11, r10, r8
	bslli     r11, r11, 2
	LW         r11, r5, r11
	ORI       r12, r0, 0
	FPGE   r20, r11, r12
	FPUN   r12, r11, r12
	BITOR        r12, r12, r20
	bneid     r12, ($BB84_7)
	ADDI      r20, r0, 1
# BB#6:                                 # %if.end85
                                        #   in Loop: Header=BB84_5 Depth=2
	ADDI      r20, r0, 0
$BB84_7:                                # %if.end85
                                        #   in Loop: Header=BB84_5 Depth=2
	bslli     r12, r10, 2
	ADDI      r21, r1, 12
	bneid     r20, ($BB84_9)
	LW         r12, r21, r12
# BB#8:                                 # %if.then.i173
                                        #   in Loop: Header=BB84_5 Depth=2
	FPNEG      r11, r11
$BB84_9:                                # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	FPMUL      r11, r12, r11
	FPLT   r12, r11, r9
	FPUN   r20, r11, r9
	BITOR        r20, r20, r12
	bneid     r20, ($BB84_11)
	ADDI      r12, r0, 1
# BB#10:                                # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	ADDI      r12, r0, 0
$BB84_11:                               # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	bneid     r12, ($BB84_13)
	NOP    
# BB#12:                                # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	ADD      r9, r11, r0
$BB84_13:                               # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	bneid     r12, ($BB84_15)
	NOP    
# BB#14:                                # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	ADD      r4, r10, r0
$BB84_15:                               # %_ZN4util4fabsERKf.exit175
                                        #   in Loop: Header=BB84_5 Depth=2
	ADDI      r10, r10, 1
	ADDI      r11, r0, 3
	CMP       r11, r11, r10
	bltid     r11, ($BB84_5)
	NOP    
$BB84_16:                               # %for.end94
                                        #   in Loop: Header=BB84_3 Depth=1
	CMP       r9, r4, r3
	beqid     r9, ($BB84_18)
	NOP    
# BB#17:                                # %for.cond97.preheader
                                        #   in Loop: Header=BB84_3 Depth=1
	bslli     r9, r3, 2
	bslli     r10, r4, 2
	LW         r11, r5, r10
	LW         r12, r5, r9
	SW         r12, r5, r10
	SW         r11, r5, r9
	ADD      r11, r5, r9
	ADD      r12, r5, r10
	LWI        r20, r12, 12
	LWI        r21, r11, 12
	SWI        r21, r12, 12
	SWI        r20, r11, 12
	LWI        r20, r12, 24
	LWI        r21, r11, 24
	SWI        r21, r12, 24
	SWI        r20, r11, 24
	LWI       r11, r7, 0
	RSUBI     r11, r11, 0
	SWI       r11, r7, 0
	ADDI      r11, r1, 12
	LW         r9, r11, r9
	SW         r9, r11, r10
$BB84_18:                               # %if.end122
                                        #   in Loop: Header=BB84_3 Depth=1
	bslli     r9, r3, 2
	SW        r4, r6, r9
	ADDI      r9, r0, 2
	CMP       r9, r9, r3
	beqid     r9, ($BB84_27)
	NOP    
# BB#19:                                # %if.then125
                                        #   in Loop: Header=BB84_3 Depth=1
	ADD      r9, r8, r3
	bslli     r10, r9, 2
	LW         r9, r5, r10
	ORI       r11, r0, 0
	FPNE   r12, r9, r11
	bneid     r12, ($BB84_21)
	ADDI      r11, r0, 1
# BB#20:                                # %if.then125
                                        #   in Loop: Header=BB84_3 Depth=1
	ADDI      r11, r0, 0
$BB84_21:                               # %if.then125
                                        #   in Loop: Header=BB84_3 Depth=1
	bneid     r11, ($BB84_23)
	NOP    
# BB#22:                                # %if.then132
                                        #   in Loop: Header=BB84_3 Depth=1
	ADDI      r9, r0, 786163455
	SW        r9, r5, r10
	ORI       r9, r0, 786163455
$BB84_23:                               # %if.end137
                                        #   in Loop: Header=BB84_3 Depth=1
	ORI       r10, r0, 1065353216
	ADDI      r3, r3, 1
	ADDI      r11, r0, 2
	CMP       r11, r11, r3
	bgtid     r11, ($BB84_27)
	NOP    
# BB#24:                                #   in Loop: Header=BB84_3 Depth=1
	FPDIV      r9, r10, r9
	ADD      r10, r3, r0
$BB84_25:                               # %for.body146
                                        #   Parent Loop BB84_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r11, r10, r8
	bslli     r11, r11, 2
	LW         r12, r5, r11
	FPMUL      r12, r12, r9
	SW         r12, r5, r11
	ADDI      r10, r10, 1
	ADDI      r11, r0, 3
	CMP       r12, r11, r10
	bltid     r12, ($BB84_25)
	NOP    
# BB#26:                                # %for.inc160
                                        #   in Loop: Header=BB84_3 Depth=1
	CMP       r8, r11, r3
	bltid     r8, ($BB84_3)
	NOP    
$BB84_27:                               # %for.end162
	LWI        r3, r5, 32
	ORI       r4, r0, 0
	FPNE   r6, r3, r4
	ADDI      r3, r0, 1
	bneid     r6, ($BB84_29)
	ADD      r4, r3, r0
# BB#28:                                # %for.end162
	ADDI      r4, r0, 0
$BB84_29:                               # %for.end162
	bneid     r4, ($BB84_31)
	NOP    
# BB#30:                                # %if.then167
	ADDI      r3, r0, 786163455
	SWI       r3, r5, 32
	ADDI      r3, r0, 1
$BB84_31:                               # %return
	LWI       r22, r1, 0
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 24
	.end	_ZN8syMatrix3LUDEPiRi
$tmp84:
	.size	_ZN8syMatrix3LUDEPiRi, ($tmp84)-_ZN8syMatrix3LUDEPiRi

	.globl	_ZN8syMatrix5LUBKSEPKiPf
	.align	2
	.type	_ZN8syMatrix5LUBKSEPKiPf,@function
	.ent	_ZN8syMatrix5LUBKSEPKiPf # @_ZN8syMatrix5LUBKSEPKiPf
_ZN8syMatrix5LUBKSEPKiPf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %if.else
	LWI       r3, r6, 0
	bslli     r4, r3, 2
	LW         r3, r7, r4
	LWI        r8, r7, 0
	SW         r8, r7, r4
	SWI        r3, r7, 0
	LWI       r4, r6, 4
	bslli     r8, r4, 2
	LW         r4, r7, r8
	LWI        r9, r7, 4
	SW         r9, r7, r8
	ORI       r8, r0, 0
	FPEQ   r3, r3, r8
	bneid     r3, ($BB85_2)
	ADDI      r8, r0, 1
# BB#1:                                 # %if.else
	ADDI      r8, r0, 0
$BB85_2:                                # %if.else
	bneid     r8, ($BB85_4)
	ADDI      r3, r0, -1
# BB#3:                                 # %if.else
	ADD      r3, r0, r0
$BB85_4:                                # %if.else
	beqid     r8, ($BB85_5)
	NOP    
# BB#7:                                 # %if.else.1
	ORI       r3, r0, 0
	FPEQ   r3, r4, r3
	bneid     r3, ($BB85_9)
	ADDI      r8, r0, 1
# BB#8:                                 # %if.else.1
	ADDI      r8, r0, 0
$BB85_9:                                # %if.else.1
	bneid     r8, ($BB85_11)
	ADDI      r3, r0, -1
# BB#10:                                # %if.then13.1
	brid      ($BB85_11)
	ADDI      r3, r0, 1
$BB85_5:
	ADD      r9, r3, r0
$BB85_6:                                # %for.body8.1
                                        # =>This Inner Loop Header: Depth=1
	ADD      r8, r9, r0
	bslli     r9, r8, 2
	LW         r9, r7, r9
	MULI      r10, r8, 3
	bslli     r10, r10, 2
	ADD      r10, r10, r5
	LWI        r10, r10, 4
	FPMUL      r9, r10, r9
	FPRSUB     r4, r9, r4
	ADDI      r9, r8, 1
	bltid     r8, ($BB85_6)
	NOP    
$BB85_11:                               # %if.end14.1
	SWI        r4, r7, 4
	LWI       r4, r6, 8
	bslli     r6, r4, 2
	LW         r4, r7, r6
	LWI        r8, r7, 8
	SW         r8, r7, r6
	ADDI      r6, r0, -1
	CMP       r6, r6, r3
	beqid     r6, ($BB85_13)
	NOP    
$BB85_12:                               # %for.body8.2
                                        # =>This Inner Loop Header: Depth=1
	bslli     r6, r3, 2
	LW         r6, r7, r6
	MULI      r8, r3, 3
	bslli     r8, r8, 2
	ADD      r8, r8, r5
	LWI        r8, r8, 8
	FPMUL      r6, r8, r6
	FPRSUB     r4, r6, r4
	ADDI      r3, r3, 1
	ADDI      r6, r0, 2
	CMP       r6, r6, r3
	bltid     r6, ($BB85_12)
	NOP    
$BB85_13:                               # %for.body28.lr.ph.2
	SWI        r4, r7, 8
	LWI        r3, r5, 32
	FPDIV      r3, r4, r3
	SWI        r3, r7, 8
	LWI        r4, r5, 28
	FPMUL      r4, r4, r3
	LWI        r6, r7, 4
	FPRSUB     r4, r4, r6
	LWI        r6, r5, 16
	FPDIV      r4, r4, r6
	SWI        r4, r7, 4
	LWI        r6, r5, 24
	FPMUL      r3, r6, r3
	LWI        r6, r5, 12
	FPMUL      r4, r6, r4
	LWI        r6, r7, 0
	FPRSUB     r4, r4, r6
	FPRSUB     r3, r3, r4
	LWI        r4, r5, 0
	FPDIV      r3, r3, r4
	rtsd      r15, 8
	SWI        r3, r7, 0
	.end	_ZN8syMatrix5LUBKSEPKiPf
$tmp85:
	.size	_ZN8syMatrix5LUBKSEPKiPf, ($tmp85)-_ZN8syMatrix5LUBKSEPKiPf

	.globl	_ZNK8syMatrix10GetInverseERS_
	.align	2
	.type	_ZNK8syMatrix10GetInverseERS_,@function
	.ent	_ZNK8syMatrix10GetInverseERS_ # @_ZNK8syMatrix10GetInverseERS_
_ZNK8syMatrix10GetInverseERS_:
	.frame	r1,116,r15
	.mask	0x1ff08000
# BB#0:                                 # %entry
	ADDI      r1, r1, -116
	SWI       r15, r1, 0
	SWI       r20, r1, 36
	SWI       r21, r1, 32
	SWI       r22, r1, 28
	SWI       r23, r1, 24
	SWI       r24, r1, 20
	SWI       r25, r1, 16
	SWI       r26, r1, 12
	SWI       r27, r1, 8
	SWI       r28, r1, 4
	ADD      r20, r6, r0
	LWI        r3, r5, 0
	SWI        r3, r20, 0
	LWI        r3, r5, 4
	SWI        r3, r20, 4
	LWI        r3, r5, 8
	SWI        r3, r20, 8
	LWI        r3, r5, 12
	SWI        r3, r20, 12
	LWI        r3, r5, 16
	SWI        r3, r20, 16
	LWI        r3, r5, 20
	SWI        r3, r20, 20
	LWI        r3, r5, 24
	SWI        r3, r20, 24
	LWI        r3, r5, 28
	SWI        r3, r20, 28
	LWI        r3, r5, 32
	SWI        r3, r20, 32
	ADDI      r5, r1, 80
	brlid     r15, memcpy
	ADDI      r7, r0, 36
	ADD      r5, r0, r0
	ADDI      r3, r20, 24
	brid      ($BB86_1)
	ADDI      r4, r20, 12
$BB86_97:                               # %if.end13.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ORI       r8, r0, 1065353216
	FPDIV      r7, r8, r7
	ADDI      r8, r1, 40
	SW         r7, r8, r6
	ADDI      r5, r5, 1
$BB86_1:                                # %for.cond.i
                                        # =>This Inner Loop Header: Depth=1
	ADDI      r6, r0, 2
	CMP       r6, r6, r5
	bgtid     r6, ($BB86_2)
	NOP    
# BB#26:                                # %for.cond2.preheader.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bslli     r6, r5, 2
	ADDI      r7, r1, 80
	LW         r8, r7, r6
	ORI       r7, r0, 0
	FPLT   r9, r8, r7
	bneid     r9, ($BB86_28)
	ADDI      r7, r0, 1
# BB#27:                                # %for.cond2.preheader.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r7, r0, 0
$BB86_28:                               # %for.cond2.preheader.i
                                        #   in Loop: Header=BB86_1 Depth=1
	beqid     r7, ($BB86_29)
	NOP    
# BB#33:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ORI       r7, r0, -2147483648
	FPGE   r9, r8, r7
	FPUN   r7, r8, r7
	BITOR        r7, r7, r9
	bneid     r7, ($BB86_35)
	ADDI      r9, r0, 1
# BB#34:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_35:                               # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ORI       r7, r0, 0
	bneid     r9, ($BB86_37)
	NOP    
# BB#36:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_37:                               # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r9, ($BB86_39)
	NOP    
# BB#38:                                # %if.then.i178.i
                                        #   in Loop: Header=BB86_1 Depth=1
	FPNEG      r7, r8
	brid      ($BB86_39)
	NOP    
$BB86_29:                               # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ORI       r7, r0, 0
	FPLE   r9, r8, r7
	FPUN   r10, r8, r7
	BITOR        r10, r10, r9
	bneid     r10, ($BB86_31)
	ADDI      r9, r0, 1
# BB#30:                                # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_31:                               # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r9, ($BB86_39)
	NOP    
# BB#32:                                # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_39:                               # %for.inc.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r8, r1, 80
	ADD      r8, r6, r8
	LWI        r8, r8, 12
	ORI       r9, r0, 0
	FPLT   r10, r8, r9
	bneid     r10, ($BB86_41)
	ADDI      r9, r0, 1
# BB#40:                                # %for.inc.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_41:                               # %for.inc.i
                                        #   in Loop: Header=BB86_1 Depth=1
	beqid     r9, ($BB86_42)
	NOP    
# BB#75:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	FPNEG      r9, r8
	FPGE   r10, r7, r9
	FPUN   r11, r7, r9
	BITOR        r11, r11, r10
	bneid     r11, ($BB86_77)
	ADDI      r10, r0, 1
# BB#76:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r10, r0, 0
$BB86_77:                               # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r10, ($BB86_79)
	NOP    
# BB#78:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_79:                               # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r10, ($BB86_81)
	NOP    
# BB#80:                                # %if.then.i178.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	brid      ($BB86_81)
	ADD      r7, r9, r0
$BB86_42:                               # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	FPLE   r9, r8, r7
	FPUN   r10, r8, r7
	BITOR        r10, r10, r9
	bneid     r10, ($BB86_44)
	ADDI      r9, r0, 1
# BB#43:                                # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_44:                               # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r9, ($BB86_81)
	NOP    
# BB#45:                                # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_81:                               # %for.inc.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r8, r1, 80
	ADD      r8, r6, r8
	LWI        r8, r8, 24
	ORI       r9, r0, 0
	FPLT   r10, r8, r9
	bneid     r10, ($BB86_83)
	ADDI      r9, r0, 1
# BB#82:                                # %for.inc.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_83:                               # %for.inc.1.i
                                        #   in Loop: Header=BB86_1 Depth=1
	beqid     r9, ($BB86_84)
	NOP    
# BB#88:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	FPNEG      r9, r8
	FPGE   r10, r7, r9
	FPUN   r11, r7, r9
	BITOR        r11, r11, r10
	bneid     r11, ($BB86_90)
	ADDI      r10, r0, 1
# BB#89:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r10, r0, 0
$BB86_90:                               # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r10, ($BB86_92)
	NOP    
# BB#91:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_92:                               # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r10, ($BB86_94)
	NOP    
# BB#93:                                # %if.then.i178.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	brid      ($BB86_94)
	ADD      r7, r9, r0
$BB86_84:                               # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	FPLE   r9, r8, r7
	FPUN   r10, r8, r7
	BITOR        r10, r10, r9
	bneid     r10, ($BB86_86)
	ADDI      r9, r0, 1
# BB#85:                                # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r9, r0, 0
$BB86_86:                               # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r9, ($BB86_94)
	NOP    
# BB#87:                                # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADD      r7, r8, r0
$BB86_94:                               # %for.inc.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ORI       r8, r0, 0
	FPEQ   r9, r7, r8
	bneid     r9, ($BB86_96)
	ADDI      r8, r0, 1
# BB#95:                                # %for.inc.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	ADDI      r8, r0, 0
$BB86_96:                               # %for.inc.2.i
                                        #   in Loop: Header=BB86_1 Depth=1
	bneid     r8, ($BB86_133)
	NOP    
	brid      ($BB86_97)
	NOP    
$BB86_2:
	brid      ($BB86_3)
	ADD      r5, r0, r0
$BB86_19:                               # %if.then125.i
                                        #   in Loop: Header=BB86_3 Depth=1
	ADD      r8, r7, r5
	bslli     r9, r8, 2
	ADDI      r8, r1, 80
	LW         r8, r8, r9
	ORI       r10, r0, 0
	FPNE   r11, r8, r10
	bneid     r11, ($BB86_21)
	ADDI      r10, r0, 1
# BB#20:                                # %if.then125.i
                                        #   in Loop: Header=BB86_3 Depth=1
	ADDI      r10, r0, 0
$BB86_21:                               # %if.then125.i
                                        #   in Loop: Header=BB86_3 Depth=1
	bneid     r10, ($BB86_23)
	NOP    
# BB#22:                                # %if.then132.i
                                        #   in Loop: Header=BB86_3 Depth=1
	ADDI      r8, r1, 80
	ADDI      r10, r0, 786163455
	SW        r10, r8, r9
	ORI       r8, r0, 786163455
$BB86_23:                               # %if.end137.i
                                        #   in Loop: Header=BB86_3 Depth=1
	ORI       r9, r0, 1065353216
	ADDI      r5, r5, 1
	ADDI      r10, r0, 2
	CMP       r10, r10, r5
	bgtid     r10, ($BB86_64)
	NOP    
# BB#24:                                #   in Loop: Header=BB86_3 Depth=1
	FPDIV      r8, r9, r8
	ADD      r9, r5, r0
$BB86_25:                               # %for.body146.i
                                        #   Parent Loop BB86_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r10, r9, r7
	bslli     r10, r10, 2
	ADDI      r11, r1, 80
	LW         r12, r11, r10
	FPMUL      r12, r12, r8
	SW         r12, r11, r10
	ADDI      r9, r9, 1
	ADDI      r10, r0, 3
	CMP       r10, r10, r9
	bltid     r10, ($BB86_25)
	NOP    
$BB86_3:                                # %for.body20.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB86_25 Depth 2
                                        #     Child Loop BB86_5 Depth 2
                                        #     Child Loop BB86_59 Depth 2
                                        #       Child Loop BB86_60 Depth 3
                                        #     Child Loop BB86_47 Depth 2
	ORI       r8, r0, 0
	ADDI      r7, r0, 1
	CMP       r9, r7, r5
	bltid     r9, ($BB86_4)
	MULI      r7, r5, 3
# BB#46:                                # %for.body25.preheader.i
                                        #   in Loop: Header=BB86_3 Depth=1
	ADD      r9, r0, r0
$BB86_47:                               # %for.body25.i
                                        #   Parent Loop BB86_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADDI      r8, r0, 1
	CMP       r8, r8, r9
	bltid     r8, ($BB86_49)
	NOP    
# BB#48:                                # %for.end46.i
                                        #   in Loop: Header=BB86_47 Depth=2
	ADD      r10, r9, r7
	bslli     r11, r7, 2
	ADDI      r8, r1, 80
	LW         r11, r8, r11
	bslli     r12, r9, 2
	LW         r12, r8, r12
	FPMUL      r11, r12, r11
	bslli     r10, r10, 2
	LW         r12, r8, r10
	FPRSUB     r11, r11, r12
	SW         r11, r8, r10
$BB86_49:                               # %for.inc52.i
                                        #   in Loop: Header=BB86_47 Depth=2
	ORI       r8, r0, 0
	ADDI      r9, r9, 1
	CMP       r10, r5, r9
	bltid     r10, ($BB86_47)
	NOP    
# BB#50:                                #   in Loop: Header=BB86_3 Depth=1
	ADD      r9, r5, r0
$BB86_59:                               # %for.body67.lr.ph.us.i
                                        #   Parent Loop BB86_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB86_60 Depth 3
	ADD      r10, r9, r7
	bslli     r11, r10, 2
	ADDI      r12, r1, 80
	LW         r10, r12, r11
	ADD      r21, r0, r0
$BB86_60:                               # %for.body67.us.i
                                        #   Parent Loop BB86_3 Depth=1
                                        #     Parent Loop BB86_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	MULI      r22, r21, 3
	ADD      r22, r22, r9
	ADD      r23, r21, r7
	bslli     r23, r23, 2
	LW         r23, r12, r23
	bslli     r22, r22, 2
	LW         r22, r12, r22
	FPMUL      r22, r22, r23
	FPRSUB     r10, r22, r10
	ADDI      r21, r21, 1
	CMP       r22, r5, r21
	bltid     r22, ($BB86_60)
	NOP    
# BB#61:                                # %if.end85.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADDI      r12, r1, 80
	SW         r10, r12, r11
	ORI       r11, r0, 0
	FPLT   r11, r10, r11
	bneid     r11, ($BB86_63)
	ADDI      r12, r0, 1
# BB#62:                                # %if.end85.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADDI      r12, r0, 0
$BB86_63:                               # %if.end85.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	bslli     r11, r9, 2
	ADDI      r21, r1, 40
	beqid     r12, ($BB86_52)
	LW         r11, r21, r11
# BB#51:                                # %if.then.i173.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	FPNEG      r10, r10
$BB86_52:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	FPMUL      r10, r11, r10
	FPLT   r11, r10, r8
	FPUN   r12, r10, r8
	BITOR        r12, r12, r11
	bneid     r12, ($BB86_54)
	ADDI      r11, r0, 1
# BB#53:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADDI      r11, r0, 0
$BB86_54:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	bneid     r11, ($BB86_56)
	NOP    
# BB#55:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADD      r8, r10, r0
$BB86_56:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	bneid     r11, ($BB86_58)
	NOP    
# BB#57:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADD      r6, r9, r0
$BB86_58:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB86_59 Depth=2
	ADDI      r9, r9, 1
	ADDI      r10, r0, 3
	CMP       r10, r10, r9
	bgeid     r10, ($BB86_16)
	NOP    
	brid      ($BB86_59)
	NOP    
$BB86_4:                                #   in Loop: Header=BB86_3 Depth=1
	ADD      r9, r5, r0
$BB86_5:                                # %if.end85.i
                                        #   Parent Loop BB86_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r10, r9, r7
	bslli     r10, r10, 2
	ADDI      r11, r1, 80
	LW         r10, r11, r10
	ORI       r11, r0, 0
	FPGE   r12, r10, r11
	FPUN   r11, r10, r11
	BITOR        r11, r11, r12
	bneid     r11, ($BB86_7)
	ADDI      r12, r0, 1
# BB#6:                                 # %if.end85.i
                                        #   in Loop: Header=BB86_5 Depth=2
	ADDI      r12, r0, 0
$BB86_7:                                # %if.end85.i
                                        #   in Loop: Header=BB86_5 Depth=2
	bslli     r11, r9, 2
	ADDI      r21, r1, 40
	bneid     r12, ($BB86_9)
	LW         r11, r21, r11
# BB#8:                                 # %if.then.i173.i
                                        #   in Loop: Header=BB86_5 Depth=2
	FPNEG      r10, r10
$BB86_9:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	FPMUL      r10, r11, r10
	FPLT   r11, r10, r8
	FPUN   r12, r10, r8
	BITOR        r12, r12, r11
	bneid     r12, ($BB86_11)
	ADDI      r11, r0, 1
# BB#10:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	ADDI      r11, r0, 0
$BB86_11:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	bneid     r11, ($BB86_13)
	NOP    
# BB#12:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	ADD      r8, r10, r0
$BB86_13:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	bneid     r11, ($BB86_15)
	NOP    
# BB#14:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	ADD      r6, r9, r0
$BB86_15:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB86_5 Depth=2
	ADDI      r9, r9, 1
	ADDI      r10, r0, 3
	CMP       r10, r10, r9
	bltid     r10, ($BB86_5)
	NOP    
$BB86_16:                               # %for.end94.i
                                        #   in Loop: Header=BB86_3 Depth=1
	CMP       r8, r6, r5
	beqid     r8, ($BB86_18)
	NOP    
# BB#17:                                # %for.cond97.preheader.i
                                        #   in Loop: Header=BB86_3 Depth=1
	bslli     r8, r5, 2
	bslli     r9, r6, 2
	ADDI      r11, r1, 80
	LW         r10, r11, r9
	LW         r12, r11, r8
	SW         r12, r11, r9
	SW         r10, r11, r8
	ADD      r10, r11, r8
	ADD      r11, r11, r9
	LWI        r12, r11, 12
	LWI        r21, r10, 12
	SWI        r21, r11, 12
	SWI        r12, r10, 12
	LWI        r12, r11, 24
	LWI        r21, r10, 24
	SWI        r21, r11, 24
	SWI        r12, r10, 24
	ADDI      r10, r1, 40
	LW         r8, r10, r8
	SW         r8, r10, r9
$BB86_18:                               # %if.end122.i
                                        #   in Loop: Header=BB86_3 Depth=1
	bslli     r8, r5, 2
	ADDI      r9, r1, 68
	SW        r6, r9, r8
	ADDI      r8, r0, 2
	CMP       r8, r8, r5
	bneid     r8, ($BB86_19)
	NOP    
$BB86_64:                               # %for.end162.i
	ORI       r6, r0, 0
	LWI        r5, r1, 112
	FPNE   r7, r5, r6
	bneid     r7, ($BB86_66)
	ADDI      r6, r0, 1
# BB#65:                                # %for.end162.i
	ADDI      r6, r0, 0
$BB86_66:                               # %for.end162.i
	bneid     r6, ($BB86_68)
	NOP    
# BB#67:                                # %if.then167.i
	ADDI      r5, r0, 786163455
	SWI       r5, r1, 112
	ORI       r5, r0, 786163455
$BB86_68:                               # %for.cond5.preheader.i
	SWI       r0, r1, 64
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r7, r0, 1065353216
	SWI       r7, r1, 56
	LWI       r6, r1, 68
	bslli     r6, r6, 2
	ADDI      r10, r1, 56
	LW         r8, r10, r6
	SW        r7, r10, r6
	SWI        r8, r1, 56
	LWI       r7, r1, 72
	bslli     r7, r7, 2
	LW         r9, r10, r7
	LWI        r11, r1, 60
	SW         r11, r10, r7
	ORI       r10, r0, 0
	FPEQ   r8, r8, r10
	bneid     r8, ($BB86_70)
	ADDI      r10, r0, 1
# BB#69:                                # %for.cond5.preheader.i
	ADDI      r10, r0, 0
$BB86_70:                               # %for.cond5.preheader.i
	bneid     r10, ($BB86_72)
	ADDI      r8, r0, -1
# BB#71:                                # %for.cond5.preheader.i
	ADD      r8, r0, r0
$BB86_72:                               # %for.cond5.preheader.i
	beqid     r10, ($BB86_73)
	NOP    
# BB#98:                                # %if.else.1.i95
	ORI       r8, r0, 0
	FPEQ   r8, r9, r8
	bneid     r8, ($BB86_100)
	ADDI      r10, r0, 1
# BB#99:                                # %if.else.1.i95
	ADDI      r10, r0, 0
$BB86_100:                              # %if.else.1.i95
	bneid     r10, ($BB86_102)
	ADDI      r8, r0, -1
# BB#101:                               # %if.then13.1.i96
	brid      ($BB86_102)
	ADDI      r8, r0, 1
$BB86_73:
	ADD      r11, r8, r0
$BB86_74:                               # %for.body8.1.i93
                                        # =>This Inner Loop Header: Depth=1
	ADD      r10, r11, r0
	bslli     r11, r10, 2
	ADDI      r12, r1, 56
	LW         r11, r12, r11
	MULI      r12, r10, 3
	bslli     r12, r12, 2
	ADDI      r21, r1, 80
	ADD      r12, r12, r21
	LWI        r12, r12, 4
	FPMUL      r11, r12, r11
	FPRSUB     r9, r11, r9
	ADDI      r11, r10, 1
	bltid     r10, ($BB86_74)
	NOP    
$BB86_102:                              # %if.end14.1.i103
	SWI        r9, r1, 60
	LWI       r9, r1, 76
	bslli     r21, r9, 2
	ADDI      r22, r1, 56
	LW         r9, r22, r21
	LWI        r10, r1, 64
	SW         r10, r22, r21
	ADDI      r10, r0, -1
	CMP       r10, r10, r8
	beqid     r10, ($BB86_104)
	NOP    
$BB86_103:                              # %for.body8.2.i114
                                        # =>This Inner Loop Header: Depth=1
	bslli     r10, r8, 2
	ADDI      r11, r1, 56
	LW         r10, r11, r10
	MULI      r11, r8, 3
	bslli     r11, r11, 2
	ADDI      r12, r1, 80
	ADD      r11, r11, r12
	LWI        r11, r11, 8
	FPMUL      r10, r11, r10
	FPRSUB     r9, r10, r9
	ADDI      r8, r8, 1
	ADDI      r10, r0, 2
	CMP       r10, r10, r8
	bltid     r10, ($BB86_103)
	NOP    
$BB86_104:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	FPDIV      r11, r9, r5
	SWI        r11, r1, 64
	LWI        r8, r1, 108
	FPMUL      r9, r8, r11
	LWI        r10, r1, 60
	FPRSUB     r10, r9, r10
	LWI        r9, r1, 96
	FPDIV      r12, r10, r9
	SWI        r12, r1, 60
	LWI        r10, r1, 92
	FPMUL      r12, r10, r12
	LWI        r23, r1, 56
	FPRSUB     r23, r12, r23
	LWI        r12, r1, 104
	FPMUL      r11, r12, r11
	FPRSUB     r23, r11, r23
	LWI        r11, r1, 80
	FPDIV      r23, r23, r11
	SWI        r23, r1, 56
	LWI       r23, r1, 64
	SWI       r23, r20, 8
	LWI       r23, r1, 60
	SWI       r23, r20, 4
	LWI       r23, r1, 56
	SWI       r23, r20, 0
	SWI       r0, r1, 64
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r20, r0, 1065353216
	SWI       r20, r1, 60
	ADD      r25, r0, r0
	LW         r23, r22, r6
	SW        r25, r22, r6
	SWI        r23, r1, 56
	LW         r24, r22, r7
	LWI        r20, r1, 60
	SW         r20, r22, r7
	ORI       r20, r0, 0
	FPEQ   r20, r23, r20
	bneid     r20, ($BB86_106)
	ADDI      r22, r0, 1
# BB#105:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	ADDI      r22, r0, 0
$BB86_106:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	bneid     r22, ($BB86_108)
	ADDI      r20, r0, -1
# BB#107:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	ADD      r20, r25, r0
$BB86_108:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	beqid     r22, ($BB86_109)
	NOP    
# BB#114:                               # %if.else.1.i40
	SWI        r24, r1, 60
	ADDI      r20, r1, 56
	LW         r22, r20, r21
	LWI        r23, r1, 64
	SW         r23, r20, r21
	ORI       r20, r0, 0
	FPNE   r24, r24, r20
	ADDI      r20, r0, 1
	bneid     r24, ($BB86_116)
	ADD      r23, r20, r0
# BB#115:                               # %if.else.1.i40
	ADDI      r23, r0, 0
$BB86_116:                              # %if.else.1.i40
	beqid     r23, ($BB86_118)
	NOP    
	brid      ($BB86_117)
	NOP    
$BB86_109:
	ADD      r26, r20, r0
$BB86_110:                              # %for.body8.1.i38
                                        # =>This Inner Loop Header: Depth=1
	ADD      r22, r26, r0
	bslli     r26, r22, 2
	ADDI      r25, r1, 56
	LW         r26, r25, r26
	MULI      r27, r22, 3
	bslli     r27, r27, 2
	ADDI      r28, r1, 80
	ADD      r27, r27, r28
	LWI        r27, r27, 4
	FPMUL      r26, r27, r26
	FPRSUB     r24, r26, r24
	ADDI      r26, r22, 1
	bltid     r22, ($BB86_110)
	NOP    
# BB#111:                               # %if.end14.1.i48
	SWI        r24, r1, 60
	LW         r22, r25, r21
	LWI        r24, r1, 64
	SW         r24, r25, r21
	ORI       r24, r0, 0
	FPEQ   r24, r23, r24
	bneid     r24, ($BB86_113)
	ADDI      r23, r0, 1
# BB#112:                               # %if.end14.1.i48
	ADDI      r23, r0, 0
$BB86_113:                              # %if.end14.1.i48
	bneid     r23, ($BB86_118)
	NOP    
$BB86_117:                              # %for.body8.2.i59
                                        # =>This Inner Loop Header: Depth=1
	bslli     r23, r20, 2
	ADDI      r24, r1, 56
	LW         r23, r24, r23
	MULI      r24, r20, 3
	bslli     r24, r24, 2
	ADDI      r25, r1, 80
	ADD      r24, r24, r25
	LWI        r24, r24, 8
	FPMUL      r23, r24, r23
	FPRSUB     r22, r23, r22
	ADDI      r20, r20, 1
	ADDI      r23, r0, 2
	CMP       r23, r23, r20
	bltid     r23, ($BB86_117)
	NOP    
$BB86_118:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	FPDIV      r20, r22, r5
	SWI        r20, r1, 64
	FPMUL      r22, r8, r20
	FPMUL      r20, r12, r20
	LWI        r23, r1, 60
	FPRSUB     r22, r22, r23
	FPDIV      r22, r22, r9
	SWI        r22, r1, 60
	FPMUL      r22, r10, r22
	LWI        r23, r1, 56
	FPRSUB     r22, r22, r23
	FPRSUB     r20, r20, r22
	FPDIV      r20, r20, r11
	SWI        r20, r1, 56
	LWI       r20, r1, 64
	SWI       r20, r4, 8
	LWI       r20, r1, 60
	SWI       r20, r4, 4
	LWI       r20, r1, 56
	SWI       r20, r4, 0
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r4, r0, 1065353216
	SWI       r4, r1, 64
	ADD      r23, r0, r0
	ADDI      r4, r1, 56
	LW         r20, r4, r6
	SW        r23, r4, r6
	SWI        r20, r1, 56
	LW         r22, r4, r7
	LWI        r6, r1, 60
	SW         r6, r4, r7
	ORI       r4, r0, 0
	FPEQ   r4, r20, r4
	bneid     r4, ($BB86_120)
	ADDI      r6, r0, 1
# BB#119:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	ADDI      r6, r0, 0
$BB86_120:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	bneid     r6, ($BB86_122)
	ADDI      r4, r0, -1
# BB#121:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	ADD      r4, r23, r0
$BB86_122:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	beqid     r6, ($BB86_123)
	NOP    
# BB#128:                               # %if.else.1.i
	SWI        r22, r1, 60
	ADDI      r4, r1, 56
	LW         r6, r4, r21
	LWI        r7, r1, 64
	SW         r7, r4, r21
	ORI       r4, r0, 0
	FPNE   r20, r22, r4
	ADDI      r4, r0, 1
	bneid     r20, ($BB86_130)
	ADD      r7, r4, r0
# BB#129:                               # %if.else.1.i
	ADDI      r7, r0, 0
$BB86_130:                              # %if.else.1.i
	beqid     r7, ($BB86_132)
	NOP    
	brid      ($BB86_131)
	NOP    
$BB86_123:
	ADD      r23, r4, r0
$BB86_124:                              # %for.body8.1.i
                                        # =>This Inner Loop Header: Depth=1
	ADD      r6, r23, r0
	bslli     r23, r6, 2
	ADDI      r7, r1, 56
	LW         r23, r7, r23
	MULI      r24, r6, 3
	bslli     r24, r24, 2
	ADDI      r25, r1, 80
	ADD      r24, r24, r25
	LWI        r24, r24, 4
	FPMUL      r23, r24, r23
	FPRSUB     r22, r23, r22
	ADDI      r23, r6, 1
	bltid     r6, ($BB86_124)
	NOP    
# BB#125:                               # %if.end14.1.i
	SWI        r22, r1, 60
	LW         r6, r7, r21
	LWI        r22, r1, 64
	SW         r22, r7, r21
	ORI       r7, r0, 0
	FPEQ   r20, r20, r7
	bneid     r20, ($BB86_127)
	ADDI      r7, r0, 1
# BB#126:                               # %if.end14.1.i
	ADDI      r7, r0, 0
$BB86_127:                              # %if.end14.1.i
	bneid     r7, ($BB86_132)
	NOP    
$BB86_131:                              # %for.body8.2.i
                                        # =>This Inner Loop Header: Depth=1
	bslli     r7, r4, 2
	ADDI      r20, r1, 56
	LW         r7, r20, r7
	MULI      r20, r4, 3
	bslli     r20, r20, 2
	ADDI      r21, r1, 80
	ADD      r20, r20, r21
	LWI        r20, r20, 8
	FPMUL      r7, r20, r7
	FPRSUB     r6, r7, r6
	ADDI      r4, r4, 1
	ADDI      r7, r0, 2
	CMP       r7, r7, r4
	bltid     r7, ($BB86_131)
	NOP    
$BB86_132:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit
	FPDIV      r4, r6, r5
	SWI        r4, r1, 64
	FPMUL      r5, r8, r4
	FPMUL      r4, r12, r4
	LWI        r6, r1, 60
	FPRSUB     r5, r5, r6
	FPDIV      r5, r5, r9
	SWI        r5, r1, 60
	FPMUL      r5, r10, r5
	LWI        r6, r1, 56
	FPRSUB     r5, r5, r6
	FPRSUB     r4, r4, r5
	FPDIV      r4, r4, r11
	SWI        r4, r1, 56
	LWI       r4, r1, 64
	SWI       r4, r3, 8
	LWI       r4, r1, 60
	SWI       r4, r3, 4
	LWI       r4, r1, 56
	SWI       r4, r3, 0
$BB86_133:                              # %_ZN8syMatrix6InvertEv.exit
	LWI       r28, r1, 4
	LWI       r27, r1, 8
	LWI       r26, r1, 12
	LWI       r25, r1, 16
	LWI       r24, r1, 20
	LWI       r23, r1, 24
	LWI       r22, r1, 28
	LWI       r21, r1, 32
	LWI       r20, r1, 36
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 116
	.end	_ZNK8syMatrix10GetInverseERS_
$tmp86:
	.size	_ZNK8syMatrix10GetInverseERS_, ($tmp86)-_ZNK8syMatrix10GetInverseERS_

	.globl	_ZN8syMatrix6InvertEv
	.align	2
	.type	_ZN8syMatrix6InvertEv,@function
	.ent	_ZN8syMatrix6InvertEv   # @_ZN8syMatrix6InvertEv
_ZN8syMatrix6InvertEv:
	.frame	r1,116,r15
	.mask	0x1ff08000
# BB#0:                                 # %entry
	ADDI      r1, r1, -116
	SWI       r15, r1, 0
	SWI       r20, r1, 36
	SWI       r21, r1, 32
	SWI       r22, r1, 28
	SWI       r23, r1, 24
	SWI       r24, r1, 20
	SWI       r25, r1, 16
	SWI       r26, r1, 12
	SWI       r27, r1, 8
	SWI       r28, r1, 4
	ADD      r20, r5, r0
	ADDI      r5, r1, 80
	ADDI      r7, r0, 36
	brlid     r15, memcpy
	ADD      r6, r20, r0
	brid      ($BB87_1)
	ADD      r3, r0, r0
$BB87_97:                               # %if.end13.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ORI       r6, r0, 1065353216
	FPDIV      r5, r6, r5
	ADDI      r6, r1, 40
	SW         r5, r6, r4
	ADDI      r3, r3, 1
$BB87_1:                                # %for.cond.i
                                        # =>This Inner Loop Header: Depth=1
	ADDI      r4, r0, 2
	CMP       r4, r4, r3
	bgtid     r4, ($BB87_2)
	NOP    
# BB#26:                                # %for.cond2.preheader.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bslli     r4, r3, 2
	ADDI      r5, r1, 80
	LW         r6, r5, r4
	ORI       r5, r0, 0
	FPLT   r7, r6, r5
	bneid     r7, ($BB87_28)
	ADDI      r5, r0, 1
# BB#27:                                # %for.cond2.preheader.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r5, r0, 0
$BB87_28:                               # %for.cond2.preheader.i
                                        #   in Loop: Header=BB87_1 Depth=1
	beqid     r5, ($BB87_29)
	NOP    
# BB#33:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ORI       r5, r0, -2147483648
	FPGE   r7, r6, r5
	FPUN   r5, r6, r5
	BITOR        r5, r5, r7
	bneid     r5, ($BB87_35)
	ADDI      r7, r0, 1
# BB#34:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_35:                               # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ORI       r5, r0, 0
	bneid     r7, ($BB87_37)
	NOP    
# BB#36:                                # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_37:                               # %_ZN4util4fabsERKf.exit.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r7, ($BB87_39)
	NOP    
# BB#38:                                # %if.then.i178.i
                                        #   in Loop: Header=BB87_1 Depth=1
	FPNEG      r5, r6
	brid      ($BB87_39)
	NOP    
$BB87_29:                               # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ORI       r5, r0, 0
	FPLE   r7, r6, r5
	FPUN   r8, r6, r5
	BITOR        r8, r8, r7
	bneid     r8, ($BB87_31)
	ADDI      r7, r0, 1
# BB#30:                                # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_31:                               # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r7, ($BB87_39)
	NOP    
# BB#32:                                # %_ZN4util4fabsERKf.exit.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_39:                               # %for.inc.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r6, r1, 80
	ADD      r6, r4, r6
	LWI        r6, r6, 12
	ORI       r7, r0, 0
	FPLT   r8, r6, r7
	bneid     r8, ($BB87_41)
	ADDI      r7, r0, 1
# BB#40:                                # %for.inc.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_41:                               # %for.inc.i
                                        #   in Loop: Header=BB87_1 Depth=1
	beqid     r7, ($BB87_42)
	NOP    
# BB#75:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	FPNEG      r7, r6
	FPGE   r8, r5, r7
	FPUN   r9, r5, r7
	BITOR        r9, r9, r8
	bneid     r9, ($BB87_77)
	ADDI      r8, r0, 1
# BB#76:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r8, r0, 0
$BB87_77:                               # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r8, ($BB87_79)
	NOP    
# BB#78:                                # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_79:                               # %_ZN4util4fabsERKf.exit.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r8, ($BB87_81)
	NOP    
# BB#80:                                # %if.then.i178.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	brid      ($BB87_81)
	ADD      r5, r7, r0
$BB87_42:                               # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	FPLE   r7, r6, r5
	FPUN   r8, r6, r5
	BITOR        r8, r8, r7
	bneid     r8, ($BB87_44)
	ADDI      r7, r0, 1
# BB#43:                                # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_44:                               # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r7, ($BB87_81)
	NOP    
# BB#45:                                # %_ZN4util4fabsERKf.exit.1.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_81:                               # %for.inc.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r6, r1, 80
	ADD      r6, r4, r6
	LWI        r6, r6, 24
	ORI       r7, r0, 0
	FPLT   r8, r6, r7
	bneid     r8, ($BB87_83)
	ADDI      r7, r0, 1
# BB#82:                                # %for.inc.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_83:                               # %for.inc.1.i
                                        #   in Loop: Header=BB87_1 Depth=1
	beqid     r7, ($BB87_84)
	NOP    
# BB#88:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	FPNEG      r7, r6
	FPGE   r8, r5, r7
	FPUN   r9, r5, r7
	BITOR        r9, r9, r8
	bneid     r9, ($BB87_90)
	ADDI      r8, r0, 1
# BB#89:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r8, r0, 0
$BB87_90:                               # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r8, ($BB87_92)
	NOP    
# BB#91:                                # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_92:                               # %_ZN4util4fabsERKf.exit.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r8, ($BB87_94)
	NOP    
# BB#93:                                # %if.then.i178.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	brid      ($BB87_94)
	ADD      r5, r7, r0
$BB87_84:                               # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	FPLE   r7, r6, r5
	FPUN   r8, r6, r5
	BITOR        r8, r8, r7
	bneid     r8, ($BB87_86)
	ADDI      r7, r0, 1
# BB#85:                                # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r7, r0, 0
$BB87_86:                               # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r7, ($BB87_94)
	NOP    
# BB#87:                                # %_ZN4util4fabsERKf.exit.2.thread.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADD      r5, r6, r0
$BB87_94:                               # %for.inc.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ORI       r6, r0, 0
	FPEQ   r7, r5, r6
	bneid     r7, ($BB87_96)
	ADDI      r6, r0, 1
# BB#95:                                # %for.inc.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	ADDI      r6, r0, 0
$BB87_96:                               # %for.inc.2.i
                                        #   in Loop: Header=BB87_1 Depth=1
	bneid     r6, ($BB87_133)
	NOP    
	brid      ($BB87_97)
	NOP    
$BB87_2:
	brid      ($BB87_3)
	ADD      r3, r0, r0
$BB87_19:                               # %if.then125.i
                                        #   in Loop: Header=BB87_3 Depth=1
	ADD      r6, r5, r3
	bslli     r7, r6, 2
	ADDI      r6, r1, 80
	LW         r6, r6, r7
	ORI       r8, r0, 0
	FPNE   r9, r6, r8
	bneid     r9, ($BB87_21)
	ADDI      r8, r0, 1
# BB#20:                                # %if.then125.i
                                        #   in Loop: Header=BB87_3 Depth=1
	ADDI      r8, r0, 0
$BB87_21:                               # %if.then125.i
                                        #   in Loop: Header=BB87_3 Depth=1
	bneid     r8, ($BB87_23)
	NOP    
# BB#22:                                # %if.then132.i
                                        #   in Loop: Header=BB87_3 Depth=1
	ADDI      r6, r1, 80
	ADDI      r8, r0, 786163455
	SW        r8, r6, r7
	ORI       r6, r0, 786163455
$BB87_23:                               # %if.end137.i
                                        #   in Loop: Header=BB87_3 Depth=1
	ORI       r7, r0, 1065353216
	ADDI      r3, r3, 1
	ADDI      r8, r0, 2
	CMP       r8, r8, r3
	bgtid     r8, ($BB87_64)
	NOP    
# BB#24:                                #   in Loop: Header=BB87_3 Depth=1
	FPDIV      r6, r7, r6
	ADD      r7, r3, r0
$BB87_25:                               # %for.body146.i
                                        #   Parent Loop BB87_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r8, r7, r5
	bslli     r8, r8, 2
	ADDI      r9, r1, 80
	LW         r10, r9, r8
	FPMUL      r10, r10, r6
	SW         r10, r9, r8
	ADDI      r7, r7, 1
	ADDI      r8, r0, 3
	CMP       r8, r8, r7
	bltid     r8, ($BB87_25)
	NOP    
$BB87_3:                                # %for.body20.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB87_25 Depth 2
                                        #     Child Loop BB87_5 Depth 2
                                        #     Child Loop BB87_59 Depth 2
                                        #       Child Loop BB87_60 Depth 3
                                        #     Child Loop BB87_47 Depth 2
	ORI       r6, r0, 0
	ADDI      r5, r0, 1
	CMP       r7, r5, r3
	bltid     r7, ($BB87_4)
	MULI      r5, r3, 3
# BB#46:                                # %for.body25.preheader.i
                                        #   in Loop: Header=BB87_3 Depth=1
	ADD      r7, r0, r0
$BB87_47:                               # %for.body25.i
                                        #   Parent Loop BB87_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADDI      r6, r0, 1
	CMP       r6, r6, r7
	bltid     r6, ($BB87_49)
	NOP    
# BB#48:                                # %for.end46.i
                                        #   in Loop: Header=BB87_47 Depth=2
	ADD      r8, r7, r5
	bslli     r9, r5, 2
	ADDI      r6, r1, 80
	LW         r9, r6, r9
	bslli     r10, r7, 2
	LW         r10, r6, r10
	FPMUL      r9, r10, r9
	bslli     r8, r8, 2
	LW         r10, r6, r8
	FPRSUB     r9, r9, r10
	SW         r9, r6, r8
$BB87_49:                               # %for.inc52.i
                                        #   in Loop: Header=BB87_47 Depth=2
	ORI       r6, r0, 0
	ADDI      r7, r7, 1
	CMP       r8, r3, r7
	bltid     r8, ($BB87_47)
	NOP    
# BB#50:                                #   in Loop: Header=BB87_3 Depth=1
	ADD      r7, r3, r0
$BB87_59:                               # %for.body67.lr.ph.us.i
                                        #   Parent Loop BB87_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB87_60 Depth 3
	ADD      r8, r7, r5
	bslli     r9, r8, 2
	ADDI      r10, r1, 80
	LW         r8, r10, r9
	ADD      r11, r0, r0
$BB87_60:                               # %for.body67.us.i
                                        #   Parent Loop BB87_3 Depth=1
                                        #     Parent Loop BB87_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	MULI      r12, r11, 3
	ADD      r12, r12, r7
	ADD      r21, r11, r5
	bslli     r21, r21, 2
	LW         r21, r10, r21
	bslli     r12, r12, 2
	LW         r12, r10, r12
	FPMUL      r12, r12, r21
	FPRSUB     r8, r12, r8
	ADDI      r11, r11, 1
	CMP       r12, r3, r11
	bltid     r12, ($BB87_60)
	NOP    
# BB#61:                                # %if.end85.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADDI      r10, r1, 80
	SW         r8, r10, r9
	ORI       r9, r0, 0
	FPLT   r9, r8, r9
	bneid     r9, ($BB87_63)
	ADDI      r10, r0, 1
# BB#62:                                # %if.end85.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADDI      r10, r0, 0
$BB87_63:                               # %if.end85.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	bslli     r9, r7, 2
	ADDI      r11, r1, 40
	beqid     r10, ($BB87_52)
	LW         r9, r11, r9
# BB#51:                                # %if.then.i173.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	FPNEG      r8, r8
$BB87_52:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	FPMUL      r8, r9, r8
	FPLT   r9, r8, r6
	FPUN   r10, r8, r6
	BITOR        r10, r10, r9
	bneid     r10, ($BB87_54)
	ADDI      r9, r0, 1
# BB#53:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADDI      r9, r0, 0
$BB87_54:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	bneid     r9, ($BB87_56)
	NOP    
# BB#55:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADD      r6, r8, r0
$BB87_56:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	bneid     r9, ($BB87_58)
	NOP    
# BB#57:                                # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADD      r4, r7, r0
$BB87_58:                               # %_ZN4util4fabsERKf.exit175.us.i
                                        #   in Loop: Header=BB87_59 Depth=2
	ADDI      r7, r7, 1
	ADDI      r8, r0, 3
	CMP       r8, r8, r7
	bgeid     r8, ($BB87_16)
	NOP    
	brid      ($BB87_59)
	NOP    
$BB87_4:                                #   in Loop: Header=BB87_3 Depth=1
	ADD      r7, r3, r0
$BB87_5:                                # %if.end85.i
                                        #   Parent Loop BB87_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ADD      r8, r7, r5
	bslli     r8, r8, 2
	ADDI      r9, r1, 80
	LW         r8, r9, r8
	ORI       r9, r0, 0
	FPGE   r10, r8, r9
	FPUN   r9, r8, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB87_7)
	ADDI      r10, r0, 1
# BB#6:                                 # %if.end85.i
                                        #   in Loop: Header=BB87_5 Depth=2
	ADDI      r10, r0, 0
$BB87_7:                                # %if.end85.i
                                        #   in Loop: Header=BB87_5 Depth=2
	bslli     r9, r7, 2
	ADDI      r11, r1, 40
	bneid     r10, ($BB87_9)
	LW         r9, r11, r9
# BB#8:                                 # %if.then.i173.i
                                        #   in Loop: Header=BB87_5 Depth=2
	FPNEG      r8, r8
$BB87_9:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	FPMUL      r8, r9, r8
	FPLT   r9, r8, r6
	FPUN   r10, r8, r6
	BITOR        r10, r10, r9
	bneid     r10, ($BB87_11)
	ADDI      r9, r0, 1
# BB#10:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	ADDI      r9, r0, 0
$BB87_11:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	bneid     r9, ($BB87_13)
	NOP    
# BB#12:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	ADD      r6, r8, r0
$BB87_13:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	bneid     r9, ($BB87_15)
	NOP    
# BB#14:                                # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	ADD      r4, r7, r0
$BB87_15:                               # %_ZN4util4fabsERKf.exit175.i
                                        #   in Loop: Header=BB87_5 Depth=2
	ADDI      r7, r7, 1
	ADDI      r8, r0, 3
	CMP       r8, r8, r7
	bltid     r8, ($BB87_5)
	NOP    
$BB87_16:                               # %for.end94.i
                                        #   in Loop: Header=BB87_3 Depth=1
	CMP       r6, r4, r3
	beqid     r6, ($BB87_18)
	NOP    
# BB#17:                                # %for.cond97.preheader.i
                                        #   in Loop: Header=BB87_3 Depth=1
	bslli     r6, r3, 2
	bslli     r7, r4, 2
	ADDI      r9, r1, 80
	LW         r8, r9, r7
	LW         r10, r9, r6
	SW         r10, r9, r7
	SW         r8, r9, r6
	ADD      r8, r9, r6
	ADD      r9, r9, r7
	LWI        r10, r9, 12
	LWI        r11, r8, 12
	SWI        r11, r9, 12
	SWI        r10, r8, 12
	LWI        r10, r9, 24
	LWI        r11, r8, 24
	SWI        r11, r9, 24
	SWI        r10, r8, 24
	ADDI      r8, r1, 40
	LW         r6, r8, r6
	SW         r6, r8, r7
$BB87_18:                               # %if.end122.i
                                        #   in Loop: Header=BB87_3 Depth=1
	bslli     r6, r3, 2
	ADDI      r7, r1, 68
	SW        r4, r7, r6
	ADDI      r6, r0, 2
	CMP       r6, r6, r3
	bneid     r6, ($BB87_19)
	NOP    
$BB87_64:                               # %for.end162.i
	ORI       r4, r0, 0
	LWI        r3, r1, 112
	FPNE   r5, r3, r4
	bneid     r5, ($BB87_66)
	ADDI      r4, r0, 1
# BB#65:                                # %for.end162.i
	ADDI      r4, r0, 0
$BB87_66:                               # %for.end162.i
	bneid     r4, ($BB87_68)
	NOP    
# BB#67:                                # %if.then167.i
	ADDI      r3, r0, 786163455
	SWI       r3, r1, 112
	ORI       r3, r0, 786163455
$BB87_68:                               # %for.cond5.preheader
	SWI       r0, r1, 64
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r5, r0, 1065353216
	SWI       r5, r1, 56
	LWI       r4, r1, 68
	bslli     r4, r4, 2
	ADDI      r8, r1, 56
	LW         r6, r8, r4
	SW        r5, r8, r4
	SWI        r6, r1, 56
	LWI       r5, r1, 72
	bslli     r5, r5, 2
	LW         r7, r8, r5
	LWI        r9, r1, 60
	SW         r9, r8, r5
	ORI       r8, r0, 0
	FPEQ   r6, r6, r8
	bneid     r6, ($BB87_70)
	ADDI      r8, r0, 1
# BB#69:                                # %for.cond5.preheader
	ADDI      r8, r0, 0
$BB87_70:                               # %for.cond5.preheader
	bneid     r8, ($BB87_72)
	ADDI      r6, r0, -1
# BB#71:                                # %for.cond5.preheader
	ADD      r6, r0, r0
$BB87_72:                               # %for.cond5.preheader
	beqid     r8, ($BB87_73)
	NOP    
# BB#98:                                # %if.else.1.i95
	ORI       r6, r0, 0
	FPEQ   r6, r7, r6
	bneid     r6, ($BB87_100)
	ADDI      r8, r0, 1
# BB#99:                                # %if.else.1.i95
	ADDI      r8, r0, 0
$BB87_100:                              # %if.else.1.i95
	bneid     r8, ($BB87_102)
	ADDI      r6, r0, -1
# BB#101:                               # %if.then13.1.i96
	brid      ($BB87_102)
	ADDI      r6, r0, 1
$BB87_73:
	ADD      r9, r6, r0
$BB87_74:                               # %for.body8.1.i93
                                        # =>This Inner Loop Header: Depth=1
	ADD      r8, r9, r0
	bslli     r9, r8, 2
	ADDI      r10, r1, 56
	LW         r9, r10, r9
	MULI      r10, r8, 3
	bslli     r10, r10, 2
	ADDI      r11, r1, 80
	ADD      r10, r10, r11
	LWI        r10, r10, 4
	FPMUL      r9, r10, r9
	FPRSUB     r7, r9, r7
	ADDI      r9, r8, 1
	bltid     r8, ($BB87_74)
	NOP    
$BB87_102:                              # %if.end14.1.i103
	SWI        r7, r1, 60
	LWI       r7, r1, 76
	bslli     r11, r7, 2
	ADDI      r12, r1, 56
	LW         r7, r12, r11
	LWI        r8, r1, 64
	SW         r8, r12, r11
	ADDI      r8, r0, -1
	CMP       r8, r8, r6
	beqid     r8, ($BB87_104)
	NOP    
$BB87_103:                              # %for.body8.2.i114
                                        # =>This Inner Loop Header: Depth=1
	bslli     r8, r6, 2
	ADDI      r9, r1, 56
	LW         r8, r9, r8
	MULI      r9, r6, 3
	bslli     r9, r9, 2
	ADDI      r10, r1, 80
	ADD      r9, r9, r10
	LWI        r9, r9, 8
	FPMUL      r8, r9, r8
	FPRSUB     r7, r8, r7
	ADDI      r6, r6, 1
	ADDI      r8, r0, 2
	CMP       r8, r8, r6
	bltid     r8, ($BB87_103)
	NOP    
$BB87_104:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	FPDIV      r9, r7, r3
	SWI        r9, r1, 64
	LWI        r6, r1, 108
	FPMUL      r7, r6, r9
	LWI        r8, r1, 60
	FPRSUB     r8, r7, r8
	LWI        r7, r1, 96
	FPDIV      r10, r8, r7
	SWI        r10, r1, 60
	LWI        r8, r1, 92
	FPMUL      r10, r8, r10
	LWI        r21, r1, 56
	FPRSUB     r21, r10, r21
	LWI        r10, r1, 104
	FPMUL      r9, r10, r9
	FPRSUB     r21, r9, r21
	LWI        r9, r1, 80
	FPDIV      r21, r21, r9
	SWI        r21, r1, 56
	LWI       r21, r1, 64
	SWI       r21, r20, 8
	LWI       r21, r1, 60
	SWI       r21, r20, 4
	LWI       r21, r1, 56
	SWI       r21, r20, 0
	SWI       r0, r1, 64
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r21, r0, 1065353216
	SWI       r21, r1, 60
	ADD      r22, r0, r0
	LW         r23, r12, r4
	SW        r22, r12, r4
	SWI        r23, r1, 56
	LW         r24, r12, r5
	LWI        r21, r1, 60
	SW         r21, r12, r5
	ORI       r12, r0, 0
	FPEQ   r12, r23, r12
	bneid     r12, ($BB87_106)
	ADDI      r25, r0, 1
# BB#105:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	ADDI      r25, r0, 0
$BB87_106:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	bneid     r25, ($BB87_108)
	ADDI      r21, r0, -1
# BB#107:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	ADD      r21, r22, r0
$BB87_108:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit115
	beqid     r25, ($BB87_109)
	ADDI      r12, r20, 12
# BB#114:                               # %if.else.1.i40
	SWI        r24, r1, 60
	ADDI      r21, r1, 56
	LW         r22, r21, r11
	LWI        r23, r1, 64
	SW         r23, r21, r11
	ORI       r21, r0, 0
	FPNE   r24, r24, r21
	ADDI      r21, r0, 1
	bneid     r24, ($BB87_116)
	ADD      r23, r21, r0
# BB#115:                               # %if.else.1.i40
	ADDI      r23, r0, 0
$BB87_116:                              # %if.else.1.i40
	beqid     r23, ($BB87_118)
	NOP    
	brid      ($BB87_117)
	NOP    
$BB87_109:
	ADD      r26, r21, r0
$BB87_110:                              # %for.body8.1.i38
                                        # =>This Inner Loop Header: Depth=1
	ADD      r22, r26, r0
	bslli     r26, r22, 2
	ADDI      r25, r1, 56
	LW         r26, r25, r26
	MULI      r27, r22, 3
	bslli     r27, r27, 2
	ADDI      r28, r1, 80
	ADD      r27, r27, r28
	LWI        r27, r27, 4
	FPMUL      r26, r27, r26
	FPRSUB     r24, r26, r24
	ADDI      r26, r22, 1
	bltid     r22, ($BB87_110)
	NOP    
# BB#111:                               # %if.end14.1.i48
	SWI        r24, r1, 60
	LW         r22, r25, r11
	LWI        r24, r1, 64
	SW         r24, r25, r11
	ORI       r24, r0, 0
	FPEQ   r24, r23, r24
	bneid     r24, ($BB87_113)
	ADDI      r23, r0, 1
# BB#112:                               # %if.end14.1.i48
	ADDI      r23, r0, 0
$BB87_113:                              # %if.end14.1.i48
	bneid     r23, ($BB87_118)
	NOP    
$BB87_117:                              # %for.body8.2.i59
                                        # =>This Inner Loop Header: Depth=1
	bslli     r23, r21, 2
	ADDI      r24, r1, 56
	LW         r23, r24, r23
	MULI      r24, r21, 3
	bslli     r24, r24, 2
	ADDI      r25, r1, 80
	ADD      r24, r24, r25
	LWI        r24, r24, 8
	FPMUL      r23, r24, r23
	FPRSUB     r22, r23, r22
	ADDI      r21, r21, 1
	ADDI      r23, r0, 2
	CMP       r23, r23, r21
	bltid     r23, ($BB87_117)
	NOP    
$BB87_118:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	FPDIV      r21, r22, r3
	SWI        r21, r1, 64
	FPMUL      r22, r6, r21
	FPMUL      r21, r10, r21
	LWI        r23, r1, 60
	FPRSUB     r22, r22, r23
	FPDIV      r22, r22, r7
	SWI        r22, r1, 60
	FPMUL      r22, r8, r22
	LWI        r23, r1, 56
	FPRSUB     r22, r22, r23
	FPRSUB     r21, r21, r22
	FPDIV      r21, r21, r9
	SWI        r21, r1, 56
	LWI       r21, r1, 64
	SWI       r21, r12, 8
	LWI       r21, r1, 60
	SWI       r21, r12, 4
	LWI       r21, r1, 56
	SWI       r21, r12, 0
	SWI       r0, r1, 60
	SWI       r0, r1, 56
	ADDI      r12, r0, 1065353216
	SWI       r12, r1, 64
	ADD      r12, r0, r0
	ADDI      r23, r1, 56
	LW         r21, r23, r4
	SW        r12, r23, r4
	SWI        r21, r1, 56
	LW         r22, r23, r5
	LWI        r4, r1, 60
	SW         r4, r23, r5
	ORI       r4, r0, 0
	FPEQ   r4, r21, r4
	bneid     r4, ($BB87_120)
	ADDI      r23, r0, 1
# BB#119:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	ADDI      r23, r0, 0
$BB87_120:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	bneid     r23, ($BB87_122)
	ADDI      r5, r0, -1
# BB#121:                               # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	ADD      r5, r12, r0
$BB87_122:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit60
	beqid     r23, ($BB87_123)
	ADDI      r4, r20, 24
# BB#128:                               # %if.else.1.i
	SWI        r22, r1, 60
	ADDI      r5, r1, 56
	LW         r12, r5, r11
	LWI        r20, r1, 64
	SW         r20, r5, r11
	ORI       r5, r0, 0
	FPNE   r20, r22, r5
	ADDI      r5, r0, 1
	bneid     r20, ($BB87_130)
	ADD      r11, r5, r0
# BB#129:                               # %if.else.1.i
	ADDI      r11, r0, 0
$BB87_130:                              # %if.else.1.i
	beqid     r11, ($BB87_132)
	NOP    
	brid      ($BB87_131)
	NOP    
$BB87_123:
	ADD      r23, r5, r0
$BB87_124:                              # %for.body8.1.i
                                        # =>This Inner Loop Header: Depth=1
	ADD      r12, r23, r0
	bslli     r23, r12, 2
	ADDI      r20, r1, 56
	LW         r23, r20, r23
	MULI      r24, r12, 3
	bslli     r24, r24, 2
	ADDI      r25, r1, 80
	ADD      r24, r24, r25
	LWI        r24, r24, 4
	FPMUL      r23, r24, r23
	FPRSUB     r22, r23, r22
	ADDI      r23, r12, 1
	bltid     r12, ($BB87_124)
	NOP    
# BB#125:                               # %if.end14.1.i
	SWI        r22, r1, 60
	LW         r12, r20, r11
	LWI        r22, r1, 64
	SW         r22, r20, r11
	ORI       r11, r0, 0
	FPEQ   r20, r21, r11
	bneid     r20, ($BB87_127)
	ADDI      r11, r0, 1
# BB#126:                               # %if.end14.1.i
	ADDI      r11, r0, 0
$BB87_127:                              # %if.end14.1.i
	bneid     r11, ($BB87_132)
	NOP    
$BB87_131:                              # %for.body8.2.i
                                        # =>This Inner Loop Header: Depth=1
	bslli     r11, r5, 2
	ADDI      r20, r1, 56
	LW         r11, r20, r11
	MULI      r20, r5, 3
	bslli     r20, r20, 2
	ADDI      r21, r1, 80
	ADD      r20, r20, r21
	LWI        r20, r20, 8
	FPMUL      r11, r20, r11
	FPRSUB     r12, r11, r12
	ADDI      r5, r5, 1
	ADDI      r11, r0, 2
	CMP       r11, r11, r5
	bltid     r11, ($BB87_131)
	NOP    
$BB87_132:                              # %_ZN8syMatrix5LUBKSEPKiPf.exit
	FPDIV      r3, r12, r3
	SWI        r3, r1, 64
	FPMUL      r5, r6, r3
	FPMUL      r3, r10, r3
	LWI        r6, r1, 60
	FPRSUB     r5, r5, r6
	FPDIV      r5, r5, r7
	SWI        r5, r1, 60
	FPMUL      r5, r8, r5
	LWI        r6, r1, 56
	FPRSUB     r5, r5, r6
	FPRSUB     r3, r3, r5
	FPDIV      r3, r3, r9
	SWI        r3, r1, 56
	LWI       r3, r1, 64
	SWI       r3, r4, 8
	LWI       r3, r1, 60
	SWI       r3, r4, 4
	LWI       r3, r1, 56
	SWI       r3, r4, 0
$BB87_133:                              # %for.end29
	LWI       r28, r1, 4
	LWI       r27, r1, 8
	LWI       r26, r1, 12
	LWI       r25, r1, 16
	LWI       r24, r1, 20
	LWI       r23, r1, 24
	LWI       r22, r1, 28
	LWI       r21, r1, 32
	LWI       r20, r1, 36
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 116
	.end	_ZN8syMatrix6InvertEv
$tmp87:
	.size	_ZN8syMatrix6InvertEv, ($tmp87)-_ZN8syMatrix6InvertEv

	.globl	_ZN8syMatrixmLERKS_
	.align	2
	.type	_ZN8syMatrixmLERKS_,@function
	.ent	_ZN8syMatrixmLERKS_     # @_ZN8syMatrixmLERKS_
_ZN8syMatrixmLERKS_:
	.frame	r1,68,r15
	.mask	0x3f08000
# BB#0:                                 # %entry
	ADDI      r1, r1, -68
	SWI       r15, r1, 0
	SWI       r20, r1, 24
	SWI       r21, r1, 20
	SWI       r22, r1, 16
	SWI       r23, r1, 12
	SWI       r24, r1, 8
	SWI       r25, r1, 4
	ADD      r20, r5, r0
	LWI        r7, r6, 0
	LWI        r21, r20, 0
	FPMUL      r4, r21, r7
	ORI       r3, r0, 0
	FPADD      r4, r4, r3
	LWI        r11, r6, 4
	LWI        r22, r20, 12
	FPMUL      r5, r22, r11
	FPADD      r4, r4, r5
	LWI        r9, r6, 8
	LWI        r23, r20, 24
	FPMUL      r5, r23, r9
	FPADD      r24, r4, r5
	LWI        r4, r6, 32
	LWI        r5, r6, 24
	LWI        r8, r6, 28
	LWI        r10, r6, 20
	LWI        r12, r6, 16
	LWI        r6, r6, 12
	SWI        r24, r1, 32
	FPMUL      r24, r21, r6
	FPADD      r24, r24, r3
	FPMUL      r25, r22, r12
	FPADD      r24, r24, r25
	FPMUL      r25, r23, r10
	FPADD      r24, r24, r25
	SWI        r24, r1, 44
	FPMUL      r22, r22, r8
	FPMUL      r21, r21, r5
	FPADD      r21, r21, r3
	FPADD      r21, r21, r22
	FPMUL      r22, r23, r4
	FPADD      r21, r21, r22
	SWI        r21, r1, 56
	LWI        r21, r20, 4
	FPMUL      r22, r21, r7
	FPADD      r22, r22, r3
	LWI        r23, r20, 16
	FPMUL      r24, r23, r11
	FPADD      r24, r22, r24
	LWI        r22, r20, 28
	FPMUL      r25, r22, r9
	FPADD      r24, r24, r25
	SWI        r24, r1, 36
	FPMUL      r24, r21, r6
	FPADD      r24, r24, r3
	FPMUL      r25, r23, r12
	FPADD      r24, r24, r25
	FPMUL      r25, r22, r10
	FPADD      r24, r24, r25
	SWI        r24, r1, 48
	FPMUL      r23, r23, r8
	FPMUL      r21, r21, r5
	FPADD      r21, r21, r3
	FPADD      r21, r21, r23
	FPMUL      r22, r22, r4
	FPADD      r21, r21, r22
	SWI        r21, r1, 60
	LWI        r21, r20, 20
	FPMUL      r22, r21, r11
	LWI        r11, r20, 8
	FPMUL      r7, r11, r7
	FPADD      r7, r7, r3
	FPADD      r22, r7, r22
	LWI        r7, r20, 32
	FPMUL      r9, r7, r9
	FPADD      r9, r22, r9
	SWI        r9, r1, 40
	FPMUL      r9, r21, r12
	FPMUL      r6, r11, r6
	FPADD      r6, r6, r3
	FPADD      r6, r6, r9
	FPMUL      r9, r7, r10
	FPADD      r6, r6, r9
	SWI        r6, r1, 52
	FPMUL      r6, r21, r8
	FPMUL      r5, r11, r5
	FPADD      r3, r5, r3
	FPADD      r3, r3, r6
	FPMUL      r4, r7, r4
	FPADD      r3, r3, r4
	SWI        r3, r1, 64
	ADDI      r6, r1, 32
	ADDI      r7, r0, 36
	brlid     r15, memcpy
	ADD      r5, r20, r0
	ADD      r3, r20, r0
	LWI       r25, r1, 4
	LWI       r24, r1, 8
	LWI       r23, r1, 12
	LWI       r22, r1, 16
	LWI       r21, r1, 20
	LWI       r20, r1, 24
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 68
	.end	_ZN8syMatrixmLERKS_
$tmp88:
	.size	_ZN8syMatrixmLERKS_, ($tmp88)-_ZN8syMatrixmLERKS_

	.globl	_ZNK8syMatrixmlERK8syVector
	.align	2
	.type	_ZNK8syMatrixmlERK8syVector,@function
	.ent	_ZNK8syMatrixmlERK8syVector # @_ZNK8syMatrixmlERK8syVector
_ZNK8syMatrixmlERK8syVector:
	.frame	r1,4,r15
	.mask	0x100000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r20, r1, 0
	LWI        r4, r6, 12
	LWI        r3, r7, 4
	FPMUL      r8, r3, r4
	LWI        r9, r6, 0
	LWI        r4, r7, 0
	FPMUL      r9, r4, r9
	FPADD      r8, r9, r8
	LWI        r7, r7, 8
	LWI        r9, r6, 24
	FPMUL      r9, r7, r9
	FPADD      r11, r8, r9
	LWI        r8, r6, 32
	LWI        r9, r6, 8
	LWI        r10, r6, 20
	LWI        r12, r6, 28
	LWI        r20, r6, 4
	LWI        r6, r6, 16
	SWI        r11, r5, 0
	FPMUL      r6, r3, r6
	FPMUL      r11, r4, r20
	FPADD      r6, r11, r6
	FPMUL      r11, r7, r12
	FPADD      r6, r6, r11
	SWI        r6, r5, 4
	FPMUL      r3, r3, r10
	FPMUL      r4, r4, r9
	FPADD      r3, r4, r3
	FPMUL      r4, r7, r8
	FPADD      r3, r3, r4
	SWI        r3, r5, 8
	LWI       r20, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_ZNK8syMatrixmlERK8syVector
$tmp89:
	.size	_ZNK8syMatrixmlERK8syVector, ($tmp89)-_ZNK8syMatrixmlERK8syVector

	.globl	_ZN8sySphereC2Ev
	.align	2
	.type	_ZN8sySphereC2Ev,@function
	.ent	_ZN8sySphereC2Ev        # @_ZN8sySphereC2Ev
_ZN8sySphereC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN8sySphereC2Ev
$tmp90:
	.size	_ZN8sySphereC2Ev, ($tmp90)-_ZN8sySphereC2Ev

	.globl	_ZN8sySphereC2ERK8syVectorRKfRKiS6_
	.align	2
	.type	_ZN8sySphereC2ERK8syVectorRKfRKiS6_,@function
	.ent	_ZN8sySphereC2ERK8syVectorRKfRKiS6_ # @_ZN8sySphereC2ERK8syVectorRKfRKiS6_
_ZN8sySphereC2ERK8syVectorRKfRKiS6_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 8
	LWI        r3, r6, 4
	SWI        r3, r5, 12
	LWI        r3, r6, 8
	SWI        r3, r5, 16
	LWI        r3, r7, 0
	SWI        r3, r5, 20
	LWI       r3, r8, 0
	SWI       r3, r5, 0
	LWI       r3, r9, 0
	rtsd      r15, 8
	SWI       r3, r5, 4
	.end	_ZN8sySphereC2ERK8syVectorRKfRKiS6_
$tmp91:
	.size	_ZN8sySphereC2ERK8syVectorRKfRKiS6_, ($tmp91)-_ZN8sySphereC2ERK8syVectorRKfRKiS6_

	.globl	_ZN8sySphere3SetERK8syVectorRKfRKiS6_
	.align	2
	.type	_ZN8sySphere3SetERK8syVectorRKfRKiS6_,@function
	.ent	_ZN8sySphere3SetERK8syVectorRKfRKiS6_ # @_ZN8sySphere3SetERK8syVectorRKfRKiS6_
_ZN8sySphere3SetERK8syVectorRKfRKiS6_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 8
	LWI        r3, r6, 4
	SWI        r3, r5, 12
	LWI        r3, r6, 8
	SWI        r3, r5, 16
	LWI        r3, r7, 0
	SWI        r3, r5, 20
	LWI       r3, r8, 0
	SWI       r3, r5, 0
	LWI       r3, r9, 0
	rtsd      r15, 8
	SWI       r3, r5, 4
	.end	_ZN8sySphere3SetERK8syVectorRKfRKiS6_
$tmp92:
	.size	_ZN8sySphere3SetERK8syVectorRKfRKiS6_, ($tmp92)-_ZN8sySphere3SetERK8syVectorRKfRKiS6_

	.globl	_ZN8sySphere3SetERK8syVectorRKf
	.align	2
	.type	_ZN8sySphere3SetERK8syVectorRKf,@function
	.ent	_ZN8sySphere3SetERK8syVectorRKf # @_ZN8sySphere3SetERK8syVectorRKf
_ZN8sySphere3SetERK8syVectorRKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 8
	LWI        r3, r6, 4
	SWI        r3, r5, 12
	LWI        r3, r6, 8
	SWI        r3, r5, 16
	LWI        r3, r7, 0
	rtsd      r15, 8
	SWI        r3, r5, 20
	.end	_ZN8sySphere3SetERK8syVectorRKf
$tmp93:
	.size	_ZN8sySphere3SetERK8syVectorRKf, ($tmp93)-_ZN8sySphere3SetERK8syVectorRKf

	.globl	_ZNK8sySphere20IntersectsWShadowRayERK3Ray
	.align	2
	.type	_ZNK8sySphere20IntersectsWShadowRayERK3Ray,@function
	.ent	_ZNK8sySphere20IntersectsWShadowRayERK3Ray # @_ZNK8sySphere20IntersectsWShadowRayERK3Ray
_ZNK8sySphere20IntersectsWShadowRayERK3Ray:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 8
	LWI        r4, r6, 0
	FPRSUB     r3, r3, r4
	LWI        r4, r5, 12
	LWI        r7, r6, 4
	FPRSUB     r4, r4, r7
	FPMUL      r7, r4, r4
	FPMUL      r8, r3, r3
	FPADD      r8, r8, r7
	LWI        r7, r5, 16
	LWI        r9, r6, 8
	FPRSUB     r7, r7, r9
	FPMUL      r9, r7, r7
	FPADD      r8, r8, r9
	LWI        r5, r5, 20
	FPMUL      r5, r5, r5
	FPRSUB     r8, r5, r8
	LWI        r9, r6, 16
	FPMUL      r5, r9, r9
	LWI        r10, r6, 12
	FPMUL      r11, r10, r10
	FPADD      r5, r11, r5
	LWI        r6, r6, 20
	FPMUL      r11, r6, r6
	FPADD      r5, r5, r11
	ORI       r11, r0, -1065353216
	FPMUL      r11, r5, r11
	FPMUL      r8, r11, r8
	FPMUL      r4, r9, r4
	FPMUL      r3, r10, r3
	FPADD      r3, r3, r4
	FPMUL      r4, r6, r7
	FPADD      r3, r3, r4
	FPADD      r4, r3, r3
	FPMUL      r3, r4, r4
	FPADD      r6, r3, r8
	ORI       r3, r0, 0
	FPLT   r3, r6, r3
	bneid     r3, ($BB94_2)
	ADDI      r7, r0, 1
# BB#1:                                 # %entry
	ADDI      r7, r0, 0
$BB94_2:                                # %entry
	ORI       r3, r0, 1203982336
	bneid     r7, ($BB94_10)
	NOP    
# BB#3:                                 # %if.end
	ORI       r3, r0, 1056964608
	FPDIV      r5, r3, r5
	FPINVSQRT r3, r6
	ORI       r6, r0, 1065353216
	FPDIV      r6, r6, r3
	FPNEG      r3, r4
	FPRSUB     r3, r6, r3
	FPMUL      r3, r3, r5
	ORI       r7, r0, 0
	FPGT   r8, r3, r7
	bneid     r8, ($BB94_5)
	ADDI      r7, r0, 1
# BB#4:                                 # %if.end
	ADDI      r7, r0, 0
$BB94_5:                                # %if.end
	bneid     r7, ($BB94_10)
	NOP    
# BB#6:                                 # %if.end19
	FPRSUB     r3, r4, r6
	FPMUL      r3, r3, r5
	ORI       r4, r0, 0
	FPGT   r5, r3, r4
	bneid     r5, ($BB94_8)
	ADDI      r4, r0, 1
# BB#7:                                 # %if.end19
	ADDI      r4, r0, 0
$BB94_8:                                # %if.end19
	ORI       r5, r0, 1203982336
	bneid     r4, ($BB94_10)
	NOP    
# BB#9:                                 # %if.end19
	ADD      r3, r5, r0
$BB94_10:                               # %return
	rtsd      r15, 8
	NOP    
	.end	_ZNK8sySphere20IntersectsWShadowRayERK3Ray
$tmp94:
	.size	_ZNK8sySphere20IntersectsWShadowRayERK3Ray, ($tmp94)-_ZNK8sySphere20IntersectsWShadowRayERK3Ray

	.globl	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
	.align	2
	.type	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord,@function
	.ent	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord # @_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
_ZNK8sySphere11IntersectsWERK3RayR9HitRecord:
	.frame	r1,4,r15
	.mask	0x100000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r20, r1, 0
	LWI        r3, r5, 8
	LWI        r4, r6, 0
	FPRSUB     r3, r3, r4
	LWI        r4, r5, 12
	LWI        r8, r6, 4
	FPRSUB     r4, r4, r8
	FPMUL      r8, r4, r4
	FPMUL      r9, r3, r3
	FPADD      r9, r9, r8
	LWI        r8, r5, 16
	LWI        r10, r6, 8
	FPRSUB     r8, r8, r10
	FPMUL      r10, r8, r8
	FPADD      r9, r9, r10
	LWI        r10, r5, 20
	FPMUL      r10, r10, r10
	FPRSUB     r9, r10, r9
	LWI        r10, r6, 16
	FPMUL      r12, r10, r10
	LWI        r11, r6, 12
	FPMUL      r20, r11, r11
	FPADD      r20, r20, r12
	LWI        r12, r6, 20
	FPMUL      r6, r12, r12
	FPADD      r6, r20, r6
	ORI       r20, r0, -1065353216
	FPMUL      r20, r6, r20
	FPMUL      r9, r20, r9
	FPMUL      r4, r10, r4
	FPMUL      r3, r11, r3
	FPADD      r3, r3, r4
	FPMUL      r4, r12, r8
	FPADD      r3, r3, r4
	FPADD      r4, r3, r3
	FPMUL      r3, r4, r4
	FPADD      r8, r3, r9
	ORI       r3, r0, 0
	FPLT   r3, r8, r3
	bneid     r3, ($BB95_2)
	ADDI      r9, r0, 1
# BB#1:                                 # %entry
	ADDI      r9, r0, 0
$BB95_2:                                # %entry
	bneid     r9, ($BB95_18)
	ADD      r3, r0, r0
# BB#3:                                 # %if.end
	ORI       r3, r0, 1056964608
	FPDIV      r3, r3, r6
	FPINVSQRT r6, r8
	ORI       r8, r0, 1065353216
	FPDIV      r6, r8, r6
	FPNEG      r8, r4
	FPRSUB     r8, r6, r8
	FPMUL      r8, r8, r3
	ORI       r9, r0, 0
	FPLE   r10, r8, r9
	FPUN   r9, r8, r9
	BITOR        r10, r9, r10
	bneid     r10, ($BB95_5)
	ADDI      r9, r0, 1
# BB#4:                                 # %if.end
	ADDI      r9, r0, 0
$BB95_5:                                # %if.end
	bneid     r9, ($BB95_10)
	NOP    
# BB#6:                                 # %land.lhs.true
	LWI        r9, r7, 0
	FPGE   r10, r8, r9
	FPUN   r9, r8, r9
	BITOR        r10, r9, r10
	bneid     r10, ($BB95_8)
	ADDI      r9, r0, 1
# BB#7:                                 # %land.lhs.true
	ADDI      r9, r0, 0
$BB95_8:                                # %land.lhs.true
	bneid     r9, ($BB95_10)
	NOP    
# BB#9:                                 # %if.then19
	brid      ($BB95_17)
	SWI        r8, r7, 0
$BB95_10:                               # %if.end23
	FPRSUB     r4, r4, r6
	FPMUL      r4, r4, r3
	ORI       r3, r0, 0
	FPLE   r6, r4, r3
	FPUN   r3, r4, r3
	BITOR        r3, r3, r6
	bneid     r3, ($BB95_12)
	ADDI      r6, r0, 1
# BB#11:                                # %if.end23
	ADDI      r6, r0, 0
$BB95_12:                               # %if.end23
	bneid     r6, ($BB95_18)
	ADD      r3, r0, r0
# BB#13:                                # %land.lhs.true29
	LWI        r3, r7, 0
	FPGE   r6, r4, r3
	FPUN   r3, r4, r3
	BITOR        r3, r3, r6
	bneid     r3, ($BB95_15)
	ADDI      r6, r0, 1
# BB#14:                                # %land.lhs.true29
	ADDI      r6, r0, 0
$BB95_15:                               # %land.lhs.true29
	bneid     r6, ($BB95_18)
	ADD      r3, r0, r0
# BB#16:                                # %if.then32
	SWI        r4, r7, 0
$BB95_17:                               # %if.then32
	LWI       r3, r5, 4
	SWI       r3, r7, 4
	LWI       r3, r5, 0
	SWI       r3, r7, 8
	ADDI      r3, r0, 1
$BB95_18:                               # %return
	LWI       r20, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord
$tmp95:
	.size	_ZNK8sySphere11IntersectsWERK3RayR9HitRecord, ($tmp95)-_ZNK8sySphere11IntersectsWERK3RayR9HitRecord

	.globl	_ZNK8sySphere6ID_MATEv
	.align	2
	.type	_ZNK8sySphere6ID_MATEv,@function
	.ent	_ZNK8sySphere6ID_MATEv  # @_ZNK8sySphere6ID_MATEv
_ZNK8sySphere6ID_MATEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADDI      r3, r5, 4
	.end	_ZNK8sySphere6ID_MATEv
$tmp96:
	.size	_ZNK8sySphere6ID_MATEv, ($tmp96)-_ZNK8sySphere6ID_MATEv

	.globl	_ZNK8sySphere6ID_OBJEv
	.align	2
	.type	_ZNK8sySphere6ID_OBJEv,@function
	.ent	_ZNK8sySphere6ID_OBJEv  # @_ZNK8sySphere6ID_OBJEv
_ZNK8sySphere6ID_OBJEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZNK8sySphere6ID_OBJEv
$tmp97:
	.size	_ZNK8sySphere6ID_OBJEv, ($tmp97)-_ZNK8sySphere6ID_OBJEv

	.globl	_ZNK8sySphere6NormalERK8syVector
	.align	2
	.type	_ZNK8sySphere6NormalERK8syVector,@function
	.ent	_ZNK8sySphere6NormalERK8syVector # @_ZNK8sySphere6NormalERK8syVector
_ZNK8sySphere6NormalERK8syVector:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 8
	LWI        r4, r7, 0
	FPRSUB     r3, r3, r4
	LWI        r4, r6, 12
	LWI        r8, r7, 4
	FPRSUB     r4, r4, r8
	FPMUL      r8, r4, r4
	FPMUL      r9, r3, r3
	FPADD      r8, r9, r8
	LWI        r6, r6, 16
	LWI        r7, r7, 8
	FPRSUB     r6, r6, r7
	FPMUL      r7, r6, r6
	FPADD      r7, r8, r7
	FPINVSQRT r7, r7
	ORI       r8, r0, 1065353216
	FPDIV      r7, r8, r7
	FPDIV      r3, r3, r7
	SWI        r3, r5, 0
	FPDIV      r3, r4, r7
	SWI        r3, r5, 4
	FPDIV      r3, r6, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8sySphere6NormalERK8syVector
$tmp98:
	.size	_ZNK8sySphere6NormalERK8syVector, ($tmp98)-_ZNK8sySphere6NormalERK8syVector

	.globl	_ZN10syTriangleC2Ei
	.align	2
	.type	_ZN10syTriangleC2Ei,@function
	.ent	_ZN10syTriangleC2Ei     # @_ZN10syTriangleC2Ei
_ZN10syTriangleC2Ei:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r6, r5, 36
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LWI       r3, r5, 36
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r5, 36
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r5, 36
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	LWI       r3, r5, 36
	ADDI      r3, r3, 6
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	SWI        r3, r5, 32
	ADD      r3, r0, r0
	sbi       r3, r5, 40
	rtsd      r15, 8
	sbi       r3, r5, 48
	.end	_ZN10syTriangleC2Ei
$tmp99:
	.size	_ZN10syTriangleC2Ei, ($tmp99)-_ZN10syTriangleC2Ei

	.globl	_ZN10syTriangle14LoadFromMemoryEi
	.align	2
	.type	_ZN10syTriangle14LoadFromMemoryEi,@function
	.ent	_ZN10syTriangle14LoadFromMemoryEi # @_ZN10syTriangle14LoadFromMemoryEi
_ZN10syTriangle14LoadFromMemoryEi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r6, r5, 36
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LWI       r3, r5, 36
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r5, 36
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r5, 36
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	LWI       r3, r5, 36
	ADDI      r3, r3, 6
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	SWI        r3, r5, 32
	ADD      r3, r0, r0
	sbi       r3, r5, 40
	rtsd      r15, 8
	sbi       r3, r5, 48
	.end	_ZN10syTriangle14LoadFromMemoryEi
$tmp100:
	.size	_ZN10syTriangle14LoadFromMemoryEi, ($tmp100)-_ZN10syTriangle14LoadFromMemoryEi

	.globl	_ZN10syTriangleC2Ev
	.align	2
	.type	_ZN10syTriangleC2Ev,@function
	.ent	_ZN10syTriangleC2Ev     # @_ZN10syTriangleC2Ev
_ZN10syTriangleC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN10syTriangleC2Ev
$tmp101:
	.size	_ZN10syTriangleC2Ev, ($tmp101)-_ZN10syTriangleC2Ev

	.globl	_ZN10syTriangleC2ERKS_
	.align	2
	.type	_ZN10syTriangleC2ERKS_,@function
	.ent	_ZN10syTriangleC2ERKS_  # @_ZN10syTriangleC2ERKS_
_ZN10syTriangleC2ERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r6, 12
	SWI        r3, r5, 12
	LWI        r3, r6, 16
	SWI        r3, r5, 16
	LWI        r3, r6, 20
	SWI        r3, r5, 20
	LWI        r3, r6, 24
	SWI        r3, r5, 24
	LWI        r3, r6, 28
	SWI        r3, r5, 28
	LWI        r3, r6, 32
	SWI        r3, r5, 32
	ADD      r3, r0, r0
	rtsd      r15, 8
	sbi       r3, r5, 48
	.end	_ZN10syTriangleC2ERKS_
$tmp102:
	.size	_ZN10syTriangleC2ERKS_, ($tmp102)-_ZN10syTriangleC2ERKS_

	.globl	_ZN10syTriangle3SetERK8syVectorS2_S2_
	.align	2
	.type	_ZN10syTriangle3SetERK8syVectorS2_S2_,@function
	.ent	_ZN10syTriangle3SetERK8syVectorS2_S2_ # @_ZN10syTriangle3SetERK8syVectorS2_S2_
_ZN10syTriangle3SetERK8syVectorS2_S2_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r7, 0
	SWI        r3, r5, 12
	LWI        r3, r7, 4
	SWI        r3, r5, 16
	LWI        r3, r7, 8
	SWI        r3, r5, 20
	LWI        r3, r8, 0
	SWI        r3, r5, 24
	LWI        r3, r8, 4
	SWI        r3, r5, 28
	LWI        r3, r8, 8
	SWI        r3, r5, 32
	ADD      r3, r0, r0
	rtsd      r15, 8
	sbi       r3, r5, 48
	.end	_ZN10syTriangle3SetERK8syVectorS2_S2_
$tmp103:
	.size	_ZN10syTriangle3SetERK8syVectorS2_S2_, ($tmp103)-_ZN10syTriangle3SetERK8syVectorS2_S2_

	.globl	_ZN10syTriangleC2ERK8syVectorS2_S2_
	.align	2
	.type	_ZN10syTriangleC2ERK8syVectorS2_S2_,@function
	.ent	_ZN10syTriangleC2ERK8syVectorS2_S2_ # @_ZN10syTriangleC2ERK8syVectorS2_S2_
_ZN10syTriangleC2ERK8syVectorS2_S2_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r3, r7, 0
	SWI        r3, r5, 12
	LWI        r3, r7, 4
	SWI        r3, r5, 16
	LWI        r3, r7, 8
	SWI        r3, r5, 20
	LWI        r3, r8, 0
	SWI        r3, r5, 24
	LWI        r3, r8, 4
	SWI        r3, r5, 28
	LWI        r3, r8, 8
	SWI        r3, r5, 32
	ADD      r3, r0, r0
	rtsd      r15, 8
	sbi       r3, r5, 48
	.end	_ZN10syTriangleC2ERK8syVectorS2_S2_
$tmp104:
	.size	_ZN10syTriangleC2ERK8syVectorS2_S2_, ($tmp104)-_ZN10syTriangleC2ERK8syVectorS2_S2_

	.globl	_ZN10syTriangle10LoadVertexEv
	.align	2
	.type	_ZN10syTriangle10LoadVertexEv,@function
	.ent	_ZN10syTriangle10LoadVertexEv # @_ZN10syTriangle10LoadVertexEv
_ZN10syTriangle10LoadVertexEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r5, 36
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r5, 36
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r5, 36
	LOAD      r3, r3, 2
	SWI        r3, r5, 8
	LWI       r3, r5, 36
	ADDI      r3, r3, 3
	LOAD      r4, r3, 0
	SWI        r4, r5, 12
	LOAD      r4, r3, 1
	SWI        r4, r5, 16
	LOAD      r3, r3, 2
	SWI        r3, r5, 20
	LWI       r3, r5, 36
	ADDI      r3, r3, 6
	LOAD      r4, r3, 0
	SWI        r4, r5, 24
	LOAD      r4, r3, 1
	SWI        r4, r5, 28
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 32
	.end	_ZN10syTriangle10LoadVertexEv
$tmp105:
	.size	_ZN10syTriangle10LoadVertexEv, ($tmp105)-_ZN10syTriangle10LoadVertexEv

	.globl	_ZNK10syTriangle12LoadMaterialEv
	.align	2
	.type	_ZNK10syTriangle12LoadMaterialEv,@function
	.ent	_ZNK10syTriangle12LoadMaterialEv # @_ZNK10syTriangle12LoadMaterialEv
_ZNK10syTriangle12LoadMaterialEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r5, 36
	ADDI      r3, r3, 10
	LOAD      r3, r3, 0
	rtsd      r15, 8
	SWI       r3, r5, 44
	.end	_ZNK10syTriangle12LoadMaterialEv
$tmp106:
	.size	_ZNK10syTriangle12LoadMaterialEv, ($tmp106)-_ZNK10syTriangle12LoadMaterialEv

	.globl	_ZNK10syTriangle6NormalEv
	.align	2
	.type	_ZNK10syTriangle6NormalEv,@function
	.ent	_ZNK10syTriangle6NormalEv # @_ZNK10syTriangle6NormalEv
_ZNK10syTriangle6NormalEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	lbui      r3, r6, 48
	bneid     r3, ($BB107_2)
	NOP    
# BB#1:                                 # %if.end
	LWI        r4, r6, 20
	LWI        r3, r6, 8
	FPRSUB     r7, r4, r3
	LWI        r4, r6, 32
	FPRSUB     r8, r4, r3
	LWI        r3, r6, 24
	LWI        r4, r6, 0
	FPRSUB     r3, r3, r4
	LWI        r9, r6, 12
	FPRSUB     r4, r9, r4
	FPMUL      r9, r4, r8
	FPMUL      r10, r7, r3
	FPRSUB     r9, r9, r10
	LWI        r10, r6, 16
	LWI        r11, r6, 4
	FPRSUB     r10, r10, r11
	LWI        r12, r6, 28
	FPRSUB     r11, r12, r11
	FPMUL      r7, r7, r11
	FPMUL      r8, r10, r8
	FPRSUB     r7, r7, r8
	SWI        r7, r6, 52
	SWI        r9, r6, 56
	FPMUL      r8, r9, r9
	FPMUL      r7, r7, r7
	FPADD      r7, r7, r8
	FPMUL      r3, r10, r3
	FPMUL      r4, r4, r11
	FPRSUB     r3, r3, r4
	SWI        r3, r6, 60
	FPMUL      r3, r3, r3
	FPADD      r3, r7, r3
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	LWI        r4, r6, 52
	FPDIV      r4, r4, r3
	SWI        r4, r6, 52
	LWI        r4, r6, 56
	FPDIV      r4, r4, r3
	SWI        r4, r6, 56
	LWI        r4, r6, 60
	FPDIV      r3, r4, r3
	SWI        r3, r6, 60
	ADDI      r3, r0, 1
	sbi       r3, r6, 48
$BB107_2:                               # %if.end
	LWI       r3, r6, 60
	SWI       r3, r5, 8
	LWI       r3, r6, 56
	SWI       r3, r5, 4
	LWI       r3, r6, 52
	rtsd      r15, 8
	SWI       r3, r5, 0
	.end	_ZNK10syTriangle6NormalEv
$tmp107:
	.size	_ZNK10syTriangle6NormalEv, ($tmp107)-_ZNK10syTriangle6NormalEv

	.globl	_ZNK10syTriangle6ID_MATEv
	.align	2
	.type	_ZNK10syTriangle6ID_MATEv,@function
	.ent	_ZNK10syTriangle6ID_MATEv # @_ZNK10syTriangle6ID_MATEv
_ZNK10syTriangle6ID_MATEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	lbui      r3, r5, 40
	beqid     r3, ($BB108_2)
	NOP    
# BB#1:                                 # %entry.return_crit_edge
	rtsd      r15, 8
	LWI       r3, r5, 44
$BB108_2:                               # %if.end
	LWI       r3, r5, 36
	ADDI      r3, r3, 10
	LOAD      r3, r3, 0
	SWI       r3, r5, 44
	ADDI      r4, r0, 1
	rtsd      r15, 8
	sbi       r4, r5, 40
	.end	_ZNK10syTriangle6ID_MATEv
$tmp108:
	.size	_ZNK10syTriangle6ID_MATEv, ($tmp108)-_ZNK10syTriangle6ID_MATEv

	.globl	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
	.align	2
	.type	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord,@function
	.ent	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord # @_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord:
	.frame	r1,48,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -48
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r7, r1, 60
	LWI        r23, r5, 8
	LWI        r3, r5, 32
	FPRSUB     r12, r23, r3
	LWI        r24, r5, 4
	LWI        r3, r5, 28
	FPRSUB     r21, r24, r3
	LWI        r4, r6, 20
	FPMUL      r3, r4, r21
	LWI        r8, r6, 16
	FPMUL      r7, r8, r12
	FPRSUB     r7, r3, r7
	LWI        r25, r5, 0
	LWI        r3, r5, 24
	FPRSUB     r22, r25, r3
	LWI        r9, r6, 12
	FPMUL      r3, r9, r12
	FPMUL      r10, r4, r22
	FPRSUB     r11, r3, r10
	LWI        r3, r5, 12
	FPRSUB     r27, r25, r3
	LWI        r3, r5, 16
	FPRSUB     r28, r24, r3
	FPMUL      r3, r11, r28
	FPMUL      r10, r7, r27
	FPADD      r3, r10, r3
	FPMUL      r10, r8, r22
	FPMUL      r20, r9, r21
	FPRSUB     r20, r10, r20
	LWI        r10, r5, 20
	FPRSUB     r29, r23, r10
	FPMUL      r10, r20, r29
	FPADD      r26, r3, r10
	ORI       r3, r0, 0
	FPGE   r10, r26, r3
	FPUN   r3, r26, r3
	BITOR        r10, r3, r10
	bneid     r10, ($BB109_2)
	ADDI      r3, r0, 1
# BB#1:                                 # %entry
	ADDI      r3, r0, 0
$BB109_2:                               # %entry
	LWI        r10, r6, 8
	LWI        r31, r6, 0
	LWI        r6, r6, 4
	bneid     r3, ($BB109_4)
	ADD      r30, r26, r0
# BB#3:                                 # %if.then.i
	FPNEG      r30, r26
$BB109_4:                               # %_ZN4util4fabsERKf.exit
	ORI       r3, r0, 953267991
	FPLT   r3, r30, r3
	bneid     r3, ($BB109_6)
	ADDI      r30, r0, 1
# BB#5:                                 # %_ZN4util4fabsERKf.exit
	ADDI      r30, r0, 0
$BB109_6:                               # %_ZN4util4fabsERKf.exit
	bneid     r30, ($BB109_26)
	ADD      r3, r0, r0
# BB#7:                                 # %if.end
	FPRSUB     r30, r23, r10
	FPRSUB     r31, r25, r31
	FPRSUB     r10, r24, r6
	FPMUL      r3, r10, r27
	FPMUL      r6, r31, r28
	FPMUL      r24, r30, r27
	FPMUL      r25, r31, r29
	FPMUL      r27, r30, r28
	FPMUL      r28, r10, r29
	FPRSUB     r23, r3, r6
	FPRSUB     r25, r25, r24
	FPRSUB     r24, r27, r28
	FPMUL      r3, r25, r21
	FPMUL      r6, r24, r22
	FPADD      r3, r6, r3
	FPMUL      r6, r23, r12
	FPADD      r3, r3, r6
	ORI       r6, r0, 1065353216
	FPDIV      r12, r6, r26
	FPMUL      r6, r3, r12
	ORI       r3, r0, 0
	FPLT   r3, r6, r3
	bneid     r3, ($BB109_9)
	ADDI      r21, r0, 1
# BB#8:                                 # %if.end
	ADDI      r21, r0, 0
$BB109_9:                               # %if.end
	bneid     r21, ($BB109_26)
	ADD      r3, r0, r0
# BB#10:                                # %if.end9
	FPMUL      r3, r11, r10
	FPMUL      r7, r7, r31
	FPADD      r3, r7, r3
	FPMUL      r7, r20, r30
	FPADD      r3, r3, r7
	FPMUL      r10, r3, r12
	ORI       r3, r0, 0
	FPLT   r7, r10, r3
	FPUN   r3, r10, r3
	BITOR        r3, r3, r7
	bneid     r3, ($BB109_12)
	ADDI      r7, r0, 1
# BB#11:                                # %if.end9
	ADDI      r7, r0, 0
$BB109_12:                              # %if.end9
	bneid     r7, ($BB109_26)
	ADD      r3, r0, r0
# BB#13:                                # %if.end9
	FPMUL      r7, r25, r8
	FPMUL      r8, r24, r9
	FPADD      r7, r8, r7
	FPMUL      r4, r23, r4
	FPADD      r4, r7, r4
	FPMUL      r4, r4, r12
	ORI       r7, r0, 0
	FPLT   r8, r4, r7
	FPUN   r7, r4, r7
	BITOR        r8, r7, r8
	bneid     r8, ($BB109_15)
	ADDI      r7, r0, 1
# BB#14:                                # %if.end9
	ADDI      r7, r0, 0
$BB109_15:                              # %if.end9
	bneid     r7, ($BB109_26)
	NOP    
# BB#16:                                # %if.end9
	FPADD      r4, r10, r4
	ORI       r7, r0, 1065353216
	FPGT   r8, r4, r7
	FPUN   r4, r4, r7
	BITOR        r7, r4, r8
	bneid     r7, ($BB109_18)
	ADDI      r4, r0, 1
# BB#17:                                # %if.end9
	ADDI      r4, r0, 0
$BB109_18:                              # %if.end9
	bneid     r4, ($BB109_26)
	NOP    
# BB#19:                                # %if.then19
	LWI       r3, r1, 60
	LWI        r3, r3, 0
	FPGE   r4, r6, r3
	FPUN   r3, r6, r3
	BITOR        r3, r3, r4
	bneid     r3, ($BB109_21)
	ADDI      r4, r0, 1
# BB#20:                                # %if.then19
	ADDI      r4, r0, 0
$BB109_21:                              # %if.then19
	bneid     r4, ($BB109_26)
	ADD      r3, r0, r0
# BB#22:                                # %if.then21
	LWI       r3, r1, 60
	SWI        r6, r3, 0
	lbui      r3, r5, 40
	beqid     r3, ($BB109_24)
	NOP    
# BB#23:                                # %entry.return_crit_edge.i
	ADDI      r4, r5, 36
	brid      ($BB109_25)
	LWI       r3, r5, 44
$BB109_24:                              # %if.end.i
	LWI       r3, r5, 36
	ADDI      r3, r3, 10
	LOAD      r3, r3, 0
	SWI       r3, r5, 44
	ADDI      r4, r0, 1
	sbi       r4, r5, 40
	ADDI      r4, r5, 36
$BB109_25:                              # %_ZNK10syTriangle6ID_MATEv.exit
	LWI       r5, r1, 60
	SWI       r3, r5, 4
	LWI       r3, r4, 0
	SWI       r3, r5, 8
	ADDI      r3, r0, 1
$BB109_26:                              # %return
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 48
	.end	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord
$tmp109:
	.size	_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord, ($tmp109)-_ZNK10syTriangle11IntersectsWERK3RayR9HitRecord

	.globl	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
	.align	2
	.type	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist,@function
	.ent	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist # @_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist:
	.frame	r1,48,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -48
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	LWI        r23, r5, 8
	LWI        r3, r5, 32
	FPRSUB     r12, r23, r3
	LWI        r24, r5, 4
	LWI        r3, r5, 28
	FPRSUB     r21, r24, r3
	LWI        r4, r6, 20
	FPMUL      r3, r4, r21
	LWI        r8, r6, 16
	FPMUL      r9, r8, r12
	FPRSUB     r10, r3, r9
	LWI        r25, r5, 0
	LWI        r3, r5, 24
	FPRSUB     r22, r25, r3
	LWI        r9, r6, 12
	FPMUL      r3, r9, r12
	FPMUL      r11, r4, r22
	FPRSUB     r11, r3, r11
	LWI        r3, r5, 12
	FPRSUB     r26, r25, r3
	LWI        r3, r5, 16
	FPRSUB     r27, r24, r3
	FPMUL      r3, r11, r27
	FPMUL      r20, r10, r26
	FPADD      r3, r20, r3
	FPMUL      r20, r8, r22
	FPMUL      r28, r9, r21
	FPRSUB     r20, r20, r28
	LWI        r5, r5, 20
	FPRSUB     r28, r23, r5
	FPMUL      r5, r20, r28
	FPADD      r5, r3, r5
	ORI       r3, r0, 0
	FPGE   r29, r5, r3
	FPUN   r3, r5, r3
	BITOR        r29, r3, r29
	bneid     r29, ($BB110_2)
	ADDI      r3, r0, 1
# BB#1:                                 # %entry
	ADDI      r3, r0, 0
$BB110_2:                               # %entry
	LWI        r29, r6, 8
	LWI        r30, r6, 0
	LWI        r6, r6, 4
	bneid     r3, ($BB110_4)
	ADD      r31, r5, r0
# BB#3:                                 # %if.then.i
	FPNEG      r31, r5
$BB110_4:                               # %_ZN4util4fabsERKf.exit
	ORI       r3, r0, 953267991
	FPLT   r3, r31, r3
	bneid     r3, ($BB110_6)
	ADDI      r31, r0, 1
# BB#5:                                 # %_ZN4util4fabsERKf.exit
	ADDI      r31, r0, 0
$BB110_6:                               # %_ZN4util4fabsERKf.exit
	bneid     r31, ($BB110_23)
	ADD      r3, r0, r0
# BB#7:                                 # %if.end
	FPRSUB     r29, r23, r29
	FPRSUB     r25, r25, r30
	FPRSUB     r30, r24, r6
	FPMUL      r3, r30, r26
	FPMUL      r6, r25, r27
	FPMUL      r23, r29, r26
	FPMUL      r24, r25, r28
	FPMUL      r26, r29, r27
	FPMUL      r27, r30, r28
	FPRSUB     r6, r3, r6
	FPRSUB     r24, r24, r23
	FPRSUB     r23, r26, r27
	FPMUL      r3, r24, r21
	FPMUL      r21, r23, r22
	FPADD      r3, r21, r3
	FPMUL      r12, r6, r12
	FPADD      r3, r3, r12
	ORI       r12, r0, 1065353216
	FPDIV      r12, r12, r5
	FPMUL      r5, r3, r12
	ORI       r3, r0, 0
	FPLT   r3, r5, r3
	bneid     r3, ($BB110_9)
	ADDI      r21, r0, 1
# BB#8:                                 # %if.end
	ADDI      r21, r0, 0
$BB110_9:                               # %if.end
	bneid     r21, ($BB110_23)
	ADD      r3, r0, r0
# BB#10:                                # %if.end9
	FPMUL      r3, r11, r30
	FPMUL      r10, r10, r25
	FPADD      r3, r10, r3
	FPMUL      r10, r20, r29
	FPADD      r3, r3, r10
	FPMUL      r10, r3, r12
	ORI       r3, r0, 0
	FPLT   r11, r10, r3
	FPUN   r3, r10, r3
	BITOR        r3, r3, r11
	bneid     r3, ($BB110_12)
	ADDI      r11, r0, 1
# BB#11:                                # %if.end9
	ADDI      r11, r0, 0
$BB110_12:                              # %if.end9
	bneid     r11, ($BB110_23)
	ADD      r3, r0, r0
# BB#13:                                # %if.end9
	FPMUL      r8, r24, r8
	FPMUL      r9, r23, r9
	FPADD      r8, r9, r8
	FPMUL      r4, r6, r4
	FPADD      r4, r8, r4
	FPMUL      r4, r4, r12
	ORI       r6, r0, 0
	FPLT   r8, r4, r6
	FPUN   r6, r4, r6
	BITOR        r8, r6, r8
	bneid     r8, ($BB110_15)
	ADDI      r6, r0, 1
# BB#14:                                # %if.end9
	ADDI      r6, r0, 0
$BB110_15:                              # %if.end9
	bneid     r6, ($BB110_23)
	NOP    
# BB#16:                                # %if.end9
	FPADD      r4, r10, r4
	ORI       r6, r0, 1065353216
	FPGT   r8, r4, r6
	FPUN   r4, r4, r6
	BITOR        r6, r4, r8
	bneid     r6, ($BB110_18)
	ADDI      r4, r0, 1
# BB#17:                                # %if.end9
	ADDI      r4, r0, 0
$BB110_18:                              # %if.end9
	bneid     r4, ($BB110_23)
	NOP    
# BB#19:                                # %if.then19
	LWI        r3, r7, 0
	FPGE   r4, r5, r3
	FPUN   r3, r5, r3
	BITOR        r3, r3, r4
	bneid     r3, ($BB110_21)
	ADDI      r4, r0, 1
# BB#20:                                # %if.then19
	ADDI      r4, r0, 0
$BB110_21:                              # %if.then19
	bneid     r4, ($BB110_23)
	ADD      r3, r0, r0
# BB#22:                                # %if.then21
	SWI        r5, r7, 0
	ADDI      r3, r0, 1
$BB110_23:                              # %return
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 48
	.end	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist
$tmp110:
	.size	_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist, ($tmp110)-_ZNK10syTriangle20IntersectsWShadowRayERK3RayR7HitDist

	.globl	_ZN8syVectorC2Ei
	.align	2
	.type	_ZN8syVectorC2Ei,@function
	.ent	_ZN8syVectorC2Ei        # @_ZN8syVectorC2Ei
_ZN8syVectorC2Ei:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LOAD      r3, r6, 0
	SWI        r3, r5, 0
	LOAD      r3, r6, 1
	SWI        r3, r5, 4
	LOAD      r3, r6, 2
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVectorC2Ei
$tmp111:
	.size	_ZN8syVectorC2Ei, ($tmp111)-_ZN8syVectorC2Ei

	.globl	_ZN8syVector14LoadFromMemoryERKi
	.align	2
	.type	_ZN8syVector14LoadFromMemoryERKi,@function
	.ent	_ZN8syVector14LoadFromMemoryERKi # @_ZN8syVector14LoadFromMemoryERKi
_ZN8syVector14LoadFromMemoryERKi:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI       r3, r6, 0
	LOAD      r3, r3, 0
	SWI        r3, r5, 0
	LWI       r3, r6, 0
	LOAD      r3, r3, 1
	SWI        r3, r5, 4
	LWI       r3, r6, 0
	LOAD      r3, r3, 2
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVector14LoadFromMemoryERKi
$tmp112:
	.size	_ZN8syVector14LoadFromMemoryERKi, ($tmp112)-_ZN8syVector14LoadFromMemoryERKi

	.globl	_ZN8syVectorC2Ev
	.align	2
	.type	_ZN8syVectorC2Ev,@function
	.ent	_ZN8syVectorC2Ev        # @_ZN8syVectorC2Ev
_ZN8syVectorC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN8syVectorC2Ev
$tmp113:
	.size	_ZN8syVectorC2Ev, ($tmp113)-_ZN8syVectorC2Ev

	.globl	_ZN8syVectorC2ERKfS1_S1_
	.align	2
	.type	_ZN8syVectorC2ERKfS1_S1_,@function
	.ent	_ZN8syVectorC2ERKfS1_S1_ # @_ZN8syVectorC2ERKfS1_S1_
_ZN8syVectorC2ERKfS1_S1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r7, 0
	SWI        r3, r5, 4
	LWI        r3, r8, 0
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVectorC2ERKfS1_S1_
$tmp114:
	.size	_ZN8syVectorC2ERKfS1_S1_, ($tmp114)-_ZN8syVectorC2ERKfS1_S1_

	.globl	_ZN8syVector3SetERKfS1_S1_
	.align	2
	.type	_ZN8syVector3SetERKfS1_S1_,@function
	.ent	_ZN8syVector3SetERKfS1_S1_ # @_ZN8syVector3SetERKfS1_S1_
_ZN8syVector3SetERKfS1_S1_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r7, 0
	SWI        r3, r5, 4
	LWI        r3, r8, 0
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVector3SetERKfS1_S1_
$tmp115:
	.size	_ZN8syVector3SetERKfS1_S1_, ($tmp115)-_ZN8syVector3SetERKfS1_S1_

	.globl	_ZN8syVectorC2EPKf
	.align	2
	.type	_ZN8syVectorC2EPKf,@function
	.ent	_ZN8syVectorC2EPKf      # @_ZN8syVectorC2EPKf
_ZN8syVectorC2EPKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVectorC2EPKf
$tmp116:
	.size	_ZN8syVectorC2EPKf, ($tmp116)-_ZN8syVectorC2EPKf

	.globl	_ZN8syVector4ZeroEv
	.align	2
	.type	_ZN8syVector4ZeroEv,@function
	.ent	_ZN8syVector4ZeroEv     # @_ZN8syVector4ZeroEv
_ZN8syVector4ZeroEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r0, r5, 0
	SWI       r0, r5, 4
	rtsd      r15, 8
	SWI       r0, r5, 8
	.end	_ZN8syVector4ZeroEv
$tmp117:
	.size	_ZN8syVector4ZeroEv, ($tmp117)-_ZN8syVector4ZeroEv

	.globl	_ZNK8syVector13LengthSquaredEv
	.align	2
	.type	_ZNK8syVector13LengthSquaredEv,@function
	.ent	_ZNK8syVector13LengthSquaredEv # @_ZNK8syVector13LengthSquaredEv
_ZNK8syVector13LengthSquaredEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 4
	FPMUL      r3, r3, r3
	LWI        r4, r5, 0
	FPMUL      r4, r4, r4
	FPADD      r3, r4, r3
	LWI        r4, r5, 8
	FPMUL      r4, r4, r4
	rtsd      r15, 8
	FPADD      r3, r3, r4
	.end	_ZNK8syVector13LengthSquaredEv
$tmp118:
	.size	_ZNK8syVector13LengthSquaredEv, ($tmp118)-_ZNK8syVector13LengthSquaredEv

	.globl	_ZNK8syVector6LengthEv
	.align	2
	.type	_ZNK8syVector6LengthEv,@function
	.ent	_ZNK8syVector6LengthEv  # @_ZNK8syVector6LengthEv
_ZNK8syVector6LengthEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 4
	FPMUL      r3, r3, r3
	LWI        r4, r5, 0
	FPMUL      r4, r4, r4
	FPADD      r3, r4, r3
	LWI        r4, r5, 8
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	rtsd      r15, 8
	FPDIV      r3, r4, r3
	.end	_ZNK8syVector6LengthEv
$tmp119:
	.size	_ZNK8syVector6LengthEv, ($tmp119)-_ZNK8syVector6LengthEv

	.globl	_ZN8syVector9NormalizeEv
	.align	2
	.type	_ZN8syVector9NormalizeEv,@function
	.ent	_ZN8syVector9NormalizeEv # @_ZN8syVector9NormalizeEv
_ZN8syVector9NormalizeEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 4
	FPMUL      r3, r3, r3
	LWI        r4, r5, 0
	FPMUL      r4, r4, r4
	FPADD      r3, r4, r3
	LWI        r4, r5, 8
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	LWI        r4, r5, 0
	FPDIV      r4, r4, r3
	SWI        r4, r5, 0
	LWI        r4, r5, 4
	FPDIV      r4, r4, r3
	SWI        r4, r5, 4
	LWI        r4, r5, 8
	FPDIV      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN8syVector9NormalizeEv
$tmp120:
	.size	_ZN8syVector9NormalizeEv, ($tmp120)-_ZN8syVector9NormalizeEv

	.globl	_ZNK8syVector13GetNormalizedEv
	.align	2
	.type	_ZNK8syVector13GetNormalizedEv,@function
	.ent	_ZNK8syVector13GetNormalizedEv # @_ZNK8syVector13GetNormalizedEv
_ZNK8syVector13GetNormalizedEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 4
	FPMUL      r3, r3, r3
	LWI        r4, r6, 0
	FPMUL      r4, r4, r4
	FPADD      r3, r4, r3
	LWI        r4, r6, 8
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	LWI        r4, r6, 0
	FPDIV      r7, r4, r3
	LWI        r4, r6, 8
	LWI        r6, r6, 4
	SWI        r7, r5, 0
	FPDIV      r6, r6, r3
	SWI        r6, r5, 4
	FPDIV      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVector13GetNormalizedEv
$tmp121:
	.size	_ZNK8syVector13GetNormalizedEv, ($tmp121)-_ZNK8syVector13GetNormalizedEv

	.globl	_ZNK8syVectorngEv
	.align	2
	.type	_ZNK8syVectorngEv,@function
	.ent	_ZNK8syVectorngEv       # @_ZNK8syVectorngEv
_ZNK8syVectorngEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 8
	LWI        r4, r6, 4
	LWI        r6, r6, 0
	FPNEG      r6, r6
	SWI        r6, r5, 0
	FPNEG      r4, r4
	SWI        r4, r5, 4
	FPNEG      r3, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectorngEv
$tmp122:
	.size	_ZNK8syVectorngEv, ($tmp122)-_ZNK8syVectorngEv

	.globl	_ZNK8syVector8GetOrthoEv
	.align	2
	.type	_ZNK8syVector8GetOrthoEv,@function
	.ent	_ZNK8syVector8GetOrthoEv # @_ZNK8syVector8GetOrthoEv
_ZNK8syVector8GetOrthoEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	ORI       r4, r0, 0
	FPGE   r7, r3, r4
	FPUN   r4, r3, r4
	BITOR        r7, r4, r7
	bneid     r7, ($BB123_2)
	ADDI      r4, r0, 1
# BB#1:                                 # %entry
	ADDI      r4, r0, 0
$BB123_2:                               # %entry
	bneid     r4, ($BB123_4)
	ADD      r7, r3, r0
# BB#3:                                 # %if.then.i59
	FPNEG      r7, r3
$BB123_4:                               # %_ZN4util4fabsERKf.exit61
	LWI        r4, r6, 4
	ORI       r8, r0, 0
	FPGE   r9, r4, r8
	FPUN   r8, r4, r8
	BITOR        r8, r8, r9
	bneid     r8, ($BB123_6)
	ADDI      r9, r0, 1
# BB#5:                                 # %_ZN4util4fabsERKf.exit61
	ADDI      r9, r0, 0
$BB123_6:                               # %_ZN4util4fabsERKf.exit61
	bneid     r9, ($BB123_8)
	ADD      r8, r4, r0
# BB#7:                                 # %if.then.i54
	FPNEG      r8, r4
$BB123_8:                               # %_ZN4util4fabsERKf.exit56
	LWI        r6, r6, 8
	ORI       r9, r0, 0
	FPGE   r10, r6, r9
	FPUN   r9, r6, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB123_10)
	ADDI      r10, r0, 1
# BB#9:                                 # %_ZN4util4fabsERKf.exit56
	ADDI      r10, r0, 0
$BB123_10:                              # %_ZN4util4fabsERKf.exit56
	bneid     r10, ($BB123_12)
	ADD      r9, r6, r0
# BB#11:                                # %if.then.i
	FPNEG      r9, r6
$BB123_12:                              # %_ZN4util4fabsERKf.exit
	FPGE   r10, r7, r8
	FPUN   r11, r7, r8
	BITOR        r10, r11, r10
	bneid     r10, ($BB123_14)
	ADDI      r12, r0, 1
# BB#13:                                # %_ZN4util4fabsERKf.exit
	ADDI      r12, r0, 0
$BB123_14:                              # %_ZN4util4fabsERKf.exit
	ORI       r11, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r12, ($BB123_18)
	NOP    
# BB#15:                                # %_ZN4util4fabsERKf.exit
	FPLT   r7, r7, r9
	bneid     r7, ($BB123_17)
	ADDI      r12, r0, 1
# BB#16:                                # %_ZN4util4fabsERKf.exit
	ADDI      r12, r0, 0
$BB123_17:                              # %_ZN4util4fabsERKf.exit
	bneid     r12, ($BB123_22)
	ADD      r7, r10, r0
$BB123_18:                              # %if.else
	FPLT   r7, r8, r9
	bneid     r7, ($BB123_20)
	ADDI      r8, r0, 1
# BB#19:                                # %if.else
	ADDI      r8, r0, 0
$BB123_20:                              # %if.else
	ORI       r7, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r8, ($BB123_22)
	ADD      r11, r10, r0
# BB#21:                                # %if.else19
	ORI       r7, r0, 0
	ORI       r10, r0, 1065353216
	ADD      r11, r7, r0
$BB123_22:                              # %if.end25
	FPMUL      r8, r6, r7
	FPMUL      r9, r4, r10
	FPRSUB     r8, r8, r9
	SWI        r8, r5, 0
	FPMUL      r8, r3, r10
	FPMUL      r6, r6, r11
	FPRSUB     r6, r8, r6
	SWI        r6, r5, 4
	FPMUL      r4, r4, r11
	FPMUL      r3, r3, r7
	FPRSUB     r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVector8GetOrthoEv
$tmp123:
	.size	_ZNK8syVector8GetOrthoEv, ($tmp123)-_ZNK8syVector8GetOrthoEv

	.globl	_ZN8syVectoraSERKS_
	.align	2
	.type	_ZN8syVectoraSERKS_,@function
	.ent	_ZN8syVectoraSERKS_     # @_ZN8syVectoraSERKS_
_ZN8syVectoraSERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectoraSERKS_
$tmp124:
	.size	_ZN8syVectoraSERKS_, ($tmp124)-_ZN8syVectoraSERKS_

	.globl	_ZNK8syVector5CrossERKS_
	.align	2
	.type	_ZNK8syVector5CrossERKS_,@function
	.ent	_ZNK8syVector5CrossERKS_ # @_ZNK8syVector5CrossERKS_
_ZNK8syVector5CrossERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 4
	LWI        r4, r6, 8
	FPMUL      r10, r4, r3
	LWI        r9, r7, 8
	LWI        r8, r6, 4
	FPMUL      r11, r8, r9
	FPRSUB     r10, r10, r11
	LWI        r7, r7, 0
	LWI        r6, r6, 0
	SWI        r10, r5, 0
	FPMUL      r9, r6, r9
	FPMUL      r4, r4, r7
	FPRSUB     r4, r9, r4
	SWI        r4, r5, 4
	FPMUL      r4, r8, r7
	FPMUL      r3, r6, r3
	FPRSUB     r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVector5CrossERKS_
$tmp125:
	.size	_ZNK8syVector5CrossERKS_, ($tmp125)-_ZNK8syVector5CrossERKS_

	.globl	_ZNK8syVector3DotERKS_
	.align	2
	.type	_ZNK8syVector3DotERKS_,@function
	.ent	_ZNK8syVector3DotERKS_  # @_ZNK8syVector3DotERKS_
_ZNK8syVector3DotERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPMUL      r3, r4, r3
	LWI        r4, r6, 0
	LWI        r7, r5, 0
	FPMUL      r4, r7, r4
	FPADD      r3, r4, r3
	LWI        r4, r6, 8
	LWI        r5, r5, 8
	FPMUL      r4, r5, r4
	rtsd      r15, 8
	FPADD      r3, r3, r4
	.end	_ZNK8syVector3DotERKS_
$tmp126:
	.size	_ZNK8syVector3DotERKS_, ($tmp126)-_ZNK8syVector3DotERKS_

	.globl	_ZNK8syVectorrmERKS_
	.align	2
	.type	_ZNK8syVectorrmERKS_,@function
	.ent	_ZNK8syVectorrmERKS_    # @_ZNK8syVectorrmERKS_
_ZNK8syVectorrmERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPMUL      r3, r4, r3
	LWI        r4, r6, 0
	LWI        r7, r5, 0
	FPMUL      r4, r7, r4
	FPADD      r3, r4, r3
	LWI        r4, r6, 8
	LWI        r5, r5, 8
	FPMUL      r4, r5, r4
	rtsd      r15, 8
	FPADD      r3, r3, r4
	.end	_ZNK8syVectorrmERKS_
$tmp127:
	.size	_ZNK8syVectorrmERKS_, ($tmp127)-_ZNK8syVectorrmERKS_

	.globl	_ZNK8syVectoreoERKS_
	.align	2
	.type	_ZNK8syVectoreoERKS_,@function
	.ent	_ZNK8syVectoreoERKS_    # @_ZNK8syVectoreoERKS_
_ZNK8syVectoreoERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 4
	LWI        r4, r6, 8
	FPMUL      r10, r4, r3
	LWI        r9, r7, 8
	LWI        r8, r6, 4
	FPMUL      r11, r8, r9
	FPRSUB     r10, r10, r11
	LWI        r7, r7, 0
	LWI        r6, r6, 0
	SWI        r10, r5, 0
	FPMUL      r9, r6, r9
	FPMUL      r4, r4, r7
	FPRSUB     r4, r9, r4
	SWI        r4, r5, 4
	FPMUL      r4, r8, r7
	FPMUL      r3, r6, r3
	FPRSUB     r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectoreoERKS_
$tmp128:
	.size	_ZNK8syVectoreoERKS_, ($tmp128)-_ZNK8syVectoreoERKS_

	.globl	_ZNK8syVectorplERKS_
	.align	2
	.type	_ZNK8syVectorplERKS_,@function
	.ent	_ZNK8syVectorplERKS_    # @_ZNK8syVectorplERKS_
_ZNK8syVectorplERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPADD      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPADD      r4, r6, r8
	SWI        r4, r5, 4
	FPADD      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectorplERKS_
$tmp129:
	.size	_ZNK8syVectorplERKS_, ($tmp129)-_ZNK8syVectorplERKS_

	.globl	_ZNK8syVectormiERKS_
	.align	2
	.type	_ZNK8syVectormiERKS_,@function
	.ent	_ZNK8syVectormiERKS_    # @_ZNK8syVectormiERKS_
_ZNK8syVectormiERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPRSUB     r4, r3, r4
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPRSUB     r4, r8, r6
	SWI        r4, r5, 4
	FPRSUB     r3, r3, r7
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectormiERKS_
$tmp130:
	.size	_ZNK8syVectormiERKS_, ($tmp130)-_ZNK8syVectormiERKS_

	.globl	_ZNK8syVectormlERKS_
	.align	2
	.type	_ZNK8syVectormlERKS_,@function
	.ent	_ZNK8syVectormlERKS_    # @_ZNK8syVectormlERKS_
_ZNK8syVectormlERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPMUL      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPMUL      r4, r6, r8
	SWI        r4, r5, 4
	FPMUL      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectormlERKS_
$tmp131:
	.size	_ZNK8syVectormlERKS_, ($tmp131)-_ZNK8syVectormlERKS_

	.globl	_ZNK8syVectordvERKS_
	.align	2
	.type	_ZNK8syVectordvERKS_,@function
	.ent	_ZNK8syVectordvERKS_    # @_ZNK8syVectordvERKS_
_ZNK8syVectordvERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPDIV      r4, r4, r3
	LWI        r3, r7, 8
	LWI        r8, r7, 4
	LWI        r7, r6, 8
	LWI        r6, r6, 4
	SWI        r4, r5, 0
	FPDIV      r4, r6, r8
	SWI        r4, r5, 4
	FPDIV      r3, r7, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectordvERKS_
$tmp132:
	.size	_ZNK8syVectordvERKS_, ($tmp132)-_ZNK8syVectordvERKS_

	.globl	_ZNK8syVectorplERKf
	.align	2
	.type	_ZNK8syVectorplERKf,@function
	.ent	_ZNK8syVectorplERKf     # @_ZNK8syVectorplERKf
_ZNK8syVectorplERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPADD      r7, r4, r3
	LWI        r4, r6, 8
	LWI        r6, r6, 4
	SWI        r7, r5, 0
	FPADD      r6, r6, r3
	SWI        r6, r5, 4
	FPADD      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectorplERKf
$tmp133:
	.size	_ZNK8syVectorplERKf, ($tmp133)-_ZNK8syVectorplERKf

	.globl	_ZNK8syVectormiERKf
	.align	2
	.type	_ZNK8syVectormiERKf,@function
	.ent	_ZNK8syVectormiERKf     # @_ZNK8syVectormiERKf
_ZNK8syVectormiERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPRSUB     r7, r3, r4
	LWI        r4, r6, 8
	LWI        r6, r6, 4
	SWI        r7, r5, 0
	FPRSUB     r6, r3, r6
	SWI        r6, r5, 4
	FPRSUB     r3, r3, r4
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectormiERKf
$tmp134:
	.size	_ZNK8syVectormiERKf, ($tmp134)-_ZNK8syVectormiERKf

	.globl	_ZNK8syVectormlERKf
	.align	2
	.type	_ZNK8syVectormlERKf,@function
	.ent	_ZNK8syVectormlERKf     # @_ZNK8syVectormlERKf
_ZNK8syVectormlERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPMUL      r7, r4, r3
	LWI        r4, r6, 8
	LWI        r6, r6, 4
	SWI        r7, r5, 0
	FPMUL      r6, r6, r3
	SWI        r6, r5, 4
	FPMUL      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectormlERKf
$tmp135:
	.size	_ZNK8syVectormlERKf, ($tmp135)-_ZNK8syVectormlERKf

	.globl	_ZNK8syVectordvERKf
	.align	2
	.type	_ZNK8syVectordvERKf,@function
	.ent	_ZNK8syVectordvERKf     # @_ZNK8syVectordvERKf
_ZNK8syVectordvERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r7, 0
	LWI        r4, r6, 0
	FPDIV      r7, r4, r3
	LWI        r4, r6, 8
	LWI        r6, r6, 4
	SWI        r7, r5, 0
	FPDIV      r6, r6, r3
	SWI        r6, r5, 4
	FPDIV      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZNK8syVectordvERKf
$tmp136:
	.size	_ZNK8syVectordvERKf, ($tmp136)-_ZNK8syVectordvERKf

	.globl	_ZN8syVectorpLERKS_
	.align	2
	.type	_ZN8syVectorpLERKS_,@function
	.ent	_ZN8syVectorpLERKS_     # @_ZN8syVectorpLERKS_
_ZN8syVectorpLERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPADD      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPADD      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPADD      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectorpLERKS_
$tmp137:
	.size	_ZN8syVectorpLERKS_, ($tmp137)-_ZN8syVectorpLERKS_

	.globl	_ZN8syVectormIERKS_
	.align	2
	.type	_ZN8syVectormIERKS_,@function
	.ent	_ZN8syVectormIERKS_     # @_ZN8syVectormIERKS_
_ZN8syVectormIERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectormIERKS_
$tmp138:
	.size	_ZN8syVectormIERKS_, ($tmp138)-_ZN8syVectormIERKS_

	.globl	_ZN8syVectormLERKS_
	.align	2
	.type	_ZN8syVectormLERKS_,@function
	.ent	_ZN8syVectormLERKS_     # @_ZN8syVectormLERKS_
_ZN8syVectormLERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPMUL      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPMUL      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPMUL      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectormLERKS_
$tmp139:
	.size	_ZN8syVectormLERKS_, ($tmp139)-_ZN8syVectormLERKS_

	.globl	_ZN8syVectordVERKS_
	.align	2
	.type	_ZN8syVectordVERKS_,@function
	.ent	_ZN8syVectordVERKS_     # @_ZN8syVectordVERKS_
_ZN8syVectordVERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPDIV      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	LWI        r4, r5, 4
	FPDIV      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	LWI        r4, r5, 8
	FPDIV      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectordVERKS_
$tmp140:
	.size	_ZN8syVectordVERKS_, ($tmp140)-_ZN8syVectordVERKS_

	.globl	_ZN8syVectorpLERKf
	.align	2
	.type	_ZN8syVectorpLERKf,@function
	.ent	_ZN8syVectorpLERKf      # @_ZN8syVectorpLERKf
_ZN8syVectorpLERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPADD      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 0
	LWI        r4, r5, 4
	FPADD      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 0
	LWI        r4, r5, 8
	FPADD      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectorpLERKf
$tmp141:
	.size	_ZN8syVectorpLERKf, ($tmp141)-_ZN8syVectorpLERKf

	.globl	_ZN8syVectormIERKf
	.align	2
	.type	_ZN8syVectormIERKf,@function
	.ent	_ZN8syVectormIERKf      # @_ZN8syVectormIERKf
_ZN8syVectormIERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 0
	LWI        r3, r6, 0
	LWI        r4, r5, 4
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 4
	LWI        r3, r6, 0
	LWI        r4, r5, 8
	FPRSUB     r3, r3, r4
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectormIERKf
$tmp142:
	.size	_ZN8syVectormIERKf, ($tmp142)-_ZN8syVectormIERKf

	.globl	_ZN8syVectormLERKf
	.align	2
	.type	_ZN8syVectormLERKf,@function
	.ent	_ZN8syVectormLERKf      # @_ZN8syVectormLERKf
_ZN8syVectormLERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPMUL      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 0
	LWI        r4, r5, 4
	FPMUL      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 0
	LWI        r4, r5, 8
	FPMUL      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectormLERKf
$tmp143:
	.size	_ZN8syVectormLERKf, ($tmp143)-_ZN8syVectormLERKf

	.globl	_ZN8syVectordVERKf
	.align	2
	.type	_ZN8syVectordVERKf,@function
	.ent	_ZN8syVectordVERKf      # @_ZN8syVectordVERKf
_ZN8syVectordVERKf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	LWI        r4, r5, 0
	FPDIV      r3, r4, r3
	SWI        r3, r5, 0
	LWI        r3, r6, 0
	LWI        r4, r5, 4
	FPDIV      r3, r4, r3
	SWI        r3, r5, 4
	LWI        r3, r6, 0
	LWI        r4, r5, 8
	FPDIV      r3, r4, r3
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN8syVectordVERKf
$tmp144:
	.size	_ZN8syVectordVERKf, ($tmp144)-_ZN8syVectordVERKf

	.globl	_ZNK8syVectoreqERKS_
	.align	2
	.type	_ZNK8syVectoreqERKS_,@function
	.ent	_ZNK8syVectoreqERKS_    # @_ZNK8syVectoreqERKS_
_ZNK8syVectoreqERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	LWI        r4, r6, 0
	FPNE   r3, r4, r3
	bneid     r3, ($BB145_2)
	ADDI      r4, r0, 1
# BB#1:                                 # %entry
	ADDI      r4, r0, 0
$BB145_2:                               # %entry
	bneid     r4, ($BB145_8)
	ADD      r3, r0, r0
# BB#3:                                 # %land.lhs.true
	LWI        r3, r5, 4
	LWI        r4, r6, 4
	FPNE   r3, r4, r3
	bneid     r3, ($BB145_5)
	ADDI      r4, r0, 1
# BB#4:                                 # %land.lhs.true
	ADDI      r4, r0, 0
$BB145_5:                               # %land.lhs.true
	bneid     r4, ($BB145_8)
	ADD      r3, r0, r0
# BB#6:                                 # %land.rhs
	LWI        r3, r5, 8
	LWI        r4, r6, 8
	FPEQ   r4, r4, r3
	bneid     r4, ($BB145_8)
	ADDI      r3, r0, 1
# BB#7:                                 # %land.rhs
	ADDI      r3, r0, 0
$BB145_8:                               # %land.end
	rtsd      r15, 8
	NOP    
	.end	_ZNK8syVectoreqERKS_
$tmp145:
	.size	_ZNK8syVectoreqERKS_, ($tmp145)-_ZNK8syVectoreqERKS_

	.globl	_ZNK8syVectorneERKS_
	.align	2
	.type	_ZNK8syVectorneERKS_,@function
	.ent	_ZNK8syVectorneERKS_    # @_ZNK8syVectorneERKS_
_ZNK8syVectorneERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r5, 0
	LWI        r4, r6, 0
	FPNE   r7, r4, r3
	ADDI      r3, r0, 1
	bneid     r7, ($BB146_2)
	ADD      r4, r3, r0
# BB#1:                                 # %entry
	ADDI      r4, r0, 0
$BB146_2:                               # %entry
	bneid     r4, ($BB146_8)
	NOP    
# BB#3:                                 # %lor.lhs.false
	LWI        r3, r5, 4
	LWI        r4, r6, 4
	FPNE   r7, r4, r3
	ADDI      r3, r0, 1
	bneid     r7, ($BB146_5)
	ADD      r4, r3, r0
# BB#4:                                 # %lor.lhs.false
	ADDI      r4, r0, 0
$BB146_5:                               # %lor.lhs.false
	bneid     r4, ($BB146_8)
	NOP    
# BB#6:                                 # %lor.rhs
	LWI        r3, r5, 8
	LWI        r4, r6, 8
	FPNE   r4, r4, r3
	bneid     r4, ($BB146_8)
	ADDI      r3, r0, 1
# BB#7:                                 # %lor.rhs
	ADDI      r3, r0, 0
$BB146_8:                               # %lor.end
	rtsd      r15, 8
	NOP    
	.end	_ZNK8syVectorneERKS_
$tmp146:
	.size	_ZNK8syVectorneERKS_, ($tmp146)-_ZNK8syVectorneERKS_

	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
	.frame	r1,4,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r15, r1, 0
	brlid     r15, _Z9trax_mainv
	NOP    
	ADD      r3, r0, r0
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	main
$tmp147:
	.size	main, ($tmp147)-main

	.globl	_Z6printfPKcz
	.align	2
	.type	_Z6printfPKcz,@function
	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
	.frame	r1,4,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r10, r1, 28
	SWI       r9, r1, 24
	SWI       r8, r1, 20
	SWI       r7, r1, 16
	SWI       r6, r1, 12
	SWI       r5, r1, 8
	ADDI      r3, r1, 8
	PRINTF     r3
	ADD      r3, r0, r0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_Z6printfPKcz
$tmp148:
	.size	_Z6printfPKcz, ($tmp148)-_Z6printfPKcz


	.globl	_ZN5ColorC1Ei
_ZN5ColorC1Ei = _ZN5ColorC2Ei
	.globl	_ZN5ColorC1Ev
_ZN5ColorC1Ev = _ZN5ColorC2Ev
	.globl	_ZN5ColorC1ERKS_
_ZN5ColorC1ERKS_ = _ZN5ColorC2ERKS_
	.globl	_ZN5ColorC1ERKfS1_S1_
_ZN5ColorC1ERKfS1_S1_ = _ZN5ColorC2ERKfS1_S1_
	.globl	_ZN9HitRecordC1Ev
_ZN9HitRecordC1Ev = _ZN9HitRecordC2Ev
	.globl	_ZN7HitDistC1Ev
_ZN7HitDistC1Ev = _ZN7HitDistC2Ev
	.globl	_ZN5ImageC1ERKiS1_S1_
_ZN5ImageC1ERKiS1_S1_ = _ZN5ImageC2ERKiS1_S1_
	.globl	_ZN13PinholeCameraC1EiRKiS1_
_ZN13PinholeCameraC1EiRKiS1_ = _ZN13PinholeCameraC2EiRKiS1_
	.globl	_ZN13PinholeCameraC1Ev
_ZN13PinholeCameraC1Ev = _ZN13PinholeCameraC2Ev
	.globl	_ZN13PinholeCameraC1ERK8syVectorS2_S2_RKfRKiS6_
_ZN13PinholeCameraC1ERK8syVectorS2_S2_RKfRKiS6_ = _ZN13PinholeCameraC2ERK8syVectorS2_S2_RKfRKiS6_
	.globl	_ZN10PointLightC1EiRK5Color
_ZN10PointLightC1EiRK5Color = _ZN10PointLightC2EiRK5Color
	.globl	_ZN10PointLightC1Ev
_ZN10PointLightC1Ev = _ZN10PointLightC2Ev
	.globl	_ZN10PointLightC1ERK8syVectorRK5Color
_ZN10PointLightC1ERK8syVectorRK5Color = _ZN10PointLightC2ERK8syVectorRK5Color
	.globl	_ZN3RayC1Ev
_ZN3RayC1Ev = _ZN3RayC2Ev
	.globl	_ZN3RayC1E8syVectorS0_
_ZN3RayC1E8syVectorS0_ = _ZN3RayC2E8syVectorS0_
	.globl	_ZN3RayC1ERKS_
_ZN3RayC1ERKS_ = _ZN3RayC2ERKS_
	.globl	_ZN5syBoxC1Ev
_ZN5syBoxC1Ev = _ZN5syBoxC2Ev
	.globl	_ZN5syBoxC1ERK8syVectorS2_
_ZN5syBoxC1ERK8syVectorS2_ = _ZN5syBoxC2ERK8syVectorS2_
	.globl	_ZN9syBVHNodeC1Ei
_ZN9syBVHNodeC1Ei = _ZN9syBVHNodeC2Ei
	.globl	_ZN8syMatrixC1Ev
_ZN8syMatrixC1Ev = _ZN8syMatrixC2Ev
	.globl	_ZN8sySphereC1Ev
_ZN8sySphereC1Ev = _ZN8sySphereC2Ev
	.globl	_ZN8sySphereC1ERK8syVectorRKfRKiS6_
_ZN8sySphereC1ERK8syVectorRKfRKiS6_ = _ZN8sySphereC2ERK8syVectorRKfRKiS6_
	.globl	_ZN10syTriangleC1Ei
_ZN10syTriangleC1Ei = _ZN10syTriangleC2Ei
	.globl	_ZN10syTriangleC1Ev
_ZN10syTriangleC1Ev = _ZN10syTriangleC2Ev
	.globl	_ZN10syTriangleC1ERKS_
_ZN10syTriangleC1ERKS_ = _ZN10syTriangleC2ERKS_
	.globl	_ZN10syTriangleC1ERK8syVectorS2_S2_
_ZN10syTriangleC1ERK8syVectorS2_S2_ = _ZN10syTriangleC2ERK8syVectorS2_S2_
	.globl	_ZN8syVectorC1Ei
_ZN8syVectorC1Ei = _ZN8syVectorC2Ei
	.globl	_ZN8syVectorC1Ev
_ZN8syVectorC1Ev = _ZN8syVectorC2Ev
	.globl	_ZN8syVectorC1ERKfS1_S1_
_ZN8syVectorC1ERKfS1_S1_ = _ZN8syVectorC2ERKfS1_S1_
	.globl	_ZN8syVectorC1EPKf
_ZN8syVectorC1EPKf = _ZN8syVectorC2EPKf
