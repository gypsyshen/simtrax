#include "sySphere.h"
#include "Utility.h"


sySphere::sySphere() {}
sySphere::sySphere(const syVector &_p, const float &_r, const int &_id_obj, const int &_id_mat) { 
	Set(_p, _r, _id_obj, _id_mat); 
}
// set values
void sySphere::Set(const syVector &_p, const float &_r) {
	pos = _p;
	radius = _r;
}
void sySphere::Set(const syVector &_p, const float &_r, const int &_id_obj, const int &_id_mat) {
	Set(_p, _r);
	id_obj = _id_obj;
	id_mat = _id_mat;
}

float sySphere::IntersectsWShadowRay(const Ray &ray) const {
	// in world space
	float r = radius;
	syVector Ctr = pos;	//the center of the sySphere
	syVector Dir = ray.dir;	//the direction of the ray
	syVector Cmr = ray.p;	//the camera's position

	float A = Dir.LengthSquared();
	float B =  Dir % (Cmr - Ctr) * 2.0f;
	float C = -r*r + (Cmr - Ctr).LengthSquared();
	float det = B*B - 4.0f*A*C;

	if(det < 0)	return BIGFLOAT;	// no interaction
	
	float sqrtD = sqrt(det);
	float inv2A = 0.5f/A;

	float t1 = (-B-sqrtD) * inv2A;
	if( t1 > 0 ) return t1;

	float t2 = (-B+sqrtD) * inv2A;
	if( t2 > 0  ) return t2;

	return BIGFLOAT; 
}

bool sySphere::IntersectsW( const Ray &ray, HitRecord &hit ) const {
	
	// in world space
	float r = radius;
	syVector Ctr = pos;	//the center of the sySphere
	syVector Dir = ray.dir;	//the direction of the ray
	syVector Cmr = ray.p;	//the camera's position

	float A = Dir.LengthSquared();
	float B =  Dir % (Cmr - Ctr) * 2.0f;
	float C = -r*r + (Cmr - Ctr).LengthSquared();
	float det = B*B - 4.0f*A*C;

	if(det < 0)	return false;	// no interaction
	
	float sqrtD = sqrt(det);
	float inv2A = 0.5f/A;

	float t1 = (-B-sqrtD) * inv2A;
	if( t1 > 0 && t1 < hit.t ) {
		hit.t = t1;
//		hit.p = ray.p + ray.dir * t1;
//		hit.N = Normal(hit.p);
		hit.id_mat = ID_MAT();
		hit.id_obj = ID_OBJ();
//		hit.front = true;
		return true;
	}

	float t2 = (-B+sqrtD) * inv2A;
	if( t2 > 0 && t2 < hit.t ) {
		hit.t = t2;
//		hit.p = ray.p + ray.dir * t2;
//		hit.N = Normal(hit.p);
		hit.id_mat = ID_MAT();
		hit.id_obj = ID_OBJ();
//		hit.front = false;
		return true;
	}

	return false; 
} 


syVector sySphere::Normal( const syVector &P ) const {
	return (P - this->pos).GetNormalized();
}

const int& sySphere::ID_OBJ() const {
	return id_obj;
}
const int& sySphere::ID_MAT() const {
	return id_mat;
}