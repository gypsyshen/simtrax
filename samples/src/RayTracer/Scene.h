#ifndef _Scene_H
#define _Scene_H

#include "trax.hpp"
#include "sySphere.h"
#include "Lambertian.h"
#include "PointLight.h"
#include "Color.h"
#include "Utility.h"
#include "syTriangle.h"

class Scene {
public:
	
	// lights
	PointLight light;

	// triangles
	int start_tri, n_tri;

	// materials
	int start_mat;

	// BVH tree
	int start_bvh;

	//
	Color background;
	Color ambient;

	float Kd, Ka;

public:

	Color GetColor(const Ray &ray, const int &rayDepth) const;
	Color Shade( const Ray &ray, const HitRecord &hit, Ray &new_ray, Color &diffuse) const;

	void TraceRay(const Ray &ray, HitRecord &hit) const;
	void TraceShadowRay(const Ray &ray, HitDist &hit_shadow) const;

	Ray HemiRay(const syVector &P, const syVector &N) const;

	void Load();
	int BVHNodeAddr(const int &node_id) const;
};

#endif