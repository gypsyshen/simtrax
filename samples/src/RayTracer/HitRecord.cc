#include "HitRecord.h"
#include "Utility.h"

// -------------------- HitRecord --------------------
HitRecord::HitRecord() {
	Init();
}

void HitRecord::Init() {
	t = BIGFLOAT;
	id_mat = -1;
	id_obj = -1;
//	front = false;
}

bool HitRecord::Hit() const {
	return ( t == BIGFLOAT ) ? false : true;
}

HitRecord& HitRecord::operator=(const HitRecord &_hit) {

	t = _hit.t;
	id_mat = _hit.id_mat;
	id_obj = _hit.id_obj;
//	p = _hit.p;
//	N = _hit.N;
//	front = _hit.front;

	return *this;
}

// -------------------- HitDist --------------------
void HitDist::Init() { t = BIGFLOAT; }
HitDist:: HitDist() { Init(); }