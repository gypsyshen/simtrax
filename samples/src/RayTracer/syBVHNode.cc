#include "syBVHNode.h"
#include "syTriangle.h"
#include "Utility.h"

syBVHNode::syBVHNode(int _addr) {
	LoadFromMemory( _addr );
}

void syBVHNode::LoadFromMemory(const int &_addr) {
	box.LoadFromMemory(_addr);
	n_leaftri = loadi(_addr+6);
	child = loadi(_addr+7);
}

bool syBVHNode::IntersectBoxW(const Ray &r, HitDist &t_box_near) const {
	return box.IntersectsW(r, t_box_near);
}

bool syBVHNode::Interior() const {
	if( n_leaftri == -1 ) return true;
	return false;
}

int syBVHNode::LeftChildID() const {
	return child;
}
int syBVHNode::RightChildID() const {
	return child + 1;
}

void syBVHNode::TraceShadowRay(const Ray &ray, HitDist &hit_dist) const {
	for(int k = 0; k != n_leaftri; ++k) {
		int addr_tri = child + k * TRI_SIZE_IN_TRAX;
		syTriangle tri( addr_tri );
		tri.IntersectsWShadowRay( ray, hit_dist );
	}
}

void syBVHNode::TraceRay(const Ray &ray, HitRecord &hit) const {
	for(int k = 0; k != n_leaftri; ++k) {
		int addr_tri = child + k * TRI_SIZE_IN_TRAX;
		syTriangle tri( addr_tri );
		tri.IntersectsW(ray, hit);
	}
}