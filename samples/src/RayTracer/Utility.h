#ifndef _Utility_H
#define _Utility_H

// -------------------- global definition --------------------
#define BIGFLOAT 100000.0f
#define SMALLFLOAT 0.0001f
#define FLOAT_BIAS 0.0001f
#define TRI_SIZE_IN_TRAX 11
#define MAT_SIZE_IN_TRAX 25
#define DIFFUSE_OFFSET_IN_TRAX 4
#define BVHNODE_SIZE_IN_TRAX 8

// -------------------- stack --------------------
class IntStack {
private:
	int data[32];
	int size; // stack size

public:
	IntStack() { Clear(); }

	bool Empty() const { 
		if(size > 0) return false; 
		return true; 
	}
	void Push(int value) { data[size] = value; size++;}
	int Pop() {
		--size;
		int value = data[size];
		data[size] = -1;
		return value;
	}
	int Size() const { return size; }
	void Clear() {
		size = 0; 
		for(int i = 0; i != 32; ++i) data[i] = -1;
	}
};


// -------------------- utility --------------------
namespace util {
	const float PI = 3.1415926f;

	float fabs(const float& x);
	void fswap(float &a, float &b);
	float finv(const float &a);
	int bincheck(const float &a, const float &b);
};

inline float util::fabs(const float& x) {
	if (x < 0) return -x; 
	return x;
}

inline void util::fswap(float &a, float &b) {
	float c = a;
	a = b;
	b = c;
}

inline float util::finv(const float &a) {

	if(a < 0 && a > -SMALLFLOAT) return -BIGFLOAT;
	
	if(a >= 0 && a < SMALLFLOAT) return BIGFLOAT;

	return 1.f/a;
}

inline int util::bincheck(const float &a, const float &b) {
	
	if (a < b) return 0;
	return 1;
}

#endif