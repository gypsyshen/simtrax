--model ../samples/scenes/conference/conference.obj
--view-file ../samples/scenes/conference/conference.view
--light-file ../samples/scenes/conference/conference.light
--num-thread-procs 32
--config-file ../samples/configs/bigcache.config
--num-TMs 20
--num-l2s 4
--num-icaches 2
--num-icache-banks 16
--load-assembly ../samples/src/RayTracer/rt-llvm.s
--simulation-threads 4
--output-prefix ../samples/src/RayTracer/out-conf-3-1.png
--ray-depth 1
--num-samples 3
loading model ../samples/scenes/conference/conference.obj
loading material file ../samples/scenes/conference/conference.mtl
43 total materials
282664 total triangles
604956 total vertex normals
vertex min/max = x: (-0.177790, 11.125200) y: (-0.164592, 7.010400) z: (-0.005078, 2.712720)
Starting BVH build.
BVH build complete with 266201 nodes.
before rotations, SAH cost = 40.241394
BVH bounds [-0.177790 -0.164592 -0.005078] [11.125200 7.010400 2.712720]
Loading assembly file ../samples/src/RayTracer/rt-llvm.s
No USIMM configuration specified, using default: ../samples/configs/usimm_configs/gddr5_8ch.cfg
Initializing usimm memory module.
---------------------------------------------
-- USIMM: the Utah SImulated Memory Module --
--              Version: 1.3               --
---------------------------------------------
Reading vi file: 1Gb_x16_amd2GHz.vi	
16 Chips per Rank
	 <== Setup time:          7.6 s ==>
Creating thread 0...
Creating thread 1...
Creating thread 2...
Creating thread 3...
Thread 3 running cores	60 to	79 ...
Thread 0 running cores	0 to	19 ...
Thread 1 running cores	20 to	39 ...
Thread 2 running cores	40 to	59 ...
	 <== Frame time:        710.5 s ==>
System-wide instruction stats (sum/average of all cores):
profile data:
kernel	total calls	total cycles
Data dependence stalls (caused by):
   ADD         	370490	(0.251 %)
   FPADD       	1520668	(1.031 %)
   FPMUL       	29268579	(19.850 %)
   LOAD        	5498670	(3.729 %)
   FPINVSQRT   	4936117	(3.348 %)
   FPLT        	103	(0.000 %)
   RAND        	156863	(0.106 %)
   ADDI        	144902	(0.098 %)
   ORI         	3078574	(2.088 %)
   LWI         	46328583	(31.421 %)
   FPDIV       	31306865	(21.233 %)
   DIV         	245375	(0.166 %)
   FPUN        	24104	(0.016 %)
   FPRSUB      	17518145	(11.881 %)
   FPGT        	7048535	(4.780 %)
Number of thread-cycles resource conflicts found when issuing:
   ADD         	1071	(0.031 %)
   MUL         	11	(0.000 %)
   FPADD       	2520	(0.074 %)
   FPMUL       	6621	(0.193 %)
   LOAD        	2504023	(73.104 %)
   ATOMIC_INC  	8320	(0.243 %)
   FPINVSQRT   	2689	(0.079 %)
   STORE       	3437	(0.100 %)
   RAND        	788	(0.023 %)
   CMP         	48	(0.001 %)
   ADDI        	1132	(0.033 %)
   MULI        	452677	(13.216 %)
   bneid       	1	(0.000 %)
   FPDIV       	427427	(12.479 %)
   DIV         	12546	(0.366 %)
   FPRSUB      	1955	(0.057 %)
Dynamic Instruction Mix: (1110144674 total)
   ADD         	124153935	(11.184 %)
   MUL         	18944	(0.002 %)
   BITOR       	3713416	(0.334 %)
   BITAND      	1718498	(0.155 %)
   FPADD       	9620438	(0.867 %)
   FPMUL       	67959936	(6.122 %)
   FPMAX       	28973	(0.003 %)
   LOAD        	70301726	(6.333 %)
   ATOMIC_INC  	18944	(0.002 %)
   FPINVSQRT   	393216	(0.035 %)
   FPCONV      	40448	(0.004 %)
   FPNE        	49152	(0.004 %)
   FPLT        	22245938	(2.004 %)
   FPLE        	802859	(0.072 %)
   STORE       	49152	(0.004 %)
   LOADIMM     	2560	(0.000 %)
   RAND        	223600	(0.020 %)
   CMP         	5117862	(0.461 %)
   RSUB        	16384	(0.001 %)
   ADDI        	138755363	(12.499 %)
   ORI         	17955040	(1.617 %)
   XORI        	147456	(0.013 %)
   MULI        	17898497	(1.612 %)
   LW          	18008180	(1.622 %)
   LWI         	146211100	(13.170 %)
   SW          	669517	(0.060 %)
   SWI         	96107840	(8.657 %)
   beqid       	4103757	(0.370 %)
   bgeid       	100864	(0.009 %)
   bgtid       	2838944	(0.256 %)
   bltid       	68096	(0.006 %)
   bneid       	138824695	(12.505 %)
   brid        	2741739	(0.247 %)
   brlid       	5120	(0.000 %)
   rtsd        	5120	(0.000 %)
   bslli       	12706457	(1.145 %)
   FPDIV       	2541698	(0.229 %)
   DIV         	16384	(0.001 %)
   FPUN        	3713416	(0.334 %)
   FPRSUB      	56203426	(5.063 %)
   FPNEG       	557565	(0.050 %)
   FPGT        	40104822	(3.613 %)
   FPGE        	5185174	(0.467 %)
   NOP         	98198423	(8.846 %)
Issue statistics:
 --Average #threads Issuing each cycle: 1863.5598
 --Issue Rate: 72.80%
 --iCache conflicts: 38385557 (2.761426%)
 --thread*cycles of resource conflicts: 3425266 (0.246398%)
 --thread*cycles of data dependence: 147446573 (10.606139%)
 --thread*cycles halted: 91470218 (6.526685%)
 --thread*cycles of issue NOP/other: 1241735 (7.064099%)
Module Utilization

	           FP AddSub:   17.89
	           FP MinMax:    0.00
	          FP Compare:    9.84
	          Int AddSub:   36.30
	              FP Mul:   18.41
	             Int Mul:   19.41
	          FP InvSqrt:    0.85
	              FP Div:    5.54
	     Conversion Unit:    0.01

System-wide L1 stats (sum of all TMs):
L1 accesses: 	70350878
L1 hits: 	69212337
L1 misses: 	1138541
L1 bank conflicts: 	2411480
L1 stores: 	49152
L1 hit rate: 	0.983816
Hit under miss: 631983

 -= L2 #0 =-
L2 accesses: 	126285
L2 hits: 	67154
L2 misses: 	59131
L2 stores: 	12309
L2 bank conflicts: 	23479
L2 hit rate: 	0.531765
L2 memory faults: 0

 -= L2 #1 =-
L2 accesses: 	126776
L2 hits: 	67866
L2 misses: 	58910
L2 stores: 	12270
L2 bank conflicts: 	24195
L2 hit rate: 	0.535322
L2 memory faults: 0

 -= L2 #2 =-
L2 accesses: 	129346
L2 hits: 	69388
L2 misses: 	59958
L2 stores: 	12267
L2 bank conflicts: 	23921
L2 hit rate: 	0.536453
L2 memory faults: 0

 -= L2 #3 =-
L2 accesses: 	124151
L2 hits: 	66640
L2 misses: 	57511
L2 stores: 	12306
L2 bank conflicts: 	24385
L2 hit rate: 	0.536766
L2 memory faults: 0

Bandwidth numbers for 1000MHz clock (GB/s):
   L1 to register bandwidth: 	 453.258179
   L2 to L1 bandwidth: 		 47.151764
   memory to L2 bandwidth: 	 18.502546

Chip area (mm2):
   Functional units: 	 30.016640
   L1 data caches: 	 185.419922
   L2 data caches: 	 14.193360
   Instruction caches: 	 20.895359
   Localstore units: 	 35.746021
   Register files: 	 37.802242
   ------------------------------
   Total: 		 324.073545

Energy consumption (Joules):
   Functional units: 	 0.004339
   L1 data caches: 	 0.025595
   L2 data caches: 	 0.000215
   Instruction caches: 	 0.014976
   Localstore units: 	 0.001808
   Register files: 	 0.023971
   DRAM: 		 0.021307
   ------------------------------
   Total: 		 0.092210
   Power draw (watts): 	 148.523599

FPS Statistics:
   Total clock cycles: 		 620846
   FPS assuming 1000MHz clock: 	 1610.7054


-------------DRAM stats-------------
Cycles 1241692
-------- Channel 0 Stats-----------
Total Reads Serviced :          13584  
Total Writes Serviced :         2155   
Average Read Latency :          50.08768
Average Read Queue Latency :    18.08768
Average Write Latency :         90.49188
Average Write Queue Latency :   56.49188
Read Page Hit Rate :            0.84541
Write Page Hit Rate :           0.90580
Max write queue length:         16
Max read queue length:          19
Average read queue length:      0.208817
Average column reads per ACT:   6.468572
Single column reads:            1020
Single columng reads(%):        7.508834
------------------------------------
-------- Channel 1 Stats-----------
Total Reads Serviced :          13677  
Total Writes Serviced :         2638   
Average Read Latency :          56.57089
Average Read Queue Latency :    24.57089
Average Write Latency :         41.85027
Average Write Queue Latency :   7.85027
Read Page Hit Rate :            0.82971
Write Page Hit Rate :           0.98256
Max write queue length:         4
Max read queue length:          55
Average read queue length:      0.281658
Average column reads per ACT:   5.872478
Single column reads:            1004
Single columng reads(%):        7.340791
------------------------------------
-------- Channel 2 Stats-----------
Total Reads Serviced :          8553   
Total Writes Serviced :         2513   
Average Read Latency :          60.76418
Average Read Queue Latency :    28.76418
Average Write Latency :         56.63191
Average Write Queue Latency :   22.63191
Read Page Hit Rate :            0.75389
Write Page Hit Rate :           0.94310
Max write queue length:         12
Max read queue length:          23
Average read queue length:      0.205021
Average column reads per ACT:   4.063183
Single column reads:            1079
Single columng reads(%):        12.615458
------------------------------------
-------- Channel 3 Stats-----------
Total Reads Serviced :          12762  
Total Writes Serviced :         2744   
Average Read Latency :          58.58494
Average Read Queue Latency :    26.58494
Average Write Latency :         43.81159
Average Write Queue Latency :   9.81159
Read Page Hit Rate :            0.81719
Write Page Hit Rate :           0.98688
Max write queue length:         16
Max read queue length:          33
Average read queue length:      0.283516
Average column reads per ACT:   5.470210
Single column reads:            1036
Single columng reads(%):        8.117850
------------------------------------
-------- Channel 4 Stats-----------
Total Reads Serviced :          11455  
Total Writes Serviced :         2553   
Average Read Latency :          49.02366
Average Read Queue Latency :    17.02366
Average Write Latency :         48.99608
Average Write Queue Latency :   14.99608
Read Page Hit Rate :            0.88695
Write Page Hit Rate :           0.95574
Max write queue length:         6
Max read queue length:          17
Average read queue length:      0.166274
Average column reads per ACT:   8.845560
Single column reads:            574
Single columng reads(%):        5.010912
------------------------------------
-------- Channel 5 Stats-----------
Total Reads Serviced :          10570  
Total Writes Serviced :         2682   
Average Read Latency :          63.55582
Average Read Queue Latency :    31.55582
Average Write Latency :         51.78635
Average Write Queue Latency :   17.78635
Read Page Hit Rate :            0.73983
Write Page Hit Rate :           0.99590
Max write queue length:         18
Max read queue length:          27
Average read queue length:      0.277134
Average column reads per ACT:   3.843636
Single column reads:            1251
Single columng reads(%):        11.835382
------------------------------------
-------- Channel 6 Stats-----------
Total Reads Serviced :          10619  
Total Writes Serviced :         2492   
Average Read Latency :          57.31820
Average Read Queue Latency :    25.31820
Average Write Latency :         77.14687
Average Write Queue Latency :   43.14687
Read Page Hit Rate :            0.78115
Write Page Hit Rate :           0.89526
Max write queue length:         13
Max read queue length:          17
Average read queue length:      0.225074
Average column reads per ACT:   4.569277
Single column reads:            1141
Single columng reads(%):        10.744891
------------------------------------
-------- Channel 7 Stats-----------
Total Reads Serviced :          12575  
Total Writes Serviced :         2428   
Average Read Latency :          57.81567
Average Read Queue Latency :    25.81567
Average Write Latency :         62.57949
Average Write Queue Latency :   28.57949
Read Page Hit Rate :            0.80374
Write Page Hit Rate :           0.86614
Max write queue length:         6
Max read queue length:          32
Average read queue length:      0.271571
Average column reads per ACT:   5.095219
Single column reads:            1165
Single columng reads(%):        9.264413
------------------------------------
-------- Channel 8 Stats-----------
Total Reads Serviced :          19569  
Total Writes Serviced :         1053   
Average Read Latency :          60.53314
Average Read Queue Latency :    28.53314
Average Write Latency :         108.81007
Average Write Queue Latency :   74.81007
Read Page Hit Rate :            0.82610
Write Page Hit Rate :           0.79202
Max write queue length:         8
Max read queue length:          40
Average read queue length:      0.465441
Average column reads per ACT:   5.750514
Single column reads:            1578
Single columng reads(%):        8.063774
------------------------------------
-------- Channel 9 Stats-----------
Total Reads Serviced :          4272   
Total Writes Serviced :         1240   
Average Read Latency :          59.55478
Average Read Queue Latency :    27.55478
Average Write Latency :         59.88952
Average Write Queue Latency :   25.88952
Read Page Hit Rate :            0.79424
Write Page Hit Rate :           0.91532
Max write queue length:         8
Max read queue length:          11
Average read queue length:      0.098242
Average column reads per ACT:   4.860068
Single column reads:            362
Single columng reads(%):        8.473783
------------------------------------
-------- Channel 10 Stats-----------
Total Reads Serviced :          5539   
Total Writes Serviced :         1230   
Average Read Latency :          51.24012
Average Read Queue Latency :    19.24012
Average Write Latency :         56.27236
Average Write Queue Latency :   22.27236
Read Page Hit Rate :            0.80809
Write Page Hit Rate :           0.93659
Max write queue length:         9
Max read queue length:          10
Average read queue length:      0.090288
Average column reads per ACT:   5.210724
Single column reads:            478
Single columng reads(%):        8.629716
------------------------------------
-------- Channel 11 Stats-----------
Total Reads Serviced :          15475  
Total Writes Serviced :         1014   
Average Read Latency :          51.63186
Average Read Queue Latency :    19.63186
Average Write Latency :         101.95069
Average Write Queue Latency :   67.95069
Read Page Hit Rate :            0.85616
Write Page Hit Rate :           0.83531
Max write queue length:         8
Max read queue length:          33
Average read queue length:      0.257131
Average column reads per ACT:   6.951932
Single column reads:            1081
Single columng reads(%):        6.985460
------------------------------------
-------- Channel 12 Stats-----------
Total Reads Serviced :          15189  
Total Writes Serviced :         986    
Average Read Latency :          64.28093
Average Read Queue Latency :    32.28093
Average Write Latency :         114.97059
Average Write Queue Latency :   80.97059
Read Page Hit Rate :            0.73593
Write Page Hit Rate :           0.93712
Max write queue length:         9
Max read queue length:          29
Average read queue length:      0.407109
Average column reads per ACT:   3.786836
Single column reads:            1848
Single columng reads(%):        12.166699
------------------------------------
-------- Channel 13 Stats-----------
Total Reads Serviced :          9524   
Total Writes Serviced :         1024   
Average Read Latency :          61.32014
Average Read Queue Latency :    29.32014
Average Write Latency :         94.56348
Average Write Queue Latency :   60.56348
Read Page Hit Rate :            0.73058
Write Page Hit Rate :           0.86816
Max write queue length:         11
Max read queue length:          19
Average read queue length:      0.232561
Average column reads per ACT:   3.711613
Single column reads:            1284
Single columng reads(%):        13.481730
------------------------------------
-------- Channel 14 Stats-----------
Total Reads Serviced :          8608   
Total Writes Serviced :         1096   
Average Read Latency :          55.95992
Average Read Queue Latency :    23.95992
Average Write Latency :         75.13230
Average Write Queue Latency :   41.13230
Read Page Hit Rate :            0.79426
Write Page Hit Rate :           0.85949
Max write queue length:         6
Max read queue length:          14
Average read queue length:      0.173034
Average column reads per ACT:   4.860531
Single column reads:            844
Single columng reads(%):        9.804832
------------------------------------
-------- Channel 15 Stats-----------
Total Reads Serviced :          7517   
Total Writes Serviced :         1210   
Average Read Latency :          56.22270
Average Read Queue Latency :    24.22270
Average Write Latency :         67.59835
Average Write Queue Latency :   33.59835
Read Page Hit Rate :            0.77491
Write Page Hit Rate :           0.92397
Max write queue length:         14
Max read queue length:          15
Average read queue length:      0.152694
Average column reads per ACT:   4.442671
Single column reads:            825
Single columng reads(%):        10.975123
------------------------------------

#-------------------------------------------------------------------------------------------------
Total memory system power = 34.318775 W
	 <== Total time:        718.1 s ==>
