#include "Color.h"

Color::Color(int _addr) {
	LoadFromMemory(_addr);
}

Color::Color() {
	r = 1.f;
	g= 1.f;
	b = 1.f;
}

Color::Color(const Color& copy) {
	r = copy.r;
	g = copy.g;
	b = copy.b;
}

Color::Color(const float &_r, const float &_g, const float &_b) {
	r = _r;
	g = _g;
	b = _b;
}

void Color::LoadFromMemory(const int &addr) {
	r = loadf(addr, 0);
	g = loadf(addr, 1);
	b = loadf(addr, 2);
}

Color Color::operator+( const Color &c ) const { return Color(r+c.r, g+c.g, b+c.b); };
Color Color::operator-( const Color &c ) const { return Color(r-c.r, g-c.g, b-c.b); };
Color Color::operator*( const Color &c ) const { return Color(r*c.r, g*c.g, b*c.b); };
Color Color::operator/( const Color &c ) const { return Color(r/c.r, g/c.g, b/c.b); };
Color Color::operator+(float n) const { return Color(r+n, g+n, b+n); };
Color Color::operator-(float n) const { return Color(r-n, g-n, b-n); };
Color Color::operator*(float n) const { return Color(r*n, g*n, b*n); };
Color Color::operator/(float n) const { return Color(r/n, g/n, b/n); };
 
///@name Assignment operators
Color& Color::operator+=( const Color &c ) { r+=c.r; g+=c.g; b+=c.b; return *this; };
Color& Color::operator-=( const Color &c ) { r-=c.r; g-=c.g; b-=c.b; return *this; };
Color& Color::operator*=( const Color &c ) { r*=c.r; g*=c.g; b*=c.b; return *this; };
Color& Color::operator/=( const Color &c ) { r/=c.r; g/=c.g; b/=c.b; return *this; };
Color& Color::operator+=(float n) { r+=n; g+=n; b+=n; return *this; };
Color& Color::operator-=(float n) { r-=n; g-=n; b-=n; return *this; };
Color& Color::operator*=(float n) { r*=n; g*=n; b*=n; return *this; };
Color& Color::operator/=(float n) { r/=n; g/=n; b/=n; return *this; };

Color Color::Clamp() const{
	Color c2;
	c2.r = R() < 0.f ? 0.f : (  R() > 1.f ? 1.f : R()  );
	c2.g = G() < 0.f ? 0.f : (  G() > 1.f ? 1.f : G()  );
	c2.b = B() < 0.f ? 0.f : (  B() > 1.f ? 1.f : B()  );
	return c2;
}

Color Color::Clamp(const Color& l, const Color &h) const{
	Color c2;
	c2.r = R() < l.R() ? l.R() : (  R() > h.R() ? h.R() : R()  );
	c2.g = G() < l.G() ? l.G() : (  G() > h.G() ? h.G() : G()  );
	c2.b = B() < l.B() ? l.B() : (  B() > h.B() ? h.B() : B()  );
	return c2;
}

const float& Color::R() const { return r; }
const float& Color::G() const { return g; }
const float& Color::B() const { return b; }