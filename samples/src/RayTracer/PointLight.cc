#include "PointLight.h"

PointLight::PointLight(int _addr, const Color &_c) {
	LoadFromMemory(_addr);
	intensity = _c;
}

PointLight::PointLight() {
	position = syVector(0.0f, 0.0f, 0.0f);
	intensity = Color(0.0f, 0.0f, 0.0f);
}


PointLight::PointLight(const syVector &_p, const Color &_c) {

	position = _p;
	intensity = _c;
}

void PointLight::LoadFromMemory(const int &_addr) {
	position.LoadFromMemory(_addr);
}

void PointLight::SetIntensity(const Color &_c) {
	intensity = _c;
}

const syVector& PointLight::Position() const {
	return position;
}

const Color& PointLight::Intensity() const {
	return intensity;
}

syVector PointLight::Direction(const syVector &_p) const {
	return position - _p;
} 