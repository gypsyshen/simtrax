#include "Scene.h"
#include "Utility.h"
#include "syBVHNode.h"

void Scene::Load() {

	// lights
	light.LoadFromMemory( GetLight() );
	light.SetIntensity( Color(1.f, 1.f, 1.f) );

	// triangles
	start_tri = GetStartTriangles();
	n_tri = GetNumTriangles();
	
	// materials
	start_mat = GetMaterials();

	// BVH tree
	start_bvh = GetBVH();

	// background
//	background.LoadFromMemory( GetBackground() );
	background = Color(0.561f, 0.729f, 0.988f);

	// ambient
/*	ambient = Color(0.98f, 0.92f, 0.84);
	Ka = 0.3f;
	Kd = 0.9f;*/
}

int Scene::BVHNodeAddr(const int &node_id) const {
	return start_bvh + node_id * BVHNODE_SIZE_IN_TRAX;
}

/* void Scene::TraceRay(const Ray &ray, HitRecord &hit) const{
	
	IntStack nodeIDs;
	nodeIDs.Push(0);
	HitDist t_box_near;
	while( !nodeIDs.Empty() ) {
		int node_id = nodeIDs.Pop();
		int node_addr = BVHNodeAddr(node_id);
		syBVHNode node(node_addr);
		// 1 -- no intersection
		if(!node.IntersectBoxW(ray, t_box_near )) continue;
		if ( hit.t < t_box_near.t ) continue;
		// 2 -- interior node
		if(node.Interior()) {
			// left bvh node
			int left_id = node.LeftChildID();
			int left_addr = BVHNodeAddr(left_id);
			syBVHNode left_node(left_addr);
			HitDist t_left_near;
			bool intersect_left = left_node.IntersectBoxW(ray, t_left_near);
			// right bvh node
			int right_id = node.RightChildID();
			int right_addr = BVHNodeAddr(right_id);
			syBVHNode right_node(right_addr);
			HitDist t_right_near;
			bool intersect_right = right_node.IntersectBoxW(ray, t_right_near);
			if( !(intersect_left || intersect_right) ) {
				continue;
			} else if(intersect_left && !intersect_right) {
				nodeIDs.Push(left_id);
			} else if(intersect_right && !intersect_left) {
				nodeIDs.Push(right_id);
			} else {
				if(t_left_near.t < t_right_near.t) {
					nodeIDs.Push(right_id);
					nodeIDs.Push(left_id);
				} else {
					nodeIDs.Push(left_id);
					nodeIDs.Push(right_id);
				}
			}

			continue;
		}
		// 3 -- leaf node		
		node.TraceRay(ray, hit);

	}

} */

void Scene::TraceRay(const Ray &ray, HitRecord &hit) const{
	
	int nodeIDs[32];
	int sp = 0; // stack pointer
	nodeIDs[sp++] = 0;
	HitDist t_box_near;
	while( sp > 0 ) {
		int node_id = nodeIDs[--sp];
		int node_addr = BVHNodeAddr(node_id);
		syBVHNode node(node_addr);
		// 1 -- no intersection
		if(!node.IntersectBoxW(ray, t_box_near )) continue;
		if ( hit.t < t_box_near.t ) continue;
		// 2 -- interior node
		if(node.Interior()) {
			// left bvh node
			int left_id = node.LeftChildID();
			int left_addr = BVHNodeAddr(left_id);
			syBVHNode left_node(left_addr);
			HitDist t_left_near;
			bool intersect_left = left_node.IntersectBoxW(ray, t_left_near);
			// right bvh node
			int right_id = node.RightChildID();
			int right_addr = BVHNodeAddr(right_id);
			syBVHNode right_node(right_addr);
			HitDist t_right_near;
			bool intersect_right = right_node.IntersectBoxW(ray, t_right_near);
			if( !(intersect_left || intersect_right) ) {
				continue;
			} else if(intersect_left && !intersect_right) {
				nodeIDs[sp++] = left_id;
			} else if(intersect_right && !intersect_left) {
				nodeIDs[sp++] = right_id;
			} else {
				if(t_left_near.t < t_right_near.t) {
					nodeIDs[sp++] = right_id;
					nodeIDs[sp++] = left_id;
				} else {
					nodeIDs[sp++] = left_id;
					nodeIDs[sp++] = right_id;
				}
			}

			continue;
		}
		// 3 -- leaf node		
		node.TraceRay(ray, hit);

	}

}


void Scene::TraceShadowRay(const Ray &ray, HitDist &hit_shadow) const {
	
	HitDist t_box_near;
	int nodeIDs[32];
	int sp = 0; //stack pointer
	nodeIDs[sp++] = 0;
	while( sp > 0 ) {
		int node_id = nodeIDs[--sp];
		int node_addr = BVHNodeAddr(node_id);
		syBVHNode node(node_addr);
		// 1 -- no intersection with the box
		if(!node.IntersectBoxW(ray, t_box_near)) continue;
		if ( hit_shadow.t < t_box_near.t ) continue;
		// 2 -- interior node
		if(node.Interior()) {
			// left bvh node
			int left_id = node.LeftChildID();
			int left_addr = BVHNodeAddr(left_id);
			syBVHNode left_node(left_addr);
			HitDist t_left_near;
			bool intersect_left = left_node.IntersectBoxW(ray, t_left_near);
			// right bvh node
			int right_id = node.RightChildID();
			int right_addr = BVHNodeAddr(right_id);
			syBVHNode right_node(right_addr);
			HitDist t_right_near;
			bool intersect_right = right_node.IntersectBoxW(ray, t_right_near);
			if( !(intersect_left || intersect_right) ) {
				continue;
			} else if(intersect_left && !intersect_right) {
				nodeIDs[sp++] = left_id;
			} else if(intersect_right && !intersect_left) {
				nodeIDs[sp++] = right_id;
			} else {
				if(t_left_near.t < t_right_near.t) {
					nodeIDs[sp++] = right_id;
					nodeIDs[sp++] = left_id;
				} else {
					nodeIDs[sp++] = left_id;
					nodeIDs[sp++] = right_id;
				}
			}
			continue;
		}
		// 3 -- leaf node
		node.TraceShadowRay(ray, hit_shadow);
	}
}

Ray Scene::HemiRay(const syVector &P, const syVector &N) const {
	// Returns a a random vector in the hemisphere defined by primary axis v
	// Direction probability is weighted by the cosine of the angle between v and the returned vector

	syVector refDir(0.0f, 0.0f, 0.0f);

	// pick random point on disk
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	float x_2 = 0.0f; // squares
	float y_2 = 0.0f;
	do // random point on [-1, 1] square, until radius <= 1 (thus, on unit disc)
	{
		x = trax_rand(); 
		y = trax_rand();
		x = x * 2.0f;
		x = x - 1.0f;
		y = y * 2.0f;
		y = y - 1.0f;
		x_2 = x * x;
		y_2 = y * y;
	}
	while((x_2 + y_2) >= 1.0f); // cut out points outside the disk
  
	// we have [x,y] on disc plane, z = project up to unit hemisphere, radius must be 1
	z = sqrt(1.0f - x_2 - y_2); 

	syVector X = N.GetOrtho(); // get any vector orthogonal to v
	syVector Y = N.Cross(X); // get the vector orthogonal to v and X
	// X, Y, v now form an orthonormal basis

	// hemisphere direction is random point on disc projected up to unit hemisphere
	refDir = (X * x) + (Y * y) + (N * z);
	refDir.Normalize();//.normalize();
  
	Ray ray;
	ray.Set(P, refDir);

	return ray;
}

Color Scene::GetColor(const Ray &ray, const int &rayDepth) const {

	Color attenuation = Color(1.f, 1.f, 1.f);
	Color result = Color(0.f, 0.f, 0.f);
	int depthCount = 0;
	Ray r(ray);
	Ray new_ray;
	Color diffuse;
	while(depthCount++ < rayDepth) {
		HitRecord hit;
		TraceRay(r, hit);

		// shade
		result += Shade(r, hit, new_ray, diffuse) * attenuation;

		if(!hit.Hit()) break;

		// generate a ray in the hemisphere
		r = new_ray;
		attenuation *= diffuse;
	}

	return result;
}

// hit, ray: in world space 
Color Scene::Shade(const Ray &ray, const HitRecord &hit, Ray &new_ray, Color &diffuse) const {

	// no hits
	if(!hit.Hit()) return background;
	
	// normal
	syTriangle tri(hit.id_obj);
	syVector N = tri.Normal();
	if(N % ray.dir > 0.f) N = N * (-1.f);
	
	// diffuse
	int addr_mat = start_mat + hit.id_mat * MAT_SIZE_IN_TRAX;
	diffuse = Color( addr_mat + DIFFUSE_OFFSET_IN_TRAX );

	// hit position
	syVector P = ray.p + ray.dir * hit.t;

	// 
	Color result = Color(0.f, 0.f, 0.f);

	// 
	syVector L = light.Direction(P); // hit point -> light sources
	float dist = L.Length();
	L.Normalize();
	// trace shadow
	Ray ray_shadow(P + N*0.05f, L);
	HitDist hit_shadow;
	TraceShadowRay(ray_shadow, hit_shadow);
	// not int shadow: direct illumination
	if(dist <= hit_shadow.t) {
		float cosphi = max(N%L, 0.0f);
		result += diffuse * light.Intensity() * cosphi;
	}
	
	// random reflection
	new_ray = HemiRay(P + N*0.05f, N);

	return result;
} 