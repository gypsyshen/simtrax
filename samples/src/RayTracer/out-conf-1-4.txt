--model ../samples/scenes/conference/conference.obj
--view-file ../samples/scenes/conference/conference.view
--light-file ../samples/scenes/conference/conference.light
--num-thread-procs 32
--config-file ../samples/configs/bigcache.config
--num-TMs 20
--num-l2s 4
--num-icaches 2
--num-icache-banks 16
--load-assembly ../samples/src/RayTracer/rt-llvm.s
--simulation-threads 4
--output-prefix ../samples/src/RayTracer/out-conf-1-4.png
--ray-depth 4
--num-samples 1
loading model ../samples/scenes/conference/conference.obj
loading material file ../samples/scenes/conference/conference.mtl
43 total materials
282664 total triangles
604956 total vertex normals
vertex min/max = x: (-0.177790, 11.125200) y: (-0.164592, 7.010400) z: (-0.005078, 2.712720)
Starting BVH build.
BVH build complete with 266201 nodes.
before rotations, SAH cost = 40.241394
BVH bounds [-0.177790 -0.164592 -0.005078] [11.125200 7.010400 2.712720]
Loading assembly file ../samples/src/RayTracer/rt-llvm.s
No USIMM configuration specified, using default: ../samples/configs/usimm_configs/gddr5_8ch.cfg
Initializing usimm memory module.
---------------------------------------------
-- USIMM: the Utah SImulated Memory Module --
--              Version: 1.3               --
---------------------------------------------
Reading vi file: 1Gb_x16_amd2GHz.vi	
16 Chips per Rank
	 <== Setup time:          7.5 s ==>
Creating thread 0...
Creating thread 1...
Creating thread 2...
Creating thread 3...
Thread 0 running cores	0 to	19 ...
Thread 1 running cores	20 to	39 ...
Thread 2 running cores	40 to	59 ...
Thread 3 running cores	60 to	79 ...
	 <== Frame time:        944.6 s ==>
System-wide instruction stats (sum/average of all cores):
profile data:
kernel	total calls	total cycles
Data dependence stalls (caused by):
   ADD         	408920	(0.206 %)
   BITOR       	3	(0.000 %)
   FPADD       	1577748	(0.794 %)
   FPMUL       	37482260	(18.859 %)
   LOAD        	17778321	(8.945 %)
   FPINVSQRT   	5895123	(2.966 %)
   FPLT        	99	(0.000 %)
   RAND        	83015	(0.042 %)
   ADDI        	198489	(0.100 %)
   ORI         	3766658	(1.895 %)
   LWI         	58041511	(29.203 %)
   FPDIV       	38389456	(19.315 %)
   DIV         	245497	(0.124 %)
   FPUN        	1	(0.000 %)
   FPRSUB      	25438715	(12.799 %)
   FPGT        	9447057	(4.753 %)
Number of thread-cycles resource conflicts found when issuing:
   ADD         	1691	(0.040 %)
   MUL         	38	(0.001 %)
   FPADD       	664	(0.016 %)
   FPMUL       	7403	(0.176 %)
   LOAD        	3211291	(76.168 %)
   ATOMIC_INC  	9326	(0.221 %)
   FPINVSQRT   	2437	(0.058 %)
   STORE       	4069	(0.097 %)
   RAND        	5	(0.000 %)
   CMP         	64	(0.002 %)
   RSUB        	1	(0.000 %)
   ADDI        	1818	(0.043 %)
   MULI        	574698	(13.631 %)
   FPDIV       	385229	(9.137 %)
   DIV         	15250	(0.362 %)
   FPRSUB      	2068	(0.049 %)
Dynamic Instruction Mix: (1448850660 total)
   ADD         	169338123	(11.688 %)
   MUL         	18944	(0.001 %)
   BITOR       	4764580	(0.329 %)
   BITAND      	2097482	(0.145 %)
   FPADD       	10950193	(0.756 %)
   FPMUL       	87238629	(6.021 %)
   FPMAX       	45853	(0.003 %)
   LOAD        	92919181	(6.413 %)
   ATOMIC_INC  	18944	(0.001 %)
   FPINVSQRT   	474933	(0.033 %)
   FPCONV      	40448	(0.003 %)
   FPNE        	65527	(0.005 %)
   FPLT        	29011113	(2.002 %)
   FPLE        	968695	(0.067 %)
   STORE       	49152	(0.003 %)
   LOADIMM     	2560	(0.000 %)
   RAND        	167102	(0.012 %)
   CMP         	6518701	(0.450 %)
   RSUB        	16384	(0.001 %)
   ADDI        	182378850	(12.588 %)
   ORI         	22863235	(1.578 %)
   XORI        	196581	(0.014 %)
   MULI        	23606225	(1.629 %)
   LW          	23880370	(1.648 %)
   LWI         	186989922	(12.906 %)
   SW          	819991	(0.057 %)
   SWI         	123004662	(8.490 %)
   beqid       	5479513	(0.378 %)
   bgeid       	84451	(0.006 %)
   bgtid       	3757732	(0.259 %)
   bltid       	16384	(0.001 %)
   bneid       	181914758	(12.556 %)
   brid        	3725980	(0.257 %)
   brlid       	5120	(0.000 %)
   rtsd        	5120	(0.000 %)
   bslli       	16960213	(1.171 %)
   FPDIV       	2887000	(0.199 %)
   DIV         	16384	(0.001 %)
   FPUN        	4764580	(0.329 %)
   FPRSUB      	73234878	(5.055 %)
   FPNEG       	671681	(0.046 %)
   FPGT        	52886521	(3.650 %)
   FPGE        	6523493	(0.450 %)
   NOP         	127470472	(8.798 %)
Issue statistics:
 --Average #threads Issuing each cycle: 1802.4140
 --Issue Rate: 70.41%
 --iCache conflicts: 48835929 (2.602275%)
 --thread*cycles of resource conflicts: 4216052 (0.224654%)
 --thread*cycles of data dependence: 198752873 (10.589866%)
 --thread*cycles halted: 177372582 (9.384402%)
 --thread*cycles of issue NOP/other: 1587550 (6.792045%)
Module Utilization

	           FP AddSub:   16.96
	           FP MinMax:    0.00
	          FP Compare:    9.54
	          Int AddSub:   36.01
	              FP Mul:   17.54
	             Int Mul:   19.00
	          FP InvSqrt:    0.76
	              FP Div:    4.67
	     Conversion Unit:    0.00

System-wide L1 stats (sum of all TMs):
L1 accesses: 	92968333
L1 hits: 	90404471
L1 misses: 	2563862
L1 bank conflicts: 	3005742
L1 stores: 	49152
L1 hit rate: 	0.972422
Hit under miss: 1509634

 -= L2 #0 =-
L2 accesses: 	260763
L2 hits: 	114566
L2 misses: 	146197
L2 stores: 	12276
L2 bank conflicts: 	52398
L2 hit rate: 	0.439349
L2 memory faults: 0

 -= L2 #1 =-
L2 accesses: 	264339
L2 hits: 	115280
L2 misses: 	149059
L2 stores: 	12279
L2 bank conflicts: 	51532
L2 hit rate: 	0.436107
L2 memory faults: 0

 -= L2 #2 =-
L2 accesses: 	264682
L2 hits: 	115687
L2 misses: 	148995
L2 stores: 	12276
L2 bank conflicts: 	53650
L2 hit rate: 	0.437079
L2 memory faults: 0

 -= L2 #3 =-
L2 accesses: 	264444
L2 hits: 	114570
L2 misses: 	149874
L2 stores: 	12321
L2 bank conflicts: 	52038
L2 hit rate: 	0.433249
L2 memory faults: 0

Bandwidth numbers for 1000MHz clock (GB/s):
   L1 to register bandwidth: 	 457.237244
   L2 to L1 bandwidth: 		 79.090706
   memory to L2 bandwidth: 	 42.205902

Chip area (mm2):
   Functional units: 	 30.016640
   L1 data caches: 	 185.419922
   L2 data caches: 	 14.193360
   Instruction caches: 	 20.895359
   Localstore units: 	 35.746021
   Register files: 	 37.802242
   ------------------------------
   Total: 		 324.073545

Energy consumption (Joules):
   Functional units: 	 0.005522
   L1 data caches: 	 0.033829
   L2 data caches: 	 0.000471
   Instruction caches: 	 0.019545
   Localstore units: 	 0.002319
   Register files: 	 0.031301
   DRAM: 		 0.038788
   ------------------------------
   Total: 		 0.131777
   Power draw (watts): 	 162.026027

FPS Statistics:
   Total clock cycles: 		 813305
   FPS assuming 1000MHz clock: 	 1229.5510


-------------DRAM stats-------------
Cycles 1626610
-------- Channel 0 Stats-----------
Total Reads Serviced :          38376  
Total Writes Serviced :         1738   
Average Read Latency :          61.31491
Average Read Queue Latency :    29.31491
Average Write Latency :         322.38723
Average Write Queue Latency :   288.38723
Read Page Hit Rate :            0.74422
Write Page Hit Rate :           0.66283
Max write queue length:         23
Max read queue length:          35
Average read queue length:      0.715208
Average column reads per ACT:   3.909535
Single column reads:            5158
Single columng reads(%):        13.440693
------------------------------------
-------- Channel 1 Stats-----------
Total Reads Serviced :          50834  
Total Writes Serviced :         1670   
Average Read Latency :          72.63304
Average Read Queue Latency :    40.63304
Average Write Latency :         280.72156
Average Write Queue Latency :   246.72156
Read Page Hit Rate :            0.64774
Write Page Hit Rate :           0.75389
Max write queue length:         10
Max read queue length:          50
Average read queue length:      1.301095
Average column reads per ACT:   2.838778
Single column reads:            9383
Single columng reads(%):        18.458118
------------------------------------
-------- Channel 2 Stats-----------
Total Reads Serviced :          28327  
Total Writes Serviced :         1977   
Average Read Latency :          69.76884
Average Read Queue Latency :    37.76884
Average Write Latency :         110.49975
Average Write Queue Latency :   76.49975
Read Page Hit Rate :            0.63618
Write Page Hit Rate :           0.84320
Max write queue length:         10
Max read queue length:          25
Average read queue length:      0.675150
Average column reads per ACT:   2.748593
Single column reads:            6053
Single columng reads(%):        21.368305
------------------------------------
-------- Channel 3 Stats-----------
Total Reads Serviced :          27691  
Total Writes Serviced :         1955   
Average Read Latency :          68.85674
Average Read Queue Latency :    36.85674
Average Write Latency :         118.49770
Average Write Queue Latency :   84.49770
Read Page Hit Rate :            0.65704
Write Page Hit Rate :           0.82916
Max write queue length:         11
Max read queue length:          24
Average read queue length:      0.644464
Average column reads per ACT:   2.915763
Single column reads:            5104
Single columng reads(%):        18.431980
------------------------------------
-------- Channel 4 Stats-----------
Total Reads Serviced :          26990  
Total Writes Serviced :         1966   
Average Read Latency :          64.98355
Average Read Queue Latency :    32.98355
Average Write Latency :         123.47762
Average Write Queue Latency :   89.47762
Read Page Hit Rate :            0.69870
Write Page Hit Rate :           0.83113
Max write queue length:         14
Max read queue length:          24
Average read queue length:      0.563882
Average column reads per ACT:   3.318987
Single column reads:            4074
Single columng reads(%):        15.094480
------------------------------------
-------- Channel 5 Stats-----------
Total Reads Serviced :          27517  
Total Writes Serviced :         1959   
Average Read Latency :          65.10179
Average Read Queue Latency :    33.10179
Average Write Latency :         134.78663
Average Write Queue Latency :   100.78663
Read Page Hit Rate :            0.70396
Write Page Hit Rate :           0.86166
Max write queue length:         15
Max read queue length:          27
Average read queue length:      0.576892
Average column reads per ACT:   3.377977
Single column reads:            4171
Single columng reads(%):        15.157902
------------------------------------
-------- Channel 6 Stats-----------
Total Reads Serviced :          29205  
Total Writes Serviced :         1967   
Average Read Latency :          71.82609
Average Read Queue Latency :    39.82609
Average Write Latency :         248.53177
Average Write Queue Latency :   214.53177
Read Page Hit Rate :            0.64236
Write Page Hit Rate :           0.78343
Max write queue length:         15
Max read queue length:          32
Average read queue length:      0.733013
Average column reads per ACT:   2.796075
Single column reads:            5649
Single columng reads(%):        19.342579
------------------------------------
-------- Channel 7 Stats-----------
Total Reads Serviced :          34198  
Total Writes Serviced :         1958   
Average Read Latency :          71.20367
Average Read Queue Latency :    39.20367
Average Write Latency :         201.26047
Average Write Queue Latency :   167.26047
Read Page Hit Rate :            0.62843
Write Page Hit Rate :           0.75230
Max write queue length:         15
Max read queue length:          38
Average read queue length:      0.845246
Average column reads per ACT:   2.691272
Single column reads:            6923
Single columng reads(%):        20.243874
------------------------------------
-------- Channel 8 Stats-----------
Total Reads Serviced :          38813  
Total Writes Serviced :         874    
Average Read Latency :          67.77523
Average Read Queue Latency :    35.77523
Average Write Latency :         267.37757
Average Write Queue Latency :   233.37757
Read Page Hit Rate :            0.70451
Write Page Hit Rate :           0.58467
Max write queue length:         13
Max read queue length:          42
Average read queue length:      0.877504
Average column reads per ACT:   3.384166
Single column reads:            6002
Single columng reads(%):        15.463892
------------------------------------
-------- Channel 9 Stats-----------
Total Reads Serviced :          20431  
Total Writes Serviced :         1054   
Average Read Latency :          63.94660
Average Read Queue Latency :    31.94660
Average Write Latency :         93.64326
Average Write Queue Latency :   59.64326
Read Page Hit Rate :            0.70927
Write Page Hit Rate :           0.87666
Max write queue length:         8
Max read queue length:          22
Average read queue length:      0.413825
Average column reads per ACT:   3.439562
Single column reads:            3048
Single columng reads(%):        14.918507
------------------------------------
-------- Channel 10 Stats-----------
Total Reads Serviced :          28317  
Total Writes Serviced :         970    
Average Read Latency :          69.22594
Average Read Queue Latency :    37.22594
Average Write Latency :         125.50000
Average Write Queue Latency :   91.50000
Read Page Hit Rate :            0.65219
Write Page Hit Rate :           0.80515
Max write queue length:         16
Max read queue length:          29
Average read queue length:      0.665460
Average column reads per ACT:   2.875114
Single column reads:            5209
Single columng reads(%):        18.395311
------------------------------------
-------- Channel 11 Stats-----------
Total Reads Serviced :          54586  
Total Writes Serviced :         805    
Average Read Latency :          64.48919
Average Read Queue Latency :    32.48919
Average Write Latency :         441.09689
Average Write Queue Latency :   407.09689
Read Page Hit Rate :            0.72412
Write Page Hit Rate :           0.70435
Max write queue length:         15
Max read queue length:          58
Average read queue length:      1.123835
Average column reads per ACT:   3.624809
Single column reads:            8200
Single columng reads(%):        15.022167
------------------------------------
-------- Channel 12 Stats-----------
Total Reads Serviced :          44083  
Total Writes Serviced :         799    
Average Read Latency :          74.63333
Average Read Queue Latency :    42.63333
Average Write Latency :         625.03129
Average Write Queue Latency :   591.03129
Read Page Hit Rate :            0.61189
Write Page Hit Rate :           0.71214
Max write queue length:         25
Max read queue length:          39
Average read queue length:      1.182513
Average column reads per ACT:   2.576597
Single column reads:            9508
Single columng reads(%):        21.568405
------------------------------------
-------- Channel 13 Stats-----------
Total Reads Serviced :          36204  
Total Writes Serviced :         830    
Average Read Latency :          70.03842
Average Read Queue Latency :    38.03842
Average Write Latency :         425.41084
Average Write Queue Latency :   391.41084
Read Page Hit Rate :            0.63441
Write Page Hit Rate :           0.68675
Max write queue length:         18
Max read queue length:          40
Average read queue length:      0.868891
Average column reads per ACT:   2.735267
Single column reads:            7225
Single columng reads(%):        19.956358
------------------------------------
-------- Channel 14 Stats-----------
Total Reads Serviced :          29241  
Total Writes Serviced :         865    
Average Read Latency :          64.58008
Average Read Queue Latency :    32.58008
Average Write Latency :         210.07283
Average Write Queue Latency :   176.07283
Read Page Hit Rate :            0.69775
Write Page Hit Rate :           0.66127
Max write queue length:         13
Max read queue length:          32
Average read queue length:      0.603657
Average column reads per ACT:   3.308554
Single column reads:            4623
Single columng reads(%):        15.809994
------------------------------------
-------- Channel 15 Stats-----------
Total Reads Serviced :          21535  
Total Writes Serviced :         973    
Average Read Latency :          62.47216
Average Read Queue Latency :    30.47216
Average Write Latency :         111.84687
Average Write Queue Latency :   77.84687
Read Page Hit Rate :            0.70327
Write Page Hit Rate :           0.71429
Max write queue length:         9
Max read queue length:          20
Average read queue length:      0.416666
Average column reads per ACT:   3.370110
Single column reads:            3442
Single columng reads(%):        15.983284
------------------------------------

#-------------------------------------------------------------------------------------------------
Total memory system power = 47.692162 W
	 <== Total time:        952.1 s ==>
