
#include "Image.h"

Image::Image(const int &xres, const int &yres, const int &s_fb) : xres(xres), yres(yres), start_fb(s_fb) {}

// 'i' is the major index in to the image array (row)
// 'j' is the minor index in to the image array (col)
// Framebuffer holds a full float for each color channel (r, g, b),
// instead of the usual char
void Image::Set(const int &i, const int &j, const Color& c) {
	Color col = c.Clamp();
  // clamp the color to [0..1]
  //col.cl
  //col.r = c.R() < 0.f? 0.f: c.R() >= 1.f? 1.f : c.R();
  //col.g = c.G() < 0.f? 0.f: c.G() >= 1.f? 1.f : c.G();
  //col.b = c.B() < 0.f? 0.f: c.B() >= 1.f? 1.f : c.B();
  
  // Translate (i, j) 2-D coordinates in to array address,
  //   and store the rgb values to main memory
  // "storef" intrinsic stores a float to main memory
  // storef(value_to_write, location to write, offset (immediate))
  storef(col.R(), start_fb + ( i * xres + j ) * 3, 0 );
  storef(col.G(), start_fb + ( i * xres + j ) * 3, 1 );
  storef(col.B(), start_fb + ( i * xres + j ) * 3, 2 );
}