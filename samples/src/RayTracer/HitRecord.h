#ifndef _HitRecord_H
#define _HitRecord_H

#include "trax.hpp"
#include "syVector.h"

class HitRecord {
public:
	float t;
	int id_mat;
	int id_obj;
//	syVector p, N;
//	bool front;

	HitRecord();

	void Init();
	bool Hit() const;

	HitRecord& operator=(const HitRecord &_hit);
};

class HitDist {
public:
	float t;

	void Init();
	HitDist();
};

#endif