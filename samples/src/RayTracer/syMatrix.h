#ifndef _syMatrix_H
#define _syMatrix_H

#include "trax.hpp"
#include "syVector.h"

class syMatrix {
public:
	float data[9];

public:
	syMatrix();
//	syMatrix(const syMatrix& m);
//	syMatrix(const float &r, const float &g, const float &b);

	void Zero();
	void Identity();

	void GetInverse( syMatrix &inverse ) const;
	void Invert();
	// Assignment operators
	syMatrix&	operator *=	( const syMatrix & right);
	// Binary operators
	syVector operator * ( const syVector& p) const;
	// 
	syVector GetColumn( int col ) const;

private:
	int LUD( int *outvect, int& output );
	void LUBKS( const int *outvect, float *output );
};

#endif