#ifndef _syBox_H
#define _syBox_H

#include "trax.hpp"
#include "syVector.h"
#include "Ray.h"
#include "HitRecord.h"

class syBox {
public:
//	syVector min, max;
	syVector bound[2];

public:
	syBox();
	syBox(const syVector &_min, const syVector &_max);

	void Set(const syVector &_min, const syVector &_max);
	void Set();

	void LoadFromMemory(const int &_addr);

	// in world space, no transformations
	bool IntersectsW( const Ray &ray, HitDist &t_box_near ) const;
};

#endif