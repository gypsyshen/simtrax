	.file	"rt.bc"
	.text
	.globl	_ZN5ImageC2Eiii
	.align	2
	.type	_ZN5ImageC2Eiii,@function
	.ent	_ZN5ImageC2Eiii         # @_ZN5ImageC2Eiii
_ZN5ImageC2Eiii:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r6, r5, 0
	SWI       r7, r5, 4
	rtsd      r15, 8
	SWI       r8, r5, 8
	.end	_ZN5ImageC2Eiii
$tmp0:
	.size	_ZN5ImageC2Eiii, ($tmp0)-_ZN5ImageC2Eiii

	.globl	_Z8getOrthoRK6Vector
	.align	2
	.type	_Z8getOrthoRK6Vector,@function
	.ent	_Z8getOrthoRK6Vector    # @_Z8getOrthoRK6Vector
_Z8getOrthoRK6Vector:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	ORI       r4, r0, 0
	FPGE   r7, r3, r4
	FPUN   r4, r3, r4
	BITOR        r7, r4, r7
	bneid     r7, ($BB1_2)
	ADDI      r4, r0, 1
# BB#1:                                 # %entry
	ADDI      r4, r0, 0
$BB1_2:                                 # %entry
	bneid     r4, ($BB1_4)
	ADD      r7, r3, r0
# BB#3:                                 # %cond.true.i55
	FPNEG      r7, r3
$BB1_4:                                 # %_ZL4Fabsf.exit57
	LWI        r4, r6, 4
	ORI       r8, r0, 0
	FPGE   r9, r4, r8
	FPUN   r8, r4, r8
	BITOR        r8, r8, r9
	bneid     r8, ($BB1_6)
	ADDI      r9, r0, 1
# BB#5:                                 # %_ZL4Fabsf.exit57
	ADDI      r9, r0, 0
$BB1_6:                                 # %_ZL4Fabsf.exit57
	bneid     r9, ($BB1_8)
	ADD      r8, r4, r0
# BB#7:                                 # %cond.true.i50
	FPNEG      r8, r4
$BB1_8:                                 # %_ZL4Fabsf.exit52
	LWI        r6, r6, 8
	ORI       r9, r0, 0
	FPGE   r10, r6, r9
	FPUN   r9, r6, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB1_10)
	ADDI      r10, r0, 1
# BB#9:                                 # %_ZL4Fabsf.exit52
	ADDI      r10, r0, 0
$BB1_10:                                # %_ZL4Fabsf.exit52
	bneid     r10, ($BB1_12)
	ADD      r9, r6, r0
# BB#11:                                # %cond.true.i
	FPNEG      r9, r6
$BB1_12:                                # %_ZL4Fabsf.exit
	FPGE   r10, r7, r8
	FPUN   r11, r7, r8
	BITOR        r10, r11, r10
	bneid     r10, ($BB1_14)
	ADDI      r12, r0, 1
# BB#13:                                # %_ZL4Fabsf.exit
	ADDI      r12, r0, 0
$BB1_14:                                # %_ZL4Fabsf.exit
	ORI       r11, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r12, ($BB1_18)
	NOP    
# BB#15:                                # %_ZL4Fabsf.exit
	FPLT   r7, r7, r9
	bneid     r7, ($BB1_17)
	ADDI      r12, r0, 1
# BB#16:                                # %_ZL4Fabsf.exit
	ADDI      r12, r0, 0
$BB1_17:                                # %_ZL4Fabsf.exit
	bneid     r12, ($BB1_22)
	ADD      r7, r10, r0
$BB1_18:                                # %if.else
	FPLT   r7, r8, r9
	bneid     r7, ($BB1_20)
	ADDI      r8, r0, 1
# BB#19:                                # %if.else
	ADDI      r8, r0, 0
$BB1_20:                                # %if.else
	ORI       r7, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r8, ($BB1_22)
	ADD      r11, r10, r0
# BB#21:                                # %if.else18
	ORI       r7, r0, 0
	ORI       r10, r0, 1065353216
	ADD      r11, r7, r0
$BB1_22:                                # %if.end24
	FPMUL      r8, r6, r7
	FPMUL      r9, r4, r10
	FPRSUB     r8, r8, r9
	SWI        r8, r5, 0
	FPMUL      r8, r3, r10
	FPMUL      r6, r6, r11
	FPRSUB     r6, r8, r6
	SWI        r6, r5, 4
	FPMUL      r4, r4, r11
	FPMUL      r3, r3, r7
	FPRSUB     r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_Z8getOrthoRK6Vector
$tmp1:
	.size	_Z8getOrthoRK6Vector, ($tmp1)-_Z8getOrthoRK6Vector

	.globl	_Z16randomReflectionRK6Vector
	.align	2
	.type	_Z16randomReflectionRK6Vector,@function
	.ent	_Z16randomReflectionRK6Vector # @_Z16randomReflectionRK6Vector
_Z16randomReflectionRK6Vector:
	.frame	r1,12,r15
	.mask	0x700000
# BB#0:                                 # %entry
	ADDI      r1, r1, -12
	SWI       r20, r1, 8
	SWI       r21, r1, 4
	SWI       r22, r1, 0
	SWI       r0, r5, 0
	SWI       r0, r5, 4
	SWI       r0, r5, 8
$BB2_1:                                 # %do.body
                                        # =>This Inner Loop Header: Depth=1
	RAND      r4
	RAND      r3
	FPADD      r3, r3, r3
	ORI       r7, r0, -1082130432
	FPADD      r3, r3, r7
	FPADD      r4, r4, r4
	FPADD      r4, r4, r7
	FPMUL      r7, r4, r4
	FPMUL      r8, r3, r3
	FPADD      r9, r7, r8
	ORI       r10, r0, 1065353216
	FPGE   r10, r9, r10
	bneid     r10, ($BB2_3)
	ADDI      r9, r0, 1
# BB#2:                                 # %do.body
                                        #   in Loop: Header=BB2_1 Depth=1
	ADDI      r9, r0, 0
$BB2_3:                                 # %do.body
                                        #   in Loop: Header=BB2_1 Depth=1
	bneid     r9, ($BB2_1)
	NOP    
# BB#4:                                 # %do.end
	ORI       r9, r0, 1065353216
	FPRSUB     r7, r7, r9
	FPRSUB     r7, r8, r7
	FPINVSQRT r10, r7
	LWI        r7, r6, 0
	ORI       r8, r0, 0
	FPGE   r11, r7, r8
	FPUN   r8, r7, r8
	BITOR        r11, r8, r11
	bneid     r11, ($BB2_6)
	ADDI      r8, r0, 1
# BB#5:                                 # %do.end
	ADDI      r8, r0, 0
$BB2_6:                                 # %do.end
	bneid     r8, ($BB2_8)
	ADD      r11, r7, r0
# BB#7:                                 # %cond.true.i55.i
	FPNEG      r11, r7
$BB2_8:                                 # %_ZL4Fabsf.exit57.i
	LWI        r8, r6, 4
	ORI       r12, r0, 0
	FPGE   r20, r8, r12
	FPUN   r12, r8, r12
	BITOR        r12, r12, r20
	bneid     r12, ($BB2_10)
	ADDI      r20, r0, 1
# BB#9:                                 # %_ZL4Fabsf.exit57.i
	ADDI      r20, r0, 0
$BB2_10:                                # %_ZL4Fabsf.exit57.i
	bneid     r20, ($BB2_12)
	ADD      r12, r8, r0
# BB#11:                                # %cond.true.i50.i
	FPNEG      r12, r8
$BB2_12:                                # %_ZL4Fabsf.exit52.i
	LWI        r6, r6, 8
	ORI       r20, r0, 0
	FPGE   r21, r6, r20
	FPUN   r20, r6, r20
	BITOR        r20, r20, r21
	bneid     r20, ($BB2_14)
	ADDI      r21, r0, 1
# BB#13:                                # %_ZL4Fabsf.exit52.i
	ADDI      r21, r0, 0
$BB2_14:                                # %_ZL4Fabsf.exit52.i
	bneid     r21, ($BB2_16)
	ADD      r20, r6, r0
# BB#15:                                # %cond.true.i.i
	FPNEG      r20, r6
$BB2_16:                                # %_ZL4Fabsf.exit.i
	FPGE   r21, r11, r12
	FPUN   r22, r11, r12
	BITOR        r21, r22, r21
	bneid     r21, ($BB2_18)
	ADDI      r22, r0, 1
# BB#17:                                # %_ZL4Fabsf.exit.i
	ADDI      r22, r0, 0
$BB2_18:                                # %_ZL4Fabsf.exit.i
	FPDIV      r9, r9, r10
	ORI       r21, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r22, ($BB2_22)
	NOP    
# BB#19:                                # %_ZL4Fabsf.exit.i
	FPLT   r22, r11, r20
	bneid     r22, ($BB2_21)
	ADDI      r11, r0, 1
# BB#20:                                # %_ZL4Fabsf.exit.i
	ADDI      r11, r0, 0
$BB2_21:                                # %_ZL4Fabsf.exit.i
	bneid     r11, ($BB2_26)
	ADD      r22, r10, r0
$BB2_22:                                # %if.else.i
	FPLT   r10, r12, r20
	bneid     r10, ($BB2_24)
	ADDI      r11, r0, 1
# BB#23:                                # %if.else.i
	ADDI      r11, r0, 0
$BB2_24:                                # %if.else.i
	ORI       r22, r0, 1065353216
	ORI       r10, r0, 0
	bneid     r11, ($BB2_26)
	ADD      r21, r10, r0
# BB#25:                                # %if.else18.i
	ORI       r22, r0, 0
	ORI       r10, r0, 1065353216
	ADD      r21, r22, r0
$BB2_26:                                # %_Z8getOrthoRK6Vector.exit
	FPMUL      r11, r8, r21
	FPMUL      r12, r7, r22
	FPRSUB     r11, r11, r12
	FPMUL      r12, r6, r21
	FPMUL      r20, r7, r10
	FPRSUB     r12, r20, r12
	FPMUL      r20, r6, r12
	FPMUL      r21, r8, r11
	FPRSUB     r20, r20, r21
	FPMUL      r21, r6, r22
	FPMUL      r10, r8, r10
	FPRSUB     r21, r21, r10
	FPMUL      r10, r7, r11
	FPMUL      r22, r6, r21
	FPRSUB     r22, r10, r22
	FPMUL      r10, r20, r3
	FPMUL      r20, r21, r4
	FPADD      r10, r20, r10
	FPMUL      r20, r7, r9
	FPADD      r10, r10, r20
	FPMUL      r20, r22, r3
	FPMUL      r22, r12, r4
	FPADD      r20, r22, r20
	FPMUL      r22, r8, r9
	FPADD      r20, r20, r22
	FPMUL      r8, r8, r21
	FPMUL      r12, r7, r12
	FPMUL      r7, r20, r20
	FPMUL      r21, r10, r10
	FPADD      r7, r21, r7
	FPRSUB     r8, r8, r12
	FPMUL      r3, r8, r3
	FPMUL      r4, r11, r4
	FPADD      r3, r4, r3
	FPMUL      r4, r6, r9
	FPADD      r3, r3, r4
	FPMUL      r4, r3, r3
	FPADD      r4, r7, r4
	FPINVSQRT r4, r4
	ORI       r6, r0, 1065353216
	FPDIV      r4, r6, r4
	FPDIV      r4, r6, r4
	FPMUL      r6, r10, r4
	SWI        r6, r5, 0
	FPMUL      r6, r20, r4
	SWI        r6, r5, 4
	FPMUL      r3, r3, r4
	SWI        r3, r5, 8
	LWI       r22, r1, 0
	LWI       r21, r1, 4
	LWI       r20, r1, 8
	rtsd      r15, 8
	ADDI      r1, r1, 12
	.end	_Z16randomReflectionRK6Vector
$tmp2:
	.size	_Z16randomReflectionRK6Vector, ($tmp2)-_Z16randomReflectionRK6Vector

	.globl	_Z20randomReflectionConeRK6Vectorf
	.align	2
	.type	_Z20randomReflectionConeRK6Vectorf,@function
	.ent	_Z20randomReflectionConeRK6Vectorf # @_Z20randomReflectionConeRK6Vectorf
_Z20randomReflectionConeRK6Vectorf:
	.frame	r1,16,r15
	.mask	0xf00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -16
	SWI       r20, r1, 12
	SWI       r21, r1, 8
	SWI       r22, r1, 4
	SWI       r23, r1, 0
	SWI       r0, r5, 0
	SWI       r0, r5, 4
	SWI       r0, r5, 8
$BB3_1:                                 # %do.body
                                        # =>This Inner Loop Header: Depth=1
	RAND      r3
	RAND      r4
	FPADD      r8, r4, r4
	ORI       r4, r0, -1082130432
	FPADD      r8, r8, r4
	FPADD      r3, r3, r3
	FPADD      r9, r3, r4
	FPMUL      r3, r9, r9
	FPMUL      r4, r8, r8
	FPADD      r10, r3, r4
	ORI       r11, r0, 1065353216
	FPGE   r11, r10, r11
	bneid     r11, ($BB3_3)
	ADDI      r10, r0, 1
# BB#2:                                 # %do.body
                                        #   in Loop: Header=BB3_1 Depth=1
	ADDI      r10, r0, 0
$BB3_3:                                 # %do.body
                                        #   in Loop: Header=BB3_1 Depth=1
	bneid     r10, ($BB3_1)
	NOP    
# BB#4:                                 # %do.end
	ORI       r11, r0, 1065353216
	FPRSUB     r3, r3, r11
	FPRSUB     r3, r4, r3
	FPINVSQRT r12, r3
	LWI        r3, r6, 0
	ORI       r4, r0, 0
	FPGE   r10, r3, r4
	FPUN   r4, r3, r4
	BITOR        r10, r4, r10
	bneid     r10, ($BB3_6)
	ADDI      r4, r0, 1
# BB#5:                                 # %do.end
	ADDI      r4, r0, 0
$BB3_6:                                 # %do.end
	bneid     r4, ($BB3_8)
	ADD      r20, r3, r0
# BB#7:                                 # %cond.true.i55.i
	FPNEG      r20, r3
$BB3_8:                                 # %_ZL4Fabsf.exit57.i
	LWI        r4, r6, 4
	ORI       r10, r0, 0
	FPGE   r21, r4, r10
	FPUN   r10, r4, r10
	BITOR        r21, r10, r21
	bneid     r21, ($BB3_10)
	ADDI      r10, r0, 1
# BB#9:                                 # %_ZL4Fabsf.exit57.i
	ADDI      r10, r0, 0
$BB3_10:                                # %_ZL4Fabsf.exit57.i
	bneid     r10, ($BB3_12)
	ADD      r21, r4, r0
# BB#11:                                # %cond.true.i50.i
	FPNEG      r21, r4
$BB3_12:                                # %_ZL4Fabsf.exit52.i
	LWI        r6, r6, 8
	ORI       r10, r0, 0
	FPGE   r22, r6, r10
	FPUN   r10, r6, r10
	BITOR        r22, r10, r22
	bneid     r22, ($BB3_14)
	ADDI      r10, r0, 1
# BB#13:                                # %_ZL4Fabsf.exit52.i
	ADDI      r10, r0, 0
$BB3_14:                                # %_ZL4Fabsf.exit52.i
	bneid     r10, ($BB3_16)
	ADD      r22, r6, r0
# BB#15:                                # %cond.true.i.i
	FPNEG      r22, r6
$BB3_16:                                # %_ZL4Fabsf.exit.i
	FPGE   r10, r20, r21
	FPUN   r23, r20, r21
	BITOR        r10, r23, r10
	bneid     r10, ($BB3_18)
	ADDI      r23, r0, 1
# BB#17:                                # %_ZL4Fabsf.exit.i
	ADDI      r23, r0, 0
$BB3_18:                                # %_ZL4Fabsf.exit.i
	FPMUL      r10, r8, r7
	FPMUL      r8, r9, r7
	FPDIV      r7, r11, r12
	ORI       r12, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r23, ($BB3_22)
	NOP    
# BB#19:                                # %_ZL4Fabsf.exit.i
	FPLT   r20, r20, r22
	bneid     r20, ($BB3_21)
	ADDI      r11, r0, 1
# BB#20:                                # %_ZL4Fabsf.exit.i
	ADDI      r11, r0, 0
$BB3_21:                                # %_ZL4Fabsf.exit.i
	bneid     r11, ($BB3_26)
	ADD      r20, r9, r0
$BB3_22:                                # %if.else.i
	FPLT   r9, r21, r22
	bneid     r9, ($BB3_24)
	ADDI      r11, r0, 1
# BB#23:                                # %if.else.i
	ADDI      r11, r0, 0
$BB3_24:                                # %if.else.i
	ORI       r20, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r11, ($BB3_26)
	ADD      r12, r9, r0
# BB#25:                                # %if.else18.i
	ORI       r20, r0, 0
	ORI       r9, r0, 1065353216
	ADD      r12, r20, r0
$BB3_26:                                # %_Z8getOrthoRK6Vector.exit
	FPMUL      r11, r4, r12
	FPMUL      r21, r3, r20
	FPRSUB     r11, r11, r21
	FPMUL      r12, r6, r12
	FPMUL      r21, r3, r9
	FPRSUB     r12, r21, r12
	FPMUL      r21, r6, r12
	FPMUL      r22, r4, r11
	FPRSUB     r22, r21, r22
	FPMUL      r20, r6, r20
	FPMUL      r9, r4, r9
	FPRSUB     r21, r20, r9
	FPMUL      r9, r3, r11
	FPMUL      r20, r6, r21
	FPRSUB     r20, r9, r20
	FPMUL      r9, r22, r10
	FPMUL      r22, r21, r8
	FPADD      r9, r22, r9
	FPMUL      r22, r3, r7
	FPADD      r9, r9, r22
	FPMUL      r20, r20, r10
	FPMUL      r22, r12, r8
	FPADD      r20, r22, r20
	FPMUL      r22, r4, r7
	FPADD      r20, r20, r22
	FPMUL      r21, r4, r21
	FPMUL      r3, r3, r12
	FPMUL      r4, r20, r20
	FPMUL      r12, r9, r9
	FPADD      r4, r12, r4
	FPRSUB     r3, r21, r3
	FPMUL      r3, r3, r10
	FPMUL      r8, r11, r8
	FPADD      r3, r8, r3
	FPMUL      r6, r6, r7
	FPADD      r3, r3, r6
	FPMUL      r6, r3, r3
	FPADD      r4, r4, r6
	FPINVSQRT r4, r4
	ORI       r6, r0, 1065353216
	FPDIV      r4, r6, r4
	FPDIV      r4, r6, r4
	FPMUL      r6, r9, r4
	SWI        r6, r5, 0
	FPMUL      r6, r20, r4
	SWI        r6, r5, 4
	FPMUL      r3, r3, r4
	SWI        r3, r5, 8
	LWI       r23, r1, 0
	LWI       r22, r1, 4
	LWI       r21, r1, 8
	LWI       r20, r1, 12
	rtsd      r15, 8
	ADDI      r1, r1, 16
	.end	_Z20randomReflectionConeRK6Vectorf
$tmp3:
	.size	_Z20randomReflectionConeRK6Vectorf, ($tmp3)-_Z20randomReflectionConeRK6Vectorf

	.globl	_ZN6SphereC2ERK6Vectorfii
	.align	2
	.type	_ZN6SphereC2ERK6Vectorfii,@function
	.ent	_ZN6SphereC2ERK6Vectorfii # @_ZN6SphereC2ERK6Vectorfii
_ZN6SphereC2ERK6Vectorfii:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r9, r5, 0
	SWI       r8, r5, 4
	LWI        r3, r6, 0
	SWI        r3, r5, 8
	LWI        r3, r6, 4
	SWI        r3, r5, 12
	LWI        r3, r6, 8
	SWI        r3, r5, 16
	rtsd      r15, 8
	SWI        r7, r5, 20
	.end	_ZN6SphereC2ERK6Vectorfii
$tmp4:
	.size	_ZN6SphereC2ERK6Vectorfii, ($tmp4)-_ZN6SphereC2ERK6Vectorfii

	.globl	_ZN6SphereC2Ev
	.align	2
	.type	_ZN6SphereC2Ev,@function
	.ent	_ZN6SphereC2Ev          # @_ZN6SphereC2Ev
_ZN6SphereC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	NOP    
	.end	_ZN6SphereC2Ev
$tmp5:
	.size	_ZN6SphereC2Ev, ($tmp5)-_ZN6SphereC2Ev

	.globl	_ZN5LightC2Ev
	.align	2
	.type	_ZN5LightC2Ev,@function
	.ent	_ZN5LightC2Ev           # @_ZN5LightC2Ev
_ZN5LightC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 12
	SWI       r3, r5, 16
	SWI       r3, r5, 20
	SWI       r3, r5, 24
	rtsd      r15, 8
	SWI       r0, r5, 28
	.end	_ZN5LightC2Ev
$tmp6:
	.size	_ZN5LightC2Ev, ($tmp6)-_ZN5LightC2Ev

	.globl	_ZN5LightC2ERK6VectorRK5Colori
	.align	2
	.type	_ZN5LightC2ERK6VectorRK5Colori,@function
	.ent	_ZN5LightC2ERK6VectorRK5Colori # @_ZN5LightC2ERK6VectorRK5Colori
_ZN5LightC2ERK6VectorRK5Colori:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r9, r6, 4
	SWI        r9, r5, 4
	LWI        r4, r6, 8
	SWI        r4, r5, 8
	LWI        r6, r7, 0
	SWI        r6, r5, 12
	LWI        r6, r7, 4
	SWI        r6, r5, 16
	LWI        r6, r7, 8
	SWI        r6, r5, 20
	LWI        r6, r7, 12
	SWI        r6, r5, 24
	SWI       r8, r5, 28
	ADDI      r6, r0, 1
	CMP       r6, r6, r8
	bneid     r6, ($BB7_2)
	NOP    
# BB#1:                                 # %if.then
	FPMUL      r6, r9, r9
	FPMUL      r3, r3, r3
	FPADD      r3, r3, r6
	FPMUL      r4, r4, r4
	FPADD      r3, r3, r4
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	FPDIV      r3, r4, r3
	LWI        r4, r5, 0
	FPMUL      r4, r4, r3
	SWI        r4, r5, 0
	LWI        r4, r5, 4
	FPMUL      r4, r4, r3
	SWI        r4, r5, 4
	LWI        r4, r5, 8
	FPMUL      r3, r4, r3
	SWI        r3, r5, 8
$BB7_2:                                 # %if.end
	rtsd      r15, 8
	NOP    
	.end	_ZN5LightC2ERK6VectorRK5Colori
$tmp7:
	.size	_ZN5LightC2ERK6VectorRK5Colori, ($tmp7)-_ZN5LightC2ERK6VectorRK5Colori

	.globl	_ZN5LightC2ERK6VectorRK5Colorf
	.align	2
	.type	_ZN5LightC2ERK6VectorRK5Colorf,@function
	.ent	_ZN5LightC2ERK6VectorRK5Colorf # @_ZN5LightC2ERK6VectorRK5Colorf
_ZN5LightC2ERK6VectorRK5Colorf:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r4, r6, 0
	SWI        r4, r5, 0
	LWI        r9, r6, 4
	SWI        r9, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	LWI        r6, r7, 0
	SWI        r6, r5, 12
	LWI        r6, r7, 4
	SWI        r6, r5, 16
	FPMUL      r4, r4, r4
	FPMUL      r6, r9, r9
	FPADD      r4, r4, r6
	LWI        r6, r7, 8
	SWI        r6, r5, 20
	FPMUL      r3, r3, r3
	FPADD      r3, r4, r3
	LWI        r4, r7, 12
	SWI        r4, r5, 24
	ADDI      r4, r0, 2
	SWI       r4, r5, 28
	SWI        r8, r5, 32
	FPINVSQRT r3, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	FPDIV      r3, r4, r3
	LWI        r4, r5, 0
	FPMUL      r4, r4, r3
	SWI        r4, r5, 0
	LWI        r4, r5, 4
	FPMUL      r4, r4, r3
	SWI        r4, r5, 4
	LWI        r4, r5, 8
	FPMUL      r3, r4, r3
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5LightC2ERK6VectorRK5Colorf
$tmp8:
	.size	_ZN5LightC2ERK6VectorRK5Colorf, ($tmp8)-_ZN5LightC2ERK6VectorRK5Colorf

	.globl	_ZNK5Light8getLightER5ColorR6VectorRKS2_
	.align	2
	.type	_ZNK5Light8getLightER5ColorR6VectorRKS2_,@function
	.ent	_ZNK5Light8getLightER5ColorR6VectorRKS2_ # @_ZNK5Light8getLightER5ColorR6VectorRKS2_
_ZNK5Light8getLightER5ColorR6VectorRKS2_:
	.frame	r1,16,r15
	.mask	0xf00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -16
	SWI       r20, r1, 12
	SWI       r21, r1, 8
	SWI       r22, r1, 4
	SWI       r23, r1, 0
	LWI       r4, r5, 28
	ADDI      r3, r0, 2
	CMP       r9, r3, r4
	ORI       r3, r0, -1082130432
	bneid     r9, ($BB9_1)
	NOP    
# BB#4:                                 # %if.then6
	LWI        r3, r5, 12
	SWI        r3, r6, 0
	LWI        r3, r5, 16
	SWI        r3, r6, 4
	LWI        r3, r5, 20
	SWI        r3, r6, 8
	LWI        r3, r5, 24
	SWI        r3, r6, 12
	LWI        r6, r5, 32
$BB9_5:                                 # %do.body.i
                                        # =>This Inner Loop Header: Depth=1
	RAND      r3
	RAND      r4
	FPADD      r8, r4, r4
	ORI       r4, r0, -1082130432
	FPADD      r8, r8, r4
	FPADD      r3, r3, r3
	FPADD      r9, r3, r4
	FPMUL      r3, r9, r9
	FPMUL      r4, r8, r8
	FPADD      r10, r3, r4
	ORI       r11, r0, 1065353216
	FPGE   r11, r10, r11
	bneid     r11, ($BB9_7)
	ADDI      r10, r0, 1
# BB#6:                                 # %do.body.i
                                        #   in Loop: Header=BB9_5 Depth=1
	ADDI      r10, r0, 0
$BB9_7:                                 # %do.body.i
                                        #   in Loop: Header=BB9_5 Depth=1
	bneid     r10, ($BB9_5)
	NOP    
# BB#8:                                 # %do.end.i
	ORI       r11, r0, 1065353216
	FPRSUB     r3, r3, r11
	FPRSUB     r3, r4, r3
	FPINVSQRT r12, r3
	LWI        r3, r5, 0
	ORI       r4, r0, 0
	FPGE   r10, r3, r4
	FPUN   r4, r3, r4
	BITOR        r10, r4, r10
	bneid     r10, ($BB9_10)
	ADDI      r4, r0, 1
# BB#9:                                 # %do.end.i
	ADDI      r4, r0, 0
$BB9_10:                                # %do.end.i
	bneid     r4, ($BB9_12)
	ADD      r20, r3, r0
# BB#11:                                # %cond.true.i55.i.i
	FPNEG      r20, r3
$BB9_12:                                # %_ZL4Fabsf.exit57.i.i
	LWI        r4, r5, 4
	ORI       r10, r0, 0
	FPGE   r21, r4, r10
	FPUN   r10, r4, r10
	BITOR        r21, r10, r21
	bneid     r21, ($BB9_14)
	ADDI      r10, r0, 1
# BB#13:                                # %_ZL4Fabsf.exit57.i.i
	ADDI      r10, r0, 0
$BB9_14:                                # %_ZL4Fabsf.exit57.i.i
	bneid     r10, ($BB9_16)
	ADD      r21, r4, r0
# BB#15:                                # %cond.true.i50.i.i
	FPNEG      r21, r4
$BB9_16:                                # %_ZL4Fabsf.exit52.i.i
	LWI        r5, r5, 8
	ORI       r10, r0, 0
	FPGE   r22, r5, r10
	FPUN   r10, r5, r10
	BITOR        r22, r10, r22
	bneid     r22, ($BB9_18)
	ADDI      r10, r0, 1
# BB#17:                                # %_ZL4Fabsf.exit52.i.i
	ADDI      r10, r0, 0
$BB9_18:                                # %_ZL4Fabsf.exit52.i.i
	bneid     r10, ($BB9_20)
	ADD      r22, r5, r0
# BB#19:                                # %cond.true.i.i.i
	FPNEG      r22, r5
$BB9_20:                                # %_ZL4Fabsf.exit.i.i
	FPGE   r10, r20, r21
	FPUN   r23, r20, r21
	BITOR        r10, r23, r10
	bneid     r10, ($BB9_22)
	ADDI      r23, r0, 1
# BB#21:                                # %_ZL4Fabsf.exit.i.i
	ADDI      r23, r0, 0
$BB9_22:                                # %_ZL4Fabsf.exit.i.i
	FPMUL      r10, r8, r6
	FPMUL      r8, r9, r6
	FPDIV      r6, r11, r12
	ORI       r12, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r23, ($BB9_26)
	NOP    
# BB#23:                                # %_ZL4Fabsf.exit.i.i
	FPLT   r20, r20, r22
	bneid     r20, ($BB9_25)
	ADDI      r11, r0, 1
# BB#24:                                # %_ZL4Fabsf.exit.i.i
	ADDI      r11, r0, 0
$BB9_25:                                # %_ZL4Fabsf.exit.i.i
	bneid     r11, ($BB9_30)
	ADD      r20, r9, r0
$BB9_26:                                # %if.else.i.i
	FPLT   r9, r21, r22
	bneid     r9, ($BB9_28)
	ADDI      r11, r0, 1
# BB#27:                                # %if.else.i.i
	ADDI      r11, r0, 0
$BB9_28:                                # %if.else.i.i
	ORI       r20, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r11, ($BB9_30)
	ADD      r12, r9, r0
# BB#29:                                # %if.else18.i.i
	ORI       r20, r0, 0
	ORI       r9, r0, 1065353216
	ADD      r12, r20, r0
$BB9_30:                                # %_Z20randomReflectionConeRK6Vectorf.exit
	FPMUL      r11, r4, r12
	FPMUL      r21, r3, r20
	FPRSUB     r11, r11, r21
	FPMUL      r12, r5, r12
	FPMUL      r21, r3, r9
	FPRSUB     r12, r21, r12
	FPMUL      r21, r5, r12
	FPMUL      r22, r4, r11
	FPRSUB     r22, r21, r22
	FPMUL      r20, r5, r20
	FPMUL      r9, r4, r9
	FPRSUB     r21, r20, r9
	FPMUL      r9, r3, r11
	FPMUL      r20, r5, r21
	FPRSUB     r20, r9, r20
	FPMUL      r9, r22, r10
	FPMUL      r22, r21, r8
	FPADD      r9, r22, r9
	FPMUL      r22, r3, r6
	FPADD      r9, r9, r22
	FPMUL      r20, r20, r10
	FPMUL      r22, r12, r8
	FPADD      r20, r22, r20
	FPMUL      r22, r4, r6
	FPADD      r20, r20, r22
	FPMUL      r21, r4, r21
	FPMUL      r3, r3, r12
	FPMUL      r4, r20, r20
	FPMUL      r12, r9, r9
	FPADD      r4, r12, r4
	FPRSUB     r3, r21, r3
	FPMUL      r3, r3, r10
	FPMUL      r8, r11, r8
	FPADD      r3, r8, r3
	FPMUL      r5, r5, r6
	FPADD      r3, r3, r5
	FPMUL      r5, r3, r3
	FPADD      r4, r4, r5
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r5, r9, r4
	SWI        r5, r7, 0
	FPMUL      r5, r20, r4
	SWI        r5, r7, 4
	brid      ($BB9_32)
	FPMUL      r3, r3, r4
$BB9_1:                                 # %entry
	ADDI      r9, r0, 1
	CMP       r9, r9, r4
	bneid     r9, ($BB9_2)
	NOP    
# BB#31:                                # %if.then14
	LWI        r3, r5, 12
	SWI        r3, r6, 0
	LWI        r3, r5, 16
	SWI        r3, r6, 4
	LWI        r3, r5, 20
	SWI        r3, r6, 8
	LWI        r3, r5, 24
	SWI        r3, r6, 12
	LWI        r3, r5, 0
	SWI        r3, r7, 0
	LWI        r3, r5, 4
	SWI        r3, r7, 4
	LWI        r3, r5, 8
$BB9_32:                                # %if.then14
	SWI        r3, r7, 8
	ORI       r3, r0, 1203982336
$BB9_33:                                # %return
	LWI       r23, r1, 0
	LWI       r22, r1, 4
	LWI       r21, r1, 8
	LWI       r20, r1, 12
	rtsd      r15, 8
	ADDI      r1, r1, 16
$BB9_2:                                 # %entry
	bneid     r4, ($BB9_33)
	NOP    
# BB#3:                                 # %if.then
	LWI        r3, r5, 12
	SWI        r3, r6, 0
	LWI        r3, r5, 16
	SWI        r3, r6, 4
	LWI        r3, r5, 20
	SWI        r3, r6, 8
	LWI        r3, r5, 24
	SWI        r3, r6, 12
	LWI        r3, r8, 0
	LWI        r4, r5, 0
	FPRSUB     r4, r3, r4
	LWI        r3, r8, 4
	LWI        r6, r5, 4
	FPRSUB     r6, r3, r6
	FPMUL      r3, r6, r6
	FPMUL      r9, r4, r4
	FPADD      r3, r9, r3
	LWI        r8, r8, 8
	LWI        r5, r5, 8
	FPRSUB     r5, r8, r5
	FPMUL      r8, r5, r5
	FPADD      r3, r3, r8
	FPINVSQRT r3, r3
	ORI       r8, r0, 1065353216
	FPDIV      r3, r8, r3
	FPDIV      r8, r8, r3
	FPMUL      r4, r4, r8
	SWI        r4, r7, 0
	FPMUL      r4, r6, r8
	SWI        r4, r7, 4
	FPMUL      r4, r5, r8
	brid      ($BB9_33)
	SWI        r4, r7, 8
	.end	_ZNK5Light8getLightER5ColorR6VectorRKS2_
$tmp9:
	.size	_ZNK5Light8getLightER5ColorR6VectorRKS2_, ($tmp9)-_ZNK5Light8getLightER5ColorR6VectorRKS2_

	.globl	_Z15getTextureColorRK9HitRecordRKii
	.align	2
	.type	_Z15getTextureColorRK9HitRecordRKii,@function
	.ent	_Z15getTextureColorRK9HitRecordRKii # @_Z15getTextureColorRK9HitRecordRKii
_Z15getTextureColorRK9HitRecordRKii:
	.frame	r1,68,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -68
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	SWI       r5, r1, 72
	LWI       r3, r6, 12
	LOAD      r3, r3, 9
	MULI      r9, r3, 9
	LWI       r3, r7, 0
	ADD      r5, r3, r9
	LOAD      r3, r5, 0
	LOAD      r4, r5, 1
	LOAD      r5, r5, 2
	LWI       r5, r7, 0
	ADD      r5, r9, r5
	ADDI      r11, r5, 3
	LOAD      r10, r11, 0
	LOAD      r5, r11, 1
	LOAD      r11, r11, 2
	LWI       r7, r7, 0
	ADD      r7, r9, r7
	ADDI      r9, r7, 6
	LOAD      r12, r9, 0
	LOAD      r7, r9, 1
	LOAD      r9, r9, 2
	LWI        r9, r6, 4
	FPMUL      r11, r10, r9
	LWI        r10, r6, 0
	FPMUL      r3, r3, r10
	FPADD      r3, r3, r11
	ORI       r6, r0, 1065353216
	FPRSUB     r6, r10, r6
	FPRSUB     r11, r9, r6
	FPMUL      r6, r12, r11
	FPADD      r3, r3, r6
	INTCONV      r6, r3
	FPCONV       r6, r6
	ADDI      r12, r0, 1
	FPEQ   r20, r3, r6
	bneid     r20, ($BB10_2)
	NOP    
# BB#1:                                 # %entry
	ADDI      r12, r0, 0
$BB10_2:                                # %entry
	FPMUL      r5, r5, r9
	FPMUL      r4, r4, r10
	FPADD      r4, r4, r5
	FPMUL      r5, r7, r11
	FPADD      r10, r4, r5
	LOAD      r9, r8, 0
	LOAD      r7, r8, 1
	LOAD      r11, r8, 2
	bneid     r12, ($BB10_4)
	NOP    
# BB#3:                                 # %if.then
	FPRSUB     r3, r6, r3
$BB10_4:                                # %if.end
	INTCONV      r4, r10
	FPCONV       r4, r4
	FPEQ   r6, r10, r4
	bneid     r6, ($BB10_6)
	ADDI      r5, r0, 1
# BB#5:                                 # %if.end
	ADDI      r5, r0, 0
$BB10_6:                                # %if.end
	bneid     r5, ($BB10_8)
	NOP    
# BB#7:                                 # %if.then32
	FPRSUB     r10, r4, r10
$BB10_8:                                # %if.end36
	ORI       r4, r0, 0
	FPGE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB10_10)
	ADDI      r4, r0, 1
# BB#9:                                 # %if.end36
	ADDI      r4, r0, 0
$BB10_10:                               # %if.end36
	bneid     r4, ($BB10_12)
	NOP    
# BB#11:                                # %if.then38
	ORI       r4, r0, 1065353216
	FPADD      r3, r3, r4
$BB10_12:                               # %if.end41
	ORI       r4, r0, 0
	FPGE   r5, r10, r4
	FPUN   r4, r10, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB10_14)
	ADDI      r4, r0, 1
# BB#13:                                # %if.end41
	ADDI      r4, r0, 0
$BB10_14:                               # %if.end41
	bneid     r4, ($BB10_16)
	ADD      r21, r11, r0
# BB#15:                                # %if.then43
	ORI       r4, r0, 1065353216
	FPADD      r10, r10, r4
$BB10_16:                               # %if.end46
	FPCONV       r4, r7
	FPMUL      r11, r3, r4
	INTCONV      r3, r11
	CMP       r3, r7, r3
	ADDI      r6, r0, 0
	ADDI      r5, r0, 1
	beqid     r3, ($BB10_18)
	ADD      r12, r5, r0
# BB#17:                                # %if.end46
	ADD      r12, r6, r0
$BB10_18:                               # %if.end46
	ORI       r3, r0, 0
	bneid     r12, ($BB10_20)
	ADD      r4, r3, r0
# BB#19:                                # %if.end46
	ADD      r4, r11, r0
$BB10_20:                               # %if.end46
	FPGE   r11, r4, r3
	FPUN   r12, r4, r3
	BITOR        r20, r12, r11
	FPCONV       r11, r9
	FPMUL      r10, r10, r11
	INTCONV      r11, r10
	CMP       r12, r9, r11
	bneid     r20, ($BB10_22)
	ADD      r11, r5, r0
# BB#21:                                # %if.end46
	ADD      r11, r6, r0
$BB10_22:                               # %if.end46
	beqid     r12, ($BB10_24)
	NOP    
# BB#23:                                # %if.end46
	ADD      r5, r6, r0
$BB10_24:                               # %if.end46
	bneid     r5, ($BB10_26)
	ADD      r20, r21, r0
# BB#25:                                # %if.end46
	ADD      r3, r10, r0
$BB10_26:                               # %if.end46
	bneid     r11, ($BB10_31)
	NOP    
# BB#27:                                # %if.then.i
	FPNEG      r5, r4
	INTCONV      r6, r5
	RSUBI     r5, r6, 0
	FPCONV       r10, r5
	FPEQ   r11, r10, r4
	bneid     r11, ($BB10_29)
	ADDI      r10, r0, 1
# BB#28:                                # %if.then.i
	ADDI      r10, r0, 0
$BB10_29:                               # %if.then.i
	bneid     r10, ($BB10_32)
	NOP    
# BB#30:                                # %if.else.i
	ADDI      r5, r0, -1
	brid      ($BB10_32)
	BITXOR       r5, r6, r5
$BB10_31:                               # %if.else6.i
	INTCONV      r5, r4
$BB10_32:                               # %_ZL5Floorf.exit
	ORI       r6, r0, 0
	FPGE   r10, r3, r6
	FPUN   r6, r3, r6
	BITOR        r10, r6, r10
	bneid     r10, ($BB10_34)
	ADDI      r6, r0, 1
# BB#33:                                # %_ZL5Floorf.exit
	ADDI      r6, r0, 0
$BB10_34:                               # %_ZL5Floorf.exit
	DIV      r10, r7, r5
	MUL       r10, r10, r7
	RSUB     r5, r10, r5
	bsrai     r10, r5, 31
	BITAND       r10, r10, r7
	ADD      r30, r10, r5
	ADDI      r5, r30, 1
	DIV      r10, r7, r5
	MUL       r10, r10, r7
	bneid     r6, ($BB10_39)
	RSUB     r28, r10, r5
# BB#35:                                # %if.then.i285
	FPNEG      r5, r3
	INTCONV      r6, r5
	RSUBI     r5, r6, 0
	FPCONV       r10, r5
	FPEQ   r11, r10, r3
	bneid     r11, ($BB10_37)
	ADDI      r10, r0, 1
# BB#36:                                # %if.then.i285
	ADDI      r10, r0, 0
$BB10_37:                               # %if.then.i285
	bneid     r10, ($BB10_40)
	NOP    
# BB#38:                                # %if.else.i287
	ADDI      r5, r0, -1
	brid      ($BB10_40)
	BITXOR       r5, r6, r5
$BB10_39:                               # %if.else6.i289
	INTCONV      r5, r3
$BB10_40:                               # %_ZL5Floorf.exit291
	DIV      r6, r9, r5
	MUL       r6, r6, r9
	RSUB     r5, r6, r5
	bsrai     r6, r5, 31
	BITAND       r6, r6, r9
	ADD      r5, r6, r5
	FPCONV       r6, r5
	FPRSUB     r3, r6, r3
	SWI       r3, r1, 60
	FPCONV       r3, r30
	FPRSUB     r3, r3, r4
	SWI       r3, r1, 64
	MUL       r6, r5, r7
	ADDI      r3, r5, 1
	DIV      r4, r9, r3
	MUL       r4, r4, r9
	RSUB     r10, r4, r3
	ADD      r4, r6, r30
	ADDI      r3, r0, 4
	CMP       r3, r3, r20
	MUL       r5, r4, r20
	ORI       r9, r0, 1065353216
	ORI       r11, r0, 0
	ADDI      r4, r8, 3
	bneid     r3, ($BB10_41)
	ADD      r5, r5, r4
# BB#45:                                # %_Z26loadTextureColorFromMemoryii.exit144.thread186
	MUL       r7, r10, r7
	ADD      r3, r7, r30
	MUL       r3, r3, r20
	ADD      r8, r7, r28
	ADD      r6, r6, r28
	bslli     r6, r6, 2
	ADD      r7, r6, r4
	bslli     r8, r8, 2
	ADD      r6, r3, r4
	ADD      r3, r8, r4
	LOAD      r4, r5, 1
	LOAD      r10, r5, 2
	LOAD      r12, r5, 3
	LOAD      r8, r5, 0
	LOAD      r11, r7, 1
	LOAD      r20, r7, 2
	LOAD      r9, r7, 3
	LOAD      r5, r7, 0
	FPCONV       r7, r12
	FPCONV       r12, r10
	FPCONV       r10, r20
	FPCONV       r11, r11
	FPCONV       r20, r8
	FPCONV       r22, r4
	ORI       r4, r0, 1132396544
	FPDIV      r8, r12, r4
	SWI       r8, r1, 48
	FPDIV      r8, r7, r4
	LOAD      r7, r6, 1
	LOAD      r12, r6, 2
	LOAD      r21, r6, 3
	LOAD      r23, r6, 0
	FPCONV       r9, r9
	FPDIV      r22, r22, r4
	FPDIV      r6, r20, r4
	SWI       r6, r1, 52
	FPDIV      r11, r11, r4
	FPDIV      r6, r10, r4
	SWI       r6, r1, 56
	LOAD      r10, r3, 1
	LOAD      r6, r3, 2
	FPCONV       r6, r6
	FPCONV       r10, r10
	FPCONV       r23, r23
	FPCONV       r26, r21
	FPCONV       r25, r12
	FPDIV      r21, r9, r4
	LOAD      r9, r3, 3
	LOAD      r3, r3, 0
	FPCONV       r12, r9
	FPCONV       r7, r7
	FPCONV       r5, r5
	FPDIV      r9, r5, r4
	FPCONV       r5, r3
	FPDIV      r24, r7, r4
	FPDIV      r25, r25, r4
	FPDIV      r26, r26, r4
	FPDIV      r27, r23, r4
	FPDIV      r3, r10, r4
	FPDIV      r29, r6, r4
	FPDIV      r23, r12, r4
	brid      ($BB10_46)
	FPDIV      r31, r5, r4
$BB10_41:                               # %_ZL5Floorf.exit291
	ADDI      r3, r0, 3
	CMP       r3, r3, r20
	bneid     r3, ($BB10_42)
	NOP    
# BB#44:                                # %_Z26loadTextureColorFromMemoryii.exit144.thread179
	MUL       r3, r10, r7
	ADD      r7, r3, r30
	MUL       r7, r7, r20
	ADD      r3, r3, r28
	ADD      r6, r6, r28
	MULI      r6, r6, 3
	ADD      r10, r7, r4
	ADD      r8, r6, r4
	MULI      r3, r3, 3
	ADD      r6, r3, r4
	LOAD      r3, r5, 0
	LOAD      r4, r5, 1
	LOAD      r5, r5, 2
	LOAD      r7, r8, 0
	LOAD      r9, r8, 1
	LOAD      r8, r8, 2
	LOAD      r11, r10, 0
	LOAD      r12, r10, 1
	LOAD      r10, r10, 2
	FPCONV       r20, r5
	FPCONV       r21, r4
	FPCONV       r5, r10
	ORI       r4, r0, 1132396544
	FPDIV      r10, r21, r4
	SWI       r10, r1, 48
	LOAD      r10, r6, 0
	FPCONV       r10, r10
	FPCONV       r12, r12
	FPCONV       r23, r11
	FPCONV       r21, r8
	FPDIV      r8, r20, r4
	LOAD      r11, r6, 1
	LOAD      r20, r6, 2
	FPCONV       r6, r11
	FPCONV       r9, r9
	FPCONV       r7, r7
	FPCONV       r3, r3
	FPDIV      r22, r3, r4
	FPDIV      r11, r7, r4
	FPCONV       r7, r20
	FPDIV      r3, r9, r4
	SWI       r3, r1, 56
	FPDIV      r21, r21, r4
	FPDIV      r24, r23, r4
	FPDIV      r25, r12, r4
	FPDIV      r26, r5, r4
	FPDIV      r3, r10, r4
	FPDIV      r29, r6, r4
	FPDIV      r23, r7, r4
	ORI       r9, r0, 1065353216
	SWI       r9, r1, 52
	ADD      r27, r9, r0
	brid      ($BB10_46)
	ADD      r31, r9, r0
$BB10_42:                               # %_ZL5Floorf.exit291
	ADDI      r3, r0, 1
	CMP       r12, r3, r20
	SWI       r11, r1, 56
	ADD      r21, r11, r0
	SWI       r9, r1, 52
	ADD      r8, r11, r0
	SWI       r11, r1, 48
	ADD      r22, r11, r0
	ADD      r24, r11, r0
	ADD      r25, r11, r0
	ADD      r26, r11, r0
	ADD      r27, r9, r0
	ADD      r31, r9, r0
	ADD      r23, r11, r0
	ADD      r29, r11, r0
	bneid     r12, ($BB10_46)
	ADD      r3, r11, r0
# BB#43:                                # %_Z26loadTextureColorFromMemoryii.exit144.thread
	MUL       r7, r10, r7
	ADD      r3, r6, r28
	ADD      r6, r7, r28
	ADD      r7, r7, r30
	MUL       r7, r7, r20
	ADD      r7, r7, r4
	ADD      r8, r3, r4
	ADD      r3, r6, r4
	LOAD      r4, r5, 0
	LOAD      r5, r8, 0
	LOAD      r6, r7, 0
	FPCONV       r7, r4
	ORI       r4, r0, 1132396544
	FPDIV      r8, r7, r4
	FPCONV       r6, r6
	FPCONV       r5, r5
	FPDIV      r11, r5, r4
	FPDIV      r24, r6, r4
	LOAD      r3, r3, 0
	FPCONV       r3, r3
	FPDIV      r23, r3, r4
	ORI       r9, r0, 1065353216
	SWI       r11, r1, 56
	ADD      r21, r11, r0
	SWI       r9, r1, 52
	SWI       r8, r1, 48
	ADD      r22, r8, r0
	ADD      r25, r24, r0
	ADD      r26, r24, r0
	ADD      r27, r9, r0
	ADD      r31, r9, r0
	ADD      r29, r23, r0
	ADD      r3, r23, r0
$BB10_46:                               # %_Z26loadTextureColorFromMemoryii.exit54
	LWI       r28, r1, 64
	FPMUL      r4, r11, r28
	ORI       r7, r0, 1065353216
	LWI       r12, r1, 60
	FPRSUB     r5, r12, r7
	FPMUL      r6, r4, r5
	FPRSUB     r4, r28, r7
	FPMUL      r7, r22, r4
	FPMUL      r7, r7, r5
	FPADD      r6, r7, r6
	FPMUL      r7, r24, r4
	FPMUL      r7, r7, r12
	FPADD      r6, r6, r7
	FPMUL      r8, r8, r4
	FPMUL      r7, r21, r28
	FPMUL      r7, r7, r5
	FPMUL      r8, r8, r5
	LWI       r10, r1, 56
	FPMUL      r10, r10, r28
	FPMUL      r10, r10, r5
	LWI       r11, r1, 48
	FPMUL      r11, r11, r4
	FPMUL      r5, r11, r5
	FPADD      r5, r5, r10
	FPADD      r7, r8, r7
	FPMUL      r3, r3, r28
	FPMUL      r3, r3, r12
	FPADD      r3, r6, r3
	FPMUL      r6, r25, r4
	FPMUL      r4, r26, r4
	LWI       r8, r1, 72
	SWI        r3, r8, 0
	FPMUL      r3, r4, r12
	FPADD      r3, r7, r3
	FPMUL      r4, r6, r12
	FPADD      r4, r5, r4
	FPMUL      r5, r29, r28
	FPMUL      r5, r5, r12
	FPADD      r4, r4, r5
	FPMUL      r5, r23, r28
	FPMUL      r5, r5, r12
	SWI        r4, r8, 4
	FPADD      r3, r3, r5
	LWI       r4, r1, 52
	FPADD      r4, r4, r9
	FPADD      r4, r4, r27
	FPADD      r4, r4, r31
	SWI        r3, r8, 8
	ORI       r3, r0, 1048576000
	FPMUL      r3, r4, r3
	SWI        r3, r8, 12
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 68
	.end	_Z15getTextureColorRK9HitRecordRKii
$tmp10:
	.size	_Z15getTextureColorRK9HitRecordRKii, ($tmp10)-_Z15getTextureColorRK9HitRecordRKii

	.globl	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
	.align	2
	.type	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color,@function
	.ent	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color # @_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color:
	.frame	r1,260,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -260
	SWI       r20, r1, 48
	SWI       r21, r1, 44
	SWI       r22, r1, 40
	SWI       r23, r1, 36
	SWI       r24, r1, 32
	SWI       r25, r1, 28
	SWI       r26, r1, 24
	SWI       r27, r1, 20
	SWI       r28, r1, 16
	SWI       r29, r1, 12
	SWI       r30, r1, 8
	SWI       r31, r1, 4
	SWI       r10, r1, 284
	SWI       r0, r5, 0
	SWI       r0, r5, 4
	SWI       r0, r5, 8
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 12
	lbui      r3, r6, 16
	beqid     r3, ($BB11_91)
	NOP    
# BB#1:                                 # %if.then
	ADD      r21, r8, r0
	SWI       r5, r1, 228
	LWI        r3, r6, 8
	LWI        r4, r7, 12
	FPMUL      r4, r4, r3
	LWI        r5, r7, 0
	FPADD      r11, r5, r4
	LWI        r4, r7, 8
	LWI        r5, r7, 20
	LWI        r8, r7, 4
	LWI        r10, r7, 16
	SWI        r11, r9, 0
	FPMUL      r10, r10, r3
	FPADD      r8, r8, r10
	SWI        r8, r9, 4
	FPMUL      r3, r5, r3
	FPADD      r3, r4, r3
	SWI        r3, r9, 8
	LWI       r20, r6, 12
	SWI       r6, r1, 240
	LOAD      r4, r20, 0
	LOAD      r5, r20, 1
	LOAD      r3, r20, 2
	ADDI      r10, r20, 3
	LOAD      r8, r10, 0
	LOAD      r12, r10, 1
	LOAD      r11, r10, 2
	ADDI      r20, r20, 6
	LOAD      r10, r20, 0
	LOAD      r6, r20, 1
	FPRSUB     r5, r6, r5
	FPRSUB     r6, r6, r12
	LOAD      r20, r20, 2
	FPRSUB     r12, r20, r11
	FPRSUB     r20, r20, r3
	FPMUL      r3, r20, r6
	FPMUL      r11, r5, r12
	FPRSUB     r3, r3, r11
	FPRSUB     r11, r10, r8
	FPRSUB     r4, r10, r4
	FPMUL      r8, r4, r12
	FPMUL      r10, r20, r11
	FPRSUB     r8, r8, r10
	FPMUL      r10, r8, r8
	FPMUL      r12, r3, r3
	FPADD      r10, r12, r10
	FPMUL      r5, r5, r11
	FPMUL      r4, r4, r6
	FPRSUB     r4, r5, r4
	FPMUL      r5, r4, r4
	FPADD      r5, r10, r5
	FPINVSQRT r5, r5
	ORI       r6, r0, 1065353216
	FPDIV      r5, r6, r5
	FPDIV      r5, r6, r5
	FPMUL      r27, r8, r5
	LWI        r6, r7, 16
	FPMUL      r6, r27, r6
	FPMUL      r8, r3, r5
	LWI        r3, r7, 12
	FPMUL      r3, r8, r3
	FPADD      r3, r3, r6
	FPMUL      r26, r4, r5
	LWI        r4, r7, 20
	FPMUL      r4, r26, r4
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB11_3)
	ADDI      r3, r0, 1
# BB#2:                                 # %if.then
	ADDI      r3, r0, 0
$BB11_3:                                # %if.then
	bneid     r3, ($BB11_5)
	ADD      r10, r21, r0
# BB#4:                                 # %if.then11
	FPNEG      r26, r26
	FPNEG      r27, r27
	FPNEG      r8, r8
$BB11_5:                                # %if.end
	SWI       r8, r1, 224
	ORI       r5, r0, 981668463
	FPMUL      r3, r26, r5
	LWI        r4, r9, 8
	FPADD      r3, r4, r3
	FPMUL      r4, r27, r5
	LWI        r6, r9, 4
	FPADD      r4, r6, r4
	FPMUL      r5, r8, r5
	LWI        r6, r9, 0
	FPADD      r5, r6, r5
	SWI        r5, r9, 0
	SWI        r4, r9, 4
	SWI        r3, r9, 8
	ORI       r22, r0, -1082130432
	ORI       r29, r0, 1065353216
	LWI       r6, r10, 40
	ADDI      r7, r0, 2
	CMP       r7, r7, r6
	bneid     r7, ($BB11_6)
	NOP    
# BB#9:                                 # %if.then6.i
	LWI        r20, r10, 44
	LWI        r29, r10, 32
	LWI        r30, r10, 28
	LWI        r31, r10, 24
	ADD      r28, r10, r0
$BB11_10:                               # %do.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	RAND      r3
	RAND      r4
	FPADD      r5, r4, r4
	ORI       r4, r0, -1082130432
	FPADD      r12, r5, r4
	FPADD      r3, r3, r3
	FPADD      r21, r3, r4
	FPMUL      r4, r21, r21
	FPMUL      r5, r12, r12
	FPADD      r3, r4, r5
	ORI       r6, r0, 1065353216
	FPGE   r6, r3, r6
	bneid     r6, ($BB11_12)
	ADDI      r3, r0, 1
# BB#11:                                # %do.body.i.i
                                        #   in Loop: Header=BB11_10 Depth=1
	ADDI      r3, r0, 0
$BB11_12:                               # %do.body.i.i
                                        #   in Loop: Header=BB11_10 Depth=1
	bneid     r3, ($BB11_10)
	NOP    
# BB#13:                                # %do.end.i.i
	ORI       r3, r0, 1065353216
	FPRSUB     r4, r4, r3
	FPRSUB     r4, r5, r4
	FPINVSQRT r23, r4
	LWI        r6, r28, 12
	ORI       r4, r0, 0
	FPGE   r5, r6, r4
	FPUN   r4, r6, r4
	BITOR        r4, r4, r5
	bneid     r4, ($BB11_15)
	ADDI      r5, r0, 1
# BB#14:                                # %do.end.i.i
	ADDI      r5, r0, 0
$BB11_15:                               # %do.end.i.i
	bneid     r5, ($BB11_17)
	ADD      r4, r6, r0
# BB#16:                                # %cond.true.i55.i.i.i
	FPNEG      r4, r6
$BB11_17:                               # %_ZL4Fabsf.exit57.i.i.i
	LWI        r7, r28, 16
	ORI       r5, r0, 0
	FPGE   r8, r7, r5
	FPUN   r5, r7, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB11_19)
	ADDI      r5, r0, 1
# BB#18:                                # %_ZL4Fabsf.exit57.i.i.i
	ADDI      r5, r0, 0
$BB11_19:                               # %_ZL4Fabsf.exit57.i.i.i
	bneid     r5, ($BB11_21)
	ADD      r24, r7, r0
# BB#20:                                # %cond.true.i50.i.i.i
	FPNEG      r24, r7
$BB11_21:                               # %_ZL4Fabsf.exit52.i.i.i
	LWI        r10, r28, 20
	ORI       r5, r0, 0
	FPGE   r8, r10, r5
	FPUN   r5, r10, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB11_23)
	ADDI      r5, r0, 1
# BB#22:                                # %_ZL4Fabsf.exit52.i.i.i
	ADDI      r5, r0, 0
$BB11_23:                               # %_ZL4Fabsf.exit52.i.i.i
	bneid     r5, ($BB11_25)
	ADD      r8, r10, r0
# BB#24:                                # %cond.true.i.i.i.i
	FPNEG      r8, r10
$BB11_25:                               # %_ZL4Fabsf.exit.i.i.i
	FPGE   r5, r4, r24
	FPUN   r11, r4, r24
	BITOR        r5, r11, r5
	bneid     r5, ($BB11_27)
	ADDI      r25, r0, 1
# BB#26:                                # %_ZL4Fabsf.exit.i.i.i
	ADDI      r25, r0, 0
$BB11_27:                               # %_ZL4Fabsf.exit.i.i.i
	FPMUL      r22, r12, r20
	FPMUL      r12, r21, r20
	FPDIV      r3, r3, r23
	ORI       r11, r0, 1065353216
	ORI       r5, r0, 0
	bneid     r25, ($BB11_31)
	NOP    
# BB#28:                                # %_ZL4Fabsf.exit.i.i.i
	FPLT   r20, r4, r8
	bneid     r20, ($BB11_30)
	ADDI      r4, r0, 1
# BB#29:                                # %_ZL4Fabsf.exit.i.i.i
	ADDI      r4, r0, 0
$BB11_30:                               # %_ZL4Fabsf.exit.i.i.i
	bneid     r4, ($BB11_35)
	ADD      r20, r5, r0
$BB11_31:                               # %if.else.i.i.i
	FPLT   r5, r24, r8
	bneid     r5, ($BB11_33)
	ADDI      r4, r0, 1
# BB#32:                                # %if.else.i.i.i
	ADDI      r4, r0, 0
$BB11_33:                               # %if.else.i.i.i
	ORI       r20, r0, 1065353216
	ORI       r5, r0, 0
	bneid     r4, ($BB11_35)
	ADD      r11, r5, r0
# BB#34:                                # %if.else18.i.i.i
	ORI       r20, r0, 0
	ORI       r5, r0, 1065353216
	ADD      r11, r20, r0
$BB11_35:                               # %_Z20randomReflectionConeRK6Vectorf.exit.i
	FPMUL      r4, r7, r11
	FPMUL      r8, r6, r20
	FPRSUB     r4, r4, r8
	FPMUL      r8, r10, r11
	FPMUL      r11, r6, r5
	FPRSUB     r8, r11, r8
	FPMUL      r11, r10, r8
	FPMUL      r21, r7, r4
	FPRSUB     r11, r11, r21
	FPMUL      r20, r10, r20
	FPMUL      r5, r7, r5
	FPRSUB     r20, r20, r5
	FPMUL      r5, r6, r4
	FPMUL      r21, r10, r20
	FPRSUB     r21, r5, r21
	FPMUL      r5, r11, r22
	FPMUL      r11, r20, r12
	FPADD      r5, r11, r5
	FPMUL      r11, r6, r3
	FPADD      r5, r5, r11
	FPMUL      r11, r21, r22
	FPMUL      r21, r8, r12
	FPADD      r11, r21, r11
	FPMUL      r21, r7, r3
	FPADD      r11, r11, r21
	FPMUL      r7, r7, r20
	FPMUL      r8, r6, r8
	FPMUL      r6, r11, r11
	FPMUL      r20, r5, r5
	FPADD      r6, r20, r6
	FPRSUB     r7, r7, r8
	FPMUL      r7, r7, r22
	FPMUL      r4, r4, r12
	FPADD      r4, r4, r7
	FPMUL      r3, r10, r3
	FPADD      r3, r4, r3
	FPMUL      r4, r3, r3
	FPADD      r4, r6, r4
	FPINVSQRT r4, r4
	ORI       r6, r0, 1065353216
	FPDIV      r4, r6, r4
	FPDIV      r4, r6, r4
	FPMUL      r3, r3, r4
	FPMUL      r6, r11, r4
	FPMUL      r5, r5, r4
	ADD      r4, r6, r0
	ORI       r22, r0, 1203982336
	brid      ($BB11_37)
	ADD      r10, r28, r0
$BB11_91:                               # %if.else
	LWI        r3, r8, 48
	SWI        r3, r5, 0
	LWI        r3, r8, 52
	SWI        r3, r5, 4
	LWI        r3, r8, 56
	SWI        r3, r5, 8
	LWI        r3, r8, 60
	brid      ($BB11_92)
	SWI        r3, r5, 12
$BB11_6:                                # %if.end
	ADDI      r7, r0, 1
	CMP       r7, r7, r6
	bneid     r7, ($BB11_7)
	NOP    
# BB#36:                                # %if.then14.i
	ORI       r22, r0, 1203982336
	LWI        r3, r10, 20
	LWI        r4, r10, 16
	LWI        r5, r10, 12
	LWI        r29, r10, 32
	LWI        r30, r10, 28
	brid      ($BB11_37)
	LWI        r31, r10, 24
$BB11_7:                                # %if.end
	ADD      r30, r29, r0
	bneid     r6, ($BB11_37)
	ADD      r31, r29, r0
# BB#8:                                 # %if.then.i
	LWI        r6, r10, 12
	FPRSUB     r5, r5, r6
	LWI        r6, r10, 16
	FPRSUB     r4, r4, r6
	FPMUL      r6, r4, r4
	FPMUL      r7, r5, r5
	FPADD      r6, r7, r6
	LWI        r7, r10, 20
	FPRSUB     r3, r3, r7
	FPMUL      r7, r3, r3
	FPADD      r6, r6, r7
	LWI        r29, r10, 32
	LWI        r30, r10, 28
	LWI        r31, r10, 24
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r22, r7, r6
	FPDIV      r6, r7, r22
	FPMUL      r3, r3, r6
	FPMUL      r4, r4, r6
	FPMUL      r5, r5, r6
$BB11_37:                               # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
	SWI       r5, r1, 188
	SWI       r4, r1, 184
	SWI       r3, r1, 180
	ADD      r6, r5, r0
	ADD      r5, r3, r0
	FPMUL      r3, r27, r4
	LWI       r4, r1, 224
	FPMUL      r4, r4, r6
	FPADD      r3, r4, r3
	FPMUL      r4, r26, r5
	FPADD      r5, r3, r4
	SWI       r5, r1, 256
	ORI       r3, r0, 0
	FPLE   r4, r5, r3
	FPUN   r5, r5, r3
	BITOR        r4, r5, r4
	bneid     r4, ($BB11_39)
	ADDI      r6, r0, 1
# BB#38:                                # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
	ADDI      r6, r0, 0
$BB11_39:                               # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
	SWI       r31, r1, 252
	SWI       r30, r1, 248
	SWI       r29, r1, 244
	SWI       r27, r1, 236
	SWI       r26, r1, 232
	ADD      r4, r3, r0
	bneid     r6, ($BB11_90)
	ADD      r5, r3, r0
# BB#40:                                # %if.then20
	SWI       r10, r1, 208
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 180
	FPDIV      r4, r3, r4
	SWI       r4, r1, 212
	LWI       r4, r1, 184
	FPDIV      r4, r3, r4
	SWI       r4, r1, 216
	LWI       r4, r1, 188
	FPDIV      r3, r3, r4
	SWI       r3, r1, 220
	ADD      r3, r0, r0
	SWI       r3, r1, 192
	LWI        r4, r9, 8
	SWI       r4, r1, 196
	LWI        r4, r9, 4
	SWI       r4, r1, 200
	LWI        r4, r9, 0
	SWI       r4, r1, 204
	brid      ($BB11_41)
	ADD      r30, r3, r0
$BB11_87:                               # %if.end19.i
                                        #   in Loop: Header=BB11_41 Depth=1
	bslli     r4, r20, 2
	ADDI      r5, r1, 52
	ADD      r4, r5, r4
	LWI       r30, r4, -4
$BB11_41:                               # %if.then20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_42 Depth 2
                                        #     Child Loop BB11_62 Depth 2
	brid      ($BB11_42)
	ADD      r20, r3, r0
$BB11_118:                              # %if.then10.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r3, r30, 1
	bslli     r4, r20, 2
	ADDI      r5, r1, 52
	SW        r3, r5, r4
	ADDI      r20, r20, 1
$BB11_42:                               # %while.body.i
                                        #   Parent Loop BB11_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	bslli     r3, r30, 3
	LWI       r4, r1, 208
	LWI       r4, r4, 0
	ADD      r6, r4, r3
	LOAD      r3, r6, 0
	LOAD      r5, r6, 1
	LOAD      r4, r6, 2
	ADDI      r10, r6, 3
	LOAD      r8, r10, 0
	LOAD      r7, r10, 1
	LWI       r9, r1, 200
	FPRSUB     r11, r9, r7
	FPRSUB     r7, r9, r5
	LOAD      r5, r10, 2
	LWI       r9, r1, 196
	FPRSUB     r5, r9, r5
	FPRSUB     r4, r9, r4
	LWI       r9, r1, 212
	FPMUL      r4, r4, r9
	FPMUL      r5, r5, r9
	LWI       r9, r1, 216
	FPMUL      r10, r7, r9
	FPMUL      r11, r11, r9
	LWI       r7, r1, 204
	FPRSUB     r8, r7, r8
	FPRSUB     r3, r7, r3
	LWI       r9, r1, 220
	FPMUL      r7, r3, r9
	FPMUL      r12, r8, r9
	FPMIN     r3, r7, r12
	FPMIN     r8, r10, r11
	FPMIN     r21, r4, r5
	FPMAX     r7, r7, r12
	FPMAX     r10, r10, r11
	FPMAX     r5, r4, r5
	FPMAX     r4, r8, r21
	FPMAX     r4, r3, r4
	FPMIN     r3, r10, r5
	FPMIN     r3, r7, r3
	FPGE   r5, r4, r3
	FPUN   r7, r4, r3
	BITOR        r7, r7, r5
	bneid     r7, ($BB11_44)
	ADDI      r5, r0, 1
# BB#43:                                # %while.body.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r5, r0, 0
$BB11_44:                               # %while.body.i
                                        #   in Loop: Header=BB11_42 Depth=2
	bneid     r5, ($BB11_86)
	NOP    
# BB#45:                                # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ORI       r5, r0, 869711765
	FPLE   r7, r4, r5
	FPUN   r5, r4, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB11_47)
	ADDI      r5, r0, 1
# BB#46:                                # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r5, r0, 0
$BB11_47:                               # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	bneid     r5, ($BB11_51)
	NOP    
# BB#48:                                # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ORI       r5, r0, 1259902592
	FPLT   r7, r4, r5
	bneid     r7, ($BB11_50)
	ADDI      r5, r0, 1
# BB#49:                                # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r5, r0, 0
$BB11_50:                               # %if.then.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	beqid     r5, ($BB11_51)
	NOP    
# BB#57:                                # %lor.lhs.false.i
                                        #   in Loop: Header=BB11_42 Depth=2
	FPLT   r3, r22, r4
	FPUN   r4, r22, r4
	BITOR        r4, r4, r3
	bneid     r4, ($BB11_59)
	ADDI      r3, r0, 1
# BB#58:                                # %lor.lhs.false.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r3, r0, 0
$BB11_59:                               # %lor.lhs.false.i
                                        #   in Loop: Header=BB11_42 Depth=2
	bneid     r3, ($BB11_86)
	NOP    
	brid      ($BB11_60)
	NOP    
$BB11_51:                               # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ORI       r4, r0, 869711765
	FPLE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB11_53)
	ADDI      r4, r0, 1
# BB#52:                                # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r4, r0, 0
$BB11_53:                               # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	bneid     r4, ($BB11_86)
	NOP    
# BB#54:                                # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ORI       r4, r0, 1259902592
	FPLT   r4, r3, r4
	bneid     r4, ($BB11_56)
	ADDI      r3, r0, 1
# BB#55:                                # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	ADDI      r3, r0, 0
$BB11_56:                               # %if.then24.i.i
                                        #   in Loop: Header=BB11_42 Depth=2
	beqid     r3, ($BB11_86)
	NOP    
$BB11_60:                               # %if.then6.i84
                                        #   in Loop: Header=BB11_42 Depth=2
	LOAD      r30, r6, 7
	LOAD      r6, r6, 6
	bltid     r6, ($BB11_118)
	NOP    
# BB#61:                                # %for.cond.preheader.i
                                        #   in Loop: Header=BB11_41 Depth=1
	bleid     r6, ($BB11_86)
	ADD      r21, r0, r0
$BB11_62:                               # %for.body.i
                                        #   Parent Loop BB11_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	LOAD      r5, r30, 0
	LOAD      r10, r30, 1
	LOAD      r4, r30, 2
	ADDI      r8, r30, 3
	LOAD      r3, r8, 0
	LOAD      r7, r8, 1
	LOAD      r11, r8, 2
	ADDI      r12, r30, 6
	LOAD      r8, r12, 0
	LOAD      r28, r12, 1
	LOAD      r23, r12, 2
	FPRSUB     r12, r23, r11
	FPRSUB     r26, r28, r7
	LWI       r25, r1, 180
	FPMUL      r7, r25, r26
	LWI       r31, r1, 184
	FPMUL      r11, r31, r12
	FPRSUB     r24, r7, r11
	FPRSUB     r3, r8, r3
	FPMUL      r7, r25, r3
	LWI       r9, r1, 188
	FPMUL      r11, r9, r12
	FPRSUB     r25, r11, r7
	LWI       r7, r1, 204
	FPRSUB     r11, r8, r7
	LWI       r7, r1, 200
	FPRSUB     r27, r28, r7
	FPMUL      r7, r27, r25
	FPMUL      r29, r11, r24
	FPADD      r7, r29, r7
	FPMUL      r29, r31, r3
	FPMUL      r31, r9, r26
	FPRSUB     r31, r29, r31
	FPRSUB     r8, r8, r5
	FPRSUB     r28, r28, r10
	LWI       r5, r1, 196
	FPRSUB     r29, r23, r5
	FPMUL      r5, r29, r31
	FPADD      r5, r7, r5
	FPMUL      r7, r28, r25
	FPMUL      r10, r8, r24
	FPADD      r7, r10, r7
	FPRSUB     r23, r23, r4
	FPMUL      r4, r23, r31
	FPADD      r4, r7, r4
	ORI       r7, r0, 1065353216
	FPDIV      r4, r7, r4
	FPMUL      r5, r5, r4
	ORI       r7, r0, 0
	FPLT   r10, r5, r7
	bneid     r10, ($BB11_64)
	ADDI      r7, r0, 1
# BB#63:                                # %for.body.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADDI      r7, r0, 0
$BB11_64:                               # %for.body.i
                                        #   in Loop: Header=BB11_62 Depth=2
	bneid     r7, ($BB11_85)
	NOP    
# BB#65:                                # %for.body.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ORI       r7, r0, 1065353216
	FPGT   r10, r5, r7
	bneid     r10, ($BB11_67)
	ADDI      r7, r0, 1
# BB#66:                                # %for.body.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADDI      r7, r0, 0
$BB11_67:                               # %for.body.i
                                        #   in Loop: Header=BB11_62 Depth=2
	bneid     r7, ($BB11_85)
	NOP    
# BB#68:                                # %if.end.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	FPMUL      r7, r29, r28
	FPMUL      r10, r27, r23
	FPRSUB     r10, r7, r10
	FPMUL      r7, r11, r23
	FPMUL      r23, r29, r8
	FPRSUB     r29, r7, r23
	LWI       r7, r1, 184
	FPMUL      r7, r7, r29
	LWI       r9, r1, 188
	FPMUL      r23, r9, r10
	FPADD      r7, r23, r7
	FPMUL      r8, r27, r8
	FPMUL      r11, r11, r28
	FPRSUB     r8, r8, r11
	LWI       r11, r1, 180
	FPMUL      r11, r11, r8
	FPADD      r7, r7, r11
	FPMUL      r11, r7, r4
	ORI       r7, r0, 0
	FPLT   r23, r11, r7
	bneid     r23, ($BB11_70)
	ADDI      r7, r0, 1
# BB#69:                                # %if.end.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADDI      r7, r0, 0
$BB11_70:                               # %if.end.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	bneid     r7, ($BB11_85)
	NOP    
# BB#71:                                # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	FPADD      r5, r11, r5
	ORI       r7, r0, 1065353216
	FPGT   r7, r5, r7
	bneid     r7, ($BB11_73)
	ADDI      r5, r0, 1
# BB#72:                                # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADDI      r5, r0, 0
$BB11_73:                               # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	bneid     r5, ($BB11_85)
	NOP    
# BB#74:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	FPMUL      r5, r26, r29
	FPMUL      r3, r3, r10
	FPADD      r3, r3, r5
	FPMUL      r5, r12, r8
	FPADD      r3, r3, r5
	FPMUL      r3, r3, r4
	ORI       r4, r0, 869711765
	FPGT   r7, r3, r4
	ADDI      r5, r0, 0
	ADDI      r4, r0, 1
	bneid     r7, ($BB11_76)
	ADD      r8, r4, r0
# BB#75:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADD      r8, r5, r0
$BB11_76:                               # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ORI       r7, r0, 0
	FPGT   r10, r3, r7
	bneid     r10, ($BB11_78)
	ADD      r7, r4, r0
# BB#77:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADD      r7, r5, r0
$BB11_78:                               # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	BITAND       r7, r7, r8
	FPLT   r10, r3, r22
	bneid     r10, ($BB11_80)
	ADD      r8, r4, r0
# BB#79:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADD      r8, r5, r0
$BB11_80:                               # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	BITAND       r5, r7, r8
	bneid     r5, ($BB11_82)
	NOP    
# BB#81:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADD      r3, r22, r0
$BB11_82:                               # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	bneid     r5, ($BB11_84)
	NOP    
# BB#83:                                # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	LWI       r4, r1, 192
$BB11_84:                               # %if.end15.i.i
                                        #   in Loop: Header=BB11_62 Depth=2
	SWI       r4, r1, 192
	ADD      r22, r3, r0
$BB11_85:                               # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i
                                        #   in Loop: Header=BB11_62 Depth=2
	ADDI      r21, r21, 1
	CMP       r3, r6, r21
	bltid     r3, ($BB11_62)
	ADDI      r30, r30, 11
$BB11_86:                               # %if.end16.i
                                        #   in Loop: Header=BB11_41 Depth=1
	ADDI      r3, r0, -1
	ADD      r3, r20, r3
	bgeid     r3, ($BB11_87)
	NOP    
# BB#88:                                # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit
	ORI       r3, r0, 0
	LWI       r4, r1, 192
	ANDI      r6, r4, 255
	ADD      r4, r3, r0
	ADD      r5, r3, r0
	bneid     r6, ($BB11_90)
	LWI       r10, r1, 208
# BB#89:                                # %if.then22
	LWI       r3, r1, 252
	LWI       r6, r1, 256
	FPMUL      r5, r3, r6
	LWI       r3, r1, 248
	FPMUL      r4, r3, r6
	LWI       r3, r1, 244
	FPMUL      r3, r3, r6
	ORI       r6, r0, 0
	FPADD      r3, r3, r6
	FPADD      r4, r4, r6
	FPADD      r5, r5, r6
	LWI       r6, r1, 228
	SWI        r5, r6, 0
	SWI        r4, r6, 4
	SWI        r3, r6, 8
$BB11_90:                               # %if.end26
	LWI       r6, r1, 240
	LWI       r6, r6, 12
	ADDI      r6, r6, 10
	LOAD      r6, r6, 0
	MULI      r6, r6, 25
	LWI       r7, r10, 4
	ADD      r6, r7, r6
	LOAD      r8, r6, 4
	LOAD      r7, r6, 5
	LOAD      r6, r6, 6
	LWI       r10, r1, 288
	SWI        r8, r10, 0
	SWI        r7, r10, 4
	SWI        r6, r10, 8
	ADDI      r9, r0, 1065353216
	SWI       r9, r10, 12
	FPMUL      r5, r5, r8
	LWI       r8, r1, 228
	SWI        r5, r8, 0
	FPMUL      r4, r4, r7
	SWI        r4, r8, 4
	FPMUL      r3, r3, r6
	SWI        r3, r8, 8
	LWI       r20, r1, 232
	LWI       r12, r1, 236
	LWI       r21, r1, 224
$BB11_92:                               # %do.body.i
                                        # =>This Inner Loop Header: Depth=1
	RAND      r3
	RAND      r4
	FPADD      r5, r4, r4
	ORI       r4, r0, -1082130432
	FPADD      r5, r5, r4
	FPADD      r3, r3, r3
	FPADD      r6, r3, r4
	FPMUL      r4, r6, r6
	FPMUL      r7, r5, r5
	FPADD      r3, r4, r7
	ORI       r8, r0, 1065353216
	FPGE   r8, r3, r8
	bneid     r8, ($BB11_94)
	ADDI      r3, r0, 1
# BB#93:                                # %do.body.i
                                        #   in Loop: Header=BB11_92 Depth=1
	ADDI      r3, r0, 0
$BB11_94:                               # %do.body.i
                                        #   in Loop: Header=BB11_92 Depth=1
	bneid     r3, ($BB11_92)
	NOP    
# BB#95:                                # %do.end.i
	ORI       r3, r0, 0
	FPGE   r8, r21, r3
	FPUN   r3, r21, r3
	BITOR        r3, r3, r8
	bneid     r3, ($BB11_97)
	ADDI      r8, r0, 1
# BB#96:                                # %do.end.i
	ADDI      r8, r0, 0
$BB11_97:                               # %do.end.i
	ORI       r3, r0, 1065353216
	FPRSUB     r4, r4, r3
	FPRSUB     r4, r7, r4
	FPINVSQRT r7, r4
	bneid     r8, ($BB11_99)
	ADD      r4, r21, r0
# BB#98:                                # %cond.true.i55.i.i
	FPNEG      r4, r21
$BB11_99:                               # %_ZL4Fabsf.exit57.i.i
	ORI       r8, r0, 0
	FPGE   r9, r12, r8
	FPUN   r8, r12, r8
	BITOR        r8, r8, r9
	bneid     r8, ($BB11_101)
	ADDI      r9, r0, 1
# BB#100:                               # %_ZL4Fabsf.exit57.i.i
	ADDI      r9, r0, 0
$BB11_101:                              # %_ZL4Fabsf.exit57.i.i
	bneid     r9, ($BB11_103)
	ADD      r8, r12, r0
# BB#102:                               # %cond.true.i50.i.i
	FPNEG      r8, r12
$BB11_103:                              # %_ZL4Fabsf.exit52.i.i
	ORI       r9, r0, 0
	FPGE   r10, r20, r9
	FPUN   r9, r20, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB11_105)
	ADDI      r10, r0, 1
# BB#104:                               # %_ZL4Fabsf.exit52.i.i
	ADDI      r10, r0, 0
$BB11_105:                              # %_ZL4Fabsf.exit52.i.i
	bneid     r10, ($BB11_107)
	ADD      r9, r20, r0
# BB#106:                               # %cond.true.i.i.i
	FPNEG      r9, r20
$BB11_107:                              # %_ZL4Fabsf.exit.i.i
	FPGE   r10, r4, r8
	FPUN   r11, r4, r8
	BITOR        r10, r11, r10
	bneid     r10, ($BB11_109)
	ADDI      r11, r0, 1
# BB#108:                               # %_ZL4Fabsf.exit.i.i
	ADDI      r11, r0, 0
$BB11_109:                              # %_ZL4Fabsf.exit.i.i
	FPDIV      r3, r3, r7
	ORI       r10, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r11, ($BB11_113)
	NOP    
# BB#110:                               # %_ZL4Fabsf.exit.i.i
	FPLT   r11, r4, r9
	bneid     r11, ($BB11_112)
	ADDI      r4, r0, 1
# BB#111:                               # %_ZL4Fabsf.exit.i.i
	ADDI      r4, r0, 0
$BB11_112:                              # %_ZL4Fabsf.exit.i.i
	bneid     r4, ($BB11_117)
	ADD      r11, r7, r0
$BB11_113:                              # %if.else.i.i
	FPLT   r7, r8, r9
	bneid     r7, ($BB11_115)
	ADDI      r4, r0, 1
# BB#114:                               # %if.else.i.i
	ADDI      r4, r0, 0
$BB11_115:                              # %if.else.i.i
	ORI       r11, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r4, ($BB11_117)
	ADD      r10, r7, r0
# BB#116:                               # %if.else18.i.i
	ORI       r11, r0, 0
	ORI       r7, r0, 1065353216
	ADD      r10, r11, r0
$BB11_117:                              # %_Z16randomReflectionRK6Vector.exit
	FPMUL      r4, r12, r10
	FPMUL      r8, r21, r11
	FPRSUB     r4, r4, r8
	FPMUL      r8, r20, r10
	FPMUL      r9, r21, r7
	FPRSUB     r8, r9, r8
	FPMUL      r9, r20, r8
	FPMUL      r10, r12, r4
	FPRSUB     r9, r9, r10
	FPMUL      r10, r20, r11
	FPMUL      r7, r12, r7
	FPRSUB     r10, r10, r7
	FPMUL      r7, r21, r4
	FPMUL      r11, r20, r10
	FPRSUB     r11, r7, r11
	FPMUL      r7, r9, r5
	FPMUL      r9, r10, r6
	FPADD      r7, r9, r7
	FPMUL      r9, r21, r3
	FPADD      r7, r7, r9
	FPMUL      r9, r11, r5
	FPMUL      r11, r8, r6
	FPADD      r9, r11, r9
	FPMUL      r11, r12, r3
	FPADD      r9, r9, r11
	FPMUL      r10, r12, r10
	FPMUL      r11, r21, r8
	FPMUL      r8, r9, r9
	FPMUL      r12, r7, r7
	FPADD      r8, r12, r8
	FPRSUB     r10, r10, r11
	FPMUL      r5, r10, r5
	FPMUL      r4, r4, r6
	FPADD      r4, r4, r5
	FPMUL      r3, r20, r3
	FPADD      r3, r4, r3
	FPMUL      r4, r3, r3
	FPADD      r4, r8, r4
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r5, r7, r4
	LWI       r6, r1, 284
	SWI        r5, r6, 0
	FPMUL      r5, r9, r4
	SWI        r5, r6, 4
	FPMUL      r3, r3, r4
	SWI        r3, r6, 8
	LWI       r31, r1, 4
	LWI       r30, r1, 8
	LWI       r29, r1, 12
	LWI       r28, r1, 16
	LWI       r27, r1, 20
	LWI       r26, r1, 24
	LWI       r25, r1, 28
	LWI       r24, r1, 32
	LWI       r23, r1, 36
	LWI       r22, r1, 40
	LWI       r21, r1, 44
	LWI       r20, r1, 48
	rtsd      r15, 8
	ADDI      r1, r1, 260
	.end	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
$tmp11:
	.size	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color, ($tmp11)-_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color

	.globl	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
	.align	2
	.type	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color,@function
	.ent	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color # @_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color:
	.frame	r1,352,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -352
	SWI       r20, r1, 48
	SWI       r21, r1, 44
	SWI       r22, r1, 40
	SWI       r23, r1, 36
	SWI       r24, r1, 32
	SWI       r25, r1, 28
	SWI       r26, r1, 24
	SWI       r27, r1, 20
	SWI       r28, r1, 16
	SWI       r29, r1, 12
	SWI       r30, r1, 8
	SWI       r31, r1, 4
	SWI       r10, r1, 376
	SWI       r9, r1, 372
	ADD      r22, r8, r0
	SWI       r22, r1, 220
	SWI       r6, r1, 360
	SWI       r5, r1, 356
	LWI        r3, r7, 8
	SWI       r3, r1, 180
	LWI        r3, r7, 4
	SWI       r3, r1, 184
	LWI        r9, r7, 0
	LWI        r3, r7, 12
	SWI       r3, r1, 236
	LWI        r8, r7, 16
	SWI       r8, r1, 240
	LWI        r4, r7, 20
	SWI       r4, r1, 244
	SWI       r0, r5, 12
	SWI       r0, r5, 8
	SWI       r0, r5, 4
	SWI       r0, r5, 0
	ORI       r11, r0, 0
	ORI       r5, r0, 1065353216
	FPDIV      r7, r5, r4
	SWI       r7, r1, 268
	FPDIV      r7, r5, r8
	SWI       r7, r1, 272
	FPDIV      r5, r5, r3
	SWI       r5, r1, 276
	ORI       r5, r0, 981668463
	FPMUL      r4, r4, r5
	SWI       r4, r1, 336
	FPMUL      r4, r8, r5
	SWI       r4, r1, 340
	FPMUL      r3, r3, r5
	SWI       r3, r1, 344
	ADD      r21, r11, r0
	ADD      r23, r11, r0
	brid      ($BB12_1)
	ADD      r12, r11, r0
$BB12_256:                              #   in Loop: Header=BB12_1 Depth=1
	LWI       r11, r1, 208
	LWI       r21, r1, 224
	LWI       r23, r1, 228
	LWI       r12, r1, 192
	LWI       r22, r1, 220
$BB12_1:                                # %do.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_212 Depth 2
                                        #       Child Loop BB12_232 Depth 3
                                        #     Child Loop BB12_43 Depth 2
                                        #       Child Loop BB12_44 Depth 3
                                        #         Child Loop BB12_65 Depth 4
                                        #         Child Loop BB12_45 Depth 4
                                        #     Child Loop BB12_11 Depth 2
	ADD      r8, r6, r0
	lbui      r5, r8, 16
	beqid     r5, ($BB12_258)
	LWI       r20, r1, 280
# BB#2:                                 # %if.then
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r23, r1, 296
	SWI       r21, r1, 292
	SWI       r12, r1, 288
	SWI       r11, r1, 284
	LWI        r5, r8, 8
	LWI       r20, r1, 236
	FPMUL      r6, r20, r5
	FPADD      r6, r9, r6
	LWI       r7, r1, 372
	SWI        r6, r7, 0
	LWI       r21, r1, 240
	FPMUL      r6, r21, r5
	LWI       r3, r1, 184
	FPADD      r4, r3, r6
	SWI        r4, r7, 4
	LWI       r26, r1, 244
	FPMUL      r4, r26, r5
	LWI       r3, r1, 180
	FPADD      r3, r3, r4
	SWI        r3, r7, 8
	ADD      r12, r7, r0
	LWI       r6, r8, 12
	LOAD      r4, r6, 0
	LOAD      r5, r6, 1
	LOAD      r3, r6, 2
	ADDI      r8, r6, 3
	LOAD      r7, r8, 0
	LOAD      r10, r8, 1
	LOAD      r9, r8, 2
	ADDI      r11, r6, 6
	LOAD      r8, r11, 0
	LOAD      r6, r11, 1
	FPRSUB     r5, r6, r5
	FPRSUB     r6, r6, r10
	LOAD      r10, r11, 2
	FPRSUB     r9, r10, r9
	FPRSUB     r10, r10, r3
	FPMUL      r3, r10, r6
	FPMUL      r11, r5, r9
	FPRSUB     r3, r3, r11
	FPRSUB     r7, r8, r7
	FPRSUB     r8, r8, r4
	FPMUL      r4, r8, r9
	FPMUL      r9, r10, r7
	FPRSUB     r4, r4, r9
	FPMUL      r9, r4, r4
	FPMUL      r10, r3, r3
	FPADD      r9, r10, r9
	FPMUL      r5, r5, r7
	FPMUL      r6, r8, r6
	FPRSUB     r5, r5, r6
	FPMUL      r6, r5, r5
	FPADD      r6, r9, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r6, r7, r6
	FPDIV      r6, r7, r6
	FPMUL      r24, r4, r6
	FPMUL      r4, r24, r21
	FPMUL      r25, r3, r6
	FPMUL      r3, r25, r20
	FPADD      r3, r3, r4
	FPMUL      r23, r5, r6
	FPMUL      r4, r23, r26
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB12_4)
	ADDI      r3, r0, 1
# BB#3:                                 # %if.then
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, 0
$BB12_4:                                # %if.then
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r3, ($BB12_6)
	LWI       r21, r1, 204
# BB#5:                                 # %if.then11
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r23, r23
	FPNEG      r24, r24
	FPNEG      r25, r25
$BB12_6:                                # %if.end
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r3, r0, 981668463
	FPMUL      r6, r23, r3
	SWI       r6, r1, 304
	FPMUL      r7, r24, r3
	SWI       r7, r1, 308
	FPMUL      r4, r25, r3
	SWI       r4, r1, 300
	LWI        r3, r12, 0
	FPADD      r4, r3, r4
	LWI        r3, r12, 8
	LWI        r5, r12, 4
	SWI        r4, r12, 0
	FPADD      r5, r5, r7
	SWI        r5, r12, 4
	FPADD      r3, r3, r6
	SWI        r3, r12, 8
	ORI       r6, r0, -1082130432
	SWI       r6, r1, 252
	ORI       r27, r0, 1065353216
	LWI       r6, r22, 40
	ADDI      r7, r0, 2
	CMP       r7, r7, r6
	bneid     r7, ($BB12_7)
	NOP    
# BB#10:                                # %if.then6.i
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI        r7, r22, 44
	LWI        r27, r22, 32
	LWI        r28, r22, 28
	LWI        r29, r22, 24
$BB12_11:                               # %do.body.i.i
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	RAND      r3
	RAND      r4
	FPADD      r5, r4, r4
	ORI       r4, r0, -1082130432
	FPADD      r9, r5, r4
	FPADD      r3, r3, r3
	FPADD      r10, r3, r4
	FPMUL      r3, r10, r10
	FPMUL      r4, r9, r9
	FPADD      r5, r3, r4
	ORI       r6, r0, 1065353216
	FPGE   r6, r5, r6
	bneid     r6, ($BB12_13)
	ADDI      r5, r0, 1
# BB#12:                                # %do.body.i.i
                                        #   in Loop: Header=BB12_11 Depth=2
	ADDI      r5, r0, 0
$BB12_13:                               # %do.body.i.i
                                        #   in Loop: Header=BB12_11 Depth=2
	bneid     r5, ($BB12_11)
	NOP    
# BB#14:                                # %do.end.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r6, r0, 1065353216
	FPRSUB     r3, r3, r6
	FPRSUB     r3, r4, r3
	FPINVSQRT r11, r3
	LWI        r3, r22, 12
	ORI       r4, r0, 0
	FPGE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB12_16)
	ADDI      r4, r0, 1
# BB#15:                                # %do.end.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r4, r0, 0
$BB12_16:                               # %do.end.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r4, ($BB12_18)
	ADD      r12, r3, r0
# BB#17:                                # %cond.true.i55.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r12, r3
$BB12_18:                               # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI        r4, r22, 16
	ORI       r5, r0, 0
	FPGE   r8, r4, r5
	FPUN   r5, r4, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB12_20)
	ADDI      r5, r0, 1
# BB#19:                                # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r5, r0, 0
$BB12_20:                               # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r5, ($BB12_22)
	ADD      r20, r4, r0
# BB#21:                                # %cond.true.i50.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r20, r4
$BB12_22:                               # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI        r5, r22, 20
	ORI       r8, r0, 0
	FPGE   r21, r5, r8
	FPUN   r8, r5, r8
	BITOR        r21, r8, r21
	bneid     r21, ($BB12_24)
	ADDI      r8, r0, 1
# BB#23:                                # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r8, r0, 0
$BB12_24:                               # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r26, r22, r0
	bneid     r8, ($BB12_26)
	ADD      r21, r5, r0
# BB#25:                                # %cond.true.i.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r21, r5
$BB12_26:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPGE   r8, r12, r20
	FPUN   r22, r12, r20
	BITOR        r8, r22, r8
	bneid     r8, ($BB12_28)
	ADDI      r22, r0, 1
# BB#27:                                # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r22, r0, 0
$BB12_28:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPMUL      r8, r9, r7
	FPMUL      r7, r10, r7
	FPDIV      r6, r6, r11
	ORI       r11, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r22, ($BB12_32)
	NOP    
# BB#29:                                # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPLT   r12, r12, r21
	bneid     r12, ($BB12_31)
	ADDI      r10, r0, 1
# BB#30:                                # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r10, r0, 0
$BB12_31:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r10, ($BB12_36)
	ADD      r12, r9, r0
$BB12_32:                               # %if.else.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPLT   r9, r20, r21
	bneid     r9, ($BB12_34)
	ADDI      r10, r0, 1
# BB#33:                                # %if.else.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r10, r0, 0
$BB12_34:                               # %if.else.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r12, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r10, ($BB12_36)
	ADD      r11, r9, r0
# BB#35:                                # %if.else18.i.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r12, r0, 0
	ORI       r9, r0, 1065353216
	ADD      r11, r12, r0
$BB12_36:                               # %_Z20randomReflectionConeRK6Vectorf.exit.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPMUL      r10, r4, r11
	FPMUL      r20, r3, r12
	FPRSUB     r10, r10, r20
	FPMUL      r11, r5, r11
	FPMUL      r20, r3, r9
	FPRSUB     r11, r20, r11
	FPMUL      r20, r5, r11
	FPMUL      r21, r4, r10
	FPRSUB     r21, r20, r21
	FPMUL      r12, r5, r12
	FPMUL      r9, r4, r9
	FPRSUB     r20, r12, r9
	FPMUL      r9, r3, r10
	FPMUL      r12, r5, r20
	FPRSUB     r12, r9, r12
	FPMUL      r9, r21, r8
	FPMUL      r21, r20, r7
	FPADD      r9, r21, r9
	FPMUL      r21, r3, r6
	FPADD      r9, r9, r21
	FPMUL      r12, r12, r8
	FPMUL      r21, r11, r7
	FPADD      r12, r21, r12
	FPMUL      r21, r4, r6
	FPADD      r12, r12, r21
	FPMUL      r4, r4, r20
	FPMUL      r11, r3, r11
	FPMUL      r3, r12, r12
	FPMUL      r20, r9, r9
	FPADD      r3, r20, r3
	FPRSUB     r4, r4, r11
	FPMUL      r4, r4, r8
	FPMUL      r7, r10, r7
	FPADD      r4, r7, r4
	FPMUL      r5, r5, r6
	FPADD      r4, r4, r5
	FPMUL      r5, r4, r4
	FPADD      r3, r3, r5
	FPINVSQRT r3, r3
	ORI       r5, r0, 1065353216
	FPDIV      r3, r5, r3
	FPDIV      r3, r5, r3
	FPMUL      r21, r4, r3
	FPMUL      r7, r12, r3
	FPMUL      r8, r9, r3
	ORI       r3, r0, 1203982336
	SWI       r3, r1, 252
	brid      ($BB12_38)
	ADD      r22, r26, r0
$BB12_7:                                # %if.end
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r7, r0, 1
	CMP       r7, r7, r6
	bneid     r7, ($BB12_8)
	NOP    
# BB#37:                                # %if.then14.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r3, r0, 1203982336
	SWI       r3, r1, 252
	LWI        r21, r22, 20
	LWI        r7, r22, 16
	LWI        r8, r22, 12
	LWI        r27, r22, 32
	LWI        r28, r22, 28
	brid      ($BB12_38)
	LWI        r29, r22, 24
$BB12_8:                                # %if.end
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r28, r27, r0
	ADD      r29, r27, r0
	LWI       r7, r1, 212
	bneid     r6, ($BB12_38)
	LWI       r8, r1, 216
# BB#9:                                 # %if.then.i
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI        r6, r22, 12
	FPRSUB     r4, r4, r6
	LWI        r6, r22, 16
	FPRSUB     r5, r5, r6
	FPMUL      r6, r5, r5
	FPMUL      r7, r4, r4
	FPADD      r6, r7, r6
	LWI        r7, r22, 20
	FPRSUB     r3, r3, r7
	FPMUL      r7, r3, r3
	FPADD      r6, r6, r7
	LWI        r27, r22, 32
	LWI        r28, r22, 28
	LWI        r29, r22, 24
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r6, r7, r6
	SWI       r6, r1, 252
	FPDIV      r6, r7, r6
	FPMUL      r21, r3, r6
	FPMUL      r7, r5, r6
	FPMUL      r8, r4, r6
$BB12_38:                               # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r8, r1, 216
	SWI       r7, r1, 212
	FPMUL      r3, r24, r7
	FPMUL      r4, r25, r8
	FPADD      r3, r4, r3
	FPMUL      r4, r23, r21
	SWI       r23, r1, 280
	FPADD      r4, r3, r4
	SWI       r4, r1, 324
	ORI       r23, r0, 0
	FPLE   r3, r4, r23
	FPUN   r4, r4, r23
	BITOR        r4, r4, r3
	bneid     r4, ($BB12_40)
	ADDI      r3, r0, 1
# BB#39:                                # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, 0
$BB12_40:                               # %_ZNK5Light8getLightER5ColorR6VectorRKS2_.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r25, r1, 332
	bneid     r3, ($BB12_41)
	SWI       r24, r1, 328
# BB#42:                                # %if.then20
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r29, r1, 320
	SWI       r28, r1, 316
	SWI       r27, r1, 312
	ORI       r3, r0, 981668463
	FPMUL      r4, r21, r3
	SWI       r4, r1, 256
	LWI       r4, r1, 212
	FPMUL      r5, r4, r3
	SWI       r5, r1, 260
	LWI       r5, r1, 216
	FPMUL      r3, r5, r3
	SWI       r3, r1, 264
	ORI       r6, r0, 1065353216
	SWI       r6, r1, 248
	FPDIV      r3, r6, r21
	SWI       r3, r1, 224
	FPDIV      r3, r6, r4
	SWI       r3, r1, 228
	ADD      r3, r5, r0
	FPDIV      r3, r6, r3
	SWI       r3, r1, 232
	LWI       r3, r1, 372
	LWI        r4, r3, 8
	SWI       r4, r1, 184
	LWI        r4, r3, 4
	SWI       r4, r1, 192
	brid      ($BB12_43)
	LWI        r3, r3, 0
$BB12_41:                               #   in Loop: Header=BB12_1 Depth=1
	ADD      r4, r23, r0
	brid      ($BB12_154)
	ADD      r3, r23, r0
$BB12_290:                              # %if.then38
                                        #   in Loop: Header=BB12_43 Depth=2
	FPMUL      r3, r4, r22
	LWI       r4, r1, 192
	FPADD      r3, r4, r3
	FPMUL      r4, r21, r22
	LWI       r5, r1, 184
	FPADD      r4, r5, r4
	LWI       r5, r1, 256
	FPADD      r4, r4, r5
	SWI       r4, r1, 184
	LWI       r4, r1, 260
	FPADD      r3, r3, r4
	SWI       r3, r1, 192
	LWI       r3, r1, 216
	FPMUL      r3, r3, r22
	LWI       r4, r1, 188
	FPADD      r3, r4, r3
	LWI       r4, r1, 264
	FPADD      r3, r3, r4
$BB12_43:                               # %if.then20
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_44 Depth 3
                                        #         Child Loop BB12_65 Depth 4
                                        #         Child Loop BB12_45 Depth 4
	SWI       r3, r1, 188
	ADD      r4, r0, r0
	SWI       r4, r1, 208
	SWI       r4, r1, 204
	LWI       r22, r1, 252
	brid      ($BB12_44)
	ADD      r3, r4, r0
$BB12_97:                               # %if.end19.i570
                                        #   in Loop: Header=BB12_44 Depth=3
	bslli     r3, r3, 2
	ADDI      r5, r1, 52
	ADD      r3, r5, r3
	LWI       r3, r3, -4
$BB12_44:                               # %do.body21
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_43 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_65 Depth 4
                                        #         Child Loop BB12_45 Depth 4
	brid      ($BB12_45)
	SWI       r4, r1, 180
$BB12_289:                              # %if.then10.i481
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r4, r3, 1
	LWI       r7, r1, 180
	bslli     r5, r7, 2
	ADDI      r6, r1, 52
	SW        r4, r6, r5
	ADDI      r7, r7, 1
	SWI       r7, r1, 180
$BB12_45:                               # %while.body.i461
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_43 Depth=2
                                        #       Parent Loop BB12_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	bslli     r3, r3, 3
	LWI       r4, r1, 220
	LWI       r4, r4, 0
	ADD      r4, r4, r3
	LOAD      r3, r4, 0
	LOAD      r6, r4, 1
	LOAD      r5, r4, 2
	ADDI      r11, r4, 3
	LOAD      r8, r11, 0
	LOAD      r9, r11, 1
	LWI       r7, r1, 192
	FPRSUB     r10, r7, r9
	FPRSUB     r9, r7, r6
	LOAD      r6, r11, 2
	LWI       r7, r1, 184
	FPRSUB     r6, r7, r6
	FPRSUB     r5, r7, r5
	LWI       r7, r1, 224
	FPMUL      r5, r5, r7
	FPMUL      r6, r6, r7
	LWI       r7, r1, 228
	FPMUL      r9, r9, r7
	FPMUL      r10, r10, r7
	LWI       r7, r1, 188
	FPRSUB     r11, r7, r8
	FPRSUB     r3, r7, r3
	LWI       r7, r1, 232
	FPMUL      r8, r3, r7
	FPMUL      r12, r11, r7
	FPMIN     r3, r8, r12
	FPMIN     r11, r9, r10
	FPMIN     r20, r5, r6
	FPMAX     r8, r8, r12
	FPMAX     r9, r9, r10
	FPMAX     r5, r5, r6
	FPMAX     r6, r11, r20
	FPMAX     r6, r3, r6
	FPMIN     r3, r9, r5
	FPMIN     r3, r8, r3
	FPGE   r5, r6, r3
	FPUN   r8, r6, r3
	BITOR        r8, r8, r5
	bneid     r8, ($BB12_47)
	ADDI      r5, r0, 1
# BB#46:                                # %while.body.i461
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r5, r0, 0
$BB12_47:                               # %while.body.i461
                                        #   in Loop: Header=BB12_45 Depth=4
	bneid     r5, ($BB12_96)
	NOP    
# BB#48:                                # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	ORI       r5, r0, 869711765
	FPLE   r8, r6, r5
	FPUN   r5, r6, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB12_50)
	ADDI      r5, r0, 1
# BB#49:                                # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r5, r0, 0
$BB12_50:                               # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	bneid     r5, ($BB12_54)
	NOP    
# BB#51:                                # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	ORI       r5, r0, 1259902592
	FPLT   r8, r6, r5
	bneid     r8, ($BB12_53)
	ADDI      r5, r0, 1
# BB#52:                                # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r5, r0, 0
$BB12_53:                               # %if.then.i.i465
                                        #   in Loop: Header=BB12_45 Depth=4
	beqid     r5, ($BB12_54)
	NOP    
# BB#60:                                # %lor.lhs.false.i471
                                        #   in Loop: Header=BB12_45 Depth=4
	FPLT   r3, r22, r6
	FPUN   r5, r22, r6
	BITOR        r5, r5, r3
	bneid     r5, ($BB12_62)
	ADDI      r3, r0, 1
# BB#61:                                # %lor.lhs.false.i471
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r3, r0, 0
$BB12_62:                               # %lor.lhs.false.i471
                                        #   in Loop: Header=BB12_45 Depth=4
	bneid     r3, ($BB12_96)
	NOP    
	brid      ($BB12_63)
	NOP    
$BB12_54:                               # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	ORI       r5, r0, 869711765
	FPLE   r6, r3, r5
	FPUN   r5, r3, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_56)
	ADDI      r5, r0, 1
# BB#55:                                # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r5, r0, 0
$BB12_56:                               # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	bneid     r5, ($BB12_96)
	NOP    
# BB#57:                                # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	ORI       r5, r0, 1259902592
	FPLT   r5, r3, r5
	bneid     r5, ($BB12_59)
	ADDI      r3, r0, 1
# BB#58:                                # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	ADDI      r3, r0, 0
$BB12_59:                               # %if.then24.i.i469
                                        #   in Loop: Header=BB12_45 Depth=4
	beqid     r3, ($BB12_96)
	NOP    
$BB12_63:                               # %if.then6.i475
                                        #   in Loop: Header=BB12_45 Depth=4
	LOAD      r3, r4, 7
	LOAD      r4, r4, 6
	bltid     r4, ($BB12_289)
	NOP    
# BB#64:                                # %for.cond.preheader.i477
                                        #   in Loop: Header=BB12_44 Depth=3
	bleid     r4, ($BB12_96)
	ADD      r11, r0, r0
$BB12_65:                               # %for.body.i528
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_43 Depth=2
                                        #       Parent Loop BB12_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	ADD      r25, r22, r0
	LOAD      r12, r3, 0
	LOAD      r24, r3, 1
	LOAD      r10, r3, 2
	ADDI      r8, r3, 3
	LOAD      r6, r8, 0
	LOAD      r5, r8, 1
	LOAD      r8, r8, 2
	ADDI      r9, r3, 6
	LOAD      r27, r9, 0
	LOAD      r29, r9, 1
	LOAD      r26, r9, 2
	FPRSUB     r20, r26, r8
	FPRSUB     r28, r29, r5
	FPMUL      r5, r21, r28
	LWI       r31, r1, 212
	FPMUL      r8, r31, r20
	FPRSUB     r5, r5, r8
	FPRSUB     r6, r27, r6
	FPMUL      r8, r21, r6
	ADD      r7, r21, r0
	LWI       r22, r1, 216
	FPMUL      r9, r22, r20
	FPRSUB     r9, r9, r8
	LWI       r8, r1, 188
	FPRSUB     r8, r27, r8
	LWI       r21, r1, 192
	FPRSUB     r30, r29, r21
	FPMUL      r21, r30, r9
	FPMUL      r23, r8, r5
	FPADD      r23, r23, r21
	FPMUL      r21, r31, r6
	FPMUL      r31, r22, r28
	FPRSUB     r21, r21, r31
	FPRSUB     r31, r27, r12
	FPRSUB     r24, r29, r24
	LWI       r12, r1, 184
	FPRSUB     r27, r26, r12
	FPMUL      r12, r27, r21
	FPADD      r29, r23, r12
	FPMUL      r9, r24, r9
	FPMUL      r5, r31, r5
	FPADD      r5, r5, r9
	FPRSUB     r26, r26, r10
	FPMUL      r9, r26, r21
	FPADD      r5, r5, r9
	ORI       r9, r0, 1065353216
	FPDIV      r12, r9, r5
	FPMUL      r29, r29, r12
	ORI       r5, r0, 0
	FPLT   r9, r29, r5
	bneid     r9, ($BB12_67)
	ADDI      r5, r0, 1
# BB#66:                                # %for.body.i528
                                        #   in Loop: Header=BB12_65 Depth=4
	ADDI      r5, r0, 0
$BB12_67:                               # %for.body.i528
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r5, ($BB12_68)
	ADD      r22, r25, r0
# BB#69:                                # %for.body.i528
                                        #   in Loop: Header=BB12_65 Depth=4
	ORI       r5, r0, 1065353216
	FPGT   r9, r29, r5
	ADDI      r5, r0, 1
	bneid     r9, ($BB12_71)
	ADD      r21, r7, r0
# BB#70:                                # %for.body.i528
                                        #   in Loop: Header=BB12_65 Depth=4
	ADDI      r5, r0, 0
$BB12_71:                               # %for.body.i528
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r5, ($BB12_95)
	NOP    
# BB#72:                                # %if.end.i.i545
                                        #   in Loop: Header=BB12_65 Depth=4
	FPMUL      r5, r27, r24
	FPMUL      r9, r30, r26
	FPRSUB     r10, r5, r9
	FPMUL      r5, r8, r26
	FPMUL      r9, r27, r31
	FPRSUB     r27, r5, r9
	LWI       r5, r1, 212
	FPMUL      r5, r5, r27
	LWI       r7, r1, 216
	FPMUL      r9, r7, r10
	FPADD      r5, r9, r5
	FPMUL      r9, r30, r31
	FPMUL      r8, r8, r24
	FPRSUB     r24, r9, r8
	FPMUL      r8, r21, r24
	FPADD      r5, r5, r8
	FPMUL      r8, r5, r12
	ORI       r5, r0, 0
	FPLT   r9, r8, r5
	bneid     r9, ($BB12_74)
	ADDI      r5, r0, 1
# BB#73:                                # %if.end.i.i545
                                        #   in Loop: Header=BB12_65 Depth=4
	ADDI      r5, r0, 0
$BB12_74:                               # %if.end.i.i545
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r5, ($BB12_95)
	NOP    
# BB#75:                                # %lor.lhs.false12.i.i548
                                        #   in Loop: Header=BB12_65 Depth=4
	FPADD      r5, r8, r29
	ORI       r9, r0, 1065353216
	FPGT   r9, r5, r9
	bneid     r9, ($BB12_77)
	ADDI      r5, r0, 1
# BB#76:                                # %lor.lhs.false12.i.i548
                                        #   in Loop: Header=BB12_65 Depth=4
	ADDI      r5, r0, 0
$BB12_77:                               # %lor.lhs.false12.i.i548
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r5, ($BB12_95)
	NOP    
# BB#78:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	FPMUL      r5, r28, r27
	FPMUL      r6, r6, r10
	FPADD      r5, r6, r5
	FPMUL      r6, r20, r24
	FPADD      r5, r5, r6
	FPMUL      r6, r5, r12
	ORI       r5, r0, 869711765
	FPGT   r9, r6, r5
	ADDI      r10, r0, 0
	ADDI      r12, r0, 1
	bneid     r9, ($BB12_80)
	ADD      r5, r12, r0
# BB#79:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	ADD      r5, r10, r0
$BB12_80:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	ORI       r9, r0, 0
	FPGT   r20, r6, r9
	bneid     r20, ($BB12_82)
	ADD      r9, r12, r0
# BB#81:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	ADD      r9, r10, r0
$BB12_82:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	BITAND       r5, r9, r5
	FPLT   r20, r6, r22
	bneid     r20, ($BB12_84)
	ADD      r9, r12, r0
# BB#83:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	ADD      r9, r10, r0
$BB12_84:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	BITAND       r10, r5, r9
	bneid     r10, ($BB12_86)
	NOP    
# BB#85:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	LWI       r29, r1, 200
$BB12_86:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r10, ($BB12_88)
	NOP    
# BB#87:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	LWI       r8, r1, 196
$BB12_88:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r10, ($BB12_90)
	NOP    
# BB#89:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	ADD      r6, r22, r0
$BB12_90:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r10, ($BB12_92)
	ADD      r5, r3, r0
# BB#91:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	LWI       r5, r1, 204
$BB12_92:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	bneid     r10, ($BB12_94)
	NOP    
# BB#93:                                # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	LWI       r12, r1, 208
$BB12_94:                               # %if.end15.i.i558
                                        #   in Loop: Header=BB12_65 Depth=4
	SWI       r12, r1, 208
	SWI       r5, r1, 204
	ADD      r22, r6, r0
	SWI       r8, r1, 196
	brid      ($BB12_95)
	SWI       r29, r1, 200
$BB12_68:                               #   in Loop: Header=BB12_65 Depth=4
	ADD      r21, r7, r0
$BB12_95:                               # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i565
                                        #   in Loop: Header=BB12_65 Depth=4
	ADDI      r11, r11, 1
	CMP       r5, r4, r11
	bltid     r5, ($BB12_65)
	ADDI      r3, r3, 11
$BB12_96:                               # %if.end16.i568
                                        #   in Loop: Header=BB12_44 Depth=3
	ADDI      r3, r0, -1
	LWI       r5, r1, 180
	ADD      r4, r5, r3
	bgeid     r4, ($BB12_97)
	ADD      r3, r5, r0
# BB#98:                                # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit571
                                        #   in Loop: Header=BB12_43 Depth=2
	LWI       r3, r1, 208
	ANDI      r3, r3, 255
	beqid     r3, ($BB12_99)
	LWI       r5, r1, 204
# BB#100:                               # %if.end26
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r3, r5, 10
	LOAD      r3, r3, 0
	MULI      r3, r3, 25
	LWI       r4, r1, 220
	LWI       r4, r4, 4
	ADD      r3, r4, r3
	LOAD      r3, r3, 15
	bltid     r3, ($BB12_149)
	NOP    
# BB#101:                               # %if.then32
                                        #   in Loop: Header=BB12_43 Depth=2
	LOAD      r4, r5, 9
	MULI      r5, r4, 9
	LWI       r10, r1, 220
	LWI       r4, r10, 8
	ADD      r6, r4, r5
	LOAD      r7, r6, 0
	LOAD      r4, r6, 1
	LOAD      r6, r6, 2
	LWI       r6, r10, 8
	ADD      r6, r5, r6
	ADDI      r9, r6, 3
	LOAD      r8, r9, 0
	LOAD      r6, r9, 1
	LOAD      r9, r9, 2
	LWI       r11, r1, 200
	FPMUL      r7, r7, r11
	LWI       r9, r1, 196
	FPMUL      r8, r8, r9
	FPADD      r8, r7, r8
	LWI       r7, r10, 8
	ADD      r5, r5, r7
	ORI       r7, r0, 1065353216
	FPRSUB     r7, r11, r7
	FPRSUB     r7, r9, r7
	ADDI      r10, r5, 6
	LOAD      r5, r10, 0
	FPMUL      r5, r5, r7
	FPADD      r20, r8, r5
	INTCONV      r5, r20
	FPCONV       r8, r5
	ADDI      r5, r0, 1
	FPEQ   r9, r20, r8
	bneid     r9, ($BB12_103)
	NOP    
# BB#102:                               # %if.then32
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 0
$BB12_103:                              # %if.then32
                                        #   in Loop: Header=BB12_43 Depth=2
	LWI       r9, r1, 200
	FPMUL      r4, r4, r9
	LOAD      r9, r10, 1
	LOAD      r10, r10, 2
	LWI       r10, r1, 196
	FPMUL      r6, r6, r10
	FPADD      r4, r4, r6
	FPMUL      r6, r9, r7
	FPADD      r23, r4, r6
	LOAD      r11, r3, 0
	LOAD      r4, r3, 1
	LOAD      r7, r3, 2
	bneid     r5, ($BB12_105)
	NOP    
# BB#104:                               # %if.then.i161
                                        #   in Loop: Header=BB12_43 Depth=2
	FPRSUB     r20, r8, r20
$BB12_105:                              # %if.end.i166
                                        #   in Loop: Header=BB12_43 Depth=2
	INTCONV      r5, r23
	FPCONV       r5, r5
	FPEQ   r8, r23, r5
	bneid     r8, ($BB12_107)
	ADDI      r6, r0, 1
# BB#106:                               # %if.end.i166
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r6, r0, 0
$BB12_107:                              # %if.end.i166
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r6, ($BB12_109)
	NOP    
# BB#108:                               # %if.then32.i168
                                        #   in Loop: Header=BB12_43 Depth=2
	FPRSUB     r23, r5, r23
$BB12_109:                              # %if.end36.i171
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r5, r0, 0
	FPGE   r6, r20, r5
	FPUN   r5, r20, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_111)
	ADDI      r5, r0, 1
# BB#110:                               # %if.end36.i171
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 0
$BB12_111:                              # %if.end36.i171
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r5, ($BB12_113)
	NOP    
# BB#112:                               # %if.then38.i173
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r5, r0, 1065353216
	FPADD      r20, r20, r5
$BB12_113:                              # %if.end41.i176
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r5, r0, 0
	FPGE   r6, r23, r5
	FPUN   r5, r23, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_115)
	ADDI      r5, r0, 1
# BB#114:                               # %if.end41.i176
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 0
$BB12_115:                              # %if.end41.i176
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r5, ($BB12_117)
	NOP    
# BB#116:                               # %if.then43.i178
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r5, r0, 1065353216
	FPADD      r23, r23, r5
$BB12_117:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	FPCONV       r5, r4
	FPMUL      r5, r20, r5
	INTCONV      r6, r5
	CMP       r6, r4, r6
	ADDI      r12, r0, 0
	ADDI      r10, r0, 1
	beqid     r6, ($BB12_119)
	ADD      r9, r10, r0
# BB#118:                               # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r9, r12, r0
$BB12_119:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r24, r21, r0
	ORI       r6, r0, 0
	bneid     r9, ($BB12_121)
	ADD      r8, r6, r0
# BB#120:                               # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r8, r5, r0
$BB12_121:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	FPGE   r5, r8, r6
	FPUN   r9, r8, r6
	BITOR        r21, r9, r5
	FPCONV       r5, r11
	FPMUL      r5, r23, r5
	INTCONV      r9, r5
	CMP       r9, r11, r9
	bneid     r21, ($BB12_123)
	ADD      r20, r10, r0
# BB#122:                               # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r20, r12, r0
$BB12_123:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	beqid     r9, ($BB12_125)
	NOP    
# BB#124:                               # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r10, r12, r0
$BB12_125:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r10, ($BB12_127)
	ADD      r21, r24, r0
# BB#126:                               # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	ADD      r6, r5, r0
$BB12_127:                              # %if.end46.i191
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r20, ($BB12_132)
	NOP    
# BB#128:                               # %if.then.i.i197
                                        #   in Loop: Header=BB12_43 Depth=2
	FPNEG      r5, r8
	INTCONV      r5, r5
	RSUBI     r10, r5, 0
	FPCONV       r9, r10
	FPEQ   r9, r9, r8
	bneid     r9, ($BB12_130)
	ADDI      r8, r0, 1
# BB#129:                               # %if.then.i.i197
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r8, r0, 0
$BB12_130:                              # %if.then.i.i197
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r8, ($BB12_133)
	NOP    
# BB#131:                               # %if.else.i.i199
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r8, r0, -1
	brid      ($BB12_133)
	BITXOR       r10, r5, r8
$BB12_132:                              # %if.else6.i.i201
                                        #   in Loop: Header=BB12_43 Depth=2
	INTCONV      r10, r8
$BB12_133:                              # %_ZL5Floorf.exit.i210
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r5, r0, 0
	FPGE   r8, r6, r5
	FPUN   r5, r6, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB12_135)
	ADDI      r5, r0, 1
# BB#134:                               # %_ZL5Floorf.exit.i210
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 0
$BB12_135:                              # %_ZL5Floorf.exit.i210
                                        #   in Loop: Header=BB12_43 Depth=2
	DIV      r8, r4, r10
	MUL       r8, r8, r4
	RSUB     r8, r8, r10
	bsrai     r9, r8, 31
	BITAND       r9, r9, r4
	ADD      r8, r9, r8
	ADDI      r9, r8, 1
	DIV      r10, r4, r9
	MUL       r10, r10, r4
	bneid     r5, ($BB12_140)
	RSUB     r20, r10, r9
# BB#136:                               # %if.then.i285.i216
                                        #   in Loop: Header=BB12_43 Depth=2
	FPNEG      r5, r6
	INTCONV      r5, r5
	RSUBI     r10, r5, 0
	FPCONV       r9, r10
	FPEQ   r9, r9, r6
	bneid     r9, ($BB12_138)
	ADDI      r6, r0, 1
# BB#137:                               # %if.then.i285.i216
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r6, r0, 0
$BB12_138:                              # %if.then.i285.i216
                                        #   in Loop: Header=BB12_43 Depth=2
	bneid     r6, ($BB12_141)
	NOP    
# BB#139:                               # %if.else.i287.i218
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r6, r0, -1
	brid      ($BB12_141)
	BITXOR       r10, r5, r6
$BB12_140:                              # %if.else6.i289.i220
                                        #   in Loop: Header=BB12_43 Depth=2
	INTCONV      r10, r6
$BB12_141:                              # %_ZL5Floorf.exit291.i237
                                        #   in Loop: Header=BB12_43 Depth=2
	DIV      r5, r11, r10
	MUL       r5, r5, r11
	RSUB     r5, r5, r10
	bsrai     r6, r5, 31
	BITAND       r6, r6, r11
	ADD      r5, r6, r5
	MUL       r12, r5, r4
	ADDI      r5, r5, 1
	DIV      r6, r11, r5
	MUL       r6, r6, r11
	RSUB     r11, r6, r5
	ADD      r6, r12, r8
	ADDI      r5, r0, 4
	CMP       r5, r5, r7
	MUL       r9, r6, r7
	ORI       r6, r0, 1065353216
	ADDI      r3, r3, 3
	bneid     r5, ($BB12_142)
	ADD      r10, r9, r3
# BB#147:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread186.i364
                                        #   in Loop: Header=BB12_43 Depth=2
	MUL       r5, r11, r4
	ADD      r4, r5, r8
	ADD      r6, r5, r20
	ADD      r5, r12, r20
	bslli     r5, r5, 2
	bslli     r4, r4, 2
	ADD      r4, r4, r3
	ADD      r5, r5, r3
	bslli     r6, r6, 2
	ADD      r3, r6, r3
	LOAD      r6, r10, 1
	LOAD      r6, r10, 2
	LOAD      r6, r10, 3
	LOAD      r6, r10, 0
	LOAD      r7, r5, 1
	LOAD      r7, r5, 2
	LOAD      r7, r5, 3
	LOAD      r5, r5, 0
	LOAD      r7, r4, 1
	LOAD      r7, r4, 2
	LOAD      r7, r4, 3
	FPCONV       r6, r6
	LOAD      r8, r4, 0
	ORI       r4, r0, 1132396544
	FPDIV      r7, r6, r4
	LOAD      r6, r3, 1
	LOAD      r6, r3, 2
	LOAD      r6, r3, 3
	FPCONV       r5, r5
	FPDIV      r6, r5, r4
	FPCONV       r5, r8
	FPDIV      r23, r5, r4
	LOAD      r3, r3, 0
	FPCONV       r3, r3
	brid      ($BB12_148)
	FPDIV      r5, r3, r4
$BB12_142:                              # %_ZL5Floorf.exit291.i237
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 3
	CMP       r5, r5, r7
	bneid     r5, ($BB12_143)
	NOP    
# BB#146:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread179.i305
                                        #   in Loop: Header=BB12_43 Depth=2
	LOAD      r5, r10, 0
	LOAD      r5, r10, 1
	LOAD      r5, r10, 2
	ADD      r5, r12, r20
	MULI      r5, r5, 3
	ADD      r5, r5, r3
	LOAD      r6, r5, 0
	LOAD      r6, r5, 1
	LOAD      r5, r5, 2
	MUL       r5, r11, r4
	ADD      r4, r5, r20
	ADD      r5, r5, r8
	MULI      r5, r5, 3
	ADD      r5, r5, r3
	LOAD      r6, r5, 0
	LOAD      r6, r5, 1
	LOAD      r5, r5, 2
	MULI      r4, r4, 3
	ADD      r3, r4, r3
	LOAD      r4, r3, 0
	LOAD      r4, r3, 1
	LOAD      r3, r3, 2
	brid      ($BB12_145)
	NOP    
$BB12_143:                              # %_ZL5Floorf.exit291.i237
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r5, r0, 1
	CMP       r9, r5, r7
	ADD      r7, r6, r0
	ADD      r23, r6, r0
	bneid     r9, ($BB12_148)
	ADD      r5, r6, r0
# BB#144:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread.i258
                                        #   in Loop: Header=BB12_43 Depth=2
	LOAD      r5, r10, 0
	ADD      r5, r20, r3
	ADD      r6, r5, r12
	LOAD      r6, r6, 0
	MUL       r4, r11, r4
	ADD      r6, r4, r8
	ADD      r3, r6, r3
	LOAD      r3, r3, 0
	ADD      r3, r5, r4
	LOAD      r3, r3, 0
$BB12_145:                              # %_Z26loadTextureColorFromMemoryii.exit144.thread.i258
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r6, r0, 1065353216
	ADD      r7, r6, r0
	ADD      r23, r6, r0
	ADD      r5, r6, r0
$BB12_148:                              # %_Z15getTextureColorRK9HitRecordRKii.exit409
                                        #   in Loop: Header=BB12_43 Depth=2
	FPADD      r3, r7, r6
	FPADD      r3, r3, r23
	FPADD      r3, r3, r5
	ORI       r4, r0, 1048576000
	FPMUL      r3, r3, r4
	SWI       r3, r1, 248
$BB12_149:                              # %if.end35
                                        #   in Loop: Header=BB12_43 Depth=2
	ORI       r23, r0, 0
	LWI       r3, r1, 248
	FPNE   r4, r3, r23
	bneid     r4, ($BB12_151)
	ADDI      r3, r0, 1
# BB#150:                               # %if.end35
                                        #   in Loop: Header=BB12_43 Depth=2
	ADDI      r3, r0, 0
$BB12_151:                              # %if.end35
                                        #   in Loop: Header=BB12_43 Depth=2
	beqid     r3, ($BB12_290)
	LWI       r4, r1, 212
# BB#152:                               #   in Loop: Header=BB12_1 Depth=1
	SWI       r4, r1, 212
	ADD      r4, r23, r0
	ADD      r3, r23, r0
$BB12_153:                              #   in Loop: Header=BB12_1 Depth=1
	LWI       r22, r1, 220
$BB12_154:                              # %if.end47
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r3, r1, 224
	SWI       r4, r1, 208
	LWI       r11, r1, 360
	LWI       r3, r11, 12
	ADDI      r3, r3, 10
	LOAD      r3, r3, 0
	MULI      r3, r3, 25
	LWI       r4, r22, 4
	ADD      r4, r4, r3
	LOAD      r20, r4, 15
	bltid     r20, ($BB12_202)
	NOP    
# BB#155:                               # %if.then57
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r21, r1, 204
	LWI       r3, r11, 12
	LOAD      r3, r3, 9
	MULI      r6, r3, 9
	LWI       r3, r22, 8
	ADD      r5, r3, r6
	LOAD      r4, r5, 0
	LOAD      r3, r5, 1
	LOAD      r5, r5, 2
	LWI       r5, r22, 8
	ADD      r5, r6, r5
	ADDI      r7, r5, 3
	LOAD      r9, r7, 0
	LOAD      r5, r7, 1
	LOAD      r7, r7, 2
	LWI       r7, r22, 8
	ADD      r6, r6, r7
	ADDI      r7, r6, 6
	LOAD      r8, r7, 0
	LOAD      r6, r7, 1
	LOAD      r7, r7, 2
	LWI        r7, r11, 4
	FPMUL      r10, r9, r7
	LWI        r9, r11, 0
	FPMUL      r4, r4, r9
	FPADD      r4, r4, r10
	ORI       r10, r0, 1065353216
	FPRSUB     r10, r9, r10
	FPRSUB     r10, r7, r10
	FPMUL      r8, r8, r10
	FPADD      r4, r4, r8
	INTCONV      r8, r4
	FPCONV       r8, r8
	ADDI      r12, r0, 1
	FPEQ   r11, r4, r8
	bneid     r11, ($BB12_157)
	NOP    
# BB#156:                               # %if.then57
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r12, r0, 0
$BB12_157:                              # %if.then57
                                        #   in Loop: Header=BB12_1 Depth=1
	FPMUL      r5, r5, r7
	FPMUL      r3, r3, r9
	FPADD      r3, r3, r5
	FPMUL      r5, r6, r10
	FPADD      r3, r3, r5
	LOAD      r21, r20, 0
	LOAD      r10, r20, 1
	LOAD      r11, r20, 2
	bneid     r12, ($BB12_159)
	NOP    
# BB#158:                               # %if.then.i108
                                        #   in Loop: Header=BB12_1 Depth=1
	FPRSUB     r4, r8, r4
$BB12_159:                              # %if.end.i109
                                        #   in Loop: Header=BB12_1 Depth=1
	INTCONV      r5, r3
	FPCONV       r5, r5
	FPEQ   r7, r3, r5
	bneid     r7, ($BB12_161)
	ADDI      r6, r0, 1
# BB#160:                               # %if.end.i109
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r6, r0, 0
$BB12_161:                              # %if.end.i109
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r6, ($BB12_163)
	NOP    
# BB#162:                               # %if.then32.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPRSUB     r3, r5, r3
$BB12_163:                              # %if.end36.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r5, r0, 0
	FPGE   r6, r4, r5
	FPUN   r5, r4, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_165)
	ADDI      r5, r0, 1
# BB#164:                               # %if.end36.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r5, r0, 0
$BB12_165:                              # %if.end36.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r5, ($BB12_167)
	NOP    
# BB#166:                               # %if.then38.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r5, r0, 1065353216
	FPADD      r4, r4, r5
$BB12_167:                              # %if.end41.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r5, r0, 0
	FPGE   r6, r3, r5
	FPUN   r5, r3, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_169)
	ADDI      r5, r0, 1
# BB#168:                               # %if.end41.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r5, r0, 0
$BB12_169:                              # %if.end41.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r5, ($BB12_171)
	NOP    
# BB#170:                               # %if.then43.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r5, r0, 1065353216
	FPADD      r3, r3, r5
$BB12_171:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPCONV       r5, r10
	FPMUL      r7, r4, r5
	INTCONV      r4, r7
	CMP       r5, r10, r4
	ADDI      r6, r0, 0
	ADDI      r4, r0, 1
	beqid     r5, ($BB12_173)
	ADD      r8, r4, r0
# BB#172:                               # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r8, r6, r0
$BB12_173:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r5, r0, 0
	bneid     r8, ($BB12_175)
	ADD      r9, r5, r0
# BB#174:                               # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r9, r7, r0
$BB12_175:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPGE   r7, r9, r5
	FPUN   r8, r9, r5
	BITOR        r12, r8, r7
	FPCONV       r7, r21
	FPMUL      r3, r3, r7
	INTCONV      r7, r3
	CMP       r8, r21, r7
	bneid     r12, ($BB12_177)
	ADD      r7, r4, r0
# BB#176:                               # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r7, r6, r0
$BB12_177:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	beqid     r8, ($BB12_179)
	NOP    
# BB#178:                               # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r4, r6, r0
$BB12_179:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r4, ($BB12_181)
	NOP    
# BB#180:                               # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r5, r3, r0
$BB12_181:                              # %if.end46.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r7, ($BB12_186)
	NOP    
# BB#182:                               # %if.then.i.i112
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r3, r9
	INTCONV      r4, r3
	RSUBI     r3, r4, 0
	FPCONV       r6, r3
	FPEQ   r7, r6, r9
	bneid     r7, ($BB12_184)
	ADDI      r6, r0, 1
# BB#183:                               # %if.then.i.i112
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r6, r0, 0
$BB12_184:                              # %if.then.i.i112
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r6, ($BB12_187)
	NOP    
# BB#185:                               # %if.else.i.i114
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, -1
	brid      ($BB12_187)
	BITXOR       r3, r4, r3
$BB12_202:                              # %if.else61
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r21, r1, 204
	ORI       r3, r0, 1065353216
	LOAD      r8, r4, 4
	LOAD      r9, r4, 5
	LOAD      r4, r4, 6
	brid      ($BB12_203)
	NOP    
$BB12_186:                              # %if.else6.i.i
                                        #   in Loop: Header=BB12_1 Depth=1
	INTCONV      r3, r9
$BB12_187:                              # %_ZL5Floorf.exit.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ORI       r4, r0, 0
	FPGE   r6, r5, r4
	FPUN   r4, r5, r4
	BITOR        r6, r4, r6
	bneid     r6, ($BB12_189)
	ADDI      r4, r0, 1
# BB#188:                               # %_ZL5Floorf.exit.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r4, r0, 0
$BB12_189:                              # %_ZL5Floorf.exit.i
                                        #   in Loop: Header=BB12_1 Depth=1
	DIV      r6, r10, r3
	MUL       r6, r6, r10
	RSUB     r3, r6, r3
	bsrai     r6, r3, 31
	BITAND       r6, r6, r10
	ADD      r26, r6, r3
	ADDI      r3, r26, 1
	DIV      r6, r10, r3
	MUL       r6, r6, r10
	bneid     r4, ($BB12_194)
	RSUB     r7, r6, r3
# BB#190:                               # %if.then.i285.i
                                        #   in Loop: Header=BB12_1 Depth=1
	FPNEG      r3, r5
	INTCONV      r4, r3
	RSUBI     r3, r4, 0
	FPCONV       r6, r3
	FPEQ   r8, r6, r5
	bneid     r8, ($BB12_192)
	ADDI      r6, r0, 1
# BB#191:                               # %if.then.i285.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r6, r0, 0
$BB12_192:                              # %if.then.i285.i
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r6, ($BB12_195)
	NOP    
# BB#193:                               # %if.else.i287.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, -1
	brid      ($BB12_195)
	BITXOR       r3, r4, r3
$BB12_194:                              # %if.else6.i289.i
                                        #   in Loop: Header=BB12_1 Depth=1
	INTCONV      r3, r5
$BB12_195:                              # %_ZL5Floorf.exit291.i
                                        #   in Loop: Header=BB12_1 Depth=1
	DIV      r4, r21, r3
	MUL       r4, r4, r21
	RSUB     r3, r4, r3
	bsrai     r4, r3, 31
	BITAND       r4, r4, r21
	ADD      r3, r4, r3
	FPCONV       r4, r3
	FPRSUB     r4, r4, r5
	SWI       r4, r1, 228
	FPCONV       r4, r26
	FPRSUB     r31, r4, r9
	MUL       r12, r3, r10
	ADDI      r3, r3, 1
	DIV      r4, r21, r3
	MUL       r4, r4, r21
	RSUB     r24, r4, r3
	ADD      r4, r12, r26
	ADDI      r3, r0, 4
	CMP       r3, r3, r11
	MUL       r4, r4, r11
	ORI       r22, r0, 1065353216
	ORI       r21, r0, 0
	ADDI      r27, r20, 3
	bneid     r3, ($BB12_196)
	ADD      r8, r4, r27
# BB#200:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread186.i
                                        #   in Loop: Header=BB12_1 Depth=1
	MUL       r4, r24, r10
	ADD      r3, r4, r26
	ADD      r4, r4, r7
	ADD      r5, r12, r7
	bslli     r5, r5, 2
	ADD      r5, r5, r27
	bslli     r6, r4, 2
	bslli     r3, r3, 2
	ADD      r4, r3, r27
	ADD      r3, r6, r27
	LOAD      r6, r8, 1
	LOAD      r9, r8, 2
	LOAD      r10, r8, 3
	LOAD      r7, r8, 0
	LOAD      r11, r5, 1
	LOAD      r12, r5, 2
	LOAD      r8, r5, 3
	LOAD      r5, r5, 0
	FPCONV       r10, r10
	FPCONV       r20, r9
	FPCONV       r9, r12
	FPCONV       r12, r11
	FPCONV       r21, r7
	FPCONV       r24, r6
	ORI       r6, r0, 1132396544
	FPDIV      r7, r20, r6
	SWI       r7, r1, 180
	FPDIV      r11, r10, r6
	LOAD      r7, r4, 1
	LOAD      r10, r4, 2
	LOAD      r20, r4, 3
	LOAD      r22, r4, 0
	FPCONV       r4, r8
	FPDIV      r8, r24, r6
	SWI       r8, r1, 192
	FPDIV      r8, r21, r6
	SWI       r8, r1, 184
	FPDIV      r21, r12, r6
	FPDIV      r8, r9, r6
	SWI       r8, r1, 188
	LOAD      r9, r3, 1
	LOAD      r8, r3, 2
	FPCONV       r8, r8
	FPCONV       r9, r9
	FPCONV       r12, r22
	FPCONV       r20, r20
	FPCONV       r10, r10
	FPDIV      r4, r4, r6
	LOAD      r22, r3, 3
	LOAD      r24, r3, 0
	FPCONV       r3, r22
	FPCONV       r25, r7
	FPCONV       r5, r5
	FPDIV      r22, r5, r6
	FPCONV       r7, r24
	FPDIV      r29, r25, r6
	FPDIV      r5, r10, r6
	FPDIV      r25, r20, r6
	FPDIV      r28, r12, r6
	FPDIV      r20, r9, r6
	FPDIV      r9, r8, r6
	FPDIV      r3, r3, r6
	brid      ($BB12_201)
	FPDIV      r6, r7, r6
$BB12_196:                              # %_ZL5Floorf.exit291.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, 3
	CMP       r3, r3, r11
	bneid     r3, ($BB12_197)
	NOP    
# BB#199:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread179.i
                                        #   in Loop: Header=BB12_1 Depth=1
	MUL       r3, r24, r10
	ADD      r4, r3, r26
	ADD      r3, r3, r7
	ADD      r5, r12, r7
	MULI      r5, r5, 3
	MULI      r4, r4, 3
	ADD      r6, r4, r27
	ADD      r9, r5, r27
	MULI      r3, r3, 3
	ADD      r4, r3, r27
	LOAD      r3, r8, 0
	LOAD      r7, r8, 1
	LOAD      r20, r8, 2
	LOAD      r5, r9, 0
	LOAD      r8, r9, 1
	LOAD      r11, r9, 2
	LOAD      r12, r6, 0
	LOAD      r10, r6, 1
	LOAD      r6, r6, 2
	FPCONV       r21, r20
	FPCONV       r9, r7
	FPCONV       r7, r6
	ORI       r6, r0, 1132396544
	FPDIV      r9, r9, r6
	SWI       r9, r1, 180
	LOAD      r9, r4, 0
	FPCONV       r9, r9
	FPCONV       r10, r10
	FPCONV       r12, r12
	FPCONV       r20, r11
	FPDIV      r11, r21, r6
	LOAD      r21, r4, 1
	LOAD      r4, r4, 2
	FPCONV       r22, r21
	FPCONV       r8, r8
	FPCONV       r5, r5
	FPCONV       r3, r3
	FPDIV      r3, r3, r6
	SWI       r3, r1, 192
	FPDIV      r21, r5, r6
	FPCONV       r3, r4
	FPDIV      r4, r8, r6
	SWI       r4, r1, 188
	FPDIV      r4, r20, r6
	FPDIV      r29, r12, r6
	FPDIV      r5, r10, r6
	FPDIV      r25, r7, r6
	FPDIV      r20, r9, r6
	FPDIV      r9, r22, r6
	FPDIV      r3, r3, r6
	ORI       r22, r0, 1065353216
	SWI       r22, r1, 184
	ADD      r28, r22, r0
	brid      ($BB12_201)
	ADD      r6, r22, r0
$BB12_197:                              # %_ZL5Floorf.exit291.i
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, 1
	CMP       r30, r3, r11
	SWI       r21, r1, 188
	ADD      r4, r21, r0
	SWI       r22, r1, 184
	ADD      r11, r21, r0
	SWI       r21, r1, 180
	SWI       r21, r1, 192
	ADD      r29, r21, r0
	ADD      r5, r21, r0
	ADD      r25, r21, r0
	ADD      r28, r22, r0
	ADD      r6, r22, r0
	ADD      r3, r21, r0
	ADD      r9, r21, r0
	bneid     r30, ($BB12_201)
	ADD      r20, r21, r0
# BB#198:                               # %_Z26loadTextureColorFromMemoryii.exit144.thread.i
                                        #   in Loop: Header=BB12_1 Depth=1
	MUL       r4, r24, r10
	ADD      r3, r4, r26
	ADD      r3, r3, r27
	ADD      r5, r7, r27
	ADD      r6, r5, r4
	ADD      r4, r5, r12
	LOAD      r7, r8, 0
	LOAD      r5, r4, 0
	LOAD      r4, r3, 0
	LOAD      r3, r6, 0
	FPCONV       r3, r3
	FPCONV       r7, r7
	ORI       r6, r0, 1132396544
	FPDIV      r3, r3, r6
	FPDIV      r11, r7, r6
	FPCONV       r5, r5
	FPDIV      r21, r5, r6
	FPCONV       r4, r4
	FPDIV      r29, r4, r6
	ORI       r22, r0, 1065353216
	SWI       r21, r1, 188
	ADD      r4, r21, r0
	SWI       r22, r1, 184
	SWI       r11, r1, 180
	SWI       r11, r1, 192
	ADD      r5, r29, r0
	ADD      r25, r29, r0
	ADD      r28, r22, r0
	ADD      r6, r22, r0
	ADD      r9, r3, r0
	ADD      r20, r3, r0
$BB12_201:                              # %_Z15getTextureColorRK9HitRecordRKii.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r24, r31, r0
	FPMUL      r4, r4, r24
	ORI       r10, r0, 1065353216
	LWI       r31, r1, 228
	FPRSUB     r7, r31, r10
	FPMUL      r8, r4, r7
	FPRSUB     r4, r24, r10
	FPMUL      r10, r11, r4
	FPMUL      r10, r10, r7
	FPADD      r8, r10, r8
	LWI       r10, r1, 188
	FPMUL      r12, r10, r24
	LWI       r10, r1, 192
	FPMUL      r11, r10, r4
	FPMUL      r10, r21, r24
	FPMUL      r10, r10, r7
	FPMUL      r11, r11, r7
	FPMUL      r12, r12, r7
	LWI       r21, r1, 180
	FPMUL      r21, r21, r4
	FPMUL      r21, r21, r7
	FPMUL      r7, r25, r4
	FPMUL      r7, r7, r31
	FPADD      r7, r8, r7
	FPADD      r8, r21, r12
	FPADD      r10, r11, r10
	FPMUL      r11, r5, r4
	FPMUL      r4, r29, r4
	FPMUL      r4, r4, r31
	FPADD      r5, r10, r4
	FPMUL      r4, r11, r31
	FPADD      r8, r8, r4
	FPMUL      r3, r3, r24
	FPMUL      r3, r3, r31
	FPADD      r4, r7, r3
	LWI       r3, r1, 184
	FPADD      r3, r3, r22
	FPMUL      r7, r9, r24
	FPMUL      r9, r20, r24
	FPMUL      r10, r9, r31
	FPMUL      r7, r7, r31
	FPADD      r9, r8, r7
	FPADD      r8, r5, r10
	FPADD      r3, r3, r28
	FPADD      r3, r3, r6
	ORI       r5, r0, 1048576000
	FPMUL      r3, r3, r5
	LWI       r22, r1, 220
$BB12_203:                              # %if.end64
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI       r5, r1, 380
	SWI        r8, r5, 0
	SWI        r9, r5, 4
	SWI        r4, r5, 8
	SWI        r3, r5, 12
	ORI       r5, r0, 1065353216
	LWI       r12, r1, 288
	FPRSUB     r7, r12, r5
	FPMUL      r5, r3, r7
	FPADD      r6, r12, r5
	ORI       r21, r0, 0
	FPEQ   r5, r6, r21
	ADDI      r10, r0, 1
	LWI       r11, r1, 284
	bneid     r5, ($BB12_205)
	LWI       r20, r1, 292
# BB#204:                               # %if.end64
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r10, r0, 0
$BB12_205:                              # %if.end64
                                        #   in Loop: Header=BB12_1 Depth=1
	ADD      r24, r21, r0
	ADD      r25, r21, r0
	bneid     r10, ($BB12_207)
	ADD      r5, r21, r0
# BB#206:                               # %if.end.i182
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI       r5, r1, 208
	FPMUL      r9, r5, r9
	LWI       r5, r1, 224
	FPMUL      r8, r5, r8
	LWI       r5, r1, 296
	FPMUL      r5, r5, r12
	FPMUL      r8, r8, r3
	FPMUL      r8, r8, r7
	FPADD      r5, r5, r8
	FPMUL      r8, r20, r12
	FPMUL      r9, r9, r3
	FPMUL      r9, r9, r7
	FPADD      r8, r8, r9
	FPMUL      r9, r11, r12
	FPMUL      r4, r23, r4
	FPMUL      r4, r4, r3
	FPMUL      r4, r4, r7
	FPADD      r4, r9, r4
	FPDIV      r24, r4, r6
	FPDIV      r25, r8, r6
	FPDIV      r5, r5, r6
	ADD      r21, r6, r0
$BB12_207:                              # %_ZN5Color5blendES_.exit183
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r5, r1, 228
	LWI       r4, r1, 356
	SWI        r5, r4, 0
	SWI        r25, r4, 4
	SWI        r24, r4, 8
	SWI        r21, r4, 12
	ORI       r4, r0, 1065353216
	FPGE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r4, r3, r5
	bneid     r4, ($BB12_209)
	ADDI      r3, r0, 1
# BB#208:                               # %_ZN5Color5blendES_.exit183
                                        #   in Loop: Header=BB12_1 Depth=1
	ADDI      r3, r0, 0
$BB12_209:                              # %_ZN5Color5blendES_.exit183
                                        #   in Loop: Header=BB12_1 Depth=1
	bneid     r3, ($BB12_210)
	SWI       r21, r1, 192
# BB#211:                               # %if.then72
                                        #   in Loop: Header=BB12_1 Depth=1
	SWI       r25, r1, 224
	SWI       r24, r1, 208
	LWI       r31, r1, 360
	SWI       r0, r31, 12
	ADDI      r3, r0, 1259902592
	SWI       r3, r31, 8
	ADD      r11, r0, r0
	sbi       r11, r31, 16
	sbi       r11, r31, 17
	ADD      r6, r31, r0
	LWI       r5, r1, 372
	LWI        r3, r5, 4
	LWI       r4, r1, 308
	FPRSUB     r4, r4, r3
	LWI        r3, r5, 8
	LWI       r7, r1, 304
	FPRSUB     r3, r7, r3
	LWI       r7, r1, 336
	FPADD      r3, r3, r7
	SWI       r3, r1, 180
	LWI       r3, r1, 340
	FPADD      r3, r4, r3
	SWI       r3, r1, 184
	LWI        r5, r5, 0
	LWI       r3, r1, 300
	FPRSUB     r5, r3, r5
	LWI       r3, r1, 344
	FPADD      r9, r5, r3
	brid      ($BB12_212)
	ADD      r4, r11, r0
$BB12_257:                              # %if.end19.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bslli     r3, r3, 2
	ADDI      r7, r1, 52
	ADD      r3, r7, r3
	LWI       r11, r3, -4
	ADD      r4, r5, r0
	LWI       r22, r1, 220
$BB12_212:                              # %while.body.i
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_232 Depth 3
	SWI       r4, r1, 188
	bslli     r5, r11, 3
	LWI       r3, r22, 0
	ADD      r23, r3, r5
	LOAD      r3, r23, 0
	LOAD      r7, r23, 1
	LOAD      r5, r23, 2
	ADDI      r8, r23, 3
	LOAD      r10, r8, 0
	LOAD      r11, r8, 1
	LWI       r4, r1, 184
	FPRSUB     r12, r4, r11
	FPRSUB     r11, r4, r7
	LOAD      r7, r8, 2
	LWI       r4, r1, 180
	FPRSUB     r8, r4, r7
	FPRSUB     r5, r4, r5
	LWI       r4, r1, 268
	FPMUL      r7, r5, r4
	FPMUL      r8, r8, r4
	LWI       r4, r1, 272
	FPMUL      r11, r11, r4
	FPMUL      r12, r12, r4
	FPRSUB     r5, r9, r10
	FPRSUB     r3, r9, r3
	LWI       r4, r1, 276
	FPMUL      r3, r3, r4
	FPMUL      r20, r5, r4
	FPMIN     r5, r3, r20
	FPMIN     r10, r11, r12
	FPMIN     r21, r7, r8
	FPMAX     r3, r3, r20
	FPMAX     r11, r11, r12
	FPMAX     r7, r7, r8
	FPMAX     r8, r10, r21
	FPMAX     r8, r5, r8
	FPMIN     r5, r11, r7
	FPMIN     r10, r3, r5
	FPGE   r5, r8, r10
	FPUN   r7, r8, r10
	BITOR        r7, r7, r5
	bneid     r7, ($BB12_214)
	ADDI      r5, r0, 1
# BB#213:                               # %while.body.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_214:                              # %while.body.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bneid     r5, ($BB12_255)
	ADD      r21, r6, r0
# BB#215:                               # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ORI       r5, r0, 869711765
	FPLE   r7, r8, r5
	FPUN   r5, r8, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB12_217)
	ADDI      r5, r0, 1
# BB#216:                               # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_217:                              # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bneid     r5, ($BB12_221)
	NOP    
# BB#218:                               # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ORI       r5, r0, 1259902592
	FPLT   r7, r8, r5
	bneid     r7, ($BB12_220)
	ADDI      r5, r0, 1
# BB#219:                               # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_220:                              # %if.then.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	beqid     r5, ($BB12_221)
	NOP    
# BB#227:                               # %lor.lhs.false.i
                                        #   in Loop: Header=BB12_212 Depth=2
	LWI        r5, r21, 8
	FPLT   r6, r5, r8
	FPUN   r5, r5, r8
	BITOR        r6, r5, r6
	bneid     r6, ($BB12_229)
	ADDI      r5, r0, 1
# BB#228:                               # %lor.lhs.false.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_229:                              # %lor.lhs.false.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bneid     r5, ($BB12_255)
	NOP    
	brid      ($BB12_230)
	NOP    
$BB12_221:                              # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ORI       r5, r0, 869711765
	FPLE   r7, r10, r5
	FPUN   r5, r10, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB12_223)
	ADDI      r5, r0, 1
# BB#222:                               # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_223:                              # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bneid     r5, ($BB12_255)
	NOP    
# BB#224:                               # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ORI       r5, r0, 1259902592
	FPLT   r6, r10, r5
	bneid     r6, ($BB12_226)
	ADDI      r5, r0, 1
# BB#225:                               # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, 0
$BB12_226:                              # %if.then24.i.i
                                        #   in Loop: Header=BB12_212 Depth=2
	beqid     r5, ($BB12_255)
	NOP    
$BB12_230:                              # %if.then6.i85
                                        #   in Loop: Header=BB12_212 Depth=2
	LOAD      r11, r23, 7
	LOAD      r23, r23, 6
	bgeid     r23, ($BB12_231)
	NOP    
# BB#291:                               # %if.then10.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADD      r6, r21, r0
	ADDI      r5, r11, 1
	LWI       r4, r1, 188
	bslli     r3, r4, 2
	ADDI      r7, r1, 52
	SW        r5, r7, r3
	ADDI      r4, r4, 1
	brid      ($BB12_212)
	LWI       r22, r1, 220
$BB12_231:                              # %for.cond.preheader.i
                                        #   in Loop: Header=BB12_212 Depth=2
	bleid     r23, ($BB12_255)
	ADD      r25, r0, r0
$BB12_232:                              # %for.body.i
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_212 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ADD      r3, r21, r0
	LOAD      r8, r11, 0
	LOAD      r24, r11, 1
	LOAD      r7, r11, 2
	ADDI      r10, r11, 3
	LOAD      r6, r10, 0
	LOAD      r5, r10, 1
	LOAD      r10, r10, 2
	ADDI      r12, r11, 6
	LOAD      r28, r12, 0
	LOAD      r30, r12, 1
	LOAD      r26, r12, 2
	FPRSUB     r27, r26, r10
	FPRSUB     r20, r30, r5
	LWI       r12, r1, 244
	FPMUL      r5, r12, r20
	LWI       r4, r1, 240
	FPMUL      r10, r4, r27
	FPRSUB     r31, r5, r10
	FPRSUB     r29, r28, r6
	FPMUL      r5, r12, r29
	LWI       r22, r1, 236
	FPMUL      r6, r22, r27
	FPRSUB     r10, r6, r5
	FPRSUB     r6, r28, r9
	LWI       r5, r1, 184
	FPRSUB     r12, r30, r5
	FPMUL      r5, r12, r10
	FPMUL      r21, r6, r31
	FPADD      r5, r21, r5
	FPMUL      r21, r4, r29
	FPMUL      r22, r22, r20
	FPRSUB     r21, r21, r22
	FPRSUB     r28, r28, r8
	FPRSUB     r30, r30, r24
	LWI       r4, r1, 180
	FPRSUB     r24, r26, r4
	FPMUL      r8, r24, r21
	FPADD      r5, r5, r8
	FPMUL      r8, r30, r10
	FPMUL      r10, r28, r31
	FPADD      r8, r10, r8
	FPRSUB     r7, r26, r7
	FPMUL      r10, r7, r21
	FPADD      r8, r8, r10
	ORI       r10, r0, 1065353216
	FPDIV      r8, r10, r8
	FPMUL      r26, r5, r8
	ORI       r5, r0, 0
	FPLT   r10, r26, r5
	bneid     r10, ($BB12_234)
	ADDI      r5, r0, 1
# BB#233:                               # %for.body.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_234:                              # %for.body.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	ADD      r21, r3, r0
# BB#235:                               # %for.body.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ORI       r5, r0, 1065353216
	FPGT   r10, r26, r5
	bneid     r10, ($BB12_237)
	ADDI      r5, r0, 1
# BB#236:                               # %for.body.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_237:                              # %for.body.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#238:                               # %if.end.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	FPMUL      r5, r24, r30
	FPMUL      r10, r12, r7
	FPRSUB     r31, r5, r10
	FPMUL      r5, r6, r7
	FPMUL      r7, r24, r28
	FPRSUB     r24, r5, r7
	LWI       r3, r1, 240
	FPMUL      r5, r3, r24
	LWI       r3, r1, 236
	FPMUL      r7, r3, r31
	FPADD      r5, r7, r5
	FPMUL      r7, r12, r28
	FPMUL      r6, r6, r30
	FPRSUB     r7, r7, r6
	LWI       r3, r1, 244
	FPMUL      r6, r3, r7
	FPADD      r5, r5, r6
	FPMUL      r6, r5, r8
	ORI       r5, r0, 0
	FPLT   r10, r6, r5
	bneid     r10, ($BB12_240)
	ADDI      r5, r0, 1
# BB#239:                               # %if.end.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_240:                              # %if.end.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#241:                               # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	FPADD      r5, r6, r26
	ORI       r10, r0, 1065353216
	FPGT   r10, r5, r10
	bneid     r10, ($BB12_243)
	ADDI      r5, r0, 1
# BB#242:                               # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_243:                              # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#244:                               # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	FPMUL      r5, r20, r24
	FPMUL      r10, r29, r31
	FPADD      r5, r10, r5
	FPMUL      r7, r27, r7
	FPADD      r5, r5, r7
	FPMUL      r8, r5, r8
	ORI       r5, r0, 0
	FPLE   r7, r8, r5
	FPUN   r5, r8, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB12_246)
	ADDI      r5, r0, 1
# BB#245:                               # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_246:                              # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#247:                               # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ORI       r5, r0, 869711765
	FPLE   r7, r8, r5
	FPUN   r5, r8, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB12_249)
	ADDI      r5, r0, 1
# BB#248:                               # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_249:                              # %if.end15.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#250:                               # %land.lhs.true.i.i17.i
                                        #   in Loop: Header=BB12_232 Depth=3
	LWI        r5, r21, 8
	FPGE   r7, r8, r5
	FPUN   r5, r8, r5
	BITOR        r7, r5, r7
	bneid     r7, ($BB12_252)
	ADDI      r5, r0, 1
# BB#251:                               # %land.lhs.true.i.i17.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r5, r0, 0
$BB12_252:                              # %land.lhs.true.i.i17.i
                                        #   in Loop: Header=BB12_232 Depth=3
	bneid     r5, ($BB12_254)
	NOP    
# BB#253:                               # %if.then21.i.i
                                        #   in Loop: Header=BB12_232 Depth=3
	SWI        r8, r21, 8
	SWI       r11, r21, 12
	ADDI      r5, r0, 1
	sbi       r5, r21, 16
	ADD      r5, r0, r0
	sbi       r5, r21, 17
	SWI        r26, r21, 0
	SWI        r6, r21, 4
$BB12_254:                              # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i
                                        #   in Loop: Header=BB12_232 Depth=3
	ADDI      r25, r25, 1
	CMP       r5, r23, r25
	bltid     r5, ($BB12_232)
	ADDI      r11, r11, 11
$BB12_255:                              # %if.end16.i
                                        #   in Loop: Header=BB12_212 Depth=2
	ADDI      r5, r0, -1
	LWI       r3, r1, 188
	ADD      r5, r3, r5
	bgeid     r5, ($BB12_257)
	ADD      r6, r21, r0
	brid      ($BB12_256)
	NOP    
$BB12_99:                               # %if.then23
                                        #   in Loop: Header=BB12_1 Depth=1
	LWI       r3, r1, 320
	LWI       r6, r1, 324
	FPMUL      r3, r3, r6
	LWI       r4, r1, 316
	FPMUL      r4, r4, r6
	LWI       r5, r1, 312
	FPMUL      r6, r5, r6
	ORI       r5, r0, 0
	FPADD      r23, r6, r5
	FPADD      r4, r4, r5
	brid      ($BB12_153)
	FPADD      r3, r3, r5
$BB12_210:
	brid      ($BB12_263)
	LWI       r20, r1, 280
$BB12_258:                              # %if.else84
	ORI       r3, r0, 1065353216
	FPRSUB     r4, r12, r3
	LWI        r5, r22, 60
	FPMUL      r3, r5, r4
	FPADD      r3, r12, r3
	ORI       r6, r0, 0
	FPEQ   r7, r3, r6
	bneid     r7, ($BB12_260)
	ADDI      r10, r0, 1
# BB#259:                               # %if.else84
	ADDI      r10, r0, 0
$BB12_260:                              # %if.else84
	ADD      r7, r6, r0
	ADD      r8, r6, r0
	bneid     r10, ($BB12_262)
	ADD      r9, r6, r0
# BB#261:                               # %if.end.i
	FPMUL      r6, r23, r12
	LWI        r7, r22, 48
	FPMUL      r7, r7, r5
	FPMUL      r7, r7, r4
	FPADD      r6, r6, r7
	FPMUL      r7, r21, r12
	LWI        r8, r22, 52
	FPMUL      r8, r8, r5
	FPMUL      r8, r8, r4
	FPADD      r8, r7, r8
	FPMUL      r7, r11, r12
	LWI        r9, r22, 56
	FPMUL      r5, r9, r5
	FPMUL      r4, r5, r4
	FPADD      r4, r7, r4
	FPDIV      r7, r4, r3
	FPDIV      r8, r8, r3
	FPDIV      r9, r6, r3
	ADD      r6, r3, r0
$BB12_262:                              # %_ZN5Color5blendES_.exit
	LWI       r3, r1, 356
	SWI        r9, r3, 0
	SWI        r8, r3, 4
	SWI        r7, r3, 8
	SWI        r6, r3, 12
$BB12_263:                              # %do.body.i
                                        # =>This Inner Loop Header: Depth=1
	RAND      r4
	RAND      r3
	FPADD      r3, r3, r3
	ORI       r5, r0, -1082130432
	FPADD      r3, r3, r5
	FPADD      r4, r4, r4
	FPADD      r4, r4, r5
	FPMUL      r6, r4, r4
	FPMUL      r7, r3, r3
	FPADD      r5, r6, r7
	ORI       r8, r0, 1065353216
	FPGE   r8, r5, r8
	bneid     r8, ($BB12_265)
	ADDI      r5, r0, 1
# BB#264:                               # %do.body.i
                                        #   in Loop: Header=BB12_263 Depth=1
	ADDI      r5, r0, 0
$BB12_265:                              # %do.body.i
                                        #   in Loop: Header=BB12_263 Depth=1
	bneid     r5, ($BB12_263)
	NOP    
# BB#266:                               # %do.end.i
	ORI       r5, r0, 0
	LWI       r9, r1, 332
	FPGE   r8, r9, r5
	FPUN   r5, r9, r5
	BITOR        r5, r5, r8
	bneid     r5, ($BB12_268)
	ADDI      r8, r0, 1
# BB#267:                               # %do.end.i
	ADDI      r8, r0, 0
$BB12_268:                              # %do.end.i
	ORI       r5, r0, 1065353216
	FPRSUB     r6, r6, r5
	FPRSUB     r6, r7, r6
	FPINVSQRT r7, r6
	bneid     r8, ($BB12_270)
	ADD      r6, r9, r0
# BB#269:                               # %cond.true.i55.i.i
	FPNEG      r6, r9
$BB12_270:                              # %_ZL4Fabsf.exit57.i.i
	ADD      r21, r9, r0
	ORI       r8, r0, 0
	LWI       r10, r1, 328
	FPGE   r9, r10, r8
	FPUN   r8, r10, r8
	BITOR        r8, r8, r9
	bneid     r8, ($BB12_272)
	ADDI      r9, r0, 1
# BB#271:                               # %_ZL4Fabsf.exit57.i.i
	ADDI      r9, r0, 0
$BB12_272:                              # %_ZL4Fabsf.exit57.i.i
	bneid     r9, ($BB12_274)
	ADD      r8, r10, r0
# BB#273:                               # %cond.true.i50.i.i
	FPNEG      r8, r10
$BB12_274:                              # %_ZL4Fabsf.exit52.i.i
	ADD      r12, r10, r0
	ORI       r9, r0, 0
	FPGE   r10, r20, r9
	FPUN   r9, r20, r9
	BITOR        r9, r9, r10
	bneid     r9, ($BB12_276)
	ADDI      r10, r0, 1
# BB#275:                               # %_ZL4Fabsf.exit52.i.i
	ADDI      r10, r0, 0
$BB12_276:                              # %_ZL4Fabsf.exit52.i.i
	bneid     r10, ($BB12_278)
	ADD      r9, r20, r0
# BB#277:                               # %cond.true.i.i.i
	FPNEG      r9, r20
$BB12_278:                              # %_ZL4Fabsf.exit.i.i
	FPGE   r10, r6, r8
	FPUN   r11, r6, r8
	BITOR        r10, r11, r10
	bneid     r10, ($BB12_280)
	ADDI      r11, r0, 1
# BB#279:                               # %_ZL4Fabsf.exit.i.i
	ADDI      r11, r0, 0
$BB12_280:                              # %_ZL4Fabsf.exit.i.i
	FPDIV      r5, r5, r7
	ORI       r10, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r11, ($BB12_284)
	NOP    
# BB#281:                               # %_ZL4Fabsf.exit.i.i
	FPLT   r11, r6, r9
	bneid     r11, ($BB12_283)
	ADDI      r6, r0, 1
# BB#282:                               # %_ZL4Fabsf.exit.i.i
	ADDI      r6, r0, 0
$BB12_283:                              # %_ZL4Fabsf.exit.i.i
	bneid     r6, ($BB12_288)
	ADD      r11, r7, r0
$BB12_284:                              # %if.else.i.i
	FPLT   r7, r8, r9
	bneid     r7, ($BB12_286)
	ADDI      r6, r0, 1
# BB#285:                               # %if.else.i.i
	ADDI      r6, r0, 0
$BB12_286:                              # %if.else.i.i
	ORI       r11, r0, 1065353216
	ORI       r7, r0, 0
	bneid     r6, ($BB12_288)
	ADD      r10, r7, r0
# BB#287:                               # %if.else18.i.i
	ORI       r11, r0, 0
	ORI       r7, r0, 1065353216
	ADD      r10, r11, r0
$BB12_288:                              # %_Z16randomReflectionRK6Vector.exit
	FPMUL      r6, r12, r10
	FPMUL      r8, r21, r11
	FPRSUB     r6, r6, r8
	FPMUL      r8, r20, r10
	FPMUL      r9, r21, r7
	FPRSUB     r8, r9, r8
	FPMUL      r9, r20, r8
	FPMUL      r10, r12, r6
	FPRSUB     r9, r9, r10
	FPMUL      r10, r20, r11
	FPMUL      r7, r12, r7
	FPRSUB     r10, r10, r7
	FPMUL      r7, r21, r6
	FPMUL      r11, r20, r10
	FPRSUB     r11, r7, r11
	FPMUL      r7, r9, r3
	FPMUL      r9, r10, r4
	FPADD      r7, r9, r7
	FPMUL      r9, r21, r5
	FPADD      r7, r7, r9
	FPMUL      r9, r11, r3
	FPMUL      r11, r8, r4
	FPADD      r9, r11, r9
	FPMUL      r11, r12, r5
	FPADD      r9, r9, r11
	FPMUL      r10, r12, r10
	FPMUL      r11, r21, r8
	FPMUL      r8, r9, r9
	FPMUL      r12, r7, r7
	FPADD      r8, r12, r8
	FPRSUB     r10, r10, r11
	FPMUL      r3, r10, r3
	FPMUL      r4, r6, r4
	FPADD      r3, r4, r3
	FPMUL      r4, r20, r5
	FPADD      r3, r3, r4
	FPMUL      r4, r3, r3
	FPADD      r4, r8, r4
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r5, r7, r4
	LWI       r6, r1, 376
	SWI        r5, r6, 0
	FPMUL      r5, r9, r4
	SWI        r5, r6, 4
	FPMUL      r3, r3, r4
	SWI        r3, r6, 8
	LWI       r31, r1, 4
	LWI       r30, r1, 8
	LWI       r29, r1, 12
	LWI       r28, r1, 16
	LWI       r27, r1, 20
	LWI       r26, r1, 24
	LWI       r25, r1, 28
	LWI       r24, r1, 32
	LWI       r23, r1, 36
	LWI       r22, r1, 40
	LWI       r21, r1, 44
	LWI       r20, r1, 48
	rtsd      r15, 8
	ADDI      r1, r1, 352
	.end	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
$tmp12:
	.size	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color, ($tmp12)-_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color

	.globl	_Z9trax_mainv
	.align	2
	.type	_Z9trax_mainv,@function
	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
	.frame	r1,400,r15
	.mask	0xfff00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -400
	SWI       r20, r1, 44
	SWI       r21, r1, 40
	SWI       r22, r1, 36
	SWI       r23, r1, 32
	SWI       r24, r1, 28
	SWI       r25, r1, 24
	SWI       r26, r1, 20
	SWI       r27, r1, 16
	SWI       r28, r1, 12
	SWI       r29, r1, 8
	SWI       r30, r1, 4
	SWI       r31, r1, 0
	ADD      r4, r0, r0
	LOAD      r3, r4, 8
	SWI       r3, r1, 188
	LOAD      r3, r4, 12
	LOAD      r5, r3, 0
	SWI       r5, r1, 268
	LOAD      r5, r3, 1
	SWI       r5, r1, 272
	LOAD      r3, r3, 2
	SWI       r3, r1, 276
	LOAD      r8, r4, 1
	SWI       r8, r1, 368
	LOAD      r3, r4, 4
	LOAD      r5, r4, 2
	SWI       r5, r1, 352
	LOAD      r5, r4, 5
	SWI       r5, r1, 356
	LOAD      r5, r4, 7
	SWI       r5, r1, 372
	LOAD      r5, r4, 9
	SWI       r5, r1, 280
	LOAD      r9, r4, 17
	SWI       r9, r1, 292
	LOAD      r5, r4, 16
	SWI       r5, r1, 284
	LOAD      r5, r4, 30
	LOAD      r6, r4, 10
	LOAD      r5, r6, 0
	SWI       r5, r1, 296
	LOAD      r5, r6, 1
	SWI       r5, r1, 300
	LOAD      r5, r6, 2
	SWI       r5, r1, 304
	ADDI      r5, r6, 9
	LOAD      r7, r5, 0
	LOAD      r7, r5, 1
	LOAD      r5, r5, 2
	ADDI      r5, r6, 18
	ADDI      r7, r6, 15
	ADDI      r6, r6, 12
	LOAD      r10, r6, 0
	SWI       r10, r1, 308
	LOAD      r10, r6, 1
	SWI       r10, r1, 312
	LOAD      r6, r6, 2
	SWI       r6, r1, 316
	LOAD      r6, r7, 0
	SWI       r6, r1, 320
	LOAD      r6, r7, 1
	SWI       r6, r1, 324
	LOAD      r6, r7, 2
	SWI       r6, r1, 328
	LOAD      r6, r5, 0
	SWI       r6, r1, 332
	LOAD      r6, r5, 1
	SWI       r6, r1, 336
	LOAD      r5, r5, 2
	SWI       r5, r1, 340
	LOAD      r4, r4, 8
	MUL       r5, r3, r8
	SWI       r5, r1, 376
	ATOMIC_INC r4, 0
	CMP       r5, r5, r4
	bgeid     r5, ($BB13_308)
	NOP    
# BB#1:                                 # %for.body.lr.ph
	FPCONV       r5, r9
	SWI       r5, r1, 380
	FPCONV       r5, r3
	SWI       r5, r1, 384
	ORI       r3, r0, 1056964608
	FPMUL      r5, r5, r3
	SWI       r5, r1, 388
	LWI       r5, r1, 368
	FPCONV       r5, r5
	SWI       r5, r1, 392
	FPMUL      r3, r5, r3
	brid      ($BB13_2)
	SWI       r3, r1, 396
$BB13_5:                                #   in Loop: Header=BB13_2 Depth=1
	SWI       r25, r1, 236
	SWI       r25, r1, 232
$BB13_6:                                # %_ZNK9RayCamera7makeRayER3Rayff.exit
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_7 Depth 3
                                        #         Child Loop BB13_207 Depth 4
                                        #           Child Loop BB13_208 Depth 5
                                        #           Child Loop BB13_228 Depth 5
                                        #         Child Loop BB13_258 Depth 4
                                        #         Child Loop BB13_9 Depth 4
                                        #           Child Loop BB13_170 Depth 5
                                        #           Child Loop BB13_10 Depth 5
	ORI       r3, r0, -1090519040
	RAND      r4
	FPADD      r4, r4, r3
	RAND      r5
	FPADD      r3, r5, r3
	FPADD      r4, r4, r4
	LWI       r5, r1, 352
	FPMUL      r4, r4, r5
	FPADD      r4, r8, r4
	LWI       r5, r1, 320
	FPMUL      r5, r5, r4
	LWI       r6, r1, 308
	FPADD      r5, r6, r5
	FPADD      r3, r3, r3
	LWI       r6, r1, 356
	FPMUL      r3, r3, r6
	FPADD      r6, r7, r3
	LWI       r3, r1, 332
	FPMUL      r3, r3, r6
	FPADD      r3, r5, r3
	LWI       r5, r1, 324
	FPMUL      r5, r5, r4
	LWI       r7, r1, 312
	FPADD      r5, r7, r5
	LWI       r7, r1, 336
	FPMUL      r7, r7, r6
	FPADD      r5, r5, r7
	FPMUL      r7, r5, r5
	FPMUL      r8, r3, r3
	FPADD      r7, r8, r7
	LWI       r8, r1, 340
	FPMUL      r6, r8, r6
	LWI       r8, r1, 328
	FPMUL      r4, r8, r4
	LWI       r8, r1, 316
	FPADD      r4, r8, r4
	FPADD      r4, r4, r6
	FPMUL      r6, r4, r4
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r24, r0, 1065353216
	FPDIV      r6, r24, r6
	FPDIV      r6, r24, r6
	FPMUL      r4, r4, r6
	SWI       r4, r1, 196
	FPMUL      r26, r5, r6
	FPMUL      r3, r3, r6
	SWI       r3, r1, 192
	ADD      r4, r0, r0
	LWI       r3, r1, 304
	SWI       r3, r1, 208
	LWI       r3, r1, 300
	SWI       r3, r1, 212
	LWI       r3, r1, 296
	SWI       r3, r1, 216
	ADD      r27, r24, r0
	brid      ($BB13_7)
	ADD      r28, r24, r0
$BB13_284:                              # %if.end46
                                        #   in Loop: Header=BB13_7 Depth=3
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r24, r24, r10
	FPMUL      r27, r27, r31
	FPMUL      r28, r28, r21
	FPMUL      r5, r9, r4
	SWI       r5, r1, 192
	FPMUL      r26, r11, r4
	FPMUL      r3, r3, r4
	SWI       r3, r1, 196
	LWI       r3, r1, 184
	SWI       r3, r1, 208
	LWI       r3, r1, 176
	SWI       r3, r1, 212
	LWI       r3, r1, 180
	SWI       r3, r1, 216
	LWI       r4, r1, 248
$BB13_7:                                # %while.cond
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_207 Depth 4
                                        #           Child Loop BB13_208 Depth 5
                                        #           Child Loop BB13_228 Depth 5
                                        #         Child Loop BB13_258 Depth 4
                                        #         Child Loop BB13_9 Depth 4
                                        #           Child Loop BB13_170 Depth 5
                                        #           Child Loop BB13_10 Depth 5
	LWI       r3, r1, 284
	CMP       r3, r3, r4
	bgeid     r3, ($BB13_285)
	NOP    
# BB#8:                                 # %while.body
                                        #   in Loop: Header=BB13_7 Depth=3
	SWI       r28, r1, 256
	SWI       r27, r1, 252
	SWI       r24, r1, 244
	SWI       r25, r1, 240
	ADDI      r4, r4, 1
	SWI       r4, r1, 248
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 196
	FPDIV      r4, r3, r4
	SWI       r4, r1, 220
	FPDIV      r4, r3, r26
	SWI       r4, r1, 224
	LWI       r4, r1, 192
	FPDIV      r3, r3, r4
	SWI       r3, r1, 228
	ORI       r29, r0, 1203982336
	ADD      r3, r0, r0
	SWI       r3, r1, 200
	SWI       r3, r1, 204
	brid      ($BB13_9)
	ADD      r10, r3, r0
$BB13_197:                              # %if.end19.i
                                        #   in Loop: Header=BB13_9 Depth=4
	bslli     r4, r31, 2
	ADDI      r5, r1, 48
	ADD      r4, r5, r4
	LWI       r10, r4, -4
$BB13_9:                                # %while.body
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB13_170 Depth 5
                                        #           Child Loop BB13_10 Depth 5
	brid      ($BB13_10)
	ADD      r31, r3, r0
$BB13_311:                              # %if.then10.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r3, r10, 1
	bslli     r4, r31, 2
	ADDI      r5, r1, 48
	SW        r3, r5, r4
	ADDI      r31, r31, 1
$BB13_10:                               # %while.body.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        #         Parent Loop BB13_9 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	bslli     r3, r10, 3
	LWI       r4, r1, 188
	ADD      r7, r3, r4
	LOAD      r3, r7, 0
	LOAD      r5, r7, 1
	LOAD      r4, r7, 2
	ADDI      r10, r7, 3
	LOAD      r8, r10, 0
	LOAD      r6, r10, 1
	LWI       r9, r1, 212
	FPRSUB     r6, r9, r6
	FPRSUB     r9, r9, r5
	LOAD      r5, r10, 2
	LWI       r10, r1, 208
	FPRSUB     r5, r10, r5
	FPRSUB     r4, r10, r4
	LWI       r10, r1, 220
	FPMUL      r4, r4, r10
	FPMUL      r5, r5, r10
	LWI       r11, r1, 224
	FPMUL      r10, r9, r11
	FPMUL      r6, r6, r11
	LWI       r11, r1, 216
	FPRSUB     r9, r11, r8
	FPRSUB     r3, r11, r3
	LWI       r11, r1, 228
	FPMUL      r8, r3, r11
	FPMUL      r9, r9, r11
	FPMIN     r3, r8, r9
	FPMIN     r11, r10, r6
	FPMIN     r12, r4, r5
	FPMAX     r9, r8, r9
	FPMAX     r6, r10, r6
	FPMAX     r4, r4, r5
	FPMAX     r5, r11, r12
	FPMAX     r8, r3, r5
	FPMIN     r3, r6, r4
	FPMIN     r3, r9, r3
	FPGE   r4, r8, r3
	FPUN   r5, r8, r3
	BITOR        r5, r5, r4
	bneid     r5, ($BB13_12)
	ADDI      r4, r0, 1
# BB#11:                                # %while.body.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r4, r0, 0
$BB13_12:                               # %while.body.i
                                        #   in Loop: Header=BB13_10 Depth=5
	bneid     r4, ($BB13_196)
	NOP    
# BB#13:                                # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	ORI       r4, r0, 869711765
	FPLE   r5, r8, r4
	FPUN   r4, r8, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_15)
	ADDI      r4, r0, 1
# BB#14:                                # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r4, r0, 0
$BB13_15:                               # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	bneid     r4, ($BB13_19)
	NOP    
# BB#16:                                # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	ORI       r4, r0, 1259902592
	FPLT   r5, r8, r4
	bneid     r5, ($BB13_18)
	ADDI      r4, r0, 1
# BB#17:                                # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r4, r0, 0
$BB13_18:                               # %if.then.i.i30
                                        #   in Loop: Header=BB13_10 Depth=5
	beqid     r4, ($BB13_19)
	NOP    
# BB#165:                               # %lor.lhs.false.i
                                        #   in Loop: Header=BB13_10 Depth=5
	FPLT   r3, r29, r8
	FPUN   r4, r29, r8
	BITOR        r4, r4, r3
	bneid     r4, ($BB13_167)
	ADDI      r3, r0, 1
# BB#166:                               # %lor.lhs.false.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r3, r0, 0
$BB13_167:                              # %lor.lhs.false.i
                                        #   in Loop: Header=BB13_10 Depth=5
	bneid     r3, ($BB13_196)
	NOP    
	brid      ($BB13_168)
	NOP    
$BB13_19:                               # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ORI       r4, r0, 869711765
	FPLE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_21)
	ADDI      r4, r0, 1
# BB#20:                                # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r4, r0, 0
$BB13_21:                               # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	bneid     r4, ($BB13_196)
	NOP    
# BB#22:                                # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ORI       r4, r0, 1259902592
	FPLT   r4, r3, r4
	bneid     r4, ($BB13_24)
	ADDI      r3, r0, 1
# BB#23:                                # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	ADDI      r3, r0, 0
$BB13_24:                               # %if.then24.i.i
                                        #   in Loop: Header=BB13_10 Depth=5
	beqid     r3, ($BB13_196)
	NOP    
$BB13_168:                              # %if.then6.i
                                        #   in Loop: Header=BB13_10 Depth=5
	LOAD      r10, r7, 7
	LOAD      r8, r7, 6
	bltid     r8, ($BB13_311)
	NOP    
# BB#169:                               # %for.cond.preheader.i
                                        #   in Loop: Header=BB13_9 Depth=4
	bleid     r8, ($BB13_196)
	ADD      r7, r0, r0
$BB13_170:                              # %for.body.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        #         Parent Loop BB13_9 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	LOAD      r4, r10, 0
	LOAD      r12, r10, 1
	LOAD      r20, r10, 2
	ADDI      r6, r10, 3
	LOAD      r3, r6, 0
	LOAD      r5, r6, 1
	LOAD      r6, r6, 2
	ADDI      r9, r10, 6
	LOAD      r23, r9, 0
	LOAD      r27, r9, 1
	LOAD      r22, r9, 2
	FPRSUB     r21, r22, r6
	FPRSUB     r30, r27, r5
	LWI       r9, r1, 196
	FPMUL      r5, r9, r30
	FPMUL      r6, r26, r21
	FPRSUB     r28, r5, r6
	FPRSUB     r3, r23, r3
	FPMUL      r5, r9, r3
	LWI       r24, r1, 192
	FPMUL      r6, r24, r21
	FPRSUB     r5, r6, r5
	LWI       r6, r1, 216
	FPRSUB     r25, r23, r6
	LWI       r6, r1, 212
	FPRSUB     r11, r27, r6
	FPMUL      r6, r11, r5
	FPMUL      r9, r25, r28
	FPADD      r9, r9, r6
	FPMUL      r6, r26, r3
	FPMUL      r24, r24, r30
	FPRSUB     r6, r6, r24
	FPRSUB     r23, r23, r4
	FPRSUB     r12, r27, r12
	LWI       r4, r1, 208
	FPRSUB     r27, r22, r4
	FPMUL      r4, r27, r6
	FPADD      r9, r9, r4
	FPMUL      r4, r12, r5
	FPMUL      r5, r23, r28
	FPADD      r5, r5, r4
	FPRSUB     r4, r22, r20
	FPMUL      r6, r4, r6
	FPADD      r5, r5, r6
	ORI       r6, r0, 1065353216
	FPDIV      r20, r6, r5
	FPMUL      r22, r9, r20
	ORI       r5, r0, 0
	FPLT   r6, r22, r5
	bneid     r6, ($BB13_172)
	ADDI      r5, r0, 1
# BB#171:                               # %for.body.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADDI      r5, r0, 0
$BB13_172:                              # %for.body.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r5, ($BB13_195)
	NOP    
# BB#173:                               # %for.body.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ORI       r5, r0, 1065353216
	FPGT   r6, r22, r5
	bneid     r6, ($BB13_175)
	ADDI      r5, r0, 1
# BB#174:                               # %for.body.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADDI      r5, r0, 0
$BB13_175:                              # %for.body.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r5, ($BB13_195)
	NOP    
# BB#176:                               # %if.end.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	FPMUL      r5, r27, r12
	FPMUL      r6, r11, r4
	FPRSUB     r28, r5, r6
	FPMUL      r4, r25, r4
	FPMUL      r5, r27, r23
	FPRSUB     r27, r4, r5
	FPMUL      r4, r26, r27
	LWI       r5, r1, 192
	FPMUL      r5, r5, r28
	FPADD      r5, r5, r4
	FPMUL      r4, r11, r23
	FPMUL      r6, r25, r12
	FPRSUB     r4, r4, r6
	LWI       r6, r1, 196
	FPMUL      r6, r6, r4
	FPADD      r5, r5, r6
	FPMUL      r5, r5, r20
	ORI       r6, r0, 0
	FPLT   r9, r5, r6
	bneid     r9, ($BB13_178)
	ADDI      r6, r0, 1
# BB#177:                               # %if.end.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADDI      r6, r0, 0
$BB13_178:                              # %if.end.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r6, ($BB13_195)
	NOP    
# BB#179:                               # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	FPADD      r5, r5, r22
	ORI       r6, r0, 1065353216
	FPGT   r6, r5, r6
	bneid     r6, ($BB13_181)
	ADDI      r5, r0, 1
# BB#180:                               # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADDI      r5, r0, 0
$BB13_181:                              # %lor.lhs.false12.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r5, ($BB13_195)
	NOP    
# BB#182:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	FPMUL      r5, r30, r27
	FPMUL      r3, r3, r28
	FPADD      r3, r3, r5
	FPMUL      r4, r21, r4
	FPADD      r3, r3, r4
	FPMUL      r3, r3, r20
	ORI       r4, r0, 869711765
	FPGT   r6, r3, r4
	ADDI      r4, r0, 0
	ADDI      r11, r0, 1
	bneid     r6, ($BB13_184)
	ADD      r5, r11, r0
# BB#183:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADD      r5, r4, r0
$BB13_184:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ORI       r6, r0, 0
	FPGT   r9, r3, r6
	bneid     r9, ($BB13_186)
	ADD      r6, r11, r0
# BB#185:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADD      r6, r4, r0
$BB13_186:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	BITAND       r5, r6, r5
	FPLT   r9, r3, r29
	bneid     r9, ($BB13_188)
	ADD      r6, r11, r0
# BB#187:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADD      r6, r4, r0
$BB13_188:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	BITAND       r4, r5, r6
	bneid     r4, ($BB13_190)
	NOP    
# BB#189:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADD      r3, r29, r0
$BB13_190:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r4, ($BB13_192)
	ADD      r5, r10, r0
# BB#191:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	LWI       r5, r1, 204
$BB13_192:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	bneid     r4, ($BB13_194)
	NOP    
# BB#193:                               # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	LWI       r11, r1, 200
$BB13_194:                              # %if.end15.i.i
                                        #   in Loop: Header=BB13_170 Depth=5
	SWI       r11, r1, 200
	SWI       r5, r1, 204
	ADD      r29, r3, r0
$BB13_195:                              # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i
                                        #   in Loop: Header=BB13_170 Depth=5
	ADDI      r7, r7, 1
	CMP       r3, r8, r7
	bltid     r3, ($BB13_170)
	ADDI      r10, r10, 11
$BB13_196:                              # %if.end16.i
                                        #   in Loop: Header=BB13_9 Depth=4
	ADDI      r3, r0, -1
	ADD      r3, r31, r3
	bgeid     r3, ($BB13_197)
	NOP    
# BB#198:                               # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r10, r0, 1065353216
	ORI       r8, r0, 1057988018
	ORI       r7, r0, 1060806590
	ORI       r30, r0, 1065151889
	LWI       r3, r1, 200
	ANDI      r3, r3, 255
	ADD      r31, r10, r0
	beqid     r3, ($BB13_257)
	ADD      r21, r10, r0
# BB#199:                               # %if.then.i
                                        #   in Loop: Header=BB13_7 Depth=3
	LWI       r10, r1, 204
	LOAD      r3, r10, 0
	LOAD      r6, r10, 1
	LOAD      r4, r10, 2
	ADDI      r7, r10, 3
	LOAD      r5, r7, 0
	LOAD      r8, r7, 1
	LOAD      r9, r7, 2
	ADDI      r11, r10, 6
	LOAD      r10, r11, 0
	LOAD      r12, r11, 1
	FPRSUB     r7, r12, r6
	FPRSUB     r8, r12, r8
	LOAD      r6, r11, 2
	FPRSUB     r9, r6, r9
	FPRSUB     r11, r6, r4
	FPMUL      r4, r11, r8
	FPMUL      r6, r7, r9
	FPRSUB     r4, r4, r6
	FPRSUB     r5, r10, r5
	FPRSUB     r6, r10, r3
	FPMUL      r3, r6, r9
	FPMUL      r9, r11, r5
	FPRSUB     r3, r3, r9
	FPMUL      r9, r3, r3
	FPMUL      r10, r4, r4
	FPADD      r9, r10, r9
	FPMUL      r5, r7, r5
	FPMUL      r6, r6, r8
	FPRSUB     r5, r5, r6
	FPMUL      r6, r5, r5
	FPADD      r6, r9, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r6, r7, r6
	FPDIV      r6, r7, r6
	FPMUL      r11, r4, r6
	FPMUL      r12, r3, r6
	FPMUL      r3, r12, r26
	LWI       r4, r1, 192
	FPMUL      r4, r11, r4
	FPADD      r3, r4, r3
	FPMUL      r10, r5, r6
	LWI       r4, r1, 196
	FPMUL      r4, r10, r4
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r3, r3, r5
	bneid     r3, ($BB13_201)
	ADDI      r7, r0, 1
# BB#200:                               # %if.then.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r7, r0, 0
$BB13_201:                              # %if.then.i
                                        #   in Loop: Header=BB13_7 Depth=3
	LWI       r3, r1, 196
	FPMUL      r3, r3, r29
	LWI       r4, r1, 208
	FPADD      r3, r4, r3
	FPMUL      r4, r26, r29
	LWI       r5, r1, 212
	FPADD      r5, r5, r4
	LWI       r4, r1, 192
	FPMUL      r4, r4, r29
	LWI       r6, r1, 216
	bneid     r7, ($BB13_203)
	FPADD      r4, r6, r4
# BB#202:                               # %if.then11.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPNEG      r10, r10
	FPNEG      r12, r12
	FPNEG      r11, r11
$BB13_203:                              # %if.then.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r6, r0, 981668463
	FPMUL      r7, r12, r6
	FPADD      r7, r5, r7
	SWI       r7, r1, 176
	FPMUL      r5, r11, r6
	FPADD      r5, r4, r5
	SWI       r5, r1, 180
	LWI       r4, r1, 268
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 272
	FPRSUB     r5, r7, r5
	FPMUL      r7, r5, r5
	FPMUL      r8, r4, r4
	FPADD      r7, r8, r7
	FPMUL      r6, r10, r6
	FPADD      r6, r3, r6
	SWI       r6, r1, 184
	LWI       r3, r1, 276
	FPRSUB     r3, r6, r3
	FPMUL      r6, r3, r3
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r24, r7, r6
	FPDIV      r6, r7, r24
	FPMUL      r9, r5, r6
	FPMUL      r5, r12, r9
	FPMUL      r4, r4, r6
	SWI       r4, r1, 192
	FPMUL      r4, r11, r4
	FPADD      r4, r4, r5
	FPMUL      r3, r3, r6
	SWI       r3, r1, 196
	FPMUL      r3, r10, r3
	FPADD      r5, r4, r3
	SWI       r5, r1, 264
	ORI       r3, r0, 0
	FPLE   r4, r5, r3
	FPUN   r5, r5, r3
	BITOR        r5, r5, r4
	bneid     r5, ($BB13_205)
	ADDI      r4, r0, 1
# BB#204:                               # %if.then.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r4, r0, 0
$BB13_205:                              # %if.then.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	SWI       r12, r1, 260
	SWI       r11, r1, 228
	bneid     r4, ($BB13_256)
	SWI       r10, r1, 224
# BB#206:                               # %if.then20.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 196
	FPDIV      r4, r3, r4
	SWI       r4, r1, 212
	FPDIV      r4, r3, r9
	SWI       r4, r1, 216
	LWI       r4, r1, 192
	FPDIV      r3, r3, r4
	SWI       r3, r1, 220
	ADD      r3, r0, r0
	SWI       r3, r1, 208
	brid      ($BB13_207)
	ADD      r10, r3, r0
$BB13_253:                              # %if.end19.i.i
                                        #   in Loop: Header=BB13_207 Depth=4
	bslli     r4, r21, 2
	ADDI      r5, r1, 48
	ADD      r4, r5, r4
	LWI       r10, r4, -4
$BB13_207:                              # %if.then20.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB13_208 Depth 5
                                        #           Child Loop BB13_228 Depth 5
	brid      ($BB13_208)
	ADD      r21, r3, r0
$BB13_312:                              # %if.then10.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r3, r10, 1
	bslli     r4, r21, 2
	ADDI      r5, r1, 48
	SW        r3, r5, r4
	ADDI      r21, r21, 1
$BB13_208:                              # %while.body.i.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        #         Parent Loop BB13_207 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	bslli     r3, r10, 3
	LWI       r4, r1, 188
	ADD      r7, r3, r4
	LOAD      r3, r7, 0
	LOAD      r6, r7, 1
	LOAD      r4, r7, 2
	ADDI      r8, r7, 3
	LOAD      r10, r8, 0
	LOAD      r5, r8, 1
	LWI       r11, r1, 176
	FPRSUB     r5, r11, r5
	FPRSUB     r6, r11, r6
	LOAD      r8, r8, 2
	LWI       r11, r1, 184
	FPRSUB     r8, r11, r8
	FPRSUB     r4, r11, r4
	LWI       r11, r1, 212
	FPMUL      r4, r4, r11
	FPMUL      r8, r8, r11
	LWI       r12, r1, 216
	FPMUL      r11, r6, r12
	FPMUL      r5, r5, r12
	LWI       r6, r1, 180
	FPRSUB     r10, r6, r10
	FPRSUB     r3, r6, r3
	LWI       r12, r1, 220
	FPMUL      r6, r3, r12
	FPMUL      r12, r10, r12
	FPMIN     r3, r6, r12
	FPMIN     r10, r11, r5
	FPMIN     r20, r4, r8
	FPMAX     r6, r6, r12
	FPMAX     r5, r11, r5
	FPMAX     r8, r4, r8
	FPMAX     r4, r10, r20
	FPMAX     r4, r3, r4
	FPMIN     r3, r5, r8
	FPMIN     r3, r6, r3
	FPGE   r5, r4, r3
	FPUN   r6, r4, r3
	BITOR        r6, r6, r5
	bneid     r6, ($BB13_210)
	ADDI      r5, r0, 1
# BB#209:                               # %while.body.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r5, r0, 0
$BB13_210:                              # %while.body.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	bneid     r5, ($BB13_252)
	NOP    
# BB#211:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ORI       r5, r0, 869711765
	FPLE   r6, r4, r5
	FPUN   r5, r4, r5
	BITOR        r6, r5, r6
	bneid     r6, ($BB13_213)
	ADDI      r5, r0, 1
# BB#212:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r5, r0, 0
$BB13_213:                              # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	bneid     r5, ($BB13_217)
	NOP    
# BB#214:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ORI       r5, r0, 1259902592
	FPLT   r6, r4, r5
	bneid     r6, ($BB13_216)
	ADDI      r5, r0, 1
# BB#215:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r5, r0, 0
$BB13_216:                              # %if.then.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	beqid     r5, ($BB13_217)
	NOP    
# BB#223:                               # %lor.lhs.false.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	FPLT   r3, r24, r4
	FPUN   r4, r24, r4
	BITOR        r4, r4, r3
	bneid     r4, ($BB13_225)
	ADDI      r3, r0, 1
# BB#224:                               # %lor.lhs.false.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r3, r0, 0
$BB13_225:                              # %lor.lhs.false.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	bneid     r3, ($BB13_252)
	NOP    
	brid      ($BB13_226)
	NOP    
$BB13_217:                              # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ORI       r4, r0, 869711765
	FPLE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_219)
	ADDI      r4, r0, 1
# BB#218:                               # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r4, r0, 0
$BB13_219:                              # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	bneid     r4, ($BB13_252)
	NOP    
# BB#220:                               # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ORI       r4, r0, 1259902592
	FPLT   r4, r3, r4
	bneid     r4, ($BB13_222)
	ADDI      r3, r0, 1
# BB#221:                               # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	ADDI      r3, r0, 0
$BB13_222:                              # %if.then24.i.i.i
                                        #   in Loop: Header=BB13_208 Depth=5
	beqid     r3, ($BB13_252)
	NOP    
$BB13_226:                              # %if.then6.i84.i
                                        #   in Loop: Header=BB13_208 Depth=5
	LOAD      r10, r7, 7
	LOAD      r7, r7, 6
	bltid     r7, ($BB13_312)
	NOP    
# BB#227:                               # %for.cond.preheader.i.i
                                        #   in Loop: Header=BB13_207 Depth=4
	bleid     r7, ($BB13_252)
	ADD      r30, r0, r0
$BB13_228:                              # %for.body.i.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        #         Parent Loop BB13_207 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	LOAD      r22, r10, 0
	LOAD      r26, r10, 1
	LOAD      r20, r10, 2
	ADDI      r4, r10, 3
	LOAD      r3, r4, 0
	LOAD      r5, r4, 1
	LOAD      r4, r4, 2
	ADDI      r6, r10, 6
	LOAD      r23, r6, 0
	LOAD      r27, r6, 1
	LOAD      r29, r6, 2
	FPRSUB     r4, r29, r4
	FPRSUB     r12, r27, r5
	LWI       r8, r1, 196
	FPMUL      r5, r8, r12
	FPMUL      r6, r9, r4
	FPRSUB     r31, r5, r6
	FPRSUB     r3, r23, r3
	FPMUL      r5, r8, r3
	LWI       r28, r1, 192
	FPMUL      r6, r28, r4
	FPRSUB     r8, r6, r5
	LWI       r5, r1, 180
	FPRSUB     r25, r23, r5
	LWI       r5, r1, 176
	FPRSUB     r11, r27, r5
	FPMUL      r5, r11, r8
	FPMUL      r6, r25, r31
	FPADD      r6, r6, r5
	FPMUL      r5, r9, r3
	FPMUL      r28, r28, r12
	FPRSUB     r5, r5, r28
	FPRSUB     r23, r23, r22
	FPRSUB     r27, r27, r26
	LWI       r22, r1, 184
	FPRSUB     r28, r29, r22
	FPMUL      r22, r28, r5
	FPADD      r22, r6, r22
	FPMUL      r6, r27, r8
	FPMUL      r8, r23, r31
	FPADD      r6, r8, r6
	FPRSUB     r29, r29, r20
	FPMUL      r5, r29, r5
	FPADD      r5, r6, r5
	ORI       r6, r0, 1065353216
	FPDIV      r20, r6, r5
	FPMUL      r22, r22, r20
	ORI       r5, r0, 0
	FPLT   r6, r22, r5
	bneid     r6, ($BB13_230)
	ADDI      r5, r0, 1
# BB#229:                               # %for.body.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADDI      r5, r0, 0
$BB13_230:                              # %for.body.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	bneid     r5, ($BB13_251)
	NOP    
# BB#231:                               # %for.body.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ORI       r5, r0, 1065353216
	FPGT   r6, r22, r5
	bneid     r6, ($BB13_233)
	ADDI      r5, r0, 1
# BB#232:                               # %for.body.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADDI      r5, r0, 0
$BB13_233:                              # %for.body.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	bneid     r5, ($BB13_251)
	NOP    
# BB#234:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	FPMUL      r5, r28, r27
	FPMUL      r6, r11, r29
	FPRSUB     r26, r5, r6
	FPMUL      r5, r25, r29
	FPMUL      r6, r28, r23
	FPRSUB     r28, r5, r6
	FPMUL      r5, r9, r28
	LWI       r6, r1, 192
	FPMUL      r6, r6, r26
	FPADD      r5, r6, r5
	FPMUL      r6, r11, r23
	FPMUL      r8, r25, r27
	FPRSUB     r11, r6, r8
	LWI       r6, r1, 196
	FPMUL      r6, r6, r11
	FPADD      r5, r5, r6
	FPMUL      r8, r5, r20
	ORI       r5, r0, 0
	FPLT   r6, r8, r5
	bneid     r6, ($BB13_236)
	ADDI      r5, r0, 1
# BB#235:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADDI      r5, r0, 0
$BB13_236:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	bneid     r5, ($BB13_251)
	NOP    
# BB#237:                               # %lor.lhs.false12.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	FPADD      r5, r8, r22
	ORI       r6, r0, 1065353216
	FPGT   r6, r5, r6
	bneid     r6, ($BB13_239)
	ADDI      r5, r0, 1
# BB#238:                               # %lor.lhs.false12.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADDI      r5, r0, 0
$BB13_239:                              # %lor.lhs.false12.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	bneid     r5, ($BB13_251)
	NOP    
# BB#240:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	FPMUL      r5, r12, r28
	FPMUL      r3, r3, r26
	FPADD      r3, r3, r5
	FPMUL      r4, r4, r11
	FPADD      r3, r3, r4
	FPMUL      r3, r3, r20
	ORI       r4, r0, 869711765
	FPGT   r5, r3, r4
	ADDI      r11, r0, 0
	ADDI      r4, r0, 1
	bneid     r5, ($BB13_242)
	ADD      r8, r4, r0
# BB#241:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADD      r8, r11, r0
$BB13_242:                              # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ORI       r5, r0, 0
	FPGT   r6, r3, r5
	bneid     r6, ($BB13_244)
	ADD      r5, r4, r0
# BB#243:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADD      r5, r11, r0
$BB13_244:                              # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	BITAND       r5, r5, r8
	FPLT   r8, r3, r24
	bneid     r8, ($BB13_246)
	ADD      r6, r4, r0
# BB#245:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADD      r6, r11, r0
$BB13_246:                              # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	BITAND       r5, r5, r6
	bneid     r5, ($BB13_248)
	NOP    
# BB#247:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADD      r3, r24, r0
$BB13_248:                              # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	bneid     r5, ($BB13_250)
	NOP    
# BB#249:                               # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	LWI       r4, r1, 208
$BB13_250:                              # %if.end15.i.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	SWI       r4, r1, 208
	ADD      r24, r3, r0
$BB13_251:                              # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i.i
                                        #   in Loop: Header=BB13_228 Depth=5
	ADDI      r30, r30, 1
	CMP       r3, r7, r30
	bltid     r3, ($BB13_228)
	ADDI      r10, r10, 11
$BB13_252:                              # %if.end16.i.i
                                        #   in Loop: Header=BB13_207 Depth=4
	ADDI      r3, r0, -1
	ADD      r3, r21, r3
	bgeid     r3, ($BB13_253)
	NOP    
# BB#254:                               # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r3, r0, 0
	LWI       r4, r1, 208
	ANDI      r4, r4, 255
	bneid     r4, ($BB13_256)
	NOP    
# BB#255:                               # %if.then22.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r3, r0, 0
	LWI       r4, r1, 264
	FPADD      r3, r4, r3
$BB13_256:                              # %if.end26.i
                                        #   in Loop: Header=BB13_7 Depth=3
	LWI       r4, r1, 204
	ADDI      r4, r4, 10
	LOAD      r4, r4, 0
	MULI      r4, r4, 25
	LWI       r5, r1, 280
	ADD      r4, r4, r5
	LOAD      r21, r4, 4
	FPMUL      r8, r3, r21
	LOAD      r31, r4, 5
	FPMUL      r7, r3, r31
	LOAD      r10, r4, 6
	FPMUL      r30, r3, r10
	LWI       r23, r1, 224
	LWI       r12, r1, 228
	LWI       r20, r1, 260
$BB13_257:                              # %if.end34.i
                                        #   in Loop: Header=BB13_7 Depth=3
	LWI       r25, r1, 240
$BB13_258:                              # %do.body.i.i
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	RAND      r3
	RAND      r4
	FPADD      r4, r4, r4
	ORI       r5, r0, -1082130432
	FPADD      r4, r4, r5
	FPADD      r3, r3, r3
	FPADD      r5, r3, r5
	FPMUL      r6, r5, r5
	FPMUL      r9, r4, r4
	FPADD      r3, r6, r9
	ORI       r11, r0, 1065353216
	FPGE   r11, r3, r11
	bneid     r11, ($BB13_260)
	ADDI      r3, r0, 1
# BB#259:                               # %do.body.i.i
                                        #   in Loop: Header=BB13_258 Depth=4
	ADDI      r3, r0, 0
$BB13_260:                              # %do.body.i.i
                                        #   in Loop: Header=BB13_258 Depth=4
	bneid     r3, ($BB13_258)
	NOP    
# BB#261:                               # %do.end.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r3, r0, 0
	FPGE   r11, r12, r3
	FPUN   r3, r12, r3
	BITOR        r3, r3, r11
	bneid     r3, ($BB13_263)
	ADDI      r11, r0, 1
# BB#262:                               # %do.end.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r11, r0, 0
$BB13_263:                              # %do.end.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r3, r0, 1065353216
	FPRSUB     r6, r6, r3
	FPRSUB     r6, r9, r6
	FPINVSQRT r9, r6
	bneid     r11, ($BB13_265)
	ADD      r6, r12, r0
# BB#264:                               # %cond.true.i55.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPNEG      r6, r12
$BB13_265:                              # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADD      r26, r12, r0
	ORI       r11, r0, 0
	FPGE   r12, r20, r11
	FPUN   r11, r20, r11
	BITOR        r12, r11, r12
	bneid     r12, ($BB13_267)
	ADDI      r11, r0, 1
# BB#266:                               # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r11, r0, 0
$BB13_267:                              # %_ZL4Fabsf.exit57.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	bneid     r11, ($BB13_269)
	ADD      r12, r20, r0
# BB#268:                               # %cond.true.i50.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPNEG      r12, r20
$BB13_269:                              # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADD      r29, r20, r0
	ORI       r11, r0, 0
	FPGE   r20, r23, r11
	FPUN   r11, r23, r11
	BITOR        r11, r11, r20
	bneid     r11, ($BB13_271)
	ADDI      r20, r0, 1
# BB#270:                               # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r20, r0, 0
$BB13_271:                              # %_ZL4Fabsf.exit52.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	bneid     r20, ($BB13_273)
	ADD      r11, r23, r0
# BB#272:                               # %cond.true.i.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPNEG      r11, r23
$BB13_273:                              # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPGE   r20, r6, r12
	FPUN   r22, r6, r12
	BITOR        r20, r22, r20
	ADDI      r22, r0, 1
	LWI       r24, r1, 244
	LWI       r27, r1, 252
	bneid     r20, ($BB13_275)
	LWI       r28, r1, 256
# BB#274:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r22, r0, 0
$BB13_275:                              # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPDIV      r3, r3, r9
	ORI       r20, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r22, ($BB13_279)
	NOP    
# BB#276:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPLT   r6, r6, r11
	bneid     r6, ($BB13_278)
	ADDI      r22, r0, 1
# BB#277:                               # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r22, r0, 0
$BB13_278:                              # %_ZL4Fabsf.exit.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	bneid     r22, ($BB13_283)
	ADD      r6, r9, r0
$BB13_279:                              # %if.else.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	FPLT   r6, r12, r11
	bneid     r6, ($BB13_281)
	ADDI      r11, r0, 1
# BB#280:                               # %if.else.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ADDI      r11, r0, 0
$BB13_281:                              # %if.else.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r6, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r11, ($BB13_283)
	ADD      r20, r9, r0
# BB#282:                               # %if.else18.i.i.i
                                        #   in Loop: Header=BB13_7 Depth=3
	ORI       r6, r0, 0
	ORI       r9, r0, 1065353216
	ADD      r20, r6, r0
$BB13_283:                              # %_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color.exit
                                        #   in Loop: Header=BB13_7 Depth=3
	FPMUL      r11, r23, r6
	FPMUL      r12, r29, r9
	FPRSUB     r12, r11, r12
	FPMUL      r6, r26, r6
	FPMUL      r11, r29, r20
	FPRSUB     r6, r11, r6
	FPMUL      r9, r26, r9
	FPMUL      r11, r23, r20
	FPRSUB     r20, r9, r11
	FPMUL      r9, r23, r20
	FPMUL      r11, r29, r6
	FPRSUB     r9, r9, r11
	FPMUL      r11, r26, r6
	FPMUL      r22, r23, r12
	FPRSUB     r11, r11, r22
	FPMUL      r11, r11, r4
	FPMUL      r22, r20, r5
	FPADD      r11, r22, r11
	FPMUL      r9, r9, r4
	FPMUL      r22, r12, r5
	FPADD      r9, r22, r9
	FPMUL      r22, r26, r3
	FPADD      r9, r9, r22
	FPMUL      r22, r29, r3
	FPADD      r11, r11, r22
	FPMUL      r12, r29, r12
	FPMUL      r20, r26, r20
	FPMUL      r8, r8, r28
	FPMUL      r7, r7, r27
	FPMUL      r22, r30, r24
	ADD      r26, r23, r0
	LWI       r23, r1, 232
	FPADD      r23, r23, r22
	SWI       r23, r1, 232
	LWI       r22, r1, 236
	FPADD      r22, r22, r7
	SWI       r22, r1, 236
	FPADD      r25, r25, r8
	FPMUL      r7, r11, r11
	FPMUL      r8, r9, r9
	FPADD      r7, r8, r7
	FPRSUB     r8, r12, r20
	FPMUL      r4, r8, r4
	FPMUL      r5, r6, r5
	FPADD      r4, r5, r4
	FPMUL      r3, r26, r3
	FPADD      r3, r4, r3
	FPMUL      r4, r3, r3
	FPADD      r4, r7, r4
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	LWI       r6, r1, 200
	ANDI      r6, r6, 255
	bneid     r6, ($BB13_284)
	NOP    
$BB13_285:                              # %for.inc
                                        #   in Loop: Header=BB13_6 Depth=2
	LWI       r4, r1, 288
	ADDI      r4, r4, 1
	SWI       r4, r1, 288
	LWI       r3, r1, 292
	CMP       r3, r3, r4
	LWI       r7, r1, 344
	bltid     r3, ($BB13_6)
	LWI       r8, r1, 348
	brid      ($BB13_286)
	NOP    
$BB13_27:                               #   in Loop: Header=BB13_2 Depth=1
	brid      ($BB13_286)
	LWI       r25, r1, 240
$BB13_2:                                # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_26 Depth 2
                                        #       Child Loop BB13_138 Depth 3
                                        #       Child Loop BB13_29 Depth 3
                                        #         Child Loop BB13_50 Depth 4
                                        #         Child Loop BB13_30 Depth 4
                                        #       Child Loop BB13_87 Depth 3
                                        #         Child Loop BB13_88 Depth 4
                                        #         Child Loop BB13_108 Depth 4
                                        #     Child Loop BB13_6 Depth 2
                                        #       Child Loop BB13_7 Depth 3
                                        #         Child Loop BB13_207 Depth 4
                                        #           Child Loop BB13_208 Depth 5
                                        #           Child Loop BB13_228 Depth 5
                                        #         Child Loop BB13_258 Depth 4
                                        #         Child Loop BB13_9 Depth 4
                                        #           Child Loop BB13_170 Depth 5
                                        #           Child Loop BB13_10 Depth 5
	LWI       r6, r1, 368
	DIV      r5, r6, r4
	FPCONV       r3, r5
	MUL       r5, r5, r6
	SWI       r5, r1, 360
	RSUB     r4, r5, r4
	SWI       r4, r1, 364
	FPCONV       r5, r4
	ORI       r4, r0, 1056964608
	ADDI      r6, r0, 1
	CMP       r6, r6, r9
	ORI       r25, r0, 0
	bltid     r6, ($BB13_3)
	NOP    
# BB#4:                                 # %for.body30.lr.ph
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r6, r1, 388
	FPRSUB     r6, r6, r3
	LWI       r3, r1, 396
	FPRSUB     r3, r3, r5
	FPADD      r3, r3, r4
	FPADD      r4, r6, r4
	FPADD      r4, r4, r4
	LWI       r5, r1, 384
	FPDIV      r7, r4, r5
	SWI       r7, r1, 344
	FPADD      r3, r3, r3
	LWI       r4, r1, 392
	FPDIV      r8, r3, r4
	SWI       r8, r1, 348
	ADDI      r3, r0, 1
	CMP       r3, r3, r9
	ORI       r25, r0, 0
	ADD      r4, r0, r0
	bneid     r3, ($BB13_5)
	SWI       r4, r1, 288
# BB#164:                               # %_ZNK9RayCamera7makeRayER3Rayff.exit165
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r3, r1, 320
	FPMUL      r3, r3, r8
	LWI       r4, r1, 308
	FPADD      r3, r4, r3
	LWI       r4, r1, 332
	FPMUL      r4, r4, r7
	FPADD      r3, r3, r4
	LWI       r4, r1, 324
	FPMUL      r4, r4, r8
	LWI       r5, r1, 312
	FPADD      r4, r5, r4
	LWI       r5, r1, 336
	FPMUL      r5, r5, r7
	FPADD      r5, r4, r5
	FPMUL      r4, r5, r5
	FPMUL      r6, r3, r3
	FPADD      r6, r6, r4
	LWI       r4, r1, 340
	FPMUL      r4, r4, r7
	LWI       r7, r1, 328
	FPMUL      r7, r7, r8
	LWI       r8, r1, 316
	FPADD      r7, r8, r7
	FPADD      r4, r7, r4
	FPMUL      r7, r4, r4
	FPADD      r6, r6, r7
	FPINVSQRT r6, r6
	ORI       r22, r0, 1065353216
	FPDIV      r6, r22, r6
	FPDIV      r6, r22, r6
	FPMUL      r4, r4, r6
	SWI       r4, r1, 196
	FPMUL      r29, r5, r6
	FPMUL      r3, r3, r6
	SWI       r3, r1, 192
	ADD      r4, r0, r0
	ORI       r3, r0, 0
	SWI       r3, r1, 232
	LWI       r5, r1, 304
	SWI       r5, r1, 208
	LWI       r5, r1, 300
	SWI       r5, r1, 212
	LWI       r5, r1, 296
	SWI       r5, r1, 216
	ADD      r23, r22, r0
	SWI       r22, r1, 244
	SWI       r3, r1, 236
	brid      ($BB13_26)
	SWI       r3, r1, 240
$BB13_25:                               # %if.end46.us
                                        #   in Loop: Header=BB13_26 Depth=2
	SWI       r25, r1, 240
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r22, r22, r24
	FPMUL      r23, r23, r30
	FPMUL      r28, r28, r26
	SWI       r28, r1, 244
	FPMUL      r5, r9, r4
	SWI       r5, r1, 192
	FPMUL      r29, r10, r4
	FPMUL      r3, r3, r4
	SWI       r3, r1, 196
	LWI       r3, r1, 184
	SWI       r3, r1, 208
	LWI       r3, r1, 176
	SWI       r3, r1, 212
	LWI       r3, r1, 180
	SWI       r3, r1, 216
	LWI       r4, r1, 252
$BB13_26:                               # %while.cond.us
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_138 Depth 3
                                        #       Child Loop BB13_29 Depth 3
                                        #         Child Loop BB13_50 Depth 4
                                        #         Child Loop BB13_30 Depth 4
                                        #       Child Loop BB13_87 Depth 3
                                        #         Child Loop BB13_88 Depth 4
                                        #         Child Loop BB13_108 Depth 4
	SWI       r23, r1, 256
	SWI       r22, r1, 248
	LWI       r3, r1, 284
	CMP       r3, r3, r4
	bgeid     r3, ($BB13_27)
	NOP    
# BB#28:                                # %while.body.us
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r4, r4, 1
	SWI       r4, r1, 252
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 196
	FPDIV      r4, r3, r4
	SWI       r4, r1, 220
	FPDIV      r4, r3, r29
	SWI       r4, r1, 224
	LWI       r4, r1, 192
	FPDIV      r3, r3, r4
	SWI       r3, r1, 228
	ORI       r9, r0, 1203982336
	ADD      r3, r0, r0
	SWI       r3, r1, 200
	SWI       r3, r1, 204
	brid      ($BB13_29)
	ADD      r26, r3, r0
$BB13_77:                               # %if.end19.i811
                                        #   in Loop: Header=BB13_29 Depth=3
	bslli     r4, r24, 2
	ADDI      r5, r1, 48
	ADD      r4, r5, r4
	LWI       r26, r4, -4
$BB13_29:                               # %while.body.us
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_50 Depth 4
                                        #         Child Loop BB13_30 Depth 4
	brid      ($BB13_30)
	ADD      r24, r3, r0
$BB13_309:                              # %if.then10.i722
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r3, r26, 1
	bslli     r4, r24, 2
	ADDI      r5, r1, 48
	SW        r3, r5, r4
	ADDI      r24, r24, 1
$BB13_30:                               # %while.body.i702
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        #       Parent Loop BB13_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	bslli     r3, r26, 3
	LWI       r4, r1, 188
	ADD      r7, r3, r4
	LOAD      r3, r7, 0
	LOAD      r5, r7, 1
	LOAD      r4, r7, 2
	ADDI      r8, r7, 3
	LOAD      r11, r8, 0
	LOAD      r10, r8, 1
	LWI       r6, r1, 212
	FPRSUB     r20, r6, r10
	FPRSUB     r5, r6, r5
	LOAD      r8, r8, 2
	LWI       r6, r1, 208
	FPRSUB     r10, r6, r8
	FPRSUB     r4, r6, r4
	LWI       r6, r1, 220
	FPMUL      r8, r4, r6
	FPMUL      r10, r10, r6
	LWI       r4, r1, 224
	FPMUL      r12, r5, r4
	FPMUL      r4, r20, r4
	LWI       r5, r1, 216
	FPRSUB     r11, r5, r11
	FPRSUB     r3, r5, r3
	LWI       r6, r1, 228
	FPMUL      r5, r3, r6
	FPMUL      r20, r11, r6
	FPMIN     r3, r5, r20
	FPMIN     r11, r12, r4
	FPMIN     r21, r8, r10
	FPMAX     r5, r5, r20
	FPMAX     r4, r12, r4
	FPMAX     r10, r8, r10
	FPMAX     r8, r11, r21
	FPMAX     r8, r3, r8
	FPMIN     r3, r4, r10
	FPMIN     r3, r5, r3
	FPGE   r4, r8, r3
	FPUN   r5, r8, r3
	BITOR        r5, r5, r4
	bneid     r5, ($BB13_32)
	ADDI      r4, r0, 1
# BB#31:                                # %while.body.i702
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r4, r0, 0
$BB13_32:                               # %while.body.i702
                                        #   in Loop: Header=BB13_30 Depth=4
	bneid     r4, ($BB13_76)
	NOP    
# BB#33:                                # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	ORI       r4, r0, 869711765
	FPLE   r5, r8, r4
	FPUN   r4, r8, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_35)
	ADDI      r4, r0, 1
# BB#34:                                # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r4, r0, 0
$BB13_35:                               # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	bneid     r4, ($BB13_39)
	NOP    
# BB#36:                                # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	ORI       r4, r0, 1259902592
	FPLT   r5, r8, r4
	bneid     r5, ($BB13_38)
	ADDI      r4, r0, 1
# BB#37:                                # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r4, r0, 0
$BB13_38:                               # %if.then.i.i706
                                        #   in Loop: Header=BB13_30 Depth=4
	beqid     r4, ($BB13_39)
	NOP    
# BB#45:                                # %lor.lhs.false.i712
                                        #   in Loop: Header=BB13_30 Depth=4
	FPLT   r3, r9, r8
	FPUN   r4, r9, r8
	BITOR        r4, r4, r3
	bneid     r4, ($BB13_47)
	ADDI      r3, r0, 1
# BB#46:                                # %lor.lhs.false.i712
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r3, r0, 0
$BB13_47:                               # %lor.lhs.false.i712
                                        #   in Loop: Header=BB13_30 Depth=4
	bneid     r3, ($BB13_76)
	NOP    
	brid      ($BB13_48)
	NOP    
$BB13_39:                               # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	ORI       r4, r0, 869711765
	FPLE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_41)
	ADDI      r4, r0, 1
# BB#40:                                # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r4, r0, 0
$BB13_41:                               # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	bneid     r4, ($BB13_76)
	NOP    
# BB#42:                                # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	ORI       r4, r0, 1259902592
	FPLT   r4, r3, r4
	bneid     r4, ($BB13_44)
	ADDI      r3, r0, 1
# BB#43:                                # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	ADDI      r3, r0, 0
$BB13_44:                               # %if.then24.i.i710
                                        #   in Loop: Header=BB13_30 Depth=4
	beqid     r3, ($BB13_76)
	NOP    
$BB13_48:                               # %if.then6.i716
                                        #   in Loop: Header=BB13_30 Depth=4
	LOAD      r26, r7, 7
	LOAD      r8, r7, 6
	bltid     r8, ($BB13_309)
	NOP    
# BB#49:                                # %for.cond.preheader.i718
                                        #   in Loop: Header=BB13_29 Depth=3
	bleid     r8, ($BB13_76)
	ADD      r7, r0, r0
$BB13_50:                               # %for.body.i769
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        #       Parent Loop BB13_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	LOAD      r11, r26, 0
	LOAD      r12, r26, 1
	LOAD      r3, r26, 2
	ADDI      r5, r26, 3
	LOAD      r10, r5, 0
	LOAD      r4, r5, 1
	LOAD      r5, r5, 2
	ADDI      r20, r26, 6
	LOAD      r21, r20, 0
	LOAD      r22, r20, 1
	LOAD      r23, r20, 2
	FPRSUB     r27, r23, r5
	FPRSUB     r30, r22, r4
	LWI       r6, r1, 196
	FPMUL      r4, r6, r30
	FPMUL      r5, r29, r27
	FPRSUB     r31, r4, r5
	FPRSUB     r10, r21, r10
	FPMUL      r4, r6, r10
	LWI       r6, r1, 192
	FPMUL      r5, r6, r27
	FPRSUB     r28, r5, r4
	LWI       r4, r1, 216
	FPRSUB     r20, r21, r4
	LWI       r4, r1, 212
	FPRSUB     r25, r22, r4
	FPMUL      r4, r25, r28
	FPMUL      r5, r20, r31
	FPADD      r5, r5, r4
	FPMUL      r4, r29, r10
	FPMUL      r6, r6, r30
	FPRSUB     r4, r4, r6
	FPRSUB     r21, r21, r11
	FPRSUB     r12, r22, r12
	LWI       r6, r1, 208
	FPRSUB     r22, r23, r6
	FPMUL      r6, r22, r4
	FPADD      r11, r5, r6
	FPMUL      r5, r12, r28
	FPMUL      r6, r21, r31
	FPADD      r5, r6, r5
	FPRSUB     r31, r23, r3
	FPMUL      r3, r31, r4
	FPADD      r3, r5, r3
	ORI       r4, r0, 1065353216
	FPDIV      r3, r4, r3
	FPMUL      r11, r11, r3
	ORI       r4, r0, 0
	FPLT   r5, r11, r4
	bneid     r5, ($BB13_52)
	ADDI      r4, r0, 1
# BB#51:                                # %for.body.i769
                                        #   in Loop: Header=BB13_50 Depth=4
	ADDI      r4, r0, 0
$BB13_52:                               # %for.body.i769
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r4, ($BB13_75)
	NOP    
# BB#53:                                # %for.body.i769
                                        #   in Loop: Header=BB13_50 Depth=4
	ORI       r4, r0, 1065353216
	FPGT   r5, r11, r4
	bneid     r5, ($BB13_55)
	ADDI      r4, r0, 1
# BB#54:                                # %for.body.i769
                                        #   in Loop: Header=BB13_50 Depth=4
	ADDI      r4, r0, 0
$BB13_55:                               # %for.body.i769
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r4, ($BB13_75)
	NOP    
# BB#56:                                # %if.end.i.i786
                                        #   in Loop: Header=BB13_50 Depth=4
	FPMUL      r4, r22, r12
	FPMUL      r5, r25, r31
	FPRSUB     r23, r4, r5
	FPMUL      r4, r20, r31
	FPMUL      r5, r22, r21
	FPRSUB     r22, r4, r5
	FPMUL      r4, r29, r22
	LWI       r5, r1, 192
	FPMUL      r5, r5, r23
	FPADD      r4, r5, r4
	FPMUL      r5, r25, r21
	FPMUL      r6, r20, r12
	FPRSUB     r12, r5, r6
	LWI       r5, r1, 196
	FPMUL      r5, r5, r12
	FPADD      r4, r4, r5
	FPMUL      r20, r4, r3
	ORI       r4, r0, 0
	FPLT   r5, r20, r4
	bneid     r5, ($BB13_58)
	ADDI      r4, r0, 1
# BB#57:                                # %if.end.i.i786
                                        #   in Loop: Header=BB13_50 Depth=4
	ADDI      r4, r0, 0
$BB13_58:                               # %if.end.i.i786
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r4, ($BB13_75)
	NOP    
# BB#59:                                # %lor.lhs.false12.i.i789
                                        #   in Loop: Header=BB13_50 Depth=4
	FPADD      r4, r20, r11
	ORI       r5, r0, 1065353216
	FPGT   r5, r4, r5
	bneid     r5, ($BB13_61)
	ADDI      r4, r0, 1
# BB#60:                                # %lor.lhs.false12.i.i789
                                        #   in Loop: Header=BB13_50 Depth=4
	ADDI      r4, r0, 0
$BB13_61:                               # %lor.lhs.false12.i.i789
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r4, ($BB13_75)
	NOP    
# BB#62:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	FPMUL      r4, r30, r22
	FPMUL      r5, r10, r23
	FPADD      r4, r5, r4
	FPMUL      r5, r27, r12
	FPADD      r4, r4, r5
	FPMUL      r3, r4, r3
	ORI       r4, r0, 869711765
	FPGT   r4, r3, r4
	ADDI      r11, r0, 0
	ADDI      r10, r0, 1
	bneid     r4, ($BB13_64)
	ADD      r12, r10, r0
# BB#63:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	ADD      r12, r11, r0
$BB13_64:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	ORI       r4, r0, 0
	FPGT   r5, r3, r4
	bneid     r5, ($BB13_66)
	ADD      r4, r10, r0
# BB#65:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	ADD      r4, r11, r0
$BB13_66:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	BITAND       r4, r4, r12
	FPLT   r12, r3, r9
	bneid     r12, ($BB13_68)
	ADD      r5, r10, r0
# BB#67:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	ADD      r5, r11, r0
$BB13_68:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	BITAND       r11, r4, r5
	bneid     r11, ($BB13_70)
	NOP    
# BB#69:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	ADD      r3, r9, r0
$BB13_70:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r11, ($BB13_72)
	ADD      r9, r26, r0
# BB#71:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	LWI       r9, r1, 204
$BB13_72:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	bneid     r11, ($BB13_74)
	NOP    
# BB#73:                                # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	LWI       r10, r1, 200
$BB13_74:                               # %if.end15.i.i799
                                        #   in Loop: Header=BB13_50 Depth=4
	SWI       r10, r1, 200
	SWI       r9, r1, 204
	ADD      r9, r3, r0
$BB13_75:                               # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i806
                                        #   in Loop: Header=BB13_50 Depth=4
	ADDI      r7, r7, 1
	CMP       r3, r8, r7
	bltid     r3, ($BB13_50)
	ADDI      r26, r26, 11
$BB13_76:                               # %if.end16.i809
                                        #   in Loop: Header=BB13_29 Depth=3
	ADDI      r3, r0, -1
	ADD      r3, r24, r3
	bgeid     r3, ($BB13_77)
	NOP    
# BB#78:                                # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit812
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r24, r0, 1065353216
	ORI       r8, r0, 1057988018
	ORI       r27, r0, 1060806590
	ORI       r7, r0, 1065151889
	LWI       r3, r1, 200
	ANDI      r3, r3, 255
	ADD      r30, r24, r0
	beqid     r3, ($BB13_137)
	ADD      r26, r24, r0
# BB#79:                                # %if.then.i233
                                        #   in Loop: Header=BB13_26 Depth=2
	LWI       r6, r1, 204
	LOAD      r3, r6, 0
	LOAD      r4, r6, 1
	LOAD      r7, r6, 2
	ADDI      r5, r6, 3
	LOAD      r11, r5, 0
	LOAD      r10, r5, 1
	LOAD      r5, r5, 2
	ADDI      r20, r6, 6
	LOAD      r12, r20, 0
	LOAD      r21, r20, 1
	FPRSUB     r8, r21, r4
	FPRSUB     r10, r21, r10
	LOAD      r4, r20, 2
	FPRSUB     r20, r4, r5
	FPRSUB     r21, r4, r7
	FPMUL      r4, r21, r10
	FPMUL      r5, r8, r20
	FPRSUB     r7, r4, r5
	FPRSUB     r4, r12, r11
	FPRSUB     r5, r12, r3
	FPMUL      r3, r5, r20
	FPMUL      r6, r21, r4
	FPRSUB     r3, r3, r6
	FPMUL      r11, r3, r3
	FPMUL      r6, r7, r7
	FPADD      r11, r6, r11
	FPMUL      r4, r8, r4
	FPMUL      r5, r5, r10
	FPRSUB     r8, r4, r5
	FPMUL      r4, r8, r8
	FPADD      r4, r11, r4
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	FPDIV      r4, r5, r4
	FPDIV      r4, r5, r4
	FPMUL      r11, r7, r4
	FPMUL      r12, r3, r4
	FPMUL      r3, r12, r29
	LWI       r5, r1, 192
	FPMUL      r5, r11, r5
	FPADD      r3, r5, r3
	FPMUL      r10, r8, r4
	LWI       r4, r1, 196
	FPMUL      r4, r10, r4
	FPADD      r3, r3, r4
	ORI       r4, r0, 0
	FPLE   r5, r3, r4
	FPUN   r3, r3, r4
	BITOR        r3, r3, r5
	bneid     r3, ($BB13_81)
	ADDI      r7, r0, 1
# BB#80:                                # %if.then.i233
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r7, r0, 0
$BB13_81:                               # %if.then.i233
                                        #   in Loop: Header=BB13_26 Depth=2
	LWI       r3, r1, 196
	FPMUL      r3, r3, r9
	LWI       r4, r1, 208
	FPADD      r3, r4, r3
	FPMUL      r4, r29, r9
	LWI       r5, r1, 212
	FPADD      r5, r5, r4
	LWI       r4, r1, 192
	FPMUL      r4, r4, r9
	LWI       r6, r1, 216
	bneid     r7, ($BB13_83)
	FPADD      r4, r6, r4
# BB#82:                                # %if.then11.i237
                                        #   in Loop: Header=BB13_26 Depth=2
	FPNEG      r10, r10
	FPNEG      r12, r12
	FPNEG      r11, r11
$BB13_83:                               # %if.then.i.i267
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r6, r0, 981668463
	FPMUL      r7, r12, r6
	FPADD      r7, r5, r7
	SWI       r7, r1, 176
	FPMUL      r5, r11, r6
	FPADD      r5, r4, r5
	SWI       r5, r1, 180
	LWI       r4, r1, 268
	FPRSUB     r4, r5, r4
	LWI       r5, r1, 272
	FPRSUB     r5, r7, r5
	FPMUL      r7, r5, r5
	FPMUL      r8, r4, r4
	FPADD      r7, r8, r7
	FPMUL      r6, r10, r6
	FPADD      r6, r3, r6
	SWI       r6, r1, 184
	LWI       r3, r1, 276
	FPRSUB     r3, r6, r3
	FPMUL      r6, r3, r3
	FPADD      r6, r7, r6
	FPINVSQRT r6, r6
	ORI       r7, r0, 1065353216
	FPDIV      r24, r7, r6
	FPDIV      r7, r7, r24
	FPMUL      r9, r5, r7
	FPMUL      r5, r12, r9
	FPMUL      r4, r4, r7
	SWI       r4, r1, 192
	FPMUL      r4, r11, r4
	FPADD      r4, r4, r5
	FPMUL      r3, r3, r7
	SWI       r3, r1, 196
	FPMUL      r3, r10, r3
	FPADD      r5, r4, r3
	SWI       r5, r1, 264
	ORI       r3, r0, 0
	FPLE   r4, r5, r3
	FPUN   r5, r5, r3
	BITOR        r5, r5, r4
	bneid     r5, ($BB13_85)
	ADDI      r4, r0, 1
# BB#84:                                # %if.then.i.i267
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r4, r0, 0
$BB13_85:                               # %if.then.i.i267
                                        #   in Loop: Header=BB13_26 Depth=2
	SWI       r12, r1, 260
	SWI       r11, r1, 228
	bneid     r4, ($BB13_136)
	SWI       r10, r1, 224
# BB#86:                                # %if.then20.i379
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r3, r0, 1065353216
	LWI       r4, r1, 196
	FPDIV      r4, r3, r4
	SWI       r4, r1, 212
	FPDIV      r4, r3, r9
	SWI       r4, r1, 216
	LWI       r4, r1, 192
	FPDIV      r3, r3, r4
	SWI       r3, r1, 220
	ADD      r3, r0, r0
	SWI       r3, r1, 208
	brid      ($BB13_87)
	ADD      r31, r3, r0
$BB13_133:                              # %if.end19.i.i532
                                        #   in Loop: Header=BB13_87 Depth=3
	bslli     r4, r30, 2
	ADDI      r5, r1, 48
	ADD      r4, r5, r4
	LWI       r31, r4, -4
$BB13_87:                               # %if.then20.i379
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_88 Depth 4
                                        #         Child Loop BB13_108 Depth 4
	brid      ($BB13_88)
	ADD      r30, r3, r0
$BB13_310:                              # %if.then10.i.i439
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r3, r31, 1
	bslli     r4, r30, 2
	ADDI      r5, r1, 48
	SW        r3, r5, r4
	ADDI      r30, r30, 1
$BB13_88:                               # %while.body.i.i419
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        #       Parent Loop BB13_87 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	bslli     r3, r31, 3
	LWI       r4, r1, 188
	ADD      r7, r3, r4
	LOAD      r3, r7, 0
	LOAD      r5, r7, 1
	LOAD      r4, r7, 2
	ADDI      r8, r7, 3
	LOAD      r10, r8, 0
	LOAD      r11, r8, 1
	LWI       r6, r1, 176
	FPRSUB     r12, r6, r11
	FPRSUB     r5, r6, r5
	LOAD      r8, r8, 2
	LWI       r6, r1, 184
	FPRSUB     r8, r6, r8
	FPRSUB     r4, r6, r4
	LWI       r6, r1, 212
	FPMUL      r4, r4, r6
	FPMUL      r8, r8, r6
	LWI       r6, r1, 216
	FPMUL      r11, r5, r6
	FPMUL      r12, r12, r6
	LWI       r5, r1, 180
	FPRSUB     r10, r5, r10
	FPRSUB     r3, r5, r3
	LWI       r6, r1, 220
	FPMUL      r5, r3, r6
	FPMUL      r20, r10, r6
	FPMIN     r3, r5, r20
	FPMIN     r10, r11, r12
	FPMIN     r21, r4, r8
	FPMAX     r5, r5, r20
	FPMAX     r11, r11, r12
	FPMAX     r8, r4, r8
	FPMAX     r4, r10, r21
	FPMAX     r4, r3, r4
	FPMIN     r3, r11, r8
	FPMIN     r3, r5, r3
	FPGE   r5, r4, r3
	FPUN   r8, r4, r3
	BITOR        r8, r8, r5
	bneid     r8, ($BB13_90)
	ADDI      r5, r0, 1
# BB#89:                                # %while.body.i.i419
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r5, r0, 0
$BB13_90:                               # %while.body.i.i419
                                        #   in Loop: Header=BB13_88 Depth=4
	bneid     r5, ($BB13_132)
	NOP    
# BB#91:                                # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	ORI       r5, r0, 869711765
	FPLE   r8, r4, r5
	FPUN   r5, r4, r5
	BITOR        r8, r5, r8
	bneid     r8, ($BB13_93)
	ADDI      r5, r0, 1
# BB#92:                                # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r5, r0, 0
$BB13_93:                               # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	bneid     r5, ($BB13_97)
	NOP    
# BB#94:                                # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	ORI       r5, r0, 1259902592
	FPLT   r8, r4, r5
	bneid     r8, ($BB13_96)
	ADDI      r5, r0, 1
# BB#95:                                # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r5, r0, 0
$BB13_96:                               # %if.then.i.i.i423
                                        #   in Loop: Header=BB13_88 Depth=4
	beqid     r5, ($BB13_97)
	NOP    
# BB#103:                               # %lor.lhs.false.i.i429
                                        #   in Loop: Header=BB13_88 Depth=4
	FPLT   r3, r24, r4
	FPUN   r4, r24, r4
	BITOR        r4, r4, r3
	bneid     r4, ($BB13_105)
	ADDI      r3, r0, 1
# BB#104:                               # %lor.lhs.false.i.i429
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r3, r0, 0
$BB13_105:                              # %lor.lhs.false.i.i429
                                        #   in Loop: Header=BB13_88 Depth=4
	bneid     r3, ($BB13_132)
	NOP    
	brid      ($BB13_106)
	NOP    
$BB13_97:                               # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	ORI       r4, r0, 869711765
	FPLE   r5, r3, r4
	FPUN   r4, r3, r4
	BITOR        r5, r4, r5
	bneid     r5, ($BB13_99)
	ADDI      r4, r0, 1
# BB#98:                                # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r4, r0, 0
$BB13_99:                               # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	bneid     r4, ($BB13_132)
	NOP    
# BB#100:                               # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	ORI       r4, r0, 1259902592
	FPLT   r4, r3, r4
	bneid     r4, ($BB13_102)
	ADDI      r3, r0, 1
# BB#101:                               # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	ADDI      r3, r0, 0
$BB13_102:                              # %if.then24.i.i.i427
                                        #   in Loop: Header=BB13_88 Depth=4
	beqid     r3, ($BB13_132)
	NOP    
$BB13_106:                              # %if.then6.i84.i433
                                        #   in Loop: Header=BB13_88 Depth=4
	LOAD      r31, r7, 7
	LOAD      r7, r7, 6
	bltid     r7, ($BB13_310)
	NOP    
# BB#107:                               # %for.cond.preheader.i.i435
                                        #   in Loop: Header=BB13_87 Depth=3
	bleid     r7, ($BB13_132)
	ADD      r8, r0, r0
$BB13_108:                              # %for.body.i.i487
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        #       Parent Loop BB13_87 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	LOAD      r11, r31, 0
	LOAD      r12, r31, 1
	LOAD      r3, r31, 2
	ADDI      r4, r31, 3
	LOAD      r10, r4, 0
	LOAD      r5, r4, 1
	LOAD      r4, r4, 2
	ADDI      r20, r31, 6
	LOAD      r21, r20, 0
	LOAD      r23, r20, 1
	LOAD      r26, r20, 2
	FPRSUB     r4, r26, r4
	FPRSUB     r22, r23, r5
	LWI       r6, r1, 196
	FPMUL      r5, r6, r22
	FPMUL      r20, r9, r4
	FPRSUB     r28, r5, r20
	FPRSUB     r10, r21, r10
	FPMUL      r5, r6, r10
	LWI       r6, r1, 192
	FPMUL      r20, r6, r4
	FPRSUB     r29, r20, r5
	LWI       r5, r1, 180
	FPRSUB     r20, r21, r5
	LWI       r5, r1, 176
	FPRSUB     r25, r23, r5
	FPMUL      r5, r25, r29
	FPMUL      r27, r20, r28
	FPADD      r5, r27, r5
	FPMUL      r27, r9, r10
	FPMUL      r6, r6, r22
	FPRSUB     r27, r27, r6
	FPRSUB     r21, r21, r11
	FPRSUB     r12, r23, r12
	LWI       r6, r1, 184
	FPRSUB     r23, r26, r6
	FPMUL      r6, r23, r27
	FPADD      r11, r5, r6
	FPMUL      r5, r12, r29
	FPMUL      r6, r21, r28
	FPADD      r5, r6, r5
	FPRSUB     r28, r26, r3
	FPMUL      r3, r28, r27
	FPADD      r3, r5, r3
	ORI       r5, r0, 1065353216
	FPDIV      r3, r5, r3
	FPMUL      r11, r11, r3
	ORI       r5, r0, 0
	FPLT   r26, r11, r5
	bneid     r26, ($BB13_110)
	ADDI      r5, r0, 1
# BB#109:                               # %for.body.i.i487
                                        #   in Loop: Header=BB13_108 Depth=4
	ADDI      r5, r0, 0
$BB13_110:                              # %for.body.i.i487
                                        #   in Loop: Header=BB13_108 Depth=4
	bneid     r5, ($BB13_131)
	NOP    
# BB#111:                               # %for.body.i.i487
                                        #   in Loop: Header=BB13_108 Depth=4
	ORI       r5, r0, 1065353216
	FPGT   r26, r11, r5
	bneid     r26, ($BB13_113)
	ADDI      r5, r0, 1
# BB#112:                               # %for.body.i.i487
                                        #   in Loop: Header=BB13_108 Depth=4
	ADDI      r5, r0, 0
$BB13_113:                              # %for.body.i.i487
                                        #   in Loop: Header=BB13_108 Depth=4
	bneid     r5, ($BB13_131)
	NOP    
# BB#114:                               # %if.end.i.i.i504
                                        #   in Loop: Header=BB13_108 Depth=4
	FPMUL      r5, r23, r12
	FPMUL      r6, r25, r28
	FPRSUB     r26, r5, r6
	FPMUL      r5, r20, r28
	FPMUL      r6, r23, r21
	FPRSUB     r23, r5, r6
	FPMUL      r5, r9, r23
	LWI       r6, r1, 192
	FPMUL      r6, r6, r26
	FPADD      r5, r6, r5
	FPMUL      r21, r25, r21
	FPMUL      r6, r20, r12
	FPRSUB     r12, r21, r6
	LWI       r6, r1, 196
	FPMUL      r6, r6, r12
	FPADD      r5, r5, r6
	FPMUL      r20, r5, r3
	ORI       r5, r0, 0
	FPLT   r21, r20, r5
	bneid     r21, ($BB13_116)
	ADDI      r5, r0, 1
# BB#115:                               # %if.end.i.i.i504
                                        #   in Loop: Header=BB13_108 Depth=4
	ADDI      r5, r0, 0
$BB13_116:                              # %if.end.i.i.i504
                                        #   in Loop: Header=BB13_108 Depth=4
	bneid     r5, ($BB13_131)
	NOP    
# BB#117:                               # %lor.lhs.false12.i.i.i507
                                        #   in Loop: Header=BB13_108 Depth=4
	FPADD      r5, r20, r11
	ORI       r6, r0, 1065353216
	FPGT   r11, r5, r6
	bneid     r11, ($BB13_119)
	ADDI      r5, r0, 1
# BB#118:                               # %lor.lhs.false12.i.i.i507
                                        #   in Loop: Header=BB13_108 Depth=4
	ADDI      r5, r0, 0
$BB13_119:                              # %lor.lhs.false12.i.i.i507
                                        #   in Loop: Header=BB13_108 Depth=4
	bneid     r5, ($BB13_131)
	NOP    
# BB#120:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	FPMUL      r5, r22, r23
	FPMUL      r6, r10, r26
	FPADD      r5, r6, r5
	FPMUL      r4, r4, r12
	FPADD      r4, r5, r4
	FPMUL      r3, r4, r3
	ORI       r4, r0, 869711765
	FPGT   r5, r3, r4
	ADDI      r10, r0, 0
	ADDI      r4, r0, 1
	bneid     r5, ($BB13_122)
	ADD      r11, r4, r0
# BB#121:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	ADD      r11, r10, r0
$BB13_122:                              # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	ORI       r5, r0, 0
	FPGT   r12, r3, r5
	bneid     r12, ($BB13_124)
	ADD      r5, r4, r0
# BB#123:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	ADD      r5, r10, r0
$BB13_124:                              # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	BITAND       r5, r5, r11
	FPLT   r12, r3, r24
	bneid     r12, ($BB13_126)
	ADD      r11, r4, r0
# BB#125:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	ADD      r11, r10, r0
$BB13_126:                              # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	BITAND       r5, r5, r11
	bneid     r5, ($BB13_128)
	NOP    
# BB#127:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	ADD      r3, r24, r0
$BB13_128:                              # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	bneid     r5, ($BB13_130)
	NOP    
# BB#129:                               # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	LWI       r4, r1, 208
$BB13_130:                              # %if.end15.i.i.i521
                                        #   in Loop: Header=BB13_108 Depth=4
	SWI       r4, r1, 208
	ADD      r24, r3, r0
$BB13_131:                              # %_ZNK3Tri9intersectER9HitRecordRK3Ray.exit.i.i526
                                        #   in Loop: Header=BB13_108 Depth=4
	ADDI      r8, r8, 1
	CMP       r3, r7, r8
	bltid     r3, ($BB13_108)
	ADDI      r31, r31, 11
$BB13_132:                              # %if.end16.i.i530
                                        #   in Loop: Header=BB13_87 Depth=3
	ADDI      r3, r0, -1
	ADD      r3, r30, r3
	bgeid     r3, ($BB13_133)
	NOP    
# BB#134:                               # %_ZNK23BoundingVolumeHierarchy9intersectER9HitRecordRK3Ray.exit.i534
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r3, r0, 0
	LWI       r4, r1, 208
	ANDI      r4, r4, 255
	bneid     r4, ($BB13_136)
	NOP    
# BB#135:                               # %if.then22.i541
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r3, r0, 0
	LWI       r4, r1, 264
	FPADD      r3, r4, r3
$BB13_136:                              # %if.end26.i557
                                        #   in Loop: Header=BB13_26 Depth=2
	LWI       r4, r1, 204
	ADDI      r4, r4, 10
	LOAD      r4, r4, 0
	MULI      r4, r4, 25
	LWI       r5, r1, 280
	ADD      r4, r4, r5
	LOAD      r26, r4, 4
	FPMUL      r8, r3, r26
	LOAD      r30, r4, 5
	FPMUL      r27, r3, r30
	LOAD      r24, r4, 6
	FPMUL      r7, r3, r24
	LWI       r21, r1, 228
	LWI       r12, r1, 260
$BB13_137:                              # %if.end34.i566
                                        #   in Loop: Header=BB13_26 Depth=2
	LWI       r25, r1, 240
$BB13_138:                              # %do.body.i.i577
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	RAND      r3
	RAND      r4
	FPADD      r4, r4, r4
	ORI       r5, r0, -1082130432
	FPADD      r4, r4, r5
	FPADD      r3, r3, r3
	FPADD      r5, r3, r5
	FPMUL      r6, r5, r5
	FPMUL      r9, r4, r4
	FPADD      r3, r6, r9
	ORI       r10, r0, 1065353216
	FPGE   r10, r3, r10
	bneid     r10, ($BB13_140)
	ADDI      r3, r0, 1
# BB#139:                               # %do.body.i.i577
                                        #   in Loop: Header=BB13_138 Depth=3
	ADDI      r3, r0, 0
$BB13_140:                              # %do.body.i.i577
                                        #   in Loop: Header=BB13_138 Depth=3
	bneid     r3, ($BB13_138)
	NOP    
# BB#141:                               # %do.end.i.i583
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r3, r0, 0
	FPGE   r10, r21, r3
	FPUN   r3, r21, r3
	BITOR        r3, r3, r10
	bneid     r3, ($BB13_143)
	ADDI      r10, r0, 1
# BB#142:                               # %do.end.i.i583
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r10, r0, 0
$BB13_143:                              # %do.end.i.i583
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r3, r0, 1065353216
	FPRSUB     r6, r6, r3
	FPRSUB     r6, r9, r6
	FPINVSQRT r9, r6
	ADD      r6, r21, r0
	bneid     r10, ($BB13_145)
	LWI       r29, r1, 224
# BB#144:                               # %cond.true.i55.i.i.i585
                                        #   in Loop: Header=BB13_26 Depth=2
	FPNEG      r6, r21
$BB13_145:                              # %_ZL4Fabsf.exit57.i.i.i588
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r10, r0, 0
	FPGE   r11, r12, r10
	FPUN   r10, r12, r10
	BITOR        r10, r10, r11
	bneid     r10, ($BB13_147)
	ADDI      r11, r0, 1
# BB#146:                               # %_ZL4Fabsf.exit57.i.i.i588
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r11, r0, 0
$BB13_147:                              # %_ZL4Fabsf.exit57.i.i.i588
                                        #   in Loop: Header=BB13_26 Depth=2
	bneid     r11, ($BB13_149)
	ADD      r10, r12, r0
# BB#148:                               # %cond.true.i50.i.i.i590
                                        #   in Loop: Header=BB13_26 Depth=2
	FPNEG      r10, r12
$BB13_149:                              # %_ZL4Fabsf.exit52.i.i.i593
                                        #   in Loop: Header=BB13_26 Depth=2
	ADD      r22, r12, r0
	ORI       r11, r0, 0
	FPGE   r12, r29, r11
	FPUN   r11, r29, r11
	BITOR        r11, r11, r12
	bneid     r11, ($BB13_151)
	ADDI      r12, r0, 1
# BB#150:                               # %_ZL4Fabsf.exit52.i.i.i593
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r12, r0, 0
$BB13_151:                              # %_ZL4Fabsf.exit52.i.i.i593
                                        #   in Loop: Header=BB13_26 Depth=2
	bneid     r12, ($BB13_153)
	ADD      r11, r29, r0
# BB#152:                               # %cond.true.i.i.i.i595
                                        #   in Loop: Header=BB13_26 Depth=2
	FPNEG      r11, r29
$BB13_153:                              # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	FPGE   r12, r6, r10
	FPUN   r20, r6, r10
	BITOR        r12, r20, r12
	ADDI      r20, r0, 1
	bneid     r12, ($BB13_155)
	LWI       r28, r1, 244
# BB#154:                               # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r20, r0, 0
$BB13_155:                              # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	FPDIV      r3, r3, r9
	ORI       r12, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r20, ($BB13_159)
	NOP    
# BB#156:                               # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	FPLT   r6, r6, r11
	bneid     r6, ($BB13_158)
	ADDI      r20, r0, 1
# BB#157:                               # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r20, r0, 0
$BB13_158:                              # %_ZL4Fabsf.exit.i.i.i600
                                        #   in Loop: Header=BB13_26 Depth=2
	bneid     r20, ($BB13_163)
	ADD      r6, r9, r0
$BB13_159:                              # %if.else.i.i.i602
                                        #   in Loop: Header=BB13_26 Depth=2
	FPLT   r6, r10, r11
	bneid     r6, ($BB13_161)
	ADDI      r10, r0, 1
# BB#160:                               # %if.else.i.i.i602
                                        #   in Loop: Header=BB13_26 Depth=2
	ADDI      r10, r0, 0
$BB13_161:                              # %if.else.i.i.i602
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r6, r0, 1065353216
	ORI       r9, r0, 0
	bneid     r10, ($BB13_163)
	ADD      r12, r9, r0
# BB#162:                               # %if.else18.i.i.i603
                                        #   in Loop: Header=BB13_26 Depth=2
	ORI       r6, r0, 0
	ORI       r9, r0, 1065353216
	ADD      r12, r6, r0
$BB13_163:                              # %_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color.exit651
                                        #   in Loop: Header=BB13_26 Depth=2
	FPMUL      r10, r29, r6
	FPMUL      r11, r22, r9
	FPRSUB     r11, r10, r11
	FPMUL      r6, r21, r6
	FPMUL      r10, r22, r12
	FPRSUB     r6, r10, r6
	FPMUL      r9, r21, r9
	FPMUL      r10, r29, r12
	FPRSUB     r12, r9, r10
	FPMUL      r9, r29, r12
	FPMUL      r10, r22, r6
	FPRSUB     r9, r9, r10
	FPMUL      r10, r21, r6
	FPMUL      r20, r29, r11
	FPRSUB     r10, r10, r20
	FPMUL      r10, r10, r4
	FPMUL      r20, r12, r5
	FPADD      r10, r20, r10
	FPMUL      r9, r9, r4
	FPMUL      r20, r11, r5
	FPADD      r9, r20, r9
	FPMUL      r20, r21, r3
	FPADD      r9, r9, r20
	FPMUL      r20, r22, r3
	FPADD      r10, r10, r20
	FPMUL      r11, r22, r11
	FPMUL      r12, r21, r12
	FPMUL      r8, r8, r28
	LWI       r21, r1, 256
	FPMUL      r20, r27, r21
	ADD      r23, r21, r0
	LWI       r22, r1, 248
	FPMUL      r7, r7, r22
	LWI       r21, r1, 232
	FPADD      r21, r21, r7
	SWI       r21, r1, 232
	LWI       r7, r1, 236
	FPADD      r7, r7, r20
	SWI       r7, r1, 236
	FPADD      r25, r25, r8
	FPMUL      r7, r10, r10
	FPMUL      r8, r9, r9
	FPADD      r7, r8, r7
	FPRSUB     r8, r11, r12
	FPMUL      r4, r8, r4
	FPMUL      r5, r6, r5
	FPADD      r4, r5, r4
	FPMUL      r3, r29, r3
	FPADD      r3, r4, r3
	FPMUL      r4, r3, r3
	FPADD      r4, r7, r4
	FPINVSQRT r4, r4
	ORI       r5, r0, 1065353216
	LWI       r6, r1, 200
	ANDI      r6, r6, 255
	bneid     r6, ($BB13_25)
	NOP    
	brid      ($BB13_286)
	NOP    
$BB13_3:                                #   in Loop: Header=BB13_2 Depth=1
	SWI       r25, r1, 236
	SWI       r25, r1, 232
$BB13_286:                              # %for.end
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r8, r1, 380
	FPDIV      r3, r25, r8
	ORI       r4, r0, 0
	FPLT   r6, r3, r4
	bneid     r6, ($BB13_288)
	ADDI      r5, r0, 1
# BB#287:                               # %for.end
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r5, r0, 0
$BB13_288:                              # %for.end
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r5, ($BB13_293)
	NOP    
# BB#289:                               # %cond.false.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ORI       r4, r0, 1065353216
	FPGE   r6, r3, r4
	bneid     r6, ($BB13_291)
	ADDI      r5, r0, 1
# BB#290:                               # %cond.false.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r5, r0, 0
$BB13_291:                              # %cond.false.i
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r5, ($BB13_293)
	NOP    
# BB#292:                               # %cond.false5.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADD      r4, r3, r0
$BB13_293:                              # %cond.end7.i
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r3, r1, 236
	FPDIV      r5, r3, r8
	ORI       r3, r0, 0
	FPLT   r7, r5, r3
	ADDI      r6, r0, 1
	bneid     r7, ($BB13_295)
	LWI       r9, r1, 292
# BB#294:                               # %cond.end7.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r6, r0, 0
$BB13_295:                              # %cond.end7.i
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r6, ($BB13_300)
	NOP    
# BB#296:                               # %cond.false12.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ORI       r3, r0, 1065353216
	FPGE   r7, r5, r3
	bneid     r7, ($BB13_298)
	ADDI      r6, r0, 1
# BB#297:                               # %cond.false12.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r6, r0, 0
$BB13_298:                              # %cond.false12.i
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r6, ($BB13_300)
	NOP    
# BB#299:                               # %cond.false16.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADD      r3, r5, r0
$BB13_300:                              # %cond.end20.i
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r5, r1, 232
	FPDIV      r6, r5, r8
	ORI       r5, r0, 0
	FPLT   r8, r6, r5
	bneid     r8, ($BB13_302)
	ADDI      r7, r0, 1
# BB#301:                               # %cond.end20.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r7, r0, 0
$BB13_302:                              # %cond.end20.i
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r7, ($BB13_307)
	NOP    
# BB#303:                               # %cond.false25.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ORI       r5, r0, 1065353216
	FPGE   r8, r6, r5
	bneid     r8, ($BB13_305)
	ADDI      r7, r0, 1
# BB#304:                               # %cond.false25.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADDI      r7, r0, 0
$BB13_305:                              # %cond.false25.i
                                        #   in Loop: Header=BB13_2 Depth=1
	bneid     r7, ($BB13_307)
	NOP    
# BB#306:                               # %cond.false29.i
                                        #   in Loop: Header=BB13_2 Depth=1
	ADD      r5, r6, r0
$BB13_307:                              # %_ZN5Image3setEiiRK5Color.exit
                                        #   in Loop: Header=BB13_2 Depth=1
	LWI       r6, r1, 360
	LWI       r7, r1, 364
	ADD      r6, r6, r7
	MULI      r6, r6, 3
	LWI       r7, r1, 372
	ADD      r6, r6, r7
	STORE     r6, r4, 0
	STORE     r6, r3, 1
	STORE     r6, r5, 2
	ATOMIC_INC r4, 0
	LWI       r3, r1, 376
	CMP       r3, r3, r4
	bltid     r3, ($BB13_2)
	NOP    
$BB13_308:                              # %for.end55
	LWI       r31, r1, 0
	LWI       r30, r1, 4
	LWI       r29, r1, 8
	LWI       r28, r1, 12
	LWI       r27, r1, 16
	LWI       r26, r1, 20
	LWI       r25, r1, 24
	LWI       r24, r1, 28
	LWI       r23, r1, 32
	LWI       r22, r1, 36
	LWI       r21, r1, 40
	LWI       r20, r1, 44
	rtsd      r15, 8
	ADDI      r1, r1, 400
	.end	_Z9trax_mainv
$tmp13:
	.size	_Z9trax_mainv, ($tmp13)-_Z9trax_mainv

	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
	.frame	r1,4,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r15, r1, 0
	brlid     r15, _Z9trax_mainv
	NOP    
	ADD      r3, r0, r0
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	main
$tmp14:
	.size	main, ($tmp14)-main

	.globl	_Z6printfPKcz
	.align	2
	.type	_Z6printfPKcz,@function
	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
	.frame	r1,4,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r10, r1, 28
	SWI       r9, r1, 24
	SWI       r8, r1, 20
	SWI       r7, r1, 16
	SWI       r6, r1, 12
	SWI       r5, r1, 8
	ADDI      r3, r1, 8
	PRINTF     r3
	ADD      r3, r0, r0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_Z6printfPKcz
$tmp15:
	.size	_Z6printfPKcz, ($tmp15)-_Z6printfPKcz


	.globl	_ZN5ImageC1Eiii
_ZN5ImageC1Eiii = _ZN5ImageC2Eiii
	.globl	_ZN6SphereC1ERK6Vectorfii
_ZN6SphereC1ERK6Vectorfii = _ZN6SphereC2ERK6Vectorfii
	.globl	_ZN6SphereC1Ev
_ZN6SphereC1Ev = _ZN6SphereC2Ev
	.globl	_ZN5LightC1Ev
_ZN5LightC1Ev = _ZN5LightC2Ev
	.globl	_ZN5LightC1ERK6VectorRK5Colori
_ZN5LightC1ERK6VectorRK5Colori = _ZN5LightC2ERK6VectorRK5Colori
	.globl	_ZN5LightC1ERK6VectorRK5Colorf
_ZN5LightC1ERK6VectorRK5Colorf = _ZN5LightC2ERK6VectorRK5Colorf
