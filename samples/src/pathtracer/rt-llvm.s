#BEGIN Preamble
	REG	r0
	REG	r1
	REG	r2
	REG	r3
	REG	r4
	REG	r5
	REG	r6
	REG	r7
	REG	r8
	REG	r9
	REG	r10
	REG	r11
	REG	r12
	REG	r13
	REG	r14
	REG	r15
	REG	r16
	REG	r17
	REG	r18
	REG	r19
	REG	r20
	REG	r21
	REG	r22
	REG	r23
	REG	r24
	REG	r25
	REG	r26
	REG	r27
	REG	r28
	REG	r29
	REG	r30
	REG	r31
	LOADIMM	r0, 0
	ADDI	r1, r0, 8192
start:	brlid	r15, main
	NOP
	HALT
abort:	ADDI	r5, r0, -1
	PRINT	r5
	HALT
#END Preamble
#	.file	"rt.bc"
#	.text
#	.globl	_ZN5ImageC2Eiii
#	.align	2
#	.type	_ZN5ImageC2Eiii,@function
#	.ent	_ZN5ImageC2Eiii         # @_ZN5ImageC2Eiii
_ZN5ImageC2Eiii:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r6,	r5,	0
	SWI	r7,	r5,	4
	rtsd	r15,	8
	SWI	r8,	r5,	8
#	.end	_ZN5ImageC2Eiii
$0tmp0:
#	.size	_ZN5ImageC2Eiii, ($tmp0)-_ZN5ImageC2Eiii

#	.globl	_Z8getOrthoRK6Vector
#	.align	2
#	.type	_Z8getOrthoRK6Vector,@function
#	.ent	_Z8getOrthoRK6Vector    # @_Z8getOrthoRK6Vector
_Z8getOrthoRK6Vector:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	ORI	r4,	r0,	0
	FPGE	r7,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r7,	r4,	r7
	bneid	r7,	$0BB1_2
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB1_2:
	bneid	r4,	$0BB1_4
	ADD	r7,	r3,	r0
	FPNEG	r7,	r3
$0BB1_4:
	LWI	r4,	r6,	4
	ORI	r8,	r0,	0
	FPGE	r9,	r4,	r8
	FPUN	r8,	r4,	r8
	BITOR	r8,	r8,	r9
	bneid	r8,	$0BB1_6
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB1_6:
	bneid	r9,	$0BB1_8
	ADD	r8,	r4,	r0
	FPNEG	r8,	r4
$0BB1_8:
	LWI	r6,	r6,	8
	ORI	r9,	r0,	0
	FPGE	r10,	r6,	r9
	FPUN	r9,	r6,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB1_10
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB1_10:
	bneid	r10,	$0BB1_12
	ADD	r9,	r6,	r0
	FPNEG	r9,	r6
$0BB1_12:
	FPGE	r10,	r7,	r8
	FPUN	r11,	r7,	r8
	BITOR	r10,	r11,	r10
	bneid	r10,	$0BB1_14
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB1_14:
	ORI	r11,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r12,	$0BB1_18
	NOP
	FPLT	r7,	r7,	r9
	bneid	r7,	$0BB1_17
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB1_17:
	bneid	r12,	$0BB1_22
	ADD	r7,	r10,	r0
$0BB1_18:
	FPLT	r7,	r8,	r9
	bneid	r7,	$0BB1_20
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB1_20:
	ORI	r7,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r8,	$0BB1_22
	ADD	r11,	r10,	r0
	ORI	r7,	r0,	0
	ORI	r10,	r0,	1065353216
	ADD	r11,	r7,	r0
$0BB1_22:
	FPMUL	r8,	r6,	r7
	FPMUL	r9,	r4,	r10
	FPRSUB	r8,	r8,	r9
	SWI	r8,	r5,	0
	FPMUL	r8,	r3,	r10
	FPMUL	r6,	r6,	r11
	FPRSUB	r6,	r8,	r6
	SWI	r6,	r5,	4
	FPMUL	r4,	r4,	r11
	FPMUL	r3,	r3,	r7
	FPRSUB	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_Z8getOrthoRK6Vector
$0tmp1:
#	.size	_Z8getOrthoRK6Vector, ($tmp1)-_Z8getOrthoRK6Vector

#	.globl	_Z16randomReflectionRK6Vector
#	.align	2
#	.type	_Z16randomReflectionRK6Vector,@function
#	.ent	_Z16randomReflectionRK6Vector # @_Z16randomReflectionRK6Vector
_Z16randomReflectionRK6Vector:
#	.frame	r1,12,r15
#	.mask	0x700000
	ADDI	r1,	r1,	-12
	SWI	r20,	r1,	8
	SWI	r21,	r1,	4
	SWI	r22,	r1,	0
	SWI	r0,	r5,	0
	SWI	r0,	r5,	4
	SWI	r0,	r5,	8
$0BB2_1:
	RAND	r4
	RAND	r3
	FPADD	r3,	r3,	r3
	ORI	r7,	r0,	-1082130432
	FPADD	r3,	r3,	r7
	FPADD	r4,	r4,	r4
	FPADD	r4,	r4,	r7
	FPMUL	r7,	r4,	r4
	FPMUL	r8,	r3,	r3
	FPADD	r9,	r7,	r8
	ORI	r10,	r0,	1065353216
	FPGE	r10,	r9,	r10
	bneid	r10,	$0BB2_3
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB2_3:
	bneid	r9,	$0BB2_1
	NOP
	ORI	r9,	r0,	1065353216
	FPRSUB	r7,	r7,	r9
	FPRSUB	r7,	r8,	r7
	FPINVSQRT	r10,	r7
	LWI	r7,	r6,	0
	ORI	r8,	r0,	0
	FPGE	r11,	r7,	r8
	FPUN	r8,	r7,	r8
	BITOR	r11,	r8,	r11
	bneid	r11,	$0BB2_6
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB2_6:
	bneid	r8,	$0BB2_8
	ADD	r11,	r7,	r0
	FPNEG	r11,	r7
$0BB2_8:
	LWI	r8,	r6,	4
	ORI	r12,	r0,	0
	FPGE	r20,	r8,	r12
	FPUN	r12,	r8,	r12
	BITOR	r12,	r12,	r20
	bneid	r12,	$0BB2_10
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB2_10:
	bneid	r20,	$0BB2_12
	ADD	r12,	r8,	r0
	FPNEG	r12,	r8
$0BB2_12:
	LWI	r6,	r6,	8
	ORI	r20,	r0,	0
	FPGE	r21,	r6,	r20
	FPUN	r20,	r6,	r20
	BITOR	r20,	r20,	r21
	bneid	r20,	$0BB2_14
	ADDI	r21,	r0,	1
	ADDI	r21,	r0,	0
$0BB2_14:
	bneid	r21,	$0BB2_16
	ADD	r20,	r6,	r0
	FPNEG	r20,	r6
$0BB2_16:
	FPGE	r21,	r11,	r12
	FPUN	r22,	r11,	r12
	BITOR	r21,	r22,	r21
	bneid	r21,	$0BB2_18
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB2_18:
	FPDIV	r9,	r9,	r10
	ORI	r21,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r22,	$0BB2_22
	NOP
	FPLT	r22,	r11,	r20
	bneid	r22,	$0BB2_21
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB2_21:
	bneid	r11,	$0BB2_26
	ADD	r22,	r10,	r0
$0BB2_22:
	FPLT	r10,	r12,	r20
	bneid	r10,	$0BB2_24
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB2_24:
	ORI	r22,	r0,	1065353216
	ORI	r10,	r0,	0
	bneid	r11,	$0BB2_26
	ADD	r21,	r10,	r0
	ORI	r22,	r0,	0
	ORI	r10,	r0,	1065353216
	ADD	r21,	r22,	r0
$0BB2_26:
	FPMUL	r11,	r8,	r21
	FPMUL	r12,	r7,	r22
	FPRSUB	r11,	r11,	r12
	FPMUL	r12,	r6,	r21
	FPMUL	r20,	r7,	r10
	FPRSUB	r12,	r20,	r12
	FPMUL	r20,	r6,	r12
	FPMUL	r21,	r8,	r11
	FPRSUB	r20,	r20,	r21
	FPMUL	r21,	r6,	r22
	FPMUL	r10,	r8,	r10
	FPRSUB	r21,	r21,	r10
	FPMUL	r10,	r7,	r11
	FPMUL	r22,	r6,	r21
	FPRSUB	r22,	r10,	r22
	FPMUL	r10,	r20,	r3
	FPMUL	r20,	r21,	r4
	FPADD	r10,	r20,	r10
	FPMUL	r20,	r7,	r9
	FPADD	r10,	r10,	r20
	FPMUL	r20,	r22,	r3
	FPMUL	r22,	r12,	r4
	FPADD	r20,	r22,	r20
	FPMUL	r22,	r8,	r9
	FPADD	r20,	r20,	r22
	FPMUL	r8,	r8,	r21
	FPMUL	r12,	r7,	r12
	FPMUL	r7,	r20,	r20
	FPMUL	r21,	r10,	r10
	FPADD	r7,	r21,	r7
	FPRSUB	r8,	r8,	r12
	FPMUL	r3,	r8,	r3
	FPMUL	r4,	r11,	r4
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r6,	r9
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r7,	r4
	FPINVSQRT	r4,	r4
	ORI	r6,	r0,	1065353216
	FPDIV	r4,	r6,	r4
	FPDIV	r4,	r6,	r4
	FPMUL	r6,	r10,	r4
	SWI	r6,	r5,	0
	FPMUL	r6,	r20,	r4
	SWI	r6,	r5,	4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r5,	8
	LWI	r22,	r1,	0
	LWI	r21,	r1,	4
	LWI	r20,	r1,	8
	rtsd	r15,	8
	ADDI	r1,	r1,	12
#	.end	_Z16randomReflectionRK6Vector
$0tmp2:
#	.size	_Z16randomReflectionRK6Vector, ($tmp2)-_Z16randomReflectionRK6Vector

#	.globl	_Z20randomReflectionConeRK6Vectorf
#	.align	2
#	.type	_Z20randomReflectionConeRK6Vectorf,@function
#	.ent	_Z20randomReflectionConeRK6Vectorf # @_Z20randomReflectionConeRK6Vectorf
_Z20randomReflectionConeRK6Vectorf:
#	.frame	r1,16,r15
#	.mask	0xf00000
	ADDI	r1,	r1,	-16
	SWI	r20,	r1,	12
	SWI	r21,	r1,	8
	SWI	r22,	r1,	4
	SWI	r23,	r1,	0
	SWI	r0,	r5,	0
	SWI	r0,	r5,	4
	SWI	r0,	r5,	8
$0BB3_1:
	RAND	r3
	RAND	r4
	FPADD	r8,	r4,	r4
	ORI	r4,	r0,	-1082130432
	FPADD	r8,	r8,	r4
	FPADD	r3,	r3,	r3
	FPADD	r9,	r3,	r4
	FPMUL	r3,	r9,	r9
	FPMUL	r4,	r8,	r8
	FPADD	r10,	r3,	r4
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r10,	r11
	bneid	r11,	$0BB3_3
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB3_3:
	bneid	r10,	$0BB3_1
	NOP
	ORI	r11,	r0,	1065353216
	FPRSUB	r3,	r3,	r11
	FPRSUB	r3,	r4,	r3
	FPINVSQRT	r12,	r3
	LWI	r3,	r6,	0
	ORI	r4,	r0,	0
	FPGE	r10,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r10,	r4,	r10
	bneid	r10,	$0BB3_6
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB3_6:
	bneid	r4,	$0BB3_8
	ADD	r20,	r3,	r0
	FPNEG	r20,	r3
$0BB3_8:
	LWI	r4,	r6,	4
	ORI	r10,	r0,	0
	FPGE	r21,	r4,	r10
	FPUN	r10,	r4,	r10
	BITOR	r21,	r10,	r21
	bneid	r21,	$0BB3_10
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB3_10:
	bneid	r10,	$0BB3_12
	ADD	r21,	r4,	r0
	FPNEG	r21,	r4
$0BB3_12:
	LWI	r6,	r6,	8
	ORI	r10,	r0,	0
	FPGE	r22,	r6,	r10
	FPUN	r10,	r6,	r10
	BITOR	r22,	r10,	r22
	bneid	r22,	$0BB3_14
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB3_14:
	bneid	r10,	$0BB3_16
	ADD	r22,	r6,	r0
	FPNEG	r22,	r6
$0BB3_16:
	FPGE	r10,	r20,	r21
	FPUN	r23,	r20,	r21
	BITOR	r10,	r23,	r10
	bneid	r10,	$0BB3_18
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB3_18:
	FPMUL	r10,	r8,	r7
	FPMUL	r8,	r9,	r7
	FPDIV	r7,	r11,	r12
	ORI	r12,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r23,	$0BB3_22
	NOP
	FPLT	r20,	r20,	r22
	bneid	r20,	$0BB3_21
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB3_21:
	bneid	r11,	$0BB3_26
	ADD	r20,	r9,	r0
$0BB3_22:
	FPLT	r9,	r21,	r22
	bneid	r9,	$0BB3_24
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB3_24:
	ORI	r20,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r11,	$0BB3_26
	ADD	r12,	r9,	r0
	ORI	r20,	r0,	0
	ORI	r9,	r0,	1065353216
	ADD	r12,	r20,	r0
$0BB3_26:
	FPMUL	r11,	r4,	r12
	FPMUL	r21,	r3,	r20
	FPRSUB	r11,	r11,	r21
	FPMUL	r12,	r6,	r12
	FPMUL	r21,	r3,	r9
	FPRSUB	r12,	r21,	r12
	FPMUL	r21,	r6,	r12
	FPMUL	r22,	r4,	r11
	FPRSUB	r22,	r21,	r22
	FPMUL	r20,	r6,	r20
	FPMUL	r9,	r4,	r9
	FPRSUB	r21,	r20,	r9
	FPMUL	r9,	r3,	r11
	FPMUL	r20,	r6,	r21
	FPRSUB	r20,	r9,	r20
	FPMUL	r9,	r22,	r10
	FPMUL	r22,	r21,	r8
	FPADD	r9,	r22,	r9
	FPMUL	r22,	r3,	r7
	FPADD	r9,	r9,	r22
	FPMUL	r20,	r20,	r10
	FPMUL	r22,	r12,	r8
	FPADD	r20,	r22,	r20
	FPMUL	r22,	r4,	r7
	FPADD	r20,	r20,	r22
	FPMUL	r21,	r4,	r21
	FPMUL	r3,	r3,	r12
	FPMUL	r4,	r20,	r20
	FPMUL	r12,	r9,	r9
	FPADD	r4,	r12,	r4
	FPRSUB	r3,	r21,	r3
	FPMUL	r3,	r3,	r10
	FPMUL	r8,	r11,	r8
	FPADD	r3,	r8,	r3
	FPMUL	r6,	r6,	r7
	FPADD	r3,	r3,	r6
	FPMUL	r6,	r3,	r3
	FPADD	r4,	r4,	r6
	FPINVSQRT	r4,	r4
	ORI	r6,	r0,	1065353216
	FPDIV	r4,	r6,	r4
	FPDIV	r4,	r6,	r4
	FPMUL	r6,	r9,	r4
	SWI	r6,	r5,	0
	FPMUL	r6,	r20,	r4
	SWI	r6,	r5,	4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r5,	8
	LWI	r23,	r1,	0
	LWI	r22,	r1,	4
	LWI	r21,	r1,	8
	LWI	r20,	r1,	12
	rtsd	r15,	8
	ADDI	r1,	r1,	16
#	.end	_Z20randomReflectionConeRK6Vectorf
$0tmp3:
#	.size	_Z20randomReflectionConeRK6Vectorf, ($tmp3)-_Z20randomReflectionConeRK6Vectorf

#	.globl	_ZN6SphereC2ERK6Vectorfii
#	.align	2
#	.type	_ZN6SphereC2ERK6Vectorfii,@function
#	.ent	_ZN6SphereC2ERK6Vectorfii # @_ZN6SphereC2ERK6Vectorfii
_ZN6SphereC2ERK6Vectorfii:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r9,	r5,	0
	SWI	r8,	r5,	4
	LWI	r3,	r6,	0
	SWI	r3,	r5,	8
	LWI	r3,	r6,	4
	SWI	r3,	r5,	12
	LWI	r3,	r6,	8
	SWI	r3,	r5,	16
	rtsd	r15,	8
	SWI	r7,	r5,	20
#	.end	_ZN6SphereC2ERK6Vectorfii
$0tmp4:
#	.size	_ZN6SphereC2ERK6Vectorfii, ($tmp4)-_ZN6SphereC2ERK6Vectorfii

#	.globl	_ZN6SphereC2Ev
#	.align	2
#	.type	_ZN6SphereC2Ev,@function
#	.ent	_ZN6SphereC2Ev          # @_ZN6SphereC2Ev
_ZN6SphereC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	NOP
#	.end	_ZN6SphereC2Ev
$0tmp5:
#	.size	_ZN6SphereC2Ev, ($tmp5)-_ZN6SphereC2Ev

#	.globl	_ZN5LightC2Ev
#	.align	2
#	.type	_ZN5LightC2Ev,@function
#	.ent	_ZN5LightC2Ev           # @_ZN5LightC2Ev
_ZN5LightC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	12
	SWI	r3,	r5,	16
	SWI	r3,	r5,	20
	SWI	r3,	r5,	24
	rtsd	r15,	8
	SWI	r0,	r5,	28
#	.end	_ZN5LightC2Ev
$0tmp6:
#	.size	_ZN5LightC2Ev, ($tmp6)-_ZN5LightC2Ev

#	.globl	_ZN5LightC2ERK6VectorRK5Colori
#	.align	2
#	.type	_ZN5LightC2ERK6VectorRK5Colori,@function
#	.ent	_ZN5LightC2ERK6VectorRK5Colori # @_ZN5LightC2ERK6VectorRK5Colori
_ZN5LightC2ERK6VectorRK5Colori:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r9,	r6,	4
	SWI	r9,	r5,	4
	LWI	r4,	r6,	8
	SWI	r4,	r5,	8
	LWI	r6,	r7,	0
	SWI	r6,	r5,	12
	LWI	r6,	r7,	4
	SWI	r6,	r5,	16
	LWI	r6,	r7,	8
	SWI	r6,	r5,	20
	LWI	r6,	r7,	12
	SWI	r6,	r5,	24
	SWI	r8,	r5,	28
	ADDI	r6,	r0,	1
	CMP	r6,	r6,	r8
	bneid	r6,	$0BB7_2
	NOP
	FPMUL	r6,	r9,	r9
	FPMUL	r3,	r3,	r3
	FPADD	r3,	r3,	r6
	FPMUL	r4,	r4,	r4
	FPADD	r3,	r3,	r4
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	FPDIV	r3,	r4,	r3
	LWI	r4,	r5,	0
	FPMUL	r4,	r4,	r3
	SWI	r4,	r5,	0
	LWI	r4,	r5,	4
	FPMUL	r4,	r4,	r3
	SWI	r4,	r5,	4
	LWI	r4,	r5,	8
	FPMUL	r3,	r4,	r3
	SWI	r3,	r5,	8
$0BB7_2:
	rtsd	r15,	8
	NOP
#	.end	_ZN5LightC2ERK6VectorRK5Colori
$0tmp7:
#	.size	_ZN5LightC2ERK6VectorRK5Colori, ($tmp7)-_ZN5LightC2ERK6VectorRK5Colori

#	.globl	_ZN5LightC2ERK6VectorRK5Colorf
#	.align	2
#	.type	_ZN5LightC2ERK6VectorRK5Colorf,@function
#	.ent	_ZN5LightC2ERK6VectorRK5Colorf # @_ZN5LightC2ERK6VectorRK5Colorf
_ZN5LightC2ERK6VectorRK5Colorf:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r4,	r6,	0
	SWI	r4,	r5,	0
	LWI	r9,	r6,	4
	SWI	r9,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	LWI	r6,	r7,	0
	SWI	r6,	r5,	12
	LWI	r6,	r7,	4
	SWI	r6,	r5,	16
	FPMUL	r4,	r4,	r4
	FPMUL	r6,	r9,	r9
	FPADD	r4,	r4,	r6
	LWI	r6,	r7,	8
	SWI	r6,	r5,	20
	FPMUL	r3,	r3,	r3
	FPADD	r3,	r4,	r3
	LWI	r4,	r7,	12
	SWI	r4,	r5,	24
	ADDI	r4,	r0,	2
	SWI	r4,	r5,	28
	SWI	r8,	r5,	32
	FPINVSQRT	r3,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	FPDIV	r3,	r4,	r3
	LWI	r4,	r5,	0
	FPMUL	r4,	r4,	r3
	SWI	r4,	r5,	0
	LWI	r4,	r5,	4
	FPMUL	r4,	r4,	r3
	SWI	r4,	r5,	4
	LWI	r4,	r5,	8
	FPMUL	r3,	r4,	r3
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5LightC2ERK6VectorRK5Colorf
$0tmp8:
#	.size	_ZN5LightC2ERK6VectorRK5Colorf, ($tmp8)-_ZN5LightC2ERK6VectorRK5Colorf

#	.globl	_ZNK5Light8getLightER5ColorR6VectorRKS2_
#	.align	2
#	.type	_ZNK5Light8getLightER5ColorR6VectorRKS2_,@function
#	.ent	_ZNK5Light8getLightER5ColorR6VectorRKS2_ # @_ZNK5Light8getLightER5ColorR6VectorRKS2_
_ZNK5Light8getLightER5ColorR6VectorRKS2_:
#	.frame	r1,16,r15
#	.mask	0xf00000
	ADDI	r1,	r1,	-16
	SWI	r20,	r1,	12
	SWI	r21,	r1,	8
	SWI	r22,	r1,	4
	SWI	r23,	r1,	0
	LWI	r4,	r5,	28
	ADDI	r3,	r0,	2
	CMP	r9,	r3,	r4
	ORI	r3,	r0,	-1082130432
	bneid	r9,	$0BB9_1
	NOP
	LWI	r3,	r5,	12
	SWI	r3,	r6,	0
	LWI	r3,	r5,	16
	SWI	r3,	r6,	4
	LWI	r3,	r5,	20
	SWI	r3,	r6,	8
	LWI	r3,	r5,	24
	SWI	r3,	r6,	12
	LWI	r6,	r5,	32
$0BB9_5:
	RAND	r3
	RAND	r4
	FPADD	r8,	r4,	r4
	ORI	r4,	r0,	-1082130432
	FPADD	r8,	r8,	r4
	FPADD	r3,	r3,	r3
	FPADD	r9,	r3,	r4
	FPMUL	r3,	r9,	r9
	FPMUL	r4,	r8,	r8
	FPADD	r10,	r3,	r4
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r10,	r11
	bneid	r11,	$0BB9_7
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB9_7:
	bneid	r10,	$0BB9_5
	NOP
	ORI	r11,	r0,	1065353216
	FPRSUB	r3,	r3,	r11
	FPRSUB	r3,	r4,	r3
	FPINVSQRT	r12,	r3
	LWI	r3,	r5,	0
	ORI	r4,	r0,	0
	FPGE	r10,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r10,	r4,	r10
	bneid	r10,	$0BB9_10
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB9_10:
	bneid	r4,	$0BB9_12
	ADD	r20,	r3,	r0
	FPNEG	r20,	r3
$0BB9_12:
	LWI	r4,	r5,	4
	ORI	r10,	r0,	0
	FPGE	r21,	r4,	r10
	FPUN	r10,	r4,	r10
	BITOR	r21,	r10,	r21
	bneid	r21,	$0BB9_14
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB9_14:
	bneid	r10,	$0BB9_16
	ADD	r21,	r4,	r0
	FPNEG	r21,	r4
$0BB9_16:
	LWI	r5,	r5,	8
	ORI	r10,	r0,	0
	FPGE	r22,	r5,	r10
	FPUN	r10,	r5,	r10
	BITOR	r22,	r10,	r22
	bneid	r22,	$0BB9_18
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB9_18:
	bneid	r10,	$0BB9_20
	ADD	r22,	r5,	r0
	FPNEG	r22,	r5
$0BB9_20:
	FPGE	r10,	r20,	r21
	FPUN	r23,	r20,	r21
	BITOR	r10,	r23,	r10
	bneid	r10,	$0BB9_22
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB9_22:
	FPMUL	r10,	r8,	r6
	FPMUL	r8,	r9,	r6
	FPDIV	r6,	r11,	r12
	ORI	r12,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r23,	$0BB9_26
	NOP
	FPLT	r20,	r20,	r22
	bneid	r20,	$0BB9_25
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB9_25:
	bneid	r11,	$0BB9_30
	ADD	r20,	r9,	r0
$0BB9_26:
	FPLT	r9,	r21,	r22
	bneid	r9,	$0BB9_28
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB9_28:
	ORI	r20,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r11,	$0BB9_30
	ADD	r12,	r9,	r0
	ORI	r20,	r0,	0
	ORI	r9,	r0,	1065353216
	ADD	r12,	r20,	r0
$0BB9_30:
	FPMUL	r11,	r4,	r12
	FPMUL	r21,	r3,	r20
	FPRSUB	r11,	r11,	r21
	FPMUL	r12,	r5,	r12
	FPMUL	r21,	r3,	r9
	FPRSUB	r12,	r21,	r12
	FPMUL	r21,	r5,	r12
	FPMUL	r22,	r4,	r11
	FPRSUB	r22,	r21,	r22
	FPMUL	r20,	r5,	r20
	FPMUL	r9,	r4,	r9
	FPRSUB	r21,	r20,	r9
	FPMUL	r9,	r3,	r11
	FPMUL	r20,	r5,	r21
	FPRSUB	r20,	r9,	r20
	FPMUL	r9,	r22,	r10
	FPMUL	r22,	r21,	r8
	FPADD	r9,	r22,	r9
	FPMUL	r22,	r3,	r6
	FPADD	r9,	r9,	r22
	FPMUL	r20,	r20,	r10
	FPMUL	r22,	r12,	r8
	FPADD	r20,	r22,	r20
	FPMUL	r22,	r4,	r6
	FPADD	r20,	r20,	r22
	FPMUL	r21,	r4,	r21
	FPMUL	r3,	r3,	r12
	FPMUL	r4,	r20,	r20
	FPMUL	r12,	r9,	r9
	FPADD	r4,	r12,	r4
	FPRSUB	r3,	r21,	r3
	FPMUL	r3,	r3,	r10
	FPMUL	r8,	r11,	r8
	FPADD	r3,	r8,	r3
	FPMUL	r5,	r5,	r6
	FPADD	r3,	r3,	r5
	FPMUL	r5,	r3,	r3
	FPADD	r4,	r4,	r5
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r5,	r9,	r4
	SWI	r5,	r7,	0
	FPMUL	r5,	r20,	r4
	SWI	r5,	r7,	4
	brid	$0BB9_32
	FPMUL	r3,	r3,	r4
$0BB9_1:
	ADDI	r9,	r0,	1
	CMP	r9,	r9,	r4
	bneid	r9,	$0BB9_2
	NOP
	LWI	r3,	r5,	12
	SWI	r3,	r6,	0
	LWI	r3,	r5,	16
	SWI	r3,	r6,	4
	LWI	r3,	r5,	20
	SWI	r3,	r6,	8
	LWI	r3,	r5,	24
	SWI	r3,	r6,	12
	LWI	r3,	r5,	0
	SWI	r3,	r7,	0
	LWI	r3,	r5,	4
	SWI	r3,	r7,	4
	LWI	r3,	r5,	8
$0BB9_32:
	SWI	r3,	r7,	8
	ORI	r3,	r0,	1203982336
$0BB9_33:
	LWI	r23,	r1,	0
	LWI	r22,	r1,	4
	LWI	r21,	r1,	8
	LWI	r20,	r1,	12
	rtsd	r15,	8
	ADDI	r1,	r1,	16
$0BB9_2:
	bneid	r4,	$0BB9_33
	NOP
	LWI	r3,	r5,	12
	SWI	r3,	r6,	0
	LWI	r3,	r5,	16
	SWI	r3,	r6,	4
	LWI	r3,	r5,	20
	SWI	r3,	r6,	8
	LWI	r3,	r5,	24
	SWI	r3,	r6,	12
	LWI	r3,	r8,	0
	LWI	r4,	r5,	0
	FPRSUB	r4,	r3,	r4
	LWI	r3,	r8,	4
	LWI	r6,	r5,	4
	FPRSUB	r6,	r3,	r6
	FPMUL	r3,	r6,	r6
	FPMUL	r9,	r4,	r4
	FPADD	r3,	r9,	r3
	LWI	r8,	r8,	8
	LWI	r5,	r5,	8
	FPRSUB	r5,	r8,	r5
	FPMUL	r8,	r5,	r5
	FPADD	r3,	r3,	r8
	FPINVSQRT	r3,	r3
	ORI	r8,	r0,	1065353216
	FPDIV	r3,	r8,	r3
	FPDIV	r8,	r8,	r3
	FPMUL	r4,	r4,	r8
	SWI	r4,	r7,	0
	FPMUL	r4,	r6,	r8
	SWI	r4,	r7,	4
	FPMUL	r4,	r5,	r8
	brid	$0BB9_33
	SWI	r4,	r7,	8
#	.end	_ZNK5Light8getLightER5ColorR6VectorRKS2_
$0tmp9:
#	.size	_ZNK5Light8getLightER5ColorR6VectorRKS2_, ($tmp9)-_ZNK5Light8getLightER5ColorR6VectorRKS2_

#	.globl	_Z15getTextureColorRK9HitRecordRKii
#	.align	2
#	.type	_Z15getTextureColorRK9HitRecordRKii,@function
#	.ent	_Z15getTextureColorRK9HitRecordRKii # @_Z15getTextureColorRK9HitRecordRKii
_Z15getTextureColorRK9HitRecordRKii:
#	.frame	r1,68,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-68
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	SWI	r5,	r1,	72
	LWI	r3,	r6,	12
	LOAD	r3,	r3,	9
	MULI	r9,	r3,	9
	LWI	r3,	r7,	0
	ADD	r5,	r3,	r9
	LOAD	r3,	r5,	0
	LOAD	r4,	r5,	1
	LOAD	r5,	r5,	2
	LWI	r5,	r7,	0
	ADD	r5,	r9,	r5
	ADDI	r11,	r5,	3
	LOAD	r10,	r11,	0
	LOAD	r5,	r11,	1
	LOAD	r11,	r11,	2
	LWI	r7,	r7,	0
	ADD	r7,	r9,	r7
	ADDI	r9,	r7,	6
	LOAD	r12,	r9,	0
	LOAD	r7,	r9,	1
	LOAD	r9,	r9,	2
	LWI	r9,	r6,	4
	FPMUL	r11,	r10,	r9
	LWI	r10,	r6,	0
	FPMUL	r3,	r3,	r10
	FPADD	r3,	r3,	r11
	ORI	r6,	r0,	1065353216
	FPRSUB	r6,	r10,	r6
	FPRSUB	r11,	r9,	r6
	FPMUL	r6,	r12,	r11
	FPADD	r3,	r3,	r6
	INTCONV	r6,	r3
	FPCONV	r6,	r6
	ADDI	r12,	r0,	1
	FPEQ	r20,	r3,	r6
	bneid	r20,	$0BB10_2
	NOP
	ADDI	r12,	r0,	0
$0BB10_2:
	FPMUL	r5,	r5,	r9
	FPMUL	r4,	r4,	r10
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r7,	r11
	FPADD	r10,	r4,	r5
	LOAD	r9,	r8,	0
	LOAD	r7,	r8,	1
	LOAD	r11,	r8,	2
	bneid	r12,	$0BB10_4
	NOP
	FPRSUB	r3,	r6,	r3
$0BB10_4:
	INTCONV	r4,	r10
	FPCONV	r4,	r4
	FPEQ	r6,	r10,	r4
	bneid	r6,	$0BB10_6
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB10_6:
	bneid	r5,	$0BB10_8
	NOP
	FPRSUB	r10,	r4,	r10
$0BB10_8:
	ORI	r4,	r0,	0
	FPGE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB10_10
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB10_10:
	bneid	r4,	$0BB10_12
	NOP
	ORI	r4,	r0,	1065353216
	FPADD	r3,	r3,	r4
$0BB10_12:
	ORI	r4,	r0,	0
	FPGE	r5,	r10,	r4
	FPUN	r4,	r10,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB10_14
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB10_14:
	bneid	r4,	$0BB10_16
	ADD	r21,	r11,	r0
	ORI	r4,	r0,	1065353216
	FPADD	r10,	r10,	r4
$0BB10_16:
	FPCONV	r4,	r7
	FPMUL	r11,	r3,	r4
	INTCONV	r3,	r11
	CMP	r3,	r7,	r3
	ADDI	r6,	r0,	0
	ADDI	r5,	r0,	1
	beqid	r3,	$0BB10_18
	ADD	r12,	r5,	r0
	ADD	r12,	r6,	r0
$0BB10_18:
	ORI	r3,	r0,	0
	bneid	r12,	$0BB10_20
	ADD	r4,	r3,	r0
	ADD	r4,	r11,	r0
$0BB10_20:
	FPGE	r11,	r4,	r3
	FPUN	r12,	r4,	r3
	BITOR	r20,	r12,	r11
	FPCONV	r11,	r9
	FPMUL	r10,	r10,	r11
	INTCONV	r11,	r10
	CMP	r12,	r9,	r11
	bneid	r20,	$0BB10_22
	ADD	r11,	r5,	r0
	ADD	r11,	r6,	r0
$0BB10_22:
	beqid	r12,	$0BB10_24
	NOP
	ADD	r5,	r6,	r0
$0BB10_24:
	bneid	r5,	$0BB10_26
	ADD	r20,	r21,	r0
	ADD	r3,	r10,	r0
$0BB10_26:
	bneid	r11,	$0BB10_31
	NOP
	FPNEG	r5,	r4
	INTCONV	r6,	r5
	RSUBI	r5,	r6,	0
	FPCONV	r10,	r5
	FPEQ	r11,	r10,	r4
	bneid	r11,	$0BB10_29
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB10_29:
	bneid	r10,	$0BB10_32
	NOP
	ADDI	r5,	r0,	-1
	brid	$0BB10_32
	BITXOR	r5,	r6,	r5
$0BB10_31:
	INTCONV	r5,	r4
$0BB10_32:
	ORI	r6,	r0,	0
	FPGE	r10,	r3,	r6
	FPUN	r6,	r3,	r6
	BITOR	r10,	r6,	r10
	bneid	r10,	$0BB10_34
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB10_34:
	DIV	r10,	r7,	r5
	MUL	r10,	r10,	r7
	RSUB	r5,	r10,	r5
	bsrai	r10,	r5,	31
	BITAND	r10,	r10,	r7
	ADD	r30,	r10,	r5
	ADDI	r5,	r30,	1
	DIV	r10,	r7,	r5
	MUL	r10,	r10,	r7
	bneid	r6,	$0BB10_39
	RSUB	r28,	r10,	r5
	FPNEG	r5,	r3
	INTCONV	r6,	r5
	RSUBI	r5,	r6,	0
	FPCONV	r10,	r5
	FPEQ	r11,	r10,	r3
	bneid	r11,	$0BB10_37
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB10_37:
	bneid	r10,	$0BB10_40
	NOP
	ADDI	r5,	r0,	-1
	brid	$0BB10_40
	BITXOR	r5,	r6,	r5
$0BB10_39:
	INTCONV	r5,	r3
$0BB10_40:
	DIV	r6,	r9,	r5
	MUL	r6,	r6,	r9
	RSUB	r5,	r6,	r5
	bsrai	r6,	r5,	31
	BITAND	r6,	r6,	r9
	ADD	r5,	r6,	r5
	FPCONV	r6,	r5
	FPRSUB	r3,	r6,	r3
	SWI	r3,	r1,	60
	FPCONV	r3,	r30
	FPRSUB	r3,	r3,	r4
	SWI	r3,	r1,	64
	MUL	r6,	r5,	r7
	ADDI	r3,	r5,	1
	DIV	r4,	r9,	r3
	MUL	r4,	r4,	r9
	RSUB	r10,	r4,	r3
	ADD	r4,	r6,	r30
	ADDI	r3,	r0,	4
	CMP	r3,	r3,	r20
	MUL	r5,	r4,	r20
	ORI	r9,	r0,	1065353216
	ORI	r11,	r0,	0
	ADDI	r4,	r8,	3
	bneid	r3,	$0BB10_41
	ADD	r5,	r5,	r4
	MUL	r7,	r10,	r7
	ADD	r3,	r7,	r30
	MUL	r3,	r3,	r20
	ADD	r8,	r7,	r28
	ADD	r6,	r6,	r28
	bslli	r6,	r6,	2
	ADD	r7,	r6,	r4
	bslli	r8,	r8,	2
	ADD	r6,	r3,	r4
	ADD	r3,	r8,	r4
	LOAD	r4,	r5,	1
	LOAD	r10,	r5,	2
	LOAD	r12,	r5,	3
	LOAD	r8,	r5,	0
	LOAD	r11,	r7,	1
	LOAD	r20,	r7,	2
	LOAD	r9,	r7,	3
	LOAD	r5,	r7,	0
	FPCONV	r7,	r12
	FPCONV	r12,	r10
	FPCONV	r10,	r20
	FPCONV	r11,	r11
	FPCONV	r20,	r8
	FPCONV	r22,	r4
	ORI	r4,	r0,	1132396544
	FPDIV	r8,	r12,	r4
	SWI	r8,	r1,	48
	FPDIV	r8,	r7,	r4
	LOAD	r7,	r6,	1
	LOAD	r12,	r6,	2
	LOAD	r21,	r6,	3
	LOAD	r23,	r6,	0
	FPCONV	r9,	r9
	FPDIV	r22,	r22,	r4
	FPDIV	r6,	r20,	r4
	SWI	r6,	r1,	52
	FPDIV	r11,	r11,	r4
	FPDIV	r6,	r10,	r4
	SWI	r6,	r1,	56
	LOAD	r10,	r3,	1
	LOAD	r6,	r3,	2
	FPCONV	r6,	r6
	FPCONV	r10,	r10
	FPCONV	r23,	r23
	FPCONV	r26,	r21
	FPCONV	r25,	r12
	FPDIV	r21,	r9,	r4
	LOAD	r9,	r3,	3
	LOAD	r3,	r3,	0
	FPCONV	r12,	r9
	FPCONV	r7,	r7
	FPCONV	r5,	r5
	FPDIV	r9,	r5,	r4
	FPCONV	r5,	r3
	FPDIV	r24,	r7,	r4
	FPDIV	r25,	r25,	r4
	FPDIV	r26,	r26,	r4
	FPDIV	r27,	r23,	r4
	FPDIV	r3,	r10,	r4
	FPDIV	r29,	r6,	r4
	FPDIV	r23,	r12,	r4
	brid	$0BB10_46
	FPDIV	r31,	r5,	r4
$0BB10_41:
	ADDI	r3,	r0,	3
	CMP	r3,	r3,	r20
	bneid	r3,	$0BB10_42
	NOP
	MUL	r3,	r10,	r7
	ADD	r7,	r3,	r30
	MUL	r7,	r7,	r20
	ADD	r3,	r3,	r28
	ADD	r6,	r6,	r28
	MULI	r6,	r6,	3
	ADD	r10,	r7,	r4
	ADD	r8,	r6,	r4
	MULI	r3,	r3,	3
	ADD	r6,	r3,	r4
	LOAD	r3,	r5,	0
	LOAD	r4,	r5,	1
	LOAD	r5,	r5,	2
	LOAD	r7,	r8,	0
	LOAD	r9,	r8,	1
	LOAD	r8,	r8,	2
	LOAD	r11,	r10,	0
	LOAD	r12,	r10,	1
	LOAD	r10,	r10,	2
	FPCONV	r20,	r5
	FPCONV	r21,	r4
	FPCONV	r5,	r10
	ORI	r4,	r0,	1132396544
	FPDIV	r10,	r21,	r4
	SWI	r10,	r1,	48
	LOAD	r10,	r6,	0
	FPCONV	r10,	r10
	FPCONV	r12,	r12
	FPCONV	r23,	r11
	FPCONV	r21,	r8
	FPDIV	r8,	r20,	r4
	LOAD	r11,	r6,	1
	LOAD	r20,	r6,	2
	FPCONV	r6,	r11
	FPCONV	r9,	r9
	FPCONV	r7,	r7
	FPCONV	r3,	r3
	FPDIV	r22,	r3,	r4
	FPDIV	r11,	r7,	r4
	FPCONV	r7,	r20
	FPDIV	r3,	r9,	r4
	SWI	r3,	r1,	56
	FPDIV	r21,	r21,	r4
	FPDIV	r24,	r23,	r4
	FPDIV	r25,	r12,	r4
	FPDIV	r26,	r5,	r4
	FPDIV	r3,	r10,	r4
	FPDIV	r29,	r6,	r4
	FPDIV	r23,	r7,	r4
	ORI	r9,	r0,	1065353216
	SWI	r9,	r1,	52
	ADD	r27,	r9,	r0
	brid	$0BB10_46
	ADD	r31,	r9,	r0
$0BB10_42:
	ADDI	r3,	r0,	1
	CMP	r12,	r3,	r20
	SWI	r11,	r1,	56
	ADD	r21,	r11,	r0
	SWI	r9,	r1,	52
	ADD	r8,	r11,	r0
	SWI	r11,	r1,	48
	ADD	r22,	r11,	r0
	ADD	r24,	r11,	r0
	ADD	r25,	r11,	r0
	ADD	r26,	r11,	r0
	ADD	r27,	r9,	r0
	ADD	r31,	r9,	r0
	ADD	r23,	r11,	r0
	ADD	r29,	r11,	r0
	bneid	r12,	$0BB10_46
	ADD	r3,	r11,	r0
	MUL	r7,	r10,	r7
	ADD	r3,	r6,	r28
	ADD	r6,	r7,	r28
	ADD	r7,	r7,	r30
	MUL	r7,	r7,	r20
	ADD	r7,	r7,	r4
	ADD	r8,	r3,	r4
	ADD	r3,	r6,	r4
	LOAD	r4,	r5,	0
	LOAD	r5,	r8,	0
	LOAD	r6,	r7,	0
	FPCONV	r7,	r4
	ORI	r4,	r0,	1132396544
	FPDIV	r8,	r7,	r4
	FPCONV	r6,	r6
	FPCONV	r5,	r5
	FPDIV	r11,	r5,	r4
	FPDIV	r24,	r6,	r4
	LOAD	r3,	r3,	0
	FPCONV	r3,	r3
	FPDIV	r23,	r3,	r4
	ORI	r9,	r0,	1065353216
	SWI	r11,	r1,	56
	ADD	r21,	r11,	r0
	SWI	r9,	r1,	52
	SWI	r8,	r1,	48
	ADD	r22,	r8,	r0
	ADD	r25,	r24,	r0
	ADD	r26,	r24,	r0
	ADD	r27,	r9,	r0
	ADD	r31,	r9,	r0
	ADD	r29,	r23,	r0
	ADD	r3,	r23,	r0
$0BB10_46:
	LWI	r28,	r1,	64
	FPMUL	r4,	r11,	r28
	ORI	r7,	r0,	1065353216
	LWI	r12,	r1,	60
	FPRSUB	r5,	r12,	r7
	FPMUL	r6,	r4,	r5
	FPRSUB	r4,	r28,	r7
	FPMUL	r7,	r22,	r4
	FPMUL	r7,	r7,	r5
	FPADD	r6,	r7,	r6
	FPMUL	r7,	r24,	r4
	FPMUL	r7,	r7,	r12
	FPADD	r6,	r6,	r7
	FPMUL	r8,	r8,	r4
	FPMUL	r7,	r21,	r28
	FPMUL	r7,	r7,	r5
	FPMUL	r8,	r8,	r5
	LWI	r10,	r1,	56
	FPMUL	r10,	r10,	r28
	FPMUL	r10,	r10,	r5
	LWI	r11,	r1,	48
	FPMUL	r11,	r11,	r4
	FPMUL	r5,	r11,	r5
	FPADD	r5,	r5,	r10
	FPADD	r7,	r8,	r7
	FPMUL	r3,	r3,	r28
	FPMUL	r3,	r3,	r12
	FPADD	r3,	r6,	r3
	FPMUL	r6,	r25,	r4
	FPMUL	r4,	r26,	r4
	LWI	r8,	r1,	72
	SWI	r3,	r8,	0
	FPMUL	r3,	r4,	r12
	FPADD	r3,	r7,	r3
	FPMUL	r4,	r6,	r12
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r29,	r28
	FPMUL	r5,	r5,	r12
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r23,	r28
	FPMUL	r5,	r5,	r12
	SWI	r4,	r8,	4
	FPADD	r3,	r3,	r5
	LWI	r4,	r1,	52
	FPADD	r4,	r4,	r9
	FPADD	r4,	r4,	r27
	FPADD	r4,	r4,	r31
	SWI	r3,	r8,	8
	ORI	r3,	r0,	1048576000
	FPMUL	r3,	r4,	r3
	SWI	r3,	r8,	12
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	68
#	.end	_Z15getTextureColorRK9HitRecordRKii
$0tmp10:
#	.size	_Z15getTextureColorRK9HitRecordRKii, ($tmp10)-_Z15getTextureColorRK9HitRecordRKii

#	.globl	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
#	.align	2
#	.type	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color,@function
#	.ent	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color # @_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color:
#	.frame	r1,260,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-260
	SWI	r20,	r1,	48
	SWI	r21,	r1,	44
	SWI	r22,	r1,	40
	SWI	r23,	r1,	36
	SWI	r24,	r1,	32
	SWI	r25,	r1,	28
	SWI	r26,	r1,	24
	SWI	r27,	r1,	20
	SWI	r28,	r1,	16
	SWI	r29,	r1,	12
	SWI	r30,	r1,	8
	SWI	r31,	r1,	4
	SWI	r10,	r1,	284
	SWI	r0,	r5,	0
	SWI	r0,	r5,	4
	SWI	r0,	r5,	8
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	12
	lbui	r3,	r6,	16
	beqid	r3,	$0BB11_91
	NOP
	ADD	r21,	r8,	r0
	SWI	r5,	r1,	228
	LWI	r3,	r6,	8
	LWI	r4,	r7,	12
	FPMUL	r4,	r4,	r3
	LWI	r5,	r7,	0
	FPADD	r11,	r5,	r4
	LWI	r4,	r7,	8
	LWI	r5,	r7,	20
	LWI	r8,	r7,	4
	LWI	r10,	r7,	16
	SWI	r11,	r9,	0
	FPMUL	r10,	r10,	r3
	FPADD	r8,	r8,	r10
	SWI	r8,	r9,	4
	FPMUL	r3,	r5,	r3
	FPADD	r3,	r4,	r3
	SWI	r3,	r9,	8
	LWI	r20,	r6,	12
	SWI	r6,	r1,	240
	LOAD	r4,	r20,	0
	LOAD	r5,	r20,	1
	LOAD	r3,	r20,	2
	ADDI	r10,	r20,	3
	LOAD	r8,	r10,	0
	LOAD	r12,	r10,	1
	LOAD	r11,	r10,	2
	ADDI	r20,	r20,	6
	LOAD	r10,	r20,	0
	LOAD	r6,	r20,	1
	FPRSUB	r5,	r6,	r5
	FPRSUB	r6,	r6,	r12
	LOAD	r20,	r20,	2
	FPRSUB	r12,	r20,	r11
	FPRSUB	r20,	r20,	r3
	FPMUL	r3,	r20,	r6
	FPMUL	r11,	r5,	r12
	FPRSUB	r3,	r3,	r11
	FPRSUB	r11,	r10,	r8
	FPRSUB	r4,	r10,	r4
	FPMUL	r8,	r4,	r12
	FPMUL	r10,	r20,	r11
	FPRSUB	r8,	r8,	r10
	FPMUL	r10,	r8,	r8
	FPMUL	r12,	r3,	r3
	FPADD	r10,	r12,	r10
	FPMUL	r5,	r5,	r11
	FPMUL	r4,	r4,	r6
	FPRSUB	r4,	r5,	r4
	FPMUL	r5,	r4,	r4
	FPADD	r5,	r10,	r5
	FPINVSQRT	r5,	r5
	ORI	r6,	r0,	1065353216
	FPDIV	r5,	r6,	r5
	FPDIV	r5,	r6,	r5
	FPMUL	r27,	r8,	r5
	LWI	r6,	r7,	16
	FPMUL	r6,	r27,	r6
	FPMUL	r8,	r3,	r5
	LWI	r3,	r7,	12
	FPMUL	r3,	r8,	r3
	FPADD	r3,	r3,	r6
	FPMUL	r26,	r4,	r5
	LWI	r4,	r7,	20
	FPMUL	r4,	r26,	r4
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB11_3
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB11_3:
	bneid	r3,	$0BB11_5
	ADD	r10,	r21,	r0
	FPNEG	r26,	r26
	FPNEG	r27,	r27
	FPNEG	r8,	r8
$0BB11_5:
	SWI	r8,	r1,	224
	ORI	r5,	r0,	981668463
	FPMUL	r3,	r26,	r5
	LWI	r4,	r9,	8
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r27,	r5
	LWI	r6,	r9,	4
	FPADD	r4,	r6,	r4
	FPMUL	r5,	r8,	r5
	LWI	r6,	r9,	0
	FPADD	r5,	r6,	r5
	SWI	r5,	r9,	0
	SWI	r4,	r9,	4
	SWI	r3,	r9,	8
	ORI	r22,	r0,	-1082130432
	ORI	r29,	r0,	1065353216
	LWI	r6,	r10,	40
	ADDI	r7,	r0,	2
	CMP	r7,	r7,	r6
	bneid	r7,	$0BB11_6
	NOP
	LWI	r20,	r10,	44
	LWI	r29,	r10,	32
	LWI	r30,	r10,	28
	LWI	r31,	r10,	24
	ADD	r28,	r10,	r0
$0BB11_10:
	RAND	r3
	RAND	r4
	FPADD	r5,	r4,	r4
	ORI	r4,	r0,	-1082130432
	FPADD	r12,	r5,	r4
	FPADD	r3,	r3,	r3
	FPADD	r21,	r3,	r4
	FPMUL	r4,	r21,	r21
	FPMUL	r5,	r12,	r12
	FPADD	r3,	r4,	r5
	ORI	r6,	r0,	1065353216
	FPGE	r6,	r3,	r6
	bneid	r6,	$0BB11_12
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB11_12:
	bneid	r3,	$0BB11_10
	NOP
	ORI	r3,	r0,	1065353216
	FPRSUB	r4,	r4,	r3
	FPRSUB	r4,	r5,	r4
	FPINVSQRT	r23,	r4
	LWI	r6,	r28,	12
	ORI	r4,	r0,	0
	FPGE	r5,	r6,	r4
	FPUN	r4,	r6,	r4
	BITOR	r4,	r4,	r5
	bneid	r4,	$0BB11_15
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_15:
	bneid	r5,	$0BB11_17
	ADD	r4,	r6,	r0
	FPNEG	r4,	r6
$0BB11_17:
	LWI	r7,	r28,	16
	ORI	r5,	r0,	0
	FPGE	r8,	r7,	r5
	FPUN	r5,	r7,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB11_19
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_19:
	bneid	r5,	$0BB11_21
	ADD	r24,	r7,	r0
	FPNEG	r24,	r7
$0BB11_21:
	LWI	r10,	r28,	20
	ORI	r5,	r0,	0
	FPGE	r8,	r10,	r5
	FPUN	r5,	r10,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB11_23
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_23:
	bneid	r5,	$0BB11_25
	ADD	r8,	r10,	r0
	FPNEG	r8,	r10
$0BB11_25:
	FPGE	r5,	r4,	r24
	FPUN	r11,	r4,	r24
	BITOR	r5,	r11,	r5
	bneid	r5,	$0BB11_27
	ADDI	r25,	r0,	1
	ADDI	r25,	r0,	0
$0BB11_27:
	FPMUL	r22,	r12,	r20
	FPMUL	r12,	r21,	r20
	FPDIV	r3,	r3,	r23
	ORI	r11,	r0,	1065353216
	ORI	r5,	r0,	0
	bneid	r25,	$0BB11_31
	NOP
	FPLT	r20,	r4,	r8
	bneid	r20,	$0BB11_30
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB11_30:
	bneid	r4,	$0BB11_35
	ADD	r20,	r5,	r0
$0BB11_31:
	FPLT	r5,	r24,	r8
	bneid	r5,	$0BB11_33
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB11_33:
	ORI	r20,	r0,	1065353216
	ORI	r5,	r0,	0
	bneid	r4,	$0BB11_35
	ADD	r11,	r5,	r0
	ORI	r20,	r0,	0
	ORI	r5,	r0,	1065353216
	ADD	r11,	r20,	r0
$0BB11_35:
	FPMUL	r4,	r7,	r11
	FPMUL	r8,	r6,	r20
	FPRSUB	r4,	r4,	r8
	FPMUL	r8,	r10,	r11
	FPMUL	r11,	r6,	r5
	FPRSUB	r8,	r11,	r8
	FPMUL	r11,	r10,	r8
	FPMUL	r21,	r7,	r4
	FPRSUB	r11,	r11,	r21
	FPMUL	r20,	r10,	r20
	FPMUL	r5,	r7,	r5
	FPRSUB	r20,	r20,	r5
	FPMUL	r5,	r6,	r4
	FPMUL	r21,	r10,	r20
	FPRSUB	r21,	r5,	r21
	FPMUL	r5,	r11,	r22
	FPMUL	r11,	r20,	r12
	FPADD	r5,	r11,	r5
	FPMUL	r11,	r6,	r3
	FPADD	r5,	r5,	r11
	FPMUL	r11,	r21,	r22
	FPMUL	r21,	r8,	r12
	FPADD	r11,	r21,	r11
	FPMUL	r21,	r7,	r3
	FPADD	r11,	r11,	r21
	FPMUL	r7,	r7,	r20
	FPMUL	r8,	r6,	r8
	FPMUL	r6,	r11,	r11
	FPMUL	r20,	r5,	r5
	FPADD	r6,	r20,	r6
	FPRSUB	r7,	r7,	r8
	FPMUL	r7,	r7,	r22
	FPMUL	r4,	r4,	r12
	FPADD	r4,	r4,	r7
	FPMUL	r3,	r10,	r3
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r6,	r4
	FPINVSQRT	r4,	r4
	ORI	r6,	r0,	1065353216
	FPDIV	r4,	r6,	r4
	FPDIV	r4,	r6,	r4
	FPMUL	r3,	r3,	r4
	FPMUL	r6,	r11,	r4
	FPMUL	r5,	r5,	r4
	ADD	r4,	r6,	r0
	ORI	r22,	r0,	1203982336
	brid	$0BB11_37
	ADD	r10,	r28,	r0
$0BB11_91:
	LWI	r3,	r8,	48
	SWI	r3,	r5,	0
	LWI	r3,	r8,	52
	SWI	r3,	r5,	4
	LWI	r3,	r8,	56
	SWI	r3,	r5,	8
	LWI	r3,	r8,	60
	brid	$0BB11_92
	SWI	r3,	r5,	12
$0BB11_6:
	ADDI	r7,	r0,	1
	CMP	r7,	r7,	r6
	bneid	r7,	$0BB11_7
	NOP
	ORI	r22,	r0,	1203982336
	LWI	r3,	r10,	20
	LWI	r4,	r10,	16
	LWI	r5,	r10,	12
	LWI	r29,	r10,	32
	LWI	r30,	r10,	28
	brid	$0BB11_37
	LWI	r31,	r10,	24
$0BB11_7:
	ADD	r30,	r29,	r0
	bneid	r6,	$0BB11_37
	ADD	r31,	r29,	r0
	LWI	r6,	r10,	12
	FPRSUB	r5,	r5,	r6
	LWI	r6,	r10,	16
	FPRSUB	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPMUL	r7,	r5,	r5
	FPADD	r6,	r7,	r6
	LWI	r7,	r10,	20
	FPRSUB	r3,	r3,	r7
	FPMUL	r7,	r3,	r3
	FPADD	r6,	r6,	r7
	LWI	r29,	r10,	32
	LWI	r30,	r10,	28
	LWI	r31,	r10,	24
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r22,	r7,	r6
	FPDIV	r6,	r7,	r22
	FPMUL	r3,	r3,	r6
	FPMUL	r4,	r4,	r6
	FPMUL	r5,	r5,	r6
$0BB11_37:
	SWI	r5,	r1,	188
	SWI	r4,	r1,	184
	SWI	r3,	r1,	180
	ADD	r6,	r5,	r0
	ADD	r5,	r3,	r0
	FPMUL	r3,	r27,	r4
	LWI	r4,	r1,	224
	FPMUL	r4,	r4,	r6
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r26,	r5
	FPADD	r5,	r3,	r4
	SWI	r5,	r1,	256
	ORI	r3,	r0,	0
	FPLE	r4,	r5,	r3
	FPUN	r5,	r5,	r3
	BITOR	r4,	r5,	r4
	bneid	r4,	$0BB11_39
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB11_39:
	SWI	r31,	r1,	252
	SWI	r30,	r1,	248
	SWI	r29,	r1,	244
	SWI	r27,	r1,	236
	SWI	r26,	r1,	232
	ADD	r4,	r3,	r0
	bneid	r6,	$0BB11_90
	ADD	r5,	r3,	r0
	SWI	r10,	r1,	208
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	180
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	212
	LWI	r4,	r1,	184
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	216
	LWI	r4,	r1,	188
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	220
	ADD	r3,	r0,	r0
	SWI	r3,	r1,	192
	LWI	r4,	r9,	8
	SWI	r4,	r1,	196
	LWI	r4,	r9,	4
	SWI	r4,	r1,	200
	LWI	r4,	r9,	0
	SWI	r4,	r1,	204
	brid	$0BB11_41
	ADD	r30,	r3,	r0
$0BB11_87:
	bslli	r4,	r20,	2
	ADDI	r5,	r1,	52
	ADD	r4,	r5,	r4
	LWI	r30,	r4,	-4
$0BB11_41:
	brid	$0BB11_42
	ADD	r20,	r3,	r0
$0BB11_118:
	ADDI	r3,	r30,	1
	bslli	r4,	r20,	2
	ADDI	r5,	r1,	52
	SW	r3,	r5,	r4
	ADDI	r20,	r20,	1
$0BB11_42:
	bslli	r3,	r30,	3
	LWI	r4,	r1,	208
	LWI	r4,	r4,	0
	ADD	r6,	r4,	r3
	LOAD	r3,	r6,	0
	LOAD	r5,	r6,	1
	LOAD	r4,	r6,	2
	ADDI	r10,	r6,	3
	LOAD	r8,	r10,	0
	LOAD	r7,	r10,	1
	LWI	r9,	r1,	200
	FPRSUB	r11,	r9,	r7
	FPRSUB	r7,	r9,	r5
	LOAD	r5,	r10,	2
	LWI	r9,	r1,	196
	FPRSUB	r5,	r9,	r5
	FPRSUB	r4,	r9,	r4
	LWI	r9,	r1,	212
	FPMUL	r4,	r4,	r9
	FPMUL	r5,	r5,	r9
	LWI	r9,	r1,	216
	FPMUL	r10,	r7,	r9
	FPMUL	r11,	r11,	r9
	LWI	r7,	r1,	204
	FPRSUB	r8,	r7,	r8
	FPRSUB	r3,	r7,	r3
	LWI	r9,	r1,	220
	FPMUL	r7,	r3,	r9
	FPMUL	r12,	r8,	r9
	FPMIN	r3,	r7,	r12
	FPMIN	r8,	r10,	r11
	FPMIN	r21,	r4,	r5
	FPMAX	r7,	r7,	r12
	FPMAX	r10,	r10,	r11
	FPMAX	r5,	r4,	r5
	FPMAX	r4,	r8,	r21
	FPMAX	r4,	r3,	r4
	FPMIN	r3,	r10,	r5
	FPMIN	r3,	r7,	r3
	FPGE	r5,	r4,	r3
	FPUN	r7,	r4,	r3
	BITOR	r7,	r7,	r5
	bneid	r7,	$0BB11_44
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_44:
	bneid	r5,	$0BB11_86
	NOP
	ORI	r5,	r0,	869711765
	FPLE	r7,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB11_47
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_47:
	bneid	r5,	$0BB11_51
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r7,	r4,	r5
	bneid	r7,	$0BB11_50
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_50:
	beqid	r5,	$0BB11_51
	NOP
	FPLT	r3,	r22,	r4
	FPUN	r4,	r22,	r4
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB11_59
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB11_59:
	bneid	r3,	$0BB11_86
	NOP
	brid	$0BB11_60
	NOP
$0BB11_51:
	ORI	r4,	r0,	869711765
	FPLE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB11_53
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB11_53:
	bneid	r4,	$0BB11_86
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB11_56
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB11_56:
	beqid	r3,	$0BB11_86
	NOP
$0BB11_60:
	LOAD	r30,	r6,	7
	LOAD	r6,	r6,	6
	bltid	r6,	$0BB11_118
	NOP
	bleid	r6,	$0BB11_86
	ADD	r21,	r0,	r0
$0BB11_62:
	LOAD	r5,	r30,	0
	LOAD	r10,	r30,	1
	LOAD	r4,	r30,	2
	ADDI	r8,	r30,	3
	LOAD	r3,	r8,	0
	LOAD	r7,	r8,	1
	LOAD	r11,	r8,	2
	ADDI	r12,	r30,	6
	LOAD	r8,	r12,	0
	LOAD	r28,	r12,	1
	LOAD	r23,	r12,	2
	FPRSUB	r12,	r23,	r11
	FPRSUB	r26,	r28,	r7
	LWI	r25,	r1,	180
	FPMUL	r7,	r25,	r26
	LWI	r31,	r1,	184
	FPMUL	r11,	r31,	r12
	FPRSUB	r24,	r7,	r11
	FPRSUB	r3,	r8,	r3
	FPMUL	r7,	r25,	r3
	LWI	r9,	r1,	188
	FPMUL	r11,	r9,	r12
	FPRSUB	r25,	r11,	r7
	LWI	r7,	r1,	204
	FPRSUB	r11,	r8,	r7
	LWI	r7,	r1,	200
	FPRSUB	r27,	r28,	r7
	FPMUL	r7,	r27,	r25
	FPMUL	r29,	r11,	r24
	FPADD	r7,	r29,	r7
	FPMUL	r29,	r31,	r3
	FPMUL	r31,	r9,	r26
	FPRSUB	r31,	r29,	r31
	FPRSUB	r8,	r8,	r5
	FPRSUB	r28,	r28,	r10
	LWI	r5,	r1,	196
	FPRSUB	r29,	r23,	r5
	FPMUL	r5,	r29,	r31
	FPADD	r5,	r7,	r5
	FPMUL	r7,	r28,	r25
	FPMUL	r10,	r8,	r24
	FPADD	r7,	r10,	r7
	FPRSUB	r23,	r23,	r4
	FPMUL	r4,	r23,	r31
	FPADD	r4,	r7,	r4
	ORI	r7,	r0,	1065353216
	FPDIV	r4,	r7,	r4
	FPMUL	r5,	r5,	r4
	ORI	r7,	r0,	0
	FPLT	r10,	r5,	r7
	bneid	r10,	$0BB11_64
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB11_64:
	bneid	r7,	$0BB11_85
	NOP
	ORI	r7,	r0,	1065353216
	FPGT	r10,	r5,	r7
	bneid	r10,	$0BB11_67
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB11_67:
	bneid	r7,	$0BB11_85
	NOP
	FPMUL	r7,	r29,	r28
	FPMUL	r10,	r27,	r23
	FPRSUB	r10,	r7,	r10
	FPMUL	r7,	r11,	r23
	FPMUL	r23,	r29,	r8
	FPRSUB	r29,	r7,	r23
	LWI	r7,	r1,	184
	FPMUL	r7,	r7,	r29
	LWI	r9,	r1,	188
	FPMUL	r23,	r9,	r10
	FPADD	r7,	r23,	r7
	FPMUL	r8,	r27,	r8
	FPMUL	r11,	r11,	r28
	FPRSUB	r8,	r8,	r11
	LWI	r11,	r1,	180
	FPMUL	r11,	r11,	r8
	FPADD	r7,	r7,	r11
	FPMUL	r11,	r7,	r4
	ORI	r7,	r0,	0
	FPLT	r23,	r11,	r7
	bneid	r23,	$0BB11_70
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB11_70:
	bneid	r7,	$0BB11_85
	NOP
	FPADD	r5,	r11,	r5
	ORI	r7,	r0,	1065353216
	FPGT	r7,	r5,	r7
	bneid	r7,	$0BB11_73
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB11_73:
	bneid	r5,	$0BB11_85
	NOP
	FPMUL	r5,	r26,	r29
	FPMUL	r3,	r3,	r10
	FPADD	r3,	r3,	r5
	FPMUL	r5,	r12,	r8
	FPADD	r3,	r3,	r5
	FPMUL	r3,	r3,	r4
	ORI	r4,	r0,	869711765
	FPGT	r7,	r3,	r4
	ADDI	r5,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r7,	$0BB11_76
	ADD	r8,	r4,	r0
	ADD	r8,	r5,	r0
$0BB11_76:
	ORI	r7,	r0,	0
	FPGT	r10,	r3,	r7
	bneid	r10,	$0BB11_78
	ADD	r7,	r4,	r0
	ADD	r7,	r5,	r0
$0BB11_78:
	BITAND	r7,	r7,	r8
	FPLT	r10,	r3,	r22
	bneid	r10,	$0BB11_80
	ADD	r8,	r4,	r0
	ADD	r8,	r5,	r0
$0BB11_80:
	BITAND	r5,	r7,	r8
	bneid	r5,	$0BB11_82
	NOP
	ADD	r3,	r22,	r0
$0BB11_82:
	bneid	r5,	$0BB11_84
	NOP
	LWI	r4,	r1,	192
$0BB11_84:
	SWI	r4,	r1,	192
	ADD	r22,	r3,	r0
$0BB11_85:
	ADDI	r21,	r21,	1
	CMP	r3,	r6,	r21
	bltid	r3,	$0BB11_62
	ADDI	r30,	r30,	11
$0BB11_86:
	ADDI	r3,	r0,	-1
	ADD	r3,	r20,	r3
	bgeid	r3,	$0BB11_87
	NOP
	ORI	r3,	r0,	0
	LWI	r4,	r1,	192
	ANDI	r6,	r4,	255
	ADD	r4,	r3,	r0
	ADD	r5,	r3,	r0
	bneid	r6,	$0BB11_90
	LWI	r10,	r1,	208
	LWI	r3,	r1,	252
	LWI	r6,	r1,	256
	FPMUL	r5,	r3,	r6
	LWI	r3,	r1,	248
	FPMUL	r4,	r3,	r6
	LWI	r3,	r1,	244
	FPMUL	r3,	r3,	r6
	ORI	r6,	r0,	0
	FPADD	r3,	r3,	r6
	FPADD	r4,	r4,	r6
	FPADD	r5,	r5,	r6
	LWI	r6,	r1,	228
	SWI	r5,	r6,	0
	SWI	r4,	r6,	4
	SWI	r3,	r6,	8
$0BB11_90:
	LWI	r6,	r1,	240
	LWI	r6,	r6,	12
	ADDI	r6,	r6,	10
	LOAD	r6,	r6,	0
	MULI	r6,	r6,	25
	LWI	r7,	r10,	4
	ADD	r6,	r7,	r6
	LOAD	r8,	r6,	4
	LOAD	r7,	r6,	5
	LOAD	r6,	r6,	6
	LWI	r10,	r1,	288
	SWI	r8,	r10,	0
	SWI	r7,	r10,	4
	SWI	r6,	r10,	8
	ADDI	r9,	r0,	1065353216
	SWI	r9,	r10,	12
	FPMUL	r5,	r5,	r8
	LWI	r8,	r1,	228
	SWI	r5,	r8,	0
	FPMUL	r4,	r4,	r7
	SWI	r4,	r8,	4
	FPMUL	r3,	r3,	r6
	SWI	r3,	r8,	8
	LWI	r20,	r1,	232
	LWI	r12,	r1,	236
	LWI	r21,	r1,	224
$0BB11_92:
	RAND	r3
	RAND	r4
	FPADD	r5,	r4,	r4
	ORI	r4,	r0,	-1082130432
	FPADD	r5,	r5,	r4
	FPADD	r3,	r3,	r3
	FPADD	r6,	r3,	r4
	FPMUL	r4,	r6,	r6
	FPMUL	r7,	r5,	r5
	FPADD	r3,	r4,	r7
	ORI	r8,	r0,	1065353216
	FPGE	r8,	r3,	r8
	bneid	r8,	$0BB11_94
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB11_94:
	bneid	r3,	$0BB11_92
	NOP
	ORI	r3,	r0,	0
	FPGE	r8,	r21,	r3
	FPUN	r3,	r21,	r3
	BITOR	r3,	r3,	r8
	bneid	r3,	$0BB11_97
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB11_97:
	ORI	r3,	r0,	1065353216
	FPRSUB	r4,	r4,	r3
	FPRSUB	r4,	r7,	r4
	FPINVSQRT	r7,	r4
	bneid	r8,	$0BB11_99
	ADD	r4,	r21,	r0
	FPNEG	r4,	r21
$0BB11_99:
	ORI	r8,	r0,	0
	FPGE	r9,	r12,	r8
	FPUN	r8,	r12,	r8
	BITOR	r8,	r8,	r9
	bneid	r8,	$0BB11_101
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB11_101:
	bneid	r9,	$0BB11_103
	ADD	r8,	r12,	r0
	FPNEG	r8,	r12
$0BB11_103:
	ORI	r9,	r0,	0
	FPGE	r10,	r20,	r9
	FPUN	r9,	r20,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB11_105
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB11_105:
	bneid	r10,	$0BB11_107
	ADD	r9,	r20,	r0
	FPNEG	r9,	r20
$0BB11_107:
	FPGE	r10,	r4,	r8
	FPUN	r11,	r4,	r8
	BITOR	r10,	r11,	r10
	bneid	r10,	$0BB11_109
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB11_109:
	FPDIV	r3,	r3,	r7
	ORI	r10,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r11,	$0BB11_113
	NOP
	FPLT	r11,	r4,	r9
	bneid	r11,	$0BB11_112
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB11_112:
	bneid	r4,	$0BB11_117
	ADD	r11,	r7,	r0
$0BB11_113:
	FPLT	r7,	r8,	r9
	bneid	r7,	$0BB11_115
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB11_115:
	ORI	r11,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r4,	$0BB11_117
	ADD	r10,	r7,	r0
	ORI	r11,	r0,	0
	ORI	r7,	r0,	1065353216
	ADD	r10,	r11,	r0
$0BB11_117:
	FPMUL	r4,	r12,	r10
	FPMUL	r8,	r21,	r11
	FPRSUB	r4,	r4,	r8
	FPMUL	r8,	r20,	r10
	FPMUL	r9,	r21,	r7
	FPRSUB	r8,	r9,	r8
	FPMUL	r9,	r20,	r8
	FPMUL	r10,	r12,	r4
	FPRSUB	r9,	r9,	r10
	FPMUL	r10,	r20,	r11
	FPMUL	r7,	r12,	r7
	FPRSUB	r10,	r10,	r7
	FPMUL	r7,	r21,	r4
	FPMUL	r11,	r20,	r10
	FPRSUB	r11,	r7,	r11
	FPMUL	r7,	r9,	r5
	FPMUL	r9,	r10,	r6
	FPADD	r7,	r9,	r7
	FPMUL	r9,	r21,	r3
	FPADD	r7,	r7,	r9
	FPMUL	r9,	r11,	r5
	FPMUL	r11,	r8,	r6
	FPADD	r9,	r11,	r9
	FPMUL	r11,	r12,	r3
	FPADD	r9,	r9,	r11
	FPMUL	r10,	r12,	r10
	FPMUL	r11,	r21,	r8
	FPMUL	r8,	r9,	r9
	FPMUL	r12,	r7,	r7
	FPADD	r8,	r12,	r8
	FPRSUB	r10,	r10,	r11
	FPMUL	r5,	r10,	r5
	FPMUL	r4,	r4,	r6
	FPADD	r4,	r4,	r5
	FPMUL	r3,	r20,	r3
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r8,	r4
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r5,	r7,	r4
	LWI	r6,	r1,	284
	SWI	r5,	r6,	0
	FPMUL	r5,	r9,	r4
	SWI	r5,	r6,	4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r6,	8
	LWI	r31,	r1,	4
	LWI	r30,	r1,	8
	LWI	r29,	r1,	12
	LWI	r28,	r1,	16
	LWI	r27,	r1,	20
	LWI	r26,	r1,	24
	LWI	r25,	r1,	28
	LWI	r24,	r1,	32
	LWI	r23,	r1,	36
	LWI	r22,	r1,	40
	LWI	r21,	r1,	44
	LWI	r20,	r1,	48
	rtsd	r15,	8
	ADDI	r1,	r1,	260
#	.end	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
$0tmp11:
#	.size	_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color, ($tmp11)-_Z12shadeLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color

#	.globl	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
#	.align	2
#	.type	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color,@function
#	.ent	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color # @_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color:
#	.frame	r1,352,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-352
	SWI	r20,	r1,	48
	SWI	r21,	r1,	44
	SWI	r22,	r1,	40
	SWI	r23,	r1,	36
	SWI	r24,	r1,	32
	SWI	r25,	r1,	28
	SWI	r26,	r1,	24
	SWI	r27,	r1,	20
	SWI	r28,	r1,	16
	SWI	r29,	r1,	12
	SWI	r30,	r1,	8
	SWI	r31,	r1,	4
	SWI	r10,	r1,	376
	SWI	r9,	r1,	372
	ADD	r22,	r8,	r0
	SWI	r22,	r1,	220
	SWI	r6,	r1,	360
	SWI	r5,	r1,	356
	LWI	r3,	r7,	8
	SWI	r3,	r1,	180
	LWI	r3,	r7,	4
	SWI	r3,	r1,	184
	LWI	r9,	r7,	0
	LWI	r3,	r7,	12
	SWI	r3,	r1,	236
	LWI	r8,	r7,	16
	SWI	r8,	r1,	240
	LWI	r4,	r7,	20
	SWI	r4,	r1,	244
	SWI	r0,	r5,	12
	SWI	r0,	r5,	8
	SWI	r0,	r5,	4
	SWI	r0,	r5,	0
	ORI	r11,	r0,	0
	ORI	r5,	r0,	1065353216
	FPDIV	r7,	r5,	r4
	SWI	r7,	r1,	268
	FPDIV	r7,	r5,	r8
	SWI	r7,	r1,	272
	FPDIV	r5,	r5,	r3
	SWI	r5,	r1,	276
	ORI	r5,	r0,	981668463
	FPMUL	r4,	r4,	r5
	SWI	r4,	r1,	336
	FPMUL	r4,	r8,	r5
	SWI	r4,	r1,	340
	FPMUL	r3,	r3,	r5
	SWI	r3,	r1,	344
	ADD	r21,	r11,	r0
	ADD	r23,	r11,	r0
	brid	$0BB12_1
	ADD	r12,	r11,	r0
$0BB12_256:
	LWI	r11,	r1,	208
	LWI	r21,	r1,	224
	LWI	r23,	r1,	228
	LWI	r12,	r1,	192
	LWI	r22,	r1,	220
$0BB12_1:
	ADD	r8,	r6,	r0
	lbui	r5,	r8,	16
	beqid	r5,	$0BB12_258
	LWI	r20,	r1,	280
	SWI	r23,	r1,	296
	SWI	r21,	r1,	292
	SWI	r12,	r1,	288
	SWI	r11,	r1,	284
	LWI	r5,	r8,	8
	LWI	r20,	r1,	236
	FPMUL	r6,	r20,	r5
	FPADD	r6,	r9,	r6
	LWI	r7,	r1,	372
	SWI	r6,	r7,	0
	LWI	r21,	r1,	240
	FPMUL	r6,	r21,	r5
	LWI	r3,	r1,	184
	FPADD	r4,	r3,	r6
	SWI	r4,	r7,	4
	LWI	r26,	r1,	244
	FPMUL	r4,	r26,	r5
	LWI	r3,	r1,	180
	FPADD	r3,	r3,	r4
	SWI	r3,	r7,	8
	ADD	r12,	r7,	r0
	LWI	r6,	r8,	12
	LOAD	r4,	r6,	0
	LOAD	r5,	r6,	1
	LOAD	r3,	r6,	2
	ADDI	r8,	r6,	3
	LOAD	r7,	r8,	0
	LOAD	r10,	r8,	1
	LOAD	r9,	r8,	2
	ADDI	r11,	r6,	6
	LOAD	r8,	r11,	0
	LOAD	r6,	r11,	1
	FPRSUB	r5,	r6,	r5
	FPRSUB	r6,	r6,	r10
	LOAD	r10,	r11,	2
	FPRSUB	r9,	r10,	r9
	FPRSUB	r10,	r10,	r3
	FPMUL	r3,	r10,	r6
	FPMUL	r11,	r5,	r9
	FPRSUB	r3,	r3,	r11
	FPRSUB	r7,	r8,	r7
	FPRSUB	r8,	r8,	r4
	FPMUL	r4,	r8,	r9
	FPMUL	r9,	r10,	r7
	FPRSUB	r4,	r4,	r9
	FPMUL	r9,	r4,	r4
	FPMUL	r10,	r3,	r3
	FPADD	r9,	r10,	r9
	FPMUL	r5,	r5,	r7
	FPMUL	r6,	r8,	r6
	FPRSUB	r5,	r5,	r6
	FPMUL	r6,	r5,	r5
	FPADD	r6,	r9,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r6,	r7,	r6
	FPDIV	r6,	r7,	r6
	FPMUL	r24,	r4,	r6
	FPMUL	r4,	r24,	r21
	FPMUL	r25,	r3,	r6
	FPMUL	r3,	r25,	r20
	FPADD	r3,	r3,	r4
	FPMUL	r23,	r5,	r6
	FPMUL	r4,	r23,	r26
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB12_4
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_4:
	bneid	r3,	$0BB12_6
	LWI	r21,	r1,	204
	FPNEG	r23,	r23
	FPNEG	r24,	r24
	FPNEG	r25,	r25
$0BB12_6:
	ORI	r3,	r0,	981668463
	FPMUL	r6,	r23,	r3
	SWI	r6,	r1,	304
	FPMUL	r7,	r24,	r3
	SWI	r7,	r1,	308
	FPMUL	r4,	r25,	r3
	SWI	r4,	r1,	300
	LWI	r3,	r12,	0
	FPADD	r4,	r3,	r4
	LWI	r3,	r12,	8
	LWI	r5,	r12,	4
	SWI	r4,	r12,	0
	FPADD	r5,	r5,	r7
	SWI	r5,	r12,	4
	FPADD	r3,	r3,	r6
	SWI	r3,	r12,	8
	ORI	r6,	r0,	-1082130432
	SWI	r6,	r1,	252
	ORI	r27,	r0,	1065353216
	LWI	r6,	r22,	40
	ADDI	r7,	r0,	2
	CMP	r7,	r7,	r6
	bneid	r7,	$0BB12_7
	NOP
	LWI	r7,	r22,	44
	LWI	r27,	r22,	32
	LWI	r28,	r22,	28
	LWI	r29,	r22,	24
$0BB12_11:
	RAND	r3
	RAND	r4
	FPADD	r5,	r4,	r4
	ORI	r4,	r0,	-1082130432
	FPADD	r9,	r5,	r4
	FPADD	r3,	r3,	r3
	FPADD	r10,	r3,	r4
	FPMUL	r3,	r10,	r10
	FPMUL	r4,	r9,	r9
	FPADD	r5,	r3,	r4
	ORI	r6,	r0,	1065353216
	FPGE	r6,	r5,	r6
	bneid	r6,	$0BB12_13
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_13:
	bneid	r5,	$0BB12_11
	NOP
	ORI	r6,	r0,	1065353216
	FPRSUB	r3,	r3,	r6
	FPRSUB	r3,	r4,	r3
	FPINVSQRT	r11,	r3
	LWI	r3,	r22,	12
	ORI	r4,	r0,	0
	FPGE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB12_16
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB12_16:
	bneid	r4,	$0BB12_18
	ADD	r12,	r3,	r0
	FPNEG	r12,	r3
$0BB12_18:
	LWI	r4,	r22,	16
	ORI	r5,	r0,	0
	FPGE	r8,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB12_20
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_20:
	bneid	r5,	$0BB12_22
	ADD	r20,	r4,	r0
	FPNEG	r20,	r4
$0BB12_22:
	LWI	r5,	r22,	20
	ORI	r8,	r0,	0
	FPGE	r21,	r5,	r8
	FPUN	r8,	r5,	r8
	BITOR	r21,	r8,	r21
	bneid	r21,	$0BB12_24
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB12_24:
	ADD	r26,	r22,	r0
	bneid	r8,	$0BB12_26
	ADD	r21,	r5,	r0
	FPNEG	r21,	r5
$0BB12_26:
	FPGE	r8,	r12,	r20
	FPUN	r22,	r12,	r20
	BITOR	r8,	r22,	r8
	bneid	r8,	$0BB12_28
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB12_28:
	FPMUL	r8,	r9,	r7
	FPMUL	r7,	r10,	r7
	FPDIV	r6,	r6,	r11
	ORI	r11,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r22,	$0BB12_32
	NOP
	FPLT	r12,	r12,	r21
	bneid	r12,	$0BB12_31
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB12_31:
	bneid	r10,	$0BB12_36
	ADD	r12,	r9,	r0
$0BB12_32:
	FPLT	r9,	r20,	r21
	bneid	r9,	$0BB12_34
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB12_34:
	ORI	r12,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r10,	$0BB12_36
	ADD	r11,	r9,	r0
	ORI	r12,	r0,	0
	ORI	r9,	r0,	1065353216
	ADD	r11,	r12,	r0
$0BB12_36:
	FPMUL	r10,	r4,	r11
	FPMUL	r20,	r3,	r12
	FPRSUB	r10,	r10,	r20
	FPMUL	r11,	r5,	r11
	FPMUL	r20,	r3,	r9
	FPRSUB	r11,	r20,	r11
	FPMUL	r20,	r5,	r11
	FPMUL	r21,	r4,	r10
	FPRSUB	r21,	r20,	r21
	FPMUL	r12,	r5,	r12
	FPMUL	r9,	r4,	r9
	FPRSUB	r20,	r12,	r9
	FPMUL	r9,	r3,	r10
	FPMUL	r12,	r5,	r20
	FPRSUB	r12,	r9,	r12
	FPMUL	r9,	r21,	r8
	FPMUL	r21,	r20,	r7
	FPADD	r9,	r21,	r9
	FPMUL	r21,	r3,	r6
	FPADD	r9,	r9,	r21
	FPMUL	r12,	r12,	r8
	FPMUL	r21,	r11,	r7
	FPADD	r12,	r21,	r12
	FPMUL	r21,	r4,	r6
	FPADD	r12,	r12,	r21
	FPMUL	r4,	r4,	r20
	FPMUL	r11,	r3,	r11
	FPMUL	r3,	r12,	r12
	FPMUL	r20,	r9,	r9
	FPADD	r3,	r20,	r3
	FPRSUB	r4,	r4,	r11
	FPMUL	r4,	r4,	r8
	FPMUL	r7,	r10,	r7
	FPADD	r4,	r7,	r4
	FPMUL	r5,	r5,	r6
	FPADD	r4,	r4,	r5
	FPMUL	r5,	r4,	r4
	FPADD	r3,	r3,	r5
	FPINVSQRT	r3,	r3
	ORI	r5,	r0,	1065353216
	FPDIV	r3,	r5,	r3
	FPDIV	r3,	r5,	r3
	FPMUL	r21,	r4,	r3
	FPMUL	r7,	r12,	r3
	FPMUL	r8,	r9,	r3
	ORI	r3,	r0,	1203982336
	SWI	r3,	r1,	252
	brid	$0BB12_38
	ADD	r22,	r26,	r0
$0BB12_7:
	ADDI	r7,	r0,	1
	CMP	r7,	r7,	r6
	bneid	r7,	$0BB12_8
	NOP
	ORI	r3,	r0,	1203982336
	SWI	r3,	r1,	252
	LWI	r21,	r22,	20
	LWI	r7,	r22,	16
	LWI	r8,	r22,	12
	LWI	r27,	r22,	32
	LWI	r28,	r22,	28
	brid	$0BB12_38
	LWI	r29,	r22,	24
$0BB12_8:
	ADD	r28,	r27,	r0
	ADD	r29,	r27,	r0
	LWI	r7,	r1,	212
	bneid	r6,	$0BB12_38
	LWI	r8,	r1,	216
	LWI	r6,	r22,	12
	FPRSUB	r4,	r4,	r6
	LWI	r6,	r22,	16
	FPRSUB	r5,	r5,	r6
	FPMUL	r6,	r5,	r5
	FPMUL	r7,	r4,	r4
	FPADD	r6,	r7,	r6
	LWI	r7,	r22,	20
	FPRSUB	r3,	r3,	r7
	FPMUL	r7,	r3,	r3
	FPADD	r6,	r6,	r7
	LWI	r27,	r22,	32
	LWI	r28,	r22,	28
	LWI	r29,	r22,	24
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r6,	r7,	r6
	SWI	r6,	r1,	252
	FPDIV	r6,	r7,	r6
	FPMUL	r21,	r3,	r6
	FPMUL	r7,	r5,	r6
	FPMUL	r8,	r4,	r6
$0BB12_38:
	SWI	r8,	r1,	216
	SWI	r7,	r1,	212
	FPMUL	r3,	r24,	r7
	FPMUL	r4,	r25,	r8
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r23,	r21
	SWI	r23,	r1,	280
	FPADD	r4,	r3,	r4
	SWI	r4,	r1,	324
	ORI	r23,	r0,	0
	FPLE	r3,	r4,	r23
	FPUN	r4,	r4,	r23
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB12_40
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_40:
	SWI	r25,	r1,	332
	bneid	r3,	$0BB12_41
	SWI	r24,	r1,	328
	SWI	r29,	r1,	320
	SWI	r28,	r1,	316
	SWI	r27,	r1,	312
	ORI	r3,	r0,	981668463
	FPMUL	r4,	r21,	r3
	SWI	r4,	r1,	256
	LWI	r4,	r1,	212
	FPMUL	r5,	r4,	r3
	SWI	r5,	r1,	260
	LWI	r5,	r1,	216
	FPMUL	r3,	r5,	r3
	SWI	r3,	r1,	264
	ORI	r6,	r0,	1065353216
	SWI	r6,	r1,	248
	FPDIV	r3,	r6,	r21
	SWI	r3,	r1,	224
	FPDIV	r3,	r6,	r4
	SWI	r3,	r1,	228
	ADD	r3,	r5,	r0
	FPDIV	r3,	r6,	r3
	SWI	r3,	r1,	232
	LWI	r3,	r1,	372
	LWI	r4,	r3,	8
	SWI	r4,	r1,	184
	LWI	r4,	r3,	4
	SWI	r4,	r1,	192
	brid	$0BB12_43
	LWI	r3,	r3,	0
$0BB12_41:
	ADD	r4,	r23,	r0
	brid	$0BB12_154
	ADD	r3,	r23,	r0
$0BB12_290:
	FPMUL	r3,	r4,	r22
	LWI	r4,	r1,	192
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r21,	r22
	LWI	r5,	r1,	184
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	256
	FPADD	r4,	r4,	r5
	SWI	r4,	r1,	184
	LWI	r4,	r1,	260
	FPADD	r3,	r3,	r4
	SWI	r3,	r1,	192
	LWI	r3,	r1,	216
	FPMUL	r3,	r3,	r22
	LWI	r4,	r1,	188
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	264
	FPADD	r3,	r3,	r4
$0BB12_43:
	SWI	r3,	r1,	188
	ADD	r4,	r0,	r0
	SWI	r4,	r1,	208
	SWI	r4,	r1,	204
	LWI	r22,	r1,	252
	brid	$0BB12_44
	ADD	r3,	r4,	r0
$0BB12_97:
	bslli	r3,	r3,	2
	ADDI	r5,	r1,	52
	ADD	r3,	r5,	r3
	LWI	r3,	r3,	-4
$0BB12_44:
	brid	$0BB12_45
	SWI	r4,	r1,	180
$0BB12_289:
	ADDI	r4,	r3,	1
	LWI	r7,	r1,	180
	bslli	r5,	r7,	2
	ADDI	r6,	r1,	52
	SW	r4,	r6,	r5
	ADDI	r7,	r7,	1
	SWI	r7,	r1,	180
$0BB12_45:
	bslli	r3,	r3,	3
	LWI	r4,	r1,	220
	LWI	r4,	r4,	0
	ADD	r4,	r4,	r3
	LOAD	r3,	r4,	0
	LOAD	r6,	r4,	1
	LOAD	r5,	r4,	2
	ADDI	r11,	r4,	3
	LOAD	r8,	r11,	0
	LOAD	r9,	r11,	1
	LWI	r7,	r1,	192
	FPRSUB	r10,	r7,	r9
	FPRSUB	r9,	r7,	r6
	LOAD	r6,	r11,	2
	LWI	r7,	r1,	184
	FPRSUB	r6,	r7,	r6
	FPRSUB	r5,	r7,	r5
	LWI	r7,	r1,	224
	FPMUL	r5,	r5,	r7
	FPMUL	r6,	r6,	r7
	LWI	r7,	r1,	228
	FPMUL	r9,	r9,	r7
	FPMUL	r10,	r10,	r7
	LWI	r7,	r1,	188
	FPRSUB	r11,	r7,	r8
	FPRSUB	r3,	r7,	r3
	LWI	r7,	r1,	232
	FPMUL	r8,	r3,	r7
	FPMUL	r12,	r11,	r7
	FPMIN	r3,	r8,	r12
	FPMIN	r11,	r9,	r10
	FPMIN	r20,	r5,	r6
	FPMAX	r8,	r8,	r12
	FPMAX	r9,	r9,	r10
	FPMAX	r5,	r5,	r6
	FPMAX	r6,	r11,	r20
	FPMAX	r6,	r3,	r6
	FPMIN	r3,	r9,	r5
	FPMIN	r3,	r8,	r3
	FPGE	r5,	r6,	r3
	FPUN	r8,	r6,	r3
	BITOR	r8,	r8,	r5
	bneid	r8,	$0BB12_47
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_47:
	bneid	r5,	$0BB12_96
	NOP
	ORI	r5,	r0,	869711765
	FPLE	r8,	r6,	r5
	FPUN	r5,	r6,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB12_50
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_50:
	bneid	r5,	$0BB12_54
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r8,	r6,	r5
	bneid	r8,	$0BB12_53
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_53:
	beqid	r5,	$0BB12_54
	NOP
	FPLT	r3,	r22,	r6
	FPUN	r5,	r22,	r6
	BITOR	r5,	r5,	r3
	bneid	r5,	$0BB12_62
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_62:
	bneid	r3,	$0BB12_96
	NOP
	brid	$0BB12_63
	NOP
$0BB12_54:
	ORI	r5,	r0,	869711765
	FPLE	r6,	r3,	r5
	FPUN	r5,	r3,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_56
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_56:
	bneid	r5,	$0BB12_96
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r5,	r3,	r5
	bneid	r5,	$0BB12_59
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_59:
	beqid	r3,	$0BB12_96
	NOP
$0BB12_63:
	LOAD	r3,	r4,	7
	LOAD	r4,	r4,	6
	bltid	r4,	$0BB12_289
	NOP
	bleid	r4,	$0BB12_96
	ADD	r11,	r0,	r0
$0BB12_65:
	ADD	r25,	r22,	r0
	LOAD	r12,	r3,	0
	LOAD	r24,	r3,	1
	LOAD	r10,	r3,	2
	ADDI	r8,	r3,	3
	LOAD	r6,	r8,	0
	LOAD	r5,	r8,	1
	LOAD	r8,	r8,	2
	ADDI	r9,	r3,	6
	LOAD	r27,	r9,	0
	LOAD	r29,	r9,	1
	LOAD	r26,	r9,	2
	FPRSUB	r20,	r26,	r8
	FPRSUB	r28,	r29,	r5
	FPMUL	r5,	r21,	r28
	LWI	r31,	r1,	212
	FPMUL	r8,	r31,	r20
	FPRSUB	r5,	r5,	r8
	FPRSUB	r6,	r27,	r6
	FPMUL	r8,	r21,	r6
	ADD	r7,	r21,	r0
	LWI	r22,	r1,	216
	FPMUL	r9,	r22,	r20
	FPRSUB	r9,	r9,	r8
	LWI	r8,	r1,	188
	FPRSUB	r8,	r27,	r8
	LWI	r21,	r1,	192
	FPRSUB	r30,	r29,	r21
	FPMUL	r21,	r30,	r9
	FPMUL	r23,	r8,	r5
	FPADD	r23,	r23,	r21
	FPMUL	r21,	r31,	r6
	FPMUL	r31,	r22,	r28
	FPRSUB	r21,	r21,	r31
	FPRSUB	r31,	r27,	r12
	FPRSUB	r24,	r29,	r24
	LWI	r12,	r1,	184
	FPRSUB	r27,	r26,	r12
	FPMUL	r12,	r27,	r21
	FPADD	r29,	r23,	r12
	FPMUL	r9,	r24,	r9
	FPMUL	r5,	r31,	r5
	FPADD	r5,	r5,	r9
	FPRSUB	r26,	r26,	r10
	FPMUL	r9,	r26,	r21
	FPADD	r5,	r5,	r9
	ORI	r9,	r0,	1065353216
	FPDIV	r12,	r9,	r5
	FPMUL	r29,	r29,	r12
	ORI	r5,	r0,	0
	FPLT	r9,	r29,	r5
	bneid	r9,	$0BB12_67
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_67:
	bneid	r5,	$0BB12_68
	ADD	r22,	r25,	r0
	ORI	r5,	r0,	1065353216
	FPGT	r9,	r29,	r5
	ADDI	r5,	r0,	1
	bneid	r9,	$0BB12_71
	ADD	r21,	r7,	r0
	ADDI	r5,	r0,	0
$0BB12_71:
	bneid	r5,	$0BB12_95
	NOP
	FPMUL	r5,	r27,	r24
	FPMUL	r9,	r30,	r26
	FPRSUB	r10,	r5,	r9
	FPMUL	r5,	r8,	r26
	FPMUL	r9,	r27,	r31
	FPRSUB	r27,	r5,	r9
	LWI	r5,	r1,	212
	FPMUL	r5,	r5,	r27
	LWI	r7,	r1,	216
	FPMUL	r9,	r7,	r10
	FPADD	r5,	r9,	r5
	FPMUL	r9,	r30,	r31
	FPMUL	r8,	r8,	r24
	FPRSUB	r24,	r9,	r8
	FPMUL	r8,	r21,	r24
	FPADD	r5,	r5,	r8
	FPMUL	r8,	r5,	r12
	ORI	r5,	r0,	0
	FPLT	r9,	r8,	r5
	bneid	r9,	$0BB12_74
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_74:
	bneid	r5,	$0BB12_95
	NOP
	FPADD	r5,	r8,	r29
	ORI	r9,	r0,	1065353216
	FPGT	r9,	r5,	r9
	bneid	r9,	$0BB12_77
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_77:
	bneid	r5,	$0BB12_95
	NOP
	FPMUL	r5,	r28,	r27
	FPMUL	r6,	r6,	r10
	FPADD	r5,	r6,	r5
	FPMUL	r6,	r20,	r24
	FPADD	r5,	r5,	r6
	FPMUL	r6,	r5,	r12
	ORI	r5,	r0,	869711765
	FPGT	r9,	r6,	r5
	ADDI	r10,	r0,	0
	ADDI	r12,	r0,	1
	bneid	r9,	$0BB12_80
	ADD	r5,	r12,	r0
	ADD	r5,	r10,	r0
$0BB12_80:
	ORI	r9,	r0,	0
	FPGT	r20,	r6,	r9
	bneid	r20,	$0BB12_82
	ADD	r9,	r12,	r0
	ADD	r9,	r10,	r0
$0BB12_82:
	BITAND	r5,	r9,	r5
	FPLT	r20,	r6,	r22
	bneid	r20,	$0BB12_84
	ADD	r9,	r12,	r0
	ADD	r9,	r10,	r0
$0BB12_84:
	BITAND	r10,	r5,	r9
	bneid	r10,	$0BB12_86
	NOP
	LWI	r29,	r1,	200
$0BB12_86:
	bneid	r10,	$0BB12_88
	NOP
	LWI	r8,	r1,	196
$0BB12_88:
	bneid	r10,	$0BB12_90
	NOP
	ADD	r6,	r22,	r0
$0BB12_90:
	bneid	r10,	$0BB12_92
	ADD	r5,	r3,	r0
	LWI	r5,	r1,	204
$0BB12_92:
	bneid	r10,	$0BB12_94
	NOP
	LWI	r12,	r1,	208
$0BB12_94:
	SWI	r12,	r1,	208
	SWI	r5,	r1,	204
	ADD	r22,	r6,	r0
	SWI	r8,	r1,	196
	brid	$0BB12_95
	SWI	r29,	r1,	200
$0BB12_68:
	ADD	r21,	r7,	r0
$0BB12_95:
	ADDI	r11,	r11,	1
	CMP	r5,	r4,	r11
	bltid	r5,	$0BB12_65
	ADDI	r3,	r3,	11
$0BB12_96:
	ADDI	r3,	r0,	-1
	LWI	r5,	r1,	180
	ADD	r4,	r5,	r3
	bgeid	r4,	$0BB12_97
	ADD	r3,	r5,	r0
	LWI	r3,	r1,	208
	ANDI	r3,	r3,	255
	beqid	r3,	$0BB12_99
	LWI	r5,	r1,	204
	ADDI	r3,	r5,	10
	LOAD	r3,	r3,	0
	MULI	r3,	r3,	25
	LWI	r4,	r1,	220
	LWI	r4,	r4,	4
	ADD	r3,	r4,	r3
	LOAD	r3,	r3,	15
	bltid	r3,	$0BB12_149
	NOP
	LOAD	r4,	r5,	9
	MULI	r5,	r4,	9
	LWI	r10,	r1,	220
	LWI	r4,	r10,	8
	ADD	r6,	r4,	r5
	LOAD	r7,	r6,	0
	LOAD	r4,	r6,	1
	LOAD	r6,	r6,	2
	LWI	r6,	r10,	8
	ADD	r6,	r5,	r6
	ADDI	r9,	r6,	3
	LOAD	r8,	r9,	0
	LOAD	r6,	r9,	1
	LOAD	r9,	r9,	2
	LWI	r11,	r1,	200
	FPMUL	r7,	r7,	r11
	LWI	r9,	r1,	196
	FPMUL	r8,	r8,	r9
	FPADD	r8,	r7,	r8
	LWI	r7,	r10,	8
	ADD	r5,	r5,	r7
	ORI	r7,	r0,	1065353216
	FPRSUB	r7,	r11,	r7
	FPRSUB	r7,	r9,	r7
	ADDI	r10,	r5,	6
	LOAD	r5,	r10,	0
	FPMUL	r5,	r5,	r7
	FPADD	r20,	r8,	r5
	INTCONV	r5,	r20
	FPCONV	r8,	r5
	ADDI	r5,	r0,	1
	FPEQ	r9,	r20,	r8
	bneid	r9,	$0BB12_103
	NOP
	ADDI	r5,	r0,	0
$0BB12_103:
	LWI	r9,	r1,	200
	FPMUL	r4,	r4,	r9
	LOAD	r9,	r10,	1
	LOAD	r10,	r10,	2
	LWI	r10,	r1,	196
	FPMUL	r6,	r6,	r10
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r9,	r7
	FPADD	r23,	r4,	r6
	LOAD	r11,	r3,	0
	LOAD	r4,	r3,	1
	LOAD	r7,	r3,	2
	bneid	r5,	$0BB12_105
	NOP
	FPRSUB	r20,	r8,	r20
$0BB12_105:
	INTCONV	r5,	r23
	FPCONV	r5,	r5
	FPEQ	r8,	r23,	r5
	bneid	r8,	$0BB12_107
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_107:
	bneid	r6,	$0BB12_109
	NOP
	FPRSUB	r23,	r5,	r23
$0BB12_109:
	ORI	r5,	r0,	0
	FPGE	r6,	r20,	r5
	FPUN	r5,	r20,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_111
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_111:
	bneid	r5,	$0BB12_113
	NOP
	ORI	r5,	r0,	1065353216
	FPADD	r20,	r20,	r5
$0BB12_113:
	ORI	r5,	r0,	0
	FPGE	r6,	r23,	r5
	FPUN	r5,	r23,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_115
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_115:
	bneid	r5,	$0BB12_117
	NOP
	ORI	r5,	r0,	1065353216
	FPADD	r23,	r23,	r5
$0BB12_117:
	FPCONV	r5,	r4
	FPMUL	r5,	r20,	r5
	INTCONV	r6,	r5
	CMP	r6,	r4,	r6
	ADDI	r12,	r0,	0
	ADDI	r10,	r0,	1
	beqid	r6,	$0BB12_119
	ADD	r9,	r10,	r0
	ADD	r9,	r12,	r0
$0BB12_119:
	ADD	r24,	r21,	r0
	ORI	r6,	r0,	0
	bneid	r9,	$0BB12_121
	ADD	r8,	r6,	r0
	ADD	r8,	r5,	r0
$0BB12_121:
	FPGE	r5,	r8,	r6
	FPUN	r9,	r8,	r6
	BITOR	r21,	r9,	r5
	FPCONV	r5,	r11
	FPMUL	r5,	r23,	r5
	INTCONV	r9,	r5
	CMP	r9,	r11,	r9
	bneid	r21,	$0BB12_123
	ADD	r20,	r10,	r0
	ADD	r20,	r12,	r0
$0BB12_123:
	beqid	r9,	$0BB12_125
	NOP
	ADD	r10,	r12,	r0
$0BB12_125:
	bneid	r10,	$0BB12_127
	ADD	r21,	r24,	r0
	ADD	r6,	r5,	r0
$0BB12_127:
	bneid	r20,	$0BB12_132
	NOP
	FPNEG	r5,	r8
	INTCONV	r5,	r5
	RSUBI	r10,	r5,	0
	FPCONV	r9,	r10
	FPEQ	r9,	r9,	r8
	bneid	r9,	$0BB12_130
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB12_130:
	bneid	r8,	$0BB12_133
	NOP
	ADDI	r8,	r0,	-1
	brid	$0BB12_133
	BITXOR	r10,	r5,	r8
$0BB12_132:
	INTCONV	r10,	r8
$0BB12_133:
	ORI	r5,	r0,	0
	FPGE	r8,	r6,	r5
	FPUN	r5,	r6,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB12_135
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_135:
	DIV	r8,	r4,	r10
	MUL	r8,	r8,	r4
	RSUB	r8,	r8,	r10
	bsrai	r9,	r8,	31
	BITAND	r9,	r9,	r4
	ADD	r8,	r9,	r8
	ADDI	r9,	r8,	1
	DIV	r10,	r4,	r9
	MUL	r10,	r10,	r4
	bneid	r5,	$0BB12_140
	RSUB	r20,	r10,	r9
	FPNEG	r5,	r6
	INTCONV	r5,	r5
	RSUBI	r10,	r5,	0
	FPCONV	r9,	r10
	FPEQ	r9,	r9,	r6
	bneid	r9,	$0BB12_138
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_138:
	bneid	r6,	$0BB12_141
	NOP
	ADDI	r6,	r0,	-1
	brid	$0BB12_141
	BITXOR	r10,	r5,	r6
$0BB12_140:
	INTCONV	r10,	r6
$0BB12_141:
	DIV	r5,	r11,	r10
	MUL	r5,	r5,	r11
	RSUB	r5,	r5,	r10
	bsrai	r6,	r5,	31
	BITAND	r6,	r6,	r11
	ADD	r5,	r6,	r5
	MUL	r12,	r5,	r4
	ADDI	r5,	r5,	1
	DIV	r6,	r11,	r5
	MUL	r6,	r6,	r11
	RSUB	r11,	r6,	r5
	ADD	r6,	r12,	r8
	ADDI	r5,	r0,	4
	CMP	r5,	r5,	r7
	MUL	r9,	r6,	r7
	ORI	r6,	r0,	1065353216
	ADDI	r3,	r3,	3
	bneid	r5,	$0BB12_142
	ADD	r10,	r9,	r3
	MUL	r5,	r11,	r4
	ADD	r4,	r5,	r8
	ADD	r6,	r5,	r20
	ADD	r5,	r12,	r20
	bslli	r5,	r5,	2
	bslli	r4,	r4,	2
	ADD	r4,	r4,	r3
	ADD	r5,	r5,	r3
	bslli	r6,	r6,	2
	ADD	r3,	r6,	r3
	LOAD	r6,	r10,	1
	LOAD	r6,	r10,	2
	LOAD	r6,	r10,	3
	LOAD	r6,	r10,	0
	LOAD	r7,	r5,	1
	LOAD	r7,	r5,	2
	LOAD	r7,	r5,	3
	LOAD	r5,	r5,	0
	LOAD	r7,	r4,	1
	LOAD	r7,	r4,	2
	LOAD	r7,	r4,	3
	FPCONV	r6,	r6
	LOAD	r8,	r4,	0
	ORI	r4,	r0,	1132396544
	FPDIV	r7,	r6,	r4
	LOAD	r6,	r3,	1
	LOAD	r6,	r3,	2
	LOAD	r6,	r3,	3
	FPCONV	r5,	r5
	FPDIV	r6,	r5,	r4
	FPCONV	r5,	r8
	FPDIV	r23,	r5,	r4
	LOAD	r3,	r3,	0
	FPCONV	r3,	r3
	brid	$0BB12_148
	FPDIV	r5,	r3,	r4
$0BB12_142:
	ADDI	r5,	r0,	3
	CMP	r5,	r5,	r7
	bneid	r5,	$0BB12_143
	NOP
	LOAD	r5,	r10,	0
	LOAD	r5,	r10,	1
	LOAD	r5,	r10,	2
	ADD	r5,	r12,	r20
	MULI	r5,	r5,	3
	ADD	r5,	r5,	r3
	LOAD	r6,	r5,	0
	LOAD	r6,	r5,	1
	LOAD	r5,	r5,	2
	MUL	r5,	r11,	r4
	ADD	r4,	r5,	r20
	ADD	r5,	r5,	r8
	MULI	r5,	r5,	3
	ADD	r5,	r5,	r3
	LOAD	r6,	r5,	0
	LOAD	r6,	r5,	1
	LOAD	r5,	r5,	2
	MULI	r4,	r4,	3
	ADD	r3,	r4,	r3
	LOAD	r4,	r3,	0
	LOAD	r4,	r3,	1
	LOAD	r3,	r3,	2
	brid	$0BB12_145
	NOP
$0BB12_143:
	ADDI	r5,	r0,	1
	CMP	r9,	r5,	r7
	ADD	r7,	r6,	r0
	ADD	r23,	r6,	r0
	bneid	r9,	$0BB12_148
	ADD	r5,	r6,	r0
	LOAD	r5,	r10,	0
	ADD	r5,	r20,	r3
	ADD	r6,	r5,	r12
	LOAD	r6,	r6,	0
	MUL	r4,	r11,	r4
	ADD	r6,	r4,	r8
	ADD	r3,	r6,	r3
	LOAD	r3,	r3,	0
	ADD	r3,	r5,	r4
	LOAD	r3,	r3,	0
$0BB12_145:
	ORI	r6,	r0,	1065353216
	ADD	r7,	r6,	r0
	ADD	r23,	r6,	r0
	ADD	r5,	r6,	r0
$0BB12_148:
	FPADD	r3,	r7,	r6
	FPADD	r3,	r3,	r23
	FPADD	r3,	r3,	r5
	ORI	r4,	r0,	1048576000
	FPMUL	r3,	r3,	r4
	SWI	r3,	r1,	248
$0BB12_149:
	ORI	r23,	r0,	0
	LWI	r3,	r1,	248
	FPNE	r4,	r3,	r23
	bneid	r4,	$0BB12_151
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_151:
	beqid	r3,	$0BB12_290
	LWI	r4,	r1,	212
	SWI	r4,	r1,	212
	ADD	r4,	r23,	r0
	ADD	r3,	r23,	r0
$0BB12_153:
	LWI	r22,	r1,	220
$0BB12_154:
	SWI	r3,	r1,	224
	SWI	r4,	r1,	208
	LWI	r11,	r1,	360
	LWI	r3,	r11,	12
	ADDI	r3,	r3,	10
	LOAD	r3,	r3,	0
	MULI	r3,	r3,	25
	LWI	r4,	r22,	4
	ADD	r4,	r4,	r3
	LOAD	r20,	r4,	15
	bltid	r20,	$0BB12_202
	NOP
	SWI	r21,	r1,	204
	LWI	r3,	r11,	12
	LOAD	r3,	r3,	9
	MULI	r6,	r3,	9
	LWI	r3,	r22,	8
	ADD	r5,	r3,	r6
	LOAD	r4,	r5,	0
	LOAD	r3,	r5,	1
	LOAD	r5,	r5,	2
	LWI	r5,	r22,	8
	ADD	r5,	r6,	r5
	ADDI	r7,	r5,	3
	LOAD	r9,	r7,	0
	LOAD	r5,	r7,	1
	LOAD	r7,	r7,	2
	LWI	r7,	r22,	8
	ADD	r6,	r6,	r7
	ADDI	r7,	r6,	6
	LOAD	r8,	r7,	0
	LOAD	r6,	r7,	1
	LOAD	r7,	r7,	2
	LWI	r7,	r11,	4
	FPMUL	r10,	r9,	r7
	LWI	r9,	r11,	0
	FPMUL	r4,	r4,	r9
	FPADD	r4,	r4,	r10
	ORI	r10,	r0,	1065353216
	FPRSUB	r10,	r9,	r10
	FPRSUB	r10,	r7,	r10
	FPMUL	r8,	r8,	r10
	FPADD	r4,	r4,	r8
	INTCONV	r8,	r4
	FPCONV	r8,	r8
	ADDI	r12,	r0,	1
	FPEQ	r11,	r4,	r8
	bneid	r11,	$0BB12_157
	NOP
	ADDI	r12,	r0,	0
$0BB12_157:
	FPMUL	r5,	r5,	r7
	FPMUL	r3,	r3,	r9
	FPADD	r3,	r3,	r5
	FPMUL	r5,	r6,	r10
	FPADD	r3,	r3,	r5
	LOAD	r21,	r20,	0
	LOAD	r10,	r20,	1
	LOAD	r11,	r20,	2
	bneid	r12,	$0BB12_159
	NOP
	FPRSUB	r4,	r8,	r4
$0BB12_159:
	INTCONV	r5,	r3
	FPCONV	r5,	r5
	FPEQ	r7,	r3,	r5
	bneid	r7,	$0BB12_161
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_161:
	bneid	r6,	$0BB12_163
	NOP
	FPRSUB	r3,	r5,	r3
$0BB12_163:
	ORI	r5,	r0,	0
	FPGE	r6,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_165
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_165:
	bneid	r5,	$0BB12_167
	NOP
	ORI	r5,	r0,	1065353216
	FPADD	r4,	r4,	r5
$0BB12_167:
	ORI	r5,	r0,	0
	FPGE	r6,	r3,	r5
	FPUN	r5,	r3,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_169
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_169:
	bneid	r5,	$0BB12_171
	NOP
	ORI	r5,	r0,	1065353216
	FPADD	r3,	r3,	r5
$0BB12_171:
	FPCONV	r5,	r10
	FPMUL	r7,	r4,	r5
	INTCONV	r4,	r7
	CMP	r5,	r10,	r4
	ADDI	r6,	r0,	0
	ADDI	r4,	r0,	1
	beqid	r5,	$0BB12_173
	ADD	r8,	r4,	r0
	ADD	r8,	r6,	r0
$0BB12_173:
	ORI	r5,	r0,	0
	bneid	r8,	$0BB12_175
	ADD	r9,	r5,	r0
	ADD	r9,	r7,	r0
$0BB12_175:
	FPGE	r7,	r9,	r5
	FPUN	r8,	r9,	r5
	BITOR	r12,	r8,	r7
	FPCONV	r7,	r21
	FPMUL	r3,	r3,	r7
	INTCONV	r7,	r3
	CMP	r8,	r21,	r7
	bneid	r12,	$0BB12_177
	ADD	r7,	r4,	r0
	ADD	r7,	r6,	r0
$0BB12_177:
	beqid	r8,	$0BB12_179
	NOP
	ADD	r4,	r6,	r0
$0BB12_179:
	bneid	r4,	$0BB12_181
	NOP
	ADD	r5,	r3,	r0
$0BB12_181:
	bneid	r7,	$0BB12_186
	NOP
	FPNEG	r3,	r9
	INTCONV	r4,	r3
	RSUBI	r3,	r4,	0
	FPCONV	r6,	r3
	FPEQ	r7,	r6,	r9
	bneid	r7,	$0BB12_184
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_184:
	bneid	r6,	$0BB12_187
	NOP
	ADDI	r3,	r0,	-1
	brid	$0BB12_187
	BITXOR	r3,	r4,	r3
$0BB12_202:
	SWI	r21,	r1,	204
	ORI	r3,	r0,	1065353216
	LOAD	r8,	r4,	4
	LOAD	r9,	r4,	5
	LOAD	r4,	r4,	6
	brid	$0BB12_203
	NOP
$0BB12_186:
	INTCONV	r3,	r9
$0BB12_187:
	ORI	r4,	r0,	0
	FPGE	r6,	r5,	r4
	FPUN	r4,	r5,	r4
	BITOR	r6,	r4,	r6
	bneid	r6,	$0BB12_189
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB12_189:
	DIV	r6,	r10,	r3
	MUL	r6,	r6,	r10
	RSUB	r3,	r6,	r3
	bsrai	r6,	r3,	31
	BITAND	r6,	r6,	r10
	ADD	r26,	r6,	r3
	ADDI	r3,	r26,	1
	DIV	r6,	r10,	r3
	MUL	r6,	r6,	r10
	bneid	r4,	$0BB12_194
	RSUB	r7,	r6,	r3
	FPNEG	r3,	r5
	INTCONV	r4,	r3
	RSUBI	r3,	r4,	0
	FPCONV	r6,	r3
	FPEQ	r8,	r6,	r5
	bneid	r8,	$0BB12_192
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_192:
	bneid	r6,	$0BB12_195
	NOP
	ADDI	r3,	r0,	-1
	brid	$0BB12_195
	BITXOR	r3,	r4,	r3
$0BB12_194:
	INTCONV	r3,	r5
$0BB12_195:
	DIV	r4,	r21,	r3
	MUL	r4,	r4,	r21
	RSUB	r3,	r4,	r3
	bsrai	r4,	r3,	31
	BITAND	r4,	r4,	r21
	ADD	r3,	r4,	r3
	FPCONV	r4,	r3
	FPRSUB	r4,	r4,	r5
	SWI	r4,	r1,	228
	FPCONV	r4,	r26
	FPRSUB	r31,	r4,	r9
	MUL	r12,	r3,	r10
	ADDI	r3,	r3,	1
	DIV	r4,	r21,	r3
	MUL	r4,	r4,	r21
	RSUB	r24,	r4,	r3
	ADD	r4,	r12,	r26
	ADDI	r3,	r0,	4
	CMP	r3,	r3,	r11
	MUL	r4,	r4,	r11
	ORI	r22,	r0,	1065353216
	ORI	r21,	r0,	0
	ADDI	r27,	r20,	3
	bneid	r3,	$0BB12_196
	ADD	r8,	r4,	r27
	MUL	r4,	r24,	r10
	ADD	r3,	r4,	r26
	ADD	r4,	r4,	r7
	ADD	r5,	r12,	r7
	bslli	r5,	r5,	2
	ADD	r5,	r5,	r27
	bslli	r6,	r4,	2
	bslli	r3,	r3,	2
	ADD	r4,	r3,	r27
	ADD	r3,	r6,	r27
	LOAD	r6,	r8,	1
	LOAD	r9,	r8,	2
	LOAD	r10,	r8,	3
	LOAD	r7,	r8,	0
	LOAD	r11,	r5,	1
	LOAD	r12,	r5,	2
	LOAD	r8,	r5,	3
	LOAD	r5,	r5,	0
	FPCONV	r10,	r10
	FPCONV	r20,	r9
	FPCONV	r9,	r12
	FPCONV	r12,	r11
	FPCONV	r21,	r7
	FPCONV	r24,	r6
	ORI	r6,	r0,	1132396544
	FPDIV	r7,	r20,	r6
	SWI	r7,	r1,	180
	FPDIV	r11,	r10,	r6
	LOAD	r7,	r4,	1
	LOAD	r10,	r4,	2
	LOAD	r20,	r4,	3
	LOAD	r22,	r4,	0
	FPCONV	r4,	r8
	FPDIV	r8,	r24,	r6
	SWI	r8,	r1,	192
	FPDIV	r8,	r21,	r6
	SWI	r8,	r1,	184
	FPDIV	r21,	r12,	r6
	FPDIV	r8,	r9,	r6
	SWI	r8,	r1,	188
	LOAD	r9,	r3,	1
	LOAD	r8,	r3,	2
	FPCONV	r8,	r8
	FPCONV	r9,	r9
	FPCONV	r12,	r22
	FPCONV	r20,	r20
	FPCONV	r10,	r10
	FPDIV	r4,	r4,	r6
	LOAD	r22,	r3,	3
	LOAD	r24,	r3,	0
	FPCONV	r3,	r22
	FPCONV	r25,	r7
	FPCONV	r5,	r5
	FPDIV	r22,	r5,	r6
	FPCONV	r7,	r24
	FPDIV	r29,	r25,	r6
	FPDIV	r5,	r10,	r6
	FPDIV	r25,	r20,	r6
	FPDIV	r28,	r12,	r6
	FPDIV	r20,	r9,	r6
	FPDIV	r9,	r8,	r6
	FPDIV	r3,	r3,	r6
	brid	$0BB12_201
	FPDIV	r6,	r7,	r6
$0BB12_196:
	ADDI	r3,	r0,	3
	CMP	r3,	r3,	r11
	bneid	r3,	$0BB12_197
	NOP
	MUL	r3,	r24,	r10
	ADD	r4,	r3,	r26
	ADD	r3,	r3,	r7
	ADD	r5,	r12,	r7
	MULI	r5,	r5,	3
	MULI	r4,	r4,	3
	ADD	r6,	r4,	r27
	ADD	r9,	r5,	r27
	MULI	r3,	r3,	3
	ADD	r4,	r3,	r27
	LOAD	r3,	r8,	0
	LOAD	r7,	r8,	1
	LOAD	r20,	r8,	2
	LOAD	r5,	r9,	0
	LOAD	r8,	r9,	1
	LOAD	r11,	r9,	2
	LOAD	r12,	r6,	0
	LOAD	r10,	r6,	1
	LOAD	r6,	r6,	2
	FPCONV	r21,	r20
	FPCONV	r9,	r7
	FPCONV	r7,	r6
	ORI	r6,	r0,	1132396544
	FPDIV	r9,	r9,	r6
	SWI	r9,	r1,	180
	LOAD	r9,	r4,	0
	FPCONV	r9,	r9
	FPCONV	r10,	r10
	FPCONV	r12,	r12
	FPCONV	r20,	r11
	FPDIV	r11,	r21,	r6
	LOAD	r21,	r4,	1
	LOAD	r4,	r4,	2
	FPCONV	r22,	r21
	FPCONV	r8,	r8
	FPCONV	r5,	r5
	FPCONV	r3,	r3
	FPDIV	r3,	r3,	r6
	SWI	r3,	r1,	192
	FPDIV	r21,	r5,	r6
	FPCONV	r3,	r4
	FPDIV	r4,	r8,	r6
	SWI	r4,	r1,	188
	FPDIV	r4,	r20,	r6
	FPDIV	r29,	r12,	r6
	FPDIV	r5,	r10,	r6
	FPDIV	r25,	r7,	r6
	FPDIV	r20,	r9,	r6
	FPDIV	r9,	r22,	r6
	FPDIV	r3,	r3,	r6
	ORI	r22,	r0,	1065353216
	SWI	r22,	r1,	184
	ADD	r28,	r22,	r0
	brid	$0BB12_201
	ADD	r6,	r22,	r0
$0BB12_197:
	ADDI	r3,	r0,	1
	CMP	r30,	r3,	r11
	SWI	r21,	r1,	188
	ADD	r4,	r21,	r0
	SWI	r22,	r1,	184
	ADD	r11,	r21,	r0
	SWI	r21,	r1,	180
	SWI	r21,	r1,	192
	ADD	r29,	r21,	r0
	ADD	r5,	r21,	r0
	ADD	r25,	r21,	r0
	ADD	r28,	r22,	r0
	ADD	r6,	r22,	r0
	ADD	r3,	r21,	r0
	ADD	r9,	r21,	r0
	bneid	r30,	$0BB12_201
	ADD	r20,	r21,	r0
	MUL	r4,	r24,	r10
	ADD	r3,	r4,	r26
	ADD	r3,	r3,	r27
	ADD	r5,	r7,	r27
	ADD	r6,	r5,	r4
	ADD	r4,	r5,	r12
	LOAD	r7,	r8,	0
	LOAD	r5,	r4,	0
	LOAD	r4,	r3,	0
	LOAD	r3,	r6,	0
	FPCONV	r3,	r3
	FPCONV	r7,	r7
	ORI	r6,	r0,	1132396544
	FPDIV	r3,	r3,	r6
	FPDIV	r11,	r7,	r6
	FPCONV	r5,	r5
	FPDIV	r21,	r5,	r6
	FPCONV	r4,	r4
	FPDIV	r29,	r4,	r6
	ORI	r22,	r0,	1065353216
	SWI	r21,	r1,	188
	ADD	r4,	r21,	r0
	SWI	r22,	r1,	184
	SWI	r11,	r1,	180
	SWI	r11,	r1,	192
	ADD	r5,	r29,	r0
	ADD	r25,	r29,	r0
	ADD	r28,	r22,	r0
	ADD	r6,	r22,	r0
	ADD	r9,	r3,	r0
	ADD	r20,	r3,	r0
$0BB12_201:
	ADD	r24,	r31,	r0
	FPMUL	r4,	r4,	r24
	ORI	r10,	r0,	1065353216
	LWI	r31,	r1,	228
	FPRSUB	r7,	r31,	r10
	FPMUL	r8,	r4,	r7
	FPRSUB	r4,	r24,	r10
	FPMUL	r10,	r11,	r4
	FPMUL	r10,	r10,	r7
	FPADD	r8,	r10,	r8
	LWI	r10,	r1,	188
	FPMUL	r12,	r10,	r24
	LWI	r10,	r1,	192
	FPMUL	r11,	r10,	r4
	FPMUL	r10,	r21,	r24
	FPMUL	r10,	r10,	r7
	FPMUL	r11,	r11,	r7
	FPMUL	r12,	r12,	r7
	LWI	r21,	r1,	180
	FPMUL	r21,	r21,	r4
	FPMUL	r21,	r21,	r7
	FPMUL	r7,	r25,	r4
	FPMUL	r7,	r7,	r31
	FPADD	r7,	r8,	r7
	FPADD	r8,	r21,	r12
	FPADD	r10,	r11,	r10
	FPMUL	r11,	r5,	r4
	FPMUL	r4,	r29,	r4
	FPMUL	r4,	r4,	r31
	FPADD	r5,	r10,	r4
	FPMUL	r4,	r11,	r31
	FPADD	r8,	r8,	r4
	FPMUL	r3,	r3,	r24
	FPMUL	r3,	r3,	r31
	FPADD	r4,	r7,	r3
	LWI	r3,	r1,	184
	FPADD	r3,	r3,	r22
	FPMUL	r7,	r9,	r24
	FPMUL	r9,	r20,	r24
	FPMUL	r10,	r9,	r31
	FPMUL	r7,	r7,	r31
	FPADD	r9,	r8,	r7
	FPADD	r8,	r5,	r10
	FPADD	r3,	r3,	r28
	FPADD	r3,	r3,	r6
	ORI	r5,	r0,	1048576000
	FPMUL	r3,	r3,	r5
	LWI	r22,	r1,	220
$0BB12_203:
	LWI	r5,	r1,	380
	SWI	r8,	r5,	0
	SWI	r9,	r5,	4
	SWI	r4,	r5,	8
	SWI	r3,	r5,	12
	ORI	r5,	r0,	1065353216
	LWI	r12,	r1,	288
	FPRSUB	r7,	r12,	r5
	FPMUL	r5,	r3,	r7
	FPADD	r6,	r12,	r5
	ORI	r21,	r0,	0
	FPEQ	r5,	r6,	r21
	ADDI	r10,	r0,	1
	LWI	r11,	r1,	284
	bneid	r5,	$0BB12_205
	LWI	r20,	r1,	292
	ADDI	r10,	r0,	0
$0BB12_205:
	ADD	r24,	r21,	r0
	ADD	r25,	r21,	r0
	bneid	r10,	$0BB12_207
	ADD	r5,	r21,	r0
	LWI	r5,	r1,	208
	FPMUL	r9,	r5,	r9
	LWI	r5,	r1,	224
	FPMUL	r8,	r5,	r8
	LWI	r5,	r1,	296
	FPMUL	r5,	r5,	r12
	FPMUL	r8,	r8,	r3
	FPMUL	r8,	r8,	r7
	FPADD	r5,	r5,	r8
	FPMUL	r8,	r20,	r12
	FPMUL	r9,	r9,	r3
	FPMUL	r9,	r9,	r7
	FPADD	r8,	r8,	r9
	FPMUL	r9,	r11,	r12
	FPMUL	r4,	r23,	r4
	FPMUL	r4,	r4,	r3
	FPMUL	r4,	r4,	r7
	FPADD	r4,	r9,	r4
	FPDIV	r24,	r4,	r6
	FPDIV	r25,	r8,	r6
	FPDIV	r5,	r5,	r6
	ADD	r21,	r6,	r0
$0BB12_207:
	SWI	r5,	r1,	228
	LWI	r4,	r1,	356
	SWI	r5,	r4,	0
	SWI	r25,	r4,	4
	SWI	r24,	r4,	8
	SWI	r21,	r4,	12
	ORI	r4,	r0,	1065353216
	FPGE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r4,	r3,	r5
	bneid	r4,	$0BB12_209
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB12_209:
	bneid	r3,	$0BB12_210
	SWI	r21,	r1,	192
	SWI	r25,	r1,	224
	SWI	r24,	r1,	208
	LWI	r31,	r1,	360
	SWI	r0,	r31,	12
	ADDI	r3,	r0,	1259902592
	SWI	r3,	r31,	8
	ADD	r11,	r0,	r0
	sbi	r11,	r31,	16
	sbi	r11,	r31,	17
	ADD	r6,	r31,	r0
	LWI	r5,	r1,	372
	LWI	r3,	r5,	4
	LWI	r4,	r1,	308
	FPRSUB	r4,	r4,	r3
	LWI	r3,	r5,	8
	LWI	r7,	r1,	304
	FPRSUB	r3,	r7,	r3
	LWI	r7,	r1,	336
	FPADD	r3,	r3,	r7
	SWI	r3,	r1,	180
	LWI	r3,	r1,	340
	FPADD	r3,	r4,	r3
	SWI	r3,	r1,	184
	LWI	r5,	r5,	0
	LWI	r3,	r1,	300
	FPRSUB	r5,	r3,	r5
	LWI	r3,	r1,	344
	FPADD	r9,	r5,	r3
	brid	$0BB12_212
	ADD	r4,	r11,	r0
$0BB12_257:
	bslli	r3,	r3,	2
	ADDI	r7,	r1,	52
	ADD	r3,	r7,	r3
	LWI	r11,	r3,	-4
	ADD	r4,	r5,	r0
	LWI	r22,	r1,	220
$0BB12_212:
	SWI	r4,	r1,	188
	bslli	r5,	r11,	3
	LWI	r3,	r22,	0
	ADD	r23,	r3,	r5
	LOAD	r3,	r23,	0
	LOAD	r7,	r23,	1
	LOAD	r5,	r23,	2
	ADDI	r8,	r23,	3
	LOAD	r10,	r8,	0
	LOAD	r11,	r8,	1
	LWI	r4,	r1,	184
	FPRSUB	r12,	r4,	r11
	FPRSUB	r11,	r4,	r7
	LOAD	r7,	r8,	2
	LWI	r4,	r1,	180
	FPRSUB	r8,	r4,	r7
	FPRSUB	r5,	r4,	r5
	LWI	r4,	r1,	268
	FPMUL	r7,	r5,	r4
	FPMUL	r8,	r8,	r4
	LWI	r4,	r1,	272
	FPMUL	r11,	r11,	r4
	FPMUL	r12,	r12,	r4
	FPRSUB	r5,	r9,	r10
	FPRSUB	r3,	r9,	r3
	LWI	r4,	r1,	276
	FPMUL	r3,	r3,	r4
	FPMUL	r20,	r5,	r4
	FPMIN	r5,	r3,	r20
	FPMIN	r10,	r11,	r12
	FPMIN	r21,	r7,	r8
	FPMAX	r3,	r3,	r20
	FPMAX	r11,	r11,	r12
	FPMAX	r7,	r7,	r8
	FPMAX	r8,	r10,	r21
	FPMAX	r8,	r5,	r8
	FPMIN	r5,	r11,	r7
	FPMIN	r10,	r3,	r5
	FPGE	r5,	r8,	r10
	FPUN	r7,	r8,	r10
	BITOR	r7,	r7,	r5
	bneid	r7,	$0BB12_214
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_214:
	bneid	r5,	$0BB12_255
	ADD	r21,	r6,	r0
	ORI	r5,	r0,	869711765
	FPLE	r7,	r8,	r5
	FPUN	r5,	r8,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB12_217
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_217:
	bneid	r5,	$0BB12_221
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r7,	r8,	r5
	bneid	r7,	$0BB12_220
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_220:
	beqid	r5,	$0BB12_221
	NOP
	LWI	r5,	r21,	8
	FPLT	r6,	r5,	r8
	FPUN	r5,	r5,	r8
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB12_229
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_229:
	bneid	r5,	$0BB12_255
	NOP
	brid	$0BB12_230
	NOP
$0BB12_221:
	ORI	r5,	r0,	869711765
	FPLE	r7,	r10,	r5
	FPUN	r5,	r10,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB12_223
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_223:
	bneid	r5,	$0BB12_255
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r6,	r10,	r5
	bneid	r6,	$0BB12_226
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_226:
	beqid	r5,	$0BB12_255
	NOP
$0BB12_230:
	LOAD	r11,	r23,	7
	LOAD	r23,	r23,	6
	bgeid	r23,	$0BB12_231
	NOP
	ADD	r6,	r21,	r0
	ADDI	r5,	r11,	1
	LWI	r4,	r1,	188
	bslli	r3,	r4,	2
	ADDI	r7,	r1,	52
	SW	r5,	r7,	r3
	ADDI	r4,	r4,	1
	brid	$0BB12_212
	LWI	r22,	r1,	220
$0BB12_231:
	bleid	r23,	$0BB12_255
	ADD	r25,	r0,	r0
$0BB12_232:
	ADD	r3,	r21,	r0
	LOAD	r8,	r11,	0
	LOAD	r24,	r11,	1
	LOAD	r7,	r11,	2
	ADDI	r10,	r11,	3
	LOAD	r6,	r10,	0
	LOAD	r5,	r10,	1
	LOAD	r10,	r10,	2
	ADDI	r12,	r11,	6
	LOAD	r28,	r12,	0
	LOAD	r30,	r12,	1
	LOAD	r26,	r12,	2
	FPRSUB	r27,	r26,	r10
	FPRSUB	r20,	r30,	r5
	LWI	r12,	r1,	244
	FPMUL	r5,	r12,	r20
	LWI	r4,	r1,	240
	FPMUL	r10,	r4,	r27
	FPRSUB	r31,	r5,	r10
	FPRSUB	r29,	r28,	r6
	FPMUL	r5,	r12,	r29
	LWI	r22,	r1,	236
	FPMUL	r6,	r22,	r27
	FPRSUB	r10,	r6,	r5
	FPRSUB	r6,	r28,	r9
	LWI	r5,	r1,	184
	FPRSUB	r12,	r30,	r5
	FPMUL	r5,	r12,	r10
	FPMUL	r21,	r6,	r31
	FPADD	r5,	r21,	r5
	FPMUL	r21,	r4,	r29
	FPMUL	r22,	r22,	r20
	FPRSUB	r21,	r21,	r22
	FPRSUB	r28,	r28,	r8
	FPRSUB	r30,	r30,	r24
	LWI	r4,	r1,	180
	FPRSUB	r24,	r26,	r4
	FPMUL	r8,	r24,	r21
	FPADD	r5,	r5,	r8
	FPMUL	r8,	r30,	r10
	FPMUL	r10,	r28,	r31
	FPADD	r8,	r10,	r8
	FPRSUB	r7,	r26,	r7
	FPMUL	r10,	r7,	r21
	FPADD	r8,	r8,	r10
	ORI	r10,	r0,	1065353216
	FPDIV	r8,	r10,	r8
	FPMUL	r26,	r5,	r8
	ORI	r5,	r0,	0
	FPLT	r10,	r26,	r5
	bneid	r10,	$0BB12_234
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_234:
	bneid	r5,	$0BB12_254
	ADD	r21,	r3,	r0
	ORI	r5,	r0,	1065353216
	FPGT	r10,	r26,	r5
	bneid	r10,	$0BB12_237
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_237:
	bneid	r5,	$0BB12_254
	NOP
	FPMUL	r5,	r24,	r30
	FPMUL	r10,	r12,	r7
	FPRSUB	r31,	r5,	r10
	FPMUL	r5,	r6,	r7
	FPMUL	r7,	r24,	r28
	FPRSUB	r24,	r5,	r7
	LWI	r3,	r1,	240
	FPMUL	r5,	r3,	r24
	LWI	r3,	r1,	236
	FPMUL	r7,	r3,	r31
	FPADD	r5,	r7,	r5
	FPMUL	r7,	r12,	r28
	FPMUL	r6,	r6,	r30
	FPRSUB	r7,	r7,	r6
	LWI	r3,	r1,	244
	FPMUL	r6,	r3,	r7
	FPADD	r5,	r5,	r6
	FPMUL	r6,	r5,	r8
	ORI	r5,	r0,	0
	FPLT	r10,	r6,	r5
	bneid	r10,	$0BB12_240
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_240:
	bneid	r5,	$0BB12_254
	NOP
	FPADD	r5,	r6,	r26
	ORI	r10,	r0,	1065353216
	FPGT	r10,	r5,	r10
	bneid	r10,	$0BB12_243
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_243:
	bneid	r5,	$0BB12_254
	NOP
	FPMUL	r5,	r20,	r24
	FPMUL	r10,	r29,	r31
	FPADD	r5,	r10,	r5
	FPMUL	r7,	r27,	r7
	FPADD	r5,	r5,	r7
	FPMUL	r8,	r5,	r8
	ORI	r5,	r0,	0
	FPLE	r7,	r8,	r5
	FPUN	r5,	r8,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB12_246
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_246:
	bneid	r5,	$0BB12_254
	NOP
	ORI	r5,	r0,	869711765
	FPLE	r7,	r8,	r5
	FPUN	r5,	r8,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB12_249
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_249:
	bneid	r5,	$0BB12_254
	NOP
	LWI	r5,	r21,	8
	FPGE	r7,	r8,	r5
	FPUN	r5,	r8,	r5
	BITOR	r7,	r5,	r7
	bneid	r7,	$0BB12_252
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_252:
	bneid	r5,	$0BB12_254
	NOP
	SWI	r8,	r21,	8
	SWI	r11,	r21,	12
	ADDI	r5,	r0,	1
	sbi	r5,	r21,	16
	ADD	r5,	r0,	r0
	sbi	r5,	r21,	17
	SWI	r26,	r21,	0
	SWI	r6,	r21,	4
$0BB12_254:
	ADDI	r25,	r25,	1
	CMP	r5,	r23,	r25
	bltid	r5,	$0BB12_232
	ADDI	r11,	r11,	11
$0BB12_255:
	ADDI	r5,	r0,	-1
	LWI	r3,	r1,	188
	ADD	r5,	r3,	r5
	bgeid	r5,	$0BB12_257
	ADD	r6,	r21,	r0
	brid	$0BB12_256
	NOP
$0BB12_99:
	LWI	r3,	r1,	320
	LWI	r6,	r1,	324
	FPMUL	r3,	r3,	r6
	LWI	r4,	r1,	316
	FPMUL	r4,	r4,	r6
	LWI	r5,	r1,	312
	FPMUL	r6,	r5,	r6
	ORI	r5,	r0,	0
	FPADD	r23,	r6,	r5
	FPADD	r4,	r4,	r5
	brid	$0BB12_153
	FPADD	r3,	r3,	r5
$0BB12_210:
	brid	$0BB12_263
	LWI	r20,	r1,	280
$0BB12_258:
	ORI	r3,	r0,	1065353216
	FPRSUB	r4,	r12,	r3
	LWI	r5,	r22,	60
	FPMUL	r3,	r5,	r4
	FPADD	r3,	r12,	r3
	ORI	r6,	r0,	0
	FPEQ	r7,	r3,	r6
	bneid	r7,	$0BB12_260
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB12_260:
	ADD	r7,	r6,	r0
	ADD	r8,	r6,	r0
	bneid	r10,	$0BB12_262
	ADD	r9,	r6,	r0
	FPMUL	r6,	r23,	r12
	LWI	r7,	r22,	48
	FPMUL	r7,	r7,	r5
	FPMUL	r7,	r7,	r4
	FPADD	r6,	r6,	r7
	FPMUL	r7,	r21,	r12
	LWI	r8,	r22,	52
	FPMUL	r8,	r8,	r5
	FPMUL	r8,	r8,	r4
	FPADD	r8,	r7,	r8
	FPMUL	r7,	r11,	r12
	LWI	r9,	r22,	56
	FPMUL	r5,	r9,	r5
	FPMUL	r4,	r5,	r4
	FPADD	r4,	r7,	r4
	FPDIV	r7,	r4,	r3
	FPDIV	r8,	r8,	r3
	FPDIV	r9,	r6,	r3
	ADD	r6,	r3,	r0
$0BB12_262:
	LWI	r3,	r1,	356
	SWI	r9,	r3,	0
	SWI	r8,	r3,	4
	SWI	r7,	r3,	8
	SWI	r6,	r3,	12
$0BB12_263:
	RAND	r4
	RAND	r3
	FPADD	r3,	r3,	r3
	ORI	r5,	r0,	-1082130432
	FPADD	r3,	r3,	r5
	FPADD	r4,	r4,	r4
	FPADD	r4,	r4,	r5
	FPMUL	r6,	r4,	r4
	FPMUL	r7,	r3,	r3
	FPADD	r5,	r6,	r7
	ORI	r8,	r0,	1065353216
	FPGE	r8,	r5,	r8
	bneid	r8,	$0BB12_265
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB12_265:
	bneid	r5,	$0BB12_263
	NOP
	ORI	r5,	r0,	0
	LWI	r9,	r1,	332
	FPGE	r8,	r9,	r5
	FPUN	r5,	r9,	r5
	BITOR	r5,	r5,	r8
	bneid	r5,	$0BB12_268
	ADDI	r8,	r0,	1
	ADDI	r8,	r0,	0
$0BB12_268:
	ORI	r5,	r0,	1065353216
	FPRSUB	r6,	r6,	r5
	FPRSUB	r6,	r7,	r6
	FPINVSQRT	r7,	r6
	bneid	r8,	$0BB12_270
	ADD	r6,	r9,	r0
	FPNEG	r6,	r9
$0BB12_270:
	ADD	r21,	r9,	r0
	ORI	r8,	r0,	0
	LWI	r10,	r1,	328
	FPGE	r9,	r10,	r8
	FPUN	r8,	r10,	r8
	BITOR	r8,	r8,	r9
	bneid	r8,	$0BB12_272
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB12_272:
	bneid	r9,	$0BB12_274
	ADD	r8,	r10,	r0
	FPNEG	r8,	r10
$0BB12_274:
	ADD	r12,	r10,	r0
	ORI	r9,	r0,	0
	FPGE	r10,	r20,	r9
	FPUN	r9,	r20,	r9
	BITOR	r9,	r9,	r10
	bneid	r9,	$0BB12_276
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB12_276:
	bneid	r10,	$0BB12_278
	ADD	r9,	r20,	r0
	FPNEG	r9,	r20
$0BB12_278:
	FPGE	r10,	r6,	r8
	FPUN	r11,	r6,	r8
	BITOR	r10,	r11,	r10
	bneid	r10,	$0BB12_280
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB12_280:
	FPDIV	r5,	r5,	r7
	ORI	r10,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r11,	$0BB12_284
	NOP
	FPLT	r11,	r6,	r9
	bneid	r11,	$0BB12_283
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_283:
	bneid	r6,	$0BB12_288
	ADD	r11,	r7,	r0
$0BB12_284:
	FPLT	r7,	r8,	r9
	bneid	r7,	$0BB12_286
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB12_286:
	ORI	r11,	r0,	1065353216
	ORI	r7,	r0,	0
	bneid	r6,	$0BB12_288
	ADD	r10,	r7,	r0
	ORI	r11,	r0,	0
	ORI	r7,	r0,	1065353216
	ADD	r10,	r11,	r0
$0BB12_288:
	FPMUL	r6,	r12,	r10
	FPMUL	r8,	r21,	r11
	FPRSUB	r6,	r6,	r8
	FPMUL	r8,	r20,	r10
	FPMUL	r9,	r21,	r7
	FPRSUB	r8,	r9,	r8
	FPMUL	r9,	r20,	r8
	FPMUL	r10,	r12,	r6
	FPRSUB	r9,	r9,	r10
	FPMUL	r10,	r20,	r11
	FPMUL	r7,	r12,	r7
	FPRSUB	r10,	r10,	r7
	FPMUL	r7,	r21,	r6
	FPMUL	r11,	r20,	r10
	FPRSUB	r11,	r7,	r11
	FPMUL	r7,	r9,	r3
	FPMUL	r9,	r10,	r4
	FPADD	r7,	r9,	r7
	FPMUL	r9,	r21,	r5
	FPADD	r7,	r7,	r9
	FPMUL	r9,	r11,	r3
	FPMUL	r11,	r8,	r4
	FPADD	r9,	r11,	r9
	FPMUL	r11,	r12,	r5
	FPADD	r9,	r9,	r11
	FPMUL	r10,	r12,	r10
	FPMUL	r11,	r21,	r8
	FPMUL	r8,	r9,	r9
	FPMUL	r12,	r7,	r7
	FPADD	r8,	r12,	r8
	FPRSUB	r10,	r10,	r11
	FPMUL	r3,	r10,	r3
	FPMUL	r4,	r6,	r4
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r20,	r5
	FPADD	r3,	r3,	r4
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r8,	r4
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r5,	r7,	r4
	LWI	r6,	r1,	376
	SWI	r5,	r6,	0
	FPMUL	r5,	r9,	r4
	SWI	r5,	r6,	4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r6,	8
	LWI	r31,	r1,	4
	LWI	r30,	r1,	8
	LWI	r29,	r1,	12
	LWI	r28,	r1,	16
	LWI	r27,	r1,	20
	LWI	r26,	r1,	24
	LWI	r25,	r1,	28
	LWI	r24,	r1,	32
	LWI	r23,	r1,	36
	LWI	r22,	r1,	40
	LWI	r21,	r1,	44
	LWI	r20,	r1,	48
	rtsd	r15,	8
	ADDI	r1,	r1,	352
#	.end	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color
$0tmp12:
#	.size	_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color, ($tmp12)-_Z20shadeTexturedLambertR9HitRecordRK3RayRK5SceneR6VectorS8_R5Color

#	.globl	_Z9trax_mainv
#	.align	2
#	.type	_Z9trax_mainv,@function
#	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
#	.frame	r1,400,r15
#	.mask	0xfff00000
	ADDI	r1,	r1,	-400
	SWI	r20,	r1,	44
	SWI	r21,	r1,	40
	SWI	r22,	r1,	36
	SWI	r23,	r1,	32
	SWI	r24,	r1,	28
	SWI	r25,	r1,	24
	SWI	r26,	r1,	20
	SWI	r27,	r1,	16
	SWI	r28,	r1,	12
	SWI	r29,	r1,	8
	SWI	r30,	r1,	4
	SWI	r31,	r1,	0
	ADD	r4,	r0,	r0
	LOAD	r3,	r4,	8
	SWI	r3,	r1,	188
	LOAD	r3,	r4,	12
	LOAD	r5,	r3,	0
	SWI	r5,	r1,	268
	LOAD	r5,	r3,	1
	SWI	r5,	r1,	272
	LOAD	r3,	r3,	2
	SWI	r3,	r1,	276
	LOAD	r8,	r4,	1
	SWI	r8,	r1,	368
	LOAD	r3,	r4,	4
	LOAD	r5,	r4,	2
	SWI	r5,	r1,	352
	LOAD	r5,	r4,	5
	SWI	r5,	r1,	356
	LOAD	r5,	r4,	7
	SWI	r5,	r1,	372
	LOAD	r5,	r4,	9
	SWI	r5,	r1,	280
	LOAD	r9,	r4,	17
	SWI	r9,	r1,	292
	LOAD	r5,	r4,	16
	SWI	r5,	r1,	284
	LOAD	r5,	r4,	30
	LOAD	r6,	r4,	10
	LOAD	r5,	r6,	0
	SWI	r5,	r1,	296
	LOAD	r5,	r6,	1
	SWI	r5,	r1,	300
	LOAD	r5,	r6,	2
	SWI	r5,	r1,	304
	ADDI	r5,	r6,	9
	LOAD	r7,	r5,	0
	LOAD	r7,	r5,	1
	LOAD	r5,	r5,	2
	ADDI	r5,	r6,	18
	ADDI	r7,	r6,	15
	ADDI	r6,	r6,	12
	LOAD	r10,	r6,	0
	SWI	r10,	r1,	308
	LOAD	r10,	r6,	1
	SWI	r10,	r1,	312
	LOAD	r6,	r6,	2
	SWI	r6,	r1,	316
	LOAD	r6,	r7,	0
	SWI	r6,	r1,	320
	LOAD	r6,	r7,	1
	SWI	r6,	r1,	324
	LOAD	r6,	r7,	2
	SWI	r6,	r1,	328
	LOAD	r6,	r5,	0
	SWI	r6,	r1,	332
	LOAD	r6,	r5,	1
	SWI	r6,	r1,	336
	LOAD	r5,	r5,	2
	SWI	r5,	r1,	340
	LOAD	r4,	r4,	8
	MUL	r5,	r3,	r8
	SWI	r5,	r1,	376
	ATOMIC_INC	r4,	0
	CMP	r5,	r5,	r4
	bgeid	r5,	$0BB13_308
	NOP
	FPCONV	r5,	r9
	SWI	r5,	r1,	380
	FPCONV	r5,	r3
	SWI	r5,	r1,	384
	ORI	r3,	r0,	1056964608
	FPMUL	r5,	r5,	r3
	SWI	r5,	r1,	388
	LWI	r5,	r1,	368
	FPCONV	r5,	r5
	SWI	r5,	r1,	392
	FPMUL	r3,	r5,	r3
	brid	$0BB13_2
	SWI	r3,	r1,	396
$0BB13_5:
	SWI	r25,	r1,	236
	SWI	r25,	r1,	232
$0BB13_6:
	ORI	r3,	r0,	-1090519040
	RAND	r4
	FPADD	r4,	r4,	r3
	RAND	r5
	FPADD	r3,	r5,	r3
	FPADD	r4,	r4,	r4
	LWI	r5,	r1,	352
	FPMUL	r4,	r4,	r5
	FPADD	r4,	r8,	r4
	LWI	r5,	r1,	320
	FPMUL	r5,	r5,	r4
	LWI	r6,	r1,	308
	FPADD	r5,	r6,	r5
	FPADD	r3,	r3,	r3
	LWI	r6,	r1,	356
	FPMUL	r3,	r3,	r6
	FPADD	r6,	r7,	r3
	LWI	r3,	r1,	332
	FPMUL	r3,	r3,	r6
	FPADD	r3,	r5,	r3
	LWI	r5,	r1,	324
	FPMUL	r5,	r5,	r4
	LWI	r7,	r1,	312
	FPADD	r5,	r7,	r5
	LWI	r7,	r1,	336
	FPMUL	r7,	r7,	r6
	FPADD	r5,	r5,	r7
	FPMUL	r7,	r5,	r5
	FPMUL	r8,	r3,	r3
	FPADD	r7,	r8,	r7
	LWI	r8,	r1,	340
	FPMUL	r6,	r8,	r6
	LWI	r8,	r1,	328
	FPMUL	r4,	r8,	r4
	LWI	r8,	r1,	316
	FPADD	r4,	r8,	r4
	FPADD	r4,	r4,	r6
	FPMUL	r6,	r4,	r4
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r24,	r0,	1065353216
	FPDIV	r6,	r24,	r6
	FPDIV	r6,	r24,	r6
	FPMUL	r4,	r4,	r6
	SWI	r4,	r1,	196
	FPMUL	r26,	r5,	r6
	FPMUL	r3,	r3,	r6
	SWI	r3,	r1,	192
	ADD	r4,	r0,	r0
	LWI	r3,	r1,	304
	SWI	r3,	r1,	208
	LWI	r3,	r1,	300
	SWI	r3,	r1,	212
	LWI	r3,	r1,	296
	SWI	r3,	r1,	216
	ADD	r27,	r24,	r0
	brid	$0BB13_7
	ADD	r28,	r24,	r0
$0BB13_284:
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r24,	r24,	r10
	FPMUL	r27,	r27,	r31
	FPMUL	r28,	r28,	r21
	FPMUL	r5,	r9,	r4
	SWI	r5,	r1,	192
	FPMUL	r26,	r11,	r4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r1,	196
	LWI	r3,	r1,	184
	SWI	r3,	r1,	208
	LWI	r3,	r1,	176
	SWI	r3,	r1,	212
	LWI	r3,	r1,	180
	SWI	r3,	r1,	216
	LWI	r4,	r1,	248
$0BB13_7:
	LWI	r3,	r1,	284
	CMP	r3,	r3,	r4
	bgeid	r3,	$0BB13_285
	NOP
	SWI	r28,	r1,	256
	SWI	r27,	r1,	252
	SWI	r24,	r1,	244
	SWI	r25,	r1,	240
	ADDI	r4,	r4,	1
	SWI	r4,	r1,	248
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	196
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	220
	FPDIV	r4,	r3,	r26
	SWI	r4,	r1,	224
	LWI	r4,	r1,	192
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	228
	ORI	r29,	r0,	1203982336
	ADD	r3,	r0,	r0
	SWI	r3,	r1,	200
	SWI	r3,	r1,	204
	brid	$0BB13_9
	ADD	r10,	r3,	r0
$0BB13_197:
	bslli	r4,	r31,	2
	ADDI	r5,	r1,	48
	ADD	r4,	r5,	r4
	LWI	r10,	r4,	-4
$0BB13_9:
	brid	$0BB13_10
	ADD	r31,	r3,	r0
$0BB13_311:
	ADDI	r3,	r10,	1
	bslli	r4,	r31,	2
	ADDI	r5,	r1,	48
	SW	r3,	r5,	r4
	ADDI	r31,	r31,	1
$0BB13_10:
	bslli	r3,	r10,	3
	LWI	r4,	r1,	188
	ADD	r7,	r3,	r4
	LOAD	r3,	r7,	0
	LOAD	r5,	r7,	1
	LOAD	r4,	r7,	2
	ADDI	r10,	r7,	3
	LOAD	r8,	r10,	0
	LOAD	r6,	r10,	1
	LWI	r9,	r1,	212
	FPRSUB	r6,	r9,	r6
	FPRSUB	r9,	r9,	r5
	LOAD	r5,	r10,	2
	LWI	r10,	r1,	208
	FPRSUB	r5,	r10,	r5
	FPRSUB	r4,	r10,	r4
	LWI	r10,	r1,	220
	FPMUL	r4,	r4,	r10
	FPMUL	r5,	r5,	r10
	LWI	r11,	r1,	224
	FPMUL	r10,	r9,	r11
	FPMUL	r6,	r6,	r11
	LWI	r11,	r1,	216
	FPRSUB	r9,	r11,	r8
	FPRSUB	r3,	r11,	r3
	LWI	r11,	r1,	228
	FPMUL	r8,	r3,	r11
	FPMUL	r9,	r9,	r11
	FPMIN	r3,	r8,	r9
	FPMIN	r11,	r10,	r6
	FPMIN	r12,	r4,	r5
	FPMAX	r9,	r8,	r9
	FPMAX	r6,	r10,	r6
	FPMAX	r4,	r4,	r5
	FPMAX	r5,	r11,	r12
	FPMAX	r8,	r3,	r5
	FPMIN	r3,	r6,	r4
	FPMIN	r3,	r9,	r3
	FPGE	r4,	r8,	r3
	FPUN	r5,	r8,	r3
	BITOR	r5,	r5,	r4
	bneid	r5,	$0BB13_12
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_12:
	bneid	r4,	$0BB13_196
	NOP
	ORI	r4,	r0,	869711765
	FPLE	r5,	r8,	r4
	FPUN	r4,	r8,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_15
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_15:
	bneid	r4,	$0BB13_19
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r5,	r8,	r4
	bneid	r5,	$0BB13_18
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_18:
	beqid	r4,	$0BB13_19
	NOP
	FPLT	r3,	r29,	r8
	FPUN	r4,	r29,	r8
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB13_167
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_167:
	bneid	r3,	$0BB13_196
	NOP
	brid	$0BB13_168
	NOP
$0BB13_19:
	ORI	r4,	r0,	869711765
	FPLE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_21
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_21:
	bneid	r4,	$0BB13_196
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB13_24
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_24:
	beqid	r3,	$0BB13_196
	NOP
$0BB13_168:
	LOAD	r10,	r7,	7
	LOAD	r8,	r7,	6
	bltid	r8,	$0BB13_311
	NOP
	bleid	r8,	$0BB13_196
	ADD	r7,	r0,	r0
$0BB13_170:
	LOAD	r4,	r10,	0
	LOAD	r12,	r10,	1
	LOAD	r20,	r10,	2
	ADDI	r6,	r10,	3
	LOAD	r3,	r6,	0
	LOAD	r5,	r6,	1
	LOAD	r6,	r6,	2
	ADDI	r9,	r10,	6
	LOAD	r23,	r9,	0
	LOAD	r27,	r9,	1
	LOAD	r22,	r9,	2
	FPRSUB	r21,	r22,	r6
	FPRSUB	r30,	r27,	r5
	LWI	r9,	r1,	196
	FPMUL	r5,	r9,	r30
	FPMUL	r6,	r26,	r21
	FPRSUB	r28,	r5,	r6
	FPRSUB	r3,	r23,	r3
	FPMUL	r5,	r9,	r3
	LWI	r24,	r1,	192
	FPMUL	r6,	r24,	r21
	FPRSUB	r5,	r6,	r5
	LWI	r6,	r1,	216
	FPRSUB	r25,	r23,	r6
	LWI	r6,	r1,	212
	FPRSUB	r11,	r27,	r6
	FPMUL	r6,	r11,	r5
	FPMUL	r9,	r25,	r28
	FPADD	r9,	r9,	r6
	FPMUL	r6,	r26,	r3
	FPMUL	r24,	r24,	r30
	FPRSUB	r6,	r6,	r24
	FPRSUB	r23,	r23,	r4
	FPRSUB	r12,	r27,	r12
	LWI	r4,	r1,	208
	FPRSUB	r27,	r22,	r4
	FPMUL	r4,	r27,	r6
	FPADD	r9,	r9,	r4
	FPMUL	r4,	r12,	r5
	FPMUL	r5,	r23,	r28
	FPADD	r5,	r5,	r4
	FPRSUB	r4,	r22,	r20
	FPMUL	r6,	r4,	r6
	FPADD	r5,	r5,	r6
	ORI	r6,	r0,	1065353216
	FPDIV	r20,	r6,	r5
	FPMUL	r22,	r9,	r20
	ORI	r5,	r0,	0
	FPLT	r6,	r22,	r5
	bneid	r6,	$0BB13_172
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_172:
	bneid	r5,	$0BB13_195
	NOP
	ORI	r5,	r0,	1065353216
	FPGT	r6,	r22,	r5
	bneid	r6,	$0BB13_175
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_175:
	bneid	r5,	$0BB13_195
	NOP
	FPMUL	r5,	r27,	r12
	FPMUL	r6,	r11,	r4
	FPRSUB	r28,	r5,	r6
	FPMUL	r4,	r25,	r4
	FPMUL	r5,	r27,	r23
	FPRSUB	r27,	r4,	r5
	FPMUL	r4,	r26,	r27
	LWI	r5,	r1,	192
	FPMUL	r5,	r5,	r28
	FPADD	r5,	r5,	r4
	FPMUL	r4,	r11,	r23
	FPMUL	r6,	r25,	r12
	FPRSUB	r4,	r4,	r6
	LWI	r6,	r1,	196
	FPMUL	r6,	r6,	r4
	FPADD	r5,	r5,	r6
	FPMUL	r5,	r5,	r20
	ORI	r6,	r0,	0
	FPLT	r9,	r5,	r6
	bneid	r9,	$0BB13_178
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB13_178:
	bneid	r6,	$0BB13_195
	NOP
	FPADD	r5,	r5,	r22
	ORI	r6,	r0,	1065353216
	FPGT	r6,	r5,	r6
	bneid	r6,	$0BB13_181
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_181:
	bneid	r5,	$0BB13_195
	NOP
	FPMUL	r5,	r30,	r27
	FPMUL	r3,	r3,	r28
	FPADD	r3,	r3,	r5
	FPMUL	r4,	r21,	r4
	FPADD	r3,	r3,	r4
	FPMUL	r3,	r3,	r20
	ORI	r4,	r0,	869711765
	FPGT	r6,	r3,	r4
	ADDI	r4,	r0,	0
	ADDI	r11,	r0,	1
	bneid	r6,	$0BB13_184
	ADD	r5,	r11,	r0
	ADD	r5,	r4,	r0
$0BB13_184:
	ORI	r6,	r0,	0
	FPGT	r9,	r3,	r6
	bneid	r9,	$0BB13_186
	ADD	r6,	r11,	r0
	ADD	r6,	r4,	r0
$0BB13_186:
	BITAND	r5,	r6,	r5
	FPLT	r9,	r3,	r29
	bneid	r9,	$0BB13_188
	ADD	r6,	r11,	r0
	ADD	r6,	r4,	r0
$0BB13_188:
	BITAND	r4,	r5,	r6
	bneid	r4,	$0BB13_190
	NOP
	ADD	r3,	r29,	r0
$0BB13_190:
	bneid	r4,	$0BB13_192
	ADD	r5,	r10,	r0
	LWI	r5,	r1,	204
$0BB13_192:
	bneid	r4,	$0BB13_194
	NOP
	LWI	r11,	r1,	200
$0BB13_194:
	SWI	r11,	r1,	200
	SWI	r5,	r1,	204
	ADD	r29,	r3,	r0
$0BB13_195:
	ADDI	r7,	r7,	1
	CMP	r3,	r8,	r7
	bltid	r3,	$0BB13_170
	ADDI	r10,	r10,	11
$0BB13_196:
	ADDI	r3,	r0,	-1
	ADD	r3,	r31,	r3
	bgeid	r3,	$0BB13_197
	NOP
	ORI	r10,	r0,	1065353216
	ORI	r8,	r0,	1057988018
	ORI	r7,	r0,	1060806590
	ORI	r30,	r0,	1065151889
	LWI	r3,	r1,	200
	ANDI	r3,	r3,	255
	ADD	r31,	r10,	r0
	beqid	r3,	$0BB13_257
	ADD	r21,	r10,	r0
	LWI	r10,	r1,	204
	LOAD	r3,	r10,	0
	LOAD	r6,	r10,	1
	LOAD	r4,	r10,	2
	ADDI	r7,	r10,	3
	LOAD	r5,	r7,	0
	LOAD	r8,	r7,	1
	LOAD	r9,	r7,	2
	ADDI	r11,	r10,	6
	LOAD	r10,	r11,	0
	LOAD	r12,	r11,	1
	FPRSUB	r7,	r12,	r6
	FPRSUB	r8,	r12,	r8
	LOAD	r6,	r11,	2
	FPRSUB	r9,	r6,	r9
	FPRSUB	r11,	r6,	r4
	FPMUL	r4,	r11,	r8
	FPMUL	r6,	r7,	r9
	FPRSUB	r4,	r4,	r6
	FPRSUB	r5,	r10,	r5
	FPRSUB	r6,	r10,	r3
	FPMUL	r3,	r6,	r9
	FPMUL	r9,	r11,	r5
	FPRSUB	r3,	r3,	r9
	FPMUL	r9,	r3,	r3
	FPMUL	r10,	r4,	r4
	FPADD	r9,	r10,	r9
	FPMUL	r5,	r7,	r5
	FPMUL	r6,	r6,	r8
	FPRSUB	r5,	r5,	r6
	FPMUL	r6,	r5,	r5
	FPADD	r6,	r9,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r6,	r7,	r6
	FPDIV	r6,	r7,	r6
	FPMUL	r11,	r4,	r6
	FPMUL	r12,	r3,	r6
	FPMUL	r3,	r12,	r26
	LWI	r4,	r1,	192
	FPMUL	r4,	r11,	r4
	FPADD	r3,	r4,	r3
	FPMUL	r10,	r5,	r6
	LWI	r4,	r1,	196
	FPMUL	r4,	r10,	r4
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r3,	r3,	r5
	bneid	r3,	$0BB13_201
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB13_201:
	LWI	r3,	r1,	196
	FPMUL	r3,	r3,	r29
	LWI	r4,	r1,	208
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r26,	r29
	LWI	r5,	r1,	212
	FPADD	r5,	r5,	r4
	LWI	r4,	r1,	192
	FPMUL	r4,	r4,	r29
	LWI	r6,	r1,	216
	bneid	r7,	$0BB13_203
	FPADD	r4,	r6,	r4
	FPNEG	r10,	r10
	FPNEG	r12,	r12
	FPNEG	r11,	r11
$0BB13_203:
	ORI	r6,	r0,	981668463
	FPMUL	r7,	r12,	r6
	FPADD	r7,	r5,	r7
	SWI	r7,	r1,	176
	FPMUL	r5,	r11,	r6
	FPADD	r5,	r4,	r5
	SWI	r5,	r1,	180
	LWI	r4,	r1,	268
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	272
	FPRSUB	r5,	r7,	r5
	FPMUL	r7,	r5,	r5
	FPMUL	r8,	r4,	r4
	FPADD	r7,	r8,	r7
	FPMUL	r6,	r10,	r6
	FPADD	r6,	r3,	r6
	SWI	r6,	r1,	184
	LWI	r3,	r1,	276
	FPRSUB	r3,	r6,	r3
	FPMUL	r6,	r3,	r3
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r24,	r7,	r6
	FPDIV	r6,	r7,	r24
	FPMUL	r9,	r5,	r6
	FPMUL	r5,	r12,	r9
	FPMUL	r4,	r4,	r6
	SWI	r4,	r1,	192
	FPMUL	r4,	r11,	r4
	FPADD	r4,	r4,	r5
	FPMUL	r3,	r3,	r6
	SWI	r3,	r1,	196
	FPMUL	r3,	r10,	r3
	FPADD	r5,	r4,	r3
	SWI	r5,	r1,	264
	ORI	r3,	r0,	0
	FPLE	r4,	r5,	r3
	FPUN	r5,	r5,	r3
	BITOR	r5,	r5,	r4
	bneid	r5,	$0BB13_205
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_205:
	SWI	r12,	r1,	260
	SWI	r11,	r1,	228
	bneid	r4,	$0BB13_256
	SWI	r10,	r1,	224
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	196
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	212
	FPDIV	r4,	r3,	r9
	SWI	r4,	r1,	216
	LWI	r4,	r1,	192
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	220
	ADD	r3,	r0,	r0
	SWI	r3,	r1,	208
	brid	$0BB13_207
	ADD	r10,	r3,	r0
$0BB13_253:
	bslli	r4,	r21,	2
	ADDI	r5,	r1,	48
	ADD	r4,	r5,	r4
	LWI	r10,	r4,	-4
$0BB13_207:
	brid	$0BB13_208
	ADD	r21,	r3,	r0
$0BB13_312:
	ADDI	r3,	r10,	1
	bslli	r4,	r21,	2
	ADDI	r5,	r1,	48
	SW	r3,	r5,	r4
	ADDI	r21,	r21,	1
$0BB13_208:
	bslli	r3,	r10,	3
	LWI	r4,	r1,	188
	ADD	r7,	r3,	r4
	LOAD	r3,	r7,	0
	LOAD	r6,	r7,	1
	LOAD	r4,	r7,	2
	ADDI	r8,	r7,	3
	LOAD	r10,	r8,	0
	LOAD	r5,	r8,	1
	LWI	r11,	r1,	176
	FPRSUB	r5,	r11,	r5
	FPRSUB	r6,	r11,	r6
	LOAD	r8,	r8,	2
	LWI	r11,	r1,	184
	FPRSUB	r8,	r11,	r8
	FPRSUB	r4,	r11,	r4
	LWI	r11,	r1,	212
	FPMUL	r4,	r4,	r11
	FPMUL	r8,	r8,	r11
	LWI	r12,	r1,	216
	FPMUL	r11,	r6,	r12
	FPMUL	r5,	r5,	r12
	LWI	r6,	r1,	180
	FPRSUB	r10,	r6,	r10
	FPRSUB	r3,	r6,	r3
	LWI	r12,	r1,	220
	FPMUL	r6,	r3,	r12
	FPMUL	r12,	r10,	r12
	FPMIN	r3,	r6,	r12
	FPMIN	r10,	r11,	r5
	FPMIN	r20,	r4,	r8
	FPMAX	r6,	r6,	r12
	FPMAX	r5,	r11,	r5
	FPMAX	r8,	r4,	r8
	FPMAX	r4,	r10,	r20
	FPMAX	r4,	r3,	r4
	FPMIN	r3,	r5,	r8
	FPMIN	r3,	r6,	r3
	FPGE	r5,	r4,	r3
	FPUN	r6,	r4,	r3
	BITOR	r6,	r6,	r5
	bneid	r6,	$0BB13_210
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_210:
	bneid	r5,	$0BB13_252
	NOP
	ORI	r5,	r0,	869711765
	FPLE	r6,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r6,	r5,	r6
	bneid	r6,	$0BB13_213
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_213:
	bneid	r5,	$0BB13_217
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r6,	r4,	r5
	bneid	r6,	$0BB13_216
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_216:
	beqid	r5,	$0BB13_217
	NOP
	FPLT	r3,	r24,	r4
	FPUN	r4,	r24,	r4
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB13_225
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_225:
	bneid	r3,	$0BB13_252
	NOP
	brid	$0BB13_226
	NOP
$0BB13_217:
	ORI	r4,	r0,	869711765
	FPLE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_219
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_219:
	bneid	r4,	$0BB13_252
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB13_222
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_222:
	beqid	r3,	$0BB13_252
	NOP
$0BB13_226:
	LOAD	r10,	r7,	7
	LOAD	r7,	r7,	6
	bltid	r7,	$0BB13_312
	NOP
	bleid	r7,	$0BB13_252
	ADD	r30,	r0,	r0
$0BB13_228:
	LOAD	r22,	r10,	0
	LOAD	r26,	r10,	1
	LOAD	r20,	r10,	2
	ADDI	r4,	r10,	3
	LOAD	r3,	r4,	0
	LOAD	r5,	r4,	1
	LOAD	r4,	r4,	2
	ADDI	r6,	r10,	6
	LOAD	r23,	r6,	0
	LOAD	r27,	r6,	1
	LOAD	r29,	r6,	2
	FPRSUB	r4,	r29,	r4
	FPRSUB	r12,	r27,	r5
	LWI	r8,	r1,	196
	FPMUL	r5,	r8,	r12
	FPMUL	r6,	r9,	r4
	FPRSUB	r31,	r5,	r6
	FPRSUB	r3,	r23,	r3
	FPMUL	r5,	r8,	r3
	LWI	r28,	r1,	192
	FPMUL	r6,	r28,	r4
	FPRSUB	r8,	r6,	r5
	LWI	r5,	r1,	180
	FPRSUB	r25,	r23,	r5
	LWI	r5,	r1,	176
	FPRSUB	r11,	r27,	r5
	FPMUL	r5,	r11,	r8
	FPMUL	r6,	r25,	r31
	FPADD	r6,	r6,	r5
	FPMUL	r5,	r9,	r3
	FPMUL	r28,	r28,	r12
	FPRSUB	r5,	r5,	r28
	FPRSUB	r23,	r23,	r22
	FPRSUB	r27,	r27,	r26
	LWI	r22,	r1,	184
	FPRSUB	r28,	r29,	r22
	FPMUL	r22,	r28,	r5
	FPADD	r22,	r6,	r22
	FPMUL	r6,	r27,	r8
	FPMUL	r8,	r23,	r31
	FPADD	r6,	r8,	r6
	FPRSUB	r29,	r29,	r20
	FPMUL	r5,	r29,	r5
	FPADD	r5,	r6,	r5
	ORI	r6,	r0,	1065353216
	FPDIV	r20,	r6,	r5
	FPMUL	r22,	r22,	r20
	ORI	r5,	r0,	0
	FPLT	r6,	r22,	r5
	bneid	r6,	$0BB13_230
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_230:
	bneid	r5,	$0BB13_251
	NOP
	ORI	r5,	r0,	1065353216
	FPGT	r6,	r22,	r5
	bneid	r6,	$0BB13_233
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_233:
	bneid	r5,	$0BB13_251
	NOP
	FPMUL	r5,	r28,	r27
	FPMUL	r6,	r11,	r29
	FPRSUB	r26,	r5,	r6
	FPMUL	r5,	r25,	r29
	FPMUL	r6,	r28,	r23
	FPRSUB	r28,	r5,	r6
	FPMUL	r5,	r9,	r28
	LWI	r6,	r1,	192
	FPMUL	r6,	r6,	r26
	FPADD	r5,	r6,	r5
	FPMUL	r6,	r11,	r23
	FPMUL	r8,	r25,	r27
	FPRSUB	r11,	r6,	r8
	LWI	r6,	r1,	196
	FPMUL	r6,	r6,	r11
	FPADD	r5,	r5,	r6
	FPMUL	r8,	r5,	r20
	ORI	r5,	r0,	0
	FPLT	r6,	r8,	r5
	bneid	r6,	$0BB13_236
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_236:
	bneid	r5,	$0BB13_251
	NOP
	FPADD	r5,	r8,	r22
	ORI	r6,	r0,	1065353216
	FPGT	r6,	r5,	r6
	bneid	r6,	$0BB13_239
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_239:
	bneid	r5,	$0BB13_251
	NOP
	FPMUL	r5,	r12,	r28
	FPMUL	r3,	r3,	r26
	FPADD	r3,	r3,	r5
	FPMUL	r4,	r4,	r11
	FPADD	r3,	r3,	r4
	FPMUL	r3,	r3,	r20
	ORI	r4,	r0,	869711765
	FPGT	r5,	r3,	r4
	ADDI	r11,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r5,	$0BB13_242
	ADD	r8,	r4,	r0
	ADD	r8,	r11,	r0
$0BB13_242:
	ORI	r5,	r0,	0
	FPGT	r6,	r3,	r5
	bneid	r6,	$0BB13_244
	ADD	r5,	r4,	r0
	ADD	r5,	r11,	r0
$0BB13_244:
	BITAND	r5,	r5,	r8
	FPLT	r8,	r3,	r24
	bneid	r8,	$0BB13_246
	ADD	r6,	r4,	r0
	ADD	r6,	r11,	r0
$0BB13_246:
	BITAND	r5,	r5,	r6
	bneid	r5,	$0BB13_248
	NOP
	ADD	r3,	r24,	r0
$0BB13_248:
	bneid	r5,	$0BB13_250
	NOP
	LWI	r4,	r1,	208
$0BB13_250:
	SWI	r4,	r1,	208
	ADD	r24,	r3,	r0
$0BB13_251:
	ADDI	r30,	r30,	1
	CMP	r3,	r7,	r30
	bltid	r3,	$0BB13_228
	ADDI	r10,	r10,	11
$0BB13_252:
	ADDI	r3,	r0,	-1
	ADD	r3,	r21,	r3
	bgeid	r3,	$0BB13_253
	NOP
	ORI	r3,	r0,	0
	LWI	r4,	r1,	208
	ANDI	r4,	r4,	255
	bneid	r4,	$0BB13_256
	NOP
	ORI	r3,	r0,	0
	LWI	r4,	r1,	264
	FPADD	r3,	r4,	r3
$0BB13_256:
	LWI	r4,	r1,	204
	ADDI	r4,	r4,	10
	LOAD	r4,	r4,	0
	MULI	r4,	r4,	25
	LWI	r5,	r1,	280
	ADD	r4,	r4,	r5
	LOAD	r21,	r4,	4
	FPMUL	r8,	r3,	r21
	LOAD	r31,	r4,	5
	FPMUL	r7,	r3,	r31
	LOAD	r10,	r4,	6
	FPMUL	r30,	r3,	r10
	LWI	r23,	r1,	224
	LWI	r12,	r1,	228
	LWI	r20,	r1,	260
$0BB13_257:
	LWI	r25,	r1,	240
$0BB13_258:
	RAND	r3
	RAND	r4
	FPADD	r4,	r4,	r4
	ORI	r5,	r0,	-1082130432
	FPADD	r4,	r4,	r5
	FPADD	r3,	r3,	r3
	FPADD	r5,	r3,	r5
	FPMUL	r6,	r5,	r5
	FPMUL	r9,	r4,	r4
	FPADD	r3,	r6,	r9
	ORI	r11,	r0,	1065353216
	FPGE	r11,	r3,	r11
	bneid	r11,	$0BB13_260
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_260:
	bneid	r3,	$0BB13_258
	NOP
	ORI	r3,	r0,	0
	FPGE	r11,	r12,	r3
	FPUN	r3,	r12,	r3
	BITOR	r3,	r3,	r11
	bneid	r3,	$0BB13_263
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB13_263:
	ORI	r3,	r0,	1065353216
	FPRSUB	r6,	r6,	r3
	FPRSUB	r6,	r9,	r6
	FPINVSQRT	r9,	r6
	bneid	r11,	$0BB13_265
	ADD	r6,	r12,	r0
	FPNEG	r6,	r12
$0BB13_265:
	ADD	r26,	r12,	r0
	ORI	r11,	r0,	0
	FPGE	r12,	r20,	r11
	FPUN	r11,	r20,	r11
	BITOR	r12,	r11,	r12
	bneid	r12,	$0BB13_267
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB13_267:
	bneid	r11,	$0BB13_269
	ADD	r12,	r20,	r0
	FPNEG	r12,	r20
$0BB13_269:
	ADD	r29,	r20,	r0
	ORI	r11,	r0,	0
	FPGE	r20,	r23,	r11
	FPUN	r11,	r23,	r11
	BITOR	r11,	r11,	r20
	bneid	r11,	$0BB13_271
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB13_271:
	bneid	r20,	$0BB13_273
	ADD	r11,	r23,	r0
	FPNEG	r11,	r23
$0BB13_273:
	FPGE	r20,	r6,	r12
	FPUN	r22,	r6,	r12
	BITOR	r20,	r22,	r20
	ADDI	r22,	r0,	1
	LWI	r24,	r1,	244
	LWI	r27,	r1,	252
	bneid	r20,	$0BB13_275
	LWI	r28,	r1,	256
	ADDI	r22,	r0,	0
$0BB13_275:
	FPDIV	r3,	r3,	r9
	ORI	r20,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r22,	$0BB13_279
	NOP
	FPLT	r6,	r6,	r11
	bneid	r6,	$0BB13_278
	ADDI	r22,	r0,	1
	ADDI	r22,	r0,	0
$0BB13_278:
	bneid	r22,	$0BB13_283
	ADD	r6,	r9,	r0
$0BB13_279:
	FPLT	r6,	r12,	r11
	bneid	r6,	$0BB13_281
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB13_281:
	ORI	r6,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r11,	$0BB13_283
	ADD	r20,	r9,	r0
	ORI	r6,	r0,	0
	ORI	r9,	r0,	1065353216
	ADD	r20,	r6,	r0
$0BB13_283:
	FPMUL	r11,	r23,	r6
	FPMUL	r12,	r29,	r9
	FPRSUB	r12,	r11,	r12
	FPMUL	r6,	r26,	r6
	FPMUL	r11,	r29,	r20
	FPRSUB	r6,	r11,	r6
	FPMUL	r9,	r26,	r9
	FPMUL	r11,	r23,	r20
	FPRSUB	r20,	r9,	r11
	FPMUL	r9,	r23,	r20
	FPMUL	r11,	r29,	r6
	FPRSUB	r9,	r9,	r11
	FPMUL	r11,	r26,	r6
	FPMUL	r22,	r23,	r12
	FPRSUB	r11,	r11,	r22
	FPMUL	r11,	r11,	r4
	FPMUL	r22,	r20,	r5
	FPADD	r11,	r22,	r11
	FPMUL	r9,	r9,	r4
	FPMUL	r22,	r12,	r5
	FPADD	r9,	r22,	r9
	FPMUL	r22,	r26,	r3
	FPADD	r9,	r9,	r22
	FPMUL	r22,	r29,	r3
	FPADD	r11,	r11,	r22
	FPMUL	r12,	r29,	r12
	FPMUL	r20,	r26,	r20
	FPMUL	r8,	r8,	r28
	FPMUL	r7,	r7,	r27
	FPMUL	r22,	r30,	r24
	ADD	r26,	r23,	r0
	LWI	r23,	r1,	232
	FPADD	r23,	r23,	r22
	SWI	r23,	r1,	232
	LWI	r22,	r1,	236
	FPADD	r22,	r22,	r7
	SWI	r22,	r1,	236
	FPADD	r25,	r25,	r8
	FPMUL	r7,	r11,	r11
	FPMUL	r8,	r9,	r9
	FPADD	r7,	r8,	r7
	FPRSUB	r8,	r12,	r20
	FPMUL	r4,	r8,	r4
	FPMUL	r5,	r6,	r5
	FPADD	r4,	r5,	r4
	FPMUL	r3,	r26,	r3
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r7,	r4
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	LWI	r6,	r1,	200
	ANDI	r6,	r6,	255
	bneid	r6,	$0BB13_284
	NOP
$0BB13_285:
	LWI	r4,	r1,	288
	ADDI	r4,	r4,	1
	SWI	r4,	r1,	288
	LWI	r3,	r1,	292
	CMP	r3,	r3,	r4
	LWI	r7,	r1,	344
	bltid	r3,	$0BB13_6
	LWI	r8,	r1,	348
	brid	$0BB13_286
	NOP
$0BB13_27:
	brid	$0BB13_286
	LWI	r25,	r1,	240
$0BB13_2:
	LWI	r6,	r1,	368
	DIV	r5,	r6,	r4
	FPCONV	r3,	r5
	MUL	r5,	r5,	r6
	SWI	r5,	r1,	360
	RSUB	r4,	r5,	r4
	SWI	r4,	r1,	364
	FPCONV	r5,	r4
	ORI	r4,	r0,	1056964608
	ADDI	r6,	r0,	1
	CMP	r6,	r6,	r9
	ORI	r25,	r0,	0
	bltid	r6,	$0BB13_3
	NOP
	LWI	r6,	r1,	388
	FPRSUB	r6,	r6,	r3
	LWI	r3,	r1,	396
	FPRSUB	r3,	r3,	r5
	FPADD	r3,	r3,	r4
	FPADD	r4,	r6,	r4
	FPADD	r4,	r4,	r4
	LWI	r5,	r1,	384
	FPDIV	r7,	r4,	r5
	SWI	r7,	r1,	344
	FPADD	r3,	r3,	r3
	LWI	r4,	r1,	392
	FPDIV	r8,	r3,	r4
	SWI	r8,	r1,	348
	ADDI	r3,	r0,	1
	CMP	r3,	r3,	r9
	ORI	r25,	r0,	0
	ADD	r4,	r0,	r0
	bneid	r3,	$0BB13_5
	SWI	r4,	r1,	288
	LWI	r3,	r1,	320
	FPMUL	r3,	r3,	r8
	LWI	r4,	r1,	308
	FPADD	r3,	r4,	r3
	LWI	r4,	r1,	332
	FPMUL	r4,	r4,	r7
	FPADD	r3,	r3,	r4
	LWI	r4,	r1,	324
	FPMUL	r4,	r4,	r8
	LWI	r5,	r1,	312
	FPADD	r4,	r5,	r4
	LWI	r5,	r1,	336
	FPMUL	r5,	r5,	r7
	FPADD	r5,	r4,	r5
	FPMUL	r4,	r5,	r5
	FPMUL	r6,	r3,	r3
	FPADD	r6,	r6,	r4
	LWI	r4,	r1,	340
	FPMUL	r4,	r4,	r7
	LWI	r7,	r1,	328
	FPMUL	r7,	r7,	r8
	LWI	r8,	r1,	316
	FPADD	r7,	r8,	r7
	FPADD	r4,	r7,	r4
	FPMUL	r7,	r4,	r4
	FPADD	r6,	r6,	r7
	FPINVSQRT	r6,	r6
	ORI	r22,	r0,	1065353216
	FPDIV	r6,	r22,	r6
	FPDIV	r6,	r22,	r6
	FPMUL	r4,	r4,	r6
	SWI	r4,	r1,	196
	FPMUL	r29,	r5,	r6
	FPMUL	r3,	r3,	r6
	SWI	r3,	r1,	192
	ADD	r4,	r0,	r0
	ORI	r3,	r0,	0
	SWI	r3,	r1,	232
	LWI	r5,	r1,	304
	SWI	r5,	r1,	208
	LWI	r5,	r1,	300
	SWI	r5,	r1,	212
	LWI	r5,	r1,	296
	SWI	r5,	r1,	216
	ADD	r23,	r22,	r0
	SWI	r22,	r1,	244
	SWI	r3,	r1,	236
	brid	$0BB13_26
	SWI	r3,	r1,	240
$0BB13_25:
	SWI	r25,	r1,	240
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r22,	r22,	r24
	FPMUL	r23,	r23,	r30
	FPMUL	r28,	r28,	r26
	SWI	r28,	r1,	244
	FPMUL	r5,	r9,	r4
	SWI	r5,	r1,	192
	FPMUL	r29,	r10,	r4
	FPMUL	r3,	r3,	r4
	SWI	r3,	r1,	196
	LWI	r3,	r1,	184
	SWI	r3,	r1,	208
	LWI	r3,	r1,	176
	SWI	r3,	r1,	212
	LWI	r3,	r1,	180
	SWI	r3,	r1,	216
	LWI	r4,	r1,	252
$0BB13_26:
	SWI	r23,	r1,	256
	SWI	r22,	r1,	248
	LWI	r3,	r1,	284
	CMP	r3,	r3,	r4
	bgeid	r3,	$0BB13_27
	NOP
	ADDI	r4,	r4,	1
	SWI	r4,	r1,	252
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	196
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	220
	FPDIV	r4,	r3,	r29
	SWI	r4,	r1,	224
	LWI	r4,	r1,	192
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	228
	ORI	r9,	r0,	1203982336
	ADD	r3,	r0,	r0
	SWI	r3,	r1,	200
	SWI	r3,	r1,	204
	brid	$0BB13_29
	ADD	r26,	r3,	r0
$0BB13_77:
	bslli	r4,	r24,	2
	ADDI	r5,	r1,	48
	ADD	r4,	r5,	r4
	LWI	r26,	r4,	-4
$0BB13_29:
	brid	$0BB13_30
	ADD	r24,	r3,	r0
$0BB13_309:
	ADDI	r3,	r26,	1
	bslli	r4,	r24,	2
	ADDI	r5,	r1,	48
	SW	r3,	r5,	r4
	ADDI	r24,	r24,	1
$0BB13_30:
	bslli	r3,	r26,	3
	LWI	r4,	r1,	188
	ADD	r7,	r3,	r4
	LOAD	r3,	r7,	0
	LOAD	r5,	r7,	1
	LOAD	r4,	r7,	2
	ADDI	r8,	r7,	3
	LOAD	r11,	r8,	0
	LOAD	r10,	r8,	1
	LWI	r6,	r1,	212
	FPRSUB	r20,	r6,	r10
	FPRSUB	r5,	r6,	r5
	LOAD	r8,	r8,	2
	LWI	r6,	r1,	208
	FPRSUB	r10,	r6,	r8
	FPRSUB	r4,	r6,	r4
	LWI	r6,	r1,	220
	FPMUL	r8,	r4,	r6
	FPMUL	r10,	r10,	r6
	LWI	r4,	r1,	224
	FPMUL	r12,	r5,	r4
	FPMUL	r4,	r20,	r4
	LWI	r5,	r1,	216
	FPRSUB	r11,	r5,	r11
	FPRSUB	r3,	r5,	r3
	LWI	r6,	r1,	228
	FPMUL	r5,	r3,	r6
	FPMUL	r20,	r11,	r6
	FPMIN	r3,	r5,	r20
	FPMIN	r11,	r12,	r4
	FPMIN	r21,	r8,	r10
	FPMAX	r5,	r5,	r20
	FPMAX	r4,	r12,	r4
	FPMAX	r10,	r8,	r10
	FPMAX	r8,	r11,	r21
	FPMAX	r8,	r3,	r8
	FPMIN	r3,	r4,	r10
	FPMIN	r3,	r5,	r3
	FPGE	r4,	r8,	r3
	FPUN	r5,	r8,	r3
	BITOR	r5,	r5,	r4
	bneid	r5,	$0BB13_32
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_32:
	bneid	r4,	$0BB13_76
	NOP
	ORI	r4,	r0,	869711765
	FPLE	r5,	r8,	r4
	FPUN	r4,	r8,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_35
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_35:
	bneid	r4,	$0BB13_39
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r5,	r8,	r4
	bneid	r5,	$0BB13_38
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_38:
	beqid	r4,	$0BB13_39
	NOP
	FPLT	r3,	r9,	r8
	FPUN	r4,	r9,	r8
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB13_47
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_47:
	bneid	r3,	$0BB13_76
	NOP
	brid	$0BB13_48
	NOP
$0BB13_39:
	ORI	r4,	r0,	869711765
	FPLE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_41
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_41:
	bneid	r4,	$0BB13_76
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB13_44
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_44:
	beqid	r3,	$0BB13_76
	NOP
$0BB13_48:
	LOAD	r26,	r7,	7
	LOAD	r8,	r7,	6
	bltid	r8,	$0BB13_309
	NOP
	bleid	r8,	$0BB13_76
	ADD	r7,	r0,	r0
$0BB13_50:
	LOAD	r11,	r26,	0
	LOAD	r12,	r26,	1
	LOAD	r3,	r26,	2
	ADDI	r5,	r26,	3
	LOAD	r10,	r5,	0
	LOAD	r4,	r5,	1
	LOAD	r5,	r5,	2
	ADDI	r20,	r26,	6
	LOAD	r21,	r20,	0
	LOAD	r22,	r20,	1
	LOAD	r23,	r20,	2
	FPRSUB	r27,	r23,	r5
	FPRSUB	r30,	r22,	r4
	LWI	r6,	r1,	196
	FPMUL	r4,	r6,	r30
	FPMUL	r5,	r29,	r27
	FPRSUB	r31,	r4,	r5
	FPRSUB	r10,	r21,	r10
	FPMUL	r4,	r6,	r10
	LWI	r6,	r1,	192
	FPMUL	r5,	r6,	r27
	FPRSUB	r28,	r5,	r4
	LWI	r4,	r1,	216
	FPRSUB	r20,	r21,	r4
	LWI	r4,	r1,	212
	FPRSUB	r25,	r22,	r4
	FPMUL	r4,	r25,	r28
	FPMUL	r5,	r20,	r31
	FPADD	r5,	r5,	r4
	FPMUL	r4,	r29,	r10
	FPMUL	r6,	r6,	r30
	FPRSUB	r4,	r4,	r6
	FPRSUB	r21,	r21,	r11
	FPRSUB	r12,	r22,	r12
	LWI	r6,	r1,	208
	FPRSUB	r22,	r23,	r6
	FPMUL	r6,	r22,	r4
	FPADD	r11,	r5,	r6
	FPMUL	r5,	r12,	r28
	FPMUL	r6,	r21,	r31
	FPADD	r5,	r6,	r5
	FPRSUB	r31,	r23,	r3
	FPMUL	r3,	r31,	r4
	FPADD	r3,	r5,	r3
	ORI	r4,	r0,	1065353216
	FPDIV	r3,	r4,	r3
	FPMUL	r11,	r11,	r3
	ORI	r4,	r0,	0
	FPLT	r5,	r11,	r4
	bneid	r5,	$0BB13_52
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_52:
	bneid	r4,	$0BB13_75
	NOP
	ORI	r4,	r0,	1065353216
	FPGT	r5,	r11,	r4
	bneid	r5,	$0BB13_55
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_55:
	bneid	r4,	$0BB13_75
	NOP
	FPMUL	r4,	r22,	r12
	FPMUL	r5,	r25,	r31
	FPRSUB	r23,	r4,	r5
	FPMUL	r4,	r20,	r31
	FPMUL	r5,	r22,	r21
	FPRSUB	r22,	r4,	r5
	FPMUL	r4,	r29,	r22
	LWI	r5,	r1,	192
	FPMUL	r5,	r5,	r23
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r25,	r21
	FPMUL	r6,	r20,	r12
	FPRSUB	r12,	r5,	r6
	LWI	r5,	r1,	196
	FPMUL	r5,	r5,	r12
	FPADD	r4,	r4,	r5
	FPMUL	r20,	r4,	r3
	ORI	r4,	r0,	0
	FPLT	r5,	r20,	r4
	bneid	r5,	$0BB13_58
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_58:
	bneid	r4,	$0BB13_75
	NOP
	FPADD	r4,	r20,	r11
	ORI	r5,	r0,	1065353216
	FPGT	r5,	r4,	r5
	bneid	r5,	$0BB13_61
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_61:
	bneid	r4,	$0BB13_75
	NOP
	FPMUL	r4,	r30,	r22
	FPMUL	r5,	r10,	r23
	FPADD	r4,	r5,	r4
	FPMUL	r5,	r27,	r12
	FPADD	r4,	r4,	r5
	FPMUL	r3,	r4,	r3
	ORI	r4,	r0,	869711765
	FPGT	r4,	r3,	r4
	ADDI	r11,	r0,	0
	ADDI	r10,	r0,	1
	bneid	r4,	$0BB13_64
	ADD	r12,	r10,	r0
	ADD	r12,	r11,	r0
$0BB13_64:
	ORI	r4,	r0,	0
	FPGT	r5,	r3,	r4
	bneid	r5,	$0BB13_66
	ADD	r4,	r10,	r0
	ADD	r4,	r11,	r0
$0BB13_66:
	BITAND	r4,	r4,	r12
	FPLT	r12,	r3,	r9
	bneid	r12,	$0BB13_68
	ADD	r5,	r10,	r0
	ADD	r5,	r11,	r0
$0BB13_68:
	BITAND	r11,	r4,	r5
	bneid	r11,	$0BB13_70
	NOP
	ADD	r3,	r9,	r0
$0BB13_70:
	bneid	r11,	$0BB13_72
	ADD	r9,	r26,	r0
	LWI	r9,	r1,	204
$0BB13_72:
	bneid	r11,	$0BB13_74
	NOP
	LWI	r10,	r1,	200
$0BB13_74:
	SWI	r10,	r1,	200
	SWI	r9,	r1,	204
	ADD	r9,	r3,	r0
$0BB13_75:
	ADDI	r7,	r7,	1
	CMP	r3,	r8,	r7
	bltid	r3,	$0BB13_50
	ADDI	r26,	r26,	11
$0BB13_76:
	ADDI	r3,	r0,	-1
	ADD	r3,	r24,	r3
	bgeid	r3,	$0BB13_77
	NOP
	ORI	r24,	r0,	1065353216
	ORI	r8,	r0,	1057988018
	ORI	r27,	r0,	1060806590
	ORI	r7,	r0,	1065151889
	LWI	r3,	r1,	200
	ANDI	r3,	r3,	255
	ADD	r30,	r24,	r0
	beqid	r3,	$0BB13_137
	ADD	r26,	r24,	r0
	LWI	r6,	r1,	204
	LOAD	r3,	r6,	0
	LOAD	r4,	r6,	1
	LOAD	r7,	r6,	2
	ADDI	r5,	r6,	3
	LOAD	r11,	r5,	0
	LOAD	r10,	r5,	1
	LOAD	r5,	r5,	2
	ADDI	r20,	r6,	6
	LOAD	r12,	r20,	0
	LOAD	r21,	r20,	1
	FPRSUB	r8,	r21,	r4
	FPRSUB	r10,	r21,	r10
	LOAD	r4,	r20,	2
	FPRSUB	r20,	r4,	r5
	FPRSUB	r21,	r4,	r7
	FPMUL	r4,	r21,	r10
	FPMUL	r5,	r8,	r20
	FPRSUB	r7,	r4,	r5
	FPRSUB	r4,	r12,	r11
	FPRSUB	r5,	r12,	r3
	FPMUL	r3,	r5,	r20
	FPMUL	r6,	r21,	r4
	FPRSUB	r3,	r3,	r6
	FPMUL	r11,	r3,	r3
	FPMUL	r6,	r7,	r7
	FPADD	r11,	r6,	r11
	FPMUL	r4,	r8,	r4
	FPMUL	r5,	r5,	r10
	FPRSUB	r8,	r4,	r5
	FPMUL	r4,	r8,	r8
	FPADD	r4,	r11,	r4
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	FPDIV	r4,	r5,	r4
	FPDIV	r4,	r5,	r4
	FPMUL	r11,	r7,	r4
	FPMUL	r12,	r3,	r4
	FPMUL	r3,	r12,	r29
	LWI	r5,	r1,	192
	FPMUL	r5,	r11,	r5
	FPADD	r3,	r5,	r3
	FPMUL	r10,	r8,	r4
	LWI	r4,	r1,	196
	FPMUL	r4,	r10,	r4
	FPADD	r3,	r3,	r4
	ORI	r4,	r0,	0
	FPLE	r5,	r3,	r4
	FPUN	r3,	r3,	r4
	BITOR	r3,	r3,	r5
	bneid	r3,	$0BB13_81
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB13_81:
	LWI	r3,	r1,	196
	FPMUL	r3,	r3,	r9
	LWI	r4,	r1,	208
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r29,	r9
	LWI	r5,	r1,	212
	FPADD	r5,	r5,	r4
	LWI	r4,	r1,	192
	FPMUL	r4,	r4,	r9
	LWI	r6,	r1,	216
	bneid	r7,	$0BB13_83
	FPADD	r4,	r6,	r4
	FPNEG	r10,	r10
	FPNEG	r12,	r12
	FPNEG	r11,	r11
$0BB13_83:
	ORI	r6,	r0,	981668463
	FPMUL	r7,	r12,	r6
	FPADD	r7,	r5,	r7
	SWI	r7,	r1,	176
	FPMUL	r5,	r11,	r6
	FPADD	r5,	r4,	r5
	SWI	r5,	r1,	180
	LWI	r4,	r1,	268
	FPRSUB	r4,	r5,	r4
	LWI	r5,	r1,	272
	FPRSUB	r5,	r7,	r5
	FPMUL	r7,	r5,	r5
	FPMUL	r8,	r4,	r4
	FPADD	r7,	r8,	r7
	FPMUL	r6,	r10,	r6
	FPADD	r6,	r3,	r6
	SWI	r6,	r1,	184
	LWI	r3,	r1,	276
	FPRSUB	r3,	r6,	r3
	FPMUL	r6,	r3,	r3
	FPADD	r6,	r7,	r6
	FPINVSQRT	r6,	r6
	ORI	r7,	r0,	1065353216
	FPDIV	r24,	r7,	r6
	FPDIV	r7,	r7,	r24
	FPMUL	r9,	r5,	r7
	FPMUL	r5,	r12,	r9
	FPMUL	r4,	r4,	r7
	SWI	r4,	r1,	192
	FPMUL	r4,	r11,	r4
	FPADD	r4,	r4,	r5
	FPMUL	r3,	r3,	r7
	SWI	r3,	r1,	196
	FPMUL	r3,	r10,	r3
	FPADD	r5,	r4,	r3
	SWI	r5,	r1,	264
	ORI	r3,	r0,	0
	FPLE	r4,	r5,	r3
	FPUN	r5,	r5,	r3
	BITOR	r5,	r5,	r4
	bneid	r5,	$0BB13_85
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_85:
	SWI	r12,	r1,	260
	SWI	r11,	r1,	228
	bneid	r4,	$0BB13_136
	SWI	r10,	r1,	224
	ORI	r3,	r0,	1065353216
	LWI	r4,	r1,	196
	FPDIV	r4,	r3,	r4
	SWI	r4,	r1,	212
	FPDIV	r4,	r3,	r9
	SWI	r4,	r1,	216
	LWI	r4,	r1,	192
	FPDIV	r3,	r3,	r4
	SWI	r3,	r1,	220
	ADD	r3,	r0,	r0
	SWI	r3,	r1,	208
	brid	$0BB13_87
	ADD	r31,	r3,	r0
$0BB13_133:
	bslli	r4,	r30,	2
	ADDI	r5,	r1,	48
	ADD	r4,	r5,	r4
	LWI	r31,	r4,	-4
$0BB13_87:
	brid	$0BB13_88
	ADD	r30,	r3,	r0
$0BB13_310:
	ADDI	r3,	r31,	1
	bslli	r4,	r30,	2
	ADDI	r5,	r1,	48
	SW	r3,	r5,	r4
	ADDI	r30,	r30,	1
$0BB13_88:
	bslli	r3,	r31,	3
	LWI	r4,	r1,	188
	ADD	r7,	r3,	r4
	LOAD	r3,	r7,	0
	LOAD	r5,	r7,	1
	LOAD	r4,	r7,	2
	ADDI	r8,	r7,	3
	LOAD	r10,	r8,	0
	LOAD	r11,	r8,	1
	LWI	r6,	r1,	176
	FPRSUB	r12,	r6,	r11
	FPRSUB	r5,	r6,	r5
	LOAD	r8,	r8,	2
	LWI	r6,	r1,	184
	FPRSUB	r8,	r6,	r8
	FPRSUB	r4,	r6,	r4
	LWI	r6,	r1,	212
	FPMUL	r4,	r4,	r6
	FPMUL	r8,	r8,	r6
	LWI	r6,	r1,	216
	FPMUL	r11,	r5,	r6
	FPMUL	r12,	r12,	r6
	LWI	r5,	r1,	180
	FPRSUB	r10,	r5,	r10
	FPRSUB	r3,	r5,	r3
	LWI	r6,	r1,	220
	FPMUL	r5,	r3,	r6
	FPMUL	r20,	r10,	r6
	FPMIN	r3,	r5,	r20
	FPMIN	r10,	r11,	r12
	FPMIN	r21,	r4,	r8
	FPMAX	r5,	r5,	r20
	FPMAX	r11,	r11,	r12
	FPMAX	r8,	r4,	r8
	FPMAX	r4,	r10,	r21
	FPMAX	r4,	r3,	r4
	FPMIN	r3,	r11,	r8
	FPMIN	r3,	r5,	r3
	FPGE	r5,	r4,	r3
	FPUN	r8,	r4,	r3
	BITOR	r8,	r8,	r5
	bneid	r8,	$0BB13_90
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_90:
	bneid	r5,	$0BB13_132
	NOP
	ORI	r5,	r0,	869711765
	FPLE	r8,	r4,	r5
	FPUN	r5,	r4,	r5
	BITOR	r8,	r5,	r8
	bneid	r8,	$0BB13_93
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_93:
	bneid	r5,	$0BB13_97
	NOP
	ORI	r5,	r0,	1259902592
	FPLT	r8,	r4,	r5
	bneid	r8,	$0BB13_96
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_96:
	beqid	r5,	$0BB13_97
	NOP
	FPLT	r3,	r24,	r4
	FPUN	r4,	r24,	r4
	BITOR	r4,	r4,	r3
	bneid	r4,	$0BB13_105
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_105:
	bneid	r3,	$0BB13_132
	NOP
	brid	$0BB13_106
	NOP
$0BB13_97:
	ORI	r4,	r0,	869711765
	FPLE	r5,	r3,	r4
	FPUN	r4,	r3,	r4
	BITOR	r5,	r4,	r5
	bneid	r5,	$0BB13_99
	ADDI	r4,	r0,	1
	ADDI	r4,	r0,	0
$0BB13_99:
	bneid	r4,	$0BB13_132
	NOP
	ORI	r4,	r0,	1259902592
	FPLT	r4,	r3,	r4
	bneid	r4,	$0BB13_102
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_102:
	beqid	r3,	$0BB13_132
	NOP
$0BB13_106:
	LOAD	r31,	r7,	7
	LOAD	r7,	r7,	6
	bltid	r7,	$0BB13_310
	NOP
	bleid	r7,	$0BB13_132
	ADD	r8,	r0,	r0
$0BB13_108:
	LOAD	r11,	r31,	0
	LOAD	r12,	r31,	1
	LOAD	r3,	r31,	2
	ADDI	r4,	r31,	3
	LOAD	r10,	r4,	0
	LOAD	r5,	r4,	1
	LOAD	r4,	r4,	2
	ADDI	r20,	r31,	6
	LOAD	r21,	r20,	0
	LOAD	r23,	r20,	1
	LOAD	r26,	r20,	2
	FPRSUB	r4,	r26,	r4
	FPRSUB	r22,	r23,	r5
	LWI	r6,	r1,	196
	FPMUL	r5,	r6,	r22
	FPMUL	r20,	r9,	r4
	FPRSUB	r28,	r5,	r20
	FPRSUB	r10,	r21,	r10
	FPMUL	r5,	r6,	r10
	LWI	r6,	r1,	192
	FPMUL	r20,	r6,	r4
	FPRSUB	r29,	r20,	r5
	LWI	r5,	r1,	180
	FPRSUB	r20,	r21,	r5
	LWI	r5,	r1,	176
	FPRSUB	r25,	r23,	r5
	FPMUL	r5,	r25,	r29
	FPMUL	r27,	r20,	r28
	FPADD	r5,	r27,	r5
	FPMUL	r27,	r9,	r10
	FPMUL	r6,	r6,	r22
	FPRSUB	r27,	r27,	r6
	FPRSUB	r21,	r21,	r11
	FPRSUB	r12,	r23,	r12
	LWI	r6,	r1,	184
	FPRSUB	r23,	r26,	r6
	FPMUL	r6,	r23,	r27
	FPADD	r11,	r5,	r6
	FPMUL	r5,	r12,	r29
	FPMUL	r6,	r21,	r28
	FPADD	r5,	r6,	r5
	FPRSUB	r28,	r26,	r3
	FPMUL	r3,	r28,	r27
	FPADD	r3,	r5,	r3
	ORI	r5,	r0,	1065353216
	FPDIV	r3,	r5,	r3
	FPMUL	r11,	r11,	r3
	ORI	r5,	r0,	0
	FPLT	r26,	r11,	r5
	bneid	r26,	$0BB13_110
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_110:
	bneid	r5,	$0BB13_131
	NOP
	ORI	r5,	r0,	1065353216
	FPGT	r26,	r11,	r5
	bneid	r26,	$0BB13_113
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_113:
	bneid	r5,	$0BB13_131
	NOP
	FPMUL	r5,	r23,	r12
	FPMUL	r6,	r25,	r28
	FPRSUB	r26,	r5,	r6
	FPMUL	r5,	r20,	r28
	FPMUL	r6,	r23,	r21
	FPRSUB	r23,	r5,	r6
	FPMUL	r5,	r9,	r23
	LWI	r6,	r1,	192
	FPMUL	r6,	r6,	r26
	FPADD	r5,	r6,	r5
	FPMUL	r21,	r25,	r21
	FPMUL	r6,	r20,	r12
	FPRSUB	r12,	r21,	r6
	LWI	r6,	r1,	196
	FPMUL	r6,	r6,	r12
	FPADD	r5,	r5,	r6
	FPMUL	r20,	r5,	r3
	ORI	r5,	r0,	0
	FPLT	r21,	r20,	r5
	bneid	r21,	$0BB13_116
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_116:
	bneid	r5,	$0BB13_131
	NOP
	FPADD	r5,	r20,	r11
	ORI	r6,	r0,	1065353216
	FPGT	r11,	r5,	r6
	bneid	r11,	$0BB13_119
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_119:
	bneid	r5,	$0BB13_131
	NOP
	FPMUL	r5,	r22,	r23
	FPMUL	r6,	r10,	r26
	FPADD	r5,	r6,	r5
	FPMUL	r4,	r4,	r12
	FPADD	r4,	r5,	r4
	FPMUL	r3,	r4,	r3
	ORI	r4,	r0,	869711765
	FPGT	r5,	r3,	r4
	ADDI	r10,	r0,	0
	ADDI	r4,	r0,	1
	bneid	r5,	$0BB13_122
	ADD	r11,	r4,	r0
	ADD	r11,	r10,	r0
$0BB13_122:
	ORI	r5,	r0,	0
	FPGT	r12,	r3,	r5
	bneid	r12,	$0BB13_124
	ADD	r5,	r4,	r0
	ADD	r5,	r10,	r0
$0BB13_124:
	BITAND	r5,	r5,	r11
	FPLT	r12,	r3,	r24
	bneid	r12,	$0BB13_126
	ADD	r11,	r4,	r0
	ADD	r11,	r10,	r0
$0BB13_126:
	BITAND	r5,	r5,	r11
	bneid	r5,	$0BB13_128
	NOP
	ADD	r3,	r24,	r0
$0BB13_128:
	bneid	r5,	$0BB13_130
	NOP
	LWI	r4,	r1,	208
$0BB13_130:
	SWI	r4,	r1,	208
	ADD	r24,	r3,	r0
$0BB13_131:
	ADDI	r8,	r8,	1
	CMP	r3,	r7,	r8
	bltid	r3,	$0BB13_108
	ADDI	r31,	r31,	11
$0BB13_132:
	ADDI	r3,	r0,	-1
	ADD	r3,	r30,	r3
	bgeid	r3,	$0BB13_133
	NOP
	ORI	r3,	r0,	0
	LWI	r4,	r1,	208
	ANDI	r4,	r4,	255
	bneid	r4,	$0BB13_136
	NOP
	ORI	r3,	r0,	0
	LWI	r4,	r1,	264
	FPADD	r3,	r4,	r3
$0BB13_136:
	LWI	r4,	r1,	204
	ADDI	r4,	r4,	10
	LOAD	r4,	r4,	0
	MULI	r4,	r4,	25
	LWI	r5,	r1,	280
	ADD	r4,	r4,	r5
	LOAD	r26,	r4,	4
	FPMUL	r8,	r3,	r26
	LOAD	r30,	r4,	5
	FPMUL	r27,	r3,	r30
	LOAD	r24,	r4,	6
	FPMUL	r7,	r3,	r24
	LWI	r21,	r1,	228
	LWI	r12,	r1,	260
$0BB13_137:
	LWI	r25,	r1,	240
$0BB13_138:
	RAND	r3
	RAND	r4
	FPADD	r4,	r4,	r4
	ORI	r5,	r0,	-1082130432
	FPADD	r4,	r4,	r5
	FPADD	r3,	r3,	r3
	FPADD	r5,	r3,	r5
	FPMUL	r6,	r5,	r5
	FPMUL	r9,	r4,	r4
	FPADD	r3,	r6,	r9
	ORI	r10,	r0,	1065353216
	FPGE	r10,	r3,	r10
	bneid	r10,	$0BB13_140
	ADDI	r3,	r0,	1
	ADDI	r3,	r0,	0
$0BB13_140:
	bneid	r3,	$0BB13_138
	NOP
	ORI	r3,	r0,	0
	FPGE	r10,	r21,	r3
	FPUN	r3,	r21,	r3
	BITOR	r3,	r3,	r10
	bneid	r3,	$0BB13_143
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB13_143:
	ORI	r3,	r0,	1065353216
	FPRSUB	r6,	r6,	r3
	FPRSUB	r6,	r9,	r6
	FPINVSQRT	r9,	r6
	ADD	r6,	r21,	r0
	bneid	r10,	$0BB13_145
	LWI	r29,	r1,	224
	FPNEG	r6,	r21
$0BB13_145:
	ORI	r10,	r0,	0
	FPGE	r11,	r12,	r10
	FPUN	r10,	r12,	r10
	BITOR	r10,	r10,	r11
	bneid	r10,	$0BB13_147
	ADDI	r11,	r0,	1
	ADDI	r11,	r0,	0
$0BB13_147:
	bneid	r11,	$0BB13_149
	ADD	r10,	r12,	r0
	FPNEG	r10,	r12
$0BB13_149:
	ADD	r22,	r12,	r0
	ORI	r11,	r0,	0
	FPGE	r12,	r29,	r11
	FPUN	r11,	r29,	r11
	BITOR	r11,	r11,	r12
	bneid	r11,	$0BB13_151
	ADDI	r12,	r0,	1
	ADDI	r12,	r0,	0
$0BB13_151:
	bneid	r12,	$0BB13_153
	ADD	r11,	r29,	r0
	FPNEG	r11,	r29
$0BB13_153:
	FPGE	r12,	r6,	r10
	FPUN	r20,	r6,	r10
	BITOR	r12,	r20,	r12
	ADDI	r20,	r0,	1
	bneid	r12,	$0BB13_155
	LWI	r28,	r1,	244
	ADDI	r20,	r0,	0
$0BB13_155:
	FPDIV	r3,	r3,	r9
	ORI	r12,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r20,	$0BB13_159
	NOP
	FPLT	r6,	r6,	r11
	bneid	r6,	$0BB13_158
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB13_158:
	bneid	r20,	$0BB13_163
	ADD	r6,	r9,	r0
$0BB13_159:
	FPLT	r6,	r10,	r11
	bneid	r6,	$0BB13_161
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB13_161:
	ORI	r6,	r0,	1065353216
	ORI	r9,	r0,	0
	bneid	r10,	$0BB13_163
	ADD	r12,	r9,	r0
	ORI	r6,	r0,	0
	ORI	r9,	r0,	1065353216
	ADD	r12,	r6,	r0
$0BB13_163:
	FPMUL	r10,	r29,	r6
	FPMUL	r11,	r22,	r9
	FPRSUB	r11,	r10,	r11
	FPMUL	r6,	r21,	r6
	FPMUL	r10,	r22,	r12
	FPRSUB	r6,	r10,	r6
	FPMUL	r9,	r21,	r9
	FPMUL	r10,	r29,	r12
	FPRSUB	r12,	r9,	r10
	FPMUL	r9,	r29,	r12
	FPMUL	r10,	r22,	r6
	FPRSUB	r9,	r9,	r10
	FPMUL	r10,	r21,	r6
	FPMUL	r20,	r29,	r11
	FPRSUB	r10,	r10,	r20
	FPMUL	r10,	r10,	r4
	FPMUL	r20,	r12,	r5
	FPADD	r10,	r20,	r10
	FPMUL	r9,	r9,	r4
	FPMUL	r20,	r11,	r5
	FPADD	r9,	r20,	r9
	FPMUL	r20,	r21,	r3
	FPADD	r9,	r9,	r20
	FPMUL	r20,	r22,	r3
	FPADD	r10,	r10,	r20
	FPMUL	r11,	r22,	r11
	FPMUL	r12,	r21,	r12
	FPMUL	r8,	r8,	r28
	LWI	r21,	r1,	256
	FPMUL	r20,	r27,	r21
	ADD	r23,	r21,	r0
	LWI	r22,	r1,	248
	FPMUL	r7,	r7,	r22
	LWI	r21,	r1,	232
	FPADD	r21,	r21,	r7
	SWI	r21,	r1,	232
	LWI	r7,	r1,	236
	FPADD	r7,	r7,	r20
	SWI	r7,	r1,	236
	FPADD	r25,	r25,	r8
	FPMUL	r7,	r10,	r10
	FPMUL	r8,	r9,	r9
	FPADD	r7,	r8,	r7
	FPRSUB	r8,	r11,	r12
	FPMUL	r4,	r8,	r4
	FPMUL	r5,	r6,	r5
	FPADD	r4,	r5,	r4
	FPMUL	r3,	r29,	r3
	FPADD	r3,	r4,	r3
	FPMUL	r4,	r3,	r3
	FPADD	r4,	r7,	r4
	FPINVSQRT	r4,	r4
	ORI	r5,	r0,	1065353216
	LWI	r6,	r1,	200
	ANDI	r6,	r6,	255
	bneid	r6,	$0BB13_25
	NOP
	brid	$0BB13_286
	NOP
$0BB13_3:
	SWI	r25,	r1,	236
	SWI	r25,	r1,	232
$0BB13_286:
	LWI	r8,	r1,	380
	FPDIV	r3,	r25,	r8
	ORI	r4,	r0,	0
	FPLT	r6,	r3,	r4
	bneid	r6,	$0BB13_288
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_288:
	bneid	r5,	$0BB13_293
	NOP
	ORI	r4,	r0,	1065353216
	FPGE	r6,	r3,	r4
	bneid	r6,	$0BB13_291
	ADDI	r5,	r0,	1
	ADDI	r5,	r0,	0
$0BB13_291:
	bneid	r5,	$0BB13_293
	NOP
	ADD	r4,	r3,	r0
$0BB13_293:
	LWI	r3,	r1,	236
	FPDIV	r5,	r3,	r8
	ORI	r3,	r0,	0
	FPLT	r7,	r5,	r3
	ADDI	r6,	r0,	1
	bneid	r7,	$0BB13_295
	LWI	r9,	r1,	292
	ADDI	r6,	r0,	0
$0BB13_295:
	bneid	r6,	$0BB13_300
	NOP
	ORI	r3,	r0,	1065353216
	FPGE	r7,	r5,	r3
	bneid	r7,	$0BB13_298
	ADDI	r6,	r0,	1
	ADDI	r6,	r0,	0
$0BB13_298:
	bneid	r6,	$0BB13_300
	NOP
	ADD	r3,	r5,	r0
$0BB13_300:
	LWI	r5,	r1,	232
	FPDIV	r6,	r5,	r8
	ORI	r5,	r0,	0
	FPLT	r8,	r6,	r5
	bneid	r8,	$0BB13_302
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB13_302:
	bneid	r7,	$0BB13_307
	NOP
	ORI	r5,	r0,	1065353216
	FPGE	r8,	r6,	r5
	bneid	r8,	$0BB13_305
	ADDI	r7,	r0,	1
	ADDI	r7,	r0,	0
$0BB13_305:
	bneid	r7,	$0BB13_307
	NOP
	ADD	r5,	r6,	r0
$0BB13_307:
	LWI	r6,	r1,	360
	LWI	r7,	r1,	364
	ADD	r6,	r6,	r7
	MULI	r6,	r6,	3
	LWI	r7,	r1,	372
	ADD	r6,	r6,	r7
	STORE	r6,	r4,	0
	STORE	r6,	r3,	1
	STORE	r6,	r5,	2
	ATOMIC_INC	r4,	0
	LWI	r3,	r1,	376
	CMP	r3,	r3,	r4
	bltid	r3,	$0BB13_2
	NOP
$0BB13_308:
	LWI	r31,	r1,	0
	LWI	r30,	r1,	4
	LWI	r29,	r1,	8
	LWI	r28,	r1,	12
	LWI	r27,	r1,	16
	LWI	r26,	r1,	20
	LWI	r25,	r1,	24
	LWI	r24,	r1,	28
	LWI	r23,	r1,	32
	LWI	r22,	r1,	36
	LWI	r21,	r1,	40
	LWI	r20,	r1,	44
	rtsd	r15,	8
	ADDI	r1,	r1,	400
#	.end	_Z9trax_mainv
$0tmp13:
#	.size	_Z9trax_mainv, ($tmp13)-_Z9trax_mainv

#	.globl	main
#	.align	2
#	.type	main,@function
#	.ent	main                    # @main
main:
#	.frame	r1,4,r15
#	.mask	0x8000
	ADDI	r1,	r1,	-4
	SWI	r15,	r1,	0
	brlid	r15,	_Z9trax_mainv
	NOP
	ADD	r3,	r0,	r0
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	main
$0tmp14:
#	.size	main, ($tmp14)-main

#	.globl	_Z6printfPKcz
#	.align	2
#	.type	_Z6printfPKcz,@function
#	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
#	.frame	r1,4,r15
#	.mask	0x0
	ADDI	r1,	r1,	-4
	SWI	r10,	r1,	28
	SWI	r9,	r1,	24
	SWI	r8,	r1,	20
	SWI	r7,	r1,	16
	SWI	r6,	r1,	12
	SWI	r5,	r1,	8
	ADDI	r3,	r1,	8
	PRINTF	r3
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_Z6printfPKcz
$0tmp15:
#	.size	_Z6printfPKcz, ($tmp15)-_Z6printfPKcz


#	.globl	_ZN5ImageC1Eiii
#_ZN5ImageC1Eiii = _ZN5ImageC2Eiii
#	.globl	_ZN6SphereC1ERK6Vectorfii
#_ZN6SphereC1ERK6Vectorfii = _ZN6SphereC2ERK6Vectorfii
#	.globl	_ZN6SphereC1Ev
#_ZN6SphereC1Ev = _ZN6SphereC2Ev
#	.globl	_ZN5LightC1Ev
#_ZN5LightC1Ev = _ZN5LightC2Ev
#	.globl	_ZN5LightC1ERK6VectorRK5Colori
#_ZN5LightC1ERK6VectorRK5Colori = _ZN5LightC2ERK6VectorRK5Colori
#	.globl	_ZN5LightC1ERK6VectorRK5Colorf
#_ZN5LightC1ERK6VectorRK5Colorf = _ZN5LightC2ERK6VectorRK5Colorf
