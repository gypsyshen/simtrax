	.file	"rt.bc"
	.text
	.globl	_Z9trax_mainv
	.align	2
	.type	_Z9trax_mainv,@function
	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
	.frame	r1,28,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -28
	SWI       r15, r1, 0
	ORI       r5, r0, ($.str)
	brlid     r15, _Z6printfPKcz
	NOP    
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 28
	.end	_Z9trax_mainv
$tmp0:
	.size	_Z9trax_mainv, ($tmp0)-_Z9trax_mainv

	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
	.frame	r1,4,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r15, r1, 0
	brlid     r15, _Z9trax_mainv
	NOP    
	ADD      r3, r0, r0
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	main
$tmp1:
	.size	main, ($tmp1)-main

	.globl	_Z6printfPKcz
	.align	2
	.type	_Z6printfPKcz,@function
	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
	.frame	r1,4,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r10, r1, 28
	SWI       r9, r1, 24
	SWI       r8, r1, 20
	SWI       r7, r1, 16
	SWI       r6, r1, 12
	SWI       r5, r1, 8
	ADDI      r3, r1, 8
	PRINTF     r3
	ADD      r3, r0, r0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_Z6printfPKcz
$tmp2:
	.size	_Z6printfPKcz, ($tmp2)-_Z6printfPKcz

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "Hello world!\n"
	.size	$.str, 14


