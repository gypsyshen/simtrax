#include <GL/glut.h>

void display() {}

int main(int argc, char **argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(512, 512);

	glutCreateWindow("Photon Map Viz");
	glutDisplayFunc(display);

	glutMainLoop();

	return 0;
}
