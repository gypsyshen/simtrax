#ifndef _syBVHNode_H
#define _syBVHNode_H

#include "trax.hpp"
#include "syBox.h"
#include "Ray.h"
#include "HitRecord.h"

class syBVHNode {
private:
	syBox box;
	int n_leaftri;
	int child;

public:
	syBVHNode(int _addr);

public:
	syBox GetRegion() const;
	
	void LoadFromMemory(const int &_addr);

	bool IntersectBoxW(const Ray &r, HitDist &t_box_near) const;
	void TraceRay(const Ray &ray, HitRecord &hit) const;
	void TraceShadowRay(const Ray &ray, HitDist &hit_dist) const;

	bool Interior() const;
	int LeftChildID() const;
	int RightChildID() const;
};

#endif
