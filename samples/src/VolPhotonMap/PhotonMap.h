#ifndef PhotonMap_H
#define PhotonMap_H

#include "trax.hpp"
#include "Scene.h"



typedef struct Photon {

	syVector pos; // photon position: float * 3
	Color power; // photon power (uncompressed): float * 3
	syVector dir; // incoming direction: float * 3
}Photon;


class PhotonMap {

private:

	int startGlobalMemory;
	int sizeGlobalMemory;

public:

	PhotonMap();

public:
	// build volume photon map and store it to the trax global memory
	void BuildVolPhotonMap(int numPhoton, const Scene &scene) const;

	// gather photons
	Color GatherPhotons( const syVector x, float radius ) const;



private:
	// check whether the n^th photon can be stored to the trax global memory
	bool TRaXMemoryAvailable( int n ) const;

	void TracePhoton(const Ray &ray, HitRecord &hit, int &bounce, const Scene &scene) const;

	// false for absorption
	bool ScatterPhoton(Ray &ray, const HitRecord &hit, Color &power, const Scene &scene) const;

	void StorePhoton(const Color &power, const Ray &ray, const HitRecord &hit, int bounce, int &numStoredPhoton) const;
	
	// store the k^th photon to TRaX global memory
	void StorePhotonToTrax(const Photon &photon, int k) const;

	void StorePhotonNumberToTrax(int n) const;

};



#endif
