#ifndef _Utility_H
#define _Utility_H

// header files
#include "trax.hpp"

#include "syVector.h"
#include "Color.h"

// -------------------- global definition --------------------
#define BIGFLOAT 100000.0f
#define SMALLFLOAT 0.0001f
#define FLOAT_BIAS 0.1f

#define TRI_SIZE_IN_TRAX 11

#define BVHNODE_SIZE_IN_TRAX 8

#define MAT_SIZE_IN_TRAX 25
#define ILLUM_OFFSET_IN_TRAX 0
#define KD_OFFSET_IN_TRAX 4
#define KS_OFFSET_IN_TRAX 7
#define D_OFFSET_IN_TRAX 16

#define ILLUM_D 0 // diffuse surface
#define ILLUM_R 5 // specular surface
#define ILLUM_T 7 // transparent surface

#define MAXBOUNCE_R 10
#define MAXBOUNCE_T 10
#define MAXBOUNCE_PHOTON 4

#define M_PI 3.1415926f
#define INV_M_PI 0.3183099f

#define INV_3F 0.3333f

#define ID_ATOMICINC_TRACE_CAMERA_RAY 0
#define ID_ATOMICINC_EMIT_PHOTON 1
#define ID_ATOMICINC_BARRIER 2
#define ID_ATOMICINC_NUM_STORED_PHOTONS 3
#define ID_ATOMICINC_STORE_PHOTON_NUM 4


//#define NUM_PHOTON 1000
//#define SIZE_PHOTON_BUFFER 3000

// -------------------- stack --------------------
class IntStack {
private:
	int data[32];
	int size; // stack size

public:
	IntStack() { Clear(); }

	bool Empty() const { 
		if(size > 0) return false; 
		return true;
	}
	void Push(int value) { data[size] = value; size++;}
	int Pop() {
		--size;
		int value = data[size];
		data[size] = -1;
		return value;
	}
	int Size() const { return size; }
	void Clear() {
		size = 0; 
		for(int i = 0; i != 32; ++i) data[i] = -1;
	}
};


// -------------------- utility --------------------
namespace util {

	const float STEP_LEN_VOLUME = 0.25f;
	const float STEP_LEN_TRACE_PHOTON = 20.f;
	const float RADIUS_GATHER_PHOTON = 10.f;

	float absf(const float& x);
	void swapf(float &a, float &b);
	float invf(const float &a);
	float powf(const float &x, const int &n); // return x^n
	syVector exp(const syVector &v); // return (e^v.x, e^v.y, e^v.z)
	Color exp(const Color &c); // return (e^c.r, e^c.g, e^c.b)
	float expf(const float &x); // return e^x
	int factorialf(const int &x); // return x!

	int bincheck(const float &a, const float &b);

	int stepVector(syVector &stepV, float stepLength, const syVector &v);

	syVector randDirNorm(); // generate a normalized random direction
};

inline syVector util::randDirNorm() {

	syVector v = syVector( trax_rand() - 0.5f, trax_rand() - 0.5f, trax_rand() - 0.5f);
	v.Normalize();
	return v;
}

inline int util::stepVector(syVector &stepV, float stepLength, const syVector &v) {
	
	int num = (int) (v.Length() / stepLength);
	stepV = v.GetNormalized() * stepLength;
	
	return num;
}

inline Color util::exp(const Color &c) {

	Color result;
	result.r = expf(c.r);
	result.g = expf(c.g);
	result.b = expf(c.b);
	return result;
}

inline syVector util::exp(const syVector &v) {
	
	syVector result;
	result.x = expf(v.x);
	result.y = expf(v.y);
	result.z = expf(v.z);
	return result;
}

inline int util::factorialf(const int &x) {
	
	if(x == 0 || x == 1) return 1;
	
	int result = 1;
	for(int k = 2; k <= x; ++k) result *= k;
	
	return result;
}

inline float util::expf(const float &x) {

	float result = 0.f;

	int n = 8;

	float a = 1.f;
	float b = 1.f;
	result += a / b;
	for(int i = 1; i < n; ++i) {

		a *= x;//util::powf(x, i);

		b *= i;//(float)util::factorialf(i);// * (i-1) * (i-2) * … * 1;

		result += a/b;

	}

	return result;
}

inline float util::powf(const float &x, const int &n) {
	float result = 1.f;
	for(int i = 0; i < n; ++i) result *= x;
	return result;
}

inline float util::absf(const float& x) {
	if (x < 0) return -x; 
	return x;
}

inline void util::swapf(float &a, float &b) {
	float c = a;
	a = b;
	b = c;
}

inline float util::invf(const float &a) {

	if(a < 0 && a > -SMALLFLOAT) return -BIGFLOAT;
	if(a >= 0 && a < SMALLFLOAT) return BIGFLOAT;

	return 1.f/a;
}

inline int util::bincheck(const float &a, const float &b) {
	if (a < b) return 0;
	return 1;
}

#endif
