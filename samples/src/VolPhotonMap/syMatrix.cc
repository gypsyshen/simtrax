#include "syMatrix.h"
#include "Utility.h"

syMatrix::syMatrix() {}

void syMatrix::Zero() { for(int i = 0; i != 9; ++i) data[i] = 0; }
void syMatrix::Identity() { for(int i=0; i<9; i++ ) data[i]=(i%4==0) ? 1.0f : 0.0f ; }

	syVector syMatrix::GetColumn( int col ) const {
	return syVector( &data[col*3] ); 
}

// private methods
/// calculate the LUD of the syMatrix
int syMatrix::LUD( int *outvect, int& output ) {
	int i, j, k, imax; 
	float VV[3], aamax, sum, dnum; 
	float SMALL = (float) 1.0e-10;

	output = 1;     /* No row interchanges yet*/
	for ( i = 0; i < 3; i++ ) {
		aamax = 0.0;
		for ( j = 0; j < 3; j++ ) {
			if( util::absf( data[ j * 3 + i ] ) > aamax )
			aamax = (float) util::absf( data[ j * 3 + i ] );
		}
		if ( aamax == 0. ) {
			return false; /* LUD_ERR flag Singular cysyMatrix3f */
		}
		VV[ i ] = (float) 1.0 / (float) aamax;
	}
    
	for ( j = 0; j < 3; j++ ) {
		if ( j > 0 ) {
			for ( i = 0; i < j; i++ ) {
				sum = data[ j * 3 + i ];
				if ( i > 0 ) {
					for ( k = 0; k < i; k++ )
						sum = sum - ( data[ k * 3 + i ] * data[ j * 3 + k ] );
					data[ j * 3 + i ] = sum;
				}
			}
		}

		aamax = 0.;

		for ( i = j; i < 3; i++ ) {
			sum = data[ j * 3 + i ];
			if ( j > 0 ) {
				for ( k = 0; k < j; k++ )
					sum = sum - ( data[ k * 3 + i ] * data[ j * 3 + k ] );
				data[ j * 3 + i ] = sum;
			}
			dnum = VV[ i ] * (float) util::absf( sum );
			if( dnum >= aamax ) {
				imax = i;
				aamax = dnum;
			}
		}
		if ( j != imax ) {
			for ( k = 0; k < 3; k++ ) {
				dnum = data[ k * 3 + imax ];
				data[ k * 3 + imax ] = data[ k * 3 + j ];
				data[ k * 3 + j ] = dnum;
			}
			output = -output;
			VV[ imax ] = VV[ j ];
		}
		outvect[ j ] = imax;
		if ( j != ( 3 - 1 ) ) {
			if ( data[ j * 3 + j ] == 0. )
				data[ j * 3 + j ] = SMALL;
			dnum = (float) 1.0 / data[ j * 3 + j ];
			for ( i = ( j + 1 ); i < 3; i++ )
				data[ j * 3 + i ] = data[ j * 3 + i ] * dnum;
		}
	}
	if ( data[ 8 ] == 0. ) data[ 8 ] = SMALL;

	return true; /* normal return */
}
/// calculate the LUD of the syMatrix
void syMatrix::LUBKS( const int *outvect, float *output ) {

	int i, j, ii, ll;
	float	sum;
	ii = -1;  /*when ii is set to a value >= 0 it is an index to
	the first non vanishing element of the output*/

	for ( i = 0; i < 3; i++ ) {
		ll = outvect[ i ];
		sum = output[ ll ];
		output[ ll ] = output[ i ];
		if ( ii != -1 ) {
			for ( j = ii; j < i; j++ )
				sum = sum - ( data[ j * 3 + i ] * output[ j ] );
		}
		else if ( sum != 0. ) {
			ii = i ; 
		}
		output[ i ] = sum;
	}
	for ( i = ( 3 - 1 ); i > -1; i-- ) {
		sum = output[ i ];
		if ( i < ( 3 - 1 ) ) {
			for ( j = ( i + 1 ); j < 3; j++ )
				sum = sum - ( data[ j * 3 + i ] * output[ j ] );
		}
		output[ i ] = sum / data[ i * 3 + i ];
	}
}

// public methods
void syMatrix::GetInverse( syMatrix &inverse ) const { 
	for(int i = 0; i != 9; ++i) inverse.data[i]=data[i]; 
		inverse.Invert(); 
	}
	void syMatrix::Invert() {

	float temp[3];
	int    IND[3], D = 0, i, j;

	// create the buffer syMatrix
	syMatrix buffer;

	for(int i = 0; i != 9; ++i ) buffer.data[i] = data[i];

	if ( ! buffer.LUD( IND, D ) ) return;

	for ( j = 0; j < 3; j++ )	{
		for ( i = 0; i < 3; i++ ) temp[i] = 0.0;
		temp[j] = 1.0;

		buffer.LUBKS( IND, temp);

		for ( i = 0; i < 3; i++ )
		data[ j * 3 + i ] = temp[ i ];
	}
}

syMatrix& syMatrix::operator *=( const syMatrix &right ){
	syMatrix buffer;  // a syMatrix of (m x k)
	for ( int i = 0; i < 3; i++ ) {
		for ( int k = 0; k < 3; k++ ) {
			buffer.data[ i + 3 * k ] = 0;
			for ( int j = 0; j < 3; j++ ) {
			buffer.data[ i + 3 * k ] += data[ i + 3 * j ] * right.data[ j + 3 * k ];
			}
		}
	}
	for(int i = 0; i != 9; ++i) data[i] = buffer.data[i];

	return *this;
}

syVector syMatrix::operator * ( const syVector& p) const {
	return syVector( p.x * data[0] + p.y * data[3] + p.z * data[6],
	p.x * data[1] + p.y * data[4] + p.z * data[7],
	p.x * data[2] + p.y * data[5] + p.z * data[8] );
}
