
#ifndef _sySphere_H
#define _sySphere_H

#include "trax.hpp"
#include "Ray.h"
#include "syVector.h"
#include "syMatrix.h"
#include "HitRecord.h"


class sySphere {
private:
	int id_obj, id_mat;
	
private:
	syVector pos;
	float radius;

public:
	sySphere();
	sySphere(const syVector &_p, const float &_r, const int &_id_obj, const int &_id_mat);

	// position and radius
	void Set(const syVector &_p, const float &_r);
	void Set(const syVector &_p, const float &_r, const int &_id_obj, const int &_id_mat);

	// in world space
	bool IntersectsW( const Ray &ray, HitRecord &hit ) const;
	float IntersectsWShadowRay(const Ray &ray) const;

	// get a normal syVector: center -> P
	syVector Normal( const syVector &P ) const;
	
	//
	const int& ID_OBJ() const;
	const int& ID_MAT() const;
};

#endif