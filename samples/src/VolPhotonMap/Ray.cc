
#include "Ray.h"
#include "Utility.h"

Ray::Ray() {}
Ray::Ray(const syVector _p, const syVector _dir) { Set(_p, _dir); }
Ray::Ray(const Ray &r) {
	*this = r;
}

void Ray::Set(const syVector _p, const syVector _dir) {
	p = _p; dir = _dir;

	Normalize();

	inv_dir = syVector(1.f/dir.x, 1.f/dir.y, 1.f/dir.z);

	sign[0] = (inv_dir.x < 0.f);
	sign[1] = (inv_dir.y < 0.f);
	sign[2] = (inv_dir.z < 0.f);
}

void Ray::Normalize() { dir.Normalize(); }

Ray& Ray::operator=(const Ray &_ray) {
	p = _ray.p;
	dir = _ray.dir;
	inv_dir = _ray.inv_dir;
	sign[0] = _ray.sign[0];
	sign[1] = _ray.sign[1];
	sign[2] = _ray.sign[2];
	return *this;
}