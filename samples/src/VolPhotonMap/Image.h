
#ifndef _Image_H
#define _Image_H

#include "Color.h"
#include "trax.hpp"

// Helper class to handle writes to the frame buffer

class Image {
protected:
	int xres, yres, start_fb;

public:
	Image(const int &xres, const int &yres, const int &start_fb);
	void Set(const int &i, const int &j, const Color& c);
};

#endif
