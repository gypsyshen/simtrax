// all trax programs must include trax.hpp
#include "trax.hpp"

#include "Image.h"
#include "Scene.h"
#include "PhotonMap.h"
#include "PinholeCamera.h"
#include "Shader.h"

// "main" function is called "trax_main", returns void instead of int
void trax_main(){

	///////////////////////////////////////////
	/// load TRaX parameters
	///////////////////////////////////////////

	int xres = GetXRes();
	int yres = GetYRes();
	int start_fb = GetFrameBuffer();
	Image image(xres, yres, start_fb);

	// load sample numbers
	int numSamples = loadi(TRAX_NUM_SAMPLES);
	// load ray depth
//	int rayDepth = loadi(TRAX_RAY_DEPTH);
	// load photon numbers
	int numPhotons = loadi(TRAX_RAY_DEPTH);
	
	// load camera
	PinholeCamera camera( GetCamera(), xres, yres );


	///////////////////////////////////////////
	/// initialize the scene
	///////////////////////////////////////////
	
	// scene
	bool volumeon = true;
	Scene scene(volumeon);


	///////////////////////////////////////////
	/// build volume photon map
	///////////////////////////////////////////	
//*
	// volume photon map
	PhotonMap volphotonmap;
	printf( ">>> building volume photon map ...\n" );
	printf(">>> %d photons to be traced ...\n", numPhotons);

	volphotonmap.BuildVolPhotonMap(numPhotons, scene); //*/

	///////////////////////////////////////////
	/// synchronize the threads before
	/// shading the scene
	///////////////////////////////////////////
	barrier( ID_ATOMICINC_BARRIER );


	///////////////////////////////////////////
	/// shade the scene
	///////////////////////////////////////////
//*
	// shader: used to shade the scene
	Shader shader;	
	
	// atomic increment allows multiple threads to get unique pixel assignments
	// generate camera ray
	for( int pix = atomicinc( ID_ATOMICINC_TRACE_CAMERA_RAY ); pix < xres*yres; pix = atomicinc(ID_ATOMICINC_TRACE_CAMERA_RAY) ) {
		// i, j are the framebuffer pixel coordinates
		// [i = 0, j = 0] is bottom-left
		// [i = yres-1, j = xres-1] is the top-right
		int i = pix / xres;
		int j = pix % xres;
		
		Color result(0.f, 0.f, 0.f);
		// anti-aliasing
		for(int s = 0; s < numSamples; ++s) {
			// generate a ray
			Ray ray;
			if(numSamples == 1) ray = camera.GenerateRay(j, i);
			else ray = camera.GenerateSampleRay(j, i);
			result += shader.GetColor(ray, scene, volphotonmap);
		}
		if(numSamples > 1) result /= (float)numSamples;
		
		// assign the color
		image.Set(i, j, result);
	} //*/
}


