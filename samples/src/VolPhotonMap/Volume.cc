#include "Volume.h"
#include "Utility.h"

// ------------------ constructors ---------------------------

Volume::Volume() { 
	Set();
}

Volume::Volume(const syVector &_min, const syVector &_max) {
	Set(_min, _max);
}

Volume::Volume(const syVector &_min, const syVector &_max, const Color &_alpha, const Color &_rho, const Color &_le) {

	Set(_min, _max, _alpha, _rho, _le);
}
// ------------------ public functions ---------------------------
// Set functions
void Volume::Set() {
	SetRegion();
	SetParams();
}
void Volume::Set(const syVector &_min, const syVector &_max) {
	SetRegion(_min, _max);
	SetParams();
}
void Volume::Set(const syVector &_min, const syVector &_max, const Color &_alpha, const Color &_rho, const Color &_le) {
	SetRegion(_min, _max);
	SetParams(_alpha, _rho, _le);
}

void Volume::SetParams() {
	alpha = Color(1.f, 1.f, 1.f);
	rho = Color(1.f, 1.f, 1.f);
	le = Color(1.f, 1.f, 1.f);
	phase = PhaseIsotropic();

	IAlpha = 1.f;
	IRho = 1.f;
	albedo = 0.5f;
}

void Volume::SetParams(const Color &_alpha, const Color &_rho, const Color &_le) {
	alpha = _alpha;
	rho = _rho;
	le = _le;
	phase = PhaseIsotropic();

	IAlpha = ( alpha.r + alpha.g + alpha.b ) * INV_3F;
	IRho = ( rho.r + rho.g + rho.b ) * INV_3F;
	albedo = IRho / ( IAlpha + IRho );
}

void Volume::SetRegion() {
	region.Set();
}

void Volume::SetRegion(const syVector &_min, const syVector _max) {
	region.Set(_min, _max);
}

bool Volume::IntersectsW ( const Ray &ray, HitDist &t_box_near ) const {

	return region.IntersectsW( ray, t_box_near );
}

//
bool Volume::IntersectsW( const Ray &ray, HitDist &t_box_near, HitDist &t_box_far ) const {
	
	return region.IntersectsW(ray, t_box_near, t_box_far);
}

bool Volume::Contains( const syVector &p ) const {

	return region.Contains( p );
}

// ------------------ private functions ---------------------------

// homogeneous, isotropic
Color Volume::Le(const syVector &x, const syVector &dir) const {

	return le;
}

// TODO: test
// homogeneous, isotropic
Color Volume::Tau(const syVector &detX) const {

	Color a = (rho + alpha) * detX.Length() * (-1.f);
	return util::exp(a);
}
Color Volume::Tau(const syVector &x, const syVector &x0) const {

	return Tau(x - x0);
}

// any media
Color Volume::Kappa(const syVector &x) const {

	return Alpha(x) + Rho(x);
}

// homogeneous, isotropic
Color Volume::Alpha(const syVector &x) const {

	return alpha;
}

// homogeneous, isotropic
Color Volume::Rho(const syVector &x) const {

	return rho;
}

float Volume::PhaseIsotropic() const {
	
	return 1.f / (4.f * M_PI);
}







