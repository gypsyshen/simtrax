#include "PhotonMap.h"
#include "Utility.h"


// ------------------- construction -------------------

PhotonMap::PhotonMap() {
	startGlobalMemory = loadi(TRAX_END_MEMORY);
	sizeGlobalMemory = loadi(TRAX_MEM_SIZE);
}


// ------------------- public functions -------------------

Color PhotonMap::GatherPhotons( const syVector x, float radius ) const {

	int numPhotons = loadi( startGlobalMemory + 1 );

	int n = 0;
	Color intensity = Color( 0.f, 0.f, 0.f );

	for( int k = 0; k < numPhotons; ++k ) {

		int offset = k * 9;

		syVector pos;
		pos.x = loadf( startGlobalMemory + 1 + offset );
		pos.y = loadf( startGlobalMemory + 1 + offset + 1 );
		pos.z = loadf( startGlobalMemory + 1 + offset + 2 );

		if( ( pos - x ).Length() > radius ) continue;

		Color power;
		power.r = loadf( startGlobalMemory + 1 + offset + 3 );
		power.g = loadf( startGlobalMemory + 1 + offset + 4 );
		power.b = loadf( startGlobalMemory + 1 + offset + 5 );

		intensity += power;
		n++;
	}
	
	return intensity;
}


void PhotonMap::BuildVolPhotonMap(int numPhoton, const Scene &scene) const {

	////////////////////////////////////////////////
	/// store 0 photon
	////////////////////////////////////////////////
	if( !scene.volumeon ) {
		if( GetCoreID() == 0 && GetThreadID() == 0 ) {
			StorePhotonNumberToTrax( 0 );
		}
		return;
	}


	////////////////////////////////////////////////
	/// trace numPhotons, store numStoredPhotons
	////////////////////////////////////////////////

	int numStoredPhotons = atomicinc( ID_ATOMICINC_NUM_STORED_PHOTONS );

	// emit photons
	for(int i = atomicinc( ID_ATOMICINC_EMIT_PHOTON ); i < numPhoton; i = atomicinc( ID_ATOMICINC_EMIT_PHOTON ) ) {


//		printf(" thread %d, photon: %d\n ", GetThreadID(), i);

		///////////////////////////////////////////////
		/// check the available memory
		///////////////////////////////////////////////

		if( TRaXMemoryAvailable( numStoredPhotons ) ) {


			///////////////////////////////////////////////
			/// initialization
			///////////////////////////////////////////////

			// ray
			syVector dir = util::randDirNorm();
			syVector p = scene.light.Position();
			Ray ray = Ray(p, dir);
			// hit record
			HitRecord hit;
			// hit bounce
			int bounce = 0;
			// power
			Color power = scene.light.Intensity();

			///////////////////////////////////////////////
			/// main loop for tracing the photon
			///////////////////////////////////////////////

			/// TODO double check: draw draw draw

			while( bounce < MAXBOUNCE_PHOTON ) {

	//			printf("photon %d's bounce: %d\n", i, bounce);

				// trace photon: 1. hit surface or media; 2. no hits
				hit.Init();
				TracePhoton(ray, hit, bounce, scene);

				// if the photon disappears, stop tracing
				if( !hit.Hit() ) break;

				// check whether the photon is absorbed
				Ray oldRay = ray;
				bool absorbed = ScatterPhoton(ray, hit, power, scene);

				// store photon when it's absorbed by media
				if( absorbed ) {
					if(hit.details.media) {
						StorePhoton(power, oldRay, hit, bounce, numStoredPhotons);
						numStoredPhotons = atomicinc( ID_ATOMICINC_NUM_STORED_PHOTONS );
//						printf(" stored photons: %d \n " , numStoredPhotons );
					}
					break;
				}

				/////////
			}
		} 
	}

	barrier( ID_ATOMICINC_STORE_PHOTON_NUM );

	if( GetCoreID() == 0 && GetThreadID() == 0 ) {
		StorePhotonNumberToTrax( numStoredPhotons + 1 );
		printf("%d photons stored to the TRaX memory!\n\n", numStoredPhotons);
	}

} 


/*
void PhotonMap::BuildVolPhotonMap(int numPhoton, const Scene &scene) const {

	////////////////////////////////////////////////
	/// store 0 photon
	////////////////////////////////////////////////
	if( !scene.volumeon ) {
		StorePhotonNumberToTrax( 0 );
		return;
	}


	////////////////////////////////////////////////
	/// trace numPhotons, store numStoredPhotons
	////////////////////////////////////////////////

	int numStoredPhotons = 0;

	int TRaXMemoryIsAvailable = true;

	// emit photons
	for(int i = 0; i < numPhoton; ++i) {

		///////////////////////////////////////////////
		/// check the available memory
		///////////////////////////////////////////////

		if( !TRaXMemoryIsAvailable ) break;


		///////////////////////////////////////////////
		/// initialization
		///////////////////////////////////////////////

		// ray
		syVector dir = util::randDirNorm();
		syVector p = scene.light.Position();
		Ray ray = Ray(p, dir);
		// hit record
		HitRecord hit;
		// hit bounce
		int bounce = 0;
		// power
		Color power = scene.light.Intensity();


		///////////////////////////////////////////////
		/// main loop for tracing the photon
		///////////////////////////////////////////////

		/// TODO double check: draw draw draw

		while( bounce < MAXBOUNCE_PHOTON ) {

//			printf("photon %d's bounce: %d\n", i, bounce);

			// trace photon: 1. hit surface or media; 2. no hits
			hit.Init();
			TracePhoton(ray, hit, bounce, scene);

			// if the photon disappears, stop tracing
			if( !hit.Hit() ) break;

			// check whether the photon is absorbed
			Ray oldRay = ray;
			bool absorbed = ScatterPhoton(ray, hit, power, scene);

			// store photon when it's absorbed by media
			if( absorbed ) {
				if(hit.details.media) {
					TRaXMemoryIsAvailable = StorePhoton(power, oldRay, hit, bounce, numStoredPhotons);
				}
				break;
			}

			/////////
		}

		/// TODO double check

	}

	StorePhotonNumberToTrax(numStoredPhotons);
	printf("%d photons stored to the TRaX memory!\n\n", numStoredPhotons);
} //*/

// ------------------- private functions -------------------

bool PhotonMap::ScatterPhoton(Ray &ray, const HitRecord &hit, Color &power, const Scene &scene) const {

	bool absorbed = false;
	
	// media
	if( hit.details.media ) {

		absorbed = scene.RussionRouletteMedia( hit.details.P );
		if( !absorbed ) ray = scene.ScatterRayInMedia( hit.details.P );
	}
	// surface reflection
	else {

		absorbed = scene.RussionRouletteSurface( hit.details );
		if( !absorbed ) {
			ray = scene.ScatterRayAtSurface( ray, hit.details );
			if( hit.details.diffusion ) power *= hit.details.Kd;
		}
	}

	return absorbed;
}

// Assumption: hit.details.media == true
void PhotonMap::StorePhoton(const Color &power, const Ray &ray, const HitRecord &hit, int bounce, int &numStoredPhoton) const {

//	bool TRaXMemoryIsAvailable = true;

	///////////////////////////////////////////////
	/// ignore the first bounce
	///////////////////////////////////////////////

	if( bounce <= 1 ) return ;


	///////////////////////////////////////////////
	/// IF the photon is absorbed by the media, store
	///////////////////////////////////////////////

//	if( hit.details.media ) {

		// DO: store the photon to the TRaX global memory
		Photon photon;

		photon.power = power;
		photon.pos = hit.details.P;
		photon.dir = ray.dir;

		StorePhotonToTrax(photon, numStoredPhoton);


//	}

//	return succeed;
}

// Assumption: hit.Init()
void PhotonMap::TracePhoton(const Ray &ray, HitRecord &hit, int &bounce, const Scene &scene) const {

	scene.TraceRayWithVolume( ray, hit, util::STEP_LEN_TRACE_PHOTON );
	if( hit.Hit() ) bounce++;
}

void PhotonMap::StorePhotonNumberToTrax(int n) const {

	storei(n, startGlobalMemory + 1);
}

// check availability of TRaX global memory
bool PhotonMap::TRaXMemoryAvailable( int n ) const {

	int offset = 9 * n;

	if( startGlobalMemory + 1 + offset + 9 > sizeGlobalMemory ) return false;

	return true;
}


// assumption: trax memory is available
void PhotonMap::StorePhotonToTrax(const Photon &photon, int k) const {

	// check availability of TRaX global memory
	int offset = 9 * k;
//	if( startGlobalMemory + 1 + offset + 9 > sizeGlobalMemory ) return false;

	// store position
	storef(photon.pos.x, startGlobalMemory + 1 + offset);
	storef(photon.pos.y, startGlobalMemory + 1 + offset + 1);
	storef(photon.pos.z, startGlobalMemory + 1 + offset + 2);

	// store power
	storef(photon.power.r, startGlobalMemory + 1 + offset + 3);
	storef(photon.power.g, startGlobalMemory + 1 + offset + 4);
	storef(photon.power.b, startGlobalMemory + 1 + offset + 5);

	// store incoming direction
	storef(photon.dir.x, startGlobalMemory + 1 + offset + 6);
	storef(photon.dir.y, startGlobalMemory + 1 + offset + 7);
	storef(photon.dir.z, startGlobalMemory + 1 + offset + 8);

//	printf( "pos: %f, %f, %f \t", photon.pos.x, photon.pos.y, photon.pos.z );
//	printf( "power: %f, %f, %f\n\n", photon.power.r, photon.power.g, photon.power.b );

//	return true;
}









