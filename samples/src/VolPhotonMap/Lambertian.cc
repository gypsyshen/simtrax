#include "Lambertian.h"


void Lambertian::SetDiffuse(const Color &_d) {
	diffuse = _d;
}

const Color& Lambertian::Diffuse() const {
	return diffuse;
}