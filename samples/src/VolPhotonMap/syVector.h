#ifndef _syVector_H
#define _syVector_H

#include "trax.hpp"

class syVector {
public:
	float x, y, z;

public:
	// initialize vector from memory
	syVector(int _addr);
	// 
	syVector();
	syVector(const float &_x, const float &_y, const float &_z);
	syVector( const float *pt );

	// Set & Get value functions
	void Zero () ;
	void Set (const float &_x, const float &_y, const float &_z);

	// Load Vector From Memorys
	void LoadFromMemory(const int &_addr);

	// Length and Normalize functions
	float LengthSquared () const ;
	float	Length () const ;
	void 	Normalize ();
	syVector GetNormalized () const ;
	
	// Cross product and dot product
	float Dot (const syVector &pt) const;
	float operator% (const syVector &pt) const;
	syVector Cross (const syVector &pt) const;
	syVector operator^ (const syVector &pt) const ;
	
	// Binary operators
	syVector operator+( const syVector &pt ) const;
	syVector operator-( const syVector &pt ) const;
	syVector operator*( const syVector &pt ) const;
	syVector operator/( const syVector &pt ) const;
	syVector operator+(const float &n) const;
	syVector operator-(const float &n) const;
	syVector operator*(const float &n) const;
	syVector operator/(const float &n) const;

	// Assignment operators
	syVector& operator=(const syVector &pt);
	syVector& operator+=( const syVector &pt );
	syVector& operator-=( const syVector &pt );
	syVector& operator*=( const syVector &pt );
	syVector& operator/=( const syVector &pt );
	syVector& operator+=( const float &n);
	syVector& operator-=( const float &n) ;
	syVector& operator*=( const float &n) ;
	syVector& operator/=( const float &n) ;

	// Unary operators
	syVector operator-() const;
	
	// Test operators
	bool operator==( const syVector& pt ) const;
	bool operator!=( const syVector& pt ) const ;


	////
	syVector GetOrtho() const;
};

#endif