
#ifndef _Color_H
#define _Color_H

#include "trax.hpp"

class Color {

public:
	float r, g, b;

public:

	// constructors
	// initialize color from memory
	Color(int _addr);

	Color();
	Color(const Color& copy);
	Color(const float &gray);
	Color(const float &r, const float &g, const float &b);

	// load from memory
	void LoadFromMemory(const int &_addr);
		
	// binary operators
    Color operator+( const Color &c ) const;
    Color operator-( const Color &c ) const;
    Color operator*( const Color &c ) const;
    Color operator/( const Color &c ) const;
    Color operator+(float n) const;
    Color operator-(float n) const;
    Color operator*(float n) const;
    Color operator/(float n) const;
 
    // assignment operators
    Color& operator+=( const Color &c );
    Color& operator-=( const Color &c );
    Color& operator*=( const Color &c );
    Color& operator/=( const Color &c );
    Color& operator+=(float n);
    Color& operator-=(float n);
    Color& operator*=(float n);
    Color& operator/=(float n);

	// clamp
	Color Clamp() const;
	Color Clamp(const Color& l, const Color &h) const;

	// r g b values
	const float& R() const;
	const float& G() const;
	const float& B() const;
};


#endif
