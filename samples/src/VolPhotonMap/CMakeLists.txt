# file: <path>/simtrax/samples/src/VolPhotonMap/CMakeLists.txt


# Write out all of the sources here
# Note that the paths are relative to directory where this CMakeLists.txt is located
set(VOLPHOTONMAP_SRC
	Color.cc
	HitRecord.cc
	Image.cc
	Lambertian.cc
	PhotonMap.cc
	PinholeCamera.cc
	PointLight.cc
	Ray.cc
	Scene.cc
	Shader.cc
	Volume.cc
	syBox.cc
	syBVHNode.cc
	syMatrix.cc
	sySphere.cc
	syTriangle.cc
	syVector.cc
	main_RayTracer.cc
)

# Write out all of the headers here
set(VOLPHOTONMAP_HDR
	Color.h
	HitRecord.h
	Image.h
	Lambertian.h
	PhotonMap.h
	PinholeCamera.h
	PointLight.h
	Scene.h
	Shader.h
	Volume.h
	syBox.h
	syBVHNode.h
	syMatrix.h
	sySphere.h
	syTriangle.h
	syVector.h
	Utility.h
	Ray.h
)

# get the project set up
# parameters: VolPhotonMap - basename of the project. Results in volphotonmap.exe and volphotonmap_rt-llvm.s
#             VOLPHOTONMAP_SRC - source files list from above
#             VOLPHOTONMAP_HDR - header files list from above
#             the other 2 should just be "" each
applyToTarget(VolPhotonMap VOLPHOTONMAP_SRC VOLPHOTONMAP_HDR "" "")
