#ifndef _Lambertian_H
#define _Lambertian_H

#include "trax.hpp"
#include "Color.h"


class Lambertian {
private:
	Color diffuse;

public:
	void SetDiffuse(const Color &_d);
	const Color& Diffuse() const; 
};

#endif