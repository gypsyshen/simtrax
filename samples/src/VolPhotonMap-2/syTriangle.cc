#include "syTriangle.h"
#include "Utility.h"

syTriangle::syTriangle(int _addr) {
	LoadFromMemory(_addr);
}

syTriangle::syTriangle() {}
syTriangle::syTriangle(const syTriangle &_tri) { Set(_tri.v0, _tri.v1, _tri.v2); }
syTriangle::syTriangle(const syVector &_v0, const syVector &_v1, const syVector &_v2) { Set(_v0, _v1, _v2); }

// set values
void syTriangle::Set(const syVector &_v0, const syVector &_v1, const syVector &_v2) {
	v0 = _v0;
	v1 = _v1;
	v2 = _v2;
	norm_ready = false;
}

void syTriangle::LoadFromMemory(const int _addr) {
	addr = _addr;
	// load 3 vertices
	LoadVertex();
	// load mat id
	mat_ready = false;
	// normal
	norm_ready = false;
}

void syTriangle::LoadVertex() {
	v0.LoadFromMemory(addr);
	v1.LoadFromMemory(addr+3);
	v2.LoadFromMemory(addr+6);
}

void syTriangle::LoadMaterial() const{
	mat_id = loadi(addr+10);
}

//syVector syTriangle::Interpolate() const{
//}

syVector syTriangle::Normal() const{
	if(norm_ready) return norm;

	syVector edge1 = v0 - v1;
	syVector edge2 = v0 - v2;
	norm = edge1 ^ edge2;
	norm.Normalize();
	norm_ready = true;
	return norm;
}

int syTriangle::ID_MAT() const{
	if(mat_ready) return mat_id;

	LoadMaterial();
	mat_ready = true;
	return mat_id;
}

// in world space
bool syTriangle::IntersectsW( const Ray &ray, HitRecord &hit ) const {

	syVector e1 = v1 - v0;
	syVector e2 = v2 - v0;
	syVector s = ray.p - v0;

	syVector p = ray.dir ^ e2;
	syVector q = s ^ e1;

	float d_tuv = p % e1;
	if( util::absf( d_tuv ) < FLOAT_BIAS ) return false;

	float inv_d_tuv = 1.f/d_tuv;

	float T = ( q % e2 ) * inv_d_tuv;
	if( T < 0 )	return false;

	float U = ( p % s ) * inv_d_tuv;
	float V = ( q % ray.dir ) * inv_d_tuv;

	if( U >= 0.0f && V >= 0.0f && U + V <= 1.0f ) {
		if( T < hit.t ) {
			hit.t = T;
//			hit.p = ray.p + ray.dir * T;
//			hit.N = Normal();
//			hit.front = ( ray.dir%hit.N < 0.f );
			hit.id_mat = ID_MAT();
			hit.id_obj = addr;
			return true;
		}
	}

	return false;
}

bool syTriangle::IntersectsWShadowRay( const Ray &ray, HitDist &hit_dist ) const {
	syVector e1 = v1 - v0;
	syVector e2 = v2 - v0;
	syVector s = ray.p - v0;

	syVector p = ray.dir ^ e2;
	syVector q = s ^ e1;

	float d_tuv = p % e1;
	if( util::absf( d_tuv ) < FLOAT_BIAS ) return false;

	float inv_d_tuv = 1.f/d_tuv;

	float T = ( q % e2 ) * inv_d_tuv;
	if( T < 0 )	return false;

	float U = ( p % s ) * inv_d_tuv;
	float V = ( q % ray.dir ) * inv_d_tuv;

	if( U >= 0.0f && V >= 0.0f && U + V <= 1.0f ) {
		if( T < hit_dist.t ) {
			hit_dist.t = T;
			return true;
		}
	}

	return false;
}
