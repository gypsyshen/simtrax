#ifndef _syTri_H
#define _syTri_H

#include "trax.hpp"
#include "syVector.h"
#include "Ray.h"
#include "HitRecord.h"

class syTriangle{
private:
	// vertices
	syVector v0, v1, v2;
	int addr;

	// material id in the memory
	mutable bool mat_ready;
	mutable int mat_id;

	// the normal
	mutable bool norm_ready;
	mutable syVector norm;

public:
	// load from memory
	syTriangle(int _addr);
	// 
	syTriangle();
	syTriangle(const syTriangle &_tri);
	syTriangle(const syVector &_v0, const syVector &_v1, const syVector &_v2);

	// load from memory
	void LoadFromMemory(const int _addr);
	// set values
	void Set(const syTriangle &_tri);
	void Set(const syVector &_v0, const syVector &_v1, const syVector &_v2); 

//	syVector GetNormal(const syVector &bc) const;
	syVector Normal() const;
	int ID_MAT() const;

	// in world space
	bool IntersectsW( const Ray &ray, HitRecord &hit ) const;
	bool IntersectsWShadowRay( const Ray &ray, HitDist &hit_dist ) const;

private:
//	syVector Interpolate() const;
	void LoadVertex();
	void LoadMaterial() const; // hack..
};

#endif