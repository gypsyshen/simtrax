#ifndef Volume_H
#define Volume_H

#include "trax.hpp"
//#include "syVector.h"
#include "syBox.h"
#include "Color.h"
#include "Ray.h"
#include "PointLight.h"

// homogeneous media with isotropic scattering
class Volume {

public:
	// the boundary of the volume
	syBox region;

	// parameters
	Color alpha; // constant absorption coefficient for homogeneous media
	Color rho; // constant scattering coefficient for homogeneous media
	Color le; // constant emitted radiance for homogeneous media
	float phase; // constant phase for isotropic scattering

	float IAlpha, IRho;
	float albedo; // constant albedo for scattering: rho / ( rho + alpha )

	Volume();
	Volume(const syVector &_min, const syVector &_max);
	Volume(const syVector &_min, const syVector &_max, const Color &_alpha, const Color &_rho, const Color &_le);

private:
	// phase function of isotropic media
	float PhaseIsotropic() const;
	
	// phase function of anisotropic media
	// float PhaseAnisotropic(const syVector &omega1, const syVector &omega2) const;

	// absorption coefficient
	Color Alpha(const syVector &x) const;
	// scattering
	Color Rho(const syVector &x) const;
	// extinction
	Color Kappa(const syVector &x) const;
	// the emitted radiance at point x along direction dir
	Color Le(const syVector &x, const syVector &dir) const;

public:
	void Set();
	void Set(const syVector &_min, const syVector &_max);
	void Set(const syVector &_min, const syVector &_max, const Color &_alpha, const Color &_rho, const Color &_le);
	void SetRegion();
	void SetRegion(const syVector &_min, const syVector _max);
	void SetParams();
	void SetParams(const Color &_alpha, const Color &_rho, const Color &_le);

	// in world space, no transformations
	bool IntersectsW( const Ray &ray, HitDist &t_box_near, HitDist &t_box_far ) const;
	bool IntersectsW( const Ray &ray, HitDist &t_box_near ) const;
	// 
	bool Contains( const syVector &p ) const;

	// transmittance along the line segment from x0 to x, where detX = x - x0
	Color Tau(const syVector &x, const syVector &x0) const;
	Color Tau(const syVector &detX) const;


};

#endif
