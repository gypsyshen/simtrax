#ifndef Shader_H
#define Shader_H

#include "trax.hpp"
#include "Scene.h"
#include "PhotonMap.h"

class Shader{

private:
	// volume rendering
	Color Illuminate(const syVector &x, const Scene &scene) const;
	Color Illuminate(const Ray &ray, const syVector &x, const Color &L0, const Scene &scene) const;
	Color Illuminate(const syVector &x, const syVector &x0, const Color &L0, const Scene &scene) const;

	Color Attenuated(const syVector &detX, const Color &L, const Scene &scene) const;
	Color Emitted(int numStep, const syVector &x0, const syVector &stepX, const Scene &scene) const;
	Color Inscattered(int numStep, const syVector &x0, const syVector &x, const syVector &stepX, const Scene &scene, const PhotonMap &volpm) const;

	Color Radiance(const syVector &x, const syVector &x0, const Color &L0, const Scene &scene, const PhotonMap &volpm) const;
	Color RadianceWithVolume(const Ray &ray, const HitRecord &hit, const Color &L0, const Scene &scene, const PhotonMap &volpm) const;

	// surface rendering
	Color DirectShadeSurfaceWithoutVolume(const Ray &ray, const HitRecord &hit, const Scene &scene) const;
	Color DirectShadeSurfaceWithVolume(const Ray &ray, const HitRecord &hit, const Scene &scene) const;
	
	Color GetDiffuseColorWithVolume(const Ray &_ray, const HitRecord &_hit, const Scene &scene) const;
	Color GetDiffuseColorWithoutVolume(const Ray &_ray, const HitDetails &_hitDetails, const Scene &scene) const;

	// TODO: indirect shading
//	Color IndirectIllumination(const Ray &ray, const HitRecord &hit, const Scene &scene) const;

	// direct illumination
	Color DirectIllumination( const Ray &ray, const HitRecord &hit, const Scene &scene, const PhotonMap &volpm ) const;

public:
	Color GetColor(const Ray &ray, const Scene &scene, const PhotonMap &volpm) const;
	
};

#endif
