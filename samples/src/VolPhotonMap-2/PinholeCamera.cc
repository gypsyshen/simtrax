#include "PinholeCamera.h"

PinholeCamera::PinholeCamera(int _addr, const int &_xres, const int &_yres) {
	Set(_addr, _xres, _yres);
}

PinholeCamera::PinholeCamera() { }

PinholeCamera::PinholeCamera(const syVector &_eye, const syVector &_up, const syVector &_gaze, const float &_ulen, const int &_xres, const int &_yres) {

	Set(_eye, _up, _gaze, _ulen, _xres, _yres);
}

void PinholeCamera::Set(int _addr, const int &_xres, const int &_yres) {
	
	//------------------------------------------------------------
	inv_xres = 1.f / (float)_xres;
	inv_yres = 1.f / (float)_yres;
	
	//------------------------------------------------------------
	LoadFromMemory(_addr);

}

void PinholeCamera::Set(const syVector &_eye, const syVector &_up, const syVector &_gaze, const float &_ulen, const int &_xres, const int &_yres) {
	
	//------------------------------------------------------------
	inv_xres = 1.f / (float)_xres;
	inv_yres = 1.f / (float)_yres;

	//------------------------------------------------------------
	eye = _eye;
	up = _up;
	u_len = _ulen;

	gaze = _gaze;
	gaze.Normalize();
	
	u = gaze ^ up;
	v = u ^ gaze;
	u.Normalize();
	v.Normalize();
	
	float aspect_ratio = (float)_xres * inv_yres;
	u *= u_len;
	v *= (u_len / aspect_ratio);
}

void PinholeCamera::LoadFromMemory(const int &_addr) {
	
	eye.LoadFromMemory(_addr);
	up.LoadFromMemory(_addr+9);
	gaze.LoadFromMemory(_addr+12);
	u.LoadFromMemory(_addr+15);
	v.LoadFromMemory(_addr+18);
}

Ray PinholeCamera::GenerateRay(const int &_x, const int &_y) const{
	
	// scale to [-1 1]
	float sc_x = (float)_x * inv_xres * 2.f - 1.f;
	float sc_y = (float)_y * inv_yres * 2.f - 1.f;

	// direction
	syVector dir = gaze + u * sc_x + v * sc_y;

	Ray ray(eye, dir);

	return ray;
}

// randomly generate a ray around pixel(x, y)
Ray PinholeCamera::GenerateSampleRay(const int &_x, const int &_y) const{
	
	// pixel area: [-0.5, 0.5]
	float offset_x = trax_rand() - 0.5f;
	float offset_y = trax_rand() - 0.5f;
	// scale to [-1 1]
	float sc_x = ((float)_x+offset_x) * inv_xres * 2.f - 1.f;
	float sc_y = ((float)_y+offset_y) * inv_yres * 2.f - 1.f;

	// direction
	syVector dir = gaze + u * sc_x + v * sc_y;

	Ray ray(eye, dir);

	return ray;
}