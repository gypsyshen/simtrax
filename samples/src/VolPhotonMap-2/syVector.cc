#include "syVector.h"
#include "Utility.h"

syVector::syVector(int _addr) {
	LoadFromMemory(_addr);
}

syVector::syVector() {}
syVector::syVector(const float &_x, const float &_y, const float &_z) { Set(_x, _y, _z); }
syVector::syVector( const float *pt ) { x=pt[0]; y=pt[1]; z=pt[2]; }

// Set & Get value functions
void syVector::Zero () {x = 0; y = 0;  z = 0;}
void syVector::Set (const float &_x, const float &_y, const float &_z) {x = _x; y = _y; z = _z;}

// Length and Normalize functions
float syVector::LengthSquared () const  { 
	float ls = x*x + y*y + z*z; 
	return ls;
}
float syVector::Length () const { 
	float l = sqrt(LengthSquared());
	return l;
}
void syVector::Normalize () { float len = Length(); x /= len; y /= len; z /= len; }

syVector syVector::GetNormalized () const { 
	float len = Length(); 
	syVector v(x/len, y/len, z/len);
	return v; 
}

void syVector::LoadFromMemory(const int &_addr) {	
	x = loadf(_addr, 0);
	y = loadf(_addr, 1);
	z = loadf(_addr, 2);
}

syVector syVector::operator-() const { 
	return syVector(-1.0f * x,-1.0f * y,-1.0f * z); 
} 

syVector syVector::GetOrtho() const {

	syVector axis(0.0f, 0.0f, 0.0f);
	float ax = util::absf(x);
	float ay = util::absf(y);
	float az = util::absf(z);

	if(ax < ay && ax < az) { 
		axis = syVector(1.0f, 0.0f, 0.0f); 
	} else if (ay < az) { 
		axis = syVector(0.0f, 1.0f, 0.0f); 
	} else { 
		axis = syVector(0.0f, 0.0f, 1.0f); 
	}
	return this->Cross(axis);
}
// Cross product and dot product
float syVector::Dot (const syVector &pt) const { 
	float d = x*pt.x + y*pt.y + z*pt.z; 
	return d;
}
float syVector::operator% (const syVector &pt) const { return Dot(pt); }

	syVector syVector::Cross (const syVector &pt) const { 
	syVector v( (y*pt.z - z*pt.y), (z*pt.x - x*pt.z), (x*pt.y - y*pt.x) );
	return v;
}

syVector syVector::operator^ (const syVector &pt) const { return Cross(pt); }
	// Binary operators
	syVector syVector::operator+( const syVector &pt ) const { 
	syVector v(x+pt.x, y+pt.y, z+pt.z); 
	return v;
}
syVector syVector::operator-( const syVector &pt ) const { 
	syVector v(x-pt.x, y-pt.y, z-pt.z); 
	return v;
}
syVector syVector::operator*( const syVector &pt ) const { 
	syVector v(x*pt.x, y*pt.y, z*pt.z);
	return v;
}
syVector syVector::operator/( const syVector &pt ) const { 
	syVector v(x/pt.x, y/pt.y, z/pt.z); 
	return v;
}
syVector syVector::operator+(const float &n) const { 
	syVector v(x+n, y+n, z+n); 
	return v;
}
syVector syVector::operator-(const float &n) const { 
	syVector v(x-n, y-n, z-n); 
	return v;
}
syVector syVector::operator*(const float &n) const { 
	syVector v(x*n, y*n, z*n); 
	return v;
}
syVector syVector::operator/(const float &n) const { 
	syVector v(x/n, y/n, z/n); 
	return v;
}

// Assignment operators
syVector& syVector::operator=(const syVector &pt) {x = pt.x; y = pt.y; z = pt.z; return *this; }
syVector& syVector::operator+=( const syVector &pt ) { x+=pt.x; y+=pt.y; z+=pt.z; return *this; }
syVector& syVector::operator-=( const syVector &pt ) { x-=pt.x; y-=pt.y; z-=pt.z; return *this; }
syVector& syVector::operator*=( const syVector &pt ) { x*=pt.x; y*=pt.y; z*=pt.z; return *this; }
syVector& syVector::operator/=( const syVector &pt ) { x/=pt.x; y/=pt.y; z/=pt.z; return *this; }
syVector& syVector::operator+=(const float &n) { x+=n; y+=n; z+=n; return *this; }
syVector& syVector::operator-=(const float &n) { x-=n; y-=n; z-=n; return *this; }
syVector& syVector::operator*=(const float &n) { x*=n; y*=n; z*=n; return *this; }
syVector& syVector::operator/=(const float &n) { x/=n; y/=n; z/=n; return *this; }
// Test operators
bool syVector::operator==( const syVector& pt ) const { return ( (pt.x==x) && (pt.y==y) && (pt.z==z) ); }
bool syVector::operator!=( const syVector& pt ) const { return ( (pt.x!=x) || (pt.y!=y) || (pt.z!=z) ); }
