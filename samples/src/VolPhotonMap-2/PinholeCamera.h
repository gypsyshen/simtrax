#ifndef _PCamera_H
#define _PCamera_H

#include "trax.hpp"
#include "syVector.h"
#include "Ray.h"


class PinholeCamera {
private:
	syVector eye, up, gaze;
	float u_len; // tan( fov/2 );

	syVector u, v;

	float inv_xres, inv_yres;
	
public:
	// initialize camera from memory
	PinholeCamera(int _addr, const int &_x_res, const int &_y_res);
	//
	PinholeCamera();
	PinholeCamera(const syVector &_eye, const syVector &_up, const syVector &_gaze, const float &_ulen, const int &_xres, const int &_yres);

	void Set(const syVector &_eye, const syVector &_up, const syVector &_gaze, const float &_ulen, const int &_x_res, const int &_y_res);
	void Set(int _addr, const int &_xres, const int &_yres);

	Ray GenerateRay(const int &_x, const int &_y) const;
	Ray GenerateSampleRay(const int &_x, const int &_y) const;

	// load from memory
	void LoadFromMemory(const int &_addr);
};

#endif