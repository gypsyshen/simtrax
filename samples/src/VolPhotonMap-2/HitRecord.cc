#include "HitRecord.h"
#include "Utility.h"

// -------------------- HitDetails --------------------
HitDetails::HitDetails() { Init(); };

void HitDetails::Init() {
	
	frontface = false;
	
	P = syVector(0.f, 0.f, 0.f);
	N = syVector(0.f, 0.f, 0.f);
	V = syVector(0.f, 0.f, 0.f);
	
	Kd = Color(1.f, 1.f, 1.f);
	Ks = Color(1.f, 1.f, 1.f);
	Kt = Color(1.f, 1.f, 1.f);
	
	d = 0.f;	
	R = 0.f;
	
	diffusion = false;
	reflection = false;
	transmission = false;
}

HitDetails& HitDetails::operator=(const HitDetails &_hitDetails) {
	
	frontface = _hitDetails.frontface;

	P = _hitDetails.P;
	N = _hitDetails.N;
	V = _hitDetails.V;
	
	Kd = _hitDetails.Kd;
	Ks = _hitDetails.Ks;
	Kt = _hitDetails.Kt;
	
	d = _hitDetails.d;
	R = _hitDetails.R;
	
	diffusion = _hitDetails.diffusion;
	reflection = _hitDetails.reflection;
	transmission = _hitDetails.transmission;
	
	return *this;
}

// -------------------- HitRecord --------------------
HitRecord::HitRecord() {
	Init();
}

void HitRecord::Init() {
	t = BIGFLOAT;
	id_mat = -1;
	id_obj = -1;

	details.Init();
}

bool HitRecord::Hit() const {

	return ( t == BIGFLOAT ) ? false : true;
}

HitRecord& HitRecord::operator=(const HitRecord &_hit) {

	t = _hit.t;
	id_mat = _hit.id_mat;
	id_obj = _hit.id_obj;
	details = _hit.details;

	return *this;
}

// -------------------- HitDist --------------------
void HitDist::Init() { t = BIGFLOAT; }
HitDist:: HitDist() { Init(); }
