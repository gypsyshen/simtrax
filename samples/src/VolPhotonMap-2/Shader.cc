#include "Shader.h"

// ----------------------------- public functions ----------------------------- 

Color Shader::GetColor(const Ray &ray, const Scene &scene, const PhotonMap &volpm) const {

	
	Color L = Color(0.f, 0.f, 0.f);


	////////////////////////////////////////////////////
	/// Trace ray
	////////////////////////////////////////////////////

	HitRecord hit;
	hit.Init();
	scene.TraceRay(ray, hit);

	
	////////////////////////////////////////////////////
	/// Pass 1: direct illumination
	////////////////////////////////////////////////////

	L += DirectIllumination(ray, hit, scene, volpm);
	
	
	////////////////////////////////////////////////////
	/// TODO Pass 2: indirect illumination
	////////////////////////////////////////////////////

//	if( hit.Hit() ) result += IndirectShade();
	
	return L;
}

// ----------------------------- private functions -----------------------------

Color Shader::DirectIllumination( const Ray &ray, const HitRecord &hit, const Scene &scene, const PhotonMap &volpm) const {

	Color L;

	if( !scene.volumeon ) { // without volume

		if( !hit.Hit() ) L = scene.background;
		else L = DirectShadeSurfaceWithoutVolume(ray, hit, scene);

	} else { // with volume

		Color L0;
		if( !hit.Hit() ) L0 = scene.background;
		else L0 = DirectShadeSurfaceWithVolume(ray, hit, scene);

		L = RadianceWithVolume(ray, hit, L0, scene, volpm);
	}

	return L;
}

// volume rendering

Color Shader::Illuminate(const syVector &x, const syVector &x0, const Color &L0, const Scene &scene) const {

	// step for emission
	syVector detX = x - x0;
	syVector stepX;
	int numStep = util::stepVector(stepX, util::STEP_LEN_VOLUME, detX);
	// emitted
	Color emitted = Emitted(numStep, x0, stepX, scene);
	// attenuated 
	Color attenuated = Attenuated(detX, L0, scene);

	//
	return emitted + attenuated;
}

// Assumption: ray arrives p directly
Color Shader::Illuminate(const Ray &ray, const syVector &p, const Color &L0, const Scene &scene) const {

	Color L;
	HitDist t_box_near, t_box_far;
	if( scene.volume.IntersectsW(ray, t_box_near, t_box_far) ) { // TODO: volume.intersect --> IntersectVolume
		syVector x0, x;
		if(t_box_near.t > 0) x = ray.p + ray.dir * t_box_near.t;
		else x = ray.p;
		x0 = p;
		// ray intersects with volume
		L = Illuminate(x, x0, L0, scene);

	} else {
		L = L0;
	}

	return L;

}

Color Shader::Illuminate(const syVector &x, const Scene &scene) const {

	syVector dir = x - scene.light.Position();
	Ray ray(scene.light.Position(), dir);
	HitRecord hit;
	hit.Init();
	scene.TraceRay(ray, hit);

	// light ray hit nothing, the light arrives x directly
	if( !hit.Hit() ) return Illuminate(ray, x, scene.light.Intensity(), scene);

	// the distant from light source to x
	float dist = dir.Length();

	// volumetric shadowing: occluded by surface ( diffuse, specular or transparent )
	if( hit.Hit() ) {
		if(hit.t < dist + FLOAT_BIAS) {
			return Color(0.f);
		}
	}

	// light ray hits point x, the light arrives x directly
	return Illuminate(ray, x, scene.light.Intensity(), scene);

}

Color Shader::Emitted(int numStep, const syVector &x0, const syVector &stepX, const Scene &scene) const {

	syVector x = x0 + stepX * numStep;
	
	Color integTau = Color(0.f, 0.f, 0.f);
	for(int k = 0; k < numStep; ++k) {
		syVector currX = x0 + stepX * k;
		integTau += scene.volume.Tau(x - currX);
	}
	Color emitted = integTau * scene.volume.alpha * scene.volume.le * stepX.Length();

	return emitted;
}

Color Shader::Attenuated(const syVector &detX, const Color &L, const Scene &scene) const {

	return scene.volume.Tau(detX) * L;
}

/// photon gather is applied here
Color Shader::Inscattered(int numStep, const syVector &x0, const syVector &x, const syVector &stepX, const Scene &scene, const PhotonMap &volpm) const {

	Color tau_L_di = Color(0.f, 0.f, 0.f);
	Color tau_L_ii = Color(0.f, 0.f, 0.f);

	// radius for gathering photons
	float r = util::RADIUS_GATHER_PHOTON;

	for(int k = 0; k < numStep; ++k) {

		syVector currX = x0 + stepX * k;
		Color tau = scene.volume.Tau( currX, x );

		// direct illumination
		Color I_di = Illuminate(currX, scene);
		if(I_di.r != 0.f || I_di.g != 0.f || I_di.b != 0.f) {
			tau_L_di += tau * I_di;
		}

		// indirect illumination
		Color I_ii = volpm.GatherPhotons(currX, r) * 3.f * scene.volume.phase / ( scene.volume.IRho * 4.f * M_PI * r * r * r );
		tau_L_ii += tau * I_ii;
	}

	float lenStepX = stepX.Length();
	Color di = scene.volume.rho * tau_L_di * scene.volume.phase * lenStepX;
	Color ii = scene.volume.rho * tau_L_ii * scene.volume.phase * lenStepX;

	Color inscattered = di + ii * scene.volume.albedo;
	
	return inscattered;
}

// Assumption: x and x0 are both in the volume
Color Shader::Radiance(const syVector &x, const syVector &x0, const Color &L0, const Scene &scene, const PhotonMap &volpm) const {

	// step for emission and inscatter
	syVector detX = x - x0;
	syVector stepX;
	int numStep = util::stepVector(stepX, util::STEP_LEN_VOLUME, detX);
	// emitted
	Color emitted = Emitted(numStep, x0, stepX, scene);
	// inscattered
	Color inscattered = Inscattered(numStep, x0, x, stepX, scene, volpm);
	// attenuated
	Color attenuated = Attenuated(detX, L0, scene);

	// 
	Color radiance = emitted + attenuated + inscattered;

	return radiance;

}

// estimate the radiance at the hit point in scenes with participating media
Color Shader::RadianceWithVolume(const Ray &ray, const HitRecord &hit, const Color &L0, const Scene &scene, const PhotonMap &volpm) const {

	Color L;
	HitDist t_box_near, t_box_far;
	if( scene.volume.IntersectsW(ray, t_box_near, t_box_far) ) { // TODO: volume.intersect --> IntersectVolume
		syVector x0, x;
		if(t_box_near.t > 0) x = ray.p + ray.dir * t_box_near.t;
		else x = ray.p;
		if( !hit.Hit() ) x0 = ray.p + ray.dir * t_box_far.t;
		else x0 = ray.p + ray.dir * hit.t;
		// ray intersects with volume
		L = Radiance(x, x0, L0, scene, volpm);

	} else {
		L = L0;
	}

	return L;
}

// surface rendering

// Assumption: hit.Hit() == true
Color Shader::DirectShadeSurfaceWithoutVolume(const Ray &_ray, const HitRecord &_hit, const Scene &scene) const {

	Ray ray = _ray;
	HitRecord hit = _hit;

	scene.ComputeHitDetails(hit, ray);
	
	Color result(0.f, 0.f, 0.f);

	// diffuse
	if(hit.details.diffusion) result += GetDiffuseColorWithoutVolume(ray, hit.details, scene);
	// reflection
	else if(hit.details.reflection) {

		int bounceR = 0;
		Color Ks = hit.details.Ks;
		while(bounceR++ < MAXBOUNCE_R) {
			
			hit.Init();
			scene.TraceRay(ray, hit);

			// terminate when hitting nothing
			if(!hit.Hit()) {
				result += scene.background * Ks;
				break;
			}
			
			scene.ComputeHitDetails(hit, ray);
			
			if(hit.details.diffusion) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Ks;
				break;
			}
			if(hit.details.reflection) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Ks;
				Ks *= hit.details.Ks;
				ray = scene.CastReflectRay(ray, hit.details);
				continue;
			}
			if(hit.details.transmission) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Ks;
				Ray newRay;
				if( scene.CastRefractRay(newRay, ray, hit.details) ) {
					Ks *= hit.details.Kt * (1.f - hit.details.R);
					ray = newRay;
					continue;
				} else {
					Ks *= hit.details.Ks + hit.details.Kt * hit.details.R;
					ray = scene.CastReflectRay(ray, hit.details);
					continue;
				}
			}
		}
	}
	// refraction
	else if(hit.details.transmission) {
		int bounceT = 0;
		Color Kt = hit.details.Kt * (1.f - hit.details.R);
		while(bounceT++ < MAXBOUNCE_T) {
			
			hit.Init();
			scene.TraceRay(ray, hit);

			// terminate when hitting nothing
			if(!hit.Hit()) {
				result += scene.background * Kt;
				break;
			}

			scene.ComputeHitDetails(hit, ray);

			if(hit.details.diffusion) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Kt;
				break;
			}
			if(hit.details.reflection) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Kt;
				Kt *= hit.details.Ks;
				ray = scene.CastReflectRay(ray, hit.details);
				continue;
			}
			if(hit.details.transmission) {
				result += GetDiffuseColorWithoutVolume(ray, hit.details, scene) * Kt;
				Ray newRay;				
				if( scene.CastRefractRay(newRay, ray, hit.details) ) { // refraction
					Kt *= hit.details.Kt * (1.f - hit.details.R);
					ray = newRay;
					continue;
				} else { // complete reflection
					Kt *= hit.details.Ks + hit.details.Kt * hit.details.R;
					ray = scene.CastReflectRay(ray, hit.details);
					continue;
				}
			}
		}
	}
	
	return result;

}

// TODO
// Assumption: hit.Hit() == true
Color Shader::DirectShadeSurfaceWithVolume(const Ray &_ray, const HitRecord &_hit, const Scene &scene) const {

	Ray ray = _ray;
	HitRecord hit = _hit;

	scene.ComputeHitDetails(hit, ray);
	
	Color result(0.f, 0.f, 0.f);

	// always count diffuse color
	if(hit.details.diffusion) result += GetDiffuseColorWithVolume(ray, hit, scene);
	// reflection
	if(hit.details.reflection) {

		int bounceR = 0;
		Color Ks = hit.details.Ks;
		while(bounceR++ < MAXBOUNCE_R) {
			
			hit.Init();
			scene.TraceRay(ray, hit);

			// terminate when hitting nothing
			if(!hit.Hit()) {
				result += scene.background * Ks;
				break;
			}
			
			scene.ComputeHitDetails(hit, ray);
			
			if(hit.details.diffusion) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Ks;
				break;
			}
			if(hit.details.reflection) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Ks;
				Ks *= hit.details.Ks;
				ray = scene.CastReflectRay(ray, hit.details);
				continue;
			}
			if(hit.details.transmission) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Ks;
				Ray newRay;
				if( scene.CastRefractRay(newRay, ray, hit.details) ) {
					Ks *= hit.details.Kt * (1.f - hit.details.R);
					ray = newRay;
					continue;
				} else {
					Ks *= hit.details.Ks + hit.details.Kt * hit.details.R;
					ray = scene.CastReflectRay(ray, hit.details);
					continue;
				}
			}
		}
	}
	// refraction
	if(hit.details.transmission) {
		int bounceT = 0;
		Color Kt = hit.details.Kt * (1.f - hit.details.R);
		while(bounceT++ < MAXBOUNCE_T) {
			
			hit.Init();
			scene.TraceRay(ray, hit);

			// terminate when hitting nothing
			if(!hit.Hit()) {
				result += scene.background * Kt;
				break;
			}

			scene.ComputeHitDetails(hit, ray);

			if(hit.details.diffusion) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Kt;
				break;
			}
			if(hit.details.reflection) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Kt;
				Kt *= hit.details.Ks;
				ray = scene.CastReflectRay(ray, hit.details);
				continue;
			}
			if(hit.details.transmission) {
				result += GetDiffuseColorWithVolume(ray, hit, scene) * Kt;
				Ray newRay;				
				if( scene.CastRefractRay(newRay, ray, hit.details) ) { // refraction
					Kt *= hit.details.Kt * (1.f - hit.details.R);
					ray = newRay;
					continue;
				} else { // complete reflection
					Kt *= hit.details.Ks + hit.details.Kt * hit.details.R;
					ray = scene.CastReflectRay(ray, hit.details);
					continue;
				}
			}
		}
	}
	
	return result;

}


Color Shader::GetDiffuseColorWithVolume(const Ray &ray, const HitRecord &hit, const Scene &scene) const {

	Color result(0.f, 0.f, 0.f);

	const HitDetails &hitDetails = hit.details;
	const syVector &P = hitDetails.P;
	const syVector &N = hitDetails.N;
	const Color &Kd = hitDetails.Kd;

	syVector L = scene.light.Direction(P); // hit point -> light source, TODO: scene.LightDir(P)
	float dist = L.Length();
	L.Normalize();
	// trace shadow
	Ray ray_shadow(P + N*FLOAT_BIAS, L);
	HitDist hit_shadow;
	scene.TraceShadowRay(ray_shadow, hit_shadow);
	// not in shadow: direct illumination
	if(dist <= hit_shadow.t) {
		float cosphi = max(N%L, 0.f);
		Ray litRay = Ray(scene.light.Position(), -L);
		Color I = Illuminate(litRay, P, scene.light.Intensity(), scene);
		result += Kd * I * cosphi;
	}
	
	return result;
}

Color Shader::GetDiffuseColorWithoutVolume(const Ray &ray, const HitDetails &hitDetails, const Scene &scene) const {

	Color result(0.f, 0.f, 0.f);
	
	const syVector &P = hitDetails.P;
	const syVector &N = hitDetails.N;
	const Color &Kd = hitDetails.Kd;

	syVector L = scene.light.Direction(P); // hit point -> light source, TODO
	float dist = L.Length();
	L.Normalize();
	// trace shadow
	Ray ray_shadow(P + N*FLOAT_BIAS, L);
	HitDist hit_shadow;
	scene.TraceShadowRay(ray_shadow, hit_shadow);
	// not in shadow: direct illumination
	if(dist <= hit_shadow.t) {
		float cosphi = max(N%L, 0.f);
		result += Kd * scene.light.Intensity() * cosphi; // TODO
	}
	
	return result;
}
