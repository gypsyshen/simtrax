#ifndef _HitRecord_H
#define _HitRecord_H

#include "trax.hpp"
#include "syVector.h"
#include "Color.h"

class HitDetails {

public:
	bool frontface;
	
	syVector P, N, V; // V: hitpoint -> camera
	
	Color Kd, Ks, Kt; // diffuse, specular, transmissive color
	
	float d; // index of refraction
	float R; // reflection weight
	bool diffusion, reflection, transmission, media;

public:
	HitDetails();
	void Init();
	HitDetails& operator=(const HitDetails &_hitDetails);
};

class HitRecord {
public:
	float t;
	int id_mat;
	int id_obj;
	HitDetails details;

	HitRecord();

	void Init();
	bool Hit() const;

	HitRecord& operator=(const HitRecord &_hit);
};

class HitDist {
public:
	float t;

	void Init();
	HitDist();
};

#endif
