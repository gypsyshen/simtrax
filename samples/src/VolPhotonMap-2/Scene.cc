#include "Scene.h"
#include "Utility.h"
#include "syBVHNode.h"

// -------------------------------- constructors ---------------------------------
Scene::Scene() { InitScene(false); }
Scene::Scene(bool _volumeon) { InitScene(_volumeon); }

// -------------------------------- public functions ---------------------------------

// init the scene
void Scene::InitScene(bool _volumeon) {

	// load the scene from the global memory
	Load();
	// whether to turn on the participating media volume
	volumeon = _volumeon;
	if(volumeon) InitVolume();
}

bool Scene::RussionRouletteMedia(const syVector &x) const {

	float rho = ( volume.rho.r + volume.rho.g + volume.rho.b ) * INV_3F;
	float alpha = ( volume.alpha.r + volume.alpha.g + volume.alpha.b ) * INV_3F;

	float albedo = rho / (rho + alpha);

	float rand = trax_rand();

	// absorbed
	if( rand > albedo )return true;
	// scattered
	return false;
}

bool Scene::RussionRouletteSurface( const HitDetails &hitDetails ) const {

	const Color &Kd = hitDetails.Kd;
	const Color &Ks = hitDetails.Ks;
	const Color &Kt = hitDetails.Kt;

	float D = ( Kd.r + Kd.g + Kd.b ) * INV_3F;
	float S = ( Ks.r + Ks.g + Ks.b ) * INV_3F;
	float T = ( Kt.r + Kt.g + Kt.b ) * INV_3F;
	float sumKdst = D + S + T;

	float rand = trax_rand();

	// absorbed
	if( rand > sumKdst ) {
//		printf("absorbed by a surface");
		return true;
	}
	// diffuse reflection, specular reflection, or transmissive
//	printf("reflected by a surface ");
	return false;
}

Ray Scene::ScatterRayInMedia ( const syVector &x ) const {

	return SphereRay( x );
}

Ray Scene::ScatterRayAtSurface ( const Ray &ray, const HitDetails &hitDetails ) const {

	Ray newRay;
	
	// diffuse reflection
	if( hitDetails.diffusion ) {
//		printf("scatter ray in a DIFFUSE surface ");
		newRay = HemiRay( hitDetails.P, hitDetails.N );
	}
	// specular reflection
	else if( hitDetails.reflection ) {
//		printf("scatter ray in a SPECULAR surface ");
		newRay = CastReflectRay(ray, hitDetails);
	}
	// transmission
	else if( hitDetails.transmission ) {
//		printf("scatter ray in a TRANSMISSIVE surface ");
		if( !CastRefractRay(newRay, ray, hitDetails) ) {
			newRay = CastReflectRay( ray, hitDetails );
		}
	}

	return newRay;
}

// Assumption: hit.Init();
void Scene::TraceRayWithVolume( const Ray &ray, HitRecord &hit, float stepsize) const {

	// trace surfaces in the scene
	TraceRay( ray, hit );
	// compute hit details
	if( hit.Hit() ) ComputeHitDetails(hit, ray);

	// if the origin of the ray is in the volume
	if( volume.Contains( ray.p ) ) {

		// if hits a surface: check whether the photon hits the media before it hits the surface ( stepsize < hit.t )
		if( hit.Hit() ) {
			// frontface
			if( hit.details.frontface ) {
//				printf("hit a frontface O~~~O\n");
				// if the photon hits the media before hitting the surface
				if( stepsize < hit.t ) {
					if( volume.Contains( ray.p + ray.dir * stepsize ) ) {
						ComputeHitDetailsInMedia(hit, ray, stepsize);
					}
				}
			}
//			else printf( "hit backface 0~~~0\n" );
		}
		// if doesn't hit a surface
		else {
			// if the photon goes out the volume and hits nothing, discard
			if( !volume.Contains( ray.p + ray.dir * stepsize ) ) {
				hit.Init();
			}
			// then the photon hits the media
			else {
				ComputeHitDetailsInMedia( hit, ray, stepsize );
			}
		}
	}
	// if the origin of the ray is NOT in the volume, check whether the photon hits the volume before hitting the surface
	else {
		HitDist t_box_near;

		// when the ray intersects with the volume
		if( volume.IntersectsW(ray, t_box_near) ) {
			// if the photon intersects with the volume before it hits the surface, it hits the media
			if( hit.Hit() ) {
				if( hit.details.frontface ) {
					if( t_box_near.t < hit.t ) {
						ComputeHitDetailsInMedia(hit, ray, t_box_near.t);
					}
				}
			}
			// then the photon directly intersects with the volume, it hits the media
			else {
				ComputeHitDetailsInMedia(hit, ray, t_box_near.t);				
			}
		}		
	}

}

// -------------------------------- private functions ---------------------------------

void Scene::ComputeHitDetailsInMedia( HitRecord &hit, const Ray &ray, float dist ) const {

	hit.Init();
	hit.t = dist;
	hit.details.media = true;
	hit.details.diffusion = false;
	hit.details.reflection = false;
	hit.details.transmission = false;
	hit.details.P = ray.p + ray.dir * hit.t;
}

// Assumption: volumeon == true
void Scene::InitVolume() {

	// initialize the region: slightly bigger than the cornell box
	int root_addr = BVHNodeAddr(0);
	syBVHNode root(root_addr);
	syBox region = root.GetRegion();
	
	syVector diagonal = region.bound[1] - region.bound[0];
	diagonal.Normalize();

	syVector _min = region.bound[0];// - diagonal;
	syVector _max = region.bound[1];// + diagonal;
	
	// void Set(const syVector &_min, const syVector &_max, const Color &_alpha, const Color &_rho, const Color &_le);
	Color absorption = Color(0.004f); // alpha
	Color scattering = Color(0.04f); // rho
	Color emission = Color(0.0f); // le
	volume.Set(_min, _max, absorption, scattering, emission);
}

// load the scene from the global memory
void Scene::Load() {

	// lights
	light.LoadFromMemory( GetLight() );
	light.SetIntensity( Color(20.f) );

	// triangles
	start_tri = GetStartTriangles();
	n_tri = GetNumTriangles();
	
	// materials
	start_mat = GetMaterials();

	// BVH tree
	start_bvh = GetBVH();

	// background
//	background.LoadFromMemory( GetBackground() );
	background = Color(0.561f, 0.729f, 0.988f);
}

int Scene::BVHNodeAddr(const int &node_id) const {
	return start_bvh + node_id * BVHNODE_SIZE_IN_TRAX;
}

// assumption: hit.Init()
void Scene::TraceRay(const Ray &ray, HitRecord &hit) const{
	
	int nodeIDs[32];
	int sp = 0; // stack pointer
	nodeIDs[sp++] = 0;
	HitDist t_box_near;
	while( sp > 0 ) {
		int node_id = nodeIDs[--sp];
		int node_addr = BVHNodeAddr(node_id);
		syBVHNode node(node_addr);
		// 1 -- no intersection
		if(!node.IntersectBoxW(ray, t_box_near )) continue;
		if ( hit.t < t_box_near.t ) continue;
		// 2 -- interior node
		if(node.Interior()) {
			// left bvh node
			int left_id = node.LeftChildID();
			int left_addr = BVHNodeAddr(left_id);
			syBVHNode left_node(left_addr);
			HitDist t_left_near;
			bool intersect_left = left_node.IntersectBoxW(ray, t_left_near);
			// right bvh node
			int right_id = node.RightChildID();
			int right_addr = BVHNodeAddr(right_id);
			syBVHNode right_node(right_addr);
			HitDist t_right_near;
			bool intersect_right = right_node.IntersectBoxW(ray, t_right_near);
			if( !(intersect_left || intersect_right) ) {
				continue;
			} else if(intersect_left && !intersect_right) {
				nodeIDs[sp++] = left_id;
			} else if(intersect_right && !intersect_left) {
				nodeIDs[sp++] = right_id;
			} else {
				if(t_left_near.t < t_right_near.t) {
					nodeIDs[sp++] = right_id;
					nodeIDs[sp++] = left_id;
				} else {
					nodeIDs[sp++] = left_id;
					nodeIDs[sp++] = right_id;
				}
			}

			continue;
		}
		// 3 -- leaf node		
		node.TraceRay(ray, hit);

	}

}


void Scene::TraceShadowRay(const Ray &ray, HitDist &hit_shadow) const {
	
	HitDist t_box_near;
	int nodeIDs[32];
	int sp = 0; //stack pointer
	nodeIDs[sp++] = 0;
	while( sp > 0 ) {
		int node_id = nodeIDs[--sp];
		int node_addr = BVHNodeAddr(node_id);
		syBVHNode node(node_addr);
		// 1 -- no intersection with the box
		if(!node.IntersectBoxW(ray, t_box_near)) continue;
		if ( hit_shadow.t < t_box_near.t ) continue;
		// 2 -- interior node
		if(node.Interior()) {
			// left bvh node
			int left_id = node.LeftChildID();
			int left_addr = BVHNodeAddr(left_id);
			syBVHNode left_node(left_addr);
			HitDist t_left_near;
			bool intersect_left = left_node.IntersectBoxW(ray, t_left_near);
			// right bvh node
			int right_id = node.RightChildID();
			int right_addr = BVHNodeAddr(right_id);
			syBVHNode right_node(right_addr);
			HitDist t_right_near;
			bool intersect_right = right_node.IntersectBoxW(ray, t_right_near);
			if( !(intersect_left || intersect_right) ) {
				continue;
			} else if(intersect_left && !intersect_right) {
				nodeIDs[sp++] = left_id;
			} else if(intersect_right && !intersect_left) {
				nodeIDs[sp++] = right_id;
			} else {
				if(t_left_near.t < t_right_near.t) {
					nodeIDs[sp++] = right_id;
					nodeIDs[sp++] = left_id;
				} else {
					nodeIDs[sp++] = left_id;
					nodeIDs[sp++] = right_id;
				}
			}
			continue;
		}
		// 3 -- leaf node
		node.TraceShadowRay(ray, hit_shadow);
	}
}

Ray Scene::HemiRay(const syVector &P, const syVector &N) const {
	// Returns a a random vector in the hemisphere defined by primary axis v
	// Direction probability is weighted by the cosine of the angle between v and the returned vector

	syVector refDir(0.0f, 0.0f, 0.0f);

	// pick random point on disk
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	float x_2 = 0.0f; // squares
	float y_2 = 0.0f;
	do // random point on [-1, 1] square, until radius <= 1 (thus, on unit disc)
	{
		x = trax_rand(); 
		y = trax_rand();
		x = x * 2.0f;
		x = x - 1.0f;
		y = y * 2.0f;
		y = y - 1.0f;
		x_2 = x * x;
		y_2 = y * y;
	}
	while((x_2 + y_2) >= 1.0f); // cut out points outside the disk
  
	// we have [x,y] on disc plane, z = project up to unit hemisphere, radius must be 1
	z = sqrt(1.0f - x_2 - y_2); 

	syVector X = N.GetOrtho(); // get any vector orthogonal to v
	syVector Y = N.Cross(X); // get the vector orthogonal to v and X
	// X, Y, v now form an orthonormal basis

	// hemisphere direction is random point on disc projected up to unit hemisphere
	refDir = (X * x) + (Y * y) + (N * z);
	refDir.Normalize();
  
	Ray ray;
	ray.Set(P, refDir);

	return ray;
}

Ray Scene::SphereRay(const syVector &x) const {

	
	syVector dir = syVector( trax_rand() - 0.5f, trax_rand() - 0.5f, trax_rand()-0.5f );
	Ray ray = Ray( x, dir );

	return ray;
}


void Scene::ComputeHitDetails(HitRecord &hit, const Ray &ray) const {
	
	// view direction
	syVector V = ray.dir * (-1.f);
	hit.details.V = V;
	
	// hit position
	syVector P = ray.p + ray.dir * hit.t;
	hit.details.P = P;
	
	// normal
	syTriangle tri(hit.id_obj);
	syVector N = tri.Normal();
	hit.details.N = N;
	
	// check front face hitting
	if(N % V < 0.f) hit.details.frontface = false;
	else hit.details.frontface = true;
	
	// material
	int addr_mat = start_mat + hit.id_mat * MAT_SIZE_IN_TRAX;
	// illum
	int illum = loadi(addr_mat + ILLUM_OFFSET_IN_TRAX);
	// diffuse, specular, transmission
	hit.details.Kd = Color(addr_mat + KD_OFFSET_IN_TRAX);
	if(illum == ILLUM_D) { // diffuse
		hit.details.diffusion = true;
		hit.details.reflection = false;
		hit.details.transmission = false;
		hit.details.media = false;

		hit.details.Ks = Color(0.f);
		hit.details.Kt = Color(0.f);

	} else if(illum == ILLUM_R) { // specular
		hit.details.reflection = true;
		hit.details.diffusion = false;
		hit.details.transmission = false;
		hit.details.media = false;

		hit.details.Ks = Color( addr_mat + KS_OFFSET_IN_TRAX );
		hit.details.Kt = Color( 0.f );

	} else if(illum == ILLUM_T) {  // transmission + specular + diffuse
		hit.details.transmission = true;
		hit.details.diffusion = false;
		hit.details.reflection = false;
		hit.details.media = false;

		hit.details.Ks = Color( addr_mat + KS_OFFSET_IN_TRAX );
		hit.details.Kt = hit.details.Kd;

		hit.details.d = loadf(addr_mat + D_OFFSET_IN_TRAX);
		
		// Schlick's Approximation
		float n1, n2;
		if(hit.details.frontface) {
			n1 = 1.f;
			n2 = 0.6f;
		} else {
			n1 = 0.6f;
			n2 = 1.f;
		}
		float R0 = util::powf( (n1-n2)/(n1+n2), 2 );
		syVector L = light.Direction(P).GetNormalized();
		syVector H = (V + L).GetNormalized();
		hit.details.R = R0 + (1-R0) * util::powf( 1.f - H % V, 5 ); // Fresnell component for reflection
	}
}

bool Scene::CastRefractRay(Ray &rayT, const Ray &ray, const HitDetails &hitDetails) const {

	syVector P = hitDetails.P;
	syVector N = hitDetails.N;
	syVector V = hitDetails.V;

	float n1, n2;
	if(hitDetails.frontface) {
		n1 = 1.f;
		n2 = 0.6f;
	} else {
		n1 = 0.6f;//hitDetails.d;
		n2 = 1.f;
		N = N * (-1.f);
	}
	
	float eta, c1, cs2;
	
	eta = n1 / n2;
	c1 = V % N;
	cs2 = 1.f - eta * eta * (1.f - c1 * c1);
	
	if(cs2 < 0.f) return false; // complete reflection
	
	syVector dirT = V * (-1.f) * eta + N * (eta * c1 - sqrt(cs2));
	dirT.Normalize();
	rayT.Set(P + dirT * 0.005f, dirT);
	
	return true;
}

Ray Scene::CastReflectRay(const Ray &ray, const HitDetails &hitDetails) const {
	
	syVector P = hitDetails.P;
	syVector N = hitDetails.N;
	syVector V = hitDetails.V;

	float c1 = N % V;
	syVector dirR = -V + N * c1 * 2.f;

	Ray rayR;
	rayR.Set(P + N * FLOAT_BIAS, dirR);

	return rayR;
}
