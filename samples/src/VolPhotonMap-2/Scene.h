#ifndef _Scene_H
#define _Scene_H

#include "trax.hpp"
#include "sySphere.h"
#include "Lambertian.h"
#include "PointLight.h"
#include "Color.h"
#include "Utility.h"
#include "syTriangle.h"
#include "Volume.h"
//#include "HitRecord.h"

class Scene {

public:
	
	// lights
	PointLight light;

	// triangles
	int start_tri, n_tri;

	// materials
	int start_mat;

	// BVH tree
	int start_bvh;

	// participating media volume
	Volume volume;
	bool volumeon;

	// background color
	Color background;

	Scene();
	Scene(bool _volumeon);

private:

	void Load();
	void InitVolume();

	int BVHNodeAddr(const int &node_id) const;

	Ray HemiRay(const syVector &P, const syVector &N) const;
	Ray SphereRay(const syVector &x) const;

public:

	void InitScene(bool _volumeon);

	///////////////////////////////////////////////////
	/// Compute Hit Details
	///////////////////////////////////////////////////

	void ComputeHitDetails(HitRecord &hit, const Ray &ray) const;

	void ComputeHitDetailsInMedia( HitRecord &hit, const Ray &ray, float dist ) const;


	///////////////////////////////////////////////////
	/// Russian Roulette
	///////////////////////////////////////////////////

	// true for absorption
	bool RussionRouletteMedia( const syVector &x ) const;

	// true for absorption
	bool RussionRouletteSurface( const HitDetails &hitDetails ) const;


	///////////////////////////////////////////////////
	/// Trace Ray
	///////////////////////////////////////////////////

	void TraceRay(const Ray &ray, HitRecord &hit) const;

	void TraceShadowRay(const Ray &ray, HitDist &hit_shadow) const;

	void TraceRayWithVolume( const Ray &ray, HitRecord &hit, float stepsize) const;


	///////////////////////////////////////////////////
	/// Cast Ray
	///////////////////////////////////////////////////

	Ray CastReflectRay(const Ray &_ray, const HitDetails &_hitDetails) const;

	// false for complete reflection 
	bool CastRefractRay(Ray &rayT, const Ray &_ray, const HitDetails &_hitDetails) const;

	// generate a random ray in media 
	Ray ScatterRayInMedia ( const syVector &x ) const;

	// generate a random ray in surface
	Ray ScatterRayAtSurface ( const Ray &ray, const HitDetails &hitDetails ) const;


};

#endif
