#BEGIN Preamble
	REG	r0
	REG	r1
	REG	r2
	REG	r3
	REG	r4
	REG	r5
	REG	r6
	REG	r7
	REG	r8
	REG	r9
	REG	r10
	REG	r11
	REG	r12
	REG	r13
	REG	r14
	REG	r15
	REG	r16
	REG	r17
	REG	r18
	REG	r19
	REG	r20
	REG	r21
	REG	r22
	REG	r23
	REG	r24
	REG	r25
	REG	r26
	REG	r27
	REG	r28
	REG	r29
	REG	r30
	REG	r31
	LOADIMM	r0, 0
	ADDI	r1, r0, 8192
start:	brlid	r15, main
	NOP
	HALT
abort:	ADDI	r5, r0, -1
	PRINT	r5
	HALT
#END Preamble
#	.file	"rt.bc"
#	.text
#	.globl	_ZN5ColorC2Ev
#	.align	2
#	.type	_ZN5ColorC2Ev,@function
#	.ent	_ZN5ColorC2Ev           # @_ZN5ColorC2Ev
_ZN5ColorC2Ev:
#	.frame	r1,0,r15
#	.mask	0x0
	ADDI	r3,	r0,	1065353216
	SWI	r3,	r5,	0
	SWI	r3,	r5,	4
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2Ev
$0tmp0:
#	.size	_ZN5ColorC2Ev, ($tmp0)-_ZN5ColorC2Ev

#	.globl	_ZN5ColorC2ERKS_
#	.align	2
#	.type	_ZN5ColorC2ERKS_,@function
#	.ent	_ZN5ColorC2ERKS_        # @_ZN5ColorC2ERKS_
_ZN5ColorC2ERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	rtsd	r15,	8
	SWI	r3,	r5,	8
#	.end	_ZN5ColorC2ERKS_
$0tmp1:
#	.size	_ZN5ColorC2ERKS_, ($tmp1)-_ZN5ColorC2ERKS_

#	.globl	_ZN5ColoraSERKS_
#	.align	2
#	.type	_ZN5ColoraSERKS_,@function
#	.ent	_ZN5ColoraSERKS_        # @_ZN5ColoraSERKS_
_ZN5ColoraSERKS_:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r3,	r6,	0
	SWI	r3,	r5,	0
	LWI	r3,	r6,	4
	SWI	r3,	r5,	4
	LWI	r3,	r6,	8
	SWI	r3,	r5,	8
	rtsd	r15,	8
	ADD	r3,	r5,	r0
#	.end	_ZN5ColoraSERKS_
$0tmp2:
#	.size	_ZN5ColoraSERKS_, ($tmp2)-_ZN5ColoraSERKS_

#	.globl	_ZNK5Color1rEv
#	.align	2
#	.type	_ZNK5Color1rEv,@function
#	.ent	_ZNK5Color1rEv          # @_ZNK5Color1rEv
_ZNK5Color1rEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	LWI	r3,	r5,	0
#	.end	_ZNK5Color1rEv
$0tmp3:
#	.size	_ZNK5Color1rEv, ($tmp3)-_ZNK5Color1rEv

#	.globl	_ZNK5Color1gEv
#	.align	2
#	.type	_ZNK5Color1gEv,@function
#	.ent	_ZNK5Color1gEv          # @_ZNK5Color1gEv
_ZNK5Color1gEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	LWI	r3,	r5,	4
#	.end	_ZNK5Color1gEv
$0tmp4:
#	.size	_ZNK5Color1gEv, ($tmp4)-_ZNK5Color1gEv

#	.globl	_ZNK5Color1bEv
#	.align	2
#	.type	_ZNK5Color1bEv,@function
#	.ent	_ZNK5Color1bEv          # @_ZNK5Color1bEv
_ZNK5Color1bEv:
#	.frame	r1,0,r15
#	.mask	0x0
	rtsd	r15,	8
	LWI	r3,	r5,	8
#	.end	_ZNK5Color1bEv
$0tmp5:
#	.size	_ZNK5Color1bEv, ($tmp5)-_ZNK5Color1bEv

#	.globl	_ZN5ColorC2Efff
#	.align	2
#	.type	_ZN5ColorC2Efff,@function
#	.ent	_ZN5ColorC2Efff         # @_ZN5ColorC2Efff
_ZN5ColorC2Efff:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r6,	r5,	0
	SWI	r7,	r5,	4
	rtsd	r15,	8
	SWI	r8,	r5,	8
#	.end	_ZN5ColorC2Efff
$0tmp6:
#	.size	_ZN5ColorC2Efff, ($tmp6)-_ZN5ColorC2Efff

#	.globl	_ZN5ImageC2Eiii
#	.align	2
#	.type	_ZN5ImageC2Eiii,@function
#	.ent	_ZN5ImageC2Eiii         # @_ZN5ImageC2Eiii
_ZN5ImageC2Eiii:
#	.frame	r1,0,r15
#	.mask	0x0
	SWI	r6,	r5,	0
	SWI	r7,	r5,	4
	rtsd	r15,	8
	SWI	r8,	r5,	8
#	.end	_ZN5ImageC2Eiii
$0tmp7:
#	.size	_ZN5ImageC2Eiii, ($tmp7)-_ZN5ImageC2Eiii

#	.globl	_ZN5Image3setEiiRK5Color
#	.align	2
#	.type	_ZN5Image3setEiiRK5Color,@function
#	.ent	_ZN5Image3setEiiRK5Color # @_ZN5Image3setEiiRK5Color
_ZN5Image3setEiiRK5Color:
#	.frame	r1,0,r15
#	.mask	0x0
	LWI	r4,	r8,	0
	ORI	r3,	r0,	0
	FPLT	r10,	r4,	r3
	bneid	r10,	$0BB8_2
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB8_2:
	bneid	r9,	$0BB8_7
	NOP
	ORI	r3,	r0,	1065353216
	FPGE	r10,	r4,	r3
	bneid	r10,	$0BB8_5
	ADDI	r9,	r0,	1
	ADDI	r9,	r0,	0
$0BB8_5:
	bneid	r9,	$0BB8_7
	NOP
	ADD	r3,	r4,	r0
$0BB8_7:
	LWI	r9,	r8,	4
	ORI	r4,	r0,	0
	FPLT	r11,	r9,	r4
	bneid	r11,	$0BB8_9
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB8_9:
	bneid	r10,	$0BB8_14
	NOP
	ORI	r4,	r0,	1065353216
	FPGE	r11,	r9,	r4
	bneid	r11,	$0BB8_12
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB8_12:
	bneid	r10,	$0BB8_14
	NOP
	ADD	r4,	r9,	r0
$0BB8_14:
	LWI	r9,	r8,	8
	ORI	r8,	r0,	0
	FPLT	r11,	r9,	r8
	bneid	r11,	$0BB8_16
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB8_16:
	bneid	r10,	$0BB8_21
	NOP
	ORI	r8,	r0,	1065353216
	FPGE	r11,	r9,	r8
	bneid	r11,	$0BB8_19
	ADDI	r10,	r0,	1
	ADDI	r10,	r0,	0
$0BB8_19:
	bneid	r10,	$0BB8_21
	NOP
	ADD	r8,	r9,	r0
$0BB8_21:
	LWI	r9,	r5,	0
	MUL	r9,	r9,	r6
	ADD	r9,	r9,	r7
	MULI	r9,	r9,	3
	LWI	r10,	r5,	8
	ADD	r9,	r9,	r10
	STORE	r9,	r3,	0
	LWI	r3,	r5,	0
	MUL	r3,	r3,	r6
	ADD	r3,	r3,	r7
	MULI	r3,	r3,	3
	LWI	r9,	r5,	8
	ADD	r3,	r3,	r9
	STORE	r3,	r4,	1
	LWI	r3,	r5,	0
	MUL	r3,	r3,	r6
	ADD	r3,	r3,	r7
	MULI	r3,	r3,	3
	LWI	r4,	r5,	8
	ADD	r3,	r3,	r4
	STORE	r3,	r8,	2
	rtsd	r15,	8
	NOP
#	.end	_ZN5Image3setEiiRK5Color
$0tmp8:
#	.size	_ZN5Image3setEiiRK5Color, ($tmp8)-_ZN5Image3setEiiRK5Color

#	.globl	_Z9trax_mainv
#	.align	2
#	.type	_Z9trax_mainv,@function
#	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
#	.frame	r1,24,r15
#	.mask	0x3f00000
	ADDI	r1,	r1,	-24
	SWI	r20,	r1,	20
	SWI	r21,	r1,	16
	SWI	r22,	r1,	12
	SWI	r23,	r1,	8
	SWI	r24,	r1,	4
	SWI	r25,	r1,	0
	ADD	r5,	r0,	r0
	LOAD	r3,	r5,	7
	LOAD	r4,	r5,	1
	LOAD	r6,	r5,	4
	MUL	r5,	r6,	r4
	ATOMIC_INC	r10,	0
	CMP	r7,	r5,	r10
	bgeid	r7,	$0BB9_19
	NOP
	FPCONV	r6,	r6
	ORI	r7,	r0,	1056964608
	FPMUL	r6,	r6,	r7
	FPCONV	r8,	r4
	FPMUL	r7,	r8,	r7
	ORI	r9,	r0,	1068708659
	FPDIV	r8,	r8,	r9
$0BB9_2:
	DIV	r11,	r4,	r10
	MUL	r9,	r11,	r4
	RSUB	r10,	r9,	r10
	FPCONV	r12,	r10
	FPRSUB	r20,	r8,	r12
	FPCONV	r11,	r11
	FPRSUB	r11,	r6,	r11
	FPDIV	r12,	r11,	r6
	FPDIV	r20,	r20,	r7
	ORI	r21,	r0,	0
	ADD	r11,	r0,	r0
	ADD	r23,	r21,	r0
$0BB9_3:
	FPMUL	r22,	r21,	r21
	FPMUL	r24,	r23,	r23
	FPRSUB	r22,	r22,	r24
	FPADD	r22,	r22,	r20
	FPADD	r23,	r23,	r23
	FPMUL	r21,	r23,	r21
	FPADD	r21,	r21,	r12
	FPMUL	r23,	r21,	r21
	FPMUL	r24,	r22,	r22
	FPADD	r23,	r24,	r23
	ORI	r24,	r0,	1082130432
	FPGE	r25,	r23,	r24
	FPUN	r23,	r23,	r24
	BITOR	r24,	r23,	r25
	bneid	r24,	$0BB9_5
	ADDI	r23,	r0,	1
	ADDI	r23,	r0,	0
$0BB9_5:
	bneid	r23,	$0BB9_7
	ADDI	r11,	r11,	1
	ADDI	r23,	r0,	100
	CMP	r24,	r23,	r11
	bltid	r24,	$0BB9_3
	ADD	r23,	r22,	r0
$0BB9_7:
	ADDI	r12,	r0,	100
	CMP	r22,	r12,	r11
	ADDI	r21,	r0,	0
	ADDI	r20,	r0,	1
	beqid	r22,	$0BB9_9
	ADD	r12,	r20,	r0
	ADD	r12,	r21,	r0
$0BB9_9:
	FPCONV	r23,	r11
	ORI	r11,	r0,	0
	bneid	r12,	$0BB9_11
	ADD	r22,	r11,	r0
	ADD	r22,	r23,	r0
$0BB9_11:
	ORI	r12,	r0,	1120403456
	FPDIV	r12,	r22,	r12
	FPLT	r22,	r12,	r11
	bneid	r22,	$0BB9_13
	NOP
	ADD	r20,	r21,	r0
$0BB9_13:
	bneid	r20,	$0BB9_18
	NOP
	ORI	r11,	r0,	1065353216
	FPGE	r21,	r12,	r11
	bneid	r21,	$0BB9_16
	ADDI	r20,	r0,	1
	ADDI	r20,	r0,	0
$0BB9_16:
	bneid	r20,	$0BB9_18
	NOP
	ADD	r11,	r12,	r0
$0BB9_18:
	ADD	r9,	r9,	r10
	MULI	r9,	r9,	3
	ADD	r9,	r9,	r3
	STORE	r9,	r11,	0
	ORI	r10,	r0,	0
	STORE	r9,	r10,	1
	STORE	r9,	r10,	2
	ATOMIC_INC	r10,	0
	CMP	r9,	r5,	r10
	bltid	r9,	$0BB9_2
	NOP
$0BB9_19:
	LWI	r25,	r1,	0
	LWI	r24,	r1,	4
	LWI	r23,	r1,	8
	LWI	r22,	r1,	12
	LWI	r21,	r1,	16
	LWI	r20,	r1,	20
	rtsd	r15,	8
	ADDI	r1,	r1,	24
#	.end	_Z9trax_mainv
$0tmp9:
#	.size	_Z9trax_mainv, ($tmp9)-_Z9trax_mainv

#	.globl	main
#	.align	2
#	.type	main,@function
#	.ent	main                    # @main
main:
#	.frame	r1,4,r15
#	.mask	0x8000
	ADDI	r1,	r1,	-4
	SWI	r15,	r1,	0
	brlid	r15,	_Z9trax_mainv
	NOP
	ADD	r3,	r0,	r0
	LWI	r15,	r1,	0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	main
$0tmp10:
#	.size	main, ($tmp10)-main

#	.globl	_Z6printfPKcz
#	.align	2
#	.type	_Z6printfPKcz,@function
#	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
#	.frame	r1,4,r15
#	.mask	0x0
	ADDI	r1,	r1,	-4
	SWI	r10,	r1,	28
	SWI	r9,	r1,	24
	SWI	r8,	r1,	20
	SWI	r7,	r1,	16
	SWI	r6,	r1,	12
	SWI	r5,	r1,	8
	ADDI	r3,	r1,	8
	PRINTF	r3
	ADD	r3,	r0,	r0
	rtsd	r15,	8
	ADDI	r1,	r1,	4
#	.end	_Z6printfPKcz
$0tmp11:
#	.size	_Z6printfPKcz, ($tmp11)-_Z6printfPKcz


#	.globl	_ZN5ColorC1Ev
#_ZN5ColorC1Ev = _ZN5ColorC2Ev
#	.globl	_ZN5ColorC1ERKS_
#_ZN5ColorC1ERKS_ = _ZN5ColorC2ERKS_
#	.globl	_ZN5ColorC1Efff
#_ZN5ColorC1Efff = _ZN5ColorC2Efff
#	.globl	_ZN5ImageC1Eiii
#_ZN5ImageC1Eiii = _ZN5ImageC2Eiii
