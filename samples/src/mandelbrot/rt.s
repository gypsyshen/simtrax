	.file	"rt.bc"
	.text
	.globl	_ZN5ColorC2Ev
	.align	2
	.type	_ZN5ColorC2Ev,@function
	.ent	_ZN5ColorC2Ev           # @_ZN5ColorC2Ev
_ZN5ColorC2Ev:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r3, r0, 1065353216
	SWI       r3, r5, 0
	SWI       r3, r5, 4
	rtsd      r15, 8
	SWI       r3, r5, 8
	.end	_ZN5ColorC2Ev
$tmp0:
	.size	_ZN5ColorC2Ev, ($tmp0)-_ZN5ColorC2Ev

	.globl	_ZN5ColorC2ERKS_
	.align	2
	.type	_ZN5ColorC2ERKS_,@function
	.ent	_ZN5ColorC2ERKS_        # @_ZN5ColorC2ERKS_
_ZN5ColorC2ERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	rtsd      r15, 8
	SWI        r3, r5, 8
	.end	_ZN5ColorC2ERKS_
$tmp1:
	.size	_ZN5ColorC2ERKS_, ($tmp1)-_ZN5ColorC2ERKS_

	.globl	_ZN5ColoraSERKS_
	.align	2
	.type	_ZN5ColoraSERKS_,@function
	.ent	_ZN5ColoraSERKS_        # @_ZN5ColoraSERKS_
_ZN5ColoraSERKS_:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r3, r6, 0
	SWI        r3, r5, 0
	LWI        r3, r6, 4
	SWI        r3, r5, 4
	LWI        r3, r6, 8
	SWI        r3, r5, 8
	rtsd      r15, 8
	ADD      r3, r5, r0
	.end	_ZN5ColoraSERKS_
$tmp2:
	.size	_ZN5ColoraSERKS_, ($tmp2)-_ZN5ColoraSERKS_

	.globl	_ZNK5Color1rEv
	.align	2
	.type	_ZNK5Color1rEv,@function
	.ent	_ZNK5Color1rEv          # @_ZNK5Color1rEv
_ZNK5Color1rEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	LWI        r3, r5, 0
	.end	_ZNK5Color1rEv
$tmp3:
	.size	_ZNK5Color1rEv, ($tmp3)-_ZNK5Color1rEv

	.globl	_ZNK5Color1gEv
	.align	2
	.type	_ZNK5Color1gEv,@function
	.ent	_ZNK5Color1gEv          # @_ZNK5Color1gEv
_ZNK5Color1gEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	LWI        r3, r5, 4
	.end	_ZNK5Color1gEv
$tmp4:
	.size	_ZNK5Color1gEv, ($tmp4)-_ZNK5Color1gEv

	.globl	_ZNK5Color1bEv
	.align	2
	.type	_ZNK5Color1bEv,@function
	.ent	_ZNK5Color1bEv          # @_ZNK5Color1bEv
_ZNK5Color1bEv:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	rtsd      r15, 8
	LWI        r3, r5, 8
	.end	_ZNK5Color1bEv
$tmp5:
	.size	_ZNK5Color1bEv, ($tmp5)-_ZNK5Color1bEv

	.globl	_ZN5ColorC2Efff
	.align	2
	.type	_ZN5ColorC2Efff,@function
	.ent	_ZN5ColorC2Efff         # @_ZN5ColorC2Efff
_ZN5ColorC2Efff:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI        r6, r5, 0
	SWI        r7, r5, 4
	rtsd      r15, 8
	SWI        r8, r5, 8
	.end	_ZN5ColorC2Efff
$tmp6:
	.size	_ZN5ColorC2Efff, ($tmp6)-_ZN5ColorC2Efff

	.globl	_ZN5ImageC2Eiii
	.align	2
	.type	_ZN5ImageC2Eiii,@function
	.ent	_ZN5ImageC2Eiii         # @_ZN5ImageC2Eiii
_ZN5ImageC2Eiii:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	SWI       r6, r5, 0
	SWI       r7, r5, 4
	rtsd      r15, 8
	SWI       r8, r5, 8
	.end	_ZN5ImageC2Eiii
$tmp7:
	.size	_ZN5ImageC2Eiii, ($tmp7)-_ZN5ImageC2Eiii

	.globl	_ZN5Image3setEiiRK5Color
	.align	2
	.type	_ZN5Image3setEiiRK5Color,@function
	.ent	_ZN5Image3setEiiRK5Color # @_ZN5Image3setEiiRK5Color
_ZN5Image3setEiiRK5Color:
	.frame	r1,0,r15
	.mask	0x0
# BB#0:                                 # %entry
	LWI        r4, r8, 0
	ORI       r3, r0, 0
	FPLT   r10, r4, r3
	bneid     r10, ($BB8_2)
	ADDI      r9, r0, 1
# BB#1:                                 # %entry
	ADDI      r9, r0, 0
$BB8_2:                                 # %entry
	bneid     r9, ($BB8_7)
	NOP    
# BB#3:                                 # %cond.false
	ORI       r3, r0, 1065353216
	FPGE   r10, r4, r3
	bneid     r10, ($BB8_5)
	ADDI      r9, r0, 1
# BB#4:                                 # %cond.false
	ADDI      r9, r0, 0
$BB8_5:                                 # %cond.false
	bneid     r9, ($BB8_7)
	NOP    
# BB#6:                                 # %cond.false5
	ADD      r3, r4, r0
$BB8_7:                                 # %cond.end7
	LWI        r9, r8, 4
	ORI       r4, r0, 0
	FPLT   r11, r9, r4
	bneid     r11, ($BB8_9)
	ADDI      r10, r0, 1
# BB#8:                                 # %cond.end7
	ADDI      r10, r0, 0
$BB8_9:                                 # %cond.end7
	bneid     r10, ($BB8_14)
	NOP    
# BB#10:                                # %cond.false12
	ORI       r4, r0, 1065353216
	FPGE   r11, r9, r4
	bneid     r11, ($BB8_12)
	ADDI      r10, r0, 1
# BB#11:                                # %cond.false12
	ADDI      r10, r0, 0
$BB8_12:                                # %cond.false12
	bneid     r10, ($BB8_14)
	NOP    
# BB#13:                                # %cond.false16
	ADD      r4, r9, r0
$BB8_14:                                # %cond.end20
	LWI        r9, r8, 8
	ORI       r8, r0, 0
	FPLT   r11, r9, r8
	bneid     r11, ($BB8_16)
	ADDI      r10, r0, 1
# BB#15:                                # %cond.end20
	ADDI      r10, r0, 0
$BB8_16:                                # %cond.end20
	bneid     r10, ($BB8_21)
	NOP    
# BB#17:                                # %cond.false25
	ORI       r8, r0, 1065353216
	FPGE   r11, r9, r8
	bneid     r11, ($BB8_19)
	ADDI      r10, r0, 1
# BB#18:                                # %cond.false25
	ADDI      r10, r0, 0
$BB8_19:                                # %cond.false25
	bneid     r10, ($BB8_21)
	NOP    
# BB#20:                                # %cond.false29
	ADD      r8, r9, r0
$BB8_21:                                # %cond.end33
	LWI       r9, r5, 0
	MUL       r9, r9, r6
	ADD      r9, r9, r7
	MULI      r9, r9, 3
	LWI       r10, r5, 8
	ADD      r9, r9, r10
	STORE     r9, r3, 0
	LWI       r3, r5, 0
	MUL       r3, r3, r6
	ADD      r3, r3, r7
	MULI      r3, r3, 3
	LWI       r9, r5, 8
	ADD      r3, r3, r9
	STORE     r3, r4, 1
	LWI       r3, r5, 0
	MUL       r3, r3, r6
	ADD      r3, r3, r7
	MULI      r3, r3, 3
	LWI       r4, r5, 8
	ADD      r3, r3, r4
	STORE     r3, r8, 2
	rtsd      r15, 8
	NOP    
	.end	_ZN5Image3setEiiRK5Color
$tmp8:
	.size	_ZN5Image3setEiiRK5Color, ($tmp8)-_ZN5Image3setEiiRK5Color

	.globl	_Z9trax_mainv
	.align	2
	.type	_Z9trax_mainv,@function
	.ent	_Z9trax_mainv           # @_Z9trax_mainv
_Z9trax_mainv:
	.frame	r1,24,r15
	.mask	0x3f00000
# BB#0:                                 # %entry
	ADDI      r1, r1, -24
	SWI       r20, r1, 20
	SWI       r21, r1, 16
	SWI       r22, r1, 12
	SWI       r23, r1, 8
	SWI       r24, r1, 4
	SWI       r25, r1, 0
	ADD      r5, r0, r0
	LOAD      r3, r5, 7
	LOAD      r4, r5, 1
	LOAD      r6, r5, 4
	MUL       r5, r6, r4
	ATOMIC_INC r10, 0
	CMP       r7, r5, r10
	bgeid     r7, ($BB9_19)
	NOP    
# BB#1:                                 # %for.body.lr.ph
	FPCONV       r6, r6
	ORI       r7, r0, 1056964608
	FPMUL      r6, r6, r7
	FPCONV       r8, r4
	FPMUL      r7, r8, r7
	ORI       r9, r0, 1068708659
	FPDIV      r8, r8, r9
$BB9_2:                                 # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_3 Depth 2
	DIV      r11, r4, r10
	MUL       r9, r11, r4
	RSUB     r10, r9, r10
	FPCONV       r12, r10
	FPRSUB     r20, r8, r12
	FPCONV       r11, r11
	FPRSUB     r11, r6, r11
	FPDIV      r12, r11, r6
	FPDIV      r20, r20, r7
	ORI       r21, r0, 0
	ADD      r11, r0, r0
	ADD      r23, r21, r0
$BB9_3:                                 # %do.body
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	FPMUL      r22, r21, r21
	FPMUL      r24, r23, r23
	FPRSUB     r22, r22, r24
	FPADD      r22, r22, r20
	FPADD      r23, r23, r23
	FPMUL      r21, r23, r21
	FPADD      r21, r21, r12
	FPMUL      r23, r21, r21
	FPMUL      r24, r22, r22
	FPADD      r23, r24, r23
	ORI       r24, r0, 1082130432
	FPGE   r25, r23, r24
	FPUN   r23, r23, r24
	BITOR        r24, r23, r25
	bneid     r24, ($BB9_5)
	ADDI      r23, r0, 1
# BB#4:                                 # %do.body
                                        #   in Loop: Header=BB9_3 Depth=2
	ADDI      r23, r0, 0
$BB9_5:                                 # %do.body
                                        #   in Loop: Header=BB9_3 Depth=2
	bneid     r23, ($BB9_7)
	ADDI      r11, r11, 1
# BB#6:                                 # %do.body
                                        #   in Loop: Header=BB9_3 Depth=2
	ADDI      r23, r0, 100
	CMP       r24, r23, r11
	bltid     r24, ($BB9_3)
	ADD      r23, r22, r0
$BB9_7:                                 # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	ADDI      r12, r0, 100
	CMP       r22, r12, r11
	ADDI      r21, r0, 0
	ADDI      r20, r0, 1
	beqid     r22, ($BB9_9)
	ADD      r12, r20, r0
# BB#8:                                 # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	ADD      r12, r21, r0
$BB9_9:                                 # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	FPCONV       r23, r11
	ORI       r11, r0, 0
	bneid     r12, ($BB9_11)
	ADD      r22, r11, r0
# BB#10:                                # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	ADD      r22, r23, r0
$BB9_11:                                # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	ORI       r12, r0, 1120403456
	FPDIV      r12, r22, r12
	FPLT   r22, r12, r11
	bneid     r22, ($BB9_13)
	NOP    
# BB#12:                                # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	ADD      r20, r21, r0
$BB9_13:                                # %do.end
                                        #   in Loop: Header=BB9_2 Depth=1
	bneid     r20, ($BB9_18)
	NOP    
# BB#14:                                # %cond.false.i
                                        #   in Loop: Header=BB9_2 Depth=1
	ORI       r11, r0, 1065353216
	FPGE   r21, r12, r11
	bneid     r21, ($BB9_16)
	ADDI      r20, r0, 1
# BB#15:                                # %cond.false.i
                                        #   in Loop: Header=BB9_2 Depth=1
	ADDI      r20, r0, 0
$BB9_16:                                # %cond.false.i
                                        #   in Loop: Header=BB9_2 Depth=1
	bneid     r20, ($BB9_18)
	NOP    
# BB#17:                                # %cond.false5.i
                                        #   in Loop: Header=BB9_2 Depth=1
	ADD      r11, r12, r0
$BB9_18:                                # %_ZN5Image3setEiiRK5Color.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	ADD      r9, r9, r10
	MULI      r9, r9, 3
	ADD      r9, r9, r3
	STORE     r9, r11, 0
	ORI       r10, r0, 0
	STORE     r9, r10, 1
	STORE     r9, r10, 2
	ATOMIC_INC r10, 0
	CMP       r9, r5, r10
	bltid     r9, ($BB9_2)
	NOP    
$BB9_19:                                # %for.end
	LWI       r25, r1, 0
	LWI       r24, r1, 4
	LWI       r23, r1, 8
	LWI       r22, r1, 12
	LWI       r21, r1, 16
	LWI       r20, r1, 20
	rtsd      r15, 8
	ADDI      r1, r1, 24
	.end	_Z9trax_mainv
$tmp9:
	.size	_Z9trax_mainv, ($tmp9)-_Z9trax_mainv

	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
	.frame	r1,4,r15
	.mask	0x8000
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r15, r1, 0
	brlid     r15, _Z9trax_mainv
	NOP    
	ADD      r3, r0, r0
	LWI       r15, r1, 0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	main
$tmp10:
	.size	main, ($tmp10)-main

	.globl	_Z6printfPKcz
	.align	2
	.type	_Z6printfPKcz,@function
	.ent	_Z6printfPKcz           # @_Z6printfPKcz
_Z6printfPKcz:
	.frame	r1,4,r15
	.mask	0x0
# BB#0:                                 # %entry
	ADDI      r1, r1, -4
	SWI       r10, r1, 28
	SWI       r9, r1, 24
	SWI       r8, r1, 20
	SWI       r7, r1, 16
	SWI       r6, r1, 12
	SWI       r5, r1, 8
	ADDI      r3, r1, 8
	PRINTF     r3
	ADD      r3, r0, r0
	rtsd      r15, 8
	ADDI      r1, r1, 4
	.end	_Z6printfPKcz
$tmp11:
	.size	_Z6printfPKcz, ($tmp11)-_Z6printfPKcz


	.globl	_ZN5ColorC1Ev
_ZN5ColorC1Ev = _ZN5ColorC2Ev
	.globl	_ZN5ColorC1ERKS_
_ZN5ColorC1ERKS_ = _ZN5ColorC2ERKS_
	.globl	_ZN5ColorC1Efff
_ZN5ColorC1Efff = _ZN5ColorC2Efff
	.globl	_ZN5ImageC1Eiii
_ZN5ImageC1Eiii = _ZN5ImageC2Eiii
